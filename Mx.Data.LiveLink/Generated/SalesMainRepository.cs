///------------------------------------------------------------------------------
/// <autogenerated>
///     This code was generated by Macromatix DataGenerator 1.4.0.12
///
///     Date:		21 May 2013
///     Time:		03:26 PM
///		Server:		localhost
///		Database:	macromatix
///
///     Changes to this file may cause incorrect behavior and will be lost if
///     the code is regenerated.
/// </autogenerated>
///------------------------------------------------------------------------------

using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Mx.Common.Data;
using System.Collections.Generic;
using System.Xml;

namespace Mx.Data.LiveLink
{
  
    public partial class SalesMainRepository
    {
          #region Wrappers for GetRecordByTransaction

        public static IDataReader GetRecordByTransaction_Reader(int entityID, DateTime businessDay, int registerID, long transactionID, IDbTransaction transaction)
        {
 
            IDbConnection connection = null;
            try
            {
			
                if (transaction == null)
                {
                    connection = DataHelper.CreateDbConnection();
                    connection.Open();
                }
                else
                {
                    connection = transaction.Connection;
                }
	            IDbCommand command = DataHelper.CreateCommand(
                  "Select Top 1 * from tbSalesMain with (nolock)\r\n"
                + "            Where EntityID = @EntityID\r\n"
                + "            And BusinessDay = @BusinessDay\r\n"
                + "            And RegisterID = @RegisterID\r\n"
                + "            And TransactionID = @TransactionID\r\n"
                + "            And RecordType = 'H'\r\n", connection, CommandType.Text, transaction);
                command.Parameters.Add(DataHelper.CreateParameter("@EntityID", SqlDbType.Int, 4, DataHelper.ParameterDbValue(entityID)));
                command.Parameters.Add(DataHelper.CreateParameter("@BusinessDay", SqlDbType.DateTime, 8, DataHelper.ParameterDbValue(businessDay)));
                command.Parameters.Add(DataHelper.CreateParameter("@RegisterID", SqlDbType.Int, 4, DataHelper.ParameterDbValue(registerID)));
                command.Parameters.Add(DataHelper.CreateParameter("@TransactionID", SqlDbType.BigInt, 8, DataHelper.ParameterDbValue(transactionID)));


                CommandBehavior commandBehaviour = transaction == null ? CommandBehavior.CloseConnection : CommandBehavior.Default;
               
                return new MxDataReader(command.ExecuteReader(commandBehaviour));
    

            }
            catch(Exception exception)
            {
                if (DataHelper.HandleException(exception))
                    throw;
            }     
            return null;
        }

        public static IDataReader GetRecordByTransaction_Reader(int entityID, DateTime businessDay, int registerID, long transactionID)
        {
            return GetRecordByTransaction_Reader(entityID, businessDay, registerID, transactionID, null);
        }


        #endregion Wrappers for GetRecordByTransaction


        #region Extras For GetRecordByTransaction


      /*
       Use this XML snippet to construct a business object in the BusinessObjects project
       by pasting it into an appropriate business object.xml file
      
      <members>
      		<member name="SalesMainID" type="long" />
		<member name="RecordType" type="string" />
		<member name="TransactionID" type="long" />
		<member name="RecordSubType" type="string" />
		<member name="EntityID" type="int" />
		<member name="RegisterID" type="int" />
		<member name="PollDate" type="string" />
		<member name="PollCount" type="Double" />
		<member name="PollAmount" type="Decimal" />
		<member name="ClerkID" type="int" />
		<member name="ClerkName" type="string" />
		<member name="CustomerID" type="string" />
		<member name="Synced" type="int" />
		<member name="PLUCodeID" type="long" />
		<member name="PLUCode" type="string" />
		<member name="SequenceNo" type="int" />
		<member name="Flag" type="int" />
		<member name="ApplyTax" type="int" />
		<member name="ItemTax" type="Decimal" />
		<member name="PriceLevel" type="string" />
		<member name="DateAdded" type="DateTime" />
		<member name="SubTypeDescription" type="string" />
		<member name="POSTransactionID" type="long" />
		<member name="TransactionVersion" type="int" />
		<member name="ParentId" type="long" />
		<member name="BusinessDay" type="DateTime" />
		<member name="ItemDiscount" type="Decimal" />
		<member name="SyncToken" type="string" />

      </members>
      */
    
        #endregion

        public static int UpdateSalesMain(long? salesMainID, string recordType, long? transactionID, string recordSubType,
            int? entityID, int? registerID, string pollDate, double? pollCount,
            decimal? pollAmount, int? clerkID, string clerkName, string customerID,
            int? synced, long? pLUCodeID, string pLUCode, int? sequenceNo,
            int? flag, int? applyTax, decimal? itemTax, string priceLevel,
            DateTime? dateAdded, string subTypeDescription, long? pOSTransactionID, int? transactionVersion,
            long? parentId, DateTime? businessDay, decimal? itemDiscount, string syncToken, IDbTransaction transaction)
        {
            int recordsAffected = 0;
 
            IDbConnection connection = null;
            try
            {
			
                if (transaction == null)
                {
                    connection = DataHelper.CreateDbConnection();
                    connection.Open();
                }
                else
                {
                    connection = transaction.Connection;
                }
	            IDbCommand command = DataHelper.CreateCommand(
                  "Update tbSalesMain Set \r\n"
                + "        [RecordType] = @RecordType,\r\n"
                + "        [TransactionID] = @TransactionID,\r\n"
                + "        [RecordSubType] = @RecordSubType,\r\n"
                + "        [EntityID] = @EntityID,\r\n"
                + "        [RegisterID] = @RegisterID,\r\n"
                + "        [PollDate] = @PollDate,\r\n"
                + "        [PollCount] = @PollCount,\r\n"
                + "        [PollAmount] = @PollAmount,\r\n"
                + "        [ClerkID] = @ClerkID,\r\n"
                + "        [ClerkName] = @ClerkName,\r\n"
                + "        [CustomerID] = @CustomerID,\r\n"
                + "        [Synced] = @Synced,\r\n"
                + "        [PLUCodeID] = @PLUCodeID,\r\n"
                + "        [PLUCode] = @PLUCode,\r\n"
                + "        [SequenceNo] = @SequenceNo,\r\n"
                + "        [Flag] = @Flag,\r\n"
                + "        [ApplyTax] = @ApplyTax,\r\n"
                + "        [ItemTax] = @ItemTax,\r\n"
                + "        [PriceLevel] = @PriceLevel,\r\n"
                + "        [DateAdded] = @DateAdded,\r\n"
                + "        [SubTypeDescription] = @SubTypeDescription,\r\n"
                + "        [POSTransactionID] = @POSTransactionID,\r\n"
                + "        [TransactionVersion] = @TransactionVersion,\r\n"
                + "        [ParentId] = @ParentId,\r\n"
                + "        [BusinessDay] = @BusinessDay,\r\n"
                + "        [ItemDiscount] = @ItemDiscount,\r\n"
                + "        [SyncToken] = @SyncToken\r\n"
                + " Where \r\n"
                + "    SalesMainID = @SalesMainID\r\n", connection, CommandType.Text, transaction);
                command.Parameters.Add(DataHelper.CreateParameter("@SalesMainID", SqlDbType.BigInt, 8, DataHelper.ParameterDbValue(salesMainID)));
                command.Parameters.Add(DataHelper.CreateParameter("@RecordType", SqlDbType.NVarChar, 255, DataHelper.ParameterDbValue(recordType)));
                command.Parameters.Add(DataHelper.CreateParameter("@TransactionID", SqlDbType.BigInt, 8, DataHelper.ParameterDbValue(transactionID)));
                command.Parameters.Add(DataHelper.CreateParameter("@RecordSubType", SqlDbType.NVarChar, 255, DataHelper.ParameterDbValue(recordSubType)));
                command.Parameters.Add(DataHelper.CreateParameter("@EntityID", SqlDbType.Int, 4, DataHelper.ParameterDbValue(entityID)));
                command.Parameters.Add(DataHelper.CreateParameter("@RegisterID", SqlDbType.Int, 4, DataHelper.ParameterDbValue(registerID)));
                command.Parameters.Add(DataHelper.CreateParameter("@PollDate", SqlDbType.NVarChar, 20, DataHelper.ParameterDbValue(pollDate)));
                command.Parameters.Add(DataHelper.CreateParameter("@PollCount", SqlDbType.Float, 8, DataHelper.ParameterDbValue(pollCount)));
                command.Parameters.Add(DataHelper.CreateParameter("@PollAmount", SqlDbType.Money, 8, DataHelper.ParameterDbValue(pollAmount)));
                command.Parameters.Add(DataHelper.CreateParameter("@ClerkID", SqlDbType.Int, 4, DataHelper.ParameterDbValue(clerkID)));
                command.Parameters.Add(DataHelper.CreateParameter("@ClerkName", SqlDbType.NVarChar, 255, DataHelper.ParameterDbValue(clerkName)));
                command.Parameters.Add(DataHelper.CreateParameter("@CustomerID", SqlDbType.NVarChar, 255, DataHelper.ParameterDbValue(customerID)));
                command.Parameters.Add(DataHelper.CreateParameter("@Synced", SqlDbType.Int, 4, DataHelper.ParameterDbValue(synced)));
                command.Parameters.Add(DataHelper.CreateParameter("@PLUCodeID", SqlDbType.BigInt, 8, DataHelper.ParameterDbValue(pLUCodeID)));
                command.Parameters.Add(DataHelper.CreateParameter("@PLUCode", SqlDbType.NVarChar, 255, DataHelper.ParameterDbValue(pLUCode)));
                command.Parameters.Add(DataHelper.CreateParameter("@SequenceNo", SqlDbType.Int, 4, DataHelper.ParameterDbValue(sequenceNo)));
                command.Parameters.Add(DataHelper.CreateParameter("@Flag", SqlDbType.Int, 4, DataHelper.ParameterDbValue(flag)));
                command.Parameters.Add(DataHelper.CreateParameter("@ApplyTax", SqlDbType.Int, 4, DataHelper.ParameterDbValue(applyTax)));
                command.Parameters.Add(DataHelper.CreateParameter("@ItemTax", SqlDbType.Money, 8, DataHelper.ParameterDbValue(itemTax)));
                command.Parameters.Add(DataHelper.CreateParameter("@PriceLevel", SqlDbType.NVarChar, 20, DataHelper.ParameterDbValue(priceLevel)));
                command.Parameters.Add(DataHelper.CreateParameter("@DateAdded", SqlDbType.DateTime, 8, DataHelper.ParameterDbValue(dateAdded)));
                command.Parameters.Add(DataHelper.CreateParameter("@SubTypeDescription", SqlDbType.NVarChar, 255, DataHelper.ParameterDbValue(subTypeDescription)));
                command.Parameters.Add(DataHelper.CreateParameter("@POSTransactionID", SqlDbType.BigInt, 8, DataHelper.ParameterDbValue(pOSTransactionID)));
                command.Parameters.Add(DataHelper.CreateParameter("@TransactionVersion", SqlDbType.Int, 4, DataHelper.ParameterDbValue(transactionVersion)));
                command.Parameters.Add(DataHelper.CreateParameter("@ParentId", SqlDbType.BigInt, 8, DataHelper.ParameterDbValue(parentId)));
                command.Parameters.Add(DataHelper.CreateParameter("@BusinessDay", SqlDbType.SmallDateTime, 4, DataHelper.ParameterDbValue(businessDay)));
                command.Parameters.Add(DataHelper.CreateParameter("@ItemDiscount", SqlDbType.Money, 8, DataHelper.ParameterDbValue(itemDiscount)));
                command.Parameters.Add(DataHelper.CreateParameter("@SyncToken", SqlDbType.VarChar, 36, DataHelper.ParameterDbValue(syncToken)));

                recordsAffected = command.ExecuteNonQuery();

                command.Dispose();

            }
            catch(Exception exception)
            {
                if (DataHelper.HandleException(exception))
                    throw;
            }     
    
            finally
            {
                if (transaction == null)
                    DataHelper.CloseConnection(connection);
            }     
            return recordsAffected;
        }

        public static int UpdateSalesMain(long? salesMainID, string recordType, long? transactionID, string recordSubType,
            int? entityID, int? registerID, string pollDate, double? pollCount,
            decimal? pollAmount, int? clerkID, string clerkName, string customerID,
            int? synced, long? pLUCodeID, string pLUCode, int? sequenceNo,
            int? flag, int? applyTax, decimal? itemTax, string priceLevel,
            DateTime? dateAdded, string subTypeDescription, long? pOSTransactionID, int? transactionVersion,
            long? parentId, DateTime? businessDay, decimal? itemDiscount, string syncToken)
        {
            return UpdateSalesMain(salesMainID, recordType, transactionID, recordSubType, entityID, registerID,
            pollDate, pollCount, pollAmount, clerkID, clerkName, customerID,
            synced, pLUCodeID, pLUCode, sequenceNo, flag, applyTax,
            itemTax, priceLevel, dateAdded, subTypeDescription, pOSTransactionID, transactionVersion,
            parentId, businessDay, itemDiscount, syncToken, null);
        }


        public static int InsertSalesMain(out long? salesMainID, string recordType, long? transactionID, string recordSubType,
            int? entityID, int? registerID, string pollDate, double? pollCount,
            decimal? pollAmount, int? clerkID, string clerkName, string customerID,
            int? synced, long? pLUCodeID, string pLUCode, int? sequenceNo,
            int? flag, int? applyTax, decimal? itemTax, string priceLevel,
            DateTime? dateAdded, string subTypeDescription, long? pOSTransactionID, int? transactionVersion,
            long? parentId, DateTime? businessDay, decimal? itemDiscount, string syncToken, IDbTransaction transaction)
        {
            int recordsAffected = 0;
            salesMainID = null;
 
            IDbConnection connection = null;
            try
            {
			
                if (transaction == null)
                {
                    connection = DataHelper.CreateDbConnection();
                    connection.Open();
                }
                else
                {
                    connection = transaction.Connection;
                }
	            IDbCommand command = DataHelper.CreateCommand(
                  "Insert into tbSalesMain (\r\n"
                + "    [RecordType],\r\n"
                + "    [TransactionID],\r\n"
                + "    [RecordSubType],\r\n"
                + "    [EntityID],\r\n"
                + "    [RegisterID],\r\n"
                + "    [PollDate],\r\n"
                + "    [PollCount],\r\n"
                + "    [PollAmount],\r\n"
                + "    [ClerkID],\r\n"
                + "    [ClerkName],\r\n"
                + "    [CustomerID],\r\n"
                + "    [Synced],\r\n"
                + "    [PLUCodeID],\r\n"
                + "    [PLUCode],\r\n"
                + "    [SequenceNo],\r\n"
                + "    [Flag],\r\n"
                + "    [ApplyTax],\r\n"
                + "    [ItemTax],\r\n"
                + "    [PriceLevel],\r\n"
                + "    [DateAdded],\r\n"
                + "    [SubTypeDescription],\r\n"
                + "    [POSTransactionID],\r\n"
                + "    [TransactionVersion],\r\n"
                + "    [ParentId],\r\n"
                + "    [BusinessDay],\r\n"
                + "    [ItemDiscount],\r\n"
                + "    [SyncToken]\r\n"
                + ") Values (\r\n"
                + "    @RecordType,\r\n"
                + "    @TransactionID,\r\n"
                + "    @RecordSubType,\r\n"
                + "    @EntityID,\r\n"
                + "    @RegisterID,\r\n"
                + "    @PollDate,\r\n"
                + "    @PollCount,\r\n"
                + "    @PollAmount,\r\n"
                + "    @ClerkID,\r\n"
                + "    @ClerkName,\r\n"
                + "    @CustomerID,\r\n"
                + "    @Synced,\r\n"
                + "    @PLUCodeID,\r\n"
                + "    @PLUCode,\r\n"
                + "    @SequenceNo,\r\n"
                + "    @Flag,\r\n"
                + "    @ApplyTax,\r\n"
                + "    @ItemTax,\r\n"
                + "    @PriceLevel,\r\n"
                + "    @DateAdded,\r\n"
                + "    @SubTypeDescription,\r\n"
                + "    @POSTransactionID,\r\n"
                + "    @TransactionVersion,\r\n"
                + "    @ParentId,\r\n"
                + "    @BusinessDay,\r\n"
                + "    @ItemDiscount,\r\n"
                + "    @SyncToken)\r\n"
                + DataHelper.GetIdentity("@SalesMainID"), connection, CommandType.Text, transaction);
                command.Parameters.Add(DataHelper.CreateParameter("@SalesMainID", SqlDbType.BigInt, 8, DataHelper.ParameterDbValue(salesMainID), true));
                command.Parameters.Add(DataHelper.CreateParameter("@RecordType", SqlDbType.NVarChar, 255, DataHelper.ParameterDbValue(recordType)));
                command.Parameters.Add(DataHelper.CreateParameter("@TransactionID", SqlDbType.BigInt, 8, DataHelper.ParameterDbValue(transactionID)));
                command.Parameters.Add(DataHelper.CreateParameter("@RecordSubType", SqlDbType.NVarChar, 255, DataHelper.ParameterDbValue(recordSubType)));
                command.Parameters.Add(DataHelper.CreateParameter("@EntityID", SqlDbType.Int, 4, DataHelper.ParameterDbValue(entityID)));
                command.Parameters.Add(DataHelper.CreateParameter("@RegisterID", SqlDbType.Int, 4, DataHelper.ParameterDbValue(registerID)));
                command.Parameters.Add(DataHelper.CreateParameter("@PollDate", SqlDbType.NVarChar, 20, DataHelper.ParameterDbValue(pollDate)));
                command.Parameters.Add(DataHelper.CreateParameter("@PollCount", SqlDbType.Float, 8, DataHelper.ParameterDbValue(pollCount)));
                command.Parameters.Add(DataHelper.CreateParameter("@PollAmount", SqlDbType.Money, 8, DataHelper.ParameterDbValue(pollAmount)));
                command.Parameters.Add(DataHelper.CreateParameter("@ClerkID", SqlDbType.Int, 4, DataHelper.ParameterDbValue(clerkID)));
                command.Parameters.Add(DataHelper.CreateParameter("@ClerkName", SqlDbType.NVarChar, 255, DataHelper.ParameterDbValue(clerkName)));
                command.Parameters.Add(DataHelper.CreateParameter("@CustomerID", SqlDbType.NVarChar, 255, DataHelper.ParameterDbValue(customerID)));
                command.Parameters.Add(DataHelper.CreateParameter("@Synced", SqlDbType.Int, 4, DataHelper.ParameterDbValue(synced)));
                command.Parameters.Add(DataHelper.CreateParameter("@PLUCodeID", SqlDbType.BigInt, 8, DataHelper.ParameterDbValue(pLUCodeID)));
                command.Parameters.Add(DataHelper.CreateParameter("@PLUCode", SqlDbType.NVarChar, 255, DataHelper.ParameterDbValue(pLUCode)));
                command.Parameters.Add(DataHelper.CreateParameter("@SequenceNo", SqlDbType.Int, 4, DataHelper.ParameterDbValue(sequenceNo)));
                command.Parameters.Add(DataHelper.CreateParameter("@Flag", SqlDbType.Int, 4, DataHelper.ParameterDbValue(flag)));
                command.Parameters.Add(DataHelper.CreateParameter("@ApplyTax", SqlDbType.Int, 4, DataHelper.ParameterDbValue(applyTax)));
                command.Parameters.Add(DataHelper.CreateParameter("@ItemTax", SqlDbType.Money, 8, DataHelper.ParameterDbValue(itemTax)));
                command.Parameters.Add(DataHelper.CreateParameter("@PriceLevel", SqlDbType.NVarChar, 20, DataHelper.ParameterDbValue(priceLevel)));
                command.Parameters.Add(DataHelper.CreateParameter("@DateAdded", SqlDbType.DateTime, 8, DataHelper.ParameterDbValue(dateAdded)));
                command.Parameters.Add(DataHelper.CreateParameter("@SubTypeDescription", SqlDbType.NVarChar, 255, DataHelper.ParameterDbValue(subTypeDescription)));
                command.Parameters.Add(DataHelper.CreateParameter("@POSTransactionID", SqlDbType.BigInt, 8, DataHelper.ParameterDbValue(pOSTransactionID)));
                command.Parameters.Add(DataHelper.CreateParameter("@TransactionVersion", SqlDbType.Int, 4, DataHelper.ParameterDbValue(transactionVersion)));
                command.Parameters.Add(DataHelper.CreateParameter("@ParentId", SqlDbType.BigInt, 8, DataHelper.ParameterDbValue(parentId)));
                command.Parameters.Add(DataHelper.CreateParameter("@BusinessDay", SqlDbType.SmallDateTime, 4, DataHelper.ParameterDbValue(businessDay)));
                command.Parameters.Add(DataHelper.CreateParameter("@ItemDiscount", SqlDbType.Money, 8, DataHelper.ParameterDbValue(itemDiscount)));
                command.Parameters.Add(DataHelper.CreateParameter("@SyncToken", SqlDbType.VarChar, 36, DataHelper.ParameterDbValue(syncToken)));

                recordsAffected = command.ExecuteNonQuery();
                salesMainID =  DataHelper.GetIdentityValue<long>(command,"@SalesMainID");

                command.Dispose();

            }
            catch(Exception exception)
            {
                if (DataHelper.HandleException(exception))
                    throw;
            }     
    
            finally
            {
                if (transaction == null)
                    DataHelper.CloseConnection(connection);
            }     
            return recordsAffected;
        }

        public static int InsertSalesMain(out long? salesMainID, string recordType, long? transactionID, string recordSubType,
            int? entityID, int? registerID, string pollDate, double? pollCount,
            decimal? pollAmount, int? clerkID, string clerkName, string customerID,
            int? synced, long? pLUCodeID, string pLUCode, int? sequenceNo,
            int? flag, int? applyTax, decimal? itemTax, string priceLevel,
            DateTime? dateAdded, string subTypeDescription, long? pOSTransactionID, int? transactionVersion,
            long? parentId, DateTime? businessDay, decimal? itemDiscount, string syncToken)
        {
            return InsertSalesMain(out salesMainID, recordType, transactionID, recordSubType, entityID, registerID,
            pollDate, pollCount, pollAmount, clerkID, clerkName, customerID,
            synced, pLUCodeID, pLUCode, sequenceNo, flag, applyTax,
            itemTax, priceLevel, dateAdded, subTypeDescription, pOSTransactionID, transactionVersion,
            parentId, businessDay, itemDiscount, syncToken, null);
        }


        public static int DeleteSalesMain(long? salesMainID, IDbTransaction transaction)
        {
            int recordsAffected = 0;
 
            IDbConnection connection = null;
            try
            {
			
                if (transaction == null)
                {
                    connection = DataHelper.CreateDbConnection();
                    connection.Open();
                }
                else
                {
                    connection = transaction.Connection;
                }
	            IDbCommand command = DataHelper.CreateCommand(
                  "Delete from tbSalesMain  Where \r\n"
                + "    SalesMainID = @SalesMainID\r\n", connection, CommandType.Text, transaction);
                command.Parameters.Add(DataHelper.CreateParameter("@SalesMainID", SqlDbType.BigInt, 8, DataHelper.ParameterDbValue(salesMainID)));

                recordsAffected = command.ExecuteNonQuery();

                command.Dispose();

            }
            catch(Exception exception)
            {
                if (DataHelper.HandleException(exception))
                    throw;
            }     
    
            finally
            {
                if (transaction == null)
                    DataHelper.CloseConnection(connection);
            }     
            return recordsAffected;
        }

        public static int DeleteSalesMain(long? salesMainID)
        {
            return DeleteSalesMain(salesMainID, null);
        }


        public static IDataReader SelectSalesMain_Reader(long? salesMainID, IDbTransaction transaction)
        {
 
            IDbConnection connection = null;
            try
            {
			
                if (transaction == null)
                {
                    connection = DataHelper.CreateDbConnection();
                    connection.Open();
                }
                else
                {
                    connection = transaction.Connection;
                }
	            IDbCommand command = DataHelper.CreateCommand(
                  "Select \r\n"
                + "        [SalesMainID],\r\n"
                + "        [RecordType],\r\n"
                + "        [TransactionID],\r\n"
                + "        [RecordSubType],\r\n"
                + "        [EntityID],\r\n"
                + "        [RegisterID],\r\n"
                + "        [PollDate],\r\n"
                + "        [PollCount],\r\n"
                + "        [PollAmount],\r\n"
                + "        [ClerkID],\r\n"
                + "        [ClerkName],\r\n"
                + "        [CustomerID],\r\n"
                + "        [Synced],\r\n"
                + "        [PLUCodeID],\r\n"
                + "        [PLUCode],\r\n"
                + "        [SequenceNo],\r\n"
                + "        [Flag],\r\n"
                + "        [ApplyTax],\r\n"
                + "        [ItemTax],\r\n"
                + "        [PriceLevel],\r\n"
                + "        [DateAdded],\r\n"
                + "        [SubTypeDescription],\r\n"
                + "        [POSTransactionID],\r\n"
                + "        [TransactionVersion],\r\n"
                + "        [ParentId],\r\n"
                + "        [BusinessDay],\r\n"
                + "        [ItemDiscount],\r\n"
                + "        [SyncToken]\r\n"
                + "     From tbSalesMain \r\n"
                + "  Where \r\n"
                + "    SalesMainID = @SalesMainID\r\n", connection, CommandType.Text, transaction);
                command.Parameters.Add(DataHelper.CreateParameter("@SalesMainID", SqlDbType.BigInt, 8, DataHelper.ParameterDbValue(salesMainID)));


                CommandBehavior commandBehaviour = transaction == null ? CommandBehavior.CloseConnection : CommandBehavior.Default;
               
                return new MxDataReader(command.ExecuteReader(commandBehaviour));
    

            }
            catch(Exception exception)
            {
                if (DataHelper.HandleException(exception))
                    throw;
            }     
            return null;
        }

        public static IDataReader SelectSalesMain_Reader(long? salesMainID)
        {
            return SelectSalesMain_Reader(salesMainID, null);
        }


        #region Extras For SelectSalesMain


      /*
       Use this XML snippet to construct a business object in the BusinessObjects project
       by pasting it into an appropriate business object.xml file
      
      <members>
      		<member name="SalesMainID" type="long" />
		<member name="RecordType" type="string" />
		<member name="TransactionID" type="long" />
		<member name="RecordSubType" type="string" />
		<member name="EntityID" type="int" />
		<member name="RegisterID" type="int" />
		<member name="PollDate" type="string" />
		<member name="PollCount" type="Double" />
		<member name="PollAmount" type="Decimal" />
		<member name="ClerkID" type="int" />
		<member name="ClerkName" type="string" />
		<member name="CustomerID" type="string" />
		<member name="Synced" type="int" />
		<member name="PLUCodeID" type="long" />
		<member name="PLUCode" type="string" />
		<member name="SequenceNo" type="int" />
		<member name="Flag" type="int" />
		<member name="ApplyTax" type="int" />
		<member name="ItemTax" type="Decimal" />
		<member name="PriceLevel" type="string" />
		<member name="DateAdded" type="DateTime" />
		<member name="SubTypeDescription" type="string" />
		<member name="POSTransactionID" type="long" />
		<member name="TransactionVersion" type="int" />
		<member name="ParentId" type="long" />
		<member name="BusinessDay" type="DateTime" />
		<member name="ItemDiscount" type="Decimal" />
		<member name="SyncToken" type="string" />

      </members>
      */
    
        #endregion


    }
}  
    