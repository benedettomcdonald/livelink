///------------------------------------------------------------------------------
/// <autogenerated>
///     This code was generated by Macromatix DataGenerator
///
///     Date:    28 Apr 2010
///     Time:    03:15 Pm
///
///     Changes to this file may cause incorrect behavior and will be lost if
///     the code is regenerated.
/// </autogenerated>
///------------------------------------------------------------------------------

using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Mx.Common;
using Mx.Common.Data;
using System.Collections.Generic;

namespace Mx.Data.LiveLink
{
  
    public partial class SessionCommandRepository
    {
          #region Wrappers for SelectRequestByCommandID

        public static IDataReader SelectRequestByCommandID_Reader(int sessionCommandID, SqlTransaction transaction)
        {

            SqlConnection connection = null;
            try
            {
                if (transaction == null)
                {
                    connection = DataHelper.CreateSqlConnection();
                    connection.Open();
                }
                else
                {
                    connection = transaction.Connection;
                }
                SqlCommand command = DataHelper.CreateCommand(
                  "      SELECT SessionCommandRequestID, SessionCommandId, MachineID, CommandType, CommandAction, CommandText, DateAdded"
                + "      FROM tbSessionCommandRequest"
                + "      WHERE SessionCommandID = @SessionCommandID"
                + "    ", connection, CommandType.Text, transaction, 0);
                command.Parameters.Add(new SqlParameter("@SessionCommandID", SqlDbType.Int)).Value = DataHelper.ParameterDbValue(sessionCommandID);


                CommandBehavior commandBehaviour = CommandBehavior.CloseConnection;
                if (transaction != null)
                {
                    commandBehaviour = CommandBehavior.Default;
                    command.Transaction = transaction;
                }
                return new MxDataReader(command.ExecuteReader(commandBehaviour));
    

            }
            catch(Exception exception)
            {
                if (DataHelper.HandleException(exception))
                    throw;
            }     
            return null;
        }

        public static IDataReader SelectRequestByCommandID_Reader(int sessionCommandID)
        {
            return SelectRequestByCommandID_Reader(sessionCommandID, null);
        }


        #endregion Wrappers for SelectRequestByCommandID


        #region Extras For SelectRequestByCommandID


      /*
       Use this XML snippet to construct a business object in the BusinessObjects project
       by pasting it into an appropriate business object.xml file
      
      <members>
      		<member name="SessionCommandRequestID" type="int" />
		<member name="SessionCommandId" type="int" />
		<member name="MachineID" type="string" />
		<member name="CommandType" type="string" />
		<member name="CommandAction" type="string" />
		<member name="CommandText" type="string" />
		<member name="DateAdded" type="DateTime" />

      </members>
      */
    
        #endregion

        #region Wrappers for SelectResponseByCommandID

        public static IDataReader SelectResponseByCommandID_Reader(int sessionCommandID, SqlTransaction transaction)
        {

            SqlConnection connection = null;
            try
            {
                if (transaction == null)
                {
                    connection = DataHelper.CreateSqlConnection();
                    connection.Open();
                }
                else
                {
                    connection = transaction.Connection;
                }
                SqlCommand command = DataHelper.CreateCommand(
                  "      SELECT SessionCommandResponseID, SessionCommandId, [Status], Response, DateAdded"
                + "      FROM tbSessionCommandResponse"
                + "      WHERE SessionCommandID = @SessionCommandID"
                + "    ", connection, CommandType.Text, transaction, 0);
                command.Parameters.Add(new SqlParameter("@SessionCommandID", SqlDbType.Int)).Value = DataHelper.ParameterDbValue(sessionCommandID);


                CommandBehavior commandBehaviour = CommandBehavior.CloseConnection;
                if (transaction != null)
                {
                    commandBehaviour = CommandBehavior.Default;
                    command.Transaction = transaction;
                }
                return new MxDataReader(command.ExecuteReader(commandBehaviour));
    

            }
            catch(Exception exception)
            {
                if (DataHelper.HandleException(exception))
                    throw;
            }     
            return null;
        }

        public static IDataReader SelectResponseByCommandID_Reader(int sessionCommandID)
        {
            return SelectResponseByCommandID_Reader(sessionCommandID, null);
        }


        #endregion Wrappers for SelectResponseByCommandID


        #region Extras For SelectResponseByCommandID


      /*
       Use this XML snippet to construct a business object in the BusinessObjects project
       by pasting it into an appropriate business object.xml file
      
      <members>
      		<member name="SessionCommandResponseID" type="int" />
		<member name="SessionCommandId" type="int" />
		<member name="Status" type="byte" />
		<member name="Response" type="string" />
		<member name="DateAdded" type="DateTime" />

      </members>
      */
    
        #endregion

        public static int InsertSessionCommandRequest(out int? sessionCommandRequestID, int sessionCommandId, string machineID, string commandType,
            string commandAction, string commandText, DateTime dateAdded, SqlTransaction transaction, int timeoutValue)
        {
            int recordsAffected = 0;
            sessionCommandRequestID = null;

            SqlConnection connection = null;
            try
            {
                if (transaction == null)
                {
                    connection = DataHelper.CreateSqlConnection();
                    connection.Open();
                }
                else
                {
                    connection = transaction.Connection;
                }
                SqlCommand command = DataHelper.CreateCommand(
                  "Insert into [dbo].[tbSessionCommandRequest] ("
                + "    SessionCommandId,"
                + "    MachineID,"
                + "    CommandType,"
                + "    CommandAction,"
                + "    CommandText,"
                + "    DateAdded"
                + ") Values ("
                + "    @SessionCommandId,"
                + "    @MachineID,"
                + "    @CommandType,"
                + "    @CommandAction,"
                + "    @CommandText,"
                + "    @DateAdded)"
                + "    Select @SessionCommandRequestID = Scope_Identity() ", connection, CommandType.Text, transaction, timeoutValue);
                SqlParameter sessionCommandRequestIDParm  = command.Parameters.Add(new SqlParameter("@SessionCommandRequestID", SqlDbType.Int, 4));
                sessionCommandRequestIDParm.Direction = ParameterDirection.Output;
                command.Parameters.Add(new SqlParameter("@SessionCommandId", SqlDbType.Int, 4)).Value = DataHelper.ParameterDbValue(sessionCommandId);
                command.Parameters.Add(new SqlParameter("@MachineID", SqlDbType.NVarChar, 40)).Value = DataHelper.ParameterDbValue(machineID);
                command.Parameters.Add(new SqlParameter("@CommandType", SqlDbType.NVarChar, 50)).Value = DataHelper.ParameterDbValue(commandType);
                command.Parameters.Add(new SqlParameter("@CommandAction", SqlDbType.NVarChar, 50)).Value = DataHelper.ParameterDbValue(commandAction);
                command.Parameters.Add(new SqlParameter("@CommandText", SqlDbType.NVarChar, -1)).Value = DataHelper.ParameterDbValue(commandText);
                command.Parameters.Add(new SqlParameter("@DateAdded", SqlDbType.DateTime, 8)).Value = DataHelper.ParameterDbValue(dateAdded);

                recordsAffected = command.ExecuteNonQuery();
                sessionCommandRequestID =  DataHelper.OutputValue<int>(command.Parameters["@SessionCommandRequestID"].Value);


            }
            catch(Exception exception)
            {
                if (DataHelper.HandleException(exception))
                    throw;
            }     
    
            finally
            {
                if (transaction == null)
                    DataHelper.CloseConnection(connection);
            }     
            return recordsAffected;
        }

        public static int InsertSessionCommandRequest(out int? sessionCommandRequestID, int sessionCommandId, string machineID, string commandType,
            string commandAction, string commandText, DateTime dateAdded)
        {
            return InsertSessionCommandRequest(out sessionCommandRequestID, sessionCommandId, machineID, commandType, commandAction, commandText,
            dateAdded, null, 0);
        }


        public static int InsertSessionCommandRequest(out int? sessionCommandRequestID, int sessionCommandId, string machineID, string commandType,
            string commandAction, string commandText, DateTime dateAdded, SqlTransaction transaction)
        {
            return InsertSessionCommandRequest(out sessionCommandRequestID, sessionCommandId, machineID, commandType, commandAction, commandText,
            dateAdded, transaction, 0);
        }


        public static int DeleteSessionCommandRequest(int sessionCommandRequestID, SqlTransaction transaction, int timeoutValue)
        {
            int recordsAffected = 0;

            SqlConnection connection = null;
            try
            {
                if (transaction == null)
                {
                    connection = DataHelper.CreateSqlConnection();
                    connection.Open();
                }
                else
                {
                    connection = transaction.Connection;
                }
                SqlCommand command = DataHelper.CreateCommand(
                  "Delete from tbSessionCommandRequest  Where "
                + "    SessionCommandRequestID = @SessionCommandRequestID", connection, CommandType.Text, transaction, timeoutValue);
                command.Parameters.Add(new SqlParameter("@SessionCommandRequestID", SqlDbType.Int, 4)).Value = DataHelper.ParameterDbValue(sessionCommandRequestID);

                recordsAffected = command.ExecuteNonQuery();


            }
            catch(Exception exception)
            {
                if (DataHelper.HandleException(exception))
                    throw;
            }     
    
            finally
            {
                if (transaction == null)
                    DataHelper.CloseConnection(connection);
            }     
            return recordsAffected;
        }

        public static int DeleteSessionCommandRequest(int sessionCommandRequestID)
        {
            return DeleteSessionCommandRequest(sessionCommandRequestID, null, 0);
        }


        public static int DeleteSessionCommandRequest(int sessionCommandRequestID, SqlTransaction transaction)
        {
            return DeleteSessionCommandRequest(sessionCommandRequestID, transaction, 0);
        }


        public static IDataReader SelectSessionCommandRequest_Reader(int sessionCommandRequestID, SqlTransaction transaction)
        {

            SqlConnection connection = null;
            try
            {
                if (transaction == null)
                {
                    connection = DataHelper.CreateSqlConnection();
                    connection.Open();
                }
                else
                {
                    connection = transaction.Connection;
                }
                SqlCommand command = DataHelper.CreateCommand(
                  "    Select "
                + "        SessionCommandRequestID,"
                + "        SessionCommandId,"
                + "        MachineID,"
                + "        CommandType,"
                + "        CommandAction,"
                + "        CommandText,"
                + "        DateAdded"
                + "     From tbSessionCommandRequest With (NoLock)"
                + "  Where "
                + "    SessionCommandRequestID = @SessionCommandRequestID ", connection, CommandType.Text, transaction, 0);
                command.Parameters.Add(new SqlParameter("@SessionCommandRequestID", SqlDbType.Int, 4)).Value = DataHelper.ParameterDbValue(sessionCommandRequestID);


                CommandBehavior commandBehaviour = CommandBehavior.CloseConnection;
                if (transaction != null)
                {
                    commandBehaviour = CommandBehavior.Default;
                    command.Transaction = transaction;
                }
                return new MxDataReader(command.ExecuteReader(commandBehaviour));
    

            }
            catch(Exception exception)
            {
                if (DataHelper.HandleException(exception))
                    throw;
            }     
            return null;
        }

        public static IDataReader SelectSessionCommandRequest_Reader(int sessionCommandRequestID)
        {
            return SelectSessionCommandRequest_Reader(sessionCommandRequestID, null);
        }


        #region Extras For SelectSessionCommandRequest


      /*
       Use this XML snippet to construct a business object in the BusinessObjects project
       by pasting it into an appropriate business object.xml file
      
      <members>
      		<member name="SessionCommandRequestID" type="int" />
		<member name="SessionCommandId" type="int" />
		<member name="MachineID" type="string" />
		<member name="CommandType" type="string" />
		<member name="CommandAction" type="string" />
		<member name="CommandText" type="string" />
		<member name="DateAdded" type="DateTime" />

      </members>
      */
    
        #endregion

        public static int InsertSessionCommandResponse(out int? sessionCommandResponseID, int sessionCommandId, byte status, string response,
            DateTime dateAdded, SqlTransaction transaction, int timeoutValue)
        {
            int recordsAffected = 0;
            sessionCommandResponseID = null;

            SqlConnection connection = null;
            try
            {
                if (transaction == null)
                {
                    connection = DataHelper.CreateSqlConnection();
                    connection.Open();
                }
                else
                {
                    connection = transaction.Connection;
                }
                SqlCommand command = DataHelper.CreateCommand(
                  "Insert into [dbo].[tbSessionCommandResponse] ("
                + "    SessionCommandId,"
                + "    Status,"
                + "    Response,"
                + "    DateAdded"
                + ") Values ("
                + "    @SessionCommandId,"
                + "    @Status,"
                + "    @Response,"
                + "    @DateAdded)"
                + "    Select @SessionCommandResponseID = Scope_Identity() ", connection, CommandType.Text, transaction, timeoutValue);
                SqlParameter sessionCommandResponseIDParm  = command.Parameters.Add(new SqlParameter("@SessionCommandResponseID", SqlDbType.Int, 4));
                sessionCommandResponseIDParm.Direction = ParameterDirection.Output;
                command.Parameters.Add(new SqlParameter("@SessionCommandId", SqlDbType.Int, 4)).Value = DataHelper.ParameterDbValue(sessionCommandId);
                command.Parameters.Add(new SqlParameter("@Status", SqlDbType.TinyInt, 1)).Value = DataHelper.ParameterDbValue(status);
                command.Parameters.Add(new SqlParameter("@Response", SqlDbType.NVarChar, -1)).Value = DataHelper.ParameterDbValue(response);
                command.Parameters.Add(new SqlParameter("@DateAdded", SqlDbType.DateTime, 8)).Value = DataHelper.ParameterDbValue(dateAdded);

                recordsAffected = command.ExecuteNonQuery();
                sessionCommandResponseID =  DataHelper.OutputValue<int>(command.Parameters["@SessionCommandResponseID"].Value);


            }
            catch(Exception exception)
            {
                if (DataHelper.HandleException(exception))
                    throw;
            }     
    
            finally
            {
                if (transaction == null)
                    DataHelper.CloseConnection(connection);
            }     
            return recordsAffected;
        }

        public static int InsertSessionCommandResponse(out int? sessionCommandResponseID, int sessionCommandId, byte status, string response,
            DateTime dateAdded)
        {
            return InsertSessionCommandResponse(out sessionCommandResponseID, sessionCommandId, status, response, dateAdded, null, 0);
        }


        public static int InsertSessionCommandResponse(out int? sessionCommandResponseID, int sessionCommandId, byte status, string response,
            DateTime dateAdded, SqlTransaction transaction)
        {
            return InsertSessionCommandResponse(out sessionCommandResponseID, sessionCommandId, status, response, dateAdded, transaction, 0);
        }


        public static int DeleteSessionCommandResponse(int sessionCommandResponseID, SqlTransaction transaction, int timeoutValue)
        {
            int recordsAffected = 0;

            SqlConnection connection = null;
            try
            {
                if (transaction == null)
                {
                    connection = DataHelper.CreateSqlConnection();
                    connection.Open();
                }
                else
                {
                    connection = transaction.Connection;
                }
                SqlCommand command = DataHelper.CreateCommand(
                  "Delete from tbSessionCommandResponse  Where "
                + "    SessionCommandResponseID = @SessionCommandResponseID", connection, CommandType.Text, transaction, timeoutValue);
                command.Parameters.Add(new SqlParameter("@SessionCommandResponseID", SqlDbType.Int, 4)).Value = DataHelper.ParameterDbValue(sessionCommandResponseID);

                recordsAffected = command.ExecuteNonQuery();


            }
            catch(Exception exception)
            {
                if (DataHelper.HandleException(exception))
                    throw;
            }     
    
            finally
            {
                if (transaction == null)
                    DataHelper.CloseConnection(connection);
            }     
            return recordsAffected;
        }

        public static int DeleteSessionCommandResponse(int sessionCommandResponseID)
        {
            return DeleteSessionCommandResponse(sessionCommandResponseID, null, 0);
        }


        public static int DeleteSessionCommandResponse(int sessionCommandResponseID, SqlTransaction transaction)
        {
            return DeleteSessionCommandResponse(sessionCommandResponseID, transaction, 0);
        }


        public static IDataReader SelectSessionCommandResponse_Reader(int sessionCommandResponseID, SqlTransaction transaction)
        {

            SqlConnection connection = null;
            try
            {
                if (transaction == null)
                {
                    connection = DataHelper.CreateSqlConnection();
                    connection.Open();
                }
                else
                {
                    connection = transaction.Connection;
                }
                SqlCommand command = DataHelper.CreateCommand(
                  "    Select "
                + "        SessionCommandResponseID,"
                + "        SessionCommandId,"
                + "        Status,"
                + "        Response,"
                + "        DateAdded"
                + "     From tbSessionCommandResponse With (NoLock)"
                + "  Where "
                + "    SessionCommandResponseID = @SessionCommandResponseID ", connection, CommandType.Text, transaction, 0);
                command.Parameters.Add(new SqlParameter("@SessionCommandResponseID", SqlDbType.Int, 4)).Value = DataHelper.ParameterDbValue(sessionCommandResponseID);


                CommandBehavior commandBehaviour = CommandBehavior.CloseConnection;
                if (transaction != null)
                {
                    commandBehaviour = CommandBehavior.Default;
                    command.Transaction = transaction;
                }
                return new MxDataReader(command.ExecuteReader(commandBehaviour));
    

            }
            catch(Exception exception)
            {
                if (DataHelper.HandleException(exception))
                    throw;
            }     
            return null;
        }

        public static IDataReader SelectSessionCommandResponse_Reader(int sessionCommandResponseID)
        {
            return SelectSessionCommandResponse_Reader(sessionCommandResponseID, null);
        }


        #region Extras For SelectSessionCommandResponse


      /*
       Use this XML snippet to construct a business object in the BusinessObjects project
       by pasting it into an appropriate business object.xml file
      
      <members>
      		<member name="SessionCommandResponseID" type="int" />
		<member name="SessionCommandId" type="int" />
		<member name="Status" type="byte" />
		<member name="Response" type="string" />
		<member name="DateAdded" type="DateTime" />

      </members>
      */
    
        #endregion


    }
}  
    