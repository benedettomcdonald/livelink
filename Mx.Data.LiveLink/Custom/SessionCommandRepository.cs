﻿using System;
using System.Data.SqlClient;
using System.Data;
using System.Collections.Generic;
using System.Text;

using Mx.BusinessObjects.LiveLink;
using Mx.Data.LiveLink.Interfaces;
using Mx.Common.Data;

namespace Mx.Data.LiveLink
{
    public partial class SessionCommandRepository : ISessionCommandRepository
    {
        #region ISessionCommandRepository Members

        public SessionCommandRequest GetRequestByID(int sessionCommandID)
        {
            IDataReader rdr = SelectRequestByCommandID_Reader(sessionCommandID);
            if (rdr.Read())
            {
                return new SessionCommandRequest(rdr);
            }
            else
                return null;
        }

        public bool InsertRequest(SessionCommandRequest request)
        {
            int? sessionCommandRequestID = null;
            int recordsAffected = InsertSessionCommandRequest(out sessionCommandRequestID, request.SessionCommandId, request.MachineID, request.CommandType, request.CommandAction, request.CommandText, DateTime.Now);

            if (sessionCommandRequestID.HasValue)
            {
                request.SessionCommandRequestID = sessionCommandRequestID.Value;
                return true;
            }
            return false;
        }

        public SessionCommandResponse GetResponseByID(int sessionCommandResponseID)
        {
            IDataReader rdr = SelectSessionCommandResponse_Reader(sessionCommandResponseID);
            if (rdr.Read())
            {
                return new SessionCommandResponse(rdr);
            }
            else
                return null;
        }

        public SessionCommandResponseList GetResponseListByCommandID(int sessionCommandID)
        {
            IDataReader rdr = SelectResponseByCommandID_Reader(sessionCommandID);
            if (rdr == null)
                return null;
            return new SessionCommandResponseList(rdr);
        }

        public bool InsertResponse(SessionCommandResponse response)
        {
            SqlTransaction transaction = null;
            bool status = true;

            try
            {
                // begin transaction
                transaction = DataHelper.BeginTransaction();

                Store entity = null;
                using (IDataReader reader = StoreInfo.GetEntityId_Reader(transaction))
                {
                    // get entity setup
                    if (reader.Read())
                        entity = new Store(reader);
                }

                if (entity == null)
                    throw new Exception("Can not save SessionCommandResponse. Entity not setup in tbStoreInfo");

                int? sessionCommandResponseID = null;
                int recordsAffected = InsertSessionCommandResponse(out sessionCommandResponseID, response.SessionCommandId, response.Status, response.Response, DateTime.Now, transaction);

                if (!sessionCommandResponseID.HasValue)
                {
                    status = false;
                    return false;
                }
                // save ID column value
                response.SessionCommandResponseID = sessionCommandResponseID.Value;

                // insert sync record for entityID
                if (LiveSyncData.Insert(entity.StoreID, "SessionCommandResponse", response.SessionCommandResponseID.Value, transaction)<=0)
                {
                    status = false;
                    return false;
                }
            }
            catch (Exception)
            {
                status = false;
            }
            finally
            {
                if (status)
                    transaction.Commit();
                else
                    transaction.Rollback();
            }

            return status;
        }

        #endregion
    }
}
