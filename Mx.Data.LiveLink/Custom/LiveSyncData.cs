﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Text;

using Mx.BusinessObjects.LiveLink;
using Mx.Data.LiveLink.Interfaces;
using Mx.Common.Data;

namespace Mx.Data.LiveLink
{
    public partial class LiveSyncData
    {
        public static LiveSyncList SelectUnsynced()
        {
            IDataReader rdr = SelectUnsynced_Reader();
            if (rdr == null)
                return null;
            return new LiveSyncList(rdr);
        }

    }
}
