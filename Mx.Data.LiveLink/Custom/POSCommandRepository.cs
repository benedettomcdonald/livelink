﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Text;

using Mx.BusinessObjects.LiveLink;
using Mx.Data.LiveLink.Interfaces;
using Mx.Common.Data;


namespace Mx.Data.LiveLink
{
    public partial class POSCommandRepository : IPOSCommandRepository
    {

        public POSCommandData GetDataByType(string type) { return GetDataByType(type, null); } 
        public POSCommandData GetDataByType(string type, string options)
        {
            POSCommandData result=null;
            using (IDataReader rdr = SelectPOSCommandDataByType_Reader(type))
            {
                if (rdr.Read())
                result = new POSCommandData(rdr);
            }
            return result;
          }

        public POSCommandData GetDataByLikeCommand(string options)
        {
            POSCommandData result=null;
            using (IDataReader rdr = SelectPOSCommandDataByCommandLikeData_Reader(options))
            {
                if (rdr.Read())
                    result= new POSCommandData(rdr);
            }
            return result;
        }

        public bool InsertData(POSCommandData commandData)
        {
            int posCommandDataID = 0;
            InsertPOSCommandData(out posCommandDataID, commandData.Type, commandData.Options, commandData.CommandData, commandData.FilePath, commandData.ApplyDate, commandData.DateAdded);
            if (posCommandDataID > 0)
            {
                commandData.POSCommandDataID = posCommandDataID;
                return true;
            }
            return false;
        }

        // function deletes any old data, but only if there is more recent data available
        public bool DeleteObsoleteData(int daysBeforeObsolete, string type)
        {
            // define the deletion date
            DateTime deleteDate = DateTime.Now.AddDays(-daysBeforeObsolete).Date;
            POSCommandDataList dataList = null;
            // check there are current records and a delete will not empty all records
            IDataReader rdr = SelectPOSCommandDataByDate_Reader(deleteDate, type);
            dataList = new POSCommandDataList(rdr);

            // check there are current records, only do a delete if there are more recent records available
            if (dataList == null || dataList.Count <= 0)
                return false;

            // delete any old records
            DeletePOSCommandDataByDate(deleteDate, type);
            // return success after deletion
            return true;
        }

        public POSCommandSettings GetSettingsByType(string type)
        {
            POSCommandSettings result=null;
            using (IDataReader rdr = SelectPOSCommandSettingByType_Reader(type))
            {
                if (rdr.Read())
                result = new POSCommandSettings(rdr);
            }
            return result;
        }

        public POSCommandSettings GetSettingsByID(int posCommandSettingsID)
        {
            POSCommandSettings result=null;
            using (IDataReader rdr = SelectPOSCommandSettings_Reader(posCommandSettingsID))
            {
                if (rdr.Read())
                result = new POSCommandSettings(rdr);
            }
            return result;
        }

        public bool InsertSettings(POSCommandSettings commandSettings)
        {
            // check if setting exists
            POSCommandSettings existingSetting = GetSettingsByID(commandSettings.POSCommandSettingsID);

            // either update or insert depending on result
            int recordsAffected = 0;
            if (existingSetting == null)
                recordsAffected = InsertPOSCommandSettings(commandSettings.POSCommandSettingsID, commandSettings.POSCommandTypeID, commandSettings.Description, commandSettings.CommandXML);
            else
                recordsAffected = UpdatePOSCommandSettings(commandSettings.POSCommandSettingsID, commandSettings.POSCommandTypeID, commandSettings.Description, commandSettings.CommandXML);

            // return true or false depending on total records affected
            return (recordsAffected > 0);
        }

        public POSCommandType GetTypeByType(string type)
        {
            POSCommandType result=null;
            using (IDataReader rdr = SelectPOSCommandTypeByType_Reader(type))
            {
                if (rdr.Read())
                result = new POSCommandType(rdr);
            }
            return result;
        }

        public bool InsertType(POSCommandType commandType)
        {
            // check if type exists
            POSCommandType existingType = GetTypeByType(commandType.Type);

            // either update or insert depending on result
            int recordsAffected = 0;
            if (existingType == null)
                recordsAffected = InsertPOSCommandType(commandType.POSCommandTypeID.Value, commandType.Type, commandType.Description);
            else
                recordsAffected = UpdatePOSCommandType(commandType.POSCommandTypeID.Value, commandType.Type, commandType.Description);

            // return true or false depending on total records affected
            return (recordsAffected > 0);
        }

    }
}
