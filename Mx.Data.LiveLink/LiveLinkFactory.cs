﻿using System;
using System.Data.Common;
//using Microsoft.Practices.Unity;
using Mx.Data.LiveLink.Interfaces;

namespace Mx.Data.LiveLink
{
    public class LiveLinkFactory : ILiveLinkFactory
    {
        public IPOSCommandRepository GetPOSCommandRepository()
        {
            return Mx.Common.IocContainer.Resolve<IPOSCommandRepository>();
            //return new POSCommandRepository();
        }

        public ISessionCommandRepository GetSessionCommandRepository()
        {
            return Mx.Common.IocContainer.Resolve<ISessionCommandRepository>();
            //return new SessionCommandRepository();
        }

    }
}
