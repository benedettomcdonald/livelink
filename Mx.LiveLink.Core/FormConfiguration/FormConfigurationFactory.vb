﻿Option Strict On
Option Explicit On

Imports System.Collections.Generic
Imports Mx.POS.EasyClock
Imports Mx.LiveLink.Interface
Imports Mx.POS.ZKSoftware
Imports Mx.POS.Compris


Public Class FormConfigurationFactory

    Public Shared Function CreateConfiguration() As Dictionary(Of LiveLinkFormType, ILiveLinkFormConfiguration)
        ' initialise the Form Types and their configurations
        Dim formConfiguration As New Dictionary(Of LiveLinkFormType, ILiveLinkFormConfiguration)
        AddConfiguration(LiveLinkFormType.EasyClock, New EasyClockFormConfiguration(), formConfiguration)
        AddConfiguration(LiveLinkFormType.ComprisActiveX, New ComprisActiveXFormConfiguration(), formConfiguration)
        AddConfiguration(LiveLinkFormType.YISController, New YISControllerFormConfiguration(), formConfiguration)
        AddConfiguration(LiveLinkFormType.ZKFingerprintUsers, New ZKFormConfiguration(), formConfiguration)
        Return formConfiguration
    End Function

    Private Shared Sub AddConfiguration(ByVal formType As LiveLinkFormType, ByVal formConfiguration As ILiveLinkFormConfiguration, ByVal formConfigurationList As Dictionary(Of LiveLinkFormType, ILiveLinkFormConfiguration))
        If (formConfiguration IsNot Nothing AndAlso formConfiguration.Enabled) Then
            formConfigurationList.Add(formType, formConfiguration)
        End If
    End Sub

End Class
