﻿Option Strict On
Option Explicit On

Imports System.Threading
Imports Mx.POS.Common
Imports Mx.LiveLink.Interface
Imports Mx.YIS
Imports Mx.YIS.Consumer



Public Class YISControllerFormConfiguration
    Inherits FormConfigurationBase

#Region "Private Variables"

    Private _yisController As YISController
    Private _consumerPOSControl As POSControlResponse
    Private _consumerRDSDisplay As POSControlResponse

#End Region


#Region "Public Properties"

    Public WriteOnly Property ConsumerPOSControl() As POSControlResponse
        Set(ByVal value As POSControlResponse)
            _consumerPOSControl = value
        End Set
    End Property

    Public WriteOnly Property ConsumerRDSDisplay() As POSControlResponse
        Set(ByVal value As POSControlResponse)
            _consumerRDSDisplay = value
        End Set
    End Property

#End Region

#Region "Public Constructor"

    Public Sub New()
        _enabled = LiveLinkConfiguration.YisEnabled
        _autoShow = False
    End Sub

#End Region

#Region "Load Form"

    Public Overrides Sub LoadForm(ByVal entityID As Long, ByVal entity As String, ByVal sessionKey As String, ByVal identity As String)
        Try
            LogEvent(EventLogEntryType.Information, "Loading YIS Controller")
            ' instantiate YIS controller object
            Dim localYISControl As YISController = New YISController(_consumerPOSControl, _consumerRDSDisplay, LiveLinkConfiguration.YisDMCServerPort, _
                                                                     LiveLinkConfiguration.AccessCode, LiveLinkConfiguration.AccessCodeCaseSensitive, _
                                                                     LiveLinkConfiguration.YisDayOpenTimeout, LiveLinkConfiguration.YisDayCloseTimeout, _
                                                                     LiveLinkConfiguration.YisEmployeeUpdateTimeout)

            ' if instantiated successfully, then store global reference to this class
            _yisController = localYISControl
            ' load form controller
            MyBase.StartFormThread(New System.Threading.ThreadStart(AddressOf RunYISController))

        Catch ex As Exception
            LogEvent(EventLogEntryType.Error, "Exception occurred creating YIS controller application. Exception: {0}", ex.ToString)
        End Try

    End Sub

    ' YIS Controller run function (called in new thread)
    Private Sub RunYISController()
        Dim mutexOwner As Boolean = False
        Dim yisMutex As Mutex

        Try
            yisMutex = YISController.GetMutexLock(mutexOwner)
            If (mutexOwner AndAlso yisMutex IsNot Nothing) Then
                Application.Run(_yisController)
                LogEvent(EventLogEntryType.Information, "YIS Controller closed")
            Else
                LogEvent(EventLogEntryType.Warning, "YIS Controller is already open")
            End If
        Catch ex As Exception
            LogEvent(EventLogEntryType.Error, "Exception occurred in YIS Controller applciation. Exception: {0}", ex.ToString)
        Finally
            _yisController.Dispose()
            _yisController = Nothing
            YISController.ReleaseMutexLock(mutexOwner, yisMutex)
        End Try

    End Sub


#End Region


End Class
