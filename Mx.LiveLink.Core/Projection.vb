#Region "Summary"

'-------------------------------------------------------------------------------------------
' AUTHOR        : $Author: Wayne $
' SPECIAL NOTES : 
' FILE NAME     : $Workfile: Projection.vb $
' ARCHIVE       : $Archive: /Source/Macromatix.root/Macromatix/LiveLink/Projection.vb $
' REVISION      : $Revision: 3 $
'
' Copyright (c) 2006 MacromatiX 
' 
' $History: Projection.vb $
'  
'  *****************  Version 3  *****************
'  User: Wayne        Date: 22/10/07   Time: 5:53p
'  Updated in $/Source/Macromatix.root/Macromatix/LiveLink
'  Issue: LiveLink Projection error loggin insufficient
'  Description: Added Additional logging
'  Reviewed by: Richard
'  Type:  Bug Fix
'  Release Notes: Internal
'  Support Ticket No: N/A
'  Status: Complete
'  
'  *****************  Version 2  *****************
'  User: Richard      Date: 24/05/07   Time: 12:27a
'  Updated in $/Source/Macromatix.root/Macromatix/LiveLink
'  Issue / Work Item: LiveLink Projection
'  Description: Added ability to bulk insert rows into the projection
'  tables
'  Release Notes: 
'  Reviewed by: Ricky
'  Status: Complete
'  
'  *****************  Version 1  *****************
'  User: Richard      Date: 22/05/07   Time: 1:55p
'  Created in $/Source/Macromatix.root/Macromatix/LiveLink
'  Issue / Work Item: MacromatiX Projection - webservices plumbing
'  Description: Added new functionality to LiveLink and web service to
'  retrieve updated projection data
'  Release Notes: 
'  Reviewed by: Colin
'  Status: Complete
'  
'-------------------------------------------------------------------------------------------

#End Region

Option Strict On
Option Explicit On

Imports Mx.POS.Common

Public Class Projection

    Public Shared Function UpdateProjections(ByVal ds As DataSet, ByVal projectionDate As DateTime) As Boolean
        Return BulkTableInsert(ds)
    End Function

    Public Shared Function BulkTableInsert(ByVal ds As DataSet) As Boolean
        Const errorLogMsg As String = "Error updating projection." & vbCrLf & vbCrLf & "Query: {0}" & vbCrLf & vbCrLf & "Error: {1}"
        Dim errorMsg As String = String.Empty
        Dim localds As DataSet = Nothing
        Dim qry As String = String.Empty
        Dim errorsFound As Boolean = False

        ' This function assumes that the datatables in the dataset that is passed in, have the exact shape of 
        ' the local LiveLink data tables.
        ' The function loops through each of the data tables, deletes all rows from each of them, and then bulk inserts the new data

        For Each table As DataTable In ds.Tables
            Try
                qry = String.Format("TRUNCATE TABLE {0}", table.TableName)
                If Not MMSGeneric_Execute(qry, errorMsg) Then
                    errorsFound = True
                    LogEvent(String.Format(errorLogMsg, qry, errorMsg), EventLogEntryType.Error)
                End If
                qry = String.Format("SELECT * FROM {0}", table.TableName)
                If Not MMSGeneric_ListAll(qry, table.TableName, localds, errorMsg) Then
                    errorsFound = True
                    LogEvent(String.Format(errorLogMsg, qry, errorMsg), EventLogEntryType.Error)
                End If
                localds.Merge(ds)
                If Not MMSGeneric_Update(qry, table.TableName, localds, errorMsg) Then
                    errorsFound = True
                    LogEvent(String.Format(errorLogMsg, qry & " (UPDATE)", errorMsg), EventLogEntryType.Error)
                End If
            Catch ex As Exception
                errorsFound = True
                LogEvent(String.Format(errorLogMsg, qry, ex.ToString), EventLogEntryType.Error)
            End Try
        Next
        Return Not errorsFound
    End Function

End Class
