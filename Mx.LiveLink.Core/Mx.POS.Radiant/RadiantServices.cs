﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mx.POS.Radiant
{
    public class RadiantServices
    {
        // for events that have no transactionid, generate a unique transactionid from the time.
        public static long GetTransactionIDFromTime(DateTime time)
        {
            return (time.Ticks - new DateTime(2011, 1, 1).Ticks);
        }
    }
}
