﻿Option Strict On
Option Explicit On

Imports Mx.LiveLink.Interface
Imports Mx.POS.Common


Public Class NewPOSConfiguration
    Inherits ImportConfigurationBase

#Region "Custom Private Variables"

    Private _import As Mx.NewPOS.NewPOS

#End Region

#Region "Custom Public Properties"

    Public Property Import() As Mx.NewPOS.NewPOS
        Get
            Return _import
        End Get
        Set(ByVal value As Mx.NewPOS.NewPOS)
            _import = value
        End Set
    End Property

#End Region

#Region "Constructors & Interface Functions"

    Public Sub New()
        MyBase.New()
        _importType = LiveLinkImportType.NewPOS
        _enabled = LiveLinkConfiguration.NewPOSEnabled
        _gatewayEnabled = LiveLinkConfiguration.NewPOSGatewayEnabled
        _pollInterval = LiveLinkConfiguration.NewPOSPollInterval
    End Sub

#End Region

End Class


