﻿Option Strict On
Option Explicit On

Imports System.Collections.Generic
Imports Mx.LiveLink.Interface
Imports Mx.POS.Common
Imports Mx.YIS


Public Class YisConfiguration
    Inherits ImportConfigurationBase

#Region "Custom Private Variables"

    ' YIS interface variables
    Private _consumerPOSControl As Mx.YIS.Consumer.POSControlResponse
    Private _consumerRDSDisplay As Mx.YIS.Consumer.POSControlResponse
    Private _consumerHMETimer As Mx.YIS.Consumer.SOSTimer
    Private _consumerQSRTimer As Mx.YIS.Consumer.SOSTimer
    Private _consumersEnabled As Boolean

#End Region

#Region "Custom Public Properties"

    Public ReadOnly Property ConsumerPOSControl() As Mx.YIS.Consumer.POSControlResponse
        Get
            Return _consumerPOSControl
        End Get
    End Property
    Public ReadOnly Property ConsumerRDSDisplay() As Mx.YIS.Consumer.POSControlResponse
        Get
            Return _consumerRDSDisplay
        End Get
    End Property
    Public ReadOnly Property ConsumerHMETimer() As Mx.YIS.Consumer.SOSTimer
        Get
            Return _consumerHMETimer
        End Get
    End Property
    Public ReadOnly Property ConsumerQSRTimer() As Mx.YIS.Consumer.SOSTimer
        Get
            Return _consumerQSRTimer
        End Get
    End Property

    Public ReadOnly Property ConsumersEnabled() As Boolean
        Get
            Return _consumersEnabled
        End Get
    End Property

#End Region

#Region "Constructors & Interface Functions"

    Public Sub New()
        MyBase.New()
        _importType = LiveLinkImportType.YIS
        _enabled = LiveLinkConfiguration.YisEnabled
        _gatewayEnabled = False
    End Sub


    Public Overrides Function Start(ByVal entityID As Integer, ByVal sessionKey As String, ByVal identity As String) As Boolean
        ' check YIS is enabled
        If (Not LiveLinkConfiguration.YisEnabled) Then
            Return True
        End If

        Dim status As Boolean = True
        _consumersEnabled = False

        ' POS Control consumer
        Try
            ' instantiate the new POSControl consumer
            If (_consumerPOSControl Is Nothing) Then
                _consumerPOSControl = New Mx.YIS.Consumer.POSControlResponse(LiveLinkConfiguration.YisDMCServerPort, LiveLinkConfiguration.YisConsumerPOSControlPort, Mx.YIS.Consumer.DMC.YisConsumerMessageClass.POSControlResponseMessage)
            End If

            ' start the POS Control consumer
            _consumerPOSControl.Start()
        Catch ex As Exception
            LogEvent(EventLogEntryType.Error, "Exception occurred starting the POS Control YIS consumer. Exception: {0}", ex.ToString)
            status = False
        End Try
        ' RDS display consumer
        Try
            ' instantiate the new RDSDisplay consumer
            If (_consumerRDSDisplay Is Nothing) Then
                _consumerRDSDisplay = New Mx.YIS.Consumer.POSControlResponse(LiveLinkConfiguration.YisDMCServerPort, LiveLinkConfiguration.YisConsumerRDSDisplayPort, Mx.YIS.Consumer.DMC.YisConsumerMessageClass.RDSDisplayResponseMessage)
            End If

            ' start the RDS Display consumer
            _consumerRDSDisplay.Start()
        Catch ex As Exception
            LogEvent(EventLogEntryType.Error, "Exception occurred starting the RDS Display YIS consumer. Exception: {0}", ex.ToString)
            status = False
        End Try
        ' HME Timer consumer
        Try
            ' instantiate the new HMETimer consumer
            If (_consumerHMETimer Is Nothing) Then
                _consumerHMETimer = New Mx.YIS.Consumer.SOSTimer(LiveLinkConfiguration.YisDMCServerPort, LiveLinkConfiguration.YisConsumerHMETimerPort, Mx.YIS.Consumer.DMC.YisConsumerMessageClass.HMESOS)
            End If

            ' start the HME Timer consumer
            _consumerHMETimer.Start()
        Catch ex As Exception
            LogEvent(EventLogEntryType.Error, "Exception occurred starting the HME Timer YIS consumer. Exception: {0}", ex.ToString)
            'status = False
        End Try
        ' QSR Timer consumer
        Try
            ' instantiate the new QSRTimer consumer
            If (_consumerQSRTimer Is Nothing) Then
                _consumerQSRTimer = New Mx.YIS.Consumer.SOSTimer(LiveLinkConfiguration.YisDMCServerPort, LiveLinkConfiguration.YisConsumerQSRTimerPort, Mx.YIS.Consumer.DMC.YisConsumerMessageClass.QSRSOS)
            End If

            ' start the QSR Timer consumer
            _consumerQSRTimer.Start()
        Catch ex As Exception
            LogEvent(EventLogEntryType.Error, "Exception occurred starting the QSR Timer YIS consumer. Exception: {0}", ex.ToString)
            'status = False
        End Try

        ' set the buttons to load the offline form to enabled
        If (status) Then
            _consumersEnabled = True
        End If
        ' return result
        Return status
    End Function

    Public Overrides Function [Stop](ByVal entityID As Integer) As Boolean
        If (Not LiveLinkConfiguration.YisEnabled) Then
            _consumersEnabled = False
            Return True
        End If

        ' stop the POS Control YIS consumer
        Try
            If (_consumerPOSControl IsNot Nothing) Then
                _consumerPOSControl.Stop()
            End If
        Catch ex As Exception
            LogEvent(EventLogEntryType.Error, "Exception occurred stopping POS Control YIS consumer. Exception: {0}", ex.ToString)
        End Try
        ' stop the RDS Display YIS consumer
        Try
            If (_consumerRDSDisplay IsNot Nothing) Then
                _consumerRDSDisplay.Stop()
            End If
        Catch ex As Exception
            LogEvent(EventLogEntryType.Error, "Exception occurred stopping RDS Display YIS consumer. Exception: {0}", ex.ToString)
        End Try
        ' stop the HME Timer YIS consumer
        Try
            If (_consumerHMETimer IsNot Nothing) Then
                _consumerHMETimer.Stop()
            End If
        Catch ex As Exception
            LogEvent(EventLogEntryType.Error, "Exception occurred stopping HME Timer YIS consumer. Exception: {0}", ex.ToString)
        End Try
        ' stop the QSR Timer YIS consumer
        Try
            If (_consumerQSRTimer IsNot Nothing) Then
                _consumerQSRTimer.Stop()
            End If
        Catch ex As Exception
            LogEvent(EventLogEntryType.Error, "Exception occurred stopping QSR Timer YIS consumer. Exception: {0}", ex.ToString)
        End Try

        _consumersEnabled = False
    End Function

    Public Overrides Function KeepAlive(ByVal entityID As Integer) As Boolean
        If (Not LiveLinkConfiguration.YisEnabled) Then
            Return True
        End If
        ' ensure the POS Control YIS consumer is kept alive
        Try
            If (_consumerPOSControl IsNot Nothing) Then
                _consumerPOSControl.KeepAlive()
            End If
        Catch ex As Exception
            LogEvent(EventLogEntryType.Error, "Exception occurred keeping alive the POS Control YIS consumer. Exception: {0}", ex.ToString)
        End Try
        ' ensure the RDS Display YIS consumer is kept alive
        Try
            If (_consumerRDSDisplay IsNot Nothing) Then
                _consumerRDSDisplay.KeepAlive()
            End If
        Catch ex As Exception
            LogEvent(EventLogEntryType.Error, "Exception occurred keeping alive the RDS Display YIS consumer. Exception: {0}", ex.ToString)
        End Try
        ' ensure the HME Timer YIS consumer is kept alive
        Try
            If (_consumerHMETimer IsNot Nothing) Then
                _consumerHMETimer.KeepAlive()
            End If
        Catch ex As Exception
            LogEvent(EventLogEntryType.Error, "Exception occurred keeping alive the HME Timer YIS consumer. Exception: {0}", ex.ToString)
        End Try
        ' ensure the QSR Timer YIS consumer is kept alive
        Try
            If (_consumerQSRTimer IsNot Nothing) Then
                _consumerQSRTimer.KeepAlive()
            End If
        Catch ex As Exception
            LogEvent(EventLogEntryType.Error, "Exception occurred keeping alive the QST Timer YIS consumer. Exception: {0}", ex.ToString)
        End Try
    End Function

#End Region

End Class


