﻿Option Strict On
Option Explicit On

Imports System.Collections.Generic
Imports Mx.LiveLink.Interface
Imports Mx.POS.Common


Public Class OfflineCashupInfoConfiguration
    Inherits ImportConfigurationBase

#Region "Custom Private Variables"


#End Region

#Region "Custom Public Properties"


#End Region

#Region "Constructors & Interface Functions"

    Public Sub New()
        MyBase.New()
        _importType = LiveLinkImportType.OfflineCashupInfo
        _enabled = LiveLinkConfiguration.UseOfflineCashup
        _gatewayEnabled = LiveLinkConfiguration.UseOfflineCashup
        _pollInterval = 0 ' set poll interval to zero, so that every interval will result in a offline cashup poll
    End Sub

#End Region

End Class


