﻿Option Strict On
Option Explicit On

Imports System.Collections.Generic
Imports Mx.POS.HomeService
Imports Mx.POS.MicrosoftRMS
Imports Mx.POS.Micros
Imports Mx.POS.iPOS
Imports Mx.POS.InTouch
Imports Mx.POS.ICG
Imports Mx.POS.Compris
Imports Mx.POS.Aloha
Imports Mx.LiveLink.Interface
Imports Mx.POS.Octane
Imports Mx.POS.Panasonic
Imports Mx.POS.PAR
Imports Mx.POS.POSI
Imports Mx.POS.SpeedOfService
Imports Mx.POS.Uniwell
Imports Mx.POS.ZKSoftware
Imports Mx.POS.POSixml
Imports Mx.POS.Xpient
Imports Mx.POS.RPOS
Imports Mx.POS.Transight
Imports Mx.POS.NewPOSTLD
Imports Mx.POS.TimePunchImport
Imports Mx.POS.NewPOSSummary
Imports Mx.POS.SUS
Imports Mx.POS.STM
Imports Mx.TrafficData.AlertSystems

Public Class ImportConfigurationFactory

    Public Shared Function CreateConfiguration() As Dictionary(Of LiveLinkImportType, ILiveLinkImportConfiguration)
        ' initialise the Import Types and there configurations
        Dim importConfiguration As New Dictionary(Of LiveLinkImportType, ILiveLinkImportConfiguration)
        AddConfiguration(LiveLinkImportType.Aloha, New AlohaConfiguration(), importConfiguration)
        AddConfiguration(LiveLinkImportType.Compris, New ComprisConfiguration(), importConfiguration)
        AddConfiguration(LiveLinkImportType.ComprisPDP, New ComprisPDPConfiguration(), importConfiguration)
        AddConfiguration(LiveLinkImportType.ComprisPDPUpdates, New ComprisPDPUpdatesConfiguration(), importConfiguration)
        AddConfiguration(LiveLinkImportType.ComprisPollFiles, New ComprisPollFilesConfiguration(), importConfiguration)
        AddConfiguration(LiveLinkImportType.ICG, New ICGConfiguration(), importConfiguration)
        AddConfiguration(LiveLinkImportType.Intouch, New IntouchConfiguration(), importConfiguration)
        AddConfiguration(LiveLinkImportType.iPOS, New iPOSConfiguration(), importConfiguration)
        AddConfiguration(LiveLinkImportType.Micros, New MicrosConfiguration(), importConfiguration)
        AddConfiguration(LiveLinkImportType.MicrosoftRMS, New RMSConfiguration(), importConfiguration)
        AddConfiguration(LiveLinkImportType.NewPOS, New NewPOSConfiguration(), importConfiguration)
        AddConfiguration(LiveLinkImportType.Octane, New OctaneConfiguration(), importConfiguration)
        AddConfiguration(LiveLinkImportType.Panasonic, New PanasonicConfiguration(), importConfiguration)
        AddConfiguration(LiveLinkImportType.PARExalt4, New PARExaltConfiguration(), importConfiguration)
        AddConfiguration(LiveLinkImportType.POSI, New POSIConfiguration(), importConfiguration)
        AddConfiguration(LiveLinkImportType.Radiant, New RadiantConfiguration(), importConfiguration)
        AddConfiguration(LiveLinkImportType.ServiceReport, New ServiceReportConfiguration(), importConfiguration)
        AddConfiguration(LiveLinkImportType.Uniwell, New UniwellConfiguration(), importConfiguration)
        AddConfiguration(LiveLinkImportType.OfflineCashupInfo, New OfflineCashupInfoConfiguration(), importConfiguration)
        AddConfiguration(LiveLinkImportType.OfflineCashupSend, New OfflineCashupSendConfiguration(), importConfiguration)
        AddConfiguration(LiveLinkImportType.OfflineCashupSettings, New OfflineCashupSettingsConfiguration(), importConfiguration)
        AddConfiguration(LiveLinkImportType.YIS, New YisConfiguration(), importConfiguration)
        AddConfiguration(LiveLinkImportType.ZKFingerprint, New ZKFingerprintConfiguration(), importConfiguration)
        AddConfiguration(LiveLinkImportType.POSixml, New POSixmlImportConfiguration(), importConfiguration)
        AddConfiguration(LiveLinkImportType.Xpient, New XpientImportConfiguration(), importConfiguration)
        AddConfiguration(LiveLinkImportType.RPOS, New RPOSImportConfiguration(), importConfiguration)
        AddConfiguration(LiveLinkImportType.IntouchMenuPackage, New IntouchMenuPackageConfiguration(), importConfiguration)
        AddConfiguration(LiveLinkImportType.Transight, New TransightImportConfiguration(), importConfiguration)
        AddConfiguration(LiveLinkImportType.NewPOSTLD, New NewPOSTLDImportConfiguration(), importConfiguration)
        AddConfiguration(LiveLinkImportType.TimePunchImport, New TimePunchImportConfiguration(), importConfiguration)
        AddConfiguration(LiveLinkImportType.NewPOSSummary, New NewPOSSummaryImportConfiguration(), importConfiguration)
        AddConfiguration(LiveLinkImportType.SUS, New SUSImportConfiguration(), importConfiguration)
        AddConfiguration(LiveLinkImportType.HomeService, New HomeServiceImportConfiguration(), importConfiguration)
        AddConfiguration(LiveLinkImportType.STM, New STMImportConfiguration(), importConfiguration)
        AddConfiguration(LiveLinkImportType.AlertSystemsTrafficData, New AlertSystemsTrafficDataConfiguration(), importConfiguration)
        Return importConfiguration
    End Function

    Private Shared Sub AddConfiguration(ByVal importType As LiveLinkImportType, ByVal importConfiguration As ILiveLinkImportConfiguration, ByVal importConfigurationList As Dictionary(Of LiveLinkImportType, ILiveLinkImportConfiguration))
        If (importConfiguration IsNot Nothing AndAlso importConfiguration.Enabled) Then
            importConfigurationList.Add(importType, importConfiguration)
        End If
    End Sub

End Class



