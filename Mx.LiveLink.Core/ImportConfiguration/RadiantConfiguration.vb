﻿Option Strict On
Option Explicit On

Imports System.Collections.Generic
Imports Mx.LiveLink.Interface
Imports Mx.POS.Common


Public Class RadiantConfiguration
    Inherits ImportConfigurationBase

#Region "Custom Private Variables"

    Private _import As Mx.POS.Radiant.RadiantPOS
    Private _punchImport As Mx.POS.Radiant.RadiantPunch

#End Region

#Region "Custom Public Properties"

    Public Property Import() As Mx.POS.Radiant.RadiantPOS
        Get
            Return _import
        End Get
        Set(ByVal value As Mx.POS.Radiant.RadiantPOS)
            _import = value
        End Set
    End Property

    Public Property PunchImport() As Mx.POS.Radiant.RadiantPunch
        Get
            Return _punchImport
        End Get
        Set(ByVal value As Mx.POS.Radiant.RadiantPunch)
            _punchImport = value
        End Set
    End Property

#End Region

#Region "Constructors & Interface Functions"

    Public Sub New()
        MyBase.New()
        _importType = LiveLinkImportType.Radiant
        _enabled = LiveLinkConfiguration.RadiantEnabled
        _gatewayEnabled = LiveLinkConfiguration.RadiantGatewayEnabled
        _pollInterval = LiveLinkConfiguration.RadiantPollInterval
    End Sub

#End Region

End Class


