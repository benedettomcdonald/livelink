﻿Public Class LiveLinkCoreSync

#Region "Fields"

    'Whether or not the lock is currently set
    Private _locked As Boolean

#End Region

#Region "Properties"

    Public ReadOnly Property Locked As Boolean
        Get
            Return _locked
        End Get
    End Property

#End Region

#Region "Constructors"

    Public Sub New()
        'Start out unlocked
        _locked = False
    End Sub

#End Region

#Region "Methods"

    ''' <summary>
    ''' Method called to perform a lock
    ''' </summary>
    ''' <remarks>Throws an exception if already locked</remarks>
    Public Sub Lock()
        SyncLock Me
            If _locked Then
                Throw New Exception("Unable to obtain lock")
            Else
                _locked = True
            End If
        End SyncLock
    End Sub

    ''' <summary>
    ''' Method called to attempt a lock
    ''' </summary>
    ''' <returns>True if locked; False if already locked</returns>
    ''' <remarks></remarks>
    Public Function TryLock() As Boolean
        SyncLock Me
            If _locked Then
                Return False
            Else
                _locked = True
                Return True
            End If
        End SyncLock
    End Function

    ''' <summary>
    ''' Method called to perform an unlock
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub Unlock()
        SyncLock Me
            If _locked Then
                _locked = False
            End If
        End SyncLock
    End Sub

#End Region

End Class
