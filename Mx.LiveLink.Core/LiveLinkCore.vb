﻿Imports System.IO
Imports System.Net
Imports System.Math
Imports System.Text
Imports System.Threading
Imports System.Xml
Imports System.Xml.Serialization
Imports Mx.BusinessObjects.LiveLink
Imports Mx.POS.Common.Composite
Imports Mx.POS.Common.BusinessObjects
Imports Mx.LiveLink.Data
Imports Mx.POS.EasyClock
Imports Mx.POS.MicrosoftRMS
Imports Mx.POS.Micros
Imports Mx.POS.iPOS
Imports Mx.POS.ICG
Imports Mx.POS.Compris
Imports Mx.POS.InTouch
Imports Mx.POS.Aloha
Imports Mx.LiveLink.Interface
Imports Microsoft.Win32
Imports System.Collections.Generic
Imports Mx.Configuration
Imports MMS.Utilities
Imports MMS.Utilities.MMSUtils.ManageFiles
Imports Mx.Services
Imports Mx.Services.BaseSystem.Polling
Imports Mx.BusinessObjects
Imports Mx.POS.Radiant
Imports Mx.YIS
Imports Mx.POS.Common
Imports Mx.POS.Octane
Imports Mx.POS.Panasonic
Imports Mx.POS.PAR
Imports Mx.POS.POSI
Imports Mx.POS.SpeedOfService
Imports Mx.POS.Uniwell
Imports Mx.POS.ZKSoftware
Imports Mx.BusinessObjects.YIS
Imports Mx.POS.POSixml
Imports Mx.POS.Xpient
Imports Mx.POS.RPOS
Imports Mx.POS.TimePunchImport
Imports Mx.POS.SUS
Imports Mx.TrafficData.AlertSystems


Public Class LiveLinkCore
    Implements ILiveLinkCore

#Region "Private Global Variables"

    Private Enum DataType
        SalesData
        AttendanceData
        SpeedOfServiceData
    End Enum

    Private Enum PollReasonType
        OneMinuteTimer
        NewDataAvailable
    End Enum

    ' enum for command line options (set the ClassName value to the expected switch. For example: /s /h /? )
    ' also add the handling of this value in the function "ParseCommandLine()"
    Private Enum CommandLineArguments
        <EnumClassName("/s")> StoreNumber
    End Enum

    Private Const _commandLineUsage As String = "LiveLink.exe [/s:StoreNumber]"
    Private _argumentStoreNumber As String = String.Empty

    Const MaxCommsBackoffMinutes As Integer = 60 * 4 ' 4 hours
    Const LogDataFormat As String = "Information"
    Const GrindProcessTimeout As Integer = 120 ' (seconds) Assume that if the Grind process (Aloha) is still running after two minutes it has a Windows MsgBox on the screen and is locked
    Const salesRecordTypes = "'H', 'I', 'F', 'C', 'X', 'S', 'V', 'M', 'IM', 'SI', 'TLD', 'FUEL', 'VALIDATION', 'P', 'IV', 'STOREDVALUE'" ' Header, Item, Financial, Control, Miscellaneous, Speed of Service, Inventory, Customer Information, Item Modifier, Store Information, Compressed Transaction Level Data (TLD)

    ' LiveLink restart/shutdown behaviour variables
    Private LiveLinkShutDownFile As String = Path.Combine(Application.StartupPath, "Shutdown.txt")
    Private LiveLinkRestartFrequency As Integer = 0
    Private LiveLinkStartTime As DateTime = DateTime.Now
    Private lastKeepAlive As DateTime = DateTime.Now

    Public CurrentCount As Long = 0
    Public AwakeCount As Long = 0
    Public CommsBackoffMinutes As Integer = 1 ' Start with a backoff time of 1 minute if there is a comms error
    Public CommsBackoffCount As Integer = 0 ' Counter used in backoff timer

    Private _noMSA_Flag As Boolean = False
    Private _okToSend As Boolean = True ' LiveLink will only attempt communications if this flag is true
    Private _conditionalSessionKey As String = String.Empty ' Session key to use when site is not activated (allows for limited comms)
    Private _flowTxInBatch As Integer ' Number of transaction to batch in a single web service call
    Private _flowIterations As Integer ' Number of loops to perform when sending transactions
    Private _flowMaxRecords As Integer ' Maximum number of records (line items) in a single call
    Private _flowInterval As Integer ' Number of minutes to wait before sending the next batch of data
    Private _awakeInterval As Integer ' Number of minutes between sending of heartbeat messages 
    Private _awakeCounterVerbose As Integer = 8 ' Number of heartbeat messages between verbose awake messages
    Private _leaseExpired As Boolean = True ' Has session lease expired - if so obtain new lease
    Private _startupSent As Boolean = False
    Private _awakeCounter As Integer = 0 ' Used to count each Awake Message sent
    Private _messageWaiting As Boolean = False
    Private _processing As LiveLinkCoreSync = New LiveLinkCoreSync()
    Private _pingEnabled As Boolean = False ' If this is true, LiveLink will poll the web server every 30 seconds to see if it is there
    Private _databaseSizeExceeded As Boolean = False
    Private _databaseSizeExceededTimeout As DateTime
    Private _databaseTotalRecords As Integer = 0
    Private _databaseTotalUnsyncedRecords As Integer = 0

    ' Pre-Send Processing variables
    Private _preSendProcess As String

    ' Pre-Send Process - PLUMAP - PLU Mapping variables
    Private Const _pluMapFile As String = "PLUMap.XML"
    Private _pluMapXmlDoc As XmlDocument

    ' Logo branding variables
    Private _logoImage As String
    Private _logoColor As String

    ' Live Sync variables
    Private _liveSyncLastPoll As DateTime

    ' Offline cashup settings
    Private _offlineCashupSetupSettingsRetrieved As Boolean = True



#End Region


#Region "Private Class Variables"

    ' constant definitions
    Private Const _timerPollInterval As Integer = 60000

    Private _sessionKey As String = String.Empty
    Private _liveLinkID As String
    Private _entityID As Long
    Private _entityName As String
    Private _isRunning As Boolean = False
    Private _initialised As Boolean = False

    Private _importConfigurationList As Dictionary(Of LiveLinkImportType, ILiveLinkImportConfiguration)
    Private _formConfigurationList As Dictionary(Of LiveLinkFormType, ILiveLinkFormConfiguration)
    Private _formAutoShowComplete As Boolean = False

    Private WithEvents _timerDelayedInitialise As New System.Timers.Timer(60000)

    Private WithEvents _timerPoll As New System.Timers.Timer(_timerPollInterval)
    Private WithEvents _timerCommsBackoff As New System.Timers.Timer(60000)
    Private WithEvents _timerSessionLease As New System.Timers.Timer(60000)
    Private WithEvents _timerPing As New System.Timers.Timer(1000)

    Private _appConfigFile As String
    Private SetStatus As StatusController
    Private LogStatus As LogController

    Private _posCommon As ICompositePOS

    Private _myRadiantListener As RadiantListener

#End Region


#Region "Private Properties"

    Private Property OK_To_Send() As Boolean
        Get
            Return _okToSend
        End Get
        Set(ByVal value As Boolean)
            _okToSend = value
        End Set
    End Property

    Private Property PingEnabled() As Boolean Implements ILiveLinkCore.PingEnabled
        Get
            Return _pingEnabled
        End Get
        Set(ByVal value As Boolean)
            ' Only set to true if using offline cashmanager and a ping interval has been specified
            _pingEnabled = LiveLinkConfiguration.UseOfflineCashup AndAlso LiveLinkConfiguration.PingInterval_KeyExists AndAlso value
            StartStopPingTimer(_pingEnabled AndAlso IsRunning)
        End Set
    End Property

    Private Sub StartStopPingTimer(ByVal start As Boolean)
        If (start AndAlso PingEnabled AndAlso Not _timerPing.Enabled) Then
            ' Start the ping timer.  Use a minimum value of 15 seconds
            _timerPing.Interval = Max(15000, LiveLinkConfiguration.PingInterval * 1000)
            _timerPing.Enabled = True
        ElseIf Not start Then
            _timerPing.Enabled = False
        End If
    End Sub

#End Region


#Region "Public Interface Events"

    Public Event ImportBeginEvent(ByVal sender As Object, ByVal importArgs As ImportEventArgs) Implements ILiveLinkCore.ImportBeginEvent
    Public Event ImportMessageEvent(ByVal sender As Object, ByVal importArgs As ImportEventArgs) Implements ILiveLinkCore.ImportMessageEvent
    Public Event ImportCompleteEvent(ByVal sender As Object, ByVal importArgs As ImportEventArgs) Implements ILiveLinkCore.ImportCompleteEvent
    Public Event SendBeginEvent(ByVal sender As Object, ByVal args As EventArgs) Implements ILiveLinkCore.SendBeginEvent
    Public Event SendCompleteEvent(ByVal sender As Object, ByVal args As EventArgs) Implements ILiveLinkCore.SendCompleteEvent

    Public Event PingStatusEvent(ByVal sender As Object, ByVal pingArgs As PingEventArgs) Implements ILiveLinkCore.PingStatusEvent
    Public Event RestartEvent(ByVal sender As Object, ByVal args As System.EventArgs) Implements ILiveLinkCore.RestartEvent
    Public Event ShutdownEvent(ByVal sender As Object, ByVal args As System.EventArgs) Implements ILiveLinkCore.ShutdownEvent
    Public Event VersionUpdateEvent(ByVal sender As Object, ByVal updateArgs As VersionUpdateEventArgs) Implements ILiveLinkCore.VersionUpdateEvent
    Public Event ProcessDateChangedEvent(ByVal sender As Object, ByVal args As ProcessDateEventArgs) Implements ILiveLinkCore.ProcessDateChangedEvent
    Public Event AccessEvent(ByVal sender As Object, ByVal args As AccessEventArgs) Implements ILiveLinkCore.AccessEvent
    Public Event FlowChangedEvent(ByVal sender As Object, ByVal args As System.EventArgs) Implements ILiveLinkCore.FlowChangedEvent

#End Region


#Region "Public Properties"

    Public ReadOnly Property Initialised() As Boolean Implements ILiveLinkCore.Initialised
        Get
            Return _initialised
        End Get
    End Property

    Public ReadOnly Property ImportConfigurationList() As Dictionary(Of LiveLinkImportType, ILiveLinkImportConfiguration) Implements ILiveLinkCore.ImportConfigurationList
        Get
            Return _importConfigurationList
        End Get
    End Property

    Public ReadOnly Property ImportEnabled(ByVal importType As LiveLinkImportType) As Boolean Implements ILiveLinkCore.ImportEnabled
        Get
            If (_importConfigurationList.ContainsKey(importType)) Then
                Return _importConfigurationList(importType).Enabled
            End If
            Return False
        End Get
    End Property

    Public Sub ToggleImportEnabled(ByVal importType As LiveLinkImportType, ByVal enabled As Boolean) Implements ILiveLinkCore.ToggleImportEnabled
        If (_importConfigurationList.ContainsKey(importType)) Then
            _importConfigurationList(importType).GatewayEnabled = enabled
        End If
    End Sub

    Public ReadOnly Property FormConfigurationList() As System.Collections.Generic.Dictionary(Of LiveLinkFormType, ILiveLinkFormConfiguration) Implements ILiveLinkCore.FormConfigurationList
        Get
            Return _formConfigurationList
        End Get
    End Property

    Public ReadOnly Property FormEnabled(ByVal formType As LiveLinkFormType) As Boolean Implements ILiveLinkCore.FormEnabled
        Get
            If (_formConfigurationList.ContainsKey(formType)) Then
                Return _formConfigurationList(formType).Enabled
            End If
            Return False
        End Get
    End Property

    Public ReadOnly Property IsRunning() As Boolean
        Get
            Return _isRunning
        End Get
    End Property

    Public Property FlowInterval() As Integer Implements ILiveLinkCore.FlowInterval
        Get
            Return _flowInterval
        End Get
        Set(ByVal value As Integer)
            _flowInterval = value
        End Set
    End Property

    Public Property AwakeInterval() As Integer Implements ILiveLinkCore.AwakeInterval
        Get
            Return _awakeInterval
        End Get
        Set(ByVal value As Integer)
            _awakeInterval = value
        End Set
    End Property

    Public ReadOnly Property Proxy() As IWebProxy Implements ILiveLinkCore.Proxy
        Get
            ' Checks the config file for proxy settings and configures the web service appropriately
            Try
                Dim myWebServiceProxy As String = LiveLinkConfiguration.WebService_Proxy
                Dim myWebServiceProxyAuth As String = LiveLinkConfiguration.WebService_Proxy_Auth
                Dim myWebServiceProxyUser As String = LiveLinkConfiguration.WebService_Proxy_User
                Dim myWebServiceProxyPassword As String = DecryptString(LiveLinkConfiguration.WebService_Proxy_Password, LiveLinkConfiguration.ClientCode, LiveLinkConfiguration.SecurityCode)
                Dim myWebServiceProxyDomain As String = LiveLinkConfiguration.WebService_Proxy_Domain
                Dim myWebServiceDestinationUrl As String = LiveLinkConfiguration.WebService_URL

                ' return new proxy
                Return HttpDownload.Proxy(myWebServiceProxy, myWebServiceProxyAuth, myWebServiceProxyUser, myWebServiceProxyPassword, myWebServiceProxyDomain, myWebServiceDestinationUrl)
            Catch ex As Exception
                LogEvent("Proxy error" & vbCrLf & "Exception: " & ex.Message, EventLogEntryType.Warning)
                Return Nothing
            End Try
        End Get
    End Property

    Public ReadOnly Property LiveLinkID() As String Implements ILiveLinkCore.LiveLinkID
        Get
            SetLiveLinkID()
            Return _liveLinkID
        End Get
    End Property

    Public ReadOnly Property SessionKey() As String Implements ILiveLinkCore.SessionKey
        Get
            Return _sessionKey
        End Get
    End Property

    Public ReadOnly Property StoreList() As List(Of EntityListItem) Implements ILiveLinkCore.StoreList
        Get
            ' get the store list from the server
            Return GetStoreList()
        End Get
    End Property

    Public Property EntityID() As Long Implements ILiveLinkCore.EntityID
        Get
            If (_entityID <= 0) Then
                _entityID = GetEntityId()
            End If
            Return _entityID
        End Get
        Set(ByVal value As Long)
            SaveEntityId(value)
            RefreshEntityID()
        End Set
    End Property

    Public ReadOnly Property EntityName() As String Implements ILiveLinkCore.EntityName
        Get
            If (String.IsNullOrEmpty(_entityName)) Then
                _entityName = GetEntityName(EntityID)
            End If
            Return _entityName
        End Get
    End Property

    Public Property ProcessDate() As DateTime Implements ILiveLinkCore.ProcessDate
        Get
            ' return date from tbStoreInfo
            GetLastProcessDate()
        End Get
        Set(ByVal value As DateTime)
            ' update date in tbStoreInfo
            SetLastProcessDate(value)
            RaiseEvent ProcessDateChangedEvent(Me, New ProcessDateEventArgs(value))
        End Set
    End Property

    ' set the LiveLinkID value if it hasn't been set already
    Private Sub SetLiveLinkID()
        If (String.IsNullOrEmpty(_liveLinkID)) Then
            _liveLinkID = GetUniqueMachineId()
        End If
    End Sub

    ' clear the LiveLinkID and reget the value
    Private Sub ResetLiveLinkID()
        _liveLinkID = String.Empty
        SetLiveLinkID()
    End Sub

#Region "Set/Get Last Date tbStoreInfo"

    Private Function GetLastProcessDate() As DateTime
        Dim sql As New StringBuilder(256)
        Dim data As DataSet = Nothing
        Dim sqlError As String = String.Empty

        sql.AppendLine("SELECT LastDate FROM tbStoreInfo ")
        sql.AppendFormat("WHERE StoreID={0} ", _entityID)

        If Not MMSGeneric_ListAll(sql.ToString, "tbStoreInfo", data, sqlError) Then
            LogEvent(EventLogEntryType.Warning, "iPOS failed to get the last query date from tbStoreInfo. SQL Error: {0}", sqlError)
            Return DateTime.MinValue
        End If

        ' check result is valid
        If (data Is Nothing OrElse data.Tables("tbStoreInfo").Rows.Count <= 0) Then
            LogEvent(EventLogEntryType.Warning, "iPOS SQL query to get last query date returned nothing")
            Return DateTime.MinValue
        End If

        ' return the last query date
        Return (DateTimeValue(data.Tables("tbStoreInfo").Rows(0)("LastDate"), DateTime.MinValue))
    End Function

    Public Sub SetLastProcessDate(ByVal queryDate As DateTime)
        Dim sql As New StringBuilder(256)
        Dim sqlError As String = String.Empty

        sql.AppendLine("UPDATE tbStoreInfo ")
        sql.AppendFormat("SET LastDate='{0}' ", queryDate.ToString(DateFormatNames.DatabaseNeutralDateFormat))
        sql.AppendFormat("WHERE StoreID={0} ", _entityID)

        If Not MMSGeneric_Execute(sql.ToString, sqlError) Then
            LogEvent(EventLogEntryType.Warning, "iPOS failed to set the last query date in tbStoreInfo. SQL Error: {0}", sqlError)
        End If
    End Sub

#End Region


#End Region


#Region "Configuration Setup"

    Private Shared Sub LogConfigMigration(ByVal sender As Object, ByVal e As Mx.Configuration.MigrationStepEventArgs)
        MMS.Utilities.MxClientLogger.LogInfo(e.Message)
    End Sub

    Private Sub UpdateAppsettings()

        ' Check that the ConnectionString key exists.
        If Not LiveLinkConfiguration.ConnectionString_KeyExists AndAlso LiveLinkConfiguration.SQL_DB_Connect_KeyExists Then
            LiveLinkConfiguration.ConnectionString = LiveLinkConfiguration.SQL_DB_Connect
            LiveLinkConfiguration.SQL_DB_Connect = "Obsolete - Use ConnectionString"
            LiveLinkConfiguration.Save()
        End If

    End Sub


    Private Sub RemoveLiveLinkShortCuts(ByVal path As String)
        ' Remove any shortcut files (with a .lnk extension that contain the word livelink)
        Try
            Dim di As New DirectoryInfo(path)
            Dim fi As FileInfo() = di.GetFiles("*livelink*.lnk")
            For Each f As FileInfo In fi
                If f.Name.ToLower.Contains("livelink") Then
                    f.Delete()
                End If
            Next
        Catch ex As Exception
            ' Do nothing
        End Try
    End Sub

#End Region


#Region " Command Line Parsing"


    Private Sub ParseCommandLine(ByVal args As String(), ByVal commandLine As String)
        Try
            ' check if there are any command line parameters
            If (args Is Nothing OrElse args.Length <= 1) Then
                ' if no arguments then don't parse anything
                Return
            End If

            Dim firstArgument As Boolean = True
            For Each argumentValue As String In args
                ' skip the first argument, which is always the executable name
                If (firstArgument) Then
                    firstArgument = False
                    Continue For
                End If

                ' check argument is a valid value
                If (String.IsNullOrEmpty(argumentValue)) Then
                    Continue For
                End If

                ' check command line argument at least match "/X" (all arguments need to match this format)
                If (argumentValue.Length < 2) Then
                    LogEvent(EventLogEntryType.Error, "Invalid command line: {0}{1}{1}Usage: {2}{1}", commandLine, vbCrLf, _commandLineUsage)
                    Return
                End If

                ' get the option value "/X" from the value
                Dim argumentType As String = argumentValue.Substring(0, 2)
                If (String.IsNullOrEmpty(argumentType)) Then
                    LogEvent(EventLogEntryType.Error, "Invalid command line: {0}{1}{1}Usage: {2}{1}", commandLine, vbCrLf, _commandLineUsage)
                    Return
                End If

                ' match argument type with available options
                Select Case (argumentType.ToLower())
                    Case EnumUtils(Of CommandLineArguments).GetClassName(CommandLineArguments.StoreNumber)
                        ' store the command line value for the store number
                        _argumentStoreNumber = GetCommandLineArgumentValue(argumentValue, CommandLineArguments.StoreNumber)

                    Case Else
                        ' on unknown option log an error
                        LogEvent(EventLogEntryType.Error, "Invalid command line option: {0}{1}{1}Usage: {2}{1}", argumentType, vbCrLf, _commandLineUsage)
                        Return
                End Select
            Next

        Catch ex As Exception
            ' on exception log an error
            LogEvent(EventLogEntryType.Error, "Exception occurred parsing command line: {0}{1}Exception:{2}{1}{1}Usage: {3}{1}", commandLine, vbCrLf, ex.Message, _commandLineUsage)
        End Try
    End Sub

    Private Function GetCommandLineArgumentValue(ByVal argumentValue As String, ByVal argumentType As CommandLineArguments, Optional ByVal defaultValue As String = "") As String
        Dim replaceValue As String = EnumUtils(Of CommandLineArguments).GetClassName(CommandLineArguments.StoreNumber)
        If (String.IsNullOrEmpty(replaceValue)) Then
            Return defaultValue
        End If

        ' create the replacement string
        replaceValue += ":"
        If (argumentValue.Contains(replaceValue)) Then
            Return argumentValue.Replace(replaceValue, "")
        End If
        ' return a default value of empty string
        Return defaultValue
    End Function


#End Region


#Region "Initialisation & Interface Calls"

    Public Sub New(ByVal commandLineArgs As String(), ByVal commandLine As String, ByVal appConfigFile As String, ByVal statusController As StatusController, ByVal logController As LogController)

        ' store the status & log controller references
        SetStatus = statusController
        LogStatus = logController
        ' store the appConfig file reference
        _appConfigFile = appConfigFile

        ' parse any command line parameters
        ParseCommandLine(commandLineArgs, commandLine)

        InitialiseLocalVariables()

    End Sub

    Private Sub InitialiseLocalVariables()

        _posCommon = New CompositePOS()

        AddHandler _posCommon.StatusChangedEvent, AddressOf PosStatusChangedHandler

    End Sub

    Public Sub DelayedInitialiseAutoStart() Implements ILiveLinkCore.DelayedInitialiseAutoStart
        LogEvent(EventLogEntryType.Information, "Live Link Starting... Delayed Initialisation")
        _timerDelayedInitialise.Enabled = True
    End Sub


    Public Sub TimerDelayedInitialise(ByVal sender As Object, ByVal e As System.Timers.ElapsedEventArgs) Handles _timerDelayedInitialise.Elapsed
        ' disable timer
        _timerDelayedInitialise.Enabled = False

        If (_initialised) Then
            LogEvent("Live Link Already Initialised", EventLogEntryType.Information)
            Return
        End If

        Try
            ' initialise the database and all other configurations
            Initialise()
        Catch ex As Exception
            LogEvent(EventLogEntryType.Error, "Exception occurred initialising LiveLink: {0}", ex.ToString())
        Finally
            ' disable the current database timer if intialisation was successfull
            If (_initialised) Then
                _timerDelayedInitialise.Enabled = False
                ' start the application
                Start()
            Else
                ' re-enable the timer and wait for initialisation to be attempted again
                _timerDelayedInitialise.Enabled = True
            End If
        End Try
    End Sub

    Public Function Initialise() As Boolean Implements ILiveLinkCore.Initialise
        ' check config is enabled correctly
        If (Not ConfigurationHelper.IsBaseConfigured()) Then
            Dim cm As New Mx.Configuration.ConfigMigrator(Path.Combine(Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath), _appConfigFile))
            AddHandler cm.MigrationStep, AddressOf LogConfigMigration
            cm.MigrateAppSettings(Path.Combine(Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath), "appSettings.config"))
            LogEvent("LiveLink configuration has been migrated to new format. See trace.log  in livelink directory for details.", EventLogEntryType.Information)

            ' raise the restart event!
            RaiseEvent RestartEvent(Me, New EventArgs())

            ' mark intialised as true, but return false (this prevents the Initialise from being called again). Application will wait for a restart...
            _initialised = True
            Return False
        End If

        UpdateAppsettings()

        LogEvent(EventLogEntryType.Information, "Initialising LiveLink")
        ' this option sets livelink to restart over a configurable number of days
        '
        ' 0=NEVER RESTART, 1=EVERY ONE DAY, 2=EVERY TWO DAYS etc...
        '
        ' this option added to free ODBC resources used when processing Aloha data
        '
        LiveLinkRestartFrequency = LiveLinkConfiguration.RestartFrequency
        ' The number of awake iterations before sending a verbose awake message 
        ' (eg if interval = 30 minutes and counter = 8 then verbose message will be sent every 4 hours
        _awakeCounterVerbose = Math.Max(1, LiveLinkConfiguration.AwakeCounterVerbose)
        _awakeInterval = LiveLinkConfiguration.AwakeInterval
        _flowTxInBatch = LiveLinkConfiguration.TransactionsInBatch
        _flowIterations = LiveLinkConfiguration.BatchSize
        _flowMaxRecords = LiveLinkConfiguration.MaxRecords
        _flowInterval = LiveLinkConfiguration.SendIntervalMinutes
        _preSendProcess = LiveLinkConfiguration.PreSendProcess

        ' initialise the import configuration setup
        If (_importConfigurationList Is Nothing) Then
            _importConfigurationList = ImportConfigurationFactory.CreateConfiguration()
            ' add import event handlers
            For Each importConfiguration As ILiveLinkImportConfiguration In _importConfigurationList.Values
                AddHandler importConfiguration.ProcessDateUpdateEvent, AddressOf Import_ProcessDateUpdateEvent
            Next
        End If

        ' initialise the form configuration setup
        If (_formConfigurationList Is Nothing) Then
            ' create form configuration list
            _formConfigurationList = FormConfigurationFactory.CreateConfiguration()

            ' add handler for easy clock local users sync
            If (_formConfigurationList.ContainsKey(LiveLinkFormType.EasyClock)) Then
                AddHandler DirectCast(_formConfigurationList(LiveLinkFormType.EasyClock), EasyClockFormConfiguration).SyncLocalUsers, AddressOf TimeClock_SyncLocalUsers
            End If
        End If

        ' initialise the LiveLinkID from the registry
        SetLiveLinkID()
        ' Log LiveLinkID property to the trace file
        LogEvent(EventLogEntryType.Information, "LiveLink ID [{0}]", _liveLinkID)

        ' database schema setup
        SetupDatabase()
        ' check if enhance schema is enabled
        If (LiveLinkConfiguration.LiveLinkSchemaEnabled) Then
            Try
                ' install livelink schema defined in Mx.LiveLink.DB
                Mx.Services.LiveLink.LiveLinkService.InstallSchema(LiveLinkConfiguration.ConnectionString)
            Catch ex As Exception
                LogStatus("Error", "Error occurred setting up new LiveLink schema")
                LogEvent(EventLogEntryType.Error, "Exception occurred installing new LiveLink schema. Exception: {0}", ex.ToString)
            End Try
        End If

        If LiveLinkConfiguration.ComprisEnabled Then
            Try
                ' check if PddFiles is set to be auto registered
                If (LiveLinkConfiguration.ComprisAutoRegisterPddFiles) Then
                    ComprisInterop.RegisterPddFilesDLL()
                End If

                ' Generate the Interop file for the Comnprios ActiveX dll
                ComprisInterop.GenerateComprisInteropFile(Application.StartupPath)

            Catch ex As Exception
                LogEvent("Unable to generate the Compris interop file" & vbCrLf & vbCrLf & "Error: " & ex.Message, EventLogEntryType.Warning)
            End Try
        End If

        LogEvent("Live Link Initialised", EventLogEntryType.Information)
        ' set Intilisation to true
        _initialised = True
        Return True
    End Function

    Private Sub Import_ProcessDateUpdateEvent(ByVal sender As Object, ByVal args As ProcessDateEventArgs)
        Me.ProcessDate = args.ProcessDate
    End Sub


    Public Sub Start() Implements ILiveLinkCore.Start
        ' log start of service
        LogEvent("Live Link started", EventLogEntryType.Information)
        LiveLinkStartTime = DateTime.Now

        ' do LiveLink auto-activation if enabled
        AutoActivateLiveLink()

        Try
            ' set processing to "True" this will prevent main timer from doing any actions until this startup process is complete
            _processing.Lock()

            ' start the main timers
            StartStopMainTimer(True)

            ' get the lease before starting any servers
            GetLease()

            ' start each of the import interfaces
            For Each importConfiguration As ILiveLinkImportConfiguration In _importConfigurationList.Values
                If (importConfiguration.Enabled AndAlso EntityID > 0) Then
                    ' call start, will only do a start if this functionality has been implemented in the configuration class
                    importConfiguration.Start(EntityID, _sessionKey, _liveLinkID)
                End If
            Next

            ' start POS specific
            AutoStartPOSSpecific()

            ' if forms are enabled, load any configured forms
            If (LiveLinkConfiguration.FormLoadEnabled AndAlso Not _formAutoShowComplete) Then
                ' check if any forms need to be auto-shown
                For Each formConfiguration As ILiveLinkFormConfiguration In _formConfigurationList.Values
                    If (formConfiguration.AutoShow AndAlso Not formConfiguration.AutoShowComplete) Then
                        Try
                            formConfiguration.LoadForm(EntityID, EntityName, SessionKey, LiveLinkID)
                        Catch ex As Exception
                            LogEvent(EventLogEntryType.Error, "Exception occurred auto-showing form [{0}]. Exception: {1}", ex.ToString)
                        Finally
                            formConfiguration.AutoShowComplete = True
                        End Try
                    End If
                Next
                ' mark auto show as completed
                _formAutoShowComplete = True
            End If

        Catch ex As Exception
            LogEvent(EventLogEntryType.Error, "Exception occurred starting LiveLink. Exception: {0}", ex.ToString())
        Finally
            ' ensure processing flag is reset after initialisation
            _processing.Unlock()
        End Try
    End Sub

    Public Sub [Stop]() Implements ILiveLinkCore.Stop
        ' stop the application
        LogEvent("Live Link stopped", EventLogEntryType.Information)
        StartStopMainTimer(False)

        ' if not initialised, then do not continue on to stop imports
        If (Not _initialised) Then
            _timerDelayedInitialise.Enabled = False
            Return
        End If

        ' stop each of the import interfaces
        For Each importConfiguration As ILiveLinkImportConfiguration In _importConfigurationList.Values
            If (importConfiguration.Enabled AndAlso EntityID > 0) Then
                importConfiguration.Stop(EntityID)
            End If
        Next

        ' stop POS specific 
        AutoStopPOSSpecific()
    End Sub


    Public Sub ForceImport() Implements ILiveLinkCore.ForceImport

        ' check entityID is set before doing any data imports
        If (EntityID > 0) Then
            ' ensure no processing is currently running
            If _processing.TryLock() Then
                ' cycle through each available data import
                For Each importConfiguration As ILiveLinkImportConfiguration In _importConfigurationList.Values
                    ' check if cancel flag has been set
                    If (CancelFlag) Then
                        Exit For
                    End If

                    ' check gateway is enabled
                    If (Not importConfiguration.GatewayEnabled) Then
                        Continue For
                    End If

                    ' go through each enabled import and force an import
                    RunImport(importConfiguration, True)
                Next

                LogStatus("Info...", "Import complete.")
                _processing.Unlock()
            Else
                LogStatus("Error...", "Failed to run import! a process is already running")
            End If
        End If
    End Sub

    Public Sub ForceImport(ByVal importType As LiveLinkImportType) Implements ILiveLinkCore.ForceImport
        ' check entityID is setup correctly
        If (EntityID <= 0) Then
            LogStatus("Error...", "No entityID set. Site is not configured")
            Return
        End If
        ' check configuration is available
        If (Not _importConfigurationList.ContainsKey(importType)) Then
            LogStatus("Error...", String.Format("{0} can not be run. It is not enabled.", EnumUtils(Of LiveLinkImportType).GetClassName(importType)))
            Return
        End If

        ' check configuration is available
        If (Not _importConfigurationList(importType).GatewayEnabled) Then
            LogStatus("Error...", String.Format("{0} can not be run. Gateway is not enabled.", EnumUtils(Of LiveLinkImportType).GetClassName(importType)))
            Return
        End If

        ' run the requested import
        RunImport(_importConfigurationList(importType), True)
    End Sub

    Private Sub RunImport(ByVal importConfiguration As ILiveLinkImportConfiguration, Optional ByVal force As Boolean = False)
        Try
            ' check if cancel flag has been set
            If (CancelFlag) Then
                Return
            End If

            ' raise an event indicating import has started
            RaiseEvent ImportBeginEvent(Me, New ImportEventArgs(importConfiguration.ImportType))

            ' check what type should be imported
            Select Case (importConfiguration.ImportType)
                Case LiveLinkImportType.Aloha
                    ProcessAloha(DirectCast(importConfiguration, AlohaConfiguration))
                Case LiveLinkImportType.Compris
                    ProcessCompris(DirectCast(importConfiguration, ComprisConfiguration))
                Case LiveLinkImportType.ComprisPDP
                    ProcessComprisPDP(DirectCast(importConfiguration, ComprisPDPConfiguration))
                Case LiveLinkImportType.ComprisPDPUpdates
                    ProcessComprisPDPUpdates(DirectCast(importConfiguration, ComprisPDPUpdatesConfiguration))
                Case LiveLinkImportType.ComprisPollFiles
                    ProcessComprisPollFiles(DirectCast(importConfiguration, ComprisPollFilesConfiguration))
                Case LiveLinkImportType.ICG
                    ProcessICG(DirectCast(importConfiguration, ICGConfiguration))
                Case LiveLinkImportType.Intouch
                    ProcessIntouch(DirectCast(importConfiguration, IntouchConfiguration))
                Case LiveLinkImportType.iPOS
                    ProcessiPOS(DirectCast(importConfiguration, iPOSConfiguration), force)
                Case LiveLinkImportType.iPOSProduct
                    UpdateiPOSProducts(DirectCast(importConfiguration, iPOSConfiguration))
                Case LiveLinkImportType.iPOSStaff
                    UpdateiPOSStaff(DirectCast(importConfiguration, iPOSConfiguration))
                Case LiveLinkImportType.Micros
                    ProcessMicros(DirectCast(importConfiguration, MicrosConfiguration), force)
                Case LiveLinkImportType.MicrosoftRMS
                    ProcessRMS(DirectCast(importConfiguration, RMSConfiguration))
                Case LiveLinkImportType.Octane
                    ProcessOctane(DirectCast(importConfiguration, OctaneConfiguration))
                Case LiveLinkImportType.OfflineCashupInfo
                    RetrieveOfflineCashupInfo(0)
                Case LiveLinkImportType.OfflineCashupSend
                    ProcessOfflineCashup(DirectCast(importConfiguration, OfflineCashupSendConfiguration))
                Case LiveLinkImportType.OfflineCashupSettings
                    RetrieveOfflineCashupSettings()
                Case LiveLinkImportType.Panasonic
                    ProcessPanasonic(DirectCast(importConfiguration, PanasonicConfiguration))
                Case LiveLinkImportType.PARExalt4
                    ProcessPar(DirectCast(importConfiguration, PARExaltConfiguration))
                Case LiveLinkImportType.POSI
                    ProcessPosi(DirectCast(importConfiguration, POSIConfiguration))
                Case LiveLinkImportType.Radiant
                    ProcessRadiant(DirectCast(importConfiguration, RadiantConfiguration))
                Case LiveLinkImportType.ServiceReport
                    ProcessServiceReport(DirectCast(importConfiguration, ServiceReportConfiguration))
                Case LiveLinkImportType.Uniwell
                    ProcessUniwell(DirectCast(importConfiguration, UniwellConfiguration))
                Case LiveLinkImportType.ZKFingerprint
                    ProcessZKFingerprintsTA(DirectCast(importConfiguration, ZKFingerprintConfiguration))
                Case LiveLinkImportType.ZKFirmware
                    UpdateZKFirmware(DirectCast(importConfiguration, ZKFingerprintConfiguration))
                Case LiveLinkImportType.POSixml
                    ProcessPOSixml(DirectCast(importConfiguration, POSixmlImportConfiguration))
                Case LiveLinkImportType.Xpient
                    ProcessXpient(DirectCast(importConfiguration, XpientImportConfiguration))
                Case LiveLinkImportType.RPOS
                    ProcessRPOS(DirectCast(importConfiguration, RPOSImportConfiguration))
                Case LiveLinkImportType.IntouchMenuPackage
                    ProcessIntouchMenuPackage(DirectCast(importConfiguration, IntouchMenuPackageConfiguration))
                Case LiveLinkImportType.Transight
                    ProcessPosCommon()
                Case LiveLinkImportType.TimePunchImport
                    ProcessTimePunchImport(DirectCast(importConfiguration, TimePunchImportConfiguration))
                Case LiveLinkImportType.SUS
                    ProcessSUSImport(DirectCast(importConfiguration, SUSImportConfiguration))
                Case LiveLinkImportType.AlertSystemsTrafficData
                    ProcessTrafficData(DirectCast(importConfiguration, AlertSystemsTrafficDataConfiguration))
                Case LiveLinkImportType.NewPOSTLD
                    ProcessPosCommon()
                Case LiveLinkImportType.NewPOSSummary
                    ProcessPosCommon()
                Case LiveLinkImportType.HomeService
                    ProcessPosCommon()
                Case LiveLinkImportType.STM
                    ProcessPosCommon()
                Case Else
                    Throw New NotImplementedException(String.Format("Import type:[{0}] is not implemented", importConfiguration.ImportType))
            End Select
        Catch ex As Exception
            LogEvent(EventLogEntryType.Error, "Exception occurred running import:[{0}]. Exception: {1}", importConfiguration.ImportType, ex.ToString())
        Finally
            ' update import values
            importConfiguration.FirstPollComplete = True
            importConfiguration.LastPoll = DateTime.UtcNow
            ' raise an event indicating import has completed
            RaiseEvent ImportCompleteEvent(Me, New ImportEventArgs(importConfiguration.ImportType))
        End Try

    End Sub


    Public Sub LoadForm(ByVal formType As LiveLinkFormType) Implements ILiveLinkCore.LoadForm
        If (Not _formConfigurationList.ContainsKey(formType)) Then
            LogStatus("Error...", String.Format("{0} can not be opened. It is not enabled.", EnumUtils(Of LiveLinkFormType).GetClassName(formType)))
            Return
        End If

        If (Not _formConfigurationList(formType).Enabled) Then
            LogStatus("Error...", String.Format("{0} can not be opened. It is not enabled.", EnumUtils(Of LiveLinkFormType).GetClassName(formType)))
            Return
        End If

        Select Case (formType)
            Case LiveLinkFormType.YISController
                ' check if the YIS controller form is enabled and the YIS import is enabled
                If (_formConfigurationList.ContainsKey(LiveLinkFormType.YISController) AndAlso _importConfigurationList.ContainsKey(LiveLinkImportType.YIS)) Then
                    ' initialise the consumers for the YIS controller
                    DirectCast(_formConfigurationList(LiveLinkFormType.YISController), YISControllerFormConfiguration).ConsumerPOSControl = DirectCast(_importConfigurationList(LiveLinkImportType.YIS), YisConfiguration).ConsumerPOSControl
                    DirectCast(_formConfigurationList(LiveLinkFormType.YISController), YISControllerFormConfiguration).ConsumerRDSDisplay = DirectCast(_importConfigurationList(LiveLinkImportType.YIS), YisConfiguration).ConsumerRDSDisplay
                End If
        End Select

        Try
            ' load the form
            _formConfigurationList(formType).LoadForm(EntityID, EntityName, SessionKey, LiveLinkID)
        Catch ex As Exception
            LogEvent(EventLogEntryType.Error, "Exception occurred loading form:[{0}]. Exception: {1}", formType, ex.ToString())
        End Try
    End Sub

    Public Sub ForceProcessing() Implements ILiveLinkCore.ForceProcessing
        If _processing.TryLock() Then
            Try
                OK_To_Send = True ' Override the backoff timer if Process Now is clicked

                If (_sessionKey = "") And (_conditionalSessionKey <> "") Then
                    ' If the site is not yet activated, expire the lease so that a new lease is obtained.
                    ' This will ensure that the activation status is checked when the button is clicked, 
                    ' rather than waiting for the lease to expire.
                    ExpireLease()
                End If
                ' send all data types to server
                SendAllData()

                ' run offline cashup send
                If (ImportEnabled(LiveLinkImportType.OfflineCashupSend)) Then
                    RunImport(_importConfigurationList(LiveLinkImportType.OfflineCashupSend), False)
                End If
                ProcessLiveSync(True)

                ' Check for any pending messages
                RetrieveMessage()
            Catch ex As Exception
                LogEvent(EventLogEntryType.Error, "Exception occurred forcing process of data. Exception: {0}", ex.ToString)
            Finally
                ' ensure processing flag is disabled at completion
                _processing.Unlock()
            End Try
            LogStatus("Info...", "Processing complete.")
        Else
            LogStatus("Error...", "Failed to force processing! a process is already running")
        End If
    End Sub

    Public Sub CancelProcesing() Implements ILiveLinkCore.CancelProcesing
        If _processing.Locked Then
            CancelFlag = True
        End If
    End Sub

    Public Sub SendData() Implements ILiveLinkCore.SendData
        If _processing.TryLock() Then
            Try
                ' send all data types to the server
                SendAllData()
            Catch ex As Exception
                LogEvent(EventLogEntryType.Error, "Exception occurred sending data. Exception: {0}", ex.ToString)
            Finally
                _processing.Unlock()
            End Try
            LogStatus("Info...", "Send complete.")
        Else
            LogStatus("Error...", "Failed to send data! a process is already running")
        End If
    End Sub

    Private Sub SendAllData()
        ' send any processed transaction level data
        SendDataToServer()
        ' send any processed summary data (if it is enabled)
        SendConfigurableData()
        ' purge any obsolete data
        PurgeAllOldData(LiveLinkConfiguration.Use_ODBC_DB_Connect, LiveLinkConfiguration.DaysBeforeObsolete, LiveLinkConfiguration.POSType)
        ' reset current count
        CurrentCount = 0
    End Sub

    Public Sub PollData(ByVal sender As Object, ByVal e As System.Timers.ElapsedEventArgs) Handles _timerPoll.Elapsed
        DoPollData(PollReasonType.OneMinuteTimer)
    End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="pollReason">The reason we are polling for data</param>
    ''' <remarks></remarks>
    Private Sub DoPollData(ByVal pollReason As PollReasonType)
        Dim AwakeInterval As Integer

        If _processing.TryLock() Then
            Try
                _timerPoll.Enabled = False

                'Only update the awake count if we have hit the one second timer again
                If pollReason = PollReasonType.OneMinuteTimer Then
                    AwakeCount += 1
                    If HasConditionalLease() Then
                        AwakeInterval = 2 * _awakeInterval ' If a conditional lease has been granted, then slow down the awake messages
                    Else
                        AwakeInterval = _awakeInterval
                    End If

                    If (AwakeCount >= AwakeInterval) Then
                        If ProcessAwake() Then
                            AwakeCount = 0 ' Only reset the awake counter after a successful heartbeat
                        End If
                    End If
                End If

                ' check entityID is set before doing any data imports
                If (EntityID > 0) Then
                    ' check if database size has been exceeded, do not process if it has been!
                    If (Not DatabaseLimitExceeded()) Then

                        ' cycle through each available data import
                        For Each importConfiguration As ILiveLinkImportConfiguration In _importConfigurationList.Values
                            ' check if cancel flag has been set
                            If (CancelFlag) Then
                                Exit For
                            End If

                            ' check gateway is enabled
                            If (Not importConfiguration.GatewayEnabled) Then
                                Continue For
                            End If

                            ' check if import is scheduled (ignore this check if we're polling because of new data received)
                            If (pollReason = PollReasonType.NewDataAvailable OrElse Not importConfiguration.FirstPollComplete OrElse importConfiguration.LastPoll.AddMinutes(importConfiguration.PollInterval) < DateTime.UtcNow) Then
                                ' run the configured import
                                RunImport(importConfiguration)
                            End If
                        Next
                    End If
                End If

                'Only check the flow interval before sending if we're polling due to the one second timer
                If pollReason = PollReasonType.NewDataAvailable Then
                    SendAllData()
                Else
                    CurrentCount += 1
                    ' check if data should be sent
                    If (CurrentCount >= _flowInterval) Then
                        SendAllData()
                    End If
                End If

                ' send live sync data
                ProcessLiveSync()
                ' check if any servers or tasks need to be kept alive
                AutoKeepAlive()

                ' check if there are any messages waiting
                If _messageWaiting Then
                    RetrieveMessage()
                End If
                ' Check if livelink needs to be shutdown or restarted
                CheckAction()

            Catch ex As Exception
                LogEvent(EventLogEntryType.Error, "Exception occurred actioning main polling tasks (PollReason={1}). Exception: {0}", ex.ToString, pollReason.ToString())
            Finally
                CancelFlag = False
                _processing.Unlock()
                _timerPoll.Enabled = True
            End Try
        End If
    End Sub

    Private Sub CommunicationsBackOff()
        CommsBackoffCount = Max(CommsBackoffCount - 1, 0)
        If CommsBackoffCount = 0 Then
            OK_To_Send = True
        End If
    End Sub

    Private Sub SuccessfulCommunications()
        OK_To_Send = True
        CommsBackoffMinutes = 1 ' After a successful communication, reset the backoff timer to 1 minute
        UpdateLastSuccessfulCommunication()
    End Sub

#End Region

#Region "Database size check"

    ' function to check if the total records, or total unsynced records exceed a limit
    Private Function DatabaseLimitExceeded() As Boolean
        If LiveLinkConfiguration.POSType.ToUpper() = "MICROS" Then
            Return False
        End If

        ' check if database limiter is enabled
        If (LiveLinkConfiguration.LimitDatabaseSize) Then
            ' check if timeout is still active...
            If (_databaseSizeExceeded) Then
                If (_databaseSizeExceededTimeout.AddMinutes(LiveLinkConfiguration.LimitExceededPause) > DateTime.UtcNow) Then
                    LogStatus("Error...", String.Format("Processing is paused for {0} minutes due to database size...", LiveLinkConfiguration.LimitExceededPause))
                    Return True
                End If
                ' reset the database size exceeded flags
                ResetDatabaseSizeExceeded()
            End If

            ' check if limiter is enabled for TOTAL size
            If (LiveLinkConfiguration.LimitTotalRecords > 0) Then
                ' get the total record count
                _databaseTotalRecords = Mx.Services.LiveLink.SalesMainService.GetRecordCount()
                If (_databaseTotalRecords > LiveLinkConfiguration.LimitTotalRecords) Then
                    ' set flag indicating database size has been exceeded
                    SetDatabaseSizeExceeded()
                    LogStatus("Error...", String.Format("Maximum database size has been exceeded (total records {0})", _databaseTotalRecords))
                    ' return database size exceeded
                    Return True
                End If
            End If

            ' check if limiter is enabled for UNSYNCED size
            If (LiveLinkConfiguration.LimitUnsyncedRecords > 0) Then
                ' get the total unsynced record count
                _databaseTotalUnsyncedRecords = Mx.Services.LiveLink.SalesMainService.GetUnsyncedRecordCount()
                If (_databaseTotalUnsyncedRecords > LiveLinkConfiguration.LimitUnsyncedRecords) Then
                    ' set flag indicating database size has been exceeded
                    SetDatabaseSizeExceeded()
                    LogStatus("Error...", String.Format("Maximum database size has been exceeded (total unsynced records {0})", _databaseTotalUnsyncedRecords))
                    ' return database size exceeded
                    Return True
                End If
            End If
        End If

        Return False
    End Function

    Private Sub SetDatabaseSizeExceeded()
        _databaseSizeExceeded = True
        _databaseSizeExceededTimeout = DateTime.UtcNow ' use UTC now time to handle DST changeover
    End Sub

    Private Sub ResetDatabaseSizeExceeded()
        _databaseSizeExceeded = False
        _databaseSizeExceededTimeout = DateTime.MinValue
    End Sub

#End Region

#Region " Web Service Data Transfer "

    Private Sub tmrCommsBackoff_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _timerCommsBackoff.Elapsed
        CommsBackoffCount = Max(CommsBackoffCount - 1, 0)
        If CommsBackoffCount = 0 Then
            OK_To_Send = True
        End If
    End Sub

    Private Sub BackOffCommunications(ByVal disablePing As Boolean, Optional ByVal ForcedBackoffInterval As Integer = -1)
        ' Reset the timer
        If disablePing Then
            PingEnabled = False
        End If
        _timerCommsBackoff.Stop()
        If ForcedBackoffInterval <> -1 Then
            ' If a value is passed in, this means that a forced back off time has been sent by
            ' the web service.
            CommsBackoffCount = Max(Min(ForcedBackoffInterval, MaxCommsBackoffMinutes), 1)
            CommsBackoffMinutes = CommsBackoffCount
        Else
            CommsBackoffCount = CommsBackoffMinutes
            ' TODO: Flush DNS cache, reset IP stack (as this back off was caused by a comms failure)
        End If
        LogStatus("Pause ...", "Sending is paused for " & CommsBackoffCount & " minute(s)")
        ' Increase the next backoff time according to the following algorithm ...
        ' 1, 2, 3, 4, 5 minutes ... then
        ' 10, 15, 20, 25, 30 minutes ... then
        ' 40, 50, 60 minutes ... then
        ' 90, 120, 150, 180 minutes ...
        ' increase by 1 hour until max (4 hours) is reached
        Dim NextBackoffMinutes As Integer
        Select Case CommsBackoffMinutes
            Case 0 To 4
                NextBackoffMinutes = CommsBackoffMinutes + 1 ' increase by 1 minute
            Case 5 To 25
                NextBackoffMinutes = CommsBackoffMinutes + 5 ' increase by 5 minutes
            Case 26 To 50
                NextBackoffMinutes = CommsBackoffMinutes + 10 ' increase by 10 minutes
            Case 51 To 150
                NextBackoffMinutes = CommsBackoffMinutes + 30 ' increase by half an hour
            Case Else
                NextBackoffMinutes = CommsBackoffMinutes + 60 ' increase by 1 hour
        End Select
        CommsBackoffMinutes = System.Math.Min(NextBackoffMinutes, MaxCommsBackoffMinutes) ' Double the next backoff time
        OK_To_Send = False
        _timerCommsBackoff.Start()
    End Sub


    Private Sub UpdateLastSuccessfulCommunication()
        ' Stub function
        ' TODO: Add the current time to a field in a table in the database
        ' Warning - do not break other LiveLinks eg Micros - Ask Mike for help here!!
        Try
            Select Case LiveLinkConfiguration.POSType.ToUpper()
                Case "MICROS", "PIXELPOINT"
                    ' Currently exclude this from all sybase instances of LiveLink
                Case Else
                    Dim sql As String = String.Format("UPDATE tbStoreInfo SET LastAwake='{0:yyyyMMdd HH:mm:ss}'", DateTime.Now)
                    Dim errorMsg As String = String.Empty
                    If Not MMSGeneric_Execute(sql, errorMsg) Then
                        LogEvent(EventLogEntryType.Error, "Error occurred updating last communication time. SQL: {0}{0}{1}{0}", vbCrLf, errorMsg)
                    End If
            End Select
        Catch ex As Exception
            LogEvent(EventLogEntryType.Error, "Exception occurred updating last communication time. Exception: {0}{0}{1}{0}", vbCrLf, ex.ToString)
        End Try
    End Sub

    Private Function Is_OK_To_Send() As Boolean
        ' Reports back whether it is OK to send and updates the visual display.
        If OK_To_Send Then
            Is_OK_To_Send = True
        Else
            Is_OK_To_Send = False
            LogStatus("Sending skipped ...", "Communications are still paused for " & CommsBackoffCount & " minute(s)")
        End If
    End Function

    Private Sub ProcessResponse(ByVal MyLiveLinkResponse As eRetailer.LiveLinkResponse, Optional ByVal Backoff As Boolean = False)
        ' Use this function to handle any responses that you do not wish to handle directly in the 
        ' routines that talk to the web service
        Select Case MyLiveLinkResponse.ResponseCode
            Case eRetailer.LiveLinkResponseCode.Backoff
                LogStatus("", "Store is temporarily offline")
                BackOffCommunications(True, MyLiveLinkResponse.ResponseNumber)
            Case eRetailer.LiveLinkResponseCode.Offline
                LogStatus("", "Store is offline")
                BackOffCommunications(True, MaxCommsBackoffMinutes)
            Case eRetailer.LiveLinkResponseCode.AwaitingActivation
                LogStatus("", "Store not yet activated")
                If Backoff Then
                    BackOffCommunications(True)
                End If
                PingEnabled = False
            Case eRetailer.LiveLinkResponseCode.LeaseExpired
                LogStatus("", "Lease expired")
                ExpireLease()
                If Backoff Then
                    BackOffCommunications(False)
                End If
            Case eRetailer.LiveLinkResponseCode.LeaseRejected
                LogStatus("", "Lease rejected")
                ExpireLease()
                If Backoff Then
                    BackOffCommunications(True)
                End If
                PingEnabled = False
            Case eRetailer.LiveLinkResponseCode.InvalidConfiguration
                LogStatus("", "Invalid configuration")
                If Backoff Then
                    BackOffCommunications(True)
                End If
                PingEnabled = False
            Case eRetailer.LiveLinkResponseCode.DataError, eRetailer.LiveLinkResponseCode.DataErrorFlagRecords
                LogStatus("", "Data Error")
                If Backoff Then
                    BackOffCommunications(True)
                End If
                PingEnabled = False
            Case eRetailer.LiveLinkResponseCode.DataErrorInvalidStore
                LogStatus("", "Data Error - Invalid store id")
                If Backoff Then
                    BackOffCommunications(True)
                End If
                PingEnabled = False
            Case eRetailer.LiveLinkResponseCode.UnknownError
                LogStatus("", "An unknown error has occured")
                If Backoff Then
                    BackOffCommunications(True)
                End If
                PingEnabled = False
        End Select
    End Sub

    Public Shared Function GettbSalesMainFields(ByVal myPOSType As String) As String
        Dim sFieldList As String = "*"
        Select Case myPOSType.Trim.ToUpper
            Case "MICROS"
                ' Note that the order of these columns is IMPORTANT.  
                ' If you change the order or insert new columns, you must update the column numbers in the function sqlSelectAllRecordsForTransaction
                sFieldList = ""
                sFieldList &= "SalesMainID, " ' 1
                sFieldList &= "RecordType, " ' 2
                sFieldList &= "TransactionID, " ' 3
                sFieldList &= "RecordSubType, " ' 4
                sFieldList &= "EntityID, " ' 5
                sFieldList &= "RegisterID, " ' 6
                sFieldList &= "convert(varchar, datepart(day, polldate)) + '-' + datename(month, polldate) + '-' + convert(varchar, datepart(year, polldate)) + ' ' + convert(varchar, datepart(hour, polldate)) + ':' + Right('0' + convert(varchar, datepart(minute, polldate)), 2) + ':' + Right('0' + convert(varchar, datepart(second, polldate)), 2) as PollDate, " ' 7
                sFieldList &= "PollCount, " ' 8
                sFieldList &= "PollAmount, " ' 9
                sFieldList &= "ClerkID, " ' 10
                sFieldList &= "ClerkName, " ' 11
                sFieldList &= "CustomerID, " ' 12
                sFieldList &= "Synced, " ' 13
                sFieldList &= "PLUCodeID, " ' 14
                sFieldList &= "PLUCode, " ' 15
                sFieldList &= "SequenceNo, " ' 16
                sFieldList &= "Flag, " ' 17
                sFieldList &= "ApplyTax, " ' 18
                sFieldList &= "PriceLevel, " ' 19
                sFieldList &= "SyncToken, " ' 20
                sFieldList &= "ParentID, " ' 21
                sFieldList &= "IFNULL(BusinessDay,NULL,convert(varchar, datepart(day, BusinessDay)) + '-' + datename(month, BusinessDay) + '-' + convert(varchar, datepart(year, BusinessDay)) + ' ' + convert(varchar, datepart(hour, BusinessDay)) + ':' + Right('0' + convert(varchar, datepart(minute, BusinessDay)), 2) + ':' + Right('0' + convert(varchar, datepart(second, BusinessDay)), 2)) as BusinessDay, " ' 22
                sFieldList &= "TransactionVersion, " ' 23
                sFieldList &= "ItemDiscount " ' 24
            Case "ALOHA"
                sFieldList = ""
                sFieldList &= "SalesMainID, "
                sFieldList &= "RecordType, "
                sFieldList &= "TransactionID, "
                sFieldList &= "RecordSubType, "
                sFieldList &= "SubTypeDescription, "
                sFieldList &= "EntityID, "
                sFieldList &= "RegisterID, "
                sFieldList &= "convert(varchar, datepart(day, polldate)) + '-' + datename(month, polldate) + '-' + convert(varchar, datepart(year, polldate)) + ' ' + convert(varchar, datepart(hour, polldate)) + ':' + Right('0' + convert(varchar, datepart(minute, polldate)), 2) + ':' + Right('0' + convert(varchar, datepart(second, polldate)), 2) as PollDate, "
                sFieldList &= "PollCount, "
                sFieldList &= "PollAmount, "
                sFieldList &= "ClerkID, "
                sFieldList &= "ClerkName, "
                sFieldList &= "CustomerID, "
                sFieldList &= "Synced, "
                sFieldList &= "PLUCodeID, "
                sFieldList &= "PLUCode, "
                sFieldList &= "SequenceNo, "
                sFieldList &= "Flag, "
                sFieldList &= "ApplyTax, "
                sFieldList &= "ItemTax, "
                sFieldList &= "ItemDiscount, "
                sFieldList &= "PriceLevel, "
                sFieldList &= "TransactionVersion, "
                sFieldList &= "POSTransactionID, "
                sFieldList &= "SyncToken, "
                sFieldList &= "ParentID, "
                sFieldList += "BusinessDay"
            Case Else
                sFieldList = "*"
        End Select
        Return sFieldList
    End Function

    Public Shared Function GettbSalesMainFieldsForInsert(ByVal myPOSType As String) As String
        Dim sFieldList As String = "*"
        Select Case myPOSType.Trim.ToUpper
            Case "ALOHA"
                sFieldList = ""
                'sFieldList += "SalesMainID, "
                sFieldList += "RecordType, "
                sFieldList += "TransactionID, "
                sFieldList += "RecordSubType, "
                sFieldList += "SubTypeDescription, "
                sFieldList += "EntityID, "
                sFieldList += "RegisterID, "
                sFieldList += "PollDate, "
                sFieldList += "PollCount, "
                sFieldList += "PollAmount, "
                sFieldList += "ClerkID, "
                sFieldList += "ClerkName, "
                sFieldList += "CustomerID, "
                sFieldList += "Synced, "
                sFieldList += "PLUCodeID, "
                sFieldList += "PLUCode, "
                sFieldList += "SequenceNo, "
                sFieldList += "Flag, "
                sFieldList += "ApplyTax, "
                sFieldList += "PriceLevel, "
                sFieldList += "POSTransactionID, "
                sFieldList += "DateAdded, "
                If LiveLinkConfiguration.AlohaVersion = "VER2" Then
                    sFieldList += "ParentID, "
                    sFieldList += "BusinessDay, "
                Else
                    sFieldList += "ParentID, "
                End If
                sFieldList &= "ItemTax, "
                sFieldList &= "ItemDiscount, "
                sFieldList &= "TransactionVersion "
            Case Else
                sFieldList = "*"
        End Select
        Return sFieldList
    End Function


    Private Enum DataPurgeMethodType
        [Default] = 0
        BusinessDay = 1
    End Enum

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="UseODBC"></param>
    ''' <param name="daysBeforeObsolete"></param>
    ''' <param name="posType"></param>
    ''' <param name="alwaysUseDefaultMethod">If true always use default purge method regardless of DataPurgeMethod setting</param>
    ''' <remarks></remarks>
    Public Sub PurgeAllOldData(ByVal UseODBC As Boolean, ByVal daysBeforeObsolete As Integer, ByVal posType As String, Optional alwaysUseDefaultMethod As Boolean = False)
        ' This function handles both OleDB and ODBC connections
        If daysBeforeObsolete = 0 Then Return

        _posCommon.PurgeOldData(daysBeforeObsolete)

        If alwaysUseDefaultMethod Then
            PurgeOldDataDefaultMethod(daysBeforeObsolete, posType, UseODBC)
            Return
        End If

        LogStatus("Waiting...", "Clearing obsolete data - over " & Trim(Str(daysBeforeObsolete)) & " days. ")
        Dim purgeMethod As DataPurgeMethodType = DataPurgeMethodType.Default

        If LiveLinkConfiguration.POSType.ToUpper() <> "MICROS" Then
            Try
                purgeMethod = DirectCast(LiveLinkConfiguration.DataPurgeMethod, DataPurgeMethodType)
            Catch ex As Exception
                LogStatus("Error...", String.Format("Unknown data purge method [{0}]. Using default method.", LiveLinkConfiguration.DataPurgeMethod))
            End Try
        End If

        Select Case (purgeMethod)
            Case DataPurgeMethodType.BusinessDay
                ' purge method utilising index on Synced & BusinessDay.
                Mx.Services.LiveLink.SalesMainService.DeleteSyncedByBusinessDay(DateTime.Now.Date.AddDays(-1 * daysBeforeObsolete), LiveLinkConfiguration.MaximumDeleteRowCount)

            Case Else
                PurgeOldDataDefaultMethod(daysBeforeObsolete, posType, UseODBC)
        End Select

    End Sub

    ''' <summary>
    ''' This is the old way of purging data for specific customer
    ''' </summary>
    ''' <param name="daysBeforeObsolete"></param>
    ''' <param name="posType"></param>
    ''' <param name="UseODBC"></param>
    ''' <remarks></remarks>
    Private Sub PurgeOldDataDefaultMethod(ByVal daysBeforeObsolete As Integer, ByVal posType As String, ByVal UseODBC As Boolean)

        Dim errMsg As String = String.Empty
        Dim qryPurge As String
        Dim qryLanguage As String = String.Empty
        Dim qryRowCount As String = String.Empty

        Select Case UCase(Trim(posType))
            Case "PROTOUCH"
                qryPurge = "(PollDate < {ts '" & Trim(Format((DateAdd(DateInterval.Day, daysBeforeObsolete * -1, Now)), "yyyy-MM-dd HH:mm:ss")) & "'}) "
            Case Is = "MICROS"
                qryPurge = "(convert(DateTime,PollDate) < '" & Trim(Format((DateAdd(DateInterval.Day, daysBeforeObsolete * -1, Now).Date), "yyyyMMdd")) & "')" & SybaseCommitWork
            Case Is = "PIXELPOINT"
                qryPurge = "(DateAdded < '" & Trim(Format((DateAdd(DateInterval.Day, daysBeforeObsolete * -1, Now).Date), "yyyyMMdd")) & "')" & SybaseCommitWork
            Case "QUICKEN"
                qryPurge = "(Date_Time < #" & Trim(Format((DateAdd(DateInterval.Day, daysBeforeObsolete * -1, Now).Date), "dd-MMM-yyyy")) & "#) "
            Case Else
                qryRowCount = String.Format("SET ROWCOUNT {0}{1} ", LiveLinkConfiguration.MaximumDeleteRowCount, vbCrLf)
                qryLanguage = "SET LANGUAGE us_english " ' Note - the us_english language is always available to SQL, regardless of the languages defined in sys.syslanguages
                qryPurge = String.Format("(convert(DateTime,PollDate) < '{0}') ", Trim(Format((DateAdd(DateInterval.Day, daysBeforeObsolete * -1, Now)), "yyyyMMdd HH:mm")))
        End Select
        qryPurge = String.Format("{0}{1}Delete From tbSalesMain Where (Synced <> 0) and {2}", qryRowCount, qryLanguage, qryPurge)

        If MMSGeneric_Execute(UseODBC, qryPurge, errMsg) Then
            LogStatus("Waiting...", "Obsolete data cleared.")
        Else
            LogStatus("Error...", "Error clearing obsolete data (Clear) - " & errMsg)
            Return
        End If
    End Sub

#End Region


#Region " Start/Stop mode functionality "

    Private Sub StartStopMainTimer(ByVal start As Boolean)
        _timerPoll.Enabled = start
        _isRunning = start
        StartStopPingTimer(start)

        If (Not start) Then
            CurrentCount = 0
        End If
    End Sub

    Private Sub AutoKeepAlive()
        ' only run keep alive every X minutes
        If (lastKeepAlive.AddMinutes(LiveLinkConfiguration.KeepAliveInterval) < DateTime.Now) Then
            lastKeepAlive = DateTime.Now

            For Each importConfiguration As ILiveLinkImportConfiguration In _importConfigurationList.Values
                ' run the keep alive command on any interfaces that implement this functionality
                If (importConfiguration.Enabled AndAlso EntityID > 0) Then
                    importConfiguration.KeepAlive(EntityID)
                End If
            Next
        End If

    End Sub

    Private Sub AutoStartPOSSpecific()
        StartRadiantListener()
    End Sub

    Private Sub AutoStopPOSSpecific()
        StopRadiantListener()
    End Sub

#End Region

#Region " Status, Setup, Config, Test "

    Public Delegate Sub SetStatusDelegate(ByVal T1 As String, ByVal T3 As String)

    Private Sub CheckAction()
        If BounceLiveLink() Then
            LogEvent(EventLogEntryType.Information, "LiveLink re-start triggered. Application was started at [{0}]. LiveLink is set to re-start every {1} days", LiveLinkStartTime.ToString(DateFormatNames.DatabaseDateTimeFormatWithSecond), LiveLinkRestartFrequency)
        ElseIf ForceShutDown() Then
            LogEvent(EventLogEntryType.Information, "User forced a shutdown with file [{0}]", LiveLinkShutDownFile)
        End If
    End Sub

    Private Function BounceLiveLink() As Boolean
        BounceLiveLink = False
        Dim TimeNow As DateTime = DateTime.Now

        If LiveLinkRestartFrequency > 0 AndAlso _
         TimeNow > LiveLinkStartTime.AddDays(LiveLinkRestartFrequency) AndAlso _
         TimeNow.Hour >= 3 AndAlso _
         TimeNow.Hour < 5 Then
            ' raise an event to restart the application
            RestartLiveLink()
            BounceLiveLink = True
        End If
    End Function

    Private Function ForceShutDown() As Boolean
        ForceShutDown = False
        If File.Exists(LiveLinkShutDownFile) Then
            File.Delete(LiveLinkShutDownFile)
            ForceShutDown = True
            ' raise an event to shutdown the application
            RaiseEvent ShutdownEvent(Me, New EventArgs())
        End If
    End Function

    Private Sub RestartLiveLink()
        LogEvent(EventLogEntryType.Information, "LiveLink is restarting ...")
        ' raise an event to restart the application
        RaiseEvent RestartEvent(Me, New EventArgs)
    End Sub


    Private Sub UserAccess(ByVal accessEnabled As Boolean)
        ' raise an event indicating the access level has changed
        RaiseEvent AccessEvent(Me, New AccessEventArgs(accessEnabled))
    End Sub

    Private Function ProcessAwake(Optional ByVal Force As Boolean = False) As Boolean
        ProcessAwake = False
        UserAccess(False)

        If Force OrElse HasConditionalLease() OrElse Is_OK_To_Send() Then
            GetLease()
            If Not _startupSent Then
                ' send startup information
                SendStartupInfo()
            End If

            If _conditionalSessionKey <> String.Empty Then ' Applies to both lease types
                Try
                    Dim LogDataSet As New DataSet
                    Dim AwakeMessage As String = String.Empty
                    CreateLogTable(LogDataSet)
                    If Force Or (_awakeCounter = 0) Then ' Only send verbose info every "AwakeCounterVerbose" times
                        If Force Then
                            AwakeMessage = "** Awake Info - Forced (UTC) **"
                        Else
                            AwakeMessage = "** Awake Info (UTC) **"
                        End If
                        AddLogInfo(LogDataSet, SessionMessage.msg_Information, AwakeMessage, Format(Now.ToUniversalTime, "dd-MMM-yyyy HH:mm:ss"))
                        AddLogInfo(LogDataSet, SessionMessage.msg_Information, "TotalRecords", GetAwakeData("TotalRecords"))
                        AddLogInfo(LogDataSet, SessionMessage.msg_Information, "UnSyncedRecords", GetAwakeData("UnSyncedRecords"))
                        AddLogInfo(LogDataSet, SessionMessage.msg_Information, "ErrorRecords", GetAwakeData("ErrorRecords"))
                        AddLogInfo(LogDataSet, SessionMessage.msg_Information, "WrongEntityRecords", GetAwakeData("WrongEntity"))
                        AddLogInfo(LogDataSet, SessionMessage.msg_Information, "DateLastRecord", GetAwakeData("DateLastRecord"))
                        AddLogInfo(LogDataSet, SessionMessage.msg_Information, "DateLastSyncedRecord", GetAwakeData("DateLastSyncedRecord"))
                        Try
                            Dim objSysInfo As New clsSysInfo
                            With objSysInfo
                                If .FixedDisk1 <> "" Then
                                    AddLogInfo(LogDataSet, SessionMessage.msg_Information, "Fixed Disk 1", .FixedDisk1)
                                End If
                                If .FixedDisk2 <> "" Then
                                    AddLogInfo(LogDataSet, SessionMessage.msg_Information, "Fixed Disk 2", .FixedDisk2)
                                End If
                                If .FixedDisk3 <> "" Then
                                    AddLogInfo(LogDataSet, SessionMessage.msg_Information, "Fixed Disk 3", .FixedDisk3)
                                End If
                            End With
                        Catch ex As Exception
                            AddLogInfo(LogDataSet, SessionMessage.msg_Warning, "System Info", "Unable to retrieve disk space info")
                        End Try
                    End If
                    If Force OrElse HasConditionalLease() Then
                        If Not Force And (AwakeMessage = String.Empty) Then
                            AddLogInfo(LogDataSet, SessionMessage.msg_Information, "** Awake Heartbeat - Conditional (UTC) **", Format(Now.ToUniversalTime, "dd-MMM-yyyy HH:mm:ss"))
                        End If
                        ' Now attach the entityid and registerid for information purposes
                        ' In the case of Quicken, the Serial_Number needs to be sent before the site can be activated
                        AddLogInfo(LogDataSet, SessionMessage.msg_Information, "Store Info", "EntityId: " & GetEntityId() & ", RegisterId: " & GetRegisterId())
                    Else
                        If AwakeMessage = String.Empty Then
                            AddLogInfo(LogDataSet, SessionMessage.msg_Information, "** Awake Heartbeat(UTC) **", Format(Now.ToUniversalTime, "dd-MMM-yyyy HH:mm:ss"))
                            ' Send a POS specific message for the system monitor
                            If (_importConfigurationList.ContainsKey(LiveLinkImportType.Intouch)) Then
                                Dim importConfiguration As IntouchConfiguration = DirectCast(_importConfigurationList(LiveLinkImportType.Intouch), IntouchConfiguration)
                                If (importConfiguration.LatestExportChanged) Then
                                    ' If using the InTouch POS, send the latest export file time
                                    AddLogInfo(LogDataSet, SessionMessage.msg_Information, "InTouch;Latest Export", Format(importConfiguration.LatestExport, "dd-MMM-yyyy HH:mm:ss"))
                                    importConfiguration.LatestExportChanged = False
                                End If
                            End If
                        End If
                    End If
                    SendLogInfo(LogDataSet)
                    'Ignore any result
                    _awakeCounter = (_awakeCounter + 1) Mod _awakeCounterVerbose
                    LogStatus("", "Heartbeat sent")
                    ProcessAwake = True
                Catch ex As WebException
                    LogStatus("Error", ex.Message)
                    BackOffCommunications(False)
                Catch ex As Exception
                    LogStatus("Error", "Unable to send Awake Info")
                End Try
            End If
        End If
    End Function



#Region "LiveSync data send"


    Private Sub ProcessLiveSync(Optional ByVal force As Boolean = False)
        ' check live sync is enabled
        If (Not LiveLinkConfiguration.LiveSyncEnabled) Then
            Return
        End If

        ' check if a sync is due
        If (_liveSyncLastPoll.AddMinutes(LiveLinkConfiguration.LiveSyncInterval) < DateTime.Now OrElse force) Then
            ' get a lease before syncing data
            GetLease()
            ' check lease is acquired
            If _sessionKey <> String.Empty Then
                ' send data to the server
                If Not LiveSyncDataSend.SendLiveSync(_liveLinkID, _sessionKey, EntityID, LogStatus) Then
                    LogStatus("Error", "Error occurring syncing live data with server. See event logs for details")
                End If
            End If
            ' set the last poll time to now
            _liveSyncLastPoll = DateTime.Now
        End If

    End Sub


#End Region


    Private Sub TimeClock_SyncLocalUsers(ByVal sender As Object, ByVal args As EventArgs)
        SyncLocalUsers()
    End Sub

    Private Function SyncLocalUsers() As Boolean
        Dim MyRemoteSync As New LiveLinkWebService
        Dim MyLiveLinkResponse As eRetailer.LiveLinkResponse

        MyRemoteSync.Url = LiveLinkConfiguration.WebService_URL
        LoadProxy(MyRemoteSync)

        GetLease()
        If _sessionKey <> String.Empty Then
            Try
                LogStatus("", "Retrieving user list")
                MyLiveLinkResponse = MyRemoteSync.GetUsers(_liveLinkID, _sessionKey, EntityID)
                If MyLiveLinkResponse.ResponseCode = eRetailer.LiveLinkResponseCode.Success Then
                    ReplaceLocalUsers(MyLiveLinkResponse.ResponseData)
                Else
                    LogStatus("", "Failed to retrieve user list")
                End If
            Catch ex As Exception
                LogStatus("Error", ex.Message)
            End Try
        End If
    End Function

    Public Function ReplaceLocalUsers(ByVal UsersData As DataSet) As Boolean
        Dim posType As String = LiveLinkConfiguration.POSType
        Dim useODBC As Boolean = LiveLinkConfiguration.Use_ODBC_DB_Connect
        Dim myRow As System.Data.DataRow
        Dim beginTransactionSQL As String = "BEGIN TRANSACTION" ' sql to begin a transaction
        Const deleteSQL As String = "DELETE FROM tbLocalUsers" + vbCrLf ' sql to delete local users
        Const sybaseDeleteSQL As String = "DELETE tbLocalUsers" + vbCrLf ' sql to delete local users for sybase
        Const endTransSQL As String = "COMMIT TRANSACTION"
        Dim insertSQL As String = String.Empty
        Dim sql As String
        Dim errorMsg As String = String.Empty
        Dim userId As Long = 0
        Dim userName As String
        Dim userPassword As String
        Dim firstName As String
        Dim lastName As String
        Dim posIDName As String
        Dim entityID As Long
        Dim employeeID As Long
        Dim employeeNumber As String

        ' add a ; to the end of the begin transaction and to the commit transaction statements for Sybase databases
        Select Case posType
            Case "MICROS", "PIXELPOINT"
                beginTransactionSQL += vbCrLf + sybaseDeleteSQL
            Case Else
                beginTransactionSQL += vbCrLf + deleteSQL
        End Select

        If UsersData.Tables(0).Rows.Count > 0 Then
            For Each myRow In UsersData.Tables(0).Rows
                userId = CLng(EvalNull(myRow("UserId"), "0"))
                userName = EvalNull(myRow.Item("UserName"), "")
                userPassword = EvalNull(myRow.Item("UserPassword"), "")
                firstName = EvalNull(myRow.Item("FirstName"), "")
                lastName = EvalNull(myRow.Item("LastName"), "")
                posIDName = EvalNull(myRow.Item("POSIDName"), "")
                employeeID = EvalNull(myRow.Item("employeeID"), "0")
                entityID = CType(EvalNull(myRow.Item("EntityID"), "0"), Long)
                employeeNumber = TryGetValueFromRow(myRow, "EmployeeNumber", String.Empty)

                If userId > 0 AndAlso _
                    userName <> String.Empty Then
                    ' create insert query
                    insertSQL += "INSERT INTO tbLocalUsers (UserId, UserName, UserPassword, FirstName, LastName, POSIDName, EmployeeID, EntityID, EmployeeNumber) VALUES ("
                    insertSQL += userId.ToString + ", "
                    insertSQL += "'" + EncodeSQL(userName) + "', "
                    insertSQL += "'" + EncodeSQL(userPassword) + "', "
                    insertSQL += "'" + EncodeSQL(firstName) + "', "
                    insertSQL += "'" + EncodeSQL(lastName) + "', "
                    insertSQL += "'" + EncodeSQL(posIDName) + "', "
                    insertSQL += employeeID.ToString + ", "
                    insertSQL += entityID.ToString() + ", "
                    insertSQL += "'" + employeeNumber + "') " + vbCrLf
                End If
            Next

            ' if there are records to insert, then create transaction sql
            If insertSQL <> String.Empty Then
                sql = beginTransactionSQL + insertSQL + endTransSQL
                If Not MMSGeneric_Execute(useODBC, sql, errorMsg) Then
                    LogEvent(EventLogEntryType.Error, "Failed to update local users.{0}Query=[{1}]{2}Error=[{3}]", vbCrLf, sql, vbCrLf, errorMsg)
                    SetStatus("Failed to update users")
                    Return False
                End If
                SetStatus("Updated local users")
                Return True
            End If
        End If

        SetStatus("No users retrieved")
        Return False
    End Function

    Private Function GetAwakeData(ByVal sDataType As String) As String
        GetAwakeData = "Unknown"
        Try
            Dim sStoreID As String = EntityID
            Dim Use_ODBC As Boolean = LiveLinkConfiguration.Use_ODBC_DB_Connect
            Dim sPOSType As String = LiveLinkConfiguration.POSType.Trim.ToUpper
            Dim sQry As String
            Dim bNoMSA_Flag As Boolean

            Select Case sPOSType
                Case Is = "MICROS"
                    bNoMSA_Flag = True
                Case Else
                    bNoMSA_Flag = False
            End Select

            Select Case sDataType
                Case "TotalRecords"
                    sQry = "Select Count(*) from tbSalesMain "
                Case "UnSyncedRecords"
                    sQry = "Select Count(*) from tbSalesMain Where Synced=0"
                Case "ErrorRecords"
                    sQry = "Select Count(*) from tbSalesMain Where Synced=2"
                Case "WrongEntity"
                    sQry = "Select Count(*) from tbSalesMain Where Synced=3"
                Case "DateLastRecord"
                    Select Case sPOSType
                        Case "QUICKEN" ' Uses a different field name for Polled Date
                            sQry = "Select Max(Date_Time) from tbSalesMain"
                        Case "MICROS", "PROTOUCH" ' Stores Polled Date as a DateTime
                            sQry = "Select Max(PollDate) from tbSalesMain"
                        Case Else ' Stores PolledDate as a VARCHAR
                            sQry = "Select Max(CONVERT(DATETIME, PollDate)) from tbSalesMain"
                    End Select
                Case "DateLastSyncedRecord"
                    Select Case sPOSType
                        Case "QUICKEN" ' Uses a different field name for Polled Date
                            sQry = "Select Max(Date_Time) from tbSalesMain Where Synced = 1"
                        Case "MICROS", "PROTOUCH" ' Stores Polled Date as a DateTime
                            sQry = "Select Max(PollDate) from tbSalesMain Where Synced = 1"
                        Case Else ' Stores Polled Date as a VARCHAR
                            sQry = "Select Max(CONVERT(DATETIME, PollDate)) from tbSalesMain Where Synced = 1"
                    End Select
            End Select

            Dim sError As String = String.Empty
            Dim awkDataSet As DataSet
            If MMSGeneric_ListAll(Use_ODBC, sQry, "tbSalesMain", awkDataSet, sError, bNoMSA_Flag) Then
                If awkDataSet.Tables("tbSalesMain").Rows.Count = 1 Then
                    Select Case sDataType
                        Case "DateLastRecord", "DateLastSyncedRecord"
                            GetAwakeData = Format(awkDataSet.Tables("tbSalesMain").Rows(0).Item(0), "dd-MMM-yyyy HH:mm:ss")
                        Case Else
                            GetAwakeData = awkDataSet.Tables("tbSalesMain").Rows(0).Item(0).ToString
                    End Select
                End If
            End If
        Catch ex As Exception
        End Try
    End Function

#End Region

#Region " Message Retrieval "

    Public Sub RetrieveMessage() Implements ILiveLinkCore.RetrieveMessage
        Dim MessageStatus As RetrieveMessageStatus = RetrieveMessageStatus.Success ' The status of the downloaded message, to be sent back to the server (1 = Success; > 1 means error)
        Dim multiCommandMessage As Boolean = False

        UserAccess(False)
        If Is_OK_To_Send() Then
            GetLease()
            If _conditionalSessionKey <> "" Then ' If the machine is online or awaiting activation, then continue
                Try
                    Dim MyMessage As New LiveLinkWebService
                    Dim sPOSType As String = LiveLinkConfiguration.POSType.Trim
                    Dim MyLiveLinkResponse As eRetailer.LiveLinkResponse
                    LogStatus("Retrieving message ...", "Retrieving message from server")

                    MyMessage.Url = LiveLinkConfiguration.WebService_URL
                    LoadProxy(MyMessage)
                    MyLiveLinkResponse = MyMessage.RetrieveMessage(_liveLinkID, _conditionalSessionKey, 0, 0)
                    _messageWaiting = MyLiveLinkResponse.ResponseMessageWaiting
                    ' determine if the current message is a multicommand message (e.g. there is more than one command in a sequence that need to be executed)
                    multiCommandMessage = (MyLiveLinkResponse.RecordCount > 0)
                    Select Case MyLiveLinkResponse.ResponseCode
                        Case eRetailer.LiveLinkResponseCode.UnknownError
                            If MyLiveLinkResponse.ResponseString.Trim.ToUpper = "NO MESSAGE FOUND" Then
                                LogStatus("Information", "No messages waiting")
                                _messageWaiting = False
                            Else
                                LogStatus("Error", "Invalid message - ignoring")
                            End If
                        Case eRetailer.LiveLinkResponseCode.NewVersion
                            LogStatus("Message", "A new version is available for download")
                            Dim NewVersion As String() = MyLiveLinkResponse.ResponseString.Split(";")
                            If NewVersion.Length = 2 Then
                                ' Flag that the response has been received so that the server can flag as done successfully (1)
                                ' Pass the message id back to the server
                                MyLiveLinkResponse = MyMessage.RetrieveMessage(_liveLinkID, _conditionalSessionKey, MyLiveLinkResponse.ResponseNumber, RetrieveMessageStatus.Success)
                                If MyLiveLinkResponse.ResponseCode = eRetailer.LiveLinkResponseCode.Success Then
                                    UpgradeLiveLink(NewVersion(0), NewVersion(1))
                                Else
                                    LogStatus("Error", "Upgrade has been cancelled")
                                End If
                            Else
                                LogStatus("Error", "Invalid url and checksum retrieved")
                            End If
                        Case eRetailer.LiveLinkResponseCode.ChangeConfigsPermanent
                            LogStatus("Message", "Configuration settings are being updated")
                            If WriteNewConfigSettings(MyLiveLinkResponse.ResponseString) Then
                                MessageStatus = RetrieveMessageStatus.Success
                            Else
                                MessageStatus = RetrieveMessageStatus.NotSuccessful
                                LogStatus("Error", "Configuration settings not updated")
                            End If
                            MyLiveLinkResponse = MyMessage.RetrieveMessage(_liveLinkID, _conditionalSessionKey, MyLiveLinkResponse.ResponseNumber, MessageStatus)

                        Case eRetailer.LiveLinkResponseCode.POSCommand
                            LogStatus("Message", "POS Command Received")
                            Dim POSCommand As String = MyLiveLinkResponse.ResponseString
                            If POSCommand = String.Empty Then
                                MessageStatus = RetrieveMessageStatus.InvalidMessageFormat
                            Else
                                If SendPOSCommand(POSCommand, MyLiveLinkResponse.ResponseNumber) Then
                                    MessageStatus = RetrieveMessageStatus.Success
                                Else
                                    MessageStatus = RetrieveMessageStatus.NotSuccessful
                                End If
                            End If
                            ' Flag the command with the response status, Pass the message id back to the server 
                            MyLiveLinkResponse = MyMessage.RetrieveMessage(_liveLinkID, _conditionalSessionKey, MyLiveLinkResponse.ResponseNumber, MessageStatus)


                        Case eRetailer.LiveLinkResponseCode.POSCommandData
                            LogStatus("Message", "POS Command Data Received")
                            Dim posCommandData As String = MyLiveLinkResponse.ResponseString
                            If String.IsNullOrEmpty(posCommandData) Then
                                MessageStatus = RetrieveMessageStatus.InvalidMessageFormat
                            Else
                                If SavePOSCommandData(posCommandData, MyLiveLinkResponse.ResponseNumber) Then
                                    MessageStatus = RetrieveMessageStatus.Success
                                Else
                                    MessageStatus = RetrieveMessageStatus.NotSuccessful
                                End If
                            End If
                            ' Flag the POS command with the response status, Pass the message id back to the server 
                            MyLiveLinkResponse = MyMessage.RetrieveMessage(_liveLinkID, _conditionalSessionKey, MyLiveLinkResponse.ResponseNumber, MessageStatus)

                        Case eRetailer.LiveLinkResponseCode.Execute
                            LogStatus("Message", "Program to execute")
                            Try
                                Dim Command As String() = MyLiveLinkResponse.ResponseString.Split(";")
                                Dim Result As Integer
                                '
                                ' Command is a ; seperated string
                                ' 1st parameter = execution text
                                ' 2nd parameter = wait, 0=false, 1=true
                                ' 3rd parameter = timeout(optional), -1=infinite(default)
                                '
                                Select Case Command.Length
                                    Case 2
                                        Result = Shell(Command(0), AppWinStyle.MinimizedNoFocus, Wait:=IIf(Command(1) = 0, False, True))
                                        ' If wait=true, 0=success, <> 0 = timeout
                                        ' If wait=false, Result=ProcessID, 0 = failed
                                        If Command(1) <> "0" AndAlso Result <> 0 Then
                                            LogStatus("Message", "Execution timed out")
                                            MessageStatus = RetrieveMessageStatus.NotSuccessful
                                        ElseIf Command(1) = "0" AndAlso Result = 0 Then
                                            LogStatus("Message", "Execution failed")
                                            MessageStatus = RetrieveMessageStatus.NotSuccessful
                                        Else
                                            LogStatus("Message", "Program executed succesfully")
                                            MessageStatus = RetrieveMessageStatus.Success
                                        End If
                                    Case 3
                                        Result = Shell(Command(0), AppWinStyle.MinimizedNoFocus, Wait:=IIf(Command(1) = 0, False, True), Timeout:=Command(2))
                                        ' If wait=true, 0=success, <> 0 = timeout
                                        ' If wait=false, Result=ProcessID, 0 = failed
                                        If Command(1) <> "0" AndAlso Result <> 0 Then
                                            LogStatus("Message", "Execution timed out")
                                            MessageStatus = RetrieveMessageStatus.NotSuccessful
                                        ElseIf Command(1) = "0" AndAlso Result = 0 Then
                                            LogStatus("Message", "Execution failed")
                                            MessageStatus = RetrieveMessageStatus.NotSuccessful
                                        Else
                                            LogStatus("Message", "Program executed succesfully")
                                            MessageStatus = RetrieveMessageStatus.Success
                                        End If
                                    Case Else
                                        LogStatus("Message", "Execution failed - invalid format")
                                        MessageStatus = RetrieveMessageStatus.InvalidMessageFormat
                                End Select

                            Catch fileEx As FileNotFoundException
                                LogStatus("Message", "Execution failed - file not found")
                                MessageStatus = RetrieveMessageStatus.NotSuccessful
                            Catch ex As Exception
                                LogStatus("Message", "Execution failed - " & ex.ToString)
                                MessageStatus = RetrieveMessageStatus.NotSuccessful
                            End Try

                            ' Flag the command with the response status, Pass the message id back to the server 
                            MyLiveLinkResponse = MyMessage.RetrieveMessage(_liveLinkID, _conditionalSessionKey, MyLiveLinkResponse.ResponseNumber, MessageStatus)
                            If MyLiveLinkResponse.ResponseCode = LiveLinkResponseCode.LeaseExpired Then
                                ' This is a long running activity and the lease may have expired in the interim.  So get a new lease and retry.
                                ExpireLease()
                                GetLease()
                                MyLiveLinkResponse = MyMessage.RetrieveMessage(_liveLinkID, _conditionalSessionKey, MyLiveLinkResponse.ResponseNumber, MessageStatus)
                            End If

                        Case eRetailer.LiveLinkResponseCode.DownloadFile
                            LogStatus("Message", "Download command received.  Downloading ...")
                            Dim DownloadInfo As String() = MyLiveLinkResponse.ResponseString.Split(";")
                            If DownloadInfo.Length = 2 Then
                                ' Download url is first element
                                Dim url As String = DownloadInfo(0)
                                Dim LocalFile As String = DownloadInfo(1)
                                Dim myError As String = String.Empty
                                Try
                                    If Not Directory.Exists(Path.GetDirectoryName(LocalFile)) Then
                                        Directory.CreateDirectory(Path.GetDirectoryName(LocalFile))
                                    End If
                                    If HttpDownload.GetFile(url, LocalFile, myError, Me.Proxy) Then
                                        LogStatus("Message", "[" & LocalFile & "] has been downloaded.")
                                        MessageStatus = RetrieveMessageStatus.Success
                                        SendLogInfo(SessionMessage.msg_Information, "Download Succeeded", LocalFile)
                                    Else
                                        MessageStatus = RetrieveMessageStatus.NotSuccessful
                                        LogStatus("Error", "Download failed: " & myError)
                                        SendLogInfo(SessionMessage.msg_Error, "Download Failed", myError)
                                    End If
                                Catch ex As Exception
                                    MessageStatus = RetrieveMessageStatus.NotSuccessful
                                    SendLogInfo(SessionMessage.msg_Error, "Download Failed", ex.Message)
                                End Try
                            Else
                                MessageStatus = RetrieveMessageStatus.InvalidMessageFormat
                                LogStatus("Error", "Invalid url and local file name retrieved")
                                SendLogInfo(SessionMessage.msg_Error, "Download Failed", RetrieveMessageStatus.InvalidMessageFormat.ToString)
                            End If
                            ' Send back message response
                            MyLiveLinkResponse = MyMessage.RetrieveMessage(_liveLinkID, _conditionalSessionKey, MyLiveLinkResponse.ResponseNumber, MessageStatus)
                            If MyLiveLinkResponse.ResponseCode = LiveLinkResponseCode.LeaseExpired Then
                                ' This is a long running activity and the lease may have expired in the interim.  So get a new lease and retry.
                                ExpireLease()
                                GetLease()
                                MyMessage.RetrieveMessage(_liveLinkID, _conditionalSessionKey, MyLiveLinkResponse.ResponseNumber, MessageStatus)
                            End If
                        Case eRetailer.LiveLinkResponseCode.Restart
                            MyLiveLinkResponse = MyMessage.RetrieveMessage(_liveLinkID, _conditionalSessionKey, MyLiveLinkResponse.ResponseNumber, RetrieveMessageStatus.Success)
                            If MyLiveLinkResponse.ResponseCode = eRetailer.LiveLinkResponseCode.Success Then
                                LogEvent(EventLogEntryType.Information, "Restart command received from server")
                                LogStatus("Message", "Restart command received.  Restarting in 5 seconds ...")
                                Threading.Thread.Sleep(5000)
                                RestartLiveLink()
                            End If

                        Case eRetailer.LiveLinkResponseCode.DatabaseUpdate
                            LogStatus("Message", "Database update received. Executing...")
                            ' Split the query string on ";"
                            Dim SqlList As String() = MyLiveLinkResponse.ResponseString.Split(";")
                            Dim sSQL, sError As String
                            Dim Result As Boolean = True
                            Dim UseODBC As Boolean = LiveLinkConfiguration.Use_ODBC_DB_Connect

                            ' Execute each query string in the list. Queries are seperated by a ";"
                            For Each sSQL In SqlList
                                If sSQL <> String.Empty Then
                                    If Not MMSGeneric_Execute(UseODBC, sSQL, sError) Then
                                        LogStatus("Message", "Query Failed: [" & sSQL & "]" & vbCrLf & sError)
                                        LogEvent("DatabaseUpdate : Query Failed [" & sSQL & "]" & vbCrLf & sError, EventLogEntryType.Error)
                                        Result = False
                                    End If
                                End If
                            Next

                            ' Check if any of the queries failed
                            If Result Then
                                MessageStatus = RetrieveMessageStatus.Success
                            Else
                                MessageStatus = RetrieveMessageStatus.NotSuccessful
                            End If
                            LogStatus("Message", "Done - " & SqlList.Length & " command(s) executed.")

                            ' Send back message response
                            MyLiveLinkResponse = MyMessage.RetrieveMessage(_liveLinkID, _conditionalSessionKey, MyLiveLinkResponse.ResponseNumber, MessageStatus)
                            If MyLiveLinkResponse.ResponseCode = LiveLinkResponseCode.LeaseExpired Then
                                ' This is a long running activity and the lease may have expired in the interim.  So get a new lease and retry.
                                ExpireLease()
                                GetLease()
                                MyMessage.RetrieveMessage(_liveLinkID, _conditionalSessionKey, MyLiveLinkResponse.ResponseNumber, MessageStatus)
                            End If

                        Case eRetailer.LiveLinkResponseCode.DatabaseQuery
                            LogStatus("Message", "Database query received. Executing...")
                            ' Split the query string on ";"
                            Dim SqlList As String() = MyLiveLinkResponse.ResponseString.Split(";")
                            Dim sSQL, sError As String
                            Dim Result As Boolean = True
                            Dim UseODBC As Boolean = LiveLinkConfiguration.Use_ODBC_DB_Connect
                            Dim ds As DataSet
                            Dim FullDs As New DataSet
                            Dim Table As DataTable
                            Dim TableCounter As Integer = 0

                            FullDs.DataSetName = "CommandId_" & MyLiveLinkResponse.ResponseNumber.ToString & "-LiveLink_EntityId_" & GetEntityId.ToString

                            ' Execute each query string in the list. Queries are seperated by a ";"
                            For Each sSQL In SqlList
                                If sSQL <> String.Empty Then
                                    If Not MMSGeneric_ListAll(UseODBC, sSQL, "Data", ds, sError, False) Then
                                        LogStatus("Message", "Query Failed: [" & sSQL & "]" & vbCrLf & sError)
                                        LogEvent("DatabaseUpdate : Query Failed [" & sSQL & "]" & vbCrLf & sError, EventLogEntryType.Error)
                                        Result = False
                                    Else
                                        For Each Table In ds.Tables
                                            TableCounter += 1
                                            Table.TableName = "Query-" & TableCounter.ToString
                                            FullDs.Tables.Add(Table.Clone)
                                            FullDs.Merge(Table)
                                        Next
                                    End If
                                End If
                            Next

                            ' Check if any of the queries failed
                            If Result Then
                                MessageStatus = RetrieveMessageStatus.Success
                            Else
                                MessageStatus = RetrieveMessageStatus.NotSuccessful
                            End If
                            LogStatus("Message", "Done - " & TableCounter.ToString & " table(s) retrieved.")

                            ' Send back message response
                            If FullDs.Tables.Count > 0 Then
                                MyMessage.RetrieveMessage(_liveLinkID, _conditionalSessionKey, MyLiveLinkResponse.ResponseNumber, MessageStatus, CompressDataSet(FullDs))
                            Else
                                MyMessage.RetrieveMessage(_liveLinkID, _conditionalSessionKey, MyLiveLinkResponse.ResponseNumber, MessageStatus)
                            End If

                        Case eRetailer.LiveLinkResponseCode.RetrieveProjectionInfo
                            LogStatus("Message", "Projection Command Received")
                            Dim projectionDate As DateTime
                            If DateTime.TryParse(MyLiveLinkResponse.ResponseString, projectionDate) AndAlso RetrieveProjections(projectionDate) Then
                                MessageStatus = RetrieveMessageStatus.Success
                            Else
                                MessageStatus = RetrieveMessageStatus.NotSuccessful
                            End If
                            ' Flag the command with the response status, Pass the message id back to the server 
                            MyLiveLinkResponse = MyMessage.RetrieveMessage(_liveLinkID, _conditionalSessionKey, MyLiveLinkResponse.ResponseNumber, MessageStatus)
                        Case eRetailer.LiveLinkResponseCode.RetrieveFile
                            Dim localFile As String = String.Empty
                            LogStatus("Message", "Retrieve File command received")
                            If RetrieveFile(MyLiveLinkResponse.ResponseString, MyLiveLinkResponse.ResponseNumber, localFile) Then
                                MessageStatus = RetrieveMessageStatus.Success
                                LogStatus("Message", "Done - " & localFile & " retrieved.")
                            Else
                                MessageStatus = RetrieveMessageStatus.NotSuccessful
                            End If
                            MyLiveLinkResponse = MyMessage.RetrieveMessage(_liveLinkID, _conditionalSessionKey, MyLiveLinkResponse.ResponseNumber, MessageStatus)
                        Case eRetailer.LiveLinkResponseCode.RetrieveOfflineCashupInfo
                            LogStatus("Message", "Offline Cashup Update Command Received")
                            Dim offlineSyncID As Integer = 0
                            Integer.TryParse(MyLiveLinkResponse.ResponseString, offlineSyncID)
                            If RetrieveOfflineCashupInfo(offlineSyncID) Then
                                MessageStatus = RetrieveMessageStatus.Success
                            Else
                                MessageStatus = RetrieveMessageStatus.NotSuccessful
                            End If
                            ' Flag the command with the response status, Pass the message id back to the server 
                            MyLiveLinkResponse = MyMessage.RetrieveMessage(_liveLinkID, _conditionalSessionKey, MyLiveLinkResponse.ResponseNumber, MessageStatus)
                        Case Else
                            LogStatus("Error", "Message type not handled")
                            ' Flag the message as having been received but not handled (3) 
                            MyMessage.RetrieveMessage(_liveLinkID, _conditionalSessionKey, MyLiveLinkResponse.ResponseNumber, 3)
                    End Select

                    ' check if command response update was successful and there is another linked message waiting (RecordCount>0 indicates a linked message)
                    If (MessageStatus = RetrieveMessageStatus.Success AndAlso _messageWaiting = True AndAlso multiCommandMessage) Then
                        ' recursively call the RetrieveMessage() function
                        ' this will get all linked messages in sequence
                        RetrieveMessage()
                    End If
                Catch ex As WebException
                    LogStatus("Error", ex.Message)
                    BackOffCommunications(False)
                Catch ex As Exception
                    _messageWaiting = False
                    LogStatus("Error", ex.Message)
                    BackOffCommunications(True)
                End Try
            End If
        End If
    End Sub
    Private Function RetrieveProjections(ByVal projectionDate As DateTime) As Boolean
        UserAccess(False)
        If Is_OK_To_Send() Then
            GetLease()
            If _sessionKey <> "" Then ' If the machine is online, then continue
                Try
                    Dim myMessage As New LiveLinkWebService
                    Dim myLiveLinkResponse As eRetailer.LiveLinkResponse
                    LogStatus("Updating projections ...", "Retrieving projection info from server")

                    myMessage.Url = LiveLinkConfiguration.WebService_URL
                    LoadProxy(myMessage)
                    myLiveLinkResponse = myMessage.GetProductionInfo(_liveLinkID, _sessionKey, EntityID, projectionDate)
                    Select Case myLiveLinkResponse.ResponseCode
                        Case eRetailer.LiveLinkResponseCode.Success
                            LogStatus("Updating projections ...", "Updating projections")
                            If Projection.UpdateProjections(DecompressDataSet(myLiveLinkResponse.ResponseCompressedData), projectionDate) Then
                                LogStatus("Updating projections ...", "Projection update completed")
                            Else
                                LogStatus("Error updating projections ...", "Projection update did NOT complete")
                            End If
                            Return True
                        Case Else
                            ProcessResponse(myLiveLinkResponse, True)
                    End Select
                Catch ex As WebException
                    LogStatus("Error", ex.Message)
                    BackOffCommunications(False)
                    Return False
                Catch ex As Exception
                    _messageWaiting = False
                    LogStatus("Error", ex.Message)
                    BackOffCommunications(True)
                    Return False
                End Try
            End If
        End If
        Return False
    End Function

    Function RetrieveFile(ByVal command As String, ByVal messageId As Integer, ByRef fullFileName As String) As Boolean
        Dim commandInfo As String() = command.Split(";"c)

        If commandInfo.Length < 2 Then
            LogStatus("Error", "Invalid file information")
            SendLogInfo(SessionMessage.msg_Error, "Retrieve File Failed", RetrieveMessageStatus.InvalidMessageFormat.ToString)
            Return False
        End If

        Dim targetFolder As String = commandInfo(1).Trim
        Dim targetFilename As String
        If commandInfo.Length >= 4 AndAlso commandInfo(3).Trim <> String.Empty Then
            ' The file needs to be renamed
            targetFilename = commandInfo(3).Trim
        Else
            ' Use the source file name
            targetFilename = Path.GetFileName(commandInfo(0).Trim)
        End If
        fullFileName = Path.Combine(targetFolder, targetFilename)
        Dim tempFileName As String = Path.ChangeExtension(Path.Combine(targetFolder, String.Format("~mx_{0}_{1}", Path.GetFileNameWithoutExtension(fullFileName), Path.GetExtension(fullFileName).Replace(".", ""))), ".mx~")
        Dim overwrite As Boolean = False

        If commandInfo.Length >= 3 AndAlso commandInfo(2).Trim = "1" Then
            overwrite = True
        End If

        If Not overwrite AndAlso File.Exists(fullFileName) Then
            LogStatus("Error", "Target file already exists")
            SendLogInfo(SessionMessage.msg_Error, "Retrieve File Failed", "File already exists")
            Return False
        End If

        GetLease()
        If _sessionKey <> "" Then ' If the machine is online, then continue
            Try
                Dim myRetrievedFile As New LiveLinkWebService
                Dim myLiveLinkResponse As eRetailer.LiveLinkResponse
                LogStatus("Retrieving file ...", "Retrieving file from server")

                myRetrievedFile.Url = LiveLinkConfiguration.WebService_URL
                LoadProxy(myRetrievedFile)
                myLiveLinkResponse = myRetrievedFile.RetrieveFile(_liveLinkID, _sessionKey, EntityID, messageId)
                Select Case myLiveLinkResponse.ResponseCode
                    Case eRetailer.LiveLinkResponseCode.Success
                        '------------------------
                        If Not Directory.Exists(targetFolder) Then
                            Directory.CreateDirectory(targetFolder)
                        End If
                        '------------------------
                        ' Download to a temp file and then rename so that the file is not accessed before the download is complete
                        Using ms As MemoryStream = DecompressByteArray(myLiveLinkResponse.ResponseCompressedData)
                            Using fs As New FileStream(tempFileName, FileMode.Create) ' Overwrite if the file exists
                                Using bw As New BinaryWriter(fs)
                                    bw.Write(ms.ToArray)
                                End Using
                            End Using
                        End Using
                        '------------------------
                        If File.Exists(fullFileName) Then
                            ' The file needs to be overwritten
                            File.Delete(fullFileName)
                        End If
                        ' Rename the file
                        File.Move(tempFileName, fullFileName)

                        LogEvent(EventLogEntryType.Information, "File retrieval successful ... [{0}]", fullFileName)
                        Return True
                    Case Else
                        ProcessResponse(myLiveLinkResponse, True)
                End Select
            Catch ex As WebException
                LogStatus("Error", ex.Message)
                SendLogInfo(SessionMessage.msg_Error, "Retrieve Failed", ex.Message)
                LogEvent(EventLogEntryType.Error, "File retrieval failed ... {0}", ex)
                BackOffCommunications(False)
                Return False
            Catch ex As Exception
                _messageWaiting = False
                LogStatus("Error", ex.Message)
                SendLogInfo(SessionMessage.msg_Error, "Retrieve Failed", ex.Message)
                LogEvent(EventLogEntryType.Error, "File retrieve failed ... {0}", ex)
                BackOffCommunications(True)
                Return False
            End Try
            Return False
        End If

    End Function


#End Region

#Region "LiveLink Version Upgrage"

    Private Enum CompareByOptions
        FileName
        LastWriteTime
        Length
    End Enum

    Private Class CompareFileInfoEntries
        Implements IComparer

        Private compareBy As CompareByOptions = CompareByOptions.FileName


        Public Sub New(ByVal cBy As CompareByOptions)
            compareBy = cBy
        End Sub

        Public Overridable Overloads Function Compare(ByVal file1 As Object, _
               ByVal file2 As Object) As Integer Implements IComparer.Compare
            'Convert file1 and file2 to FileInfo entries
            Dim f1 As FileInfo = CType(file1, FileInfo)
            Dim f2 As FileInfo = CType(file2, FileInfo)

            'Compare the file names
            Select Case compareBy
                Case CompareByOptions.FileName
                    Return String.Compare(f1.Name, f2.Name)
                Case CompareByOptions.LastWriteTime
                    Return DateTime.Compare(f1.LastWriteTime, f2.LastWriteTime)
                Case CompareByOptions.Length
                    Return f1.Length - f2.Length
            End Select
        End Function
    End Class

    Private Sub DeleteAllFilesInDirectory(ByVal DirectoryName)
        Try
            Dim Files As String() = Directory.GetFiles(DirectoryName)
            Dim dirFile As String

            ' Delete all of the files in the directory
            For Each dirFile In Files
                ' Remove any ReadOnly attributes first
                ClearFileAttribute(Path.GetFullPath(dirFile), FileAttributes.ReadOnly Or FileAttributes.Hidden Or FileAttributes.System)
                File.Delete(Path.GetFullPath(dirFile))
            Next

            ' Now do the same for all subdirectories within this folder
            Dim Directories As String() = Directory.GetDirectories(DirectoryName)
            Dim dirDirectories As String
            For Each dirDirectories In Directories
                ' Calls itself recursively
                DeleteAllFilesInDirectory(Path.GetFullPath(dirDirectories))
                Directory.Delete(Path.GetFullPath(dirDirectories))
            Next
        Catch ex As Exception
        End Try
    End Sub

    Private Sub CopyAllFiles(ByVal SourceDir As String, ByVal TargetDir As String)
        Try
            Dim Files As String() = Directory.GetFiles(SourceDir)
            Dim dirFile As String

            If Strings.Right(SourceDir, 1) <> "\" Then
                SourceDir &= "\"
            End If
            If Strings.Right(TargetDir, 1) <> "\" Then
                TargetDir &= "\"
            End If

            ' Copy all of the files in the directory
            For Each dirFile In Files
                File.Copy(Path.GetFullPath(dirFile), TargetDir & Path.GetFileName(dirFile), True)
            Next
        Catch ex As Exception
        End Try
    End Sub


    Private Function UpgradeLiveLink(ByVal url As String, ByVal Hash As String) As Boolean
        ' default to NOT execute update (only set to true after all validation hsa been done)
        Dim executeUpdate As Boolean = False
        Dim liveLinkPath As String = Application.StartupPath

        Try
            Dim ErrorMessage As String = String.Empty

            Const cUpgradeFile As String = "LiveLinkUpgrade.zip"
            Const cLiveLinkUpdater As String = "LiveLinkUpdate.exe"

            LogStatus("Updating ...", "Updating LiveLink")
            Dim LiveLinkPath2 As String = Path.GetDirectoryName(Environment.GetCommandLineArgs(0))
            If Strings.Right(liveLinkPath, 1) <> "\" Then
                liveLinkPath &= "\"
            End If
            Dim NewVersionDirectory As String = liveLinkPath & "Upgrade\"
            Dim DownloadDirectory As String = liveLinkPath & "Download\"
            Dim BackupDirectory As String = liveLinkPath & "Backup\"
            If Not Directory.Exists(DownloadDirectory) Then
                Directory.CreateDirectory(DownloadDirectory)
            End If
            If File.Exists(DownloadDirectory & cUpgradeFile) Then
                ' Make sure the file is not readonly
                ClearFileAttribute(DownloadDirectory & cUpgradeFile, FileAttributes.ReadOnly)
                File.Delete(DownloadDirectory & cUpgradeFile)
            End If
            ' Download the update file
            LogStatus("", "Downloading new version")
            If HttpDownload.GetFile(url, DownloadDirectory & cUpgradeFile, ErrorMessage, Me.Proxy) Then
                ' TODO: Backup the existing version
                If Not Directory.Exists(BackupDirectory) Then
                    Directory.CreateDirectory(BackupDirectory)
                End If
                DeleteAllFilesInDirectory(BackupDirectory)
                CopyAllFiles(liveLinkPath, BackupDirectory)
                ' Extract the downloaded files (first delete all files from the extraction directory)
                DeleteAllFilesInDirectory(NewVersionDirectory)
                If ExtractZip(DownloadDirectory & cUpgradeFile, NewVersionDirectory, ErrorMessage) Then
                    ' Validate the extracted files
                    ' Create part 1 of the checksum by adding the file lengths together, each multiplied by their position in the sorted list
                    ' Create part 2 of the checksum by creating a concatenated list of the sorted file names
                    Dim di As New DirectoryInfo(NewVersionDirectory)
                    Dim fiArr As FileInfo() = di.GetFiles()
                    Dim fi As FileInfo
                    Dim i As Integer
                    Dim AllFileNames As String = String.Empty
                    Dim myCheckSum As Long = 0

                    fiArr.Sort(fiArr, New CompareFileInfoEntries(CompareByOptions.FileName))
                    For i = 0 To fiArr.Length - 1
                        myCheckSum += fiArr(i).Length * (i + 1)
                        AllFileNames &= Path.GetFileName(fiArr(i).FullName).Trim.ToUpper
                    Next

                    ' Now add the two parts together and calculate the SHA1 hash - this should match the checksum
                    If Polling.SHA1(myCheckSum.ToString & AllFileNames) = Hash Then
                        LogStatus("", "File validation completed")
                        ' Check for a new version of the updater and, if it exists copy to the LiveLink directory
                        Try
                            If File.Exists(NewVersionDirectory & cLiveLinkUpdater) Then
                                File.Copy(NewVersionDirectory & cLiveLinkUpdater, liveLinkPath & cLiveLinkUpdater, True)
                            End If
                        Catch ex As Exception
                        End Try

                        ' Validation completed! proceed with LiveLink update
                        executeUpdate = True
                    Else
                        LogStatus("Error", "Downloaded files could not be validated")
                    End If
                Else
                    LogStatus("Error", ErrorMessage)
                End If
            Else
                LogStatus("Error", ErrorMessage)
            End If
        Catch ex As Exception
            LogStatus("", ex.Message)
        End Try

        If (Not executeUpdate) Then
            Return False
        End If

        ' raise the update event!
        LogEvent(EventLogEntryType.Information, "LiveLink is updating ...")
        RaiseEvent VersionUpdateEvent(Me, New VersionUpdateEventArgs(url, Hash, liveLinkPath))
        ' return success
        Return True
    End Function

#End Region


#Region " Offline Cashup"


    Private Sub ProcessOfflineCashup(ByVal importConfiguration As OfflineCashupSendConfiguration)
        Try
            If (OfflineCashup.AreAnyOfflineSyncsReady(EntityID)) Then
                ' send offline cashup info
                SendOfflineCashupInfo()
            End If
        Catch ex As Exception
            LogStatus("Error", ex.Message)
            LogStatus(String.Empty, "Offlinecashup needs to be run first.")
        End Try
    End Sub

    Private Sub SendOfflineCashupInfo()
        If Is_OK_To_Send() Then
            GetLease()
            If _sessionKey <> String.Empty Then
                Try
                    For i As Integer = 1 To LiveLinkConfiguration.OfflineCashupBatchIterations
                        Dim batch As OfflineCashManagementClientToServerList = OfflineCashup.GetBatchToSend()

                        If (batch.Count = 0) Then Exit For

                        Dim mxWebService As New LiveLinkWebService
                        Dim myLiveLinkResponse As eRetailer.LiveLinkResponse
                        LogStatus("Synchronising offline cashup data...", String.Format("Sending batch {0} of {1} cashup transactions to server", i, batch.Count))

                        mxWebService.Url = LiveLinkConfiguration.WebService_URL
                        LoadProxy(mxWebService)
                        Dim serializer As XmlSerializer = New XmlSerializer(GetType(Mx.BusinessObjects.OfflineCashManagementClientToServerList))
                        Using ms As New MemoryStream
                            serializer.Serialize(ms, batch)
                            myLiveLinkResponse = mxWebService.SendOfflineCashupInfo(_liveLinkID, _sessionKey, EntityID, CompressByteArray(ms.ToArray))
                        End Using

                        Select Case myLiveLinkResponse.ResponseCode
                            Case eRetailer.LiveLinkResponseCode.Success
                                LogStatus("Synchronising offline cashup data...", "Cashup information sent")
                                OfflineCashup.MarkBatchAsSent(batch)
                            Case Else
                                ProcessResponse(myLiveLinkResponse, False)
                        End Select
                    Next
                Catch ex As Exception
                    LogStatus("Error", "Unable to send cashup data")
                    LogEvent("Unable to send cashup data" & vbCrLf & "Error: " & ex.ToString, EventLogEntryType.Error)
                End Try
            End If
        End If
    End Sub


    Public Function RetrieveOfflineCashupSettings() As Boolean

        Dim ok As Boolean = True
        ' create schema
        Try
            DatabaseIntallerService.InstallMxClientDatabaseIfNotAlreadyInstalled(Mx.BusinessObjects.MxClientApplication.OfflineCashManager)

            If (Not StoreInfoService.IsStoreSet) Then
                LogStatus("Cannot synchronise offline cashup data.", "Store must be set before offline cashup settings can be retrieved.")
                Return False
            End If
        Catch ex As Exception
            ok = False
            LogEvent(ex.Message, EventLogEntryType.Error)
        End Try

        If (Not ok) Then Return False

        UserAccess(False)
        If Is_OK_To_Send() Then
            GetLease()
            If _sessionKey <> "" Then ' If the machine is online, then continue
                Try
                    Dim myMessage As New LiveLinkWebService
                    Dim myLiveLinkResponse As eRetailer.LiveLinkResponse
                    LogStatus("Synchronising offline cashup data...", "Retrieving offline cashup settings from server")

                    myMessage.Url = LiveLinkConfiguration.WebService_URL
                    LoadProxy(myMessage)
                    myLiveLinkResponse = myMessage.GetOfflineCashupSettings(_liveLinkID, _sessionKey, EntityID, DateTime.Now)

                    Select Case myLiveLinkResponse.ResponseCode
                        Case eRetailer.LiveLinkResponseCode.Success
                            '---------------------------------
                            Dim offlineCashupInfo As Mx.BusinessObjects.OfflineCashManagementServerToClient
                            ' Decompress and Deserialise the offline cashup business object
                            Using ms As MemoryStream = DecompressByteArray(myLiveLinkResponse.ResponseCompressedData)
                                Dim serializer As XmlSerializer = New XmlSerializer(GetType(Mx.BusinessObjects.OfflineCashManagementServerToClient))
                                ms.Position = 0
                                offlineCashupInfo = CType(serializer.Deserialize(ms), Mx.BusinessObjects.OfflineCashManagementServerToClient)
                            End Using
                            LogStatus("Synchronising offline cashup data...", "Saving offline cashup settings")
                            If offlineCashupInfo Is Nothing Then
                                LogStatus("Synchronising offline cashup data...", "No data found")
                            ElseIf OfflineCashup.ProcessOfflineSettingsFromServer(offlineCashupInfo) Then
                                ' need to track if settings are downloaded - can't do anything else until they are complete.
                                _offlineCashupSetupSettingsRetrieved = True
                                LogStatus("Synchronising offline cashup data...", "Offline cashup update completed")
                            Else
                                LogStatus("Error synchronising offline cashup settings...", "Offline cashup synchronisation did NOT complete. This may be because the Offline Cash Manager has not been run yet.")
                            End If
                            Return True
                        Case Else
                            ProcessResponse(myLiveLinkResponse, True)
                    End Select
                Catch ex As WebException
                    LogStatus("Error", ex.Message)
                    BackOffCommunications(False)
                    Return False
                Catch ex As Exception
                    _messageWaiting = False
                    LogStatus("Error", ex.Message)
                    BackOffCommunications(True)
                    Return False
                End Try
            End If
        End If
        Return False
    End Function

    Private Function RetrieveOfflineCashupInfo(ByVal offlineSyncID As Integer) As Boolean

        If (Not _offlineCashupSetupSettingsRetrieved) Then
            If Not (RetrieveOfflineCashupSettings()) Then
                LogStatus("Cannot synchronise offline cashup data.", "Could not retrieve offline cashup settings.")
                Return False
            End If
        End If

        UserAccess(False)
        If Is_OK_To_Send() Then
            GetLease()
            If _sessionKey <> "" Then ' If the machine is online, then continue
                Try
                    Dim myMessage As New LiveLinkWebService
                    Dim myLiveLinkResponse As eRetailer.LiveLinkResponse
                    LogStatus("Synchronising offline cashup data...", "Retrieving offline cashup info from server")

                    myMessage.Url = LiveLinkConfiguration.WebService_URL
                    LoadProxy(myMessage)
                    myLiveLinkResponse = myMessage.GetOfflineCashupInfoByID(_liveLinkID, _sessionKey, EntityID, DateTime.Now, offlineSyncID)

                    Select Case myLiveLinkResponse.ResponseCode
                        Case eRetailer.LiveLinkResponseCode.Success
                            Dim offlineCashupInfo As Mx.BusinessObjects.OfflineCashManagementServerToClient
                            ' Decompress and Deserialise the offline cashup business object
                            Using ms As MemoryStream = DecompressByteArray(myLiveLinkResponse.ResponseCompressedData)
                                Dim serializer As XmlSerializer = New XmlSerializer(GetType(Mx.BusinessObjects.OfflineCashManagementServerToClient))
                                ms.Position = 0
                                offlineCashupInfo = CType(serializer.Deserialize(ms), Mx.BusinessObjects.OfflineCashManagementServerToClient)
                            End Using
                            LogStatus("Synchronising offline cashup data...", "Saving offline cashup data")
                            If offlineCashupInfo Is Nothing Then
                                LogStatus("Synchronising offline cashup data...", "No data found")
                                Return False
                            ElseIf OfflineCashup.ProcessOfflineCashupFromServer(offlineCashupInfo) Then
                                LogStatus("Synchronising offline cashup data...", "Offline cashup update completed")
                            Else
                                LogStatus("Error synchronising offline cashup data...", "Offline cashup synchronisation did NOT complete")
                                Return False
                            End If
                            Return True
                        Case Else
                            ProcessResponse(myLiveLinkResponse, True)
                    End Select
                Catch ex As WebException
                    LogStatus("Error", ex.Message)
                    BackOffCommunications(False)
                    Return False
                Catch ex As Exception
                    _messageWaiting = False
                    LogStatus("Error", ex.Message)
                    BackOffCommunications(True)
                    Return False
                End Try
            End If
        End If
        Return False
    End Function


#End Region

#Region " PAR Gateway Interface "

    Private Sub ProcessPar(ByVal importConfiguration As PARExaltConfiguration)
        Try
            If Not importConfiguration.Start(EntityID, _sessionKey, _liveLinkID) Then
                ' if the TLD Server listener is not being used or has already been started... do an import
                If importConfiguration.UseTLDServer Then
                    If importConfiguration.TLDServer IsNot Nothing Then
                        ' check the health of the TLD server
                        ' will initialise a recovery if necessary
                        importConfiguration.TLDServer.CheckStatus()
                    End If
                Else
                    Dim performFullImport As Boolean = False

                    If importConfiguration.CurrentDay <> DateTime.Now.Date Then
                        performFullImport = True
                        importConfiguration.CurrentDay = DateTime.Now.Date
                    End If

                    ' do an import using the standard GetTLD
                    ParGetTld(importConfiguration, performFullImport)
                    ParImportFile(importConfiguration, performFullImport)
                End If
            End If
        Catch ex As Runtime.InteropServices.COMException
            LogEvent(EventLogEntryType.Information, "PARTLD: PARTLD server has been restarted, reinitialising the listener.")
            importConfiguration.TLDServer = Nothing
        End Try
    End Sub

    Private Sub ParGetTld(ByVal importConfiguration As PARExaltConfiguration, ByVal FullImport As Boolean)
        Dim Result As Integer
        Dim GetTLD As String
        Try
            SetStatus("Get PAR Data...")
            If FullImport Then
                GetTLD = LiveLinkConfiguration.PARGetAllTLDsExe
                Result = Shell(GetTLD, Style:=AppWinStyle.MinimizedNoFocus, Wait:=True)
                importConfiguration.SetStatusMessage("Get all TLDs executed.")
            Else
                GetTLD = LiveLinkConfiguration.PARGetAllTLDsExe
                Result = Shell(GetTLD, Style:=AppWinStyle.MinimizedNoFocus, Wait:=True)
                importConfiguration.SetStatusMessage("GetTLD executed.")
            End If
            If Result <> 0 Then
                importConfiguration.SetStatusMessage("GetTLD failed. Result code: " & Result)
                LogEvent("Error running GetTLD." & vbCrLf & "Executable: " & GetTLD & vbCrLf & "Result Code: " & Result, EventLogEntryType.Error)
            End If
            ' Now pause for 5 seconds.  It appears as if shell returns even with Wait=True before the file has been fully released
            Threading.Thread.Sleep(5000)
        Catch ex As Exception
            LogStatus("Error...", "Unable to execute GetTLD")
            LogStatus("", "Error: " & ex.Message)
            importConfiguration.SetStatusMessage("GetTLD failed. Error: " & ex.Message)
            LogEvent("GetTLD execution failed." & vbCrLf & "Executable: " & GetTLD & vbCrLf & "Error: " & ex.Message, EventLogEntryType.Error)
        Finally
            SetStatus("")
        End Try
    End Sub

    Private Sub ParImportFile(ByVal importConfiguration As PARExaltConfiguration, ByVal FullImport As Boolean)
        SetStatus("Process PAR...")
        importConfiguration.ImportDateStart = DateTime.Now
        importConfiguration.ImportedTotalCount = 0
        importConfiguration.SkippedTotalCount = 0

        If FullImport OrElse importConfiguration.LastPoll.Date <> DateTime.Now.Date Then
            Dim CurrentDayOfWeek As Integer = DateTime.Now.DayOfWeek
            For i As Integer = 0 To 6
                ParImportFileSpecific(importConfiguration, (i + CurrentDayOfWeek + 1) Mod 7)
            Next
        Else
            ParImportFileSpecific(importConfiguration, DateTime.Now.DayOfWeek)
        End If

        SetStatus("")
    End Sub

    Private Sub ParImportFileSpecific(ByVal importConfiguration As PARExaltConfiguration, ByVal FileNumber As Integer)
        Try
            Dim strFileName As String = LiveLinkConfiguration.PARImportFilePath & "TLD." & FileNumber

            If Not File.Exists(strFileName) Then
                importConfiguration.SetStatusMessage("Import file " & Path.GetFileName(strFileName) & " does not exist.")
                LogStatus("Error...", "Import file " & Path.GetFileName(strFileName) & " does not exist.")
            Else
                importConfiguration.SetStatusMessage("Opening file...")
                importConfiguration.ImportedCount = 0
                importConfiguration.SkippedCount = 0

                importConfiguration.ImportFile = New ParFile(LiveLinkConfiguration.ConnectionString)
                If importConfiguration.ImportFile.Import(strFileName) = False Then
                    LogStatus("Error...", "Error importing file TLD." & FileNumber.ToString)
                End If
                importConfiguration.ImportFile = Nothing

                ' Archive file.
                ArchiveFile(strFileName, ArchiveFolderStyle.Archive_yyyy_MM_dd, ArchiveFileNameStyle.orig_A_yyyyMMdd_HHmmss)

                importConfiguration.SetStatusMessage("Finished. " & importConfiguration.ImportedTotalCount & " records in " & DateTime.Now.Subtract(importConfiguration.ImportDateStart).TotalSeconds & " seconds. " & vbCrLf & importConfiguration.SkippedCount & " records skipped.")
                LogStatus("", "TLD." & FileNumber.ToString & " imported.  Records: " & importConfiguration.ImportedCount.ToString & ", skipped: " & importConfiguration.SkippedCount)
            End If

        Catch ex As Exception
            LogEvent("Error during ParImportFileSpecific (TLD." & FileNumber.ToString & ")" & vbCrLf & "Error: " & ex.Message, EventLogEntryType.Error)
        End Try

    End Sub


    Private Sub ParImport(ByVal importConfiguration As PARExaltConfiguration, ByVal FullImport As Boolean, Optional ByVal InitiateRecovery As Boolean = False)
        SetStatus("Process PAR...")
        importConfiguration.ImportDateStart = DateTime.Now
        importConfiguration.ImportedTotalCount = 0
        importConfiguration.SkippedTotalCount = 0

        If FullImport OrElse importConfiguration.LastPoll.Date <> DateTime.Now.Date Then
            ' reset the import count for the new day, or for full import
            importConfiguration.ImportedCount = 0
            importConfiguration.TLDDayServer = New ParTLDDayServer(LiveLinkConfiguration.POSType, EntityID, LiveLinkConfiguration.ConnectionString, LiveLinkConfiguration.PARTLDNumberOfDaysToRecover, LiveLinkConfiguration.DontProcessBefore)
        Else
            ' recover the last 3 days by default
            importConfiguration.TLDDayServer = New ParTLDDayServer(LiveLinkConfiguration.POSType, EntityID, LiveLinkConfiguration.ConnectionString, 3, LiveLinkConfiguration.DontProcessBefore)
        End If
        importConfiguration.TLDDayServer.Import(InitiateRecovery)
        SetStatus("")
    End Sub

#End Region

#Region " Aloha Gateway Interface "


    Private Sub StripIndexBit(ByVal FileName As String)
        Try
            Dim i As Integer
            Dim fs As FileStream
            Dim Retry As Boolean
            For i = 1 To 3 ' Attempt to open the file exclusively 3 times, waiting 5 seconds after each failed attempt
                Try
                    Retry = False
                    fs = File.Open(FileName, FileMode.Open, FileAccess.Write, FileShare.None)
                    fs.Seek(28, SeekOrigin.Begin) ' The 28th byte contains a 0 if no corresponding index file exists or 1 if one does
                    fs.WriteByte(0) ' Disable index requirement
                Catch ex As Exception
                    Retry = True
                Finally
                    Try
                        fs.Close()
                    Catch ex As Exception
                        Retry = True
                    End Try
                End Try
                If Not Retry Then
                    Exit Sub ' Break here if the update was successful
                End If
                Threading.Thread.Sleep(5000)
            Next i
        Catch ex As Exception
        End Try
    End Sub

    Private Sub RemoveIndexBitFromDBFs(ByVal Folder As String)
        ' Certain errors are occurring on site that cannot be reproduced in the dev environment.
        ' The "Index Not Found" error on the TDR.DBF file is one such error and this routine is a workaround.
        ' It Changes Byte 28 of the DBF file (part of the DBF header) to indicate that an index is not required.
        Try
            StripIndexBit(Path.Combine(Folder, "TDR.DBF"))
            StripIndexBit(Path.Combine(Folder, "RSN.DBF"))
            StripIndexBit(Path.Combine(Folder, "GNDBREAK.DBF"))
            StripIndexBit(Path.Combine(Folder, "EMP.DBF"))
            StripIndexBit(Path.Combine(Folder, "ITM.DBF")) ' Not sure about this one
            StripIndexBit(Path.Combine(Folder, "CMP.DBF"))
            StripIndexBit(Path.Combine(Folder, "PRO.DBF"))
            StripIndexBit(Path.Combine(Folder, "ADJTIME.DBF"))
            StripIndexBit(Path.Combine(Folder, "GNDITEM.DBF"))
            StripIndexBit(Path.Combine(Folder, "CAT.DBF"))
            StripIndexBit(Path.Combine(Folder, "GNDDEPST.DBF"))
        Catch ex As Exception
        End Try
    End Sub

    Private Sub ProcessAloha(ByVal importConfiguration As AlohaConfiguration)
        Try
            Dim sConnect_OleDB As String = LiveLinkConfiguration.OleDB_DB_Connect
            Dim sConnect_OleDB_Template As String = sConnect_OleDB
            Dim sCurrentDOB As String = String.Empty
            Dim sCurrentCheck As String = String.Empty
            Dim sPOSType As String = LiveLinkConfiguration.POSType
            Dim sFieldListInsert As String = GettbSalesMainFieldsForInsert(sPOSType)

            AlohaV1.GetDoneFromtbStoreInfo(sCurrentDOB, sCurrentCheck)

            If sCurrentDOB <> String.Empty Then
                Dim theDate As DateTime = Now
                ' Determine how many days back to process with a maximum
                Dim DaysToProcess As Integer = Max(Min(DateDiff(DateInterval.Day, CDate(sCurrentDOB).Date, theDate.Date), importConfiguration.PreviousDaysToCheck), importConfiguration.PreviousDaysToIncludeInProcess)
                Dim i As Integer
                Dim GrindFolder As String

                'First perform the Grind on the current day's data
                If (Not importConfiguration.SkipGrind) Then
                    SetStatus("Grinding ...")
                    AlohaGrind(importConfiguration)
                End If

                SetStatus("Importing ...")
                'ShowDebug("Processing " & DaysToProcess & " days.", DebugModes.Verbose)
                For i = 0 To DaysToProcess
                    'Start with the oldest day first
                    If i = DaysToProcess Then
                        GrindFolder = "DATA"
                    Else
                        GrindFolder = Format(DateAdd(DateInterval.Day, i - DaysToProcess, theDate), "yyyyMMdd")
                    End If
                    If Not ((GrindFolder = "DATA") And importConfiguration.SkipDataDirectory) Then
                        sConnect_OleDB = sConnect_OleDB_Template.Replace("[DataDir]", GrindFolder)
                        importConfiguration.ImportFilePath = importConfiguration.ImportFilePathTemplate.Replace("[DataDir]", GrindFolder)
                        If Directory.Exists(importConfiguration.ImportFilePath) Then
                            LogStatus("", "Processing Grind data [" & GrindFolder & "]")
                            importConfiguration.SetStatusMessage("Importing Grind information (" & GrindFolder & ")")
                            RemoveIndexBitFromDBFs(importConfiguration.ImportFilePath)
                            Application.DoEvents()
                            If importConfiguration.Version = "VER2" Then
                                ' dispose of the previous class instance if it is not nothing
                                If importConfiguration.Import IsNot Nothing Then
                                    importConfiguration.Import.Dispose()
                                    importConfiguration.Import = Nothing
                                End If
                                ' create new instance of the aloha interface
                                importConfiguration.Import = New AlohaV2(sConnect_OleDB, EntityID, sFieldListInsert, importConfiguration.ServiceType, importConfiguration.QuickServiceSOS, importConfiguration.TimeAttendanceEnabled, importConfiguration.ServiceTypeMapping, importConfiguration.ImportFilePath)
                                importConfiguration.Import.Process()
                                importConfiguration.Import.Dispose()
                                importConfiguration.Import = Nothing

                                If CancelFlag Then
                                    LogStatus("", String.Format("User cancelled processing ({0})", GrindFolder))
                                    Exit For
                                End If
                            Else
                                Dim import As AlohaV1 = New AlohaV1()
                                If Not import.AlohaImportFile(sConnect_OleDB, EntityID, sFieldListInsert, importConfiguration.ServiceType, importConfiguration.QuickServiceSOS) Then
                                    ' If an error occurred while processing the file, do not 
                                    ' process any more files as this will cause any outstanding transactions 
                                    ' in the error file to be skipped.
                                    If Not CancelFlag Then
                                        LogStatus("Error", "An error occurred while processing (" & GrindFolder & ")")
                                    Else
                                        LogStatus("", "User cancelled processing (" & GrindFolder & ")")
                                    End If
                                    Exit For
                                End If
                            End If
                        End If
                    End If
                Next
            Else
                LogStatus("Error", "Unable to retrieve latest processed date")
            End If
        Catch ex As Exception
            LogStatus("Error", ex.Message)
        Finally
            importConfiguration.SetStatusMessage("")
            SetStatus("")
        End Try
    End Sub

    Private Sub AlohaGrind(ByVal importConfiguration As AlohaConfiguration, Optional ByVal GrindFolder As String = "DATA")
        Dim GrindProcessId As Integer
        Dim GrindProcesses() As Process
        Dim GrindProcess As Process
        Dim GrindStartTime As DateTime
        Try
            SetStatus("Get Aloha Data ...")
            ' Check to see whether the Grind process is already running and wait a specified amount of time 
            ' for it to complete.  If it is still running after this time.
            importConfiguration.SetStatusMessage("Waiting for Grind process to complete")
            GrindStartTime = Now
            GrindProcesses = Process.GetProcessesByName(Path.GetFileNameWithoutExtension(importConfiguration.GrindExe))
            While (GrindProcesses.Length > 0) And (DateDiff(DateInterval.Second, GrindStartTime, Now) < GrindProcessTimeout)
                Threading.Thread.Sleep(100) ' Prevents LiveLink from consuming 100% CPU while waiting
                GrindProcesses = Process.GetProcessesByName(Path.GetFileNameWithoutExtension(importConfiguration.GrindExe))
            End While
            If GrindProcesses.Length <> 0 Then ' there must be blocked grind Processes (eg Error Dialog box waiting to be cleared)
                LogStatus("", "Terminating locked Grind processes")
                importConfiguration.SetStatusMessage("Terminating locked Grind processes")
                For Each GrindProcess In GrindProcesses
                    Try
                        If GrindProcess.Id > 0 Then ' The DOt Net process bug may cause process arrays with dummy processes to be returned.
                            GrindProcess.Kill() ' ignore any errors in case these processes have terminated in the meantime
                        End If
                    Catch ex As Exception
                    End Try
                Next
            End If
            Try
                importConfiguration.SetStatusMessage("Grinding " & GrindFolder)
                GrindStartTime = Now
                GrindProcessId = Shell("""" & importConfiguration.GrindExe & """ /DATE " & GrindFolder & " /NoWindow", AppWinStyle.MinimizedFocus, True, GrindProcessTimeout * 1000)
                If GrindProcessId <> 0 Then
                    LogStatus("Error", "The Grind process did not automatically terminate")
                    Try
                        GrindProcess.Kill()
                    Catch ex As Exception
                    End Try
                Else
                    LogStatus("", "Grind process completed")
                    importConfiguration.SetStatusMessage("Grind process completed")
                End If
            Catch ex As Exception
                LogStatus("Error", "Unable to Grind data - " & ex.Message)
            End Try

        Catch ex As Exception
            LogStatus("Error", ex.Message)
        Finally
            SetStatus("")
        End Try
    End Sub

#End Region

#Region " Micros Gateway Interface "

    Private Sub ProcessMicros(ByVal importConfiguration As MicrosConfiguration, Optional ByVal force As Boolean = False)
        Try
            Dim MyMicrosPreviousDaysToCheck As Integer = Max(Min(LiveLinkConfiguration.MicrosPreviousDaysToCheck, 14), 1)
            Dim MicrosDontProcessBefore As String = LiveLinkConfiguration.MicrosDontProcessBefore
            Dim StartDate As DateTime
            Dim CurrentProcessingDate As DateTime
            Dim EndDate As DateTime
            Dim sConnect_OleDB As String = LiveLinkConfiguration.OleDB_DB_Connect
            Dim sConnect_OleDB_Template As String = sConnect_OleDB
            Dim MicrosImportFilePath As String = LiveLinkConfiguration.MicrosImportFilePath
            Dim MicrosImportFilePath_Template As String = MicrosImportFilePath
            Dim sCurrentDOB As String
            Dim sCurrentCheck As String
            Dim sPOSType As String = LiveLinkConfiguration.POSType
            Dim sFieldListInsert As String = GettbSalesMainFieldsForInsert(sPOSType)
            Dim ChecksProcessed As Integer
            Dim TimeRecordsProcessed As Integer = 0
            Dim TotalChecksProcessed As Integer = 0
            Dim TotalTimeRecordsProcessed As Integer = 0
            Dim NoSaleRecordsProcessed As Integer = 0
            Dim TotalNoSaleRecordsProcessed As Integer = 0

            If Not IsDate(MicrosDontProcessBefore) Then
                MicrosDontProcessBefore = Format(DateTime.MinValue.Date, "dd-MMM-yyyy")
            End If
            EndDate = GetLatestMicrosClosedCheck.Date
            If (Now.Day <> importConfiguration.LastPoll.Day) Or force Then
                ' If LiveLink has just started, or the day has rolled over, go back and check the past x days
                ' for any old transactions that have just been closed
                LogStatus("", "Checking previous days for missing checks")
                StartDate = DateAdd(DateInterval.Day, -importConfiguration.PreviousDaysToCheck, EndDate)
            Else
                StartDate = EndDate
            End If
            ' Now make sure that we don't process any data before the specified date in the config file
            If StartDate < CDate(importConfiguration.DoNotProcessBefore) Then
                StartDate = CDate(importConfiguration.DoNotProcessBefore)
            End If
            CurrentProcessingDate = StartDate
            While CurrentProcessingDate <= EndDate
                LogStatus("", "Importing Micros data [" & Format(CurrentProcessingDate, "yyyy-MM-dd") & "]")
                If Not MicrosImportData(CurrentProcessingDate, EntityID, LiveLinkConfiguration.MicrosGatewayVersion, ChecksProcessed, TimeRecordsProcessed, NoSaleRecordsProcessed, LiveLinkConfiguration.MicrosUseCheckDetailTax) Then
                    If Not CancelFlag Then
                        LogStatus("Error", "An error occurred while importing")
                    Else
                        LogStatus("", "User cancelled processing")
                    End If
                    LogStatus("", "Number of checks processed: " & ChecksProcessed)
                    If TimeRecordsProcessed > 0 Then
                        LogStatus("", "Time/attendance processed: " & TimeRecordsProcessed)
                        TotalTimeRecordsProcessed += TimeRecordsProcessed
                    ElseIf TimeRecordsProcessed < 0 Then
                        LogStatus("Error", "Error processing time/attendance data")
                    End If
                    TotalChecksProcessed += ChecksProcessed
                    Exit While
                End If
                LogStatus("", "Number of checks processed: " & ChecksProcessed)
                If TimeRecordsProcessed > 0 Then
                    LogStatus("", "Time/attendance processed: " & TimeRecordsProcessed)
                    TotalTimeRecordsProcessed += TimeRecordsProcessed
                ElseIf TimeRecordsProcessed < 0 Then
                    LogStatus("Error", "Error processing time/attendance data")
                End If
                If NoSaleRecordsProcessed > 0 Then
                    LogStatus("", "NoSales processed: " & NoSaleRecordsProcessed)
                    TotalNoSaleRecordsProcessed += NoSaleRecordsProcessed
                ElseIf NoSaleRecordsProcessed < 0 Then
                    LogStatus("Error", "Error processing NoSale data")
                End If
                CurrentProcessingDate = DateAdd(DateInterval.Day, 1, CurrentProcessingDate)
                TotalChecksProcessed += ChecksProcessed
            End While
            LogStatus("", "Importing Micros data complete [" & TotalChecksProcessed & " check(s)]")
            If TotalTimeRecordsProcessed > 0 Then
                LogStatus("", "Importing T/A data complete [" & TotalTimeRecordsProcessed & " record(s)]")
            End If
            If TotalNoSaleRecordsProcessed > 0 Then
                LogStatus("", "Importing NoSale data complete [" & TotalNoSaleRecordsProcessed & " record(s)]")
            End If
        Catch ex As Exception
            LogStatus("Error", ex.Message)
        Finally
            SetStatus("")
        End Try

    End Sub

    ' select which version of micros to use for importing data
    Private Function MicrosImportData(ByVal TradingDate As Date, ByVal EntityId As Integer, ByVal MicrosGatewayVersion As String, ByRef ChecksProcessed As Integer, ByRef TimeRecordsProcessed As Integer, ByRef NoSaleRecordsProcessed As Integer, ByVal MicrosUseCheckDetailTax As Boolean) As Boolean
        Dim sPOSType As String = LiveLinkConfiguration.POSType

        Try
            Select Case sPOSType.ToUpper
                Case "MICROS"
                    Return Micros3700.Micros3700_ImportData(TradingDate, EntityId, MicrosGatewayVersion, ChecksProcessed, TimeRecordsProcessed, NoSaleRecordsProcessed, MicrosUseCheckDetailTax, LiveLinkConfiguration.MicrosRevenueCentreFilter)
                Case "MICROS9700"
                    Return Micros9700.Micros9700_ImportData(TradingDate, EntityId, MicrosGatewayVersion, ChecksProcessed, TimeRecordsProcessed, MicrosUseCheckDetailTax)
                Case Else
                    Return False
            End Select
        Catch ex As Exception
        End Try
    End Function

    ' select which version of micros to use
    Private Function GetLatestMicrosClosedCheck() As Date
        Dim sPOSType As String = LiveLinkConfiguration.POSType

        Try
            Select Case sPOSType.ToUpper
                Case "MICROS"
                    Return Micros3700.Micros3700_GetLatestClosedCheck(LiveLinkConfiguration.MicrosRevenueCentreFilter)
                Case "MICROS9700"
                    Return Micros9700.Micros9700_GetLatestClosedCheck()
            End Select
        Catch ex As Exception
        End Try
        Return DateTime.MinValue
    End Function

#End Region

#Region " POSI Gateway Interface "

    Private Sub ProcessPosi(ByVal importConfiguration As POSIConfiguration)
        Dim dateStart As DateTime = DateTime.Now
        importConfiguration.ImportDatabase = New PosiDatabase(LiveLinkConfiguration.ConnectionString)
        importConfiguration.ImportDatabase.Import(LiveLinkConfiguration.POSIConnectionString)
        importConfiguration.ImportDatabase = Nothing
        importConfiguration.SetStatusMessage("Finished. " & importConfiguration.ImportedCount & " records imported from " & Path.GetFileName(LiveLinkConfiguration.POSIConnectionString) & " in " & DateTime.Now.Subtract(dateStart).TotalSeconds & " seconds. " & vbCrLf & importConfiguration.SkippedCount & " records skipped.")
    End Sub


#End Region

#Region " Compris Gateway Interface "

    Private Sub ProcessCompris(ByVal importConfiguration As ComprisConfiguration)
        Try
            SetStatus("Processing...")
            importConfiguration.Import = (New ComprisLoader(LiveLinkConfiguration.ComprisVariantType)).LoadPos(LiveLinkConfiguration.ConnectionString, LiveLinkConfiguration.ComprisXMLFilePath, LiveLinkConfiguration.DaysBeforeObsolete)
            importConfiguration.Import.ImportXmlData()
            importConfiguration.Import.Dispose()
        Catch ex As Exception
            LogEvent(EventLogEntryType.Error, "Exception: [ProcessCompris]  {0}{1}{1}{2}", ex.Message, vbCrLf, ex.StackTrace)
            LogStatus("Error", ex.Message)
        Finally
            SetStatus("")
        End Try
    End Sub

    Private Sub ProcessComprisPDP(ByVal importConfiguration As ComprisPDPConfiguration)
        Try
            SetStatus("Processing PDP...")
            LogStatus("", "Retrieving PDP totals")

            ' create a new DCR report and get the totals
            importConfiguration.Import = New MxCompris.Reports.DCR(EntityID)
            importConfiguration.Import.GetTotals()
            importConfiguration.Import = Nothing

        Catch ex As Exception
            LogEvent(EventLogEntryType.Error, "Exception: [ProcessComprisPDP]  {0}{1}{1}{2}", ex.Message, vbCrLf, ex.StackTrace)
            LogStatus("Error", ex.Message)
        Finally
            SetStatus("")
        End Try
    End Sub

    ' callback function to display status message
    Public Sub LogStatusUpdate(ByVal status As String)
        LogStatus("", status)
    End Sub

    Private Sub ProcessComprisPDPUpdates(ByVal importConfiguration As ComprisPDPUpdatesConfiguration)
        Try
            SetStatus("PDP Updates...")
            ' get a lease prior to sending updates (the session key and identity are required for communications)
            GetLease()
            ' create a new DCR report and get the totals
            MxCompris.Updates.Helper.Update(_liveLinkID, _sessionKey, EntityID, AddressOf LogStatusUpdate)

        Catch ex As Exception
            LogEvent(EventLogEntryType.Error, "Exception: [ProcessComprisPDPUpdates]  {0}{1}{1}{2}", ex.Message, vbCrLf, ex.StackTrace)
            LogStatus("Error", ex.Message)
        Finally
            SetStatus("")
        End Try
    End Sub

    Private Sub ProcessComprisPollFiles(ByVal importConfiguration As ComprisPollFilesConfiguration)
        Try
            SetStatus("Poll Files...")
            ' get a lease prior to sending updates (the session key and identity are required for communications)
            GetLease()
            ' process any poll files, COGS or Transfers.
            MxCompris.Reports.PollFiles.Process(_liveLinkID, _sessionKey, EntityID, MxCompris.Reports.PollFileType.COGS)
            MxCompris.Reports.PollFiles.Process(_liveLinkID, _sessionKey, EntityID, MxCompris.Reports.PollFileType.Transfers)

        Catch ex As Exception
            LogEvent(EventLogEntryType.Error, "Exception: [ProcessComprisPollFiles]  {0}{1}{1}{2}", ex.Message, vbCrLf, ex.StackTrace)
            LogStatus("Error", ex.Message)
        Finally
            SetStatus("")
        End Try
    End Sub


#End Region

#Region " Panasonic Gateway Interface"

    Private Sub ProcessPanasonic(ByVal importConfiguration As PanasonicConfiguration)
        Try
            SetStatus("Processing...")
            PAN7700.PAN7700_ImportData()
        Catch ex As Exception
            LogStatus("Error", ex.Message)
        Finally
            SetStatus("")
        End Try
    End Sub


#End Region

#Region " Intouch Gateway Interface"

    Private Sub ProcessIntouch(ByVal importConfiguration As IntouchConfiguration)
        Try
            ' show a message in livelink, showing it is currently processing
            SetStatus("Processing...")

            ' Instantiate the Intouch interface class
            importConfiguration.Import = New Intouch(LiveLinkConfiguration.ConnectionString, _
                                    LiveLinkConfiguration.IntouchExportExe, _
                                    LiveLinkConfiguration.IntouchExportFilePath, _
                                    EntityID, _
                                    LiveLinkConfiguration.IntouchServiceTypeMappings, _
                                    LiveLinkConfiguration.IntouchAutomatedExport, _
                                    LiveLinkConfiguration.IntouchDrawerPullsEnabled, _
                                    LiveLinkConfiguration.IntouchItemCodeField, _
                                    LiveLinkConfiguration.IntouchItemDescriptionField, _
                                    LiveLinkConfiguration.IntouchModifierCodeField, _
                                    LiveLinkConfiguration.IntouchModifierDescriptionField)
            ' Process intouch
            importConfiguration.Import.Process()
            importConfiguration.LatestExport = importConfiguration.Import.LatestInTouchExport
            importConfiguration.Import = Nothing
        Catch ex As Exception
            LogStatus("Error", ex.Message)
        Finally
            SetStatus("")
        End Try
    End Sub

#End Region

#Region " Intouch menu package Gateway Interface"

    Private Sub ProcessIntouchMenuPackage(ByVal importConfiguration As IntouchMenuPackageConfiguration)
        Try
            ' show a message in livelink, showing it is currently processing
            SetStatus("Processing...")

            ' Instantiate the Intouch interface class
            importConfiguration.Import = New IntouchMenuPackage()
            ' Process intouch
            importConfiguration.Import.ProcessOutputFile()
            importConfiguration.Import = Nothing
        Catch ex As Exception
            LogStatus("Error", ex.Message)
        Finally
            SetStatus("")
        End Try
    End Sub

#End Region

#Region " Octane POS"


    Private Sub ProcessOctane(ByVal importConfiguration As OctaneConfiguration)
        Try
            ' show a message in livelink, showing it is currently processing
            SetStatus("Processing...")

            ' Instantiate the Octane interface class if it hasn't been done already
            If importConfiguration.Import Is Nothing Then
                importConfiguration.Import = New Octane(LiveLinkConfiguration.ConnectionString, _
                                    LiveLinkConfiguration.OctaneExportFilePath, _
                                    LiveLinkConfiguration.OctaneWorkingFilePath, _
                                    EntityID, _
                                    LiveLinkConfiguration.DaysBeforeObsolete)
            End If

            ' Process Octane
            importConfiguration.Import.Process()
        Catch ex As Exception
            LogStatus("Error", ex.ToString)
        Finally
            SetStatus("")
        End Try
    End Sub

#End Region

#Region " ICG Gateway"


    Private Sub ProcessICG(ByVal importConfiguration As ICGConfiguration)
        Try
            ' show a message in livelink, showing it is currently processing
            SetStatus("Processing...")
            ' Instantiate the ICG interface class if it hasn't been done already
            If importConfiguration.Import Is Nothing Then
                importConfiguration.Import = New ICG.ICGProcess(EntityID)
            End If
            ' Process ICG
            importConfiguration.Import.Process()
        Catch ex As Exception
            LogStatus("Error", ex.ToString)
        Finally
            SetStatus("")
        End Try
    End Sub


#End Region

#Region " Microsoft RMS Gateway"


    Private Sub ProcessRMS(ByVal importConfiguration As RMSConfiguration)
        Try
            ' show a message in livelink, showing it is currently processing
            SetStatus("Processing...")

            ' Instantiate the RMS interface class if it hasn't been done already
            If importConfiguration.Import Is Nothing Then
                importConfiguration.Import = New MicrosoftRMS.RMS(CLng(EntityID))
            End If

            ' Process RMS
            importConfiguration.Import.Process()
        Catch ex As Exception
            LogStatus("Error", ex.ToString)
        Finally
            SetStatus("")
        End Try
    End Sub


#End Region

#Region " NewPOS Gateway"


    Private Sub ProcessNewPOS(ByVal importConfiguration As NewPOSConfiguration, Optional ByVal Force As Boolean = False)
        Try
            ' show a message in livelink, showing it is currently processing
            SetStatus("Processing...")

            ' Instantiate the NewPOS interface class if it hasn't been done already
            If importConfiguration.Import Is Nothing Then
                importConfiguration.Import = New Mx.NewPOS.NewPOS(CInt(EntityID), LiveLinkConfiguration.ConnectionString, _
                                      LiveLinkConfiguration.NewPOSExportFilePath, LiveLinkConfiguration.NewPOSRegisterListFile, _
                                      LiveLinkConfiguration.NewPOSPipeName, LiveLinkConfiguration.NewPOSTableVersion)
            End If

            Dim queryDate As DateTime
            If (Force OrElse importConfiguration.FirstPollComplete = False) Then
                queryDate = DateTime.Now.AddDays(-LiveLinkConfiguration.NewPOSDaysToRecover).Date
            Else
                queryDate = importConfiguration.Import.GetLastQueryDate()
                ' if failed to get a query date, then recover previous days
                If queryDate = DateTime.MinValue Then
                    queryDate = DateTime.Now.AddDays(-LiveLinkConfiguration.NewPOSDaysToRecover).Date
                End If
            End If

            ' process up until current day
            While (queryDate <= DateTime.Now.Date)
                Application.DoEvents()
                ' Process NewPOS
                If importConfiguration.Import.Process(queryDate) Then
                    ' if process was successfull, update the last query date value
                    importConfiguration.Import.SetLastQueryDate(queryDate)
                End If
                ' attemp to query the next business day 
                queryDate = queryDate.AddDays(1)
            End While
        Catch ex As Exception
            LogStatus("Error", ex.ToString)
        Finally
            SetStatus("")
        End Try
    End Sub


#End Region

#Region "iPOS Gateway"


    Private Sub ProcessiPOS(ByVal importConfiguration As iPOSConfiguration, Optional ByVal Force As Boolean = False)
        If (importConfiguration.UpdatingDatabase) Then
            LogStatus("", "iPOS database update in progress, skipping data import")
            Exit Sub
        End If

        Try
            importConfiguration.UpdatingDatabase = True
            ' show a message in livelink, showing it is currently processing
            SetStatus("Processing...")

            ' Instantiate the iPOS interface class if it hasn't been done already
            If importConfiguration.Import Is Nothing Then
                importConfiguration.Import = New iPOS(EntityID, LiveLinkConfiguration.ConnectionString)
            Else
                ' refresh the entityID cached value (in case it has been changed)
                importConfiguration.Import.EntityID = EntityID
            End If

            ' get the last query date
            Dim queryDate As DateTime = Me.ProcessDate

            If (queryDate = DateTime.MinValue) Then
                queryDate = DateTime.Now.AddDays(-LiveLinkConfiguration.iPOSDaysToRecover).Date
            ElseIf (Force OrElse Not importConfiguration.FirstPollComplete) Then
                ' if first poll or forced poll, check to see the amount of days to process in the past is at least the value of DaysToRecover
                If (queryDate > DateTime.Now.AddDays(-LiveLinkConfiguration.iPOSDaysToRecoverOnStartup).Date) Then
                    queryDate = DateTime.Now.AddDays(-LiveLinkConfiguration.iPOSDaysToRecoverOnStartup).Date
                End If
            End If

            ' display the last query date on the form
            Me.ProcessDate = queryDate

            ' make sure query date isn't too far in the past
            If ((LiveLinkConfiguration.DaysBeforeObsolete <> 0) AndAlso (queryDate < DateTime.Now.AddDays(-LiveLinkConfiguration.DaysBeforeObsolete))) Then
                queryDate = DateTime.Now.AddDays(-LiveLinkConfiguration.DaysBeforeObsolete).Date
            End If

            ' process up until current day
            While (queryDate <= DateTime.Now.Date)

                If (importConfiguration.UpdatingDatabase) Then
                    LogStatus("", "iPOS database update in progress, ending the data import")
                    Exit While
                End If

                ' Process iPOS
                If importConfiguration.Import.Process(queryDate) Then
                    ' if process was successfull, update the last query date value
                    importConfiguration.Import.SetLastQueryDate(queryDate)
                    Me.ProcessDate = queryDate
                End If
                ' attempt to query the next business day 
                queryDate = queryDate.AddDays(1)
            End While

            ' clean up rolling cache
            If (LiveLinkConfiguration.DaysBeforeObsolete <> 0) Then
                ' delete old cache
                importConfiguration.Import.DeleteFromCache(DateTime.Now.AddDays(-LiveLinkConfiguration.DaysBeforeObsolete).Date)
            End If
        Catch ex As Exception
            LogEvent(EventLogEntryType.Error, "Exception occurred processing iPOS data. Exception: {0}", ex.ToString)
            LogStatus("Error", ex.ToString)
        Finally
            importConfiguration.UpdatingDatabase = False
            SetStatus("")
        End Try
    End Sub



    Private Sub UpdateiPOSProducts(ByVal importConfiguration As iPOSConfiguration)
        If (importConfiguration.UpdatingDatabase) Then
            LogStatus("", "iPOS database update in progress, cannot action product update")
            Exit Sub
        End If

        Try
            importConfiguration.UpdatingDatabase = True
            SetStatus("Updating iPOS...")
            LogEvent(EventLogEntryType.Information, "Attempt to update iPOS products triggered from UI")

            ' make sure you have a lease prior to update
            GetLease()
            ' process product update
            iPOSUpdateHelper.ProcessUpdate(iPOSUpdateHelper.UpdateType.iPOSProductUpdate, _liveLinkID, _sessionKey, CInt(EntityID))

        Catch ex As Exception
            LogEvent(EventLogEntryType.Error, "Exception occurred doing manual update of all iPOS products. Exception: {0}", ex.ToString)
        Finally
            importConfiguration.UpdatingDatabase = False
            SetStatus("")
        End Try
    End Sub

    Private Sub UpdateiPOSStaff(ByVal importConfiguration As iPOSConfiguration)
        If (importConfiguration.UpdatingDatabase) Then
            LogStatus("", "iPOS database update in progress, cannot action staff update")
            Exit Sub
        End If

        Try
            importConfiguration.UpdatingDatabase = True
            SetStatus("Updating iPOS...")
            LogEvent(EventLogEntryType.Information, "Attempt to update iPOS staff triggered from UI")

            ' make sure you have a lease prior to update
            GetLease()
            ' process staff update
            iPOSUpdateHelper.ProcessUpdate(iPOSUpdateHelper.UpdateType.iPOSStaffUpdate, _liveLinkID, _sessionKey, CInt(EntityID))

        Catch ex As Exception
            LogEvent(EventLogEntryType.Error, "Exception occurred doing manual update of all iPOS staff. Exception: {0}", ex.ToString)
        Finally
            importConfiguration.UpdatingDatabase = False
            SetStatus("")
        End Try
    End Sub

#End Region

#Region "Uniwell Gateway"


    Private Sub ProcessUniwell(ByVal importConfiguration As UniwellConfiguration)
        Try
            ' show a message in livelink, showing it is currently processing
            SetStatus("Processing...")

            ' process Uniwell reports
            Uniwell.DataHelper.ProcessReports(EntityID)

        Catch ex As Exception
            LogStatus("Error", ex.ToString)
        Finally
            SetStatus("")
        End Try
    End Sub

#End Region

#Region "Radiant Gateway"


    Private Sub ProcessRadiant(ByVal importConfiguration As RadiantConfiguration)
        Try
            ' show a message in livelink, showing it is currently processing
            SetStatus("Processing...")

            ' process Radiant data (add reference to Radiant class here)
            importConfiguration.Import = New RadiantPOS()
            importConfiguration.Import.ProcessExportXMLFiles()

            ' process Radiant data (add reference to Radiant class here)
            importConfiguration.PunchImport = New RadiantPunch()
            importConfiguration.PunchImport.ProcessExportXMLFiles()
        Catch ex As Exception
            LogStatus("Error", ex.ToString)
        Finally
            SetStatus("")
        End Try
    End Sub

    Private Sub StartRadiantListener()
        Try
            ' check if Radiant is enabled
            If LiveLinkConfiguration.RadiantEnabled Then
                ' check if object has been created
                If (_myRadiantListener Is Nothing) Then
                    ' create new listener object
                    _myRadiantListener = New RadiantListener()
                End If
                ' start the listener
                _myRadiantListener.Start()
                AddHandler _myRadiantListener.FileCreated, AddressOf MyRadiantListener_FileCreated
            End If
        Catch ex As Exception
            LogEvent(EventLogEntryType.Error, "Exception occurred starting the radiant file listener. Exception: {0}", ex.ToString)
        End Try
    End Sub

    Private Sub StopRadiantListener()
        Try
            ' check if radiant is enabled
            If LiveLinkConfiguration.RadiantEnabled Then
                ' check if radiant listener has been created (only stop if it has been created)
                If (_myRadiantListener IsNot Nothing) Then
                    RemoveHandler _myRadiantListener.FileCreated, AddressOf MyRadiantListener_FileCreated
                    _myRadiantListener.Stop()
                End If
            End If
        Catch ex As Exception
            LogEvent(EventLogEntryType.Error, "Exception occurred stopping the radiant file listener. Exception: {0}", ex.ToString)
        End Try
    End Sub

    Private Sub MyRadiantListener_FileCreated(ByVal sender As Object, ByVal args As System.IO.FileSystemEventArgs)
        DoPollData(PollReasonType.NewDataAvailable)
    End Sub


#End Region

#Region "POSixml Gateway"
    Private Sub ProcessPOSixml(ByVal importConfiguration As POSixmlImportConfiguration)
        Try
            ' show a message in livelink, showing it is currently processing
            SetStatus("Processing...")

            ' process POSixml data (add reference to POSixml class here)
            importConfiguration.Import = New POSixmlImport()
            importConfiguration.Import.ProcessExportXmlFiles()
        Catch ex As Exception
            LogStatus("Error", ex.ToString)
        Finally
            SetStatus("")
        End Try
    End Sub
#End Region

#Region "Xpient Gateway"

    Private Sub ProcessXpient(ByVal importConfiguration As XpientImportConfiguration)
        Dim EntityId As Integer = GetEntityId()
        Try
            ' show a message in livelink, showing it is currently processing
            SetStatus("Processing...")

            ' process Xpient data (add reference to Xpient class here)
            importConfiguration.Import = New XpientPOS()
            importConfiguration.Import.DataProcess(EntityId)
        Catch ex As Exception
            LogStatus("Error", ex.ToString)
        Finally
            SetStatus("")
        End Try
    End Sub
#End Region

#Region "RPOS Gateway"
    Private Sub ProcessRPOS(ByVal importConfiguration As RPOSImportConfiguration)
        Try
            ' show a message in livelink, showing it is currently processing
            SetStatus("Processing...")

            ' process RPOS data (add reference to RPOS class here)
            importConfiguration.Import = New RadiantRPOS()
            importConfiguration.Import.ProcessData()
        Catch ex As Exception
            LogStatus("Error", ex.ToString)
        Finally
            SetStatus("")
        End Try
    End Sub
#End Region

#Region " ZK Fingerprint Scanner"


    Private Function ProcessZKFingerprintsTA(ByVal importConfiguration As ZKFingerprintConfiguration) As Boolean
        Try
            If importConfiguration.Fingerprint Is Nothing Then
                ' initialise the fingerprint device interdface
                importConfiguration.Fingerprint = New zkFingerPrint(EntityID, LiveLinkConfiguration.ZKFingerprintIP, LiveLinkConfiguration.ZKFingerprintPort)
            End If
            ' get all the TA records
            importConfiguration.Fingerprint.ExecuteRequest(zkFingerPrint.RequestType.RetrieveTA)
        Catch ex As Exception
            LogStatus("Error", ex.ToString)
        Finally
            SetStatus("")
        End Try
    End Function


#End Region

#Region "Time Punch Import"

    Private Sub ProcessTimePunchImport(ByVal importConfiguration As TimePunchImportConfiguration)
        Dim entityId As Integer = GetEntityId()
        Try
            ' show a message in livelink, showing it is currently processing
            SetStatus("Processing...")

            ' process Time Punch data
            importConfiguration.Import = New TimePunchImportPOS()
            importConfiguration.Import.DataProcess(entityId)
        Catch ex As Exception
            LogStatus("Error", ex.ToString)
        Finally
            SetStatus("")
        End Try
    End Sub
#End Region

#Region "SUS POS Import"

    Private Sub ProcessSUSImport(ByVal importConfiguration As SUSImportConfiguration)
        Dim entityId As Integer = GetEntityId()
        Try
            ' show a message in livelink, showing it is currently processing
            ' process Time Punch data
            importConfiguration.Import = New SUSPOS()
            importConfiguration.Import.DataProcess(entityId)
        Catch ex As Exception
            LogStatus("Error", ex.ToString)
        Finally
            SetStatus("")
        End Try
    End Sub
#End Region

#Region "Alert Systems Traffic Data Processing"

    Private Sub ProcessTrafficData(ByVal importConfiguration As AlertSystemsTrafficDataConfiguration)
        Dim entityId As Integer = GetEntityId()
        Try
            Dim liveLinkWebService As LiveLinkWebService = New LiveLinkWebService()
            liveLinkWebService.Url = LiveLinkConfiguration.WebService_URL
            LoadProxy(liveLinkWebService)

            importConfiguration.FileProcessor = New AlertSystemsFileProcessor(_liveLinkID, _sessionKey, entityId, liveLinkWebService)
            importConfiguration.FileProcessor.ProcessTrafficDataFiles()
        Catch ex As Exception
            LogStatus("Error", ex.ToString)
        Finally
            SetStatus(String.Empty)
        End Try
    End Sub
#End Region

#Region "Speed of service reporting"

    Private Function ProcessServiceReport(ByVal importConfiguration As ServiceReportConfiguration) As Boolean
        Try
            ' get the speed of service reporting data and add it to the database
            If ServiceReport.GetServiceTimes(EntityID) Then
                LogStatus("Info", "Speed of service report processed")
            Else
                LogStatus("Error", "Speed of service report failed")
            End If
        Catch ex As Exception
            LogStatus("Error", ex.ToString)
        End Try
    End Function

#End Region

#Region " Common Pos Gateway Interface"

    Private Sub ProcessPosCommon()

        Dim EntityId As Integer = GetEntityId()
        Try
            ' show a message in livelink, showing it is currently processing
            SetStatus("Processing...")

            _posCommon.ProcessData(EntityId)

        Catch ex As Exception
            LogStatus("Error", ex.ToString)
        Finally
            SetStatus("")
        End Try
    End Sub

    'Action trigger when pos status changed
    Private Sub PosStatusChangedHandler(ByVal sender As Object, ByVal e As Progress)
        'Show status for common pos
        Dim msg As KeyValuePair(Of String, String) = e.StatusMsg
        LogStatus(msg.Key, msg.Value)
    End Sub

#End Region

#Region " Setup Tab   "

    Private Function GetStoreList() As List(Of EntityListItem)
        Dim MyData As New LiveLinkWebService ' TODO: Enable GZip support: eRetailerCompressed
        Dim MyLiveLinkResponse As eRetailer.LiveLinkResponse
        Dim Row As DataRow
        Dim storeList As List(Of EntityListItem)
        Dim Store As EntityListItem

        Try
            MyData.Url = LiveLinkConfiguration.WebService_URL
            LoadProxy(MyData)
            GetLease()
            If _conditionalSessionKey <> String.Empty Then
                MyLiveLinkResponse = MyData.ListStores(_liveLinkID, _conditionalSessionKey)
                If MyLiveLinkResponse.ResponseCode = eRetailer.LiveLinkResponseCode.Success Then
                    If MyLiveLinkResponse.RecordCount = 0 Then
                        Return Nothing
                    Else
                        storeList = New List(Of EntityListItem)
                        For Each Row In MyLiveLinkResponse.ResponseData.Tables("StoreList").Rows
                            storeList.Add(New EntityListItem(Row.Item("EntityId"), Row.Item("Entity")))
                        Next
                        LogStatus("", "Stores loaded")
                        SuccessfulCommunications()
                    End If
                Else
                    ProcessResponse(MyLiveLinkResponse)
                End If
            End If
        Catch ex As WebException
            LogStatus("Error", ex.Message)
            BackOffCommunications(False)
        Catch ex As Exception
            LogStatus("Error", ex.Message)
        End Try
        Return storeList
    End Function

    Private Function GetRegisterId() As Int64
        ' This function returns the EntityId that is used to populate tbSalesMain
        ' It differs for each POS type
        Dim Use_ODBC As Boolean = LiveLinkConfiguration.Use_ODBC_DB_Connect
        Dim sConnect_ODBC As String = LiveLinkConfiguration.ODBC_DB_Connect
        Dim sPOSType As String = LiveLinkConfiguration.POSType
        Dim sSql As String = String.Empty
        Dim WSDataSet As DataSet
        Dim sError As String = String.Empty

        GetRegisterId = -1
        Try
            Select Case sPOSType.ToUpper.Trim
                Case "PARJOURNAL", "PAN7700", "CRATOS", "ALOHA", "SIGNATURE", "PARINFUSION", "PCAMERICA", "PIXELPOINT", "COMPRIS", "PAREXALT4", "SILVERWARE", "ICG", "MICROSOFTRMS", "NEWPOS", "IPOS", "UNIWELL"
                Case "MICROS"
                Case "QUICKEN"
                    ' EThe unique register ID is passed through to the web service.  
                    ' Retrive the register Id from tbSalesMain - Serial_Number.
                    sSql = "SELECT Top 1 Serial_Number As RegisterId From tbSalesMain Order by SalesMainId DESC"
                Case "PROTOUCH"
                Case Else
            End Select

            If sSql <> String.Empty Then
                If Use_ODBC Then
                    If MMSLocalGeneric_ListAll(sConnect_ODBC, sSql, "tbStoreInfo", WSDataSet, sError) Then
                        If WSDataSet.Tables("tbStoreinfo").Rows.Count = 1 Then
                            ' Quicken uses 8 byte serial numbers that overflow int64.  COnvert to unsigned int64 and then back with -ve sign if necessary
                            GetRegisterId = Int64.Parse(Convert.ToUInt64(WSDataSet.Tables("tbStoreInfo").Rows(0).Item("RegisterId")).ToString("X"), Globalization.NumberStyles.HexNumber)
                        End If
                    End If
                Else
                    If MMSGeneric_ListAll(sSql, "tbStoreInfo", WSDataSet, sError) Then
                        If WSDataSet.Tables("tbStoreinfo").Rows.Count = 1 Then
                            ' Quicken uses 8 byte serial numbers that overflow int64.  COnvert to unsigned int64 and then back with -ve sign if necessary
                            GetRegisterId = Int64.Parse(Convert.ToUInt64(WSDataSet.Tables("tbStoreInfo").Rows(0).Item("RegisterId")).ToString("X"), Globalization.NumberStyles.HexNumber)
                        End If
                    End If
                End If
            End If

        Catch ex As Exception
            GetRegisterId = -1
        End Try
    End Function

    Private Function SaveEntityId(ByVal EntityId As Integer) As Boolean
        ' This function saves EntityId that is used to populate tbSalesMain
        ' The location differs for each POS type
        Dim Use_ODBC As Boolean = LiveLinkConfiguration.Use_ODBC_DB_Connect
        Dim sConnect_ODBC As String = LiveLinkConfiguration.ODBC_DB_Connect
        Dim sPOSType As String = LiveLinkConfiguration.POSType
        Dim sSql As String = String.Empty
        Dim sError As String = String.Empty

        SaveEntityId = False

        Try
            Select Case sPOSType.ToUpper
                Case "PARJOURNAL", "PAN7700", "CRATOS", "ALOHA", "SIGNATURE", "PARINFUSION", "MICROS9700", "PCAMERICA", "PIXELPOINT", "COMPRIS", "INTOUCH", "PAREXALT4", "SILVERWARE", "OCTANE", "ICG", "MICROSOFTRMS", "NEWPOS", "IPOS", "UNIWELL"
                    ' Entity Id is stored in the tbStoreInfo table in the MacromatiX database
                    sSql = "IF NOT EXISTS (SELECT * FROM tbStoreInfo) "
                    sSql = sSql + "INSERT INTO tbStoreInfo (StoreId, RegisterId) VALUES (" & EntityId & ", 0) "
                    sSql = sSql + "ELSE "
                    sSql = sSql + "UPDATE tbStoreinfo SET StoreId = " & EntityId & " "
                Case "MICROS"
                    ' Entity Id is saved in the custom.tbStoreInfo table in the Micros Sybase database
                    sSql = "IF NOT EXISTS (SELECT * FROM custom.tbStoreInfo) "
                    sSql = sSql + "INSERT INTO custom.tbStoreInfo (StoreId) VALUES (" & EntityId & ") "
                    sSql = sSql + "ELSE "
                    sSql = sSql + "UPDATE custom.tbStoreinfo SET StoreId = " & EntityId & " "
                Case "QUICKEN"
                    ' Entity Id is not set anywhere on the POS.  The unique register ID is passed through to
                    ' the web service.  Store the Entity Id in mxlivelink.config as a double check only.
                    LiveLinkConfiguration.StoreID = EntityId
                    LiveLinkConfiguration.Save()
                    SaveEntityId = True
                Case "PROTOUCH"
                    ' Save the EntityId from the Protouch ini file
                    Dim ProTouchIniFile As String = LiveLinkConfiguration.ProTouchIniFile
                    If ProTouchIniFile = "" Then
                        ProTouchIniFile = cProTouchIniFile
                    End If
                    Dim objIniFile As New IniFile(ProTouchIniFile)
                    objIniFile.WriteInteger("MacroMatix", "EntityId", EntityId)
                    If objIniFile.GetInteger("MacroMatix", "EntityId", "-1") = EntityId Then
                        SaveEntityId = True
                    End If
                Case Else
                    If Use_ODBC Then
                        ' default to saving the Entity Id in mxlivelink.config
                        LiveLinkConfiguration.StoreID = EntityId
                        LiveLinkConfiguration.Save()
                        SaveEntityId = True
                    Else
                        ' default to saving the Entity Id in tbStoreInfo
                        sSql = "IF NOT EXISTS (SELECT * FROM tbStoreInfo) "
                        sSql = sSql + "INSERT INTO tbStoreInfo (StoreId, RegisterId) VALUES (" & EntityId & ", 0) "
                        sSql = sSql + "ELSE "
                        sSql = sSql + "UPDATE tbStoreinfo SET StoreId = " & EntityId & " "
                    End If
            End Select
            If sSql <> String.Empty Then
                If Use_ODBC Then
                    If MMSLocalGeneric_Execute(sConnect_ODBC, sSql, sError) Then
                        SaveEntityId = True
                    Else
                        LogEvent("Unable to save store number" & vbCrLf & "Error: " & sError & vbCrLf & "SQL: " & sSql, EventLogEntryType.Error)
                    End If
                Else
                    If MMSGeneric_Execute(sSql, sError) Then
                        SaveEntityId = True
                    Else
                        LogEvent("Unable to save store number" & vbCrLf & "Error: " & sError & vbCrLf & "SQL: " & sSql, EventLogEntryType.Error)
                    End If
                End If
            End If
        Catch ex As Exception
            SaveEntityId = False
        End Try
    End Function


#End Region

#Region "  Session/Lease Handling   "

    Public Sub SendSystemInfo() Implements ILiveLinkCore.SendSystemInfo
        ExpireLease() ' Force a new lease acquisition
        ProcessAwake(True)
    End Sub

    Public Function RecreateLiveLinkID() As Boolean Implements ILiveLinkCore.RecreateLiveLinkID
        Return RecreateUniqueMachineId(_liveLinkID)
    End Function

    Public Sub RefreshEntityID() Implements ILiveLinkCore.RefreshEntityID
        _entityID = GetEntityId()
        _entityName = String.Empty
    End Sub

    Public Sub RefreshConfiguration() Implements ILiveLinkCore.RefreshConfiguration
        LiveLinkConfiguration.Refresh()
    End Sub

    Function CreateChallenge() As String
        Dim sClientCode As String = LiveLinkConfiguration.ClientCode
        Dim sSecurityCode As String = LiveLinkConfiguration.SecurityCode
        Dim ChallengeTime As String = Format(Date.UtcNow, "yyyyMMddHHmmss")
        CreateChallenge = Polling.ChallengeResponse(Guid.NewGuid.ToString(), sClientCode, sSecurityCode) & ChallengeTime & Format(CInt(SessionServiceType.LiveLink), "000")
    End Function

    Private Sub ExpireLease()
        _leaseExpired = True
        _sessionKey = String.Empty
        _conditionalSessionKey = String.Empty
    End Sub

    Private Function HasConditionalLease() As Boolean
        ' Returns true if a conditional lease has been obtained
        If Not _leaseExpired And _sessionKey = String.Empty And _conditionalSessionKey <> String.Empty Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub SessionLease(ByVal sender As System.Object, ByVal e As System.Timers.ElapsedEventArgs) Handles _timerSessionLease.Elapsed
        ExpireLease()
    End Sub

    Private Sub GetLease(Optional ByVal siteNotConfiguredBackOff As Boolean = True)
        Dim MySession As New LiveLinkWebService ' TODO: Enable GZip support: eRetailerCompressed
        Dim MyLiveLinkResponse As eRetailer.LiveLinkResponse
        Dim ResponseServiceType As SessionServiceType
        Dim myKey As String
        Dim myChallenge As String
        Dim LeaseObtained As Boolean = False

        If _liveLinkID <> String.Empty Then
            MySession.Url = LiveLinkConfiguration.WebService_URL
            LoadProxy(MySession)

            If _leaseExpired Then
                Try
                    'SetDebugLevel()
                    myChallenge = CreateChallenge()
                    LogStatus("", "Acquiring lease")
                    'ShowDebug("Live Link Challenge: [" & myChallenge & "]", DebugModes.Verbose)
                    MyLiveLinkResponse = MySession.RequestLease(_liveLinkID, myChallenge, Application.ProductVersion)
                    If MyLiveLinkResponse.ResponseCode = eRetailer.LiveLinkResponseCode.Success Or _
                    MyLiveLinkResponse.ResponseCode = eRetailer.LiveLinkResponseCode.AwaitingActivation Then
                        Dim sClientCode As String = LiveLinkConfiguration.ClientCode
                        Dim sSecurityCode As String = LiveLinkConfiguration.SecurityCode
                        Dim ParseResult As Integer
                        'ShowDebug("Server Response: [" & MyLiveLinkResponse.ResponseString & "]", DebugModes.Verbose)
                        ParseResult = Polling.ParseKey(MyLiveLinkResponse.ResponseString, True, myKey, ResponseServiceType)
                        'ShowDebug("Server Response Key: [" & myKey & "]", DebugModes.Verbose)
                        If ParseResult = 0 Then
                            ' At this stage a valid response was returned, within the date range
                            Dim myServerChallenge As String = Polling.ChallengeResponse(myChallenge, sClientCode, sSecurityCode)
                            'ShowDebug("Live Link Validation: [" & myServerChallenge & "]", DebugModes.Verbose)
                            If (ResponseServiceType = SessionServiceType.LiveLink) And (myKey = myServerChallenge) Then
                                ' At this stage, the service code recived is correct and the initial challange was met 
                                ' with a valid response.  ie The server has properly authenticated itself to the client
                                ' Now calculate the Session key that will be used to authticate the client to the server.
                                ' (the server has already saved its own calculated version into tbSessionStatus)
                                _conditionalSessionKey = Polling.ChallengeResponse(MyLiveLinkResponse.ResponseString, sClientCode, sSecurityCode) & Format(CInt(SessionServiceType.LiveLink), "000")
                                ' If the client has not yet been activated by the server (manual step), then the session key
                                ' can only be used for limited functionality.
                                If MyLiveLinkResponse.ResponseCode = eRetailer.LiveLinkResponseCode.Success Then
                                    ' If the client is deemed to be Online by the server (ie activated), then the
                                    ' session key may be used for all functions.
                                    _sessionKey = _conditionalSessionKey
                                    PingEnabled = True
                                    ' Alter the transaction flow if required
                                    With MyLiveLinkResponse
                                        Dim FlowChanged As Boolean = False
                                        Dim AwakeFlowChanged As Boolean = False
                                        If (.ResponseFlow(0) > 0) And (.ResponseFlow(0) <> _flowTxInBatch) Then
                                            _flowTxInBatch = .ResponseFlow(0)
                                            FlowChanged = True
                                        End If
                                        If (.ResponseFlow(1) > 0) And (.ResponseFlow(1) <> _flowIterations) Then
                                            _flowIterations = .ResponseFlow(1)
                                            FlowChanged = True
                                        End If
                                        If (.ResponseFlow(2) > 0) And (.ResponseFlow(2) <> _flowMaxRecords) Then
                                            _flowMaxRecords = .ResponseFlow(2)
                                            FlowChanged = True
                                        End If
                                        If (.ResponseFlow(3) > 0) And (.ResponseFlow(3) <> _flowInterval) Then
                                            _flowInterval = .ResponseFlow(3)
                                            FlowChanged = True
                                        End If
                                        If FlowChanged Then
                                            LogStatus("", "Flow adjusted: " & _flowIterations.ToString & " batches of " & _flowTxInBatch.ToString & " (Max records: " & _flowMaxRecords & ")")
                                            LogStatus("", "Flow adjusted: Send interval - " & _flowInterval.ToString & " minute(s)")
                                        End If
                                        If (.ResponseFlow(4) > 0) And (.ResponseFlow(4) <> _awakeInterval) Then
                                            _awakeInterval = .ResponseFlow(4)
                                            AwakeFlowChanged = True
                                        End If
                                        If (.ResponseFlow(5) > 0) And (.ResponseFlow(5) <> _awakeCounterVerbose) Then
                                            _awakeCounterVerbose = .ResponseFlow(5)
                                            AwakeFlowChanged = True
                                        End If
                                        If AwakeFlowChanged Then
                                            LogStatus("", "Awake Flow adjusted: Send interval - " & _awakeInterval.ToString & " minute(s)")
                                            LogStatus("", "Awake Flow adjusted: Verbose iterations - " & _awakeCounterVerbose.ToString)
                                            RaiseEvent FlowChangedEvent(Me, New EventArgs())
                                        End If
                                    End With
                                Else ' If site not activated then ...
                                    If (siteNotConfiguredBackOff) Then
                                        BackOffCommunications(True) ' Make sure that comms slowly backoff if site is not activated
                                    End If
                                End If
                                _timerSessionLease.Stop()
                                ' Set the lease time as 30 seconds less than the granted lease time to account for
                                ' communication delays
                                ' Lease must be between 1 and 60 minutes
                                _timerSessionLease.Interval = Max(1, Min(60, CInt(MyLiveLinkResponse.ResponseNumber))) * 60000 - 30000
                                _leaseExpired = False
                                LeaseObtained = True
                                _messageWaiting = MyLiveLinkResponse.ResponseMessageWaiting
                                _timerSessionLease.Start()
                                LogStatus("", "Lease acquired")
                            Else
                                ExpireLease()
                                PingEnabled = False
                                LogStatus("", "Lease acquisition failed - negotiation failed")
                            End If
                        Else
                            ExpireLease()
                            PingEnabled = False
                            LogStatus("", "Lease acquisition failed - invalid response (code " & ParseResult & ")")
                        End If
                    Else
                        ExpireLease()
                        PingEnabled = False
                        ProcessResponse(MyLiveLinkResponse)
                    End If
                    If Not LeaseObtained Then
                        LogStatus("", "Lease not obtained")
                        BackOffCommunications(False)
                    End If

                Catch ex As WebException
                    LogStatus("Error", ex.Message)
                    BackOffCommunications(False)
                Catch ex As Exception
                    LogEvent(String.Format("Error occurred while obtaining lease{0}Error: {1}", vbCrLf, ex), EventLogEntryType.Error)
                    LogStatus("", "Error occurred while obtaining lease.")
                End Try
            End If
        Else
            ExpireLease()
            LogStatus("Error", "Unable to determine LiveLink's unique id.")
        End If
    End Sub


#End Region

#Region "  SQL Functions "

    Private Function sqlSelectTopNonSyncedHeaders(ByVal sPosType As String, ByVal TxInBatch As Integer, Optional ByVal DataType As DataType = DataType.SalesData) As String
        Dim sFieldList As String = GettbSalesMainFields(sPosType)
        Select Case DataType
            Case DataType.SalesData
                'First record must be header record "H" or a "1" for Quicken
                Select Case sPosType.ToUpper
                    Case "QUICKEN"
                        sqlSelectTopNonSyncedHeaders = "Select Top " & TxInBatch.ToString & " " & sFieldList & " from tbSalesMain Where (Sequence_Number=1) and (Synced=0) Order By Date_Time, SalesMainID "
                    Case "PARJOURNAL", "PAREXALT4", "ICG", "MICROSOFTRMS", "IPOS"
                        sqlSelectTopNonSyncedHeaders = "Select Top " & TxInBatch.ToString & " " & sFieldList & " from tbSalesMain Where (RecordType ='H') and (Synced=0) Order By CONVERT(DATETIME,PollDate), TransactionVersion, SalesMainID "
                    Case "PIXELPOINT" ' PixelPoint uses Sybase 5 which does not support the TOP command
                        If TxInBatch = 1 Then
                            sqlSelectTopNonSyncedHeaders = "Select First " & sFieldList & " from tbSalesMain Where (RecordType ='H') and (Synced=0) Order By SalesMainID "
                        Else
                            sqlSelectTopNonSyncedHeaders = "Select " & sFieldList & " from tbSalesMain Where (RecordType ='H') and (Synced=0) Order By SalesMainID "
                        End If
                    Case Else
                        sqlSelectTopNonSyncedHeaders = "Select Top " & TxInBatch.ToString & " " & sFieldList & " from tbSalesMain Where (RecordType ='H') and (Synced=0) Order By SalesMainID "
                End Select
            Case DataType.AttendanceData
                Select Case sPosType.ToUpper
                    Case "QUICKEN" ' Not supported on Quicken so return empty recordset
                        sqlSelectTopNonSyncedHeaders = "Select Top " & TxInBatch.ToString & " " & sFieldList & " from tbSalesMain Where (Sequence_Number=1) and (Sequence_Number=0) " ' Return empty recordset
                    Case "PIXELPOINT" ' PixelPoint uses Sybase 5 which does not support the TOP command
                        If TxInBatch = 1 Then
                            sqlSelectTopNonSyncedHeaders = "Select First " & sFieldList & " from tbSalesMain Where (RecordType ='T') and (Synced=0) Order By SalesMainID "
                        Else
                            sqlSelectTopNonSyncedHeaders = "Select " & sFieldList & " from tbSalesMain Where (RecordType ='T') and (Synced=0) Order By SalesMainID "
                        End If
                    Case Else
                        sqlSelectTopNonSyncedHeaders = "Select Top " & TxInBatch.ToString & " " & sFieldList & " from tbSalesMain Where (RecordType ='T') and (Synced=0) Order By SalesMainID "
                End Select
        End Select
    End Function

    Private Function sqlUpdateSyncToken(ByVal sPosType As String, ByVal HeaderRow As DataRow, Optional ByVal DataType As DataType = DataType.SalesData) As String
        Dim sQry As String = String.Empty
        Try
            Select Case DataType
                Case DataType.SalesData
                    sQry = "Update tbSalesMain set SyncToken ='" & Guid.NewGuid.ToString & "' where (TransactionId =" & HeaderRow.Item("TransactionID").ToString & ") And (SyncToken is Null) "
                    Select Case sPosType.Trim.ToUpper
                        Case "QUICKEN" ' Quicken resets transaction ID's every day
                            sQry &= " AND  (Date_Time=#" & Format(HeaderRow.Item("Date_Time"), "dd-MMM-yyyy HH:mm:ss") & "#) "

                        Case "ALOHA" ' Aloha resets transaction ID's every day
                            sQry &= " AND (CONVERT(DATETIME, PollDate)='" & Format(CDate(HeaderRow.Item("PollDate")), "dd-MMM-yyyy HH:mm:ss") & "') "
                            sQry &= " AND (TransactionVersion = " & HeaderRow.Item("TransactionVersion").ToString & ")"

                        Case "MICROS", "PIXELPOINT", "PCAMERICA", "SIGNATURE", "COMPRIS", "INTOUCH", "PAREXALT4", "SILVERWARE", "OCTANE" ' Micros checks that are reopened get assigned the same transaction number plus a new version
                            sQry &= " AND (TransactionVersion = " & HeaderRow.Item("TransactionVersion").ToString & ")"

                        Case "NEWPOS", "IPOS"
                            sQry &= String.Format(" AND (RegisterID = {0})", HeaderRow.Item("RegisterID"))
                            sQry &= String.Format(" AND (PollDate = '{0}')", HeaderRow.Item("PollDate"))
                            sQry &= String.Format(" AND (TransactionVersion = {0})", HeaderRow.Item("TransactionVersion"))

                        Case "MICROSOFTRMS"
                            sQry &= String.Format(" AND (PollDate = '{0}')", HeaderRow.Item("PollDate"))
                            sQry &= String.Format(" AND (TransactionVersion = {0})", HeaderRow.Item("TransactionVersion"))

                        Case "ICG"
                            sQry &= String.Format(" AND (TransactionVersion = {0}) ", IntegerValue(HeaderRow("TransactionVersion"), 0))
                            sQry &= String.Format(" AND (PollDate = '{0}') ", HeaderRow("PollDate").ToString)
                            sQry &= String.Format(" AND (SubTypeDescription = '{0}') ", HeaderRow("SubTypeDescription").ToString)

                        Case "PARJOURNAL", "PAN7700"
                            ' do not need to add extra fields

                        Case Else ' else handles any generic POS types
                            sQry &= String.Format(" AND (PollDate = '{0}')", HeaderRow.Item("PollDate"))
                            sQry &= String.Format(" AND (TransactionVersion = {0})", HeaderRow.Item("TransactionVersion"))
                    End Select
                    sQry &= " AND (RecordType IN (" & salesRecordTypes & "))"

                Case DataType.AttendanceData
                    sQry = "Update tbSalesMain set SyncToken ='" & Guid.NewGuid.ToString & "' where (SalesMainId =" & HeaderRow.Item("SalesMainID").ToString & ") And (SyncToken is Null) "
            End Select
        Catch ex As Exception
            sQry = ""
        End Try
        sqlUpdateSyncToken = sQry
    End Function

    Private Function sqlGetSyncToken(ByVal sPosType As String, ByVal HeaderRow As DataRow, Optional ByVal DataType As DataType = DataType.SalesData) As String
        Dim sQry As String = String.Empty
        Try
            Select Case DataType
                Case DataType.SalesData
                    sQry = "SELECT SyncToken FROM tbSalesMain WHERE (TransactionId =" & HeaderRow.Item("TransactionID").ToString & ") "
                    Select Case sPosType.Trim.ToUpper
                        Case "QUICKEN" ' Quicken resets transaction ID's every day
                            sQry &= " AND  (Date_Time=#" & Format(HeaderRow.Item("Date_Time"), "dd-MMM-yyyy HH:mm:ss") & "#) "

                        Case "ALOHA" ' Aloha resets transaction ID's every day
                            sQry &= " AND (CONVERT(DATETIME, PollDate)='" & Format(CDate(HeaderRow.Item("PollDate")), "dd-MMM-yyyy HH:mm:ss") & "') "
                            sQry &= " AND (TransactionVersion = " & HeaderRow.Item("TransactionVersion").ToString & ")"

                        Case "MICROS", "PIXELPOINT", "PCAMERICA", "SIGNATURE", "COMPRIS", "PAREXALT4", "SILVERWARE" ' Micros checks that are reopened get assigned the same transaction number plus a new version
                            sQry &= " AND (TransactionVersion = " & HeaderRow.Item("TransactionVersion").ToString & ")"

                        Case "NEWPOS", "IPOS"
                            sQry &= String.Format(" AND (RegisterID = {0})", HeaderRow.Item("RegisterID"))
                            sQry &= String.Format(" AND (PollDate = '{0}')", HeaderRow.Item("PollDate"))
                            sQry &= String.Format(" AND (TransactionVersion = {0})", HeaderRow.Item("TransactionVersion"))

                        Case "MICROSOFTRMS"
                            sQry &= String.Format(" AND (PollDate = '{0}')", HeaderRow.Item("PollDate"))
                            sQry &= String.Format(" AND (TransactionVersion = {0})", HeaderRow.Item("TransactionVersion"))

                        Case "ICG"
                            sQry &= String.Format(" AND (TransactionVersion = {0}) ", IntegerValue(HeaderRow("TransactionVersion"), 0))
                            sQry &= String.Format(" AND (PollDate = '{0}') ", HeaderRow("PollDate").ToString)
                            sQry &= String.Format(" AND (SubTypeDescription = '{0}') ", HeaderRow("SubTypeDescription").ToString)

                        Case "PARJOURNAL", "PAN7700"
                            ' do not need to add extra fields

                        Case Else ' else handles any generic POS types
                            sQry &= String.Format(" AND (PollDate = '{0}')", HeaderRow.Item("PollDate"))
                            sQry &= String.Format(" AND (TransactionVersion = {0})", HeaderRow.Item("TransactionVersion"))
                    End Select

                    sQry &= " AND (RecordType IN (" & salesRecordTypes & "))"
                Case DataType.AttendanceData
                    sQry = "SELECT SyncToken FROM tbSalesMain WHERE (SalesMainId =" & HeaderRow.Item("SalesMainID").ToString & ") "
            End Select
        Catch ex As Exception
            sQry = ""
        End Try
        sqlGetSyncToken = sQry
    End Function

    Private Function sqlSelectAllRecordsForTransaction(ByVal sPosType As String, ByVal UseODBC As Boolean, ByVal HeaderRow As DataRow, ByVal sFieldList As String) As String
        ' Sales Data only
        Dim sQry As String
        Dim sTransactionId As String

        Try
            sTransactionId = HeaderRow.Item("TransactionID").ToString
            Select Case sPosType.Trim.ToUpper
                Case "QUICKEN"
                    sQry = "Select * from tbSalesMain Where (TransactionID=" & sTransactionId & ") AND (Synced = 0) "
                    sQry &= " AND  (Date_Time=#" & Format(HeaderRow.Item("Date_Time"), "dd-MMM-yyyy HH:mm:ss") & "#) "
                    ' sQry &= " AND (RecordType IN (" & cSalesRecordTypes & ")) " ??? NOT SURE FOR QUICKEN
                    sQry &= "Order by Sequence_Number"

                Case "ALOHA" ' Aloha resets transaction ID's each day
                    sQry = "Select 1 as myOrder, " & sFieldList & " from tbSalesMain Where (TransactionID=" & sTransactionId & ") AND (TransactionVersion=" & HeaderRow.Item("TransactionVersion").ToString & ") and (RecordType = 'H') AND (Synced = 0) "
                    sQry &= "AND (CONVERT(DATETIME, PollDate)='" & Format(CDate(HeaderRow.Item("PollDate")), "dd-MMM-yyyy HH:mm:ss") & "') "
                    sQry &= "UNION Select 2 as myOrder, " & sFieldList & " from tbSalesMain Where (TransactionID=" & sTransactionId & ") AND (TransactionVersion=" & HeaderRow.Item("TransactionVersion").ToString & ") and (RecordType <> 'H') AND (Synced = 0) "
                    sQry &= "AND (CONVERT(DATETIME, PollDate)='" & Format(CDate(HeaderRow.Item("PollDate")), "dd-MMM-yyyy HH:mm:ss") & "') "
                    sQry &= " AND (RecordType IN (" & salesRecordTypes & ")) "
                    If UseODBC Then
                        sQry &= "Order by 1, ParentID, SalesMainID"
                    Else
                        sQry &= "Order by myOrder, ParentID, SalesMainID"
                    End If

                Case "MICROS", "PIXELPOINT", "PCAMERICA", "SIGNATURE", "COMPRIS", "INTOUCH", "PAREXALT4", "SILVERWARE", "OCTANE" ' Micros checks that are reopened get assigned the same transaction number plus a new version
                    sQry = "Select 1 as myOrder, " & sFieldList & " from tbSalesMain Where (TransactionId =" & sTransactionId & ") AND (TransactionVersion = " & HeaderRow.Item("TransactionVersion").ToString & ") AND (RecordType = 'H') AND (Synced = 0) "
                    sQry &= "UNION Select 2 as myOrder, " & sFieldList & " from tbSalesMain Where (TransactionId =" & sTransactionId & ") AND (TransactionVersion = " & HeaderRow.Item("TransactionVersion").ToString & ") AND (RecordType <> 'H') AND (Synced = 0) "
                    sQry &= " AND (RecordType IN (" & salesRecordTypes & ")) "
                    If UseODBC Then
                        ' For Micros RES 3.2, the version of Sybase does not support ORDER BY columnName when a UNION is used.  This is OK in RES 4.1.
                        sQry &= "Order by 1, 22, 2" ' ORDER BY myOrder, ParentId, SalesMainID (Note: Add 1 to the column number in sFieldList as myOrder is inserted as column 1)
                    Else
                        sQry &= "Order by myOrder, ParentId, SalesMainID"
                    End If

                Case "NEWPOS", "IPOS"
                    Dim sql As New StringBuilder(1024)
                    Dim pollDate As String = HeaderRow.Item("PollDate").ToString
                    Dim registerId As Integer = IntegerValue(HeaderRow.Item("RegisterID"), 0)
                    Dim transactionVersion As Integer = IntegerValue(HeaderRow.Item("TransactionVersion"), 0)

                    ' create the query string
                    sql.AppendFormat("SELECT 1 AS myOrder, {0} {1}", sFieldList, vbCrLf)
                    sql.AppendFormat("FROM tbSalesMain WHERE (TransactionID = {0} AND PollDate = '{1}' AND RegisterID = {2} AND TransactionVersion = {3}) {4}", sTransactionId, pollDate, registerId, transactionVersion, vbCrLf)
                    sql.AppendFormat("AND (RecordType = 'H') AND (Synced = 0) {0}", vbCrLf)
                    sql.AppendFormat("UNION Select 2 as myOrder, {0} {1}", sFieldList, vbCrLf)
                    sql.AppendFormat("FROM tbSalesMain WHERE (TransactionID = {0} AND PollDate = '{1}' AND RegisterID = {2} AND TransactionVersion = {3}) {4}", sTransactionId, pollDate, registerId, transactionVersion, vbCrLf)
                    sql.AppendFormat("AND (RecordType <> 'H') AND (Synced = 0) {0}", vbCrLf)
                    sql.AppendFormat("ORDER BY myOrder, ParentId, SalesMainID {0}", vbCrLf)
                    sQry = sql.ToString

                Case "MICROSOFTRMS"
                    Dim sql As New StringBuilder(1024)
                    Dim pollDate As String = HeaderRow.Item("PollDate").ToString
                    Dim transactionVersion As Integer = IntegerValue(HeaderRow.Item("TransactionVersion"), 0)

                    ' create the query string
                    sql.AppendFormat("SELECT 1 AS myOrder, {0} {1}", sFieldList, vbCrLf)
                    sql.AppendFormat("FROM tbSalesMain WHERE (TransactionID = {0} AND PollDate = '{1}' AND TransactionVersion = '{2}') {3}", sTransactionId, pollDate, transactionVersion, vbCrLf)
                    sql.AppendFormat("AND (RecordType = 'H') AND (Synced = 0) {0}", vbCrLf)
                    sql.AppendFormat("UNION Select 2 as myOrder, {0} {1}", sFieldList, vbCrLf)
                    sql.AppendFormat("FROM tbSalesMain WHERE (TransactionID = {0} AND PollDate = '{1}' AND TransactionVersion = '{2}') {3}", sTransactionId, pollDate, transactionVersion, vbCrLf)
                    sql.AppendFormat("AND (RecordType <> 'H') AND (Synced = 0) {0}", vbCrLf)
                    sql.AppendFormat("ORDER BY myOrder, ParentId, SalesMainID {0}", vbCrLf)
                    sQry = sql.ToString

                Case "ICG"
                    Dim sql As New StringBuilder(1024)
                    Dim pollDate As String = HeaderRow.Item("PollDate").ToString
                    Dim posRegisterID As String = HeaderRow.Item("SubTypeDescription").ToString
                    Dim transactionVersion As Integer = IntegerValue(HeaderRow.Item("TransactionVersion"), 0)

                    ' create the query string, since ICG has data for multiple entities the 
                    sql.AppendFormat("SELECT 1 AS myOrder, {0} {1}", sFieldList, vbCrLf)
                    sql.AppendFormat("FROM tbSalesMain WHERE (TransactionID = {0} AND TransactionVersion = {1} AND PollDate = '{2}' AND SubTypeDescription = '{3}') {4}", sTransactionId, transactionVersion, pollDate, posRegisterID, vbCrLf)
                    sql.AppendFormat("AND (RecordType = 'H') AND (Synced = 0) {0}", vbCrLf)
                    sql.AppendFormat("UNION Select 2 as myOrder, {0} {1}", sFieldList, vbCrLf)
                    sql.AppendFormat("FROM tbSalesMain WHERE (TransactionID = {0} AND TransactionVersion = {1} AND PollDate = '{2}' AND SubTypeDescription = '{3}') {4}", sTransactionId, transactionVersion, pollDate, posRegisterID, vbCrLf)
                    sql.AppendFormat("AND (RecordType <> 'H') AND (Synced = 0) {0}", vbCrLf)
                    sql.AppendFormat("ORDER BY myOrder {0}", vbCrLf)
                    sQry = sql.ToString

                Case "PARJOURNAL", "PAN7700"
                    Dim sql As New StringBuilder(1024)
                    ' create the query string (specifically doesn't use TransactionVersion column)
                    sql.AppendFormat("SELECT 1 AS myOrder, {0} {1}", sFieldList, vbCrLf)
                    sql.AppendFormat("FROM tbSalesMain WHERE (TransactionID = {0}) {1}", sTransactionId, vbCrLf)
                    sql.AppendFormat("AND (RecordType = 'H') AND (Synced = 0) {0}", vbCrLf)
                    sql.AppendFormat("UNION Select 2 as myOrder, {0} {1}", sFieldList, vbCrLf)
                    sql.AppendFormat("FROM tbSalesMain WHERE (TransactionID = {0}) {1}", sTransactionId, vbCrLf)
                    sql.AppendFormat("AND (RecordType <> 'H') AND (Synced = 0) {0}", vbCrLf)

                    ' define order method depending on use of ODBC or not
                    If UseODBC Then
                        sql.AppendFormat("ORDER BY 1, ParentId, SalesMainID {0}", vbCrLf)
                    Else
                        sql.AppendFormat("ORDER BY myOrder, ParentId, SalesMainID {0}", vbCrLf)
                    End If
                    sQry = sql.ToString

                Case Else
                    Dim sql As New StringBuilder(1024)
                    Dim pollDate As String = HeaderRow.Item("PollDate").ToString
                    Dim transactionVersion As Integer = IntegerValue(HeaderRow.Item("TransactionVersion"), 0)

                    ' create the query string
                    sql.AppendFormat("SELECT (CASE RecordType WHEN 'H' THEN 1 ELSE 2 END) AS myOrder, {0} {1}", sFieldList, vbCrLf)
                    sql.AppendFormat("FROM tbSalesMain WHERE (TransactionID = {0} AND PollDate = '{1}' AND TransactionVersion = '{2}') AND (Synced = 0) {3}", sTransactionId, pollDate, transactionVersion, vbCrLf)

                    ' define order method depending on use of ODBC or not
                    If UseODBC Then
                        sql.AppendFormat("ORDER BY 1, ParentId, SalesMainID {0}", vbCrLf)
                    Else
                        sql.AppendFormat("ORDER BY myOrder, ParentId, SalesMainID {0}", vbCrLf)
                    End If
                    sQry = sql.ToString
            End Select
        Catch ex As Exception
            sQry = ""
        End Try
        sqlSelectAllRecordsForTransaction = sQry
    End Function

    Private Function MarkDuplicateTransactionIdsAsErrors(ByVal sPosType As String, ByVal UseODBC As Boolean)
        Dim sSql As String = String.Empty
        Dim sError As String = String.Empty

        Try
            Select Case sPosType.ToUpper.Trim
                Case "CRATOS"
                    ' Cratos only adds the PollDate to Header records
                    ' First update the PollDate field to match the previous header record
                    ' This will also assist with archiving old data
                    sSql = "SET NOCOUNT ON "
                    sSql &= "WHILE EXISTS("
                    sSql &= "  SELECT A.SalesMainId "
                    sSql &= "  FROM  tbSalesMain A JOIN tbSalesMain B "
                    sSql &= "  ON A.SalesMainId = B.SalesMainId + 1 "
                    sSql &= "  WHERE A.TransactionId = B.TransactionId "
                    sSql &= "  AND A.PollDate IS NULL "
                    sSql &= "  AND B.PollDate IS NOT NULL) "
                    sSql &= "    UPDATE A SET PollDate = B.PollDate  "
                    sSql &= "    FROM tbSalesMain A JOIN tbSalesMain B "
                    sSql &= "    ON A.SalesMainId = B.SalesMainId + 1 "
                    sSql &= "    WHERE A.TransactionId = B.TransactionId "
                    sSql &= "    AND A.PollDate IS NULL "
                    sSql &= "    AND B.PollDate IS NOT NULL "

                    sSql &= "Update tbSalesMain "
                    sSql &= "SET Synced=11 "  ' Flag as Error - TransactionId appears twice in one day
                    sSql &= "WHERE SalesMainId IN ( "
                    sSql &= "  Select SalesMainId "
                    sSql &= "  FROM tbSalesMain A "
                    sSql &= "  WHERE SalesMainId < ( "
                    sSql &= "    SELECT MAX(SalesMainId) As SalesMainId "
                    sSql &= "    FROM tbsalesmain B "
                    sSql &= "    Where Synced=0 AND RecordType = 'H' "
                    sSql &= "    AND "
                    sSql &= "    (Convert(VarChar(15), DatePart(year, Convert(datetime, A.PollDate))) + "
                    sSql &= "     DateName(month, Convert(datetime, A.PollDate)) + "
                    sSql &= "     Convert(VarChar(15), DatePart(Day, Convert(datetime, A.PollDate)))) = "
                    sSql &= "    (Convert(VarChar(15), DatePart(year, Convert(datetime, B.PollDate))) + "
                    sSql &= "     DateName(month, Convert(datetime, B.PollDate)) + "
                    sSql &= "     Convert(VarChar(15), DatePart(Day, Convert(datetime, B.PollDate)))) "
                    sSql &= "    AND A.TransactionId = B.TransactionId "
                    sSql &= "    GROUP BY TransactionId, "
                    sSql &= "    Convert(VarChar(15), DatePart(year, Convert(datetime, PollDate))) + "
                    sSql &= "    DateName(month, Convert(datetime, PollDate)) + "
                    sSql &= "    Convert(VarChar(15), DatePart(Day, Convert(datetime, PollDate))) "
                    sSql &= "    HAVING Count(TransactionId) > 1 "
                    sSql &= "  ) "
                    sSql &= ") "
            End Select
            If sSql <> String.Empty Then
                If Not MMSGeneric_Execute(UseODBC, sSql, sError) Then
                    LogEvent("Error clearing duplicate TransactionIds" & vbCrLf & "Error: " & sError & vbCrLf & "SQL: " & sSql, EventLogEntryType.Warning)
                End If
            End If
        Catch ex As Exception
            LogEvent("Error clearing duplicate TransactionIds" & vbCrLf & "Exception: " & ex.Message & vbCrLf & "SQL: " & sSql, EventLogEntryType.Warning)
        End Try
    End Function

    Private Function sqlUpdateSyncedFlag(ByVal sPosType As String, ByVal ResultRow As DataRow, Optional ByVal DataType As DataType = DataType.SalesData)
        Dim sQry As String = String.Empty
        Try
            Select Case DataType
                Case DataType.SalesData
                    sQry = "Update tbSalesMain set Synced = " & ResultRow.Item("Result") & " where TransactionId =" & ResultRow.Item("TransactionID").ToString & " AND Synced=0"
                    Select Case sPosType.Trim.ToUpper
                        Case "QUICKEN"
                            sQry &= " AND  (Date_Time=#" & Format(ResultRow.Item("Date_Time"), "dd-MMM-yyyy HH:mm:ss") & "#) "

                        Case "ALOHA" ' Aloha resets transaction ID's every day
                            sQry &= " AND (CONVERT(DATETIME, PollDate)='" & Format(CDate(ResultRow.Item("PollDate")), "dd-MMM-yyyy HH:mm:ss") & "') "
                            sQry &= " AND (TransactionVersion = " & ResultRow.Item("TransactionVersion").ToString & ")"

                        Case "NEWPOS", "IPOS"
                            sQry &= String.Format(" AND (PollDate = '{0}')", ResultRow.Item("PollDate"))
                            sQry &= String.Format(" AND (RegisterID = {0})", ResultRow.Item("RegisterID"))
                            sQry &= String.Format(" AND (TransactionVersion = {0})", ResultRow.Item("TransactionVersion"))

                        Case "MICROSOFTRMS"
                            sQry &= String.Format(" AND (PollDate = '{0}')", ResultRow.Item("PollDate"))
                            sQry &= String.Format(" AND (TransactionVersion = {0})", ResultRow.Item("TransactionVersion"))

                        Case "MICROS", "PIXELPOINT", "PCAMERICA", "SIGNATURE", "COMPRIS", "INTOUCH", "PAREXALT4", "SILVERWARE", "OCTANE" ' Micros checks that are reopened get assigned the same transaction number plus a new version
                            sQry &= " AND (TransactionVersion = " & ResultRow.Item("TransactionVersion").ToString & ")"

                        Case "ICG"
                            ' match the date, and the subtypedescription (this holds an alpha-numeric register id value)
                            sQry &= String.Format(" AND TransactionVersion = {0} AND PollDate = '{1}' AND SubTypeDescription = '{2}'", ResultRow("TransactionVersion"), ResultRow("PollDate"), ResultRow("PosRegisterID"))

                        Case "PARJOURNAL", "PAN7700"
                            ' do not need to add extra fields

                        Case Else
                            ' handle all other POS types using PollDate and TransactionVersion as the required columns
                            sQry &= String.Format(" AND (PollDate = '{0}')", ResultRow.Item("PollDate"))
                            sQry &= String.Format(" AND (TransactionVersion = {0})", ResultRow.Item("TransactionVersion"))
                    End Select
                    sQry &= " AND (RecordType IN (" & salesRecordTypes & ")) "

                Case DataType.AttendanceData
                    sQry = "Update tbSalesMain set Synced = " & ResultRow.Item("Result") & " where SalesMainId =" & ResultRow.Item("SalesMainID").ToString & " AND Synced=0"
                    'Select Case sPosType.Trim.ToUpper
                    '    Case "QUICKEN"
                    '        sQry &= " AND  (Date_Time=#" & Format(ResultRow.Item("Date_Time"), "dd-MMM-yyyy HH:mm:ss") & "#) "
                    '    Case "ALOHA" ' Aloha resets transaction ID's every day
                    '        sQry &= " AND  (CONVERT(DATETIME, PollDate)='" & Format(CDate(ResultRow.Item("PollDate")), "dd-MMM-yyyy HH:mm:ss") & "') "
                    '    Case "MICROS" ' Micros checks that are reopened get assigned the same transaction number plus a new version
                    '        sQry &= " AND (TransactionVersion = " & ResultRow.Item("TransactionVersion").ToString & ")"
                    'End Select
            End Select
        Catch ex As Exception
            LogEvent(EventLogEntryType.Error, "sqlUpdateSyncedFlag error.  Error: {1}", sPosType, ex)
            sQry = ""
        End Try
        sqlUpdateSyncedFlag = sQry
    End Function

    Private Sub FixSchema(ByVal POSType As String, ByVal Use_ODBC As Boolean, ByVal HeaderDataSet As DataSet)
        Dim sSQL As String
        Dim sError As String = String.Empty

        Try
            If HeaderDataSet.Tables(0).Columns.IndexOf("SyncToken") = -1 Then ' Column does not exist
                sSQL = "ALTER TABLE tbSalesMain ADD SyncToken VARCHAR(36) NULL"
                If MMSGeneric_Execute(Use_ODBC, sSQL, sError) Then
                    LogStatus("", "Schema adjusted: SyncToken column added")
                End If
            End If
            Select Case POSType.ToUpper
                Case "MICROS"
                    If HeaderDataSet.Tables(0).Columns.IndexOf("PriceLevel") = -1 Then ' Column does not exist
                        sSQL = "ALTER TABLE tbSalesMain ADD PriceLevel VARCHAR(255) NULL" & SybaseCommitWork
                        If MMSGeneric_Execute(Use_ODBC, sSQL, sError) Then
                            LogStatus("", "Schema adjusted: PriceLevel column added")
                        End If
                    End If
                    If HeaderDataSet.Tables(0).Columns.IndexOf("MicrosId") = -1 Then ' Column does not exist
                        sSQL = "ALTER TABLE tbSalesMain ADD MicrosId INTEGER NULL" & SybaseCommitWork
                        If MMSGeneric_Execute(Use_ODBC, sSQL, sError) Then
                            LogStatus("", "Schema adjusted: MicrosId column added")
                        End If
                    End If
                    If HeaderDataSet.Tables(0).Columns.IndexOf("TransactionVersion") = -1 Then ' Column does not exist
                        sSQL = "ALTER TABLE tbSalesMain ADD TransactionVersion int NULL DEFAULT 0" & SybaseCommitWork
                        If MMSGeneric_Execute(Use_ODBC, sSQL, sError) Then
                            LogStatus("", "Schema adjusted: TransactionVersion column added")
                            ' Change any NULL values to version 0
                            sSQL = "UPDATE tbSalesMain SET TransactionVersion = 0 WHERE TransactionVersion IS NULL" & SybaseCommitWork
                            MMSGeneric_Execute(Use_ODBC, sSQL, sError)
                            ' Create an index on this column
                            sSQL = "IF NOT EXISTS(SELECT I.* from systable T KEY JOIN sysindex I WHERE table_name = 'tbSalesMain' "
                            sSQL &= "AND index_name = 'ixTransactionVersion') THEN "
                            sSQL &= "  create index ixTransactionVersion on custom.tbSalesMain (TransactionVersion ASC) "
                            sSQL &= "END IF " & SybaseCommitWork
                            MMSGeneric_Execute(Use_ODBC, sSQL, sError)
                        End If
                    End If
                Case "PARJOURNAL", "PAREXALT4"
                    If HeaderDataSet.Tables(0).Columns("PollDate").DataType Is GetType(System.DateTime) Then
                        ' drop the PollDate index prior to adjusting schema!
                        sSQL = "DROP INDEX tbSalesMain.PollDate"
                        If Not MMSGeneric_Execute(Use_ODBC, sSQL, sError) Then
                            LogEvent(EventLogEntryType.Warning, "Failed to drop index 'tbSalesMain.PollDate'. SQL Error: {0}", sError)
                        End If

                        sSQL = "ALTER TABLE tbSalesMain ADD PollDateConvert VARCHAR(20) NULL"
                        If Not MMSGeneric_Execute(Use_ODBC, sSQL, sError) Then
                            LogEvent(EventLogEntryType.Warning, "Failed to add converting column 'PollDateConvert'. SQL Error: {0}", sError)
                        End If

                        sSQL = "BEGIN TRANSACTION " + vbCrLf _
                                + "UPDATE tbSalesMain SET PollDateConvert=CONVERT(VARCHAR(20),PollDate,106) + ' ' + CONVERT(VARCHAR(20),PollDate,108) " + vbCrLf _
                                + "ALTER TABLE tbSalesMain ALTER COLUMN PollDate VARCHAR(20) NULL " + vbCrLf _
                                + "UPDATE tbSalesMain SET PollDate=PollDateConvert " + vbCrLf _
                                + "ALTER TABLE tbSalesMain DROP COLUMN PollDateConvert " + vbCrLf + _
                                "COMMIT TRANSACTION"

                        If MMSGeneric_Execute(Use_ODBC, sSQL, sError) Then
                            LogStatus("", "Schema adjusted: PollDate column modified")
                            LogEvent(EventLogEntryType.Warning, "Schema adjusted. PollDate column modified")
                        Else
                            LogStatus("", "Schema adjust error: " + vbCrLf + sError)
                            LogEvent(EventLogEntryType.Warning, "Failed to alter column of PollDate to VARCHAR(). SQL Error: {0}", sError)
                        End If
                    End If
            End Select
        Catch ex As Exception
        End Try
    End Sub

    Private Function isDataToSend(ByRef SalesData As Boolean, ByRef AttendanceData As Boolean) As Boolean
        Dim Use_ODBC As Boolean = LiveLinkConfiguration.Use_ODBC_DB_Connect
        Dim sPOSType As String = LiveLinkConfiguration.POSType.Trim
        Dim HeaderDataSet As DataSet
        Dim sError As String = String.Empty
        Dim sSql As String

        SalesData = False
        AttendanceData = False

        isDataToSend = False

        Select Case sPOSType.ToUpper
            Case Is = "MICROS", "PIXELPOINT"
                _noMSA_Flag = True
            Case Else
                _noMSA_Flag = False
        End Select
        Try
            ' First check for Sales Data
            sSql = sqlSelectTopNonSyncedHeaders(sPOSType, 1, DataType.SalesData)
            If MMSGeneric_ListAll(Use_ODBC, sSql, "SalesData", HeaderDataSet, sError, _noMSA_Flag) Then
                If HeaderDataSet.Tables("SalesData").Rows.Count > 0 Then
                    SalesData = True
                End If
                ' Now fix any missing columns, indexes etc (This occurs when Micros overwrites the entire database eg Sumo)
                FixSchema(sPOSType, Use_ODBC, HeaderDataSet)
            Else
                LogEvent("Error checking for unsynced sales data " & vbCrLf & "Error: " & sError & vbCrLf & "SQL: " & sSql, EventLogEntryType.Error)
            End If

            ' Next check for Attendance Data
            sSql = sqlSelectTopNonSyncedHeaders(sPOSType, 1, DataType.AttendanceData)
            If MMSGeneric_ListAll(Use_ODBC, sSql, "AttendanceData", HeaderDataSet, sError, _noMSA_Flag) Then
                If HeaderDataSet.Tables("AttendanceData").Rows.Count > 0 Then
                    AttendanceData = True
                End If
            Else
                LogEvent("Error checking for unsynced attendance data " & vbCrLf & "Error: " & sError & vbCrLf & "SQL: " & sSql, EventLogEntryType.Error)
            End If
            isDataToSend = SalesData Or AttendanceData
        Catch ex As Exception
            LogEvent("Error checking for unsynced data (isDataToSend)" & vbCrLf & "Error: " & ex.Message, EventLogEntryType.Error)
        End Try
    End Function

    Private Function CreateTableName(ByVal sPosType As String, ByVal HeaderRow As DataRow) As String
        Select Case sPosType.Trim.ToUpper
            Case "QUICKEN"
                Return "SalesData-" & Format(CDate(HeaderRow.Item("Date_Time")), "yyyyMMddHHmmss") & "-" & HeaderRow.Item("TransactionID").ToString
            Case "MICROS", "PIXELPOINT", "PCAMERICA", "SIGNATURE", "COMPRIS", "INTOUCH", "PAREXALT4", "SILVERWARE", "OCTANE", "ALOHA"
                Return "SalesData-" & Format(CDate(HeaderRow.Item("PollDate")), "yyyyMMdd") & "-" & HeaderRow.Item("TransactionID").ToString & "." & HeaderRow.Item("TransactionVersion").ToString
            Case "NEWPOS", "IPOS"
                ' return the table name "SalesData-PollDate-RegisterID-TransactionID-TransactionVersion". e.g. "SalesData-20090115123500-113-1234-0"
                Return String.Format("SalesData-{0:yyyyMMddHHmmss}-{1}-{2}-{3}", CDate(HeaderRow("PollDate")), HeaderRow("RegisterID"), HeaderRow("TransactionID"), HeaderRow("TransactionVersion"))
            Case "MICROSOFTRMS"
                ' return the table name "SalesData-PollDate-TransactionID-TransactionVersion". e.g. "SalesData-20090115123500-1234-0"
                Return String.Format("SalesData-{0:yyyyMMddHHmmss}-{1}-{2}", CDate(HeaderRow("PollDate")), HeaderRow("TransactionID"), HeaderRow("TransactionVersion"))
            Case "ICG"
                ' return the table name "SalesData-PollDate-PosRegisterID-TransactionID-TransactionVersion". e.g. "SalesData-20090115123500-01Z-1234-0"
                Return String.Format("SalesData-{0:yyyyMMddHHmmss}-{1}-{2}-{3}", CDate(HeaderRow("PollDate")), HeaderRow("SubTypeDescription"), HeaderRow("TransactionID"), HeaderRow("TransactionVersion"))
            Case "PARJOURNAL", "PAN7700"
                Return "SalesData-" & Format(CDate(HeaderRow.Item("PollDate")), "yyyyMMdd") & "-" & HeaderRow.Item("TransactionID").ToString

            Case Else ' default POS type handling
                ' return the table name "SalesData-PollDate-TransactionID-TransactionVersion". e.g. "SalesData-20090115123500-1234-0"
                Return String.Format("SalesData-{0:yyyyMMddHHmmss}-{1}-{2}", CDate(HeaderRow("PollDate")), HeaderRow("TransactionID"), HeaderRow("TransactionVersion"))
        End Select
    End Function

#End Region

#Region " Pre-Send Processing"

    ' check to see if any pre-processing should be done on data before sending it
    Private Sub PreSendProcess(ByRef ds As DataSet)
        ' Check what type of processing to do on data, prior to sending it
        Select Case _preSendProcess.ToUpper
            Case "PLUMAP"
                ' at the moment, only PLUMAP is a presend process
                PLUMap(ds)
        End Select
    End Sub

#Region " PLU Mapping functions"

    Private Sub PLUMap(ByRef ds As DataSet)
        Try
            Dim TableIndex As Integer
            Dim RowIndex As Integer

            Dim ModifierPLU As Integer
            Dim ServiceType As Integer
            Dim ReplacementPLU As Integer

            Dim PluNode As XmlNode
            Dim PluMapNode As XmlNode

            ' If xml document hasn't been read, load it
            If _pluMapXmlDoc Is Nothing Then
                Try
                    _pluMapXmlDoc = New XmlDocument
                    _pluMapXmlDoc.Load(Path.Combine(Application.StartupPath, _pluMapFile))
                Catch ex As Exception
                    LogEvent("Error: Failed to read PLU Mappings from file:[" & Path.Combine(Application.StartupPath, _pluMapFile) & "] - Exception: " & ex.Message, EventLogEntryType.Error)
                    Return
                End Try
            End If

            ' loop through each transaction
            For TableIndex = 0 To (ds.Tables.Count - 1)
                With ds.Tables(TableIndex)
                    ' loop through each item in the transaction
                    For RowIndex = 0 To (.Rows.Count - 1)
                        ' only check Items
                        If EvalNull(.Rows(RowIndex).Item("RecordType")) = "I" Then
                            Try
                                ' check if there is a node with a matching plu value
                                PluNode = _pluMapXmlDoc.SelectSingleNode("plumap/plu[@value=""" & EvalNull(.Rows(RowIndex).Item("PLUCodeId")) & """]")
                                If Not PluNode Is Nothing Then
                                    ' if plu node is not nothing, then there are mappings available for this plu
                                    For Each PluMapNode In PluNode.ChildNodes
                                        ' allocate the childnodes modifier plu, servicetype and replacement plu...
                                        ' if they are not set, then they will default to -1
                                        ModifierPLU = GetNodeAttribute(PluMapNode, "modifier")
                                        ServiceType = GetNodeAttribute(PluMapNode, "servicetype")
                                        ReplacementPLU = GetNodeAttribute(PluMapNode, "replacement")

                                        ' replacement plu must be set, otherwise there is no point comparing
                                        If ReplacementPLU = -1 Then
                                            ' exit this for loop, continue onto next item
                                            LogEvent("Warning [PLUMap]: replacement PLU is not set for PLU=[" & EvalNull(.Rows(RowIndex).Item("PLUCodeId")) & "]", EventLogEntryType.Warning)
                                            Exit For
                                        End If

                                        ' check to see if there is a modifier item associated with it
                                        If (RowIndex + 1) < .Rows.Count Then
                                            ' check if the plu matches, (plu < 0, is classed as not required)
                                            If ModifierPLU < 0 Or _
                                                (EvalNull(.Rows(RowIndex + 1).Item("RecordType")) = "I" AndAlso _
                                                EvalNull(.Rows(RowIndex + 1).Item("PLUCodeId")) = ModifierPLU) Then

                                                ' check if the service type matches, (servicetype < 0, is classed as not set)
                                                If ServiceType < 0 Or _
                                                    ((Not ds.Tables(TableIndex).Rows(0).Item("Flag") Is DBNull.Value) AndAlso _
                                                    EvalNull(ds.Tables(TableIndex).Rows(0).Item("RecordType")) = "H" AndAlso _
                                                    ds.Tables(TableIndex).Rows(0).Item("Flag") = ServiceType) Then

                                                    ' MATCH FOUND -- Replace the PLU, then exit the for loop
                                                    .Rows(RowIndex).Item("PLUCodeId") = ReplacementPLU
                                                    Exit For
                                                End If
                                            End If
                                        Else
                                            ' check if the ModPLU is not required (plu < 0, is classed as not required)
                                            ' and that the service type is not required or matches. Service type is stored in the Flag field
                                            If ModifierPLU < 0 AndAlso _
                                                (ServiceType < 0 Or _
                                                (Not ds.Tables(TableIndex).Rows(0).Item("Flag") Is DBNull.Value) AndAlso _
                                                EvalNull(ds.Tables(TableIndex).Rows(0).Item("RecordType")) = "H" AndAlso _
                                                ds.Tables(TableIndex).Rows(0).Item("Flag") = ServiceType) Then

                                                ' MATCH FOUND -- Replace the PLU, then exit the for loop
                                                .Rows(RowIndex).Item("PLUCodeId") = ReplacementPLU
                                                Exit For
                                            End If
                                        End If
                                    Next
                                End If

                            Catch ex As Exception
                                LogEvent("Error: Failed to check for PLU mappings, Item PLU=[" & EvalNull(.Rows(RowIndex).Item("PLUCodeId")) & "] - Exception: " & ex.Message, EventLogEntryType.Error)
                            End Try
                        End If
                    Next
                End With
            Next
        Catch ex As Exception
            LogEvent("Error: Failed to check for PLU mappings - Exception: " & ex.Message, EventLogEntryType.Error)
        End Try
    End Sub

    Private Function GetNodeAttribute(ByVal Node As XmlNode, ByVal NodeName As String) As Integer
        GetNodeAttribute = -1
        Try
            Dim AttrNode As XmlNode
            AttrNode = Node.SelectSingleNode(NodeName)
            If Not AttrNode Is Nothing Then
                If AttrNode.Attributes.Count > 0 Then
                    GetNodeAttribute = AttrNode.Attributes(0).Value
                End If
            End If
        Catch ex As Exception
        End Try
    End Function

#End Region

#End Region

#Region "  Send & Proxy Functions "

    Private Sub LoadProxy(ByRef WebService As LiveLinkWebService)
        WebService.Proxy = Me.Proxy()
    End Sub

    Private Sub SendConfigurableData()
        Dim dataType As SendDataHelper.LiveLinkDataType
        ' check if summary data is enabled
        If (String.IsNullOrEmpty(LiveLinkConfiguration.SendDataType)) Then
            Exit Sub
        End If

        Try
            ' try to parse the SendDataType value into the LiveLinkDataType enum
            dataType = DirectCast([Enum].Parse(GetType(SendDataHelper.LiveLinkDataType), LiveLinkConfiguration.SendDataType, True), SendDataHelper.LiveLinkDataType)
        Catch ex As Exception
            LogEvent(EventLogEntryType.Warning, "Warning: Failed to parse send data type from value ({0}). Exception: {1}", LiveLinkConfiguration.SendDataType, ex.ToString)
            Exit Sub
        End Try

        Try
            UserAccess(False)
            ' check ok to send
            If Not Is_OK_To_Send() Then
                Exit Sub
            End If

            ' check there is data to send
            Dim headerRecords As DataTable = SendDataHelper.GetHeaders(dataType, _flowTxInBatch)
            If headerRecords Is Nothing OrElse headerRecords.Rows.Count <= 0 Then
                LogStatus("Waiting...", String.Format("No data ({0}) to send.", dataType))
                Exit Sub
            End If
            ' toggle visible buttons during send
            'ToggleSendButtons(False)
            CancelFlag = False

            ' get a new lease
            GetLease()
            ' check lease status
            If (String.IsNullOrEmpty(_sessionKey)) Then
                If (_conditionalSessionKey <> "") And (_sessionKey = String.Empty) Then
                    LogStatus("", "Site not yet activated.")
                End If
                ' exit send sub
                Exit Sub
            End If

            Dim posType As String = LiveLinkConfiguration.POSType
            Dim response As eRetailer.LiveLinkResponse
            Dim posData As New LiveLinkWebService
            Dim transferredCount As Integer = 0

            'Set the webservice reference
            posData.Url = LiveLinkConfiguration.WebService_URL
            LoadProxy(posData)

            'Loop through all transactions and synchronise 1 at a time. Can send more than transaction in a batch
            LogStatus("Started", String.Format("Preparing ({0}) batches - {1} batches of {2} transactions.", dataType, _flowIterations, _flowTxInBatch))

            For batchNo As Integer = 1 To _flowIterations
                Dim transferData As New DataSet
                Dim transferRecords As Integer = 0
                Dim dataList As New List(Of ILiveLinkData)

                ' Get first batch of header records
                headerRecords = SendDataHelper.GetHeaders(dataType, _flowTxInBatch)
                If headerRecords Is Nothing OrElse headerRecords.Rows.Count <= 0 Then
                    LogStatus("Waiting...", String.Format("No data ({0}) to send for batch number {1}", dataType, batchNo))
                    Exit For
                End If

                ' process each data header retrieved
                For Each header As DataRow In headerRecords.Rows
                    Dim dataRecord As ILiveLinkData
                    dataRecord = SendDataHelper.Create(dataType, header)

                    ' do any required application events
                    Application.DoEvents()

                    ' check if table already exists in dataset, this prevents duplicate table error which blocks sending
                    If transferData.Tables.Contains(dataRecord.TableName) Then
                        LogEvent(EventLogEntryType.Error, "Error [SendData]: Duplicate ({0}) table created ({1}). Marking as error and continueing", dataType, dataRecord.TableName)
                        LogStatus("Error", String.Format("Duplicate ({0}) table ({1}). Skipping transaction.", dataType, dataRecord.TableName))
                        dataRecord.UpdateSynced(SendDataHelper.Synced.DuplicateTransaction)
                        Continue For
                    End If

                    ' clone the data table into the dataset to transfer
                    transferData.Tables.Add(dataRecord.Data.Clone())
                    transferData.Merge(dataRecord.Data)
                    transferRecords += dataRecord.Data.Rows.Count

                    ' store the parsed data record in a list
                    dataList.Add(dataRecord)
                Next

                ' check if there is data to send
                If transferData.Tables.Count > 0 Then
                    ' log current progress
                    LogStatus("Sending data...", String.Format("Batch {0}. Sending {1} transactions ({2} records).", batchNo, transferData.Tables.Count, transferRecords))

                    ' Check for a valid lease. Get if not valid
                    GetLease()
                    Application.DoEvents()


                    ' get the data format & result data format
                    Dim dataFormat As String = dataList(0).DataFormat
                    Dim resultDataFormat As String = dataList(0).ResultDataFormat

                    ' send data to server (always use compression)
                    response = posData.Send_POSData2(_liveLinkID, _sessionKey, dataFormat, CompressDataSet(transferData), EntityID, posType)

                    ' process response
                    With response
                        ' check response code
                        If .ResponseCode <> eRetailer.LiveLinkResponseCode.Success Then
                            ' Response code returned was not Success
                            ProcessResponse(response, True)
                            Exit For ' Do not process any more
                        End If

                        ' set the message waiting flag
                        _messageWaiting = response.ResponseMessageWaiting

                        ' check valid response data
                        If .ResponseData Is Nothing Then
                            ' This should never occur - empty dataset of results
                            LogStatus("Error ...", "No results were returned")
                            BackOffCommunications(False)
                            Exit For ' Do not process any more
                        End If

                        ' check valid transaction response count was returned
                        If .ResponseData.Tables(resultDataFormat).Rows.Count <> transferData.Tables.Count Then
                            ' This should never occur
                            LogStatus("Warning...", "Response count differs from transactions sent")
                            BackOffCommunications(False)
                        End If

                        For Each result As DataRow In .ResponseData.Tables(resultDataFormat).Rows
                            ' match the returned result to the processed data record
                            Dim dataRecord As ILiveLinkData = SendDataHelper.CompareResultList(dataList, result)

                            If dataRecord Is Nothing Then
                                ' this should never happen
                                LogStatus("Warning...", String.Format("Failed to match result to transaction ({0}). TransactionID: {1}", dataType, result("TransactionID")))
                                Continue For
                            End If

                            ' update the transactions synced value with the returned result
                            dataRecord.UpdateSynced()

                            ' All is good at this point
                            '
                            ' It may still be possible to receive a successful response and have certain
                            ' transaction flagged as error or wrong entity
                            ' These transactions then get flagged as errors in tbSalesMain but processing continues.
                            ' Note these errors are not communications errors or web service failures,
                            ' but rather explicit errors returned by the web service.
                            '
                            Select Case result("Result")
                                Case LiveLinkTransactionResponseCode.Unsynced
                                    ' This should never happen
                                    LogStatus("", String.Format("Warning: Transaction [{0}] remains unsynced.", dataRecord.TransactionID))
                                Case LiveLinkTransactionResponseCode.DataError
                                    ' This should never happen
                                    LogStatus("", String.Format("Warning: Transaction [{0}] is invalid.", dataRecord.TransactionID))
                                Case LiveLinkTransactionResponseCode.WrongEntity
                                    ' This should never happen
                                    LogStatus("", String.Format("Warning: Transaction [{0}] is for the wrong store.", dataRecord.TransactionID))
                                Case LiveLinkTransactionResponseCode.Sync
                                    transferredCount += 1
                            End Select
                            ' mark successful communications
                            SuccessfulCommunications()
                        Next
                    End With
                End If
            Next

            ' log total transferred count
            If transferredCount > 0 Then
                LogStatus("Waiting...", String.Format("Transmission complete ({0}) - {1} successful transactions.", dataType, transferredCount))
            Else
                LogStatus("Waiting...", String.Format("Transmission complete ({0})", dataType))
            End If

        Catch ex As Exception
            LogEvent(EventLogEntryType.Error, "Error [SendData]: Occurred sending data ({0}) to server. Exception: {1}", dataType, ex.ToString)
            LogStatus("Error", ex.Message)
            ' on exception, back off communications
            BackOffCommunications(True)
        Finally
            ' reset buttons to "Send"
            'ToggleSendButtons(True)
        End Try
    End Sub


    Private Sub SendDataToServer()
        ' This function replaces SendDataToServer and DoQuickenPOS.
        ' It handles all POS types and all connection types
        Dim MyLiveLinkResponse As eRetailer.LiveLinkResponse
        Dim isSalesData As Boolean = False
        Dim isAttendancedata As Boolean = False
        UserAccess(False)
        If Is_OK_To_Send() Then
            If isDataToSend(isSalesData, isAttendancedata) Then
                'ToggleSendButtons(False)
                CancelFlag = False

                If Not _startupSent Then
                    ' send startup information
                    SendStartupInfo()
                End If

                GetLease()
                If _sessionKey <> String.Empty Then
                    SetStatus("Sending...")
                    'SEND POS DATA TO SERVER
                    Dim WSDataSet As DataSet
                    Dim HeaderDataSet As DataSet
                    Dim TXDataSet As DataSet
                    Dim sFieldListtbSalesMain As String = "*"
                    Dim dbResultOK As Boolean

                    'Get settings from config file
                    Dim sConnect As String = LiveLinkConfiguration.ConnectionString
                    Dim sConnect_ODBC As String = LiveLinkConfiguration.ODBC_DB_Connect
                    Dim Use_ODBC As Boolean = LiveLinkConfiguration.Use_ODBC_DB_Connect
                    Dim iDaysBeforeObsolete As Integer = LiveLinkConfiguration.DaysBeforeObsolete
                    If iDaysBeforeObsolete < 0 Then iDaysBeforeObsolete = 0 '0 means do not clear data
                    If (iDaysBeforeObsolete > 0) And (iDaysBeforeObsolete < 7) Then iDaysBeforeObsolete = 7 'Keep Minimum 7 days

                    Dim sPOSType As String = LiveLinkConfiguration.POSType.Trim
                    Select Case sPOSType.ToUpper
                        Case Is = "MICROS", "PIXELPOINT"
                            _noMSA_Flag = True
                        Case Else
                            _noMSA_Flag = False
                    End Select

                    sFieldListtbSalesMain = GettbSalesMainFields(sPOSType.ToUpper)

                    ' always use Token mode
                    Dim sDataFormat As String = "SalesDataWithToken"
                    Dim MyPosData As New LiveLinkWebService

                    'Set the webservice reference
                    MyPosData.Url = LiveLinkConfiguration.WebService_URL
                    LoadProxy(MyPosData)
                    Dim MyCount As Integer

                    'Select records which have not been synchronised. 
                    'First record must be header record "H" or a "1" for Quicken
                    'Followed by Item "I" and then Financial "F" records
                    Dim sQry As String
                    Dim sQryTX As String = ""
                    Dim sQrySynced As String = ""
                    Dim sError As String = ""

                    Dim i As Integer
                    Dim j As Integer
                    Dim RecordCount As Integer
                    Dim TxCount As Integer = 0
                    Dim AttendanceCount As Integer = 0
                    Dim HeaderRow As DataRow
                    Dim ResultRow As DataRow
                    Dim myTableName As String
                    Try
                        ' First process Sales Data
                        If isSalesData Then
                            'Loop through all transactions and synchronise 1 at a time. Can send more than transaction in a batch
                            LogStatus("Started", "Preparing batches - " & _flowIterations.ToString & " batches of " & _flowTxInBatch.ToString & " transactions.")

                            For i = 1 To _flowIterations
                                WSDataSet = New DataSet
                                RecordCount = 0
                                MarkDuplicateTransactionIdsAsErrors(sPOSType, Use_ODBC)
                                sQry = sqlSelectTopNonSyncedHeaders(sPOSType, _flowTxInBatch)
                                'Get all Transactions in this batch
                                If MMSGeneric_ListAll(Use_ODBC, sQry, "SalesData", HeaderDataSet, sError, _noMSA_Flag) Then
                                    ' For each header row, receive all associated records and add to a unique recordset
                                    For Each HeaderRow In HeaderDataSet.Tables("SalesData").Rows
                                        'need to set a guid for this record if it does not exist
                                        'If it exists, data has been sent previously but needs to be resent
                                        sQryTX = sqlUpdateSyncToken(sPOSType, HeaderRow)
                                        If sQryTX <> "" Then
                                            If Not MMSGeneric_Execute(Use_ODBC, sQryTX, sError) Then
                                                LogStatus("Error...", "Unable to set local token.")
                                                LogStatus("", sError)
                                                LogEvent("Unable to set local token" & vbCrLf & "Error: " & sError & vbCrLf & "SQL: " & sQryTX, EventLogEntryType.Error)
                                                BackOffCommunications(True)
                                                Exit For
                                            End If
                                        Else
                                            LogStatus("Error...", "Unable to set local token from header record.")
                                            BackOffCommunications(True)
                                            Exit For
                                        End If
                                        Application.DoEvents()
                                        'Get all records for this Transaction associated with header record
                                        sQryTX = sqlSelectAllRecordsForTransaction(sPOSType, Use_ODBC, HeaderRow, sFieldListtbSalesMain)
                                        If sQryTX <> "" Then
                                            myTableName = CreateTableName(sPOSType, HeaderRow)
                                            If MMSGeneric_ListAll(Use_ODBC, sQryTX, myTableName, TXDataSet, sError, _noMSA_Flag) Then
                                                Application.DoEvents()
                                                'Add this recordset to the WSDataset (there will be one recordset per transaction)
                                                With TXDataSet.Tables(myTableName)
                                                    WSDataSet.Tables.Add(.Clone)
                                                    WSDataSet.Merge(TXDataSet.Tables(myTableName))
                                                    RecordCount += .Rows.Count
                                                End With
                                            Else
                                                Try
                                                    LogStatus("Sending data to server...", "Unable to retrieve detail for transaction " & HeaderRow.Item("TransactionID").ToString & ")")
                                                Catch ex As Exception
                                                End Try
                                                LogStatus("", sError)
                                                BackOffCommunications(True)
                                            End If
                                        Else
                                            LogStatus("Error...", "Unable to retrieve transaction detail - (sql error)")
                                            BackOffCommunications(True)
                                        End If
                                        TXDataSet = Nothing
                                        If RecordCount > _flowMaxRecords Then
                                            ' If the maximum number of records tp be sent in a single batch has been exceed, then do not add
                                            ' any more transaction recordsets to the dataset to be transmitted
                                            Exit For
                                        End If
                                        Application.DoEvents()
                                        If WSDataSet.Tables.Count >= _flowTxInBatch Then
                                            Exit For ' Sybase 5 (PixelPoint) cannot limit the number of header rows, so check here
                                        End If
                                    Next

                                    ' ------------ END CREATION OF DATASET TO SEND -----------------
                                    If WSDataSet.Tables.Count > 0 Then ' If there was at least one transaction retrieved for sending

                                        '
                                        ' NEW PRE-SEND PROCESSING - MG 19/09/2006
                                        '
                                        PreSendProcess(WSDataSet)

                                        'Now process all records for this transaction.
                                        'Send Transaction and wait for result
                                        LogStatus("Sending data to server...", "Batch " & i.ToString & ". Sending " & WSDataSet.Tables.Count.ToString & " transactions (" & RecordCount.ToString & " records)")
                                        GetLease()
                                        Application.DoEvents()
                                        ' send data (always use compression)
                                        MyLiveLinkResponse = MyPosData.Send_POSData2(_liveLinkID, _sessionKey, sDataFormat, CompressDataSet(WSDataSet), EntityID, sPOSType)
                                        Application.DoEvents()
                                        'MyPosData.Send_POSData2(_liveLinkID, _sessionKey, sDataFormat, TxDataSet, sStoreID, sPOSType)
                                        '***MyCount = MyPosData.Send_POSDataMain(sClientCode, sSecurityCode, sDataFormat, TXDataSet, sStoreID, sPOSType)
                                        'If Success then set that record to synced
                                        With MyLiveLinkResponse
                                            If .ResponseCode = eRetailer.LiveLinkResponseCode.Success Then
                                                _messageWaiting = MyLiveLinkResponse.ResponseMessageWaiting
                                                If Not (.ResponseData Is Nothing) Then
                                                    ' There should be a reecord called "SalesDataResults" set containing the results of each transaction
                                                    If .ResponseData.Tables("SalesDataResults").Rows.Count <> WSDataSet.Tables.Count Then
                                                        ' This should never occur
                                                        LogStatus("Warning ...", "Response count differs from transactions sent")
                                                        BackOffCommunications(False)
                                                    End If
                                                    ' Continue to process any transactions returned, even if the number of results differs from the number sent
                                                    For Each ResultRow In .ResponseData.Tables("SalesDataResults").Rows
                                                        sQrySynced = sqlUpdateSyncedFlag(sPOSType, ResultRow)
                                                        If sQrySynced <> "" Then
                                                            If MMSGeneric_Execute(Use_ODBC, sQrySynced, sError) Then
                                                                ' All is good
                                                                ' It may still be possible to receive a successful response and have certain
                                                                ' transaction flagged as error or wrong entity
                                                                ' These transactions then get flagged as errors in tbSalesMain but processing continues.
                                                                ' Note these errors are not communications errors or web service failures,
                                                                ' but rather explicit errors returned by the web service.
                                                                Select Case ResultRow.Item("Result")
                                                                    Case LiveLinkTransactionResponseCode.Unsynced
                                                                        ' This should never happen
                                                                        LogStatus("", "Warning: Transaction [" & ResultRow.Item("TransactionID").ToString & "] remains unsynced.")
                                                                    Case LiveLinkTransactionResponseCode.DataError
                                                                        ' This should never happen
                                                                        LogStatus("", "Warning: Transaction [" & ResultRow.Item("TransactionID").ToString & "] is invalid.")
                                                                    Case LiveLinkTransactionResponseCode.WrongEntity
                                                                        ' This should never happen
                                                                        LogStatus("", "Warning: Transaction [" & ResultRow.Item("TransactionID").ToString & "] is for the wrong store.")
                                                                    Case LiveLinkTransactionResponseCode.Sync
                                                                        TxCount += 1
                                                                End Select
                                                                SuccessfulCommunications()
                                                            Else
                                                                Try
                                                                    LogStatus("Error ...", "Unable to save results for transaction " & ResultRow.Item("TransactionID").ToString)
                                                                Catch ex As Exception
                                                                End Try
                                                                LogStatus("", sError)
                                                                LogEvent("Unable to save results for transaction " & ResultRow.Item("TransactionID").ToString & vbCrLf & "Error: " & sError & vbCrLf & "SQL: " & sQrySynced, EventLogEntryType.Error)
                                                                BackOffCommunications(True)
                                                                ' Don't exit FOR loop here because other resuts might be OK
                                                            End If
                                                        Else
                                                            ' Error generating SQL code
                                                            LogStatus("Error ...", "Unable to save results - (sql error)")
                                                            BackOffCommunications(True)
                                                            ' Don't exit FOR loop here because other resuts might be OK
                                                        End If
                                                        Application.DoEvents()
                                                    Next
                                                Else
                                                    ' This should never occur - empty dataset of results
                                                    LogStatus("Error ...", "No results were returned")
                                                    BackOffCommunications(False)
                                                    Exit For ' Do not process any more
                                                End If
                                            Else
                                                ' Response code returned was not Success
                                                ' TODO: Handle other responses
                                                ProcessResponse(MyLiveLinkResponse, True)
                                                Exit For ' Do not process any more
                                            End If
                                        End With
                                    Else
                                        LogStatus("Waiting...", "No data to send.")
                                        Exit For
                                    End If
                                Else
                                    LogStatus("Error", "Unable to retrieve unsynced headers")
                                    LogStatus("", sError)
                                    BackOffCommunications(True)
                                    Exit For
                                End If
                                Application.DoEvents()
                                If CancelFlag Then
                                    LogStatus("", "Sending has been cancelled")
                                    Exit For
                                End If
                                WSDataSet = Nothing
                            Next
                        End If
                        '++++++++++++++++++ NOW SEND ATTENDANCE DATA ++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                        If (Not CancelFlag) And isAttendancedata Then
                            Dim AttendanceDataSet As DataSet

                            'Loop through all transactions and synchronise 1 at a time. Can send more than transaction in a batch
                            LogStatus("Started", "Preparing Attendance batches - " & _flowIterations.ToString & " batches of " & _flowMaxRecords.ToString & " records.")

                            sDataFormat = "AttendanceData"
                            For i = 1 To _flowIterations
                                RecordCount = 0
                                sQry = sqlSelectTopNonSyncedHeaders(sPOSType, _flowMaxRecords, DataType.AttendanceData)
                                'Get all records in this batch
                                If MMSGeneric_ListAll(Use_ODBC, sQry, "AttendanceData", AttendanceDataSet, sError, _noMSA_Flag) Then
                                    ' For each header row, receive all associated records and add to a unique recordset
                                    j = 0
                                    For Each HeaderRow In AttendanceDataSet.Tables("AttendanceData").Rows
                                        'If TokenMode Then ' TokenMode is now mandatory
                                        'need to set a guid for this record if it does not exist
                                        'If it exists, data has been sent previously but needs to be resent
                                        sQryTX = sqlUpdateSyncToken(sPOSType, HeaderRow, DataType.AttendanceData)
                                        If sQryTX <> "" Then
                                            If MMSGeneric_Execute(Use_ODBC, sQryTX, sError) Then
                                                ' Now return the SyncToken and update the dataset
                                                sQryTX = sqlGetSyncToken(sPOSType, HeaderRow, DataType.AttendanceData)
                                                If sQryTX <> "" Then
                                                    If MMSGeneric_ListAll(Use_ODBC, sQryTX, "SyncToken", TXDataSet, sError, _noMSA_Flag) _
                                                    AndAlso TXDataSet.Tables("SyncToken").Rows.Count = 1 Then
                                                        HeaderRow.Item("SyncToken") = TXDataSet.Tables("SyncToken").Rows(0).Item("SyncToken")
                                                    Else
                                                        LogStatus("Error...", "Unable to set retrieve local token (attendance).")
                                                        LogStatus("", sError)
                                                        BackOffCommunications(True)
                                                        Exit For
                                                    End If
                                                Else
                                                    LogStatus("Error...", "Unable to retrieve local token (attendance) from header record.")
                                                    BackOffCommunications(True)
                                                    Exit For
                                                End If
                                            Else
                                                LogStatus("Error...", "Unable to set local token (attendance).")
                                                LogStatus("", sError)
                                                LogEvent("Unable to set local token (attendance)" & vbCrLf & "Error: " & sError & vbCrLf & "SQL: " & sQryTX, EventLogEntryType.Error)
                                                BackOffCommunications(True)
                                                Exit For
                                            End If
                                        Else
                                            LogStatus("Error...", "Unable to set local token (attendance) from header record.")
                                            BackOffCommunications(True)
                                            Exit For
                                        End If
                                        'End If
                                        Application.DoEvents()
                                        j += 1
                                        If j >= _flowMaxRecords Then
                                            Exit For ' Sybase 5 (PixelPoint) cannot limit the number of header rows, so check here
                                        End If
                                    Next
                                    ' ------------ END CREATION OF ATTENDANCE DATASET TO SEND -----------------
                                    If AttendanceDataSet.Tables("AttendanceData").Rows.Count > 0 Then ' If there was at least one attendance record retrieved for sending
                                        'Now process all attendance records
                                        'Send Transaction and wait for result
                                        LogStatus("Sending attendance data to server...", "Attendance Batch " & i.ToString & ". Sending " & AttendanceDataSet.Tables("AttendanceData").Rows.Count & " records")
                                        GetLease()
                                        Application.DoEvents()
                                        ' send data (always use compression)
                                        MyLiveLinkResponse = MyPosData.Send_POSData2(_liveLinkID, _sessionKey, sDataFormat, CompressDataSet(AttendanceDataSet), EntityID, sPOSType)
                                        Application.DoEvents()
                                        'MyPosData.Send_POSData2(_liveLinkID, _sessionKey, sDataFormat, TxDataSet, sStoreID, sPOSType)
                                        '***MyCount = MyPosData.Send_POSDataMain(sClientCode, sSecurityCode, sDataFormat, TXDataSet, sStoreID, sPOSType)
                                        'If Success then set that record to synced
                                        With MyLiveLinkResponse
                                            If .ResponseCode = eRetailer.LiveLinkResponseCode.Success Then
                                                If Not (.ResponseData Is Nothing) Then
                                                    ' There should be a datatable called "SalesDataResults" containing the results of each transaction
                                                    If .ResponseData.Tables("AttendanceDataResults").Rows.Count <> AttendanceDataSet.Tables("AttendanceData").Rows.Count Then
                                                        ' This should never occur
                                                        LogStatus("Warning ...", "Response count differs from attendance records sent")
                                                        BackOffCommunications(False)
                                                    End If
                                                    ' Continue to process any transactions returned, even if the number of results differs from the number sent
                                                    For Each ResultRow In .ResponseData.Tables("AttendanceDataResults").Rows
                                                        sQrySynced = sqlUpdateSyncedFlag(sPOSType, ResultRow, DataType.AttendanceData)
                                                        If sQrySynced <> "" Then
                                                            If MMSGeneric_Execute(Use_ODBC, sQrySynced, sError) Then
                                                                ' All is good
                                                                ' It may still be possible to receive a successful response and have certain
                                                                ' records flagged as error or wrong entity
                                                                ' These transactions then get flagged as errors in tbSalesMain but processing continues.
                                                                ' Note these errors are not communications errors or web service failures,
                                                                ' but rather explicit errors returned by the web service.
                                                                Select Case ResultRow.Item("Result")
                                                                    Case LiveLinkTransactionResponseCode.Unsynced
                                                                        ' This should never happen
                                                                        LogStatus("", "Warning: Record [" & ResultRow.Item("SalesMainID").ToString & "] remains unsynced.")
                                                                    Case LiveLinkTransactionResponseCode.DataError
                                                                        ' This should never happen
                                                                        LogStatus("", "Warning: Record [" & ResultRow.Item("SalesMainID").ToString & "] is invalid.")
                                                                    Case LiveLinkTransactionResponseCode.WrongEntity
                                                                        ' This should never happen
                                                                        LogStatus("", "Warning: Record [" & ResultRow.Item("SalesMainID").ToString & "] is for the wrong store.")
                                                                    Case LiveLinkTransactionResponseCode.Sync
                                                                        AttendanceCount += 1
                                                                End Select
                                                                SuccessfulCommunications()
                                                            Else
                                                                Try
                                                                    LogStatus("Error ...", "Unable to save results for record " & ResultRow.Item("SalesMainID").ToString)
                                                                Catch ex As Exception
                                                                End Try
                                                                LogStatus("", sError)
                                                                LogEvent("Unable to save results for record " & ResultRow.Item("SalesMainID").ToString & vbCrLf & "Error: " & sError & vbCrLf & "SQL: " & sQrySynced, EventLogEntryType.Error)
                                                                BackOffCommunications(True)
                                                                ' Don't exit FOR loop here because other resuts might be OK
                                                            End If
                                                        Else
                                                            ' Error generating SQL code
                                                            LogStatus("Error ...", "Unable to save attendance results - (sql error)")
                                                            BackOffCommunications(True)
                                                            ' Don't exit FOR loop here because other resuts might be OK
                                                        End If
                                                        Application.DoEvents()
                                                    Next
                                                Else
                                                    ' This should never occur - empty dataset of results
                                                    LogStatus("Error ...", "No attendance results were returned")
                                                    BackOffCommunications(False)
                                                    Exit For ' Do not process any more
                                                End If
                                            Else
                                                ' Response code returned was not Success
                                                ' TODO: Handle other responses
                                                ProcessResponse(MyLiveLinkResponse, True)
                                                Exit For ' Do not process any more
                                            End If
                                        End With
                                    Else
                                        LogStatus("Waiting...", "No data to send.")
                                        Exit For
                                    End If
                                Else
                                    LogStatus("Error", "Unable to retrieve attendance data")
                                    LogStatus("", sError)
                                    BackOffCommunications(True)
                                    Exit For
                                End If
                                Application.DoEvents()
                                If CancelFlag Then
                                    LogStatus("", "Sending has been cancelled")
                                    Exit For
                                End If
                                AttendanceDataSet = Nothing
                            Next
                        End If
                    Catch ex As WebException
                        LogStatus("Error", ex.Message)
                        BackOffCommunications(False)
                    Catch ex As Exception
                        LogStatus("Error", ex.ToString)
                        BackOffCommunications(True)
                    End Try
                    If isSalesData Then
                        If TxCount > 0 Then
                            LogStatus("Waiting...", "Sales transmission complete - " & TxCount.ToString & " successful transactions.")
                        Else
                            LogStatus("Waiting...", "Sales transmission complete")
                        End If
                    End If
                    If isAttendancedata Then
                        If AttendanceCount > 0 Then
                            LogStatus("Waiting...", "Attendance transmission complete - " & AttendanceCount.ToString & " successful records.")
                        Else
                            LogStatus("Waiting...", "Attendance transmission complete")
                        End If
                    End If
                    SetStatus("")
                Else
                    If (_conditionalSessionKey <> "") And (_sessionKey = String.Empty) Then
                        LogStatus("", "Site not yet activated.")
                    End If
                End If
            Else
                LogStatus("Waiting...", "No data to send.")
            End If
        End If
    End Sub

#End Region


#Region "  Logging functionality "

    Private Function AddLogInfo(ByRef LogDataSet As DataSet, ByVal MessageType As SessionMessage, ByVal MessageName As String, ByVal MessageDescription As String)
        Dim InfoRow As DataRow
        Try
            InfoRow = LogDataSet.Tables(LogDataFormat).NewRow
            InfoRow.Item("MessageType") = MessageType
            InfoRow.Item("MessageName") = MessageName
            InfoRow.Item("MessageDescription") = MessageDescription
            LogDataSet.Tables(LogDataFormat).Rows.Add(InfoRow)
        Catch ex As Exception
        End Try
    End Function

    Private Sub CreateLogTable(ByRef LogDataSet As DataSet)
        Try
            LogDataSet.Tables.Add(New DataTable(LogDataFormat))
            LogDataSet.Tables(LogDataFormat).Columns.Add(New DataColumn("MessageType", System.Type.GetType("System.String")))
            LogDataSet.Tables(LogDataFormat).Columns.Add(New DataColumn("MessageName", System.Type.GetType("System.String")))
            LogDataSet.Tables(LogDataFormat).Columns.Add(New DataColumn("MessageDescription", System.Type.GetType("System.String")))
        Catch ex As Exception
        End Try
    End Sub

    Private Function SendLogInfo(ByVal MessageType As SessionMessage, ByVal MessageName As String, ByVal MessageDescription As String) As Boolean
        GetLease()
        SendLogInfo = False
        If HasConditionalLease() OrElse Is_OK_To_Send() Then
            Try
                If _conditionalSessionKey <> "" Then
                    Dim WSDataSet As New DataSet
                    CreateLogTable(WSDataSet)
                    AddLogInfo(WSDataSet, MessageType, MessageName, MessageDescription)
                    SendLogInfo = SendLogInfo(WSDataSet) ' Calls overloaded function with same name
                End If
            Catch ex As Exception
            End Try
        End If
    End Function

    Private Function SendLogInfo(ByVal LogDataSet As DataSet) As Boolean
        GetLease()
        SendLogInfo = False
        If HasConditionalLease() OrElse Is_OK_To_Send() Then
            If _conditionalSessionKey <> String.Empty Then
                Dim MyLogInfo As New LiveLinkWebService
                Dim sPOSType As String = LiveLinkConfiguration.POSType.Trim
                Dim MyLiveLinkResponse As eRetailer.LiveLinkResponse

                MyLogInfo.Url = LiveLinkConfiguration.WebService_URL
                LoadProxy(MyLogInfo)
                Dim Retries As Integer
                For Retries = 1 To 2 ' The lease may have expired due to long running activities such as large file downloads
                    ' send data to server (always use compression)
                    MyLiveLinkResponse = MyLogInfo.Send_POSData2(_liveLinkID, _conditionalSessionKey, "Information", CompressDataSet(LogDataSet), EntityID, sPOSType)
                    Select Case MyLiveLinkResponse.ResponseCode
                        Case eRetailer.LiveLinkResponseCode.Success
                            SendLogInfo = True
                            _messageWaiting = MyLiveLinkResponse.ResponseMessageWaiting
                            UpdateLastSuccessfulCommunication()
                            Exit For
                        Case eRetailer.LiveLinkResponseCode.LeaseExpired
                            If Retries = 1 Then
                                ExpireLease()
                                GetLease()
                            End If
                        Case Else
                            SendLogInfo = False
                            Exit For
                    End Select
                Next
            End If
        End If
    End Function

#End Region

#Region "Setup Information"

    Function SendStartupInfo() As Boolean
        Dim Msg As String
        Dim LogDataSet As New DataSet
        CreateLogTable(LogDataSet)
        Msg = "Live Link was started at " & Format(LiveLinkStartTime, "dd-MMM-yyyy HH:mm:ss") & " (" & Format(LiveLinkStartTime.ToUniversalTime, "dd-MMM-yyyy HH:mm:ss") & " UTC)"
        AddLogInfo(LogDataSet, SessionMessage.msg_Information, "Live Link started", Msg)
        AddLogInfo(LogDataSet, SessionMessage.msg_Information, "Live Link Version", Application.ProductVersion)
        AddLogInfo(LogDataSet, SessionMessage.msg_Information, "Live Link Directory", Application.StartupPath)
        Try
            ' if the store number argument is supplied, and auto-activation is enabled. Log the information
            If (Not String.IsNullOrEmpty(_argumentStoreNumber)) Then
                AddLogInfo(LogDataSet, SessionMessage.msg_Information, "Auto-Activation Store Number", _argumentStoreNumber)
            End If

            Dim objSysInfo As New clsSysInfo
            With objSysInfo
                AddLogInfo(LogDataSet, SessionMessage.msg_Information, "Computer Name", .ComputerName)
                AddLogInfo(LogDataSet, SessionMessage.msg_Information, "Computer Manufacturer", .Manufacturer)
                AddLogInfo(LogDataSet, SessionMessage.msg_Information, "Computer Model", .Model)
                AddLogInfo(LogDataSet, SessionMessage.msg_Information, "OS Name", .OsName)
                AddLogInfo(LogDataSet, SessionMessage.msg_Information, "OS Version", .OSVersion)
                AddLogInfo(LogDataSet, SessionMessage.msg_Information, "System Type", .SystemType)
                AddLogInfo(LogDataSet, SessionMessage.msg_Information, "Total Physical Memory", .TotalPhysicalMemory)
                AddLogInfo(LogDataSet, SessionMessage.msg_Information, "Windows Directory", .WindowsDirectory)
                AddLogInfo(LogDataSet, SessionMessage.msg_Information, "DotNet 1.1 Installed", IsDotNetInstalled("1.1.4322"))
                AddLogInfo(LogDataSet, SessionMessage.msg_Information, "DotNet 2.0 Installed", IsDotNetInstalled("2.0.50727"))
                If .FixedDisk1 <> "" Then
                    AddLogInfo(LogDataSet, SessionMessage.msg_Information, "Fixed Disk 1", .FixedDisk1)
                End If
                If .FixedDisk2 <> "" Then
                    AddLogInfo(LogDataSet, SessionMessage.msg_Information, "Fixed Disk 2", .FixedDisk2)
                End If
                If .FixedDisk3 <> "" Then
                    AddLogInfo(LogDataSet, SessionMessage.msg_Information, "Fixed Disk 3", .FixedDisk3)
                End If
            End With
        Catch ex As Exception
            LogStatus("", ex.Message)
            AddLogInfo(LogDataSet, SessionMessage.msg_Warning, "System Info", "Unable to retrieve system information")
        End Try

        Try
            If SendLogInfo(LogDataSet) Then
                _startupSent = True
            End If
        Catch ex As WebException
            LogStatus("Error", ex.Message)
            BackOffCommunications(False)
        Catch ex As Exception
            LogStatus("Error", "Unable to send Startup Info")
        End Try
    End Function

    Private Function IsDotNetInstalled(ByVal version As String) As String
        Dim result As String = RegRead(Registry.LocalMachine, "Software\Microsoft\NET Framework Setup\NDP\v" + version, "Install", "Unknown")
        Return result
    End Function

#End Region


#Region "POS Commands"


    Private Function SavePOSCommandData(ByVal commandData As String, ByVal messageID As Integer) As Boolean
        If (LiveLinkConfiguration.YisEnabled) Then
            Return HandleYISCommandData(commandData, messageID)
        End If

        If (LiveLinkConfiguration.LiveLinkPOSCommandDataEnabled) Then
            Return HandlePOSCommandData(commandData, messageID)
        End If
        Return True
    End Function
    Private Function HandlePOSCommandData(ByVal request As String, ByVal messageID As Integer) As Boolean
        Dim status As Boolean = True
        Dim type As String = "POSCommand"
        Try

            If (String.IsNullOrEmpty(request)) Then
                LogEvent(EventLogEntryType.Error, "Failed to parse POS command data:[{0}]", request)
                Return False
            End If

            ' add the POS Command data to tbPOSCommandData
            Mx.Services.LiveLink.POSCommandService.InsertData(New Mx.BusinessObjects.LiveLink.POSCommandData(Nothing, type, messageID, request, String.Empty, DateTime.Now, DateTime.Now))
            ' delete any old built up data
            If (LiveLinkConfiguration.DaysBeforeObsolete <> 0) Then
                Mx.Services.LiveLink.POSCommandService.DeleteObsoleteData(LiveLinkConfiguration.DaysBeforeObsolete, type)
            End If

        Catch ex As Exception
            LogEvent(EventLogEntryType.Error, "Exception occurred handling POS command data:[{0}]. Exception: {1}", request, ex.ToString)
            status = False
        End Try
        Return status
    End Function

    ' send commands to the POS... SOD, EOD etc.
    Private Function SendPOSCommand(ByVal command As String, ByVal messageID As Integer) As Boolean
        Dim posType As String = LiveLinkConfiguration.POSType.Trim.ToUpper
        SendPOSCommand = True

        Try
            ' check if YIS is enabled
            If (LiveLinkConfiguration.YisEnabled) Then
                ' only do yis commands if YIS enabled
                Return HandleYISCommand(command, messageID)
            End If

            Select Case (posType)
                Case "COMPRIS"
                    ' check compris configuration is available
                    If (Not _importConfigurationList.ContainsKey(LiveLinkImportType.Compris)) Then
                        Throw New Exception("Compris is not enabled, can not process POS command")
                    End If

                    Dim comprisImportConfig As ComprisConfiguration = DirectCast(_importConfigurationList(LiveLinkImportType.Compris), ComprisConfiguration)
                    ' COMPRIS is the only POS that supports commands so far
                    Dim ComprisPOS As New ComprisActiveX(LiveLinkConfiguration.ComprisActiveMeKeySOD, _
                                                            LiveLinkConfiguration.ComprisActiveMeKeyEOD, _
                                                            LiveLinkConfiguration.ComprisActiveScriptPreSOD, _
                                                            LiveLinkConfiguration.ComprisActiveScriptPostSOD, _
                                                            LiveLinkConfiguration.ComprisActiveScriptPreEOD, _
                                                            LiveLinkConfiguration.ComprisActiveScriptPostEOD, _
                                                            LiveLinkConfiguration.ComprisActiveRegisterCount)


                    Dim CmdParameters() As String = command.Split(";")
                    Select Case CmdParameters(0).ToUpper
                        Case "SOD"
                            LogStatus("Message", "Sending SOD to POS")
                            LogEvent("Received SOD command from server. Sending to POS", EventLogEntryType.Information)
                            ' send a start of day to the compris POS
                            '
                            Dim ResultList As Object() = Nothing
                            Dim Result As Integer
                            Dim TerminalNo As Integer = 1
                            Dim Message As String = String.Empty
                            Dim FailedCount As Integer = 0
                            Dim FailedTerminalNo As Integer = 0

                            Try
                                Select Case ComprisPOS.SendOpen(ResultList)
                                    Case ComprisStatusSOD.Success
                                        If ResultList Is Nothing Then
                                            comprisImportConfig.RaisePOSError("Failed to send START OF DAY", ComprisActiveMessageType.sod, ComprisActiveErrorType.NoResult)
                                            SendPOSCommand = False
                                        Else
                                            Dim errorTerminals As New List(Of Short)
                                            For Each Result In ResultList
                                                If Result = 0 Then
                                                    LogStatus("Message", "Terminal #" & TerminalNo & " - SENT")
                                                Else
                                                    LogStatus("Message", "Terminal #" & TerminalNo & " - FAILED")
                                                    SendPOSCommand = False

                                                    FailedCount += 1
                                                    FailedTerminalNo = TerminalNo
                                                    errorTerminals.Add(CShort(TerminalNo))
                                                End If
                                                TerminalNo += 1
                                            Next

                                            If errorTerminals.Count > 0 Then
                                                comprisImportConfig.RaisePOSError(Nothing, ComprisActiveMessageType.sod, ComprisActiveErrorType.TerminalFailure, errorTerminals)
                                            Else
                                                LogEvent("Successfullly Sent SOD server command to POS", EventLogEntryType.Information)
                                            End If

                                            If FailedCount = 1 Then
                                                comprisImportConfig.RaisePOSError("Failed to send START OF DAY to terminal #" & FailedTerminalNo, Nothing, Nothing)
                                            ElseIf FailedCount > 1 Then
                                                comprisImportConfig.RaisePOSError("Failed to send START OF DAY to multiple terminals", Nothing, Nothing)
                                            End If
                                        End If

                                    Case ComprisStatusSOD.DayAlreadyOpen
                                        comprisImportConfig.RaisePOSError("Failed to send START OF DAY. Day is already open!", ComprisActiveMessageType.sod, ComprisActiveErrorType.DayAlreadyOpen)
                                        SendPOSCommand = True ' No need to throw error.

                                    Case ComprisStatusSOD.NoResponse
                                        comprisImportConfig.RaisePOSError("Failed to send START OF DAY. Got failure response.", ComprisActiveMessageType.sod, ComprisActiveErrorType.NoResponse)
                                        SendPOSCommand = False


                                    Case Else
                                        comprisImportConfig.RaisePOSError("Failed to send START OF DAY", ComprisActiveMessageType.sod, ComprisActiveErrorType.Generic)
                                        SendPOSCommand = False
                                End Select

                            Catch ex As Exception
                                LogEvent("Exception: [SendPOSCommand] - (" & posType & ") - " & ex.Message, EventLogEntryType.Error)
                                LogStatus("Message", "SOD Exception - " & ex.Message)
                                comprisImportConfig.RaisePOSError("Failed to send START OF DAY.", ComprisActiveMessageType.sod, ComprisActiveErrorType.Exception)
                                SendPOSCommand = False
                            End Try

                        Case "EOD"
                            LogStatus("Message", "Sending EOD to POS")
                            LogEvent("Received EOD command from server. Sending to POS", EventLogEntryType.Information)
                            '
                            ' send a end of day to the compris POS
                            '
                            ' If the length of the parameter list is <= 1 or parameter value <> "NOCHECK", then check online status by default
                            '
                            Try
                                ' check online status of terminals
                                Dim offlineTerminals As List(Of Short) = ComprisPOS.CheckOnlineStatus()
                                Dim openOrdersExist As Boolean = ComprisPOS.CheckOpenOrders()

                                If (Not LiveLinkConfiguration.ComprisProcessEODIgnoreOpenOrdersOrOfflineTerminals) Then
                                    If CmdParameters.Length <= 1 OrElse CmdParameters(1).ToUpper <> "NOCHECK" Then
                                        ' check online status of terminals
                                        ' raise error if terminals are offline
                                        If offlineTerminals IsNot Nothing Then
                                            LogEvent("Info: There are terminals offline. Cannot send EOD", EventLogEntryType.Warning)
                                            comprisImportConfig.RaisePOSError("There are terminals offline. Cannot send END OF DAY", ComprisActiveMessageType.eod, ComprisActiveErrorType.TerminalsOffline, offlineTerminals)
                                            SendPOSCommand = False
                                            Exit Select
                                        End If
                                    End If

                                    If openOrdersExist Then
                                        LogEvent("Info: There are currently open orders. Cannot send EOD", EventLogEntryType.Warning)
                                        comprisImportConfig.RaisePOSError("There are currently open orders. Cannot send END OF DAY", ComprisActiveMessageType.eod, ComprisActiveErrorType.OrdersOpen)
                                        SendPOSCommand = False
                                        Exit Select
                                    End If
                                Else
                                    If (offlineTerminals IsNot Nothing OrElse openOrdersExist) Then
                                        Dim terminalList As String = If(offlineTerminals IsNot Nothing, SplitIds(offlineTerminals), String.Empty)
                                        LogEvent(String.Format("EOD completed. But there are currently open orders or terminals offline : {0}", terminalList), EventLogEntryType.Warning)
                                        'Send an ALERT message
                                        Services.LiveLink.SalesMainService.InsertAlertMessage(EntityID, AlertRecordSubType.Warning, "COMPRIS EOD Process Warning", String.Format("There are {0}Open order(s) and ({1}) offline terminal(s)", If(openOrdersExist, "", "NO "), If(String.IsNullOrEmpty(terminalList), "NO", terminalList)))
                                    End If
                                End If

                                Dim ResultList As Object() = Nothing
                                Dim Result As Integer
                                Dim TerminalNo As Integer = 1
                                Dim FailedCount As Integer = 0
                                Dim FailedTerminalNo As Integer = 0

                                Select Case ComprisPOS.SendClose(ResultList)
                                    Case ComprisStatusEOD.Success
                                        If ResultList Is Nothing Then
                                            comprisImportConfig.RaisePOSError("Failed to send END OF DAY", ComprisActiveMessageType.eod, ComprisActiveErrorType.NoResult)
                                            SendPOSCommand = False
                                        Else
                                            Dim errorTerminals As New List(Of Short)
                                            For Each Result In ResultList
                                                If Result = 0 Then
                                                    LogStatus("Message", "Terminal #" & TerminalNo & " - SENT")
                                                Else
                                                    LogStatus("Message", "Terminal #" & TerminalNo & " - FAILED")
                                                    SendPOSCommand = False

                                                    FailedCount += 1
                                                    FailedTerminalNo = TerminalNo
                                                    errorTerminals.Add(CShort(TerminalNo))
                                                End If
                                                TerminalNo += 1
                                            Next

                                            If errorTerminals.Count > 0 Then
                                                comprisImportConfig.RaisePOSError(Nothing, ComprisActiveMessageType.eod, ComprisActiveErrorType.TerminalFailure, errorTerminals)
                                            Else
                                                LogEvent("Successfullly Sent EOD server command to POS", EventLogEntryType.Information)
                                            End If

                                            If FailedCount = 1 Then
                                                comprisImportConfig.RaisePOSError("Failed to send END OF DAY to terminal #" & FailedTerminalNo, Nothing, Nothing)
                                            ElseIf FailedCount > 1 Then
                                                comprisImportConfig.RaisePOSError("Failed to send END OF DAY to multiple terminals", Nothing, Nothing)
                                            End If
                                        End If

                                    Case ComprisStatusEOD.NoResponse
                                        comprisImportConfig.RaisePOSError("Failed to send END OF DAY. Got failure response.", ComprisActiveMessageType.eod, ComprisActiveErrorType.NoResponse)
                                        SendPOSCommand = False

                                    Case Else
                                        comprisImportConfig.RaisePOSError("Failed to send END OF DAY.", ComprisActiveMessageType.eod, ComprisActiveErrorType.Generic)
                                        SendPOSCommand = False
                                End Select

                            Catch ex As Exception
                                LogEvent("Exception: [SendPOSCommand] - (" & posType & ") - " & ex.Message, EventLogEntryType.Error)
                                LogStatus("Message", "EOD Exception - " & ex.Message)
                                comprisImportConfig.RaisePOSError("Failed to send END OF DAY.", ComprisActiveMessageType.eod, ComprisActiveErrorType.Exception)
                                SendPOSCommand = False
                            End Try

                        Case "PDDFILES"
                            ' if PddFiles command received, then force a register of the DLL
                            ComprisInterop.RegisterPddFilesDLL(True)
                    End Select

                Case "IPOS"
                    If (Not _importConfigurationList.ContainsKey(LiveLinkImportType.iPOS)) Then
                        Return False
                    End If

                    Dim iposImportConfig As iPOSConfiguration = DirectCast(_importConfigurationList(LiveLinkImportType.iPOS), iPOSConfiguration)
                    ' if update flag is set to true, then there is already an update in progress. Do not action another!
                    If (iposImportConfig.UpdatingDatabase) Then
                        LogEvent("Error: iPOS database update in progress, cannot action remote update.")
                        Return False
                    End If
                    Try
                        iposImportConfig.UpdatingDatabase = True
                        SetStatus("Updating iPOS...")

                        ' iPOS updates (e.g. Products/Staff)
                        If (Not iPOSUpdateHelper.ProcessUpdate(command, _liveLinkID, _sessionKey, CInt(EntityID))) Then
                            SendPOSCommand = False
                        End If
                    Catch ex As Exception
                        LogEvent(EventLogEntryType.Error, "Exception: [iPOS Update Command] - ({0}) - {1}", posType, ex.ToString)
                        SendPOSCommand = False
                    Finally
                        iposImportConfig.UpdatingDatabase = False
                        SetStatus("")
                    End Try
            End Select

            ' check if a ZK fingerprint command is requested
            If (_importConfigurationList.ContainsKey(LiveLinkImportType.ZKFingerprint)) Then
                Dim zkImportConfig As ZKFingerprintConfiguration = DirectCast(_importConfigurationList(LiveLinkImportType.ZKFingerprint), ZKFingerprintConfiguration)
                If (zkImportConfig.Fingerprint Is Nothing) Then
                    ' initialise the fingerprint device interface
                    zkImportConfig.Fingerprint = New zkFingerPrint(EntityID, LiveLinkConfiguration.ZKFingerprintIP, LiveLinkConfiguration.ZKFingerprintPort)
                End If

                ' attempt to process the command
                If Not zkImportConfig.Fingerprint.ProcessCommand(command) Then
                    SendPOSCommand = False
                End If
            End If

        Catch ex As Exception
            LogEvent("Exception: [SendPOSCommand] - (" & posType & ") - " & ex.Message, EventLogEntryType.Error)
            LogStatus("Message", "POS Command Error - " & ex.Message)
            SendPOSCommand = False
        End Try
    End Function


#End Region


    Private Function WriteNewConfigSettings(ByVal newConfigs As String) As Boolean
        Try
            ' New configuration settings are returned as name value pairs separated by semicolons
            ' eg.  UseMicros=Yes;Version=;"ServiceTypes=PickUp=1;TakeOut=2" ...
            '      yields (1) Key: UseMicros, Value: Yes
            '             (2) Key: ServiceTypes, Value: Pickup=1;TakeOut=2
            '      Note that ; are ignored when surrounded by quotes
            '      Note also, only the first equals sign is used to split
            Dim newSettings As New List(Of String())

            Using sr As New StringReader(newConfigs)
                Using textParser As New Microsoft.VisualBasic.FileIO.TextFieldParser(sr)
                    textParser.TextFieldType = Microsoft.VisualBasic.FileIO.FieldType.Delimited
                    textParser.Delimiters = New String() {";"}
                    textParser.HasFieldsEnclosedInQuotes = True
                    'Loop through all of the rows in the stream
                    While Not textParser.EndOfData
                        Dim rowFields As String() = textParser.ReadFields()
                        For Each field As String In rowFields
                            Dim nvPair As String() = field.Split(New Char() {"="c}, 2) ' Split on first equals sign only
                            If nvPair.Length = 2 Then
                                ' Store the settings in a list.  This ensures that we only update the settings if the entire string can be properly parsed.
                                newSettings.Add(nvPair)
                            Else
                                Throw New Microsoft.VisualBasic.FileIO.MalformedLineException(String.Format("Unable to split field [{0}]", field))
                            End If
                        Next
                    End While
                End Using
            End Using
            For Each nvPair As String() In newSettings
                LiveLinkConfiguration.SetSetting(nvPair(0), nvPair(1))
            Next
            LiveLinkConfiguration.Save()
            Return True
        Catch ex As Exception
            LogEvent("Error writing new config settings." & vbCrLf & vbCrLf & "Error: " & ex.Message, EventLogEntryType.Warning)
            Return False
        End Try

    End Function

    Public Sub SetupDatabase()
        Dim Use_ODBC As Boolean = LiveLinkConfiguration.Use_ODBC_DB_Connect
        Dim sPOSType As String = LiveLinkConfiguration.POSType.Trim.ToUpper
        Dim sVersion As String = String.Empty
        Dim dbResult As Integer

        Select Case sPOSType
            Case "PIXELPOINT"
                ' do nothing (do not check for or create database)

            Case "MICROS"
                sVersion = LiveLinkConfiguration.MicrosGatewayVersion.Trim.ToUpper
                SetupTablesAndStoredProcedures(Use_ODBC, sPOSType, sVersion:=sVersion)
            Case Else
                '"PARJOURNAL", "ALOHA", "SIGNATURE", "PARINFUSION", "MICROS9700", "PCAMERICA", "COMPRIS", "PAN7700", "INTOUCH", "PAREXALT4", "SILVERWARE", "OCTANE", "ICG", "MICROSOFTRMS", "NEWPOS", "IPOS", "UNIWELL"
                '                SetStatus("Checking database ...", "Checking MacromatiX database")
                dbResult = CheckDatabase(Use_ODBC, sPOSType)
                Select Case dbResult
                    Case 0 ' The MacromatiX database exists
                        ' Create the tbSalesMain etc tables if they have not yet been created 
                        ' Ignore errors as the tables may already have been created
                        SetupTablesAndStoredProcedures(Use_ODBC, sPOSType)
                    Case 1
                        LogStatus("Error", "SQL Server is not running or installed")
                    Case 2
                        If CreateDatabase(Use_ODBC, sPOSType) Then
                            LogStatus("Setup", "MacromatiX database created")
                            Threading.Thread.Sleep(15000) ' Pause for 15 seconds before creating the schema
                            If SetupTablesAndStoredProcedures(Use_ODBC, sPOSType) Then
                                LogStatus("Setup", "MacromatiX schema created.")
                            Else
                                LogStatus("Error", "Unable to create the MacromatiX schema")
                            End If
                        Else
                            LogStatus("Error", "Unable to create the MacromatiX database")
                        End If
                End Select
        End Select

        _posCommon.SetupDatabase()

        Try
            Dim ds As DataSet
            Dim sSQL As String
            Dim sError As String = String.Empty

            Select Case sPOSType.ToUpper
                Case "PIXELPOINT" ' PixelPoint uses Sybase 5 which does not support the TOP command
                    sSQL = "Select First * from tbSalesMain"
                Case Else
                    sSQL = "Select Top 1 * from tbSalesMain"
            End Select

            If MMSGeneric_ListAll(Use_ODBC, sSQL, "tbSalesMain", ds, sError, True) Then
                FixSchema(sPOSType, Use_ODBC, ds)
            End If
        Catch ex As Exception
        End Try

    End Sub




#Region "YIS Interface"

    Private Function HandleYISCommandData(ByVal request As String, ByVal messageID As Integer) As Boolean
        Dim status As Boolean = True
        Try
            ' parse the command
            Dim commandData As LiveLinkPOSCommandData = LiveLinkPOSCommandData.Parse(request)
            If (commandData Is Nothing) Then
                LogEvent(EventLogEntryType.Error, "Failed to parse YIS command data:[{0}]", request)
                Return False
            End If

            ' add the POS Command data to tbPOSCommandData
            Mx.Services.LiveLink.POSCommandService.InsertData(New Mx.BusinessObjects.LiveLink.POSCommandData(Nothing, commandData.Type, commandData.Options, commandData.CData, commandData.FilePath, commandData.ApplyDate, DateTime.Now))
            ' delete any old built up data
            If (LiveLinkConfiguration.DaysBeforeObsolete <> 0) Then
                Mx.Services.LiveLink.POSCommandService.DeleteObsoleteData(LiveLinkConfiguration.DaysBeforeObsolete, commandData.Type)
            End If

        Catch ex As Exception
            LogEvent(EventLogEntryType.Error, "Exception occurred handling YIS command data:[{0}]. Exception: {1}", request, ex.ToString)
            status = False
        End Try
        Return status
    End Function

    Private Function HandleYISCommand(ByVal request As String, ByVal messageID As Integer) As Boolean
        Dim status As Boolean = True
        Try
            ' parse the command
            Dim yisCommand As LiveLinkPOSCommand = LiveLinkPOSCommand.Parse(request)
            If (yisCommand Is Nothing) Then
                LogEvent(EventLogEntryType.Error, "Failed to parse YIS command:[{0}]", request)
                Return False
            End If

            ' create the YIS command message and pass in any parameters
            Dim message As Producer.POSControlMessage = New Producer.POSControlMessage(LiveLinkConfiguration.YisDMCServerPort, yisCommand.CommandType, MxCommandSource.Online, messageID, yisCommand.MxParameterCollection)
            ' check message was created successfully
            If (message Is Nothing) Then
                LogEvent(EventLogEntryType.Error, "YIS Message not initialised")
                Return False
            End If

            ' send the message to YIS
            message.SendMessage()
        Catch ex As Exception
            LogEvent(EventLogEntryType.Error, "Exception occurred handling YIS command:[{0}]. Exception: {1}", request, ex.ToString)
            status = False
        End Try
        Return status
    End Function



#End Region



    Private Function GetEntityName(ByVal EntityID As String) As String
        Dim MyMessage As New LiveLinkWebService
        Dim MyLiveLinkResponse As eRetailer.LiveLinkResponse
        Dim EntityName As String = String.Empty

        MyMessage.Url = LiveLinkConfiguration.WebService_URL
        LoadProxy(MyMessage)

        GetLease()
        If _sessionKey <> String.Empty Then
            Try
                MyLiveLinkResponse = MyMessage.GetStore(_liveLinkID, _sessionKey, EntityID)
                If MyLiveLinkResponse.ResponseCode = eRetailer.LiveLinkResponseCode.Success Then
                    If MyLiveLinkResponse.ResponseData.Tables("EntityStore").Rows.Count > 0 Then
                        EntityName = EvalNull(MyLiveLinkResponse.ResponseData.Tables("EntityStore").Rows(0).Item("Entity"), "")
                    End If
                End If
            Catch ex As Exception
            End Try
        End If
        Return EntityName
    End Function

    Private Function EncodeSQL(ByVal StringToFix As String) As String
        Return StringToFix.Replace("'", "''")
    End Function


#Region "Ping"

    Private Function Ping() As Boolean
        Try
            _timerPing.Enabled = False
            Dim pingService As New LiveLinkWebService

            pingService.Url = LiveLinkConfiguration.WebService_URL
            LoadProxy(pingService)
            pingService.Timeout = 10000 ' 10 Seconds
            pingService.Ping()
            ' raise a event indicating a successfull ping
            RaiseEvent PingStatusEvent(Me, New PingEventArgs(True))
            SuccessfulCommunications()

        Catch ex As Exception
            RaiseEvent PingStatusEvent(Me, New PingEventArgs(True, ex.Message))
        Finally
            _timerPing.Enabled = True
        End Try
    End Function

#End Region



#Region "LiveLink Auto-Activation Process"


    Private Sub AutoActivateLiveLink()
        ' check if auto-activation is enabled
        If (Not LiveLinkConfiguration.AutoActivationEnabled) Then
            Return
        End If
        Try
            ' check if store details were supplied
            If (String.IsNullOrEmpty(_argumentStoreNumber)) Then
                ' no store number supplied from the command line, so do not activate
                Return
            End If

            If (String.IsNullOrEmpty(_liveLinkID)) Then
                ' ensure that the LiveLinkID is retrieved before proceeding
                SetLiveLinkID()
            End If

            LogStatus("Info", String.Format("Attempting to auto-activate LiveLink for Store #{0}", _argumentStoreNumber))
            ' get a lease from the server before attempting activation
            GetLease(False)

            Dim sessionKey As String = String.Empty
            ' check if a conditional or full lease has been granted
            ' (this ensures the LiveLink setup is valid and communicating with the server correctly)
            If (HasConditionalLease()) Then
                sessionKey = _conditionalSessionKey
            ElseIf (Is_OK_To_Send()) Then
                sessionKey = _sessionKey
            Else
                ' not allowed to send with a conditional lease or valid lease
                LogEvent(EventLogEntryType.Warning, "Auto-Activation: Failed to get lease for Auto-Activation of LiveLink")
                LogStatus("Error", "Activation failed. Lease error")
                Return
            End If

            ' create webservice
            Dim webService As New LiveLinkWebService
            Dim response As eRetailer.LiveLinkResponse = Nothing
            Dim entityList As SimpleEntityList = Nothing

            ' setup webservice URL and proxy
            webService.Url = LiveLinkConfiguration.WebService_URL
            webService.Proxy = ConfigProxy()

            ' get the list of entities for the supplied store number
            response = webService.ValidateStoreNumber(_liveLinkID, sessionKey, _argumentStoreNumber)
            ' check response code from request
            Select Case (response.ResponseCode)
                Case eRetailer.LiveLinkResponseCode.Success
                    ' valid result returned to get entity information (de-serialise the result list)
                    entityList = entityList.Deserialise(response.ResponseCompressedData)
                    Exit Select

                Case eRetailer.LiveLinkResponseCode.InvalidConfiguration
                    If (String.IsNullOrEmpty(response.ResponseString)) Then
                        LogEvent(EventLogEntryType.Error, "Auto-Activation: Invalid configuration response when requesting Entity for StoreNumber:[{0}]", _argumentStoreNumber)
                        LogStatus("Error", "Activation failed. Invalid Configuration")
                    Else
                        LogEvent(EventLogEntryType.Error, "Auto-Activation: Invalid configuration response when requesting Entity for StoreNumber:[{0}]. Error: {1}", _argumentStoreNumber, response.ResponseString)
                        LogStatus("Error", String.Format("Activation failed. {0}", response.ResponseString))
                    End If
                    Return
                Case eRetailer.LiveLinkResponseCode.Backoff
                    LogEvent(EventLogEntryType.Error, "Auto-Activation: BackOff response returned when requesting Entity for StoreNumber:[{0}]", _argumentStoreNumber)
                    Return
                Case eRetailer.LiveLinkResponseCode.Offline
                    LogEvent(EventLogEntryType.Error, "Auto-Activation: Offline response returned when requesting Entity for StoreNumber:[{0}]", _argumentStoreNumber)
                    Return
                Case Else
                    If (String.IsNullOrEmpty(response.ResponseString)) Then
                        LogEvent(EventLogEntryType.Error, "Auto-Activation: Unhandled response:[{0}] for requesting Entity for StoreNumber:[{1}]", response.ResponseCode, _argumentStoreNumber)
                        LogStatus("Error", String.Format("Activation failed. Unhandled response: {0}", response.ResponseCode))
                    Else
                        LogEvent(EventLogEntryType.Error, "Auto-Activation: Unhandled response:[{0}] for requesting Entity for StoreNumber:[{1}]. Error: {2}", response.ResponseCode, _argumentStoreNumber, response.ResponseString)
                        LogStatus("Error", String.Format("Activation failed. {0}", response.ResponseString))
                    End If
                    Return
            End Select

            ' check valid entityList is available
            If (entityList Is Nothing OrElse entityList.Count <= 0) Then
                LogEvent(EventLogEntryType.Error, "Auto-Activation: No entities returned for StoreNumber:[{0}]. Activation failed!", _argumentStoreNumber)
                LogStatus("Error", String.Format("Activation failed. No entities for Store #{0}", _argumentStoreNumber))
                Return
            End If

            ' check that only 1 entity was returned (anymore than 1 results in ambiguous StoreNumber)
            If (entityList.Count > 1) Then
                LogEvent(EventLogEntryType.Error, "Auto-Activation: Ambiguous StoreNumber:[{0}]. More than one entity returned (total of {0}). Activation failed!", _argumentStoreNumber, entityList.Count)
                LogStatus("Error", String.Format("Activation failed. Ambiguous Store #{0}", _argumentStoreNumber))
                Return
            End If

            ' use the supplied EntityID
            Dim currentEntityID As Long = EntityID
            Dim activateEntityID As Long = entityList(0).EntityID

            ' check if there is a currently saved entityID and if it differs from the new one being setup
            If (currentEntityID <= 0) Then
                ' EntityID is not currently set, so this is a new install
            ElseIf (currentEntityID <> activateEntityID) Then
                ' There is already a saved EntityID, switching store to new EntityID (log information)
                LogEvent(EventLogEntryType.Warning, "Auto-Activation: Saved EntityID ({0}) is being changed to new EntityID ({1}) for Store #{2}", currentEntityID, EntityID, _argumentStoreNumber)
                LogStatus("Warning", "EntityID for LiveLink activation has changed")
            End If
            ' save the EntityID in tbStoreInfo
            EntityID = activateEntityID

            ' call webservice to Auto-Activate the LiveLink store
            response = webService.AutoActivateStore(_liveLinkID, sessionKey, EntityID)

            ' check response code from request
            Select Case (response.ResponseCode)
                Case eRetailer.LiveLinkResponseCode.Success
                    ' Auto-Activation succeeded! log success event
                    LogEvent(EventLogEntryType.Information, "Auto-Activation: Successfully activated StoreNumber:[{0}] for EntityID:[{1}]", _argumentStoreNumber, EntityID)
                    LogStatus("", String.Format("Activation succeeded for Store #{0}", _argumentStoreNumber))
                    Exit Select

                Case eRetailer.LiveLinkResponseCode.InvalidConfiguration
                    If (String.IsNullOrEmpty(response.ResponseString)) Then
                        LogEvent(EventLogEntryType.Error, "Auto-Activation: Invalid configuration response when requesting activation of StoreNumber:[{0}], EntityID:[{1}]", _argumentStoreNumber, EntityID)
                        LogStatus("Error", "Activation failed. Invalid Configuration")
                    Else
                        LogEvent(EventLogEntryType.Error, "Auto-Activation: Invalid configuration response when requesting activation of StoreNumber:[{0}], EntityID:[{1}]. Error: {2}", _argumentStoreNumber, EntityID, response.ResponseString)
                        LogStatus("Error", String.Format("Activation failed. {0}", response.ResponseString))
                    End If
                    Return
                Case eRetailer.LiveLinkResponseCode.Backoff
                    LogEvent(EventLogEntryType.Error, "Auto-Activation: Backoff response when requesting activation of StoreNumber:[{0}], EntityID:[{1}]", _argumentStoreNumber, EntityID)
                    Return
                Case eRetailer.LiveLinkResponseCode.Offline
                    LogEvent(EventLogEntryType.Error, "Auto-Activation: Offline response when requesting activation of StoreNumber:[{0}], EntityID:[{1}]", _argumentStoreNumber, EntityID)
                    Return
                Case Else
                    If (String.IsNullOrEmpty(response.ResponseString)) Then
                        LogEvent(EventLogEntryType.Error, "Auto-Activation: Unhandled response:[{0}] when requesting activation of StoreNumber:[{1}], EntityID:[{2}]", response.ResponseCode, _argumentStoreNumber, EntityID)
                        LogStatus("Error", String.Format("Activation failed. Unhandled response: {0}", response.ResponseCode))
                    Else
                        LogEvent(EventLogEntryType.Error, "Auto-Activation: Unhandled response:[{0}] when requesting activation of StoreNumber:[{1}], EntityID:[{2}]. Error: {3}", response.ResponseCode, _argumentStoreNumber, EntityID, response.ResponseString)
                        LogStatus("Error", String.Format("Activation failed. {0}", response.ResponseString))
                    End If
                    Return
            End Select

        Catch ex As Exception
            ' log an event on exception
            LogEvent(EventLogEntryType.Error, "Exception occurred auto-activating LiveLink. Exception: {0}", ex.ToString)
            LogStatus("Error", String.Format("Activation failed. Exception: {0}", ex.Message))
        End Try
    End Sub


#End Region


#Region "ZK Fingerprint"

    Private Sub UpdateZKFirmware(ByVal importConfiguration As ZKFingerprintConfiguration, Optional ByVal fileName As String = "main.gz", Optional ByVal rebootDevice As Boolean = True)
        ' check zk server is enabled
        If (Not LiveLinkConfiguration.ZKFingerprintAuthServerEnabled) Then
            Exit Sub
        End If

        Try
            If (importConfiguration.Server Is Nothing) Then
                importConfiguration.SetStatusMessage("Not initialised")
                Exit Sub
            End If

            Dim zkState As zkCommunications.zkServer.ServerState
            zkState = importConfiguration.Server.GetState()

            Select Case (zkState)
                Case zkCommunications.zkServer.ServerState.Stopped, zkCommunications.zkServer.ServerState.Stopping
                    importConfiguration.SetStatusMessage(zkState.ToString())
                    Exit Sub
                Case zkCommunications.zkServer.ServerState.WaitingForConnection
                    importConfiguration.SetStatusMessage("Waiting for device...")
                    Exit Sub
            End Select

            importConfiguration.SetStatusMessage("Connected...")

            If (importConfiguration.Server IsNot Nothing) Then
                ' update the zk devices firmware
                importConfiguration.Server.UpdateFile(fileName)
                ' check the fingerprint processing object has been created
                If (importConfiguration.Fingerprint Is Nothing) Then
                    importConfiguration.Fingerprint = New zkFingerPrint(EntityID, LiveLinkConfiguration.ZKFingerprintIP, LiveLinkConfiguration.ZKFingerprintPort)
                End If

                Const timeoutMilliseconds As Integer = 200
                Const maxRetries As Integer = 300   ' 300 retries of 200 millisecond interval. Will cause a maximum wait of 1 minute..
                Dim completed As Boolean = False
                Dim retries As Integer = 0

                While (Not completed And (retries < maxRetries))
                    If (importConfiguration.Server.UpdateCompleted(timeoutMilliseconds)) Then
                        completed = True
                        importConfiguration.SetStatusMessage("Refreshing device data...")

                        ' call a data refresh on the device after the update (this will force a sync of the filesystem)
                        importConfiguration.Fingerprint.ExecuteRequest(zkFingerPrint.RequestType.RefreshData)
                        '' restart the device so that all the changes will take effect
                        If (rebootDevice) Then
                            ' wait for 10 Seconds (100 * 100 millisecond sleeps) before initialising device restart
                            ' will give the device time to sync file system changes
                            For syncWaitCount As Integer = 0 To 100
                                Thread.CurrentThread.Sleep(100)
                            Next
                            importConfiguration.SetStatusMessage("Rebooting device...")
                            ' execute restart request
                            importConfiguration.Fingerprint.ExecuteRequest(zkFingerPrint.RequestType.RestartDevice)
                        End If
                    End If
                    ' do any events in between wait
                    Application.DoEvents()
                    retries += 1
                End While

                If (Not completed) Then
                    LogEvent(EventLogEntryType.Error, "Error getting ZK update completion response. Forcing filesystem sync, but no reboot")
                    importConfiguration.SetStatusMessage("Refreshing...")
                    importConfiguration.Fingerprint.ExecuteRequest(zkFingerPrint.RequestType.RefreshData)
                End If

                ' after update or failed update, set status back to waiting for device
                importConfiguration.SetStatusMessage("Waiting for device...")
            End If
        Catch ex As Exception
            LogEvent(EventLogEntryType.Warning, "Error updating file through ZK auth server (File:{0}). Exception: {1}", fileName, ex.ToString)
        Finally

        End Try
    End Sub


#End Region

End Class
