Imports System.IO
Imports Mx.POS.Common
Imports MXServer
Imports System.Text
Imports System.Text.RegularExpressions
Imports Mx.POS.ICG
Imports Mx.POS.Micros
Imports Mx.POS.Panasonic


Module modSetup

    Public Const SybaseCommitWork As String = "; commit work; "
    Private _masterConnectionString As String = String.Empty
    Private _connectionString As String = String.Empty
    Private _databaseName As String = String.Empty
    Private Const ConnectionStringMatchPattern As String = "(?<=(\b)Database[ ]*=[ ]*)[\w]*(?=(\b))"

    Private ReadOnly Property ConnectionString() As String
        Get
            If _connectionString = String.Empty Then
                _connectionString = LiveLinkConfiguration.ConnectionString
            End If
            Return _connectionString
        End Get
    End Property

    Private ReadOnly Property DatabaseName() As String
        Get
            If _databaseName = String.Empty Then
                _databaseName = Regex.Matches(ConnectionString, ConnectionStringMatchPattern, RegexOptions.IgnoreCase)(0).ToString
            End If
            Return _databaseName
        End Get
    End Property

    Private ReadOnly Property MasterConnectionString() As String
        Get
            If _masterConnectionString = String.Empty Then
                _masterConnectionString = Regex.Replace(ConnectionString, ConnectionStringMatchPattern, "Master", RegexOptions.IgnoreCase)
            End If
            Return _masterConnectionString
        End Get
    End Property

    Function CheckDatabase(ByVal Use_ODBC As Boolean, ByVal POSType As String) As Integer
        Dim sSql As String
        Dim bNoMSA_Flag As Boolean
        Dim dbDataSet As DataSet = Nothing
        Dim sError As String = String.Empty

        POSType = POSType.Trim.ToUpper

        Select Case POSType
            Case Is = "MICROS"
                bNoMSA_Flag = True
            Case Else
                bNoMSA_Flag = False
        End Select

        Try
            Dim myServer As New MXServer.MXServer

            sSql = String.Format("SELECT name FROM master.dbo.sysdatabases WHERE name = N'{0}'", DatabaseName)

            If myServer.svrMX_ListAll(MasterConnectionString, sSql, "Databases", dbDataSet, sError) Then
                If dbDataSet.Tables("Databases").Rows.Count = 0 Then
                    ' SQL Server is running, but no MacromatiX database has been setup yet
                    CheckDatabase = 2
                Else
                    CheckDatabase = 0 ' SQL Server is running and a MacromatiX database exists
                End If
            Else
                LogEvent("LiveLink - CheckDatabase() - Query=" & sSql & vbCrLf & "Error=" & sError, EventLogEntryType.Error)
                CheckDatabase = 1 ' Unknown - database not running or MSDE not installed
            End If

        Catch ex As Exception
            LogEvent("LiveLink - CheckDatabase() - Exception - " & vbCrLf & ex.StackTrace, EventLogEntryType.Error)
            CheckDatabase = 1 ' Unknown - database not running or MSDE not installed
        End Try

    End Function

    Function CreateDatabase(ByVal Use_ODBC As Boolean, ByVal POSType As String) As Boolean
        Const cDatabaseDirectory As String = "C:\Program Files\MacromatiX\Database"
        Dim SqlArray As New ArrayList
        Dim sSql As String
        Dim sError As String = String.Empty

        POSType = POSType.Trim.ToUpper

        Try
            Try
                ' Create the folder in which to create the database
                Directory.CreateDirectory(cDatabaseDirectory)
            Catch ex As Exception
            End Try

            sSql = String.Format("CREATE DATABASE [{0}] COLLATE Latin1_General_CI_AS ", DatabaseName)
            SqlArray.Add(sSql)
            sSql = String.Format("exec sp_dboption N'{0}', N'autoclose', N'false' ", DatabaseName)
            SqlArray.Add(sSql)
            sSql = String.Format("exec sp_dboption N'{0}', N'bulkcopy', N'false' ", DatabaseName)
            SqlArray.Add(sSql)
            sSql = String.Format("exec sp_dboption N'{0}', N'trunc. log', N'true' ", DatabaseName)
            SqlArray.Add(sSql)
            sSql = String.Format("exec sp_dboption N'{0}', N'torn page detection', N'true' ", DatabaseName)
            SqlArray.Add(sSql)
            sSql = String.Format("exec sp_dboption N'{0}', N'read only', N'false' ", DatabaseName)
            SqlArray.Add(sSql)
            sSql = String.Format("exec sp_dboption N'{0}', N'dbo use', N'false' ", DatabaseName)
            SqlArray.Add(sSql)
            sSql = String.Format("exec sp_dboption N'{0}', N'single', N'false' ", DatabaseName)
            SqlArray.Add(sSql)
            sSql = String.Format("exec sp_dboption N'{0}', N'autoshrink', N'true' ", DatabaseName)
            SqlArray.Add(sSql)
            sSql = String.Format("exec sp_dboption N'{0}', N'ANSI null default', N'false' ", DatabaseName)
            SqlArray.Add(sSql)
            sSql = String.Format("exec sp_dboption N'{0}', N'recursive triggers', N'false' ", DatabaseName)
            SqlArray.Add(sSql)
            sSql = String.Format("exec sp_dboption N'{0}', N'ANSI nulls', N'false' ", DatabaseName)
            SqlArray.Add(sSql)
            sSql = String.Format("exec sp_dboption N'{0}', N'concat null yields null', N'false' ", DatabaseName)
            SqlArray.Add(sSql)
            sSql = String.Format("exec sp_dboption N'{0}', N'cursor close on commit', N'false' ", DatabaseName)
            SqlArray.Add(sSql)
            sSql &= String.Format("exec sp_dboption N'{0}', N'default to local cursor', N'false' ", DatabaseName)
            SqlArray.Add(sSql)
            sSql = String.Format("exec sp_dboption N'{0}', N'quoted identifier', N'false' ", DatabaseName)
            SqlArray.Add(sSql)
            sSql = String.Format("exec sp_dboption N'{0}', N'ANSI warnings', N'false' ", DatabaseName)
            SqlArray.Add(sSql)
            sSql = String.Format("exec sp_dboption N'{0}', N'auto create statistics', N'true' ", DatabaseName)
            SqlArray.Add(sSql)
            sSql = String.Format("exec sp_dboption N'{0}', N'auto update statistics', N'true' ", DatabaseName)
            SqlArray.Add(sSql)
            sSql = "if( (@@microsoftversion / power(2, 24) = 8) and (@@microsoftversion & 0xffff >= 724) ) "
            sSql &= String.Format("	exec sp_dboption N'{0}', N'db chaining', N'false' ", DatabaseName)
            SqlArray.Add(sSql)

            Dim myServer As New MXServer.MXServer

            Dim sCmd As String
            Dim sConnect As String = MasterConnectionString()
            For Each sCmd In SqlArray
                If myServer.svrMX_Execute(sConnect, sCmd, sError) Then
                    CreateDatabase = True ' MacromatiX database created successfully
                Else
                    If LiveLinkConfiguration.DebugLogging Then
                        MMS.Utilities.MxLogger.LogErrorToTrace("LiveLink - CreateDatabase() - SQL: {0}", sCmd)
                        MMS.Utilities.MxLogger.LogErrorToTrace("LiveLink - CreateDatabase() - Error: {0}", sError)
                    End If

                    CreateDatabase = False ' Unknown - database not running or MSDE not installed
                    Exit For
                End If
            Next
            myServer = Nothing

        Catch ex As Exception
            If LiveLinkConfiguration.DebugLogging Then
                MMS.Utilities.MxLogger.LogErrorToTrace("LiveLink - CreateDatabase() - Exception - {0}", ex.StackTrace)
            End If
            CreateDatabase = False ' Unknown error
        End Try

    End Function

    Function GenerateLocalUsersTableSQL() As String
        Dim sql As New StringBuilder(256)
        'sql.AppendLine("IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[tbLocalUsers]') AND type in (N'U'))")
        sql.AppendLine("CREATE TABLE tbLocalUsers (")
        sql.AppendLine("UserId bigint NOT NULL ,")
        sql.AppendLine("UserName nvarchar (50) NOT NULL ,")
        sql.AppendLine("UserPassword nvarchar (150) NULL ,")
        sql.AppendLine("EntityID bigint NULL ,")
        sql.AppendLine("FirstName nvarchar (35) NOT NULL ,")
        sql.AppendLine("LastName nvarchar (35) NOT NULL ,")
        sql.AppendLine("POSIDName nvarchar (50) NOT NULL ,")
        sql.AppendLine("EmployeeId bigint NULL, ")
        sql.AppendLine("EmployeeNumber nvarchar(50) NULL")
        sql.AppendLine(")")
        Return sql.ToString()
    End Function

    Private Sub AlterLocalUsersTableSQL(ByVal sqlArray As ArrayList)
        Dim sb As New StringBuilder(256)
        sb.Append("if not exists (select 1 from syscolumns where name='UserId' and id = object_id(N'tbLocalUsers'))")
        sb.Append(" ALTER TABLE tbLocalUsers ADD UserId BIGINT NULL")
        sqlArray.Add(sb.ToString())

        sb = New StringBuilder(256)
        sb.Append("if not exists (select 1 from syscolumns where name='EmployeeId' and id = object_id(N'tbLocalUsers'))")
        sb.Append(" ALTER TABLE tbLocalUsers ADD EmployeeId BIGINT NULL")
        sqlArray.Add(sb.ToString())

        sb = New StringBuilder(256)
        sb.Append("if not exists (select 1 from syscolumns where name='UserPassword' and id = object_id(N'tbLocalUsers'))")
        sb.Append(" ALTER TABLE tbLocalUsers ADD UserPassword NVARCHAR(150) NULL")
        sqlArray.Add(sb.ToString())

        sb = New StringBuilder(256)
        sb.Append("if not exists (select 1 from syscolumns where name='EmployeeNumber' and id = object_id(N'tbLocalUsers'))")
        sb.Append(" ALTER TABLE tbLocalUsers ADD EmployeeNumber NVARCHAR(50) NULL")
        sqlArray.Add(sb.ToString())
    End Sub


    Function GenerateLocalUsersTableSybase() As String
        Dim sql As New StringBuilder(256)
        sql.AppendLine("create table custom.tbLocalUsers (")
        sql.AppendLine("UserId real NOT NULL ,")
        sql.AppendLine("UserName varchar (50) NOT NULL ,")
        sql.AppendLine("UserPassword varchar (150) NULL ,")
        sql.AppendLine("EntityID real NULL ,")
        sql.AppendLine("FirstName varchar (35) NOT NULL ,")
        sql.AppendLine("LastName varchar (35) NOT NULL ,")
        sql.AppendLine("POSIDName varchar (50) NOT NULL ,")
        sql.AppendLine("EmployeeId real NOT NULL ")
        sql.AppendLine(");")
        sql.AppendFormat("   grant all privileges on custom.tbLocalUsers to public {0}", SybaseCommitWork)
        Return sql.ToString()
    End Function

    Private Sub AlterLocalUsersTableSybase(ByVal sqlArray As ArrayList)
        Dim sql As New StringBuilder(256)
        sql.Append("IF NOT EXISTS (SELECT column_name FROM systable T KEY JOIN syscolumn C ")
        sql.AppendLine("WHERE table_name = 'tbLocalUsers' AND column_name = 'UserId') THEN ")
        sql.AppendLine("ALTER TABLE custom.tbLocalUsers ")
        sql.AppendLine("ADD UserId real NULL ")
        sql.AppendFormat("END IF {0}", SybaseCommitWork)
        sqlArray.Add(sql.ToString())

        sql = New StringBuilder(256)
        sql.Append("IF NOT EXISTS (SELECT column_name FROM systable T KEY JOIN syscolumn C ")
        sql.AppendLine("WHERE table_name = 'tbLocalUsers' AND column_name = 'UserPassword') THEN ")
        sql.AppendLine("ALTER TABLE custom.tbLocalUsers ")
        sql.AppendLine("ADD UserPassword varchar (150) NULL ")
        sql.AppendFormat("END IF {0}", SybaseCommitWork)
        sqlArray.Add(sql.ToString())

        sql = New StringBuilder(256)
        sql.Append("IF NOT EXISTS (SELECT column_name FROM systable T KEY JOIN syscolumn C ")
        sql.AppendLine("WHERE table_name = 'tbLocalUsers' AND column_name = 'EmployeeId') THEN ")
        sql.AppendLine("ALTER TABLE custom.tbLocalUsers ")
        sql.AppendLine("ADD EmployeeId real NULL ")
        sql.AppendFormat("END IF {0}", SybaseCommitWork)
        sqlArray.Add(sql.ToString())

        sql = New StringBuilder(256)
        sql.Append("IF NOT EXISTS (SELECT column_name FROM systable T KEY JOIN syscolumn C ")
        sql.AppendLine("WHERE table_name = 'tbLocalUsers' AND column_name = 'EmployeeNumber') THEN ")
        sql.AppendLine("ALTER TABLE custom.tbLocalUsers ")
        sql.AppendLine("ADD EmployeeNumber varchar(50) NULL ")
        sql.AppendFormat("END IF {0}", SybaseCommitWork)
        sqlArray.Add(sql.ToString())
    End Sub

    Private Sub AddNewColumns(ByVal sqlArray As ArrayList)
        ' Adds a new alter table statement to the sql array list
        Dim sb As New StringBuilder(256)
        sb.Append("if not exists (select 1 from syscolumns where name='ParentId' and id = object_id(N'tbSalesMain'))")
        sb.Append(" ALTER TABLE tbSalesMain ADD ParentId BIGINT NULL")
        sqlArray.Add(sb.ToString)
        ' clear the stringbuilder
        sb = New StringBuilder(256)
        sb.Append("if not exists (select 1 from syscolumns where name='BusinessDay' and id = object_id(N'tbSalesMain'))")
        sb.Append(" ALTER TABLE tbSalesMain ADD BusinessDay [smalldatetime] NULL")
        sqlArray.Add(sb.ToString)
        ' clear the stringbuilder
        sb = New StringBuilder(256)
        sb.Append("if not exists (select 1 from syscolumns where name='ItemTax' and id = object_id(N'tbSalesMain'))")
        sb.Append(" ALTER TABLE tbSalesMain ADD ItemTax [money] NULL")
        sqlArray.Add(sb.ToString)
        ' clear the stringbuilder
        sb = New StringBuilder(256)
        sb.Append("if not exists (select 1 from syscolumns where name='ItemDiscount' and id = object_id(N'tbSalesMain'))")
        sb.Append(" ALTER TABLE tbSalesMain ADD ItemDiscount [money] NULL")
        sqlArray.Add(sb.ToString)
        ' clear the stringbuilder
        sb = New StringBuilder(256)
        sb.Append("if not exists (select 1 from syscolumns where name='Status' and id = object_id(N'tbStoreInfo'))")
        sb.Append(" ALTER TABLE tbStoreInfo ADD [Status] [tinyint] NULL")
        sqlArray.Add(sb.ToString)
    End Sub

    Function SetupTablesAndStoredProcedures(ByVal Use_ODBC As Boolean, ByVal POSType As String, Optional ByVal sVersion As String = "") As Boolean
        ' This function creates tbSalesMain and any corresponding tables, stored proceduers and indexes.
        ' It DOES NOT drop any object first and therefore can be run many times eg at starup of LiveLink
        Dim SqlArray As New ArrayList
        Dim sSql As String
        Dim sError As String = String.Empty
        Dim ds As DataSet = Nothing
        Dim bNoMSA_Flag As Boolean

        POSType = POSType.Trim.ToUpper

        Select Case POSType
            Case Is = "MICROS"
                bNoMSA_Flag = True
            Case Else
                bNoMSA_Flag = False
        End Select

        Try
            Select Case POSType
                Case "PARJOURNAL"
                    sSql = "IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[tbSalesMain]') AND type in (N'U'))"
                    sSql &= "CREATE TABLE [dbo].[tbSalesMain] ( "
                    sSql &= "   [SalesMainID] [bigint] IDENTITY (1, 1) NOT NULL , "
                    sSql &= "   [RecordType] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL , "
                    sSql &= "   [TransactionID] [bigint] NULL , "
                    sSql &= "   [RecordSubType] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL , "
                    sSql &= "   [EntityID] [float] NULL , "
                    sSql &= "	[RegisterID] [float] NULL , "
                    sSql &= "	[PollDate] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL , "
                    sSql &= "   [PollCount] [float] NULL , "
                    sSql &= "   [PollAmount] [money] NULL , "
                    sSql &= "   [ClerkID] [float] NULL , "
                    sSql &= "   [ClerkName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL , "
                    sSql &= "   [CustomerID] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL , "
                    sSql &= "   [Synced] [int] NULL , "
                    sSql &= "   [PLUCodeID] [bigint] NULL , "
                    sSql &= "   [PLUCode] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL , "
                    sSql &= "   [SequenceNo] [int] NULL , "
                    sSql &= "   [Flag] [int] NULL , "
                    sSql &= "   [ApplyTax] [int] NULL , "
                    sSql &= "   [DateAdded] [datetime] NULL , "
                    sSql &= "   [SubTypeDescription] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL , "
                    sSql &= "   [POSTransactionID] [float] NULL, "
                    sSql &= "   [SyncToken] [varchar] (36) NULL, "
                    sSql &= "   [ParentId] [bigint] NULL "
                    sSql &= ") ON [PRIMARY] "
                    SqlArray.Add(sSql)

                    sSql = "IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[tbStoreInfo]') AND type in (N'U'))"
                    sSql &= "CREATE TABLE [dbo].[tbStoreInfo] ( "
                    sSql &= "    [StoreID] [smallint] NULL , "
                    sSql &= "    [RegisterID] [smallint] NULL , "
                    sSql &= "    [UniqueIDCount] [bigint] NOT NULL, "
                    sSql &= "    [LastAwake] [datetime] NULL, "
                    sSql &= "    [Status] [tinyint] NULL "
                    sSql &= ") ON [PRIMARY] "
                    SqlArray.Add(sSql)

                    sSql = "ALTER TABLE [dbo].[tbSalesMain] WITH NOCHECK ADD "
                    sSql &= "CONSTRAINT [PK_tbSalesMain_1] PRIMARY KEY  CLUSTERED "
                    sSql &= "([SalesMainID]) ON [PRIMARY] "
                    SqlArray.Add(sSql)

                    sSql = "CREATE  INDEX [RecordType] ON [dbo].[tbSalesMain]([RecordType]) ON [PRIMARY]"
                    SqlArray.Add(sSql)

                    sSql = "CREATE  INDEX [Synced] ON [dbo].[tbSalesMain]([Synced]) ON [PRIMARY]"
                    SqlArray.Add(sSql)

                    sSql = "CREATE  INDEX [TransactionID] ON [dbo].[tbSalesMain]([TransactionID]) ON [PRIMARY]"
                    SqlArray.Add(sSql)

                    sSql = "CREATE  INDEX [SequenceNo] ON [dbo].[tbSalesMain]([SequenceNo]) ON [PRIMARY]"
                    SqlArray.Add(sSql)

                    sSql = "CREATE  INDEX [RecordSubType] ON [dbo].[tbSalesMain]([RecordSubType]) ON [PRIMARY]"
                    SqlArray.Add(sSql)

                    sSql = "CREATE  INDEX [PLUCodeID] ON [dbo].[tbSalesMain]([PLUCodeID]) ON [PRIMARY]"
                    SqlArray.Add(sSql)

                    sSql = "CREATE  INDEX [Flag] ON [dbo].[tbSalesMain]([Flag]) ON [PRIMARY]"
                    SqlArray.Add(sSql)

                    sSql = "CREATE  INDEX [PollDate] ON [dbo].[tbSalesMain]([PollDate]) ON [PRIMARY]"
                    SqlArray.Add(sSql)

                    AddNewColumns(SqlArray)

                    sSql = "ALTER TABLE [dbo].[tbStoreInfo] ADD "
                    sSql &= "CONSTRAINT [DF_tbStoreInfo_UniqueIDCount] DEFAULT (0) FOR [UniqueIDCount]"
                    SqlArray.Add(sSql)

                    ' check if the stored proc needs to be created or altered
                    sSql = "SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[qryMMSStoreInfo_GetUniqueID]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1"
                    If MMSGeneric_ListAll(sSql, "Objects", ds, sError) Then
                        If ds.Tables("Objects").Rows.Count <= 0 Then
                            sSql = "CREATE PROCEDURE [dbo].[qryMMSStoreInfo_GetUniqueID] AS " + vbCrLf
                        Else
                            sSql = "ALTER PROCEDURE [dbo].[qryMMSStoreInfo_GetUniqueID] AS " + vbCrLf
                        End If
                    Else
                        sSql = "CREATE PROCEDURE [dbo].[qryMMSStoreInfo_GetUniqueID] AS " + vbCrLf
                    End If
                    sSql &= "BEGIN TRANSACTION " & vbCrLf
                    sSql &= "    Update tbStoreInfo Set UniqueIDCount = UniqueIDCount + 1 " & vbCrLf
                    sSql &= "    Select UniqueIDCount from tbStoreInfo " & vbCrLf
                    sSql &= "COMMIT TRANSACTION "
                    SqlArray.Add(sSql)

                    SqlArray.Add(GenerateLocalUsersTableSQL())
                    AlterLocalUsersTableSQL(SqlArray)

                    Dim sCmd As String
                    Dim Result As Boolean = True
                    For Each sCmd In SqlArray
                        If Not MMSGeneric_Execute(sCmd, sError) Then
                            Result = False
                        End If
                    Next
                    SetupTablesAndStoredProcedures = Result
                Case "ALOHA"
                    sSql = "IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[tbSalesMain]') AND type in (N'U'))"
                    sSql &= "CREATE TABLE [dbo].[tbSalesMain] ( "
                    sSql &= "   [SalesMainID] [bigint] IDENTITY (1, 1) NOT NULL , "
                    sSql &= "   [RecordType] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL , "
                    sSql &= "   [TransactionID] [bigint] NULL , "
                    sSql &= "   [RecordSubType] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL , "
                    sSql &= "   [EntityID] [float] NULL , "
                    sSql &= "	[RegisterID] [float] NULL , "
                    sSql &= "	[PollDate] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL , "
                    sSql &= "   [PollCount] [float] NULL , "
                    sSql &= "   [PollAmount] [money] NULL , "
                    sSql &= "   [ClerkID] [float] NULL , "
                    sSql &= "   [ClerkName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL , "
                    sSql &= "   [CustomerID] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL , "
                    sSql &= "   [Synced] [int] NULL , "
                    sSql &= "   [PLUCodeID] [bigint] NULL , "
                    sSql &= "   [PLUCode] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL , "
                    sSql &= "   [SequenceNo] [int] NULL , "
                    sSql &= "   [Flag] [int] NULL , "
                    sSql &= "   [ApplyTax] [int] NULL , "
                    sSql &= "   [PriceLevel] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL , "
                    sSql &= "   [DateAdded] [datetime] NULL , "
                    sSql &= "   [SubTypeDescription] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL , "
                    sSql &= "   [POSTransactionID] [float] NULL, "
                    sSql &= "   [SyncToken] [varchar] (36) NULL, "
                    sSql &= "   [ParentId] [bigint] NULL "
                    sSql &= ") ON [PRIMARY] "
                    SqlArray.Add(sSql)

                    sSql = "IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[tbStoreInfo]') AND type in (N'U'))"
                    sSql &= "CREATE TABLE [dbo].[tbStoreInfo] ( "
                    sSql &= "    [StoreID] [smallint] NULL , "
                    sSql &= "    [RegisterID] [smallint] NULL , "
                    sSql &= "    [UniqueIDCount] [bigint] NOT NULL , "
                    sSql &= "    [LastDate] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL , "
                    sSql &= "    [LastTransaction] [bigint] NOT NULL, "
                    sSql &= "    [LastAwake] [datetime] NULL, "
                    sSql &= "    [Status] [tinyint] NULL "
                    sSql &= ") ON [PRIMARY] "
                    SqlArray.Add(sSql)

                    sSql = "ALTER TABLE [dbo].[tbSalesMain] WITH NOCHECK ADD "
                    sSql &= "CONSTRAINT [PK_tbSalesMain_1] PRIMARY KEY  CLUSTERED "
                    sSql &= "([SalesMainID]) ON [PRIMARY] "
                    SqlArray.Add(sSql)

                    sSql = "ALTER TABLE [dbo].[tbSalesMain] ADD "
                    sSql &= "[TransactionVersion] [int] null DEFAULT 0 WITH VALUES"
                    SqlArray.Add(sSql)

                    sSql = "ALTER TABLE [dbo].[tbSalesMain] ADD "
                    sSql &= "CONSTRAINT	[DF_tbSalesMain_Synced] DEFAULT 100 FOR Synced "
                    SqlArray.Add(sSql)

                    sSql = "CREATE  INDEX [RecordType] ON [dbo].[tbSalesMain]([RecordType]) ON [PRIMARY]"
                    SqlArray.Add(sSql)

                    sSql = "CREATE  INDEX [Synced] ON [dbo].[tbSalesMain]([Synced]) ON [PRIMARY]"
                    SqlArray.Add(sSql)

                    sSql = "CREATE  INDEX [TransactionID] ON [dbo].[tbSalesMain]([TransactionID]) ON [PRIMARY]"
                    SqlArray.Add(sSql)

                    sSql = "CREATE  INDEX [SequenceNo] ON [dbo].[tbSalesMain]([SequenceNo]) ON [PRIMARY]"
                    SqlArray.Add(sSql)

                    sSql = "CREATE  INDEX [RecordSubType] ON [dbo].[tbSalesMain]([RecordSubType]) ON [PRIMARY]"
                    SqlArray.Add(sSql)

                    sSql = "CREATE  INDEX [PLUCodeID] ON [dbo].[tbSalesMain]([PLUCodeID]) ON [PRIMARY]"
                    SqlArray.Add(sSql)

                    sSql = "CREATE  INDEX [Flag] ON [dbo].[tbSalesMain]([Flag]) ON [PRIMARY]"
                    SqlArray.Add(sSql)

                    sSql = "ALTER TABLE [dbo].[tbStoreInfo] ADD "
                    sSql &= "CONSTRAINT [DF_tbStoreInfo_UniqueIDCount] DEFAULT (0) FOR [UniqueIDCount]"
                    SqlArray.Add(sSql)

                    sSql = "ALTER TABLE [dbo].[tbStoreInfo] ADD "
                    sSql &= "CONSTRAINT [DF_tbStoreInfo_LastDate] DEFAULT ('01-Jan-1900 00:00:00') FOR [LastDate]"
                    SqlArray.Add(sSql)

                    sSql = "ALTER TABLE [dbo].[tbStoreInfo] ADD "
                    sSql &= "CONSTRAINT [DF_tbStoreInfo_LastTransaction] DEFAULT (0) FOR [LastTransaction]"
                    SqlArray.Add(sSql)

                    sSql = "CREATE PROCEDURE qryMMSStoreInfo_GetUniqueID AS " & vbCrLf
                    sSql &= "    Update tbStoreInfo Set UniqueIDCount = UniqueIDCount + 1 " & vbCrLf
                    sSql &= "    Select UniqueIDCount from tbStoreInfo"
                    SqlArray.Add(sSql)

                    AddNewColumns(SqlArray)

                    SqlArray.Add(GenerateLocalUsersTableSQL())
                    AlterLocalUsersTableSQL(SqlArray)


                    Dim sCmd As String
                    Dim Result As Boolean = True
                    For Each sCmd In SqlArray
                        If Not MMSGeneric_Execute(sCmd, sError) Then
                            Result = False
                        End If
                    Next
                    SetupTablesAndStoredProcedures = Result

                Case "MICROS"
                    Dim Result As Boolean = True
                    Dim CreateProc As Boolean = True
                    Dim sCmd As String

                    ' First setup tbSalesMain if it does not exist
                    sSql = "SELECT object_id('tbSalesMain') As ObjectId;"
                    If MMSGeneric_ListAll(Use_ODBC, sSql, "Objects", ds, sError, bNoMSA_Flag) Then
                        If ds.Tables("Objects").Rows(0).Item("ObjectId") Is DBNull.Value Then
                            sSql = "    create table custom.tbSalesMain("
                            sSql &= "      SalesMainID integer not null default"
                            sSql &= "      autoincrement,"
                            sSql &= "      RecordType varchar(255) null,"
                            sSql &= "      TransactionID real null,"
                            sSql &= "      RecordSubType varchar(255) null,"
                            sSql &= "      EntityID real null,"
                            sSql &= "      RegisterID real null,"
                            sSql &= "      PollDate DATETIME null,"
                            sSql &= "      PollCount real null,"
                            sSql &= "      PollAmount MONEY18,"
                            sSql &= "      ClerkID real null,"
                            sSql &= "      ClerkName varchar(255) null,"
                            sSql &= "      CustomerID varchar(255) null,"
                            sSql &= "      Synced integer null,"
                            sSql &= "      PLUCodeID integer null,"
                            sSql &= "      PLUCode varchar(255) null,"
                            sSql &= "      SequenceNo integer null,"
                            sSql &= "      Flag integer null,"
                            sSql &= "      ApplyTax integer null,"
                            sSql &= "      PriceLevel varchar(255) null,"
                            sSql &= "      ItemDiscount MONEY18,"
                            sSql &= "      MicrosId integer null,"
                            sSql &= "      SyncToken varchar(36) null,"
                            sSql &= "      TransactionVersion int null DEFAULT 0," ' Used for reopened check handling
                            sSql &= "      BusinessDay DATETIME null,"
                            sSql &= "      ParentID integer null,"
                            sSql &= "      ItemTax MONEY18,"
                            sSql &= "      SubTypeDescription varchar(255) null,"
                            sSql &= "      primary key(SalesMainID),"
                            sSql &= "      );"
                            sSql &= "    grant all privileges on custom.tbSalesMain to public" & SybaseCommitWork
                            SqlArray.Add(sSql)

                            For Each sCmd In SqlArray
                                If Not MMSGeneric_Execute(Use_ODBC, sCmd, sError) Then
                                    Result = False
                                    LogEvent("Table setup error (tbSalesMain): " & sError & vbCrLf & vbCrLf & "Sql: " & sCmd, EventLogEntryType.Error)
                                End If
                            Next
                            If Result Then
                                LogEvent("tbSalesMain table created", EventLogEntryType.Information)
                            End If
                        End If
                        ' Now set up the indexes if they do not exist
                        SqlArray.Clear()
                        sSql = "IF NOT EXISTS(SELECT I.* from systable T KEY JOIN sysindex I WHERE table_name = 'tbSalesMain' "
                        sSql &= "AND index_name = 'ixTransactionId') THEN "
                        sSql &= "  create index ixTransactionId on custom.tbSalesMain (TransactionId ASC) "
                        sSql &= "END IF; "
                        SqlArray.Add(sSql)
                        sSql = "IF NOT EXISTS(SELECT I.* from systable T KEY JOIN sysindex I WHERE table_name = 'tbSalesMain' "
                        sSql &= "AND index_name = 'ixTransactionVersion') THEN "
                        sSql &= "  create index ixTransactionVersion on custom.tbSalesMain (TransactionVersion ASC) "
                        sSql &= "END IF; "
                        SqlArray.Add(sSql)
                        sSql = "IF NOT EXISTS(SELECT I.* from systable T KEY JOIN sysindex I WHERE table_name = 'tbSalesMain' "
                        sSql &= "AND index_name = 'ixSynced') THEN "
                        sSql &= "  create index ixSynced on custom.tbSalesMain (Synced ASC) "
                        sSql &= "END IF; "
                        SqlArray.Add(sSql)
                        sSql = "IF NOT EXISTS(SELECT I.* from systable T KEY JOIN sysindex I WHERE table_name = 'tbSalesMain' "
                        sSql &= "AND index_name = 'ixPollDate') THEN "
                        sSql &= "  create index ixPollDate on custom.tbSalesMain (PollDate ASC) "
                        sSql &= "END IF; "
                        SqlArray.Add(sSql)
                        sSql = "IF NOT EXISTS(SELECT I.* from systable T KEY JOIN sysindex I WHERE table_name = 'tbSalesMain' "
                        sSql &= "AND index_name = 'ixSequenceNo') THEN "
                        sSql &= "  create index ixSequenceNo on custom.tbSalesMain (SequenceNo ASC) "
                        sSql &= "END IF; "
                        SqlArray.Add(sSql)
                        sSql = "IF NOT EXISTS(SELECT I.* from systable T KEY JOIN sysindex I WHERE table_name = 'tbSalesMain' "
                        sSql &= "AND index_name = 'ixRecordType') THEN "
                        sSql &= "  create index ixRecordType on custom.tbSalesMain (RecordType ASC) "
                        sSql &= "END IF " & SybaseCommitWork
                        SqlArray.Add(sSql)

                        ' create the sql command to add ParentID column
                        Dim sqlBuilder As New StringBuilder(256)
                        sqlBuilder.Append("IF NOT EXISTS (SELECT column_name FROM systable T KEY JOIN syscolumn C ")
                        sqlBuilder.AppendLine("WHERE table_name = 'tbSalesMain' AND column_name = 'ParentID') THEN ")
                        sqlBuilder.AppendLine("ALTER TABLE custom.tbSalesMain ")
                        sqlBuilder.AppendLine("ADD ParentID INTEGER NULL ")
                        sqlBuilder.AppendFormat("END IF {0}", SybaseCommitWork)
                        SqlArray.Add(sqlBuilder.ToString())
                        ' create the sql command to add BusinessDay column
                        sqlBuilder = New StringBuilder(256)
                        sqlBuilder.Append("IF NOT EXISTS (SELECT column_name FROM systable T KEY JOIN syscolumn C ")
                        sqlBuilder.AppendLine("WHERE table_name = 'tbSalesMain' AND column_name = 'BusinessDay') THEN ")
                        sqlBuilder.AppendLine("ALTER TABLE custom.tbSalesMain ")
                        sqlBuilder.AppendLine("ADD BusinessDay DATETIME NULL ")
                        sqlBuilder.AppendFormat("END IF {0}", SybaseCommitWork)
                        SqlArray.Add(sqlBuilder.ToString())
                        ' create the sql command to add ItemDiscount column
                        sqlBuilder = New StringBuilder(256)
                        sqlBuilder.Append("IF NOT EXISTS (SELECT column_name FROM systable T KEY JOIN syscolumn C ")
                        sqlBuilder.AppendLine("WHERE table_name = 'tbSalesMain' AND column_name = 'ItemDiscount') THEN ")
                        sqlBuilder.AppendLine("ALTER TABLE custom.tbSalesMain ")
                        sqlBuilder.AppendLine("ADD ItemDiscount MONEY18 NULL ")
                        sqlBuilder.AppendFormat("END IF {0}", SybaseCommitWork)
                        SqlArray.Add(sqlBuilder.ToString())

                        ' create the sql command to add TaxAmount column
                        sqlBuilder = New StringBuilder(256)
                        sqlBuilder.Append("IF NOT EXISTS (SELECT column_name FROM systable T KEY JOIN syscolumn C ")
                        sqlBuilder.AppendLine("WHERE table_name = 'tbSalesMain' AND column_name = 'ItemTax') THEN ")
                        sqlBuilder.AppendLine("ALTER TABLE custom.tbSalesMain ")
                        sqlBuilder.AppendLine("ADD ItemTax MONEY18 NULL ")
                        sqlBuilder.AppendFormat("END IF {0}", SybaseCommitWork)
                        SqlArray.Add(sqlBuilder.ToString())

                        ' create the sql command to add TaxAmount column
                        sqlBuilder = New StringBuilder(256)
                        sqlBuilder.Append("IF NOT EXISTS (SELECT column_name FROM systable T KEY JOIN syscolumn C ")
                        sqlBuilder.AppendLine("WHERE table_name = 'tbSalesMain' AND column_name = 'SubTypeDescription') THEN ")
                        sqlBuilder.AppendLine("ALTER TABLE custom.tbSalesMain ")
                        sqlBuilder.AppendLine("ADD SubTypeDescription varchar(255) null ")
                        sqlBuilder.AppendFormat("END IF {0}", SybaseCommitWork)
                        SqlArray.Add(sqlBuilder.ToString())

                        For Each sCmd In SqlArray
                            If Not MMSGeneric_Execute(Use_ODBC, sCmd, sError) Then
                                Result = False
                                LogEvent("Index setup error (tbSalesMain): " & sError & vbCrLf & vbCrLf & "Sql: " & sCmd, EventLogEntryType.Error)
                            End If
                        Next
                    Else
                        Result = False
                        LogEvent("Table setup error (Checking tbSalesMain status): " & sError, EventLogEntryType.Error)
                    End If
                    ' ------------------------------------------------
                    ' Next setup tbStoreInfo if it does not exist
                    SqlArray.Clear()
                    sSql = "SELECT object_id('tbStoreInfo') As ObjectId;"
                    If MMSGeneric_ListAll(Use_ODBC, sSql, "Objects", ds, sError, bNoMSA_Flag) Then
                        If ds.Tables("Objects").Rows(0).Item("ObjectId") Is DBNull.Value Then
                            sSql = "    create table custom.tbStoreInfo("
                            sSql &= "      StoreID integer NULL , "
                            sSql &= "      RegisterID integer NULL , "
                            sSql &= "      UniqueIDCount integer NOT NULL DEFAULT(0), "
                            sSql &= "      LastAwake DATETIME NULL "
                            sSql &= "      );"
                            sSql &= "    grant all privileges on custom.tbStoreinfo to public" & SybaseCommitWork
                            SqlArray.Add(sSql)

                            For Each sCmd In SqlArray
                                If Not MMSGeneric_Execute(Use_ODBC, sCmd, sError) Then
                                    Result = False
                                    LogEvent("Table setup error (tbStoreInfo): " & sError & vbCrLf & vbCrLf & "Sql: " & sCmd, EventLogEntryType.Error)
                                End If
                            Next
                            If Result Then
                                LogEvent("tbStoreInfo table created", EventLogEntryType.Information)
                                '--------------------
                                'Copy the value from micros.res_def into StoreId (This used to be where the StoreId was kept)
                                SqlArray.Clear()
                                sSql = "SELECT Top 1 IsNull(location_name_2, -1) AS EntityId From micros.rest_def"
                                If MMSGeneric_ListAll(Use_ODBC, sSql, "StoreInfo", ds, sError, bNoMSA_Flag) AndAlso _
                                    ds.Tables("StoreInfo").Rows.Count = 1 AndAlso _
                                    IsNumeric(ds.Tables("StoreInfo").Rows(0).Item("EntityId")) Then
                                    sSql = String.Format("INSERT INTO custom.tbStoreInfo (StoreId) VALUES ({0}){1}", ds.Tables("StoreInfo").Rows(0).Item("EntityId"), SybaseCommitWork)
                                    SqlArray.Add(sSql)

                                    For Each sCmd In SqlArray
                                        If Not MMSGeneric_Execute(Use_ODBC, sCmd, sError) Then
                                            Result = False
                                            LogEvent("Table setup error (tbStoreInfo - StoreId): " & sError & vbCrLf & vbCrLf & "Sql: " & sCmd, EventLogEntryType.Error)
                                        End If
                                    Next
                                    If Result Then
                                        LogEvent("StoreId migrated to tbStoreInfo from micros.rest_def", EventLogEntryType.Information)
                                    End If
                                End If
                            End If
                        End If
                    End If
                    '-----------------------
                    If sVersion = "VER2" Then ' For Micros VER2 sites create the stored procedure
                        ' 1st delete the version 1 stored procedure (written by Micros) as we need it
                        ' to stop populating tbSalesMain
                        sSql = "if NOT (select object_id('custom.spMacromatiX_SalesExport')) IS NULL Then "
                        sSql &= "   DROP PROCEDURE custom.spMacromatiX_SalesExport "
                        sSql &= "end if " & SybaseCommitWork
                        If Not MMSGeneric_Execute(Use_ODBC, sSql, sError) Then
                            Result = False
                            LogEvent("Table setup error (Dropping SalesExport stored procedure): " & sError, EventLogEntryType.Error)
                        Else
                            LogEvent("Procedure spMacromatiX_SalesExport dropped if it existed.", EventLogEntryType.Information)
                        End If

                        sSql = "if NOT (select object_id('custom.spMacromatix_TA_Export')) IS NULL Then "
                        sSql &= "   DROP PROCEDURE custom.spMacromatix_TA_Export "
                        sSql &= "end if " & SybaseCommitWork
                        If Not MMSGeneric_Execute(Use_ODBC, sSql, sError) Then
                            Result = False
                            LogEvent("Table setup error (Dropping TAExport stored procedure): " & sError, EventLogEntryType.Error)
                        Else
                            LogEvent("Procedure spMacromatix_TA_Export dropped if it existed.", EventLogEntryType.Information)
                        End If

                        'Check for parent_dtl_seq column exists in the Micros.mi_dtl table
                        Dim isParentDtlColumnExist As Boolean = False
                        Dim sSqlParentDtlColumn = "select column_name FROM systable T KEY JOIN syscolumn C" & vbCrLf
                        sSqlParentDtlColumn &= "WHERE table_name='mi_dtl' and column_name='parent_dtl_seq'"
                        If MMSGeneric_ListAll(Use_ODBC, sSqlParentDtlColumn, "ParentDtlColumnExist", ds, sError, bNoMSA_Flag) Then
                            If ds.Tables("ParentDtlColumnExist").Rows.Count = 0 Then
                                isParentDtlColumnExist = False
                            Else
                                isParentDtlColumnExist = True
                            End If
                        End If

                        'Check for void_chk_seq column exists in the Micros.chk_dtl table
                        Dim isVoidChkSeqColumnExist As Boolean = False
                        Dim sSqlVoidChkSeqColumn = "select column_name FROM systable T KEY JOIN syscolumn C" & vbCrLf
                        sSqlVoidChkSeqColumn &= "WHERE table_name='chk_dtl' and column_name='void_chk_seq'"
                        If MMSGeneric_ListAll(Use_ODBC, sSqlVoidChkSeqColumn, "VoidChkSeqColumnExist", ds, sError, bNoMSA_Flag) Then
                            If ds.Tables("VoidChkSeqColumnExist").Rows.Count = 0 Then
                                isVoidChkSeqColumnExist = False
                            Else
                                isVoidChkSeqColumnExist = True
                            End If
                        End If

                        'Check for prefix_override_level  column exists in the Micros.mi_dtl  table
                        Dim isPrefixOverrideLvlColumnExist As Boolean = False
                        Dim sSqlPrefixOverrideLvlColumn = "select column_name FROM systable T KEY JOIN syscolumn C" & vbCrLf
                        sSqlPrefixOverrideLvlColumn &= "WHERE table_name='mi_dtl' and column_name='prefix_override_level'"
                        If MMSGeneric_ListAll(Use_ODBC, sSqlPrefixOverrideLvlColumn, "PrefixOverrideLvlColumnExist", ds, sError, bNoMSA_Flag) Then
                            If ds.Tables("PrefixOverrideLvlColumnExist").Rows.Count = 0 Then
                                isPrefixOverrideLvlColumnExist = False
                            Else
                                isPrefixOverrideLvlColumnExist = True
                            End If
                        End If

                        '---------------------------
                        sSql = "SELECT object_id('custom.spMacromatiX_GetCheck') As ObjectId;"
                        If MMSGeneric_ListAll(Use_ODBC, sSql, "Objects", ds, sError, bNoMSA_Flag) Then
                            If ds.Tables("Objects").Rows(0).Item("ObjectId") Is DBNull.Value Then
                                sSql = "CREATE PROCEDURE custom.spMacromatiX_GetCheck(@chk_seq SEQ_NUM) " & vbCrLf
                            Else
                                sSql = "ALTER PROCEDURE custom.spMacromatiX_GetCheck(@chk_seq SEQ_NUM) " & vbCrLf
                                CreateProc = False
                            End If
                            sSql &= "/*" & vbCrLf
                            sSql &= "    -- Procedure written by Richard Miller, MacromatiX Pty Ltd, 28/Sep/2005 " & vbCrLf
                            sSql &= "    -- Altered by Richard Miller 7 Dec 2005 to deal with Reopened Checks. " & vbCrLf
                            sSql &= "*/" & vbCrLf
                            sSql &= "    AS " & vbCrLf
                            sSql &= "BEGIN " & vbCrLf
                            sSql &= "       SELECT DISTINCT " & vbCrLf ' use distinct to remove duplicate ref_dtl entries
                            sSql &= "        TransactionId=C.chk_seq, " & vbCrLf
                            sSql &= "        CheckNumber=C.chk_num, " & vbCrLf
                            sSql &= "        RevenueCentre=C.rvc_seq, " & vbCrLf
                            sSql &= "        RegisterId=C.last_uws_seq, " & vbCrLf
                            If LiveLinkConfiguration.MicrosAlternateCheckClosedTimeEnabled Then
                                sSql &= "        PollDate= coalesce(C.chk_clsd_date_time,ACC.closed_time), " & vbCrLf
                            Else
                                sSql &= "        PollDate=C.chk_clsd_date_time, " & vbCrLf
                            End If
                            sSql &= "        CheckTotal=C.sub_ttl, " & vbCrLf
                            sSql &= "        AmountDueTotal=C.amt_due_ttl, " & vbCrLf
                            sSql &= "        CoverCount=C.cov_cnt, " & vbCrLf
                            sSql &= "        CheckAdded=C.ob_ccs04_chk_added, " & vbCrLf
                            sSql &= "        CheckReopened=C.ob_chk_reopened, " & vbCrLf
                            sSql &= "        CheckEdited=C.ob_ccs11_chk_edited, " & vbCrLf
                            sSql &= "        CheckNumDetail=C.num_dtl, " & vbCrLf
                            If isVoidChkSeqColumnExist Then
                                sSql &= "        VD.VoidCheckNumDetail, " & vbCrLf
                                sSql &= "        VD.VoidLastDetailNum, " & vbCrLf
                                sSql &= "        VD.VoidCheckSeq, " & vbCrLf
                                sSql &= "        VD.VoidCheckNum, " & vbCrLf
                                sSql &= "        VD.ReasonSeq, " & vbCrLf
                            End If
                            sSql &= "        BusinessDay=T.business_date, " & vbCrLf
                            sSql &= "        MTC.tax_class_seq, " & vbCrLf
                            sSql &= "        CI.line_01, " & vbCrLf ' check information, stores loyalty card number linked to sale
                            sSql &= "        CI.line_02, " & vbCrLf ' check information, stores online/offline flag for loyalty
                            sSql &= "        CI.line_03, " & vbCrLf ' check information, stores loyalty benefit information
                            sSql &= "        CI.line_04, " & vbCrLf ' check information, stores loyalty benefit information continued..
                            sSql &= "        CI.line_05, " & vbCrLf ' check information, stores loyalty benefit information continued..
                            sSql &= "        CI.line_06, " & vbCrLf ' check information, stores loyalty benefit information continued..
                            sSql &= "        CI.line_07, " & vbCrLf ' check information, stores loyalty benefit information continued..
                            sSql &= "        CI.line_08, " & vbCrLf ' check information, stores loyalty benefit information continued..
                            sSql &= "        CI.line_09, " & vbCrLf ' check information, stores loyalty benefit information continued..
                            sSql &= "        CI.line_10, " & vbCrLf ' check information, stores loyalty benefit information continued..
                            sSql &= "        CI.line_11, " & vbCrLf ' check information, stores loyalty benefit information continued..
                            sSql &= "        CI.line_12, " & vbCrLf ' check information, stores loyalty benefit information continued..
                            sSql &= "        CI.line_13, " & vbCrLf ' check information, stores loyalty benefit information continued..
                            sSql &= "        CI.line_14, " & vbCrLf ' check information, stores loyalty benefit information continued..
                            sSql &= "        CI.line_15, " & vbCrLf ' check information, stores loyalty benefit information continued..
                            sSql &= "        CI.line_16, " & vbCrLf ' check information, stores loyalty benefit information continued..                            
                            sSql &= "        ref=MRD.ref, " & vbCrLf ' reference number, stores any sold loyalty card information
                            sSql &= "        ref2=MRD2.ref, " & vbCrLf ' stores discount memo or tax invoice number
                            sSql &= "        ref3=MRD3.ref, " & vbCrLf ' stores gift card number for payment by gift card (SVC)
                            sSql &= "        ObjectNumber = M.obj_num + 0, " & vbCrLf ' item object number (like PLU) - need to add 0 to remove the automatic unique constraint that gets added
                            sSql &= "        C.num_chk_info_lines, " & vbCrLf
                            sSql &= "        C.num_mi_dtl, " & vbCrLf
                            sSql &= "        C.tax_ttl, " & vbCrLf
                            sSql &= "        PollCount= " & vbCrLf
                            sSql &= "            case when D.ob_item_shared = 'T' then " & vbCrLf
                            sSql &= "                case when D.chk_ttl < 0 then " & vbCrLf
                            sSql &= "/*" & vbCrLf
                            sSql &= "                        -- With split checks only, the fraction component does not get the -ve sign on a voided transaction " & vbCrLf
                            sSql &= "                        -- This workaround should work now and when this gets resolved " & vbCrLf
                            sSql &= "*/" & vbCrLf
                            sSql &= "                        -abs(convert(real,D.shared_numerator)/D.shared_denominator)  " & vbCrLf
                            sSql &= "                else " & vbCrLf
                            sSql &= "                    convert(real,D.shared_numerator)/D.shared_denominator " & vbCrLf
                            sSql &= "                end " & vbCrLf
                            sSql &= "            else " & vbCrLf
                            sSql &= "                D.chk_cnt " & vbCrLf
                            sSql &= "            end, " & vbCrLf
                            sSql &= "        PollAmount=D.chk_ttl-D.inclusive_tax_ttl, " & vbCrLf
                            sSql &= "        ItemDiscount=IDD.ItemDiscount-IDD.ItemDiscountTax, " & vbCrLf ' Item Level Discount
                            sSql &= "        ItemDiscountTax=IDD.ItemDiscountTax, " & vbCrLf ' Item Level Discount Tax
                            sSql &= "        Tax=d.inclusive_tax_ttl, " & vbCrLf
                            sSql &= "        VoidFlag= " & vbCrLf
                            sSql &= "            case  " & vbCrLf
                            sSql &= "                when D.ob_dtl05_void_flag = 'T' and D.ob_error_correct = 'T' then  " & vbCrLf
                            sSql &= "                    'E' " & vbCrLf
                            sSql &= "                when D.ob_dtl05_void_flag = 'T' and D.ob_error_correct = 'F' then  " & vbCrLf
                            sSql &= "                    'V'  " & vbCrLf
                            sSql &= "                else " & vbCrLf
                            sSql &= "                    null " & vbCrLf
                            sSql &= "            end, " & vbCrLf
                            sSql &= "        ApplyTax= " & vbCrLf
                            sSql &= "            case when D.inclusive_tax_ttl <> 0 then  " & vbCrLf
                            sSql &= "                1  " & vbCrLf
                            sSql &= "            else  " & vbCrLf
                            sSql &= "                0  " & vbCrLf
                            sSql &= "            end, " & vbCrLf
                            sSql &= "        ClerkId=C.emp_seq, " & vbCrLf
                            sSql &= "        ClerkId2=E.obj_num, " & vbCrLf
                            sSql &= "        ClerkName=TRIM(E." & LiveLinkConfiguration.MicrosFirstNameField & "+' '+E." & LiveLinkConfiguration.MicrosLastNameField & "), " & vbCrLf
                            sSql &= "        SequenceNo=D.dtl_id, " & vbCrLf
                            sSql &= "        D.trans_seq, " & vbCrLf
                            sSql &= "        D.dtl_seq, " & vbCrLf
                            sSql &= "        D.dtl_index, " & vbCrLf
                            sSql &= "        D.record_type, " & vbCrLf
                            sSql &= "        D.dtl_type, " & vbCrLf
                            sSql &= "        DD.parent_dtl_seq, " & vbCrLf
                            sSql &= "        DD.parent_dtl_id, " & vbCrLf
                            sSql &= "        ServiceType=DM.type, " & vbCrLf
                            sSql &= "        PluCodeId= " & vbCrLf
                            sSql &= "            case D.dtl_type when 'D' then " & vbCrLf
                            sSql &= "                DM.dsvc_seq " & vbCrLf
                            sSql &= "            when 'M' then  " & vbCrLf
                            sSql &= "                M.mi_seq " & vbCrLf
                            sSql &= "            when 'T' then  " & vbCrLf
                            sSql &= "                TM.tmed_seq " & vbCrLf
                            sSql &= "            end, " & vbCrLf
                            sSql &= "        PluCode= " & vbCrLf
                            sSql &= "            case  " & vbCrLf
                            sSql &= "                when D.dtl_type = 'D' then  " & vbCrLf
                            sSql &= "                    convert(varchar(50),DM.name) " & vbCrLf
                            sSql &= "                when D.dtl_type = 'M' then  " & vbCrLf
                            sSql &= "                    M.name_1 " & vbCrLf
                            sSql &= "                when D.dtl_type = 'T' then  " & vbCrLf
                            sSql &= "                    TM.name " & vbCrLf
                            sSql &= "            end, " & vbCrLf
                            sSql &= "        PriceLevel= " & vbCrLf
                            sSql &= "            case  " & vbCrLf
                            sSql &= "                when D.dtl_type = 'M' then  " & vbCrLf
                            sSql &= "                    'L'+convert(varchar(3),MI.price_lvl)  " & vbCrLf
                            sSql &= "                else null " & vbCrLf
                            sSql &= "            end, " & vbCrLf
                            If isPrefixOverrideLvlColumnExist Then
                                sSql &= "        ModifierPrefixOverrideLevel= " & vbCrLf
                                sSql &= "            case  " & vbCrLf
                                sSql &= "                when MI.prefix_override_level is not null then  " & vbCrLf
                                sSql &= "                    'L'+convert(varchar(3),MI.prefix_override_level)  " & vbCrLf
                                sSql &= "                else null " & vbCrLf
                                sSql &= "            end, " & vbCrLf
                            End If
                            sSql &= "        ComboMealNumber=MI.combo_meal_num, " & vbCrLf
                            sSql &= "        ComboGroupSeq=MI.combo_grp_seq, " & vbCrLf
                            sSql &= "        ModifierDetailSeq=MI.dtl_seq, " & vbCrLf
                            If isParentDtlColumnExist Then
                                sSql &= "        ModifierParentDetailSeq=MI.parent_dtl_seq, " & vbCrLf
                            Else
                                sSql &= "        ModifierParentDetailSeq=null, " & vbCrLf
                            End If
                            sSql &= "       C.uws_seq " & vbCrLf
                            sSql &= "    FROM " & vbCrLf
                            sSql &= "        micros.chk_dtl as C join " & vbCrLf
                            sSql &= "        micros.trans_dtl as T on C.chk_seq = T.chk_seq join " & vbCrLf
                            sSql &= "        micros.dtl as D on T.trans_seq = D.trans_seq left outer join " & vbCrLf
                            If isVoidChkSeqColumnExist Then
                                sSql &= "        ( " & vbCrLf
                                sSql &= "		    select " & vbCrLf
                                sSql &= "	    		C.chk_seq, " & vbCrLf
                                sSql &= "			    count(*) as VoidCheckNumDetail, " & vbCrLf
                                sSql &= "			    max(VD.dtl_id) as VoidLastDetailNum, " & vbCrLf
                                sSql &= "               C.void_chk_seq as VoidCheckSeq, " & vbCrLf
                                sSql &= "               VC.chk_num as VoidCheckNum, " & vbCrLf
                                sSql &= "               C.reason_seq as ReasonSeq " & vbCrLf
                                sSql &= "	    	from " & vbCrLf
                                sSql &= "   			micros.chk_dtl C " & vbCrLf
                                sSql &= "		    join micros.trans_dtl VTD " & vbCrLf
                                sSql &= "	    	on VTD.chk_seq = C.void_chk_seq " & vbCrLf
                                sSql &= "		    join micros.dtl VD " & vbCrLf
                                sSql &= "	    	on VD.trans_seq = VTD.trans_seq " & vbCrLf
                                sSql &= "           join micros.chk_dtl VC on VC.chk_seq=C.void_chk_seq " & vbCrLf
                                sSql &= "   		where " & vbCrLf
                                sSql &= "	    		C.chk_seq = @chk_seq " & vbCrLf
                                sSql &= "   		group by " & vbCrLf
                                sSql &= "		    	C.chk_seq,C.void_chk_seq,VC.chk_num,C.reason_seq " & vbCrLf
                                sSql &= "        ) as VD on C.chk_seq=VD.chk_seq left outer join " & vbCrLf
                            End If
                            sSql &= "        micros.emp_def as E on C.emp_seq = E.emp_seq left outer join " & vbCrLf
                            sSql &= "        micros.chk_info_dtl as CI on C.chk_seq = CI.chk_seq left outer join " & vbCrLf
                            sSql &= "        micros.mi_dtl as MI on d.trans_seq = MI.trans_seq and d.dtl_seq = MI.dtl_seq left outer join " & vbCrLf
                            sSql &= "        micros.mi_def as M on MI.mi_seq = M.mi_seq left outer join " & vbCrLf
                            sSql &= "        micros.mi_type_class_def as MTC on MTC.mi_type_seq = M.mi_type_seq left outer join " & vbCrLf
                            sSql &= "       (" & vbCrLf
                            sSql &= "         select trans_seq,parent_dtl_seq, max(ref) as ref" & vbCrLf
                            sSql &= "         from(micros.ref_dtl)" & vbCrLf
                            sSql &= "         group by  trans_seq,parent_dtl_seq) as MRD on MI.dtl_seq = MRD.parent_dtl_seq and T.trans_seq = MRD.trans_seq left outer join " & vbCrLf
                            sSql &= "       (" & vbCrLf
                            sSql &= "         select trans_seq, parent_trans_seq, parent_dtl_seq, ref_type, max(ref) as ref" & vbCrLf
                            sSql &= "         from micros.ref_dtl" & vbCrLf
                            sSql &= "         group by trans_seq, parent_trans_seq, parent_dtl_seq, ref_type" & vbCrLf
                            sSql &= "       ) as MRD2 on (D.dtl_type = 'D' and D.dtl_seq = MRD2.parent_dtl_seq and D.trans_seq = MRD2.parent_trans_seq)" & vbCrLf
                            sSql &= "                 or (D.dtl_type = 'T' and MRD2.ref_type = 1 and D.trans_seq = MRD2.trans_seq)" & vbCrLf
                            sSql &= "        left outer join" & vbCrLf
                            ' Additioanl ref detail to get the Gift card (SVC) numbers for payments
                            sSql &= "       (" & vbCrLf
                            sSql &= "         select trans_seq, parent_dtl_seq, max(ref) as ref" & vbCrLf
                            sSql &= "         from micros.ref_dtl where ref_type = 0 " & vbCrLf
                            sSql &= "         group by  trans_seq,parent_dtl_seq) as MRD3 on (D.dtl_type = 'T' and D.dtl_seq = MRD3.parent_dtl_seq and D.trans_seq = MRD3.trans_seq) left outer join " & vbCrLf
                            sSql &= "        micros.tmed_dtl as TD on d.trans_seq = TD.trans_seq and d.dtl_seq = TD.dtl_seq left outer join " & vbCrLf
                            sSql &= "        micros.tmed_def as TM on TD.tmed_seq = TM.tmed_seq left outer join " & vbCrLf
                            sSql &= "        micros.dsvc_dtl as DD on d.trans_seq = DD.trans_seq and d.dtl_seq = DD.dtl_seq left outer join " & vbCrLf
                            sSql &= "        micros.dsvc_def as DM on DD.dsvc_seq = DM.dsvc_seq left outer join " & vbCrLf
                            ' additional sub query here to get a sum of item level discount to it's parent item and to include it in the single row result for an item in the "ItemDiscount" value
                            If (LiveLinkConfiguration.MicrosApplyTransactionDiscountToItems) Then
                                sSql &= "       ( " & vbCrLf
                                sSql &= "		    select " & vbCrLf
                                sSql &= "	    		C.chk_seq, " & vbCrLf
                                sSql &= "   			DD.parent_dtl_id, " & vbCrLf
                                sSql &= "			    sum(D.rpt_ttl) as ItemDiscount, " & vbCrLf
                                sSql &= "		    	sum(D.rpt_inclusive_tax_ttl) as ItemDiscountTax " & vbCrLf
                                sSql &= "	    	from " & vbCrLf
                                sSql &= "   			micros.chk_dtl C " & vbCrLf
                                sSql &= "		    join micros.trans_dtl T " & vbCrLf
                                sSql &= "	    	on C.chk_seq = T.chk_seq " & vbCrLf
                                sSql &= "   		join " & vbCrLf
                                sSql &= "		    	micros.dsvc_dtl DD " & vbCrLf
                                sSql &= "	    	on T.trans_seq = DD.trans_seq " & vbCrLf
                                sSql &= "   		join " & vbCrLf
                                sSql &= "		    	micros.dtl D " & vbCrLf
                                sSql &= "	    	on D.trans_seq = DD.trans_seq " & vbCrLf
                                sSql &= "   		and D.dtl_seq = DD.dtl_seq " & vbCrLf
                                sSql &= "   		where " & vbCrLf
                                sSql &= "	    		C.chk_seq = @chk_seq " & vbCrLf
                                sSql &= "   		group by " & vbCrLf
                                sSql &= "		    	C.chk_seq, " & vbCrLf
                                sSql &= "	    		DD.parent_dtl_id " & vbCrLf
                                sSql &= "        ) as IDD on C.chk_seq = IDD.Chk_seq AND D.dtl_id=IDD.parent_dtl_id " & vbCrLf
                            Else
                                sSql &= "       ( " & vbCrLf
                                sSql &= "		    select " & vbCrLf
                                sSql &= "	    		DD.parent_trans_seq, " & vbCrLf
                                sSql &= "   			DD.parent_dtl_seq, " & vbCrLf
                                sSql &= "			    sum(D.chk_ttl) as ItemDiscount, " & vbCrLf
                                sSql &= "		    	sum(D.inclusive_tax_ttl) as ItemDiscountTax " & vbCrLf
                                sSql &= "	    	from " & vbCrLf
                                sSql &= "   			micros.chk_dtl C " & vbCrLf
                                sSql &= "		    join micros.trans_dtl T " & vbCrLf
                                sSql &= "	    	on C.chk_seq = T.chk_seq " & vbCrLf
                                sSql &= "   		join " & vbCrLf
                                sSql &= "		    	micros.dsvc_dtl DD " & vbCrLf
                                sSql &= "	    	on T.trans_seq = DD.trans_seq " & vbCrLf
                                sSql &= "   		join " & vbCrLf
                                sSql &= "		    	micros.dtl D " & vbCrLf
                                sSql &= "	    	on D.trans_seq = DD.trans_seq " & vbCrLf
                                sSql &= "   		and D.dtl_seq = DD.dtl_seq " & vbCrLf
                                sSql &= "   		where " & vbCrLf
                                sSql &= "	    		C.chk_seq = @chk_seq " & vbCrLf
                                sSql &= "   		group by " & vbCrLf
                                sSql &= "		    	DD.parent_trans_seq, " & vbCrLf
                                sSql &= "	    		DD.parent_dtl_seq " & vbCrLf
                                sSql &= "        ) as IDD on D.trans_seq = IDD.parent_trans_seq AND D.dtl_seq=IDD.parent_dtl_seq " & vbCrLf
                            End If
                            If LiveLinkConfiguration.MicrosAlternateCheckClosedTimeEnabled Then
                                sSql &= "	join " & vbCrLf
                                sSql &= "       ( " & vbCrLf
                                sSql &= "		    select " & vbCrLf
                                sSql &= "	    		chk_seq, " & vbCrLf
                                sSql &= "			    max(end_date_tm) as closed_time " & vbCrLf
                                sSql &= "	    	from " & vbCrLf
                                sSql &= "   			micros.trans_dtl " & vbCrLf
                                sSql &= "   		where " & vbCrLf
                                sSql &= "	    		chk_seq = @chk_seq " & vbCrLf
                                sSql &= "   		group by " & vbCrLf
                                sSql &= "	    		chk_seq " & vbCrLf
                                sSql &= "        ) as ACC on C.chk_seq = ACC.chk_seq " & vbCrLf
                            End If
                            sSql &= "    WHERE " & vbCrLf
                            sSql &= "        C.chk_seq = @chk_seq  " & vbCrLf

                            If LiveLinkConfiguration.MicrosIgnoreSkipItems Then
                                sSql &= "    AND    D.record_type <> 'S'  " & vbCrLf
                            End If

                            sSql &= "    ORDER BY " & vbCrLf
                            sSql &= "        C.chk_seq asc, D.trans_seq asc, D.dtl_seq asc " & vbCrLf
                            sSql &= "END " & vbCrLf


                            If Not MMSGeneric_Execute(Use_ODBC, sSql, sError) Then
                                Result = False
                                LogEvent("Table setup error (GetCheck stored procedure): " & sError & vbCrLf & vbCrLf & "Sql: " & sSql, EventLogEntryType.Error)
                            Else
                                If CreateProc Then
                                    LogEvent("Procedure spMacromatiX_GetCheck created.", EventLogEntryType.Information)
                                Else
                                    LogEvent("Procedure spMacromatiX_GetCheck altered.", EventLogEntryType.Information)
                                End If
                            End If
                        Else
                            Result = False
                            LogEvent("Table setup error (Checking GetCheck stored procedure): " & sError, EventLogEntryType.Error)
                        End If
                    End If

                    ' setup tbLocalUsers if it does not exist
                    sSql = "SELECT object_id('tbLocalUsers') As ObjectId;"
                    If MMSGeneric_ListAll(Use_ODBC, sSql, "Objects", ds, sError, bNoMSA_Flag) Then
                        If ds.Tables("Objects").Rows(0).Item("ObjectId") Is DBNull.Value Then
                            sCmd = GenerateLocalUsersTableSybase()
                            If Not MMSGeneric_Execute(Use_ODBC, sCmd, sError) Then
                                Result = False
                                LogEvent("Table setup error (tbLocalUsers): " & sError & vbCrLf & vbCrLf & "Sql: " & sCmd, EventLogEntryType.Error)
                            Else
                                LogEvent("Table setup complete (tbLocalUsers)", EventLogEntryType.Information)
                            End If
                        Else
                            ' run any required alter statements for the tbLocalUsers table
                            SqlArray.Clear()
                            AlterLocalUsersTableSybase(SqlArray)
                            For Each sCmd In SqlArray
                                If Not MMSGeneric_Execute(Use_ODBC, sCmd, sError) Then
                                    Result = False
                                    LogEvent("Table alter complete (tbLocalUsers): " & sError & vbCrLf & vbCrLf & "Sql: " & sCmd, EventLogEntryType.Error)
                                End If
                            Next
                        End If
                    End If

                    'Get the StoredValue transactions if the config "MicrosStoredValueValidationEnabled" is enable
                    If (LiveLinkConfiguration.MicrosStoredValueValidationEnabled) Then
                        sSql = "SELECT object_id('MX_ref_dtl') As ObjectId;"
                        If MMSGeneric_ListAll(Use_ODBC, sSql, "Objects", ds, sError, bNoMSA_Flag) Then
                            If ds.Tables("Objects").Rows(0).Item("ObjectId") IsNot DBNull.Value Then
                                sSql = "SELECT object_id('custom.spMacromatiX_GetSV_Ref_dtl') As ObjectId;"
                                If MMSGeneric_ListAll(Use_ODBC, sSql, "Objects", ds, sError, bNoMSA_Flag) Then
                                    If ds.Tables("Objects").Rows(0).Item("ObjectId") Is DBNull.Value Then
                                        sSql = "CREATE PROCEDURE custom.spMacromatiX_GetSV_Ref_dtl(@chk_seq SEQ_NUM) " & vbCrLf
                                    Else
                                        sSql = "ALTER PROCEDURE custom.spMacromatiX_GetSV_Ref_dtl(@chk_seq SEQ_NUM) " & vbCrLf
                                        CreateProc = False
                                    End If
                                    sSql &= "    AS " & vbCrLf
                                    sSql &= "BEGIN " & vbCrLf
                                    sSql &= "       SELECT " & vbCrLf
                                    sSql &= "        M.Chk_Seq, " & vbCrLf
                                    sSql &= "        C.Chk_Num, " & vbCrLf
                                    sSql &= "        M.Auth_Code, " & vbCrLf
                                    sSql &= "        M.CN_Clear, " & vbCrLf
                                    sSql &= "        M.CN_Masked, " & vbCrLf
                                    sSql &= "        M.CN_Hash, " & vbCrLf
                                    sSql &= "        M.Ex_Rate, " & vbCrLf
                                    sSql &= "        M.Currency, " & vbCrLf
                                    sSql &= "        M.Balance, " & vbCrLf
                                    sSql &= "        M.Ext_Auth_Code, " & vbCrLf
                                    sSql &= "        M.Activity, " & vbCrLf
                                    sSql &= "        M.OfflineFlag, " & vbCrLf
                                    sSql &= "        M.Amount " & vbCrLf
                                    sSql &= "	     FROM " & vbCrLf
                                    sSql &= "   			custom.MX_ref_dtl as M" & vbCrLf
                                    sSql &= "               JOIN micros.chk_dtl as C ON C.Chk_Seq=M.Chk_Seq " & vbCrLf
                                    sSql &= "    WHERE " & vbCrLf
                                    sSql &= "        M.chk_seq = @chk_seq  " & vbCrLf
                                    sSql &= "    ORDER BY " & vbCrLf
                                    sSql &= "        M.chk_seq asc " & vbCrLf
                                    sSql &= "END " & vbCrLf
                                    If Not MMSGeneric_Execute(Use_ODBC, sSql, sError) Then
                                        Result = False
                                        LogEvent("Table setup error (GetSV_Ref_dtl stored procedure): " & sError & vbCrLf & vbCrLf & "Sql: " & sSql, EventLogEntryType.Error)
                                    Else
                                        If CreateProc Then
                                            LogEvent("Procedure spMacromatiX_GetSV_Ref_dtl created.", EventLogEntryType.Information)
                                        Else
                                            LogEvent("Procedure spMacromatiX_GetSV_Ref_dtl altered.", EventLogEntryType.Information)
                                        End If
                                    End If
                                Else
                                    Result = False
                                    LogEvent("Table setup error (Checking GetSV_Ref_dtl stored procedure): " & sError, EventLogEntryType.Error)
                                End If
                            End If
                        End If

                    End If

                    SetupTablesAndStoredProcedures = Result

                Case Else
                    ' "SIGNATURE", "PARINFUSION", "MICROS9700", "PCAMERICA", "COMPRIS", "PAN7700", "INTOUCH", "PAREXALT4", "SILVERWARE", "OCTANE", "ICG", "MICROSOFTRMS", "NEWPOS", 
                    ' "IPOS", "UNIWELL", "XPient", "Transight" ...

                    ' Use this definition for all POS types by default
                    'sSql = "IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[tbSalesMain]') AND type in (N'U'))"
                    sSql = "CREATE TABLE [dbo].[tbSalesMain] ( "
                    sSql &= "   [SalesMainID] [bigint] IDENTITY (1, 1) NOT NULL , "
                    sSql &= "   [RecordType] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL , "
                    sSql &= "   [TransactionID] [bigint] NULL , "
                    sSql &= "   [RecordSubType] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL , "
                    sSql &= "   [EntityID] [int] NULL , "
                    sSql &= "	[RegisterID] [int] NULL , "
                    sSql &= "	[PollDate] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL , "
                    sSql &= "   [PollCount] [float] NULL , "
                    sSql &= "   [PollAmount] [money] NULL , "
                    sSql &= "   [ClerkID] [int] NULL , "
                    sSql &= "   [ClerkName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL , "
                    sSql &= "   [CustomerID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL , "
                    sSql &= "   [Synced] [int] NULL , "
                    sSql &= "   [PLUCodeID] [bigint] NULL , "
                    sSql &= "   [PLUCode] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL , "
                    sSql &= "   [SequenceNo] [int] NULL , "
                    sSql &= "   [Flag] [int] NULL , "
                    sSql &= "   [ApplyTax] [int] NULL , "
                    sSql &= "   [ItemTax] [money] NULL , "
                    sSql &= "   [ItemDiscount] [money] NULL , "
                    sSql &= "   [PriceLevel] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL , "
                    sSql &= "   [DateAdded] [datetime] NULL , "
                    sSql &= "   [SubTypeDescription] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL , "
                    sSql &= "   [POSTransactionID] [bigint] NULL, "
                    sSql &= "   [TransactionVersion] [int] null DEFAULT 0," ' Used for reopened check handling
                    sSql &= "   [SyncToken] [nvarchar] (36) NULL, "
                    sSql &= "   [ParentId] [bigint] NULL, "
                    sSql &= "   [BusinessDay] [smalldatetime] NULL "
                    sSql &= ") ON [PRIMARY] "
                    SqlArray.Add(sSql)

                    'sSql = "IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[tbStoreInfo]') AND type in (N'U'))"
                    sSql = "CREATE TABLE [dbo].[tbStoreInfo] ( "
                    sSql &= "    [StoreID] [smallint] NULL , "
                    sSql &= "    [RegisterID] [smallint] NULL , "
                    sSql &= "    [UniqueIDCount] [bigint] NOT NULL , "
                    sSql &= "    [LastDate] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL , "
                    sSql &= "    [LastTransaction] [bigint] NOT NULL, "
                    sSql &= "    [LastAwake] [datetime] NULL, "
                    sSql &= "    [Status] [tinyint] NULL "
                    sSql &= ") ON [PRIMARY] "
                    SqlArray.Add(sSql)

                    ' CREATE INDEXES ON LIVELINK TABLES
                    sSql = "IF NOT EXISTS (select 1 from sysobjects where name='PK_tbSalesMain_1' and parent_obj = object_id(N'tbSalesMain'))"
                    sSql &= "ALTER TABLE [dbo].[tbSalesMain] WITH NOCHECK ADD "
                    sSql &= "CONSTRAINT [PK_tbSalesMain_1] PRIMARY KEY  CLUSTERED "
                    sSql &= "([SalesMainID]) ON [PRIMARY] "
                    SqlArray.Add(sSql)

                    sSql = "IF NOT EXISTS (select 1 from sysobjects where name='DF_tbSalesMain_Synced' and parent_obj = object_id(N'tbSalesMain'))"
                    sSql &= "ALTER TABLE [dbo].[tbSalesMain] ADD "
                    sSql &= "CONSTRAINT	[DF_tbSalesMain_Synced] DEFAULT 100 FOR Synced "
                    SqlArray.Add(sSql)

                    sSql = "if not exists (select 1 from syscolumns where name='LastAwake' and id = object_id(N'tbStoreInfo'))"
                    sSql &= "ALTER TABLE [dbo].[tbStoreInfo] ADD "
                    sSql &= "[LastAwake] [datetime] NULL"
                    SqlArray.Add(sSql)

                    sSql = "if not exists (select 1 from sysindexes where name='Synced' and id = object_id(N'tbSalesMain'))"
                    sSql &= " CREATE  INDEX [Synced] ON [dbo].[tbSalesMain]([Synced]) ON [PRIMARY]"
                    SqlArray.Add(sSql)

                    ' INDEX ADDED FOR SELECTING HEADER RECORDS TO SEND
                    sSql = "if not exists (select 1 from sysindexes where name='IX_tbSalesMain_RecordTypeSynced' and id = object_id(N'tbSalesMain')) "
                    sSql &= " CREATE INDEX IX_tbSalesMain_RecordTypeSynced ON dbo.tbSalesMain(RecordType,Synced) "
                    SqlArray.Add(sSql)

                    ' INDEX ADDED FOR SELECTING SEND DATA AND UPDATING SYNCTOKEN VALUES
                    sSql = "if not exists (select 1 from sysindexes where name='IX_tbSalesMain_TransactionIDPollDateTransactionVersionRecordType' and id = object_id(N'tbSalesMain')) "
                    sSql &= " CREATE INDEX IX_tbSalesMain_TransactionIDPollDateTransactionVersionRecordType ON dbo.tbSalesMain(TransactionID,PollDate,TransactionVersion,RecordType) "
                    SqlArray.Add(sSql)

                    ' INDEX UTILISED BY SOME POS INTEGRATIONS
                    sSql = "if not exists (select 1 from sysindexes where name='IX_tbSalesMain_BusinessDayRecordTypeSubType' and id = object_id(N'tbSalesMain')) "
                    sSql &= " CREATE INDEX IX_tbSalesMain_BusinessDayRecordTypeSubType ON dbo.tbSalesMain(BusinessDay,RecordType,RecordSubType) "
                    SqlArray.Add(sSql)

                    ' INDEX FOR OPTIMISED DELETION
                    sSql = "if not exists (select 1 from sysindexes where name='IX_tbSalesMain_BusinessDaySynced' and id = object_id(N'tbSalesMain')) "
                    sSql &= " CREATE INDEX IX_tbSalesMain_BusinessDaySynced ON dbo.tbSalesMain(BusinessDay,Synced) "
                    SqlArray.Add(sSql)

                    ' INDEX FOR DUPLICATED TRANSACTION
                    sSql = "if not exists (select 1 from sysindexes where name='IX_tbSalesMain_EntityIDBusinessDayRegisterIDTransactionID' and id = object_id(N'tbSalesMain')) "
                    sSql &= " CREATE INDEX IX_tbSalesMain_EntityIDBusinessDayRegisterIDTransactionID ON dbo.tbSalesMain(EntityID, BusinessDay, RegisterID, TransactionID) "
                    SqlArray.Add(sSql)

                    ' DROP UNUSED INDEXES FROM HISTORICAL LIVELINK INSTANCES
                    sSql = "if exists (select 1 from sysindexes where name='RecordType' and id = object_id(N'tbSalesMain'))"
                    sSql &= " DROP INDEX RecordType ON dbo.tbSalesMain"
                    SqlArray.Add(sSql)

                    sSql = "if exists (select 1 from sysindexes where name='TransactionID' and id = object_id(N'tbSalesMain'))"
                    sSql &= " DROP INDEX TransactionID ON dbo.tbSalesMain"
                    SqlArray.Add(sSql)

                    sSql = "if exists (select 1 from sysindexes where name='SequenceNo' and id = object_id(N'tbSalesMain'))"
                    sSql &= " DROP INDEX SequenceNo ON dbo.tbSalesMain"
                    SqlArray.Add(sSql)

                    sSql = "if exists (select 1 from sysindexes where name='RecordSubType' and id = object_id(N'tbSalesMain'))"
                    sSql &= " DROP INDEX RecordSubType ON dbo.tbSalesMain"
                    SqlArray.Add(sSql)

                    sSql = "if exists (select 1 from sysindexes where name='PLUCodeID' and id = object_id(N'tbSalesMain'))"
                    sSql &= " DROP INDEX PLUCodeID ON dbo.tbSalesMain"
                    SqlArray.Add(sSql)

                    sSql = "if exists (select 1 from sysindexes where name='PLUCode' and id = object_id(N'tbSalesMain'))"
                    sSql &= " DROP INDEX PLUCode ON dbo.tbSalesMain"
                    SqlArray.Add(sSql)

                    sSql = "if exists (select 1 from sysindexes where name='Flag' and id = object_id(N'tbSalesMain'))"
                    sSql &= " DROP INDEX Flag ON dbo.tbSalesMain"
                    SqlArray.Add(sSql)

                    ' DEFAULT CONSTRAINTS
                    sSql = "IF NOT EXISTS (select 1 from sysobjects where name='DF_tbStoreInfo_UniqueIDCount' and parent_obj = object_id(N'tbStoreInfo'))"
                    sSql &= " ALTER TABLE [dbo].[tbStoreInfo] ADD "
                    sSql &= " CONSTRAINT [DF_tbStoreInfo_UniqueIDCount] DEFAULT (0) FOR [UniqueIDCount]"
                    SqlArray.Add(sSql)

                    sSql = "IF NOT EXISTS (select 1 from sysobjects where name='DF_tbStoreInfo_LastDate' and parent_obj = object_id(N'tbStoreInfo'))"
                    sSql &= "ALTER TABLE [dbo].[tbStoreInfo] ADD "
                    sSql &= "CONSTRAINT [DF_tbStoreInfo_LastDate] DEFAULT ('01-Jan-1900 00:00:00') FOR [LastDate]"
                    SqlArray.Add(sSql)

                    sSql = "IF NOT EXISTS (select 1 from sysobjects where name='DF_tbStoreInfo_LastTransaction' and parent_obj = object_id(N'tbStoreInfo'))"
                    sSql &= "ALTER TABLE [dbo].[tbStoreInfo] ADD "
                    sSql &= "CONSTRAINT [DF_tbStoreInfo_LastTransaction] DEFAULT (0) FOR [LastTransaction]"
                    SqlArray.Add(sSql)

                    SqlArray.Add(GenerateLocalUsersTableSQL())
                    AlterLocalUsersTableSQL(SqlArray)
                    AddNewColumns(SqlArray)

                    Dim sCmd As String
                    Dim Result As Boolean = True
                    For Each sCmd In SqlArray
                        If Not MMSGeneric_Execute(sCmd, sError) Then
                            If LiveLinkConfiguration.DebugLogging Then
                                MMS.Utilities.MxLogger.LogErrorToTrace("LiveLink - SetupTablesAndStoredProcedures() - SQL: {0}", sCmd)
                                MMS.Utilities.MxLogger.LogErrorToTrace("LiveLink - SetupTablesAndStoredProcedures() - Error: {0}", sError)
                            End If
                            Result = False
                        End If
                    Next

                    ' Add additional stored procedures specific to POS types
                    Select Case POSType
                        Case "MICROS9700"
                            If Not Micros9700.Micros9700_CreateStoredProcedures() Then
                                Result = False
                            End If

                        Case "PAN7700"
                            If Not Mx.POS.Panasonic.PAN7700.PAN7700_CreateStoredProcedures() Then
                                Result = False
                            End If

                        Case "COMPRIS"
                            sSql = "CREATE TRIGGER MxComprisTransactionId ON tbSalesMain FOR INSERT AS "
                            sSql &= "BEGIN "
                            sSql &= "UPDATE tbSalesMain SET tbSalesMain.TransactionID=tbSalesMain.SalesMainId "
                            sSql &= "FROM tbSalesMain JOIN inserted ON tbSalesMain.SalesMainID=Inserted.SalesMainId "
                            sSql &= "WHERE tbSalesMain.RecordType='H' AND tbSalesMain.PriceLevel=1 "
                            sSql &= "END"

                            If Not MMSGeneric_Execute(sSql, sError) Then
                                Result = False
                            End If

                        Case "PAREXALT4"
                            sSql = "ALTER TABLE [dbo].[tbSalesMain] ADD "
                            sSql &= "[TransactionVersion] [int] null DEFAULT 0 WITH VALUES"

                            If Not MMSGeneric_Execute(sSql, sError) Then
                                Result = False
                            End If

                            ' check if the stored proc needs to be created or altered
                            sSql = "SELECT * FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[qryMMSStoreInfo_GetUniqueID]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1"
                            If MMSGeneric_ListAll(sSql, "Objects", ds, sError) Then
                                If ds.Tables("Objects").Rows.Count <= 0 Then
                                    sSql = "CREATE PROCEDURE [dbo].[qryMMSStoreInfo_GetUniqueID] AS " + vbCrLf
                                Else
                                    sSql = "ALTER PROCEDURE [dbo].[qryMMSStoreInfo_GetUniqueID] AS " + vbCrLf
                                End If
                            Else
                                sSql = "CREATE PROCEDURE [dbo].[qryMMSStoreInfo_GetUniqueID] AS " + vbCrLf
                            End If
                            sSql &= "BEGIN TRANSACTION " & vbCrLf
                            sSql &= "    Update tbStoreInfo Set UniqueIDCount = UniqueIDCount + 1 " & vbCrLf
                            sSql &= "    Select UniqueIDCount from tbStoreInfo " & vbCrLf
                            sSql &= "COMMIT TRANSACTION "

                            If Not MMSGeneric_Execute(sSql, sError) Then
                                Result = False
                            End If

                        Case "ICG"
                            ' create ICG tables
                            If Not ICG.ICGProcess.CreateTables() Then
                                Result = False
                            End If

                            ' create ICG stored procs
                            If Not ICG.ICGProcess.CreateStoredProcedures() Then
                                Result = False
                            End If
                    End Select

                    SetupTablesAndStoredProcedures = Result
            End Select

        Catch ex As Exception
            If LiveLinkConfiguration.DebugLogging Then
                MMS.Utilities.MxLogger.LogErrorToTrace("LiveLink - SetupTablesAndStoredProcedures() - Exception - : {0}", ex.StackTrace)
            End If
            SetupTablesAndStoredProcedures = False ' Unknown error
        End Try

    End Function

    Private Function ProcessSetupResourceFile(ByVal resource As String, ByVal resourceName As String) As Boolean
        Dim batchSeparators() As String = {vbCrLf & "GO" & vbCrLf, vbCrLf & "go" & vbCrLf, vbCrLf & "Go" & vbCrLf}
        Dim errorMsg As String = String.Empty
        Dim errorFound As Boolean = False
        Dim sql() As String = resource.Split(batchSeparators, StringSplitOptions.RemoveEmptyEntries)

        For Each command As String In sql
            If command.Trim <> String.Empty Then
                If Not MMSGeneric_Execute(command, errorMsg) Then
                    errorFound = True
                    LogEvent(String.Format("{0} setup error: {1}{2}{3}Sql: {4}", resourceName, errorMsg, vbCrLf, vbCrLf, command), EventLogEntryType.Error)
                End If
            End If
        Next
        Return Not errorFound
    End Function




End Module



