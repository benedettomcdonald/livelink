Option Strict On
Option Explicit On

Imports Mx.Services
Imports Mx.BusinessObjects
Imports MMS.Utilities
Imports Mx.POS.Common

Public Class OfflineCashup
    Public Shared Function ProcessOfflineCashupFromServer(ByVal offlineCashupInfo As OfflineCashManagementServerToClient) As Boolean
        Return OfflineServiceClient.ProcessOfflineData(GetEntityId(), offlineCashupInfo)
    End Function
    Public Shared Function ProcessOfflineSettingsFromServer(ByVal offlineCashupInfo As OfflineCashManagementServerToClient) As Boolean
        Return OfflineServiceClient.ProcessOfflineSettings(GetEntityId(), offlineCashupInfo)
    End Function
    Public Shared Function AreAnyOfflineSyncsReady(ByVal entityId As Long) As Boolean
        Return OfflineServiceShared.GetNumberOfOfflineSyncsReady(entityId) > 0
    End Function
	Public Shared Function RetrieveOfflineCashupInfoToSend(ByRef searchFromSyncId As Long) As OfflineCashManagementClientToServer
		' Retrieve the data from the database and create a new business object
		Return OfflineServiceClient.GetOfflineCashManagementDataForUploading(GetEntityId(), searchFromSyncId)
	End Function
    Public Shared Sub MarkOfflineCashupInfoAsSent(ByVal offlineCashupInfo As OfflineCashManagementClientToServer)
        ' Now mark the records in the offline cashup tables as sent 
        OfflineServiceShared.SetSyncRecordComplete(offlineCashupInfo.OfflineSync)
	End Sub

	Public Shared Function GetBatchToSend() As OfflineCashManagementClientToServerList

		Dim batchSize As Integer = Math.Min(LiveLinkConfiguration.OfflineCashupBatchSize, OfflineServiceShared.GetNumberOfOfflineSyncsReady(GetEntityId()))

		Dim result As New Mx.BusinessObjects.OfflineCashManagementClientToServerList()
		Dim lastSyncId As Long = -1

		For i As Integer = 1 To batchSize
			Dim transaction As OfflineCashManagementClientToServer = OfflineCashup.RetrieveOfflineCashupInfoToSend(lastSyncId)
			result.Add(transaction)
		Next

		Return result

	End Function

	Public Shared Sub MarkBatchAsSent(ByVal batch As OfflineCashManagementClientToServerList)

		For Each offlineCashupInfo As OfflineCashManagementClientToServer In batch
			MarkOfflineCashupInfoAsSent(offlineCashupInfo)
		Next

	End Sub

End Class
