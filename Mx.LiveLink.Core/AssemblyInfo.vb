Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("Mx.LiveLink.Core")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyProduct("Mx.LiveLink.Core")> 
<Assembly: AssemblyTrademark("")> 
<Assembly: CLSCompliant(True)> 

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("F69FDFA4-6968-4DBE-AF37-02FA29E5DB2C")> 


' 6.10.x (1-Oct-2006) - Added file download and restart capabilities 
' 6.9.x (29-Sep-2006) - Added proxy handling to download file
' 6.9.x (06-Sep-2006) - Added COMPRIS POS
' 6.8.x (22-Aug-2006) - BUG FIX - Changed MICROS T/A query to reference 'obj_num' instead of 'id', needed to reference the table with obj_num. E.obj_num
' 6.8.x (9-Aug-2006) - Changed database creation default size to 2MB (required by Sql Server Express 2005)
' 6.7.x (14-Jul-2006) - Changed for Signature - uses TransactionVersion now.
' 6.7.x (10-Jul-2006) - Changes for Aloha - Fixed T/A import where no rows are returned when calculating min/max times
' 6.7.x (7-Jul-2006) - Changes for Aloha - Adds product description to PLUCode - for undefined items report
' 6.6.x (30-May-2006) - Changed for PCAmerica - uses TransactionVersion now.
' 6.6.x (26-May-2006) - Changed PixelPoint handling - Sybase 5 date format needs to be yyyyMMdd
' 6.5.x (26-May-2006) - Changed PixelPoint handling - Sybase 5 does not support the sql TOP command
' 6.5.x (24-May-2006) - Changed PixelPoint handling as it now uses Sybase.
' 6.5.x (19-May-2006) - Changed sort order for non synced headers for PARJournal - now uses PollDate, not SalesMain id because TLD Server brings data in newest first.
' 6.5.x (15-May-2006) - Forced LiveLink to use en-AU culture (specifically for Micros Israel), caught situation where logging to a full event log caused problems
' 6.5.x (11-May-2006) - Added PixelPoint POS type
' 6.5.x (9-May-2006) - Added TLD Server option for PARJournal
' 6.4.x (26-Apr-2006) - Added PCAmerica POS type; Changed archive directory structure for PAR
' 6.4.x (11-Apr-2006) - Changes for PAR - Overrings
' 6.3.x (9-Mar-2006) - Changes for Micros - T/A id's can be swipe card serial numbers.  Changed cast from int to long to deal with this.
' 6.3.x (7-Mar-2006) - Added time clock functionality, spawns off a time clock form in a new thread
' 6.3.x (7-Mar-2006) - Changes for Micros - Fixed T/A date times to convert to string so local time is used
' 6.2.x (28-Feb-2006) - Changes for Micros - Imports time and attendance records from POS
' 6.2.x (28-Feb-2006) - Changes for Micros - Imports time and attendance records from POS
' 6.2.x (27-Feb-2006) - Changes for PARJournal - No longer require FILE_START (|998) at start of TLD file.
' 6.2.x (14-Feb-2006) - Added the ability to add/update config settings.
' 6.2.x (4-Feb-2006) - Changes for Aloha.  Now returns guest count as type F-R.
' 6.2.x (3-Feb-2006) - Changes for PARJournal.  No longer crashes if TLD is locked.
' 6.1.x (31-Jan-2006) - Changed the way the unique machine id is calculated.  Also old machine ids update themselves.
' 6.1.x (26-Jan-2006) - New POS - ParInfusion
' 6.1.x (19-Jan-2006) - Changes for Micros - can now handling the "inserting" of a record after the last element of a check array
' 6.1.10  - Added Time and Attendance handling
' 6.1.6   - Added proxy server handling
' 6.1.2   - Changes for Micros - Changed processing to handle S records (skips) and to deal with the index out of bounds errors.
' 5.12.29 - Changes for Micros - can now handle closed checks that have been edited (ie ob_ccs11_chk_edited = 'T').
' 5.12.13 - Changes for Micros - rewrote reopened checks routines due to poor performance on Sybase.  CPU was maxing out.
' 5.12.12 - Changes for Micros - bug fix due to way Sybase handles FLOAT
' 5.12.9  - Changes for Micros - Can now handle reopened checks (Requires web service update), and Cover count was corrected from transferred tables
' 5.11.19 - New POS - Signature
' 5.11.16 - Changes for PARJournal - Added a new financial subtype which contains any unaccounted for variances 
' 5.11.15 - Changes for PARJournal - Handles TLD processing differently 
' 5.11.8  - Changes for Aloha - Ignores tax records when price is 0
'                             - Handles VOIDs differently
'                             - Removes the Index Requirement bit from the TDR.DBF and RSN.DBF files to avoid "Index Not Found" errors
' 5.11.5  - Changes for Quicken - Change RegisterId from overflowed INT64 to unsigned INT64 and then back to signed INT64 to avoid overflow
'         - Changes for Quicken - Can now handle duplicate transaction IDs on the same day
'         - Changes for Quicken - Sort by Date_Time, then SalesMainID because the eRetailerConnection app doesn't put transactions in in the correct order
' 5.11.1  - Changes for Cratos - Mark duplicate TransactionId's as errors; Set PollDate for records with no polldate to that of header record
'         - Changes for all - added " AND (Sycned = 0) " to the record selection.  This ensures that any records flagged as errors to not get sent.
' 5.10.31 - Changes for Quicken to send Serial Number through while still on conditional lease - For GoSushi
'         - All awake message now come through on conditional lease as well (ie before site activation)
'         - Changed Max backoff timer to 4 hours (down from 12 hours)
' 5.10.28 - Fixes for PARJournal
