﻿Namespace Exceptions
    Public Class XmlFileProcessException
        Inherits SystemException
        Public Property FileName() As String
            Get
                Return _fileName
            End Get
            Set(value As String)
                _fileName = value
            End Set
        End Property

        Private _fileName As String

        Public Sub New(message As String, name As String, innerException As Exception)
            MyBase.New(message, innerException)
            FileName = name
        End Sub

        Public Sub New(message As String, name As String)
            MyBase.New(message)
            FileName = name
        End Sub
    End Class
End Namespace