﻿

#Region "Summary"

'-------------------------------------------------------------------------------------------
' AUTHOR        : $Author: Ricky $
' SPECIAL NOTES : 
' FILE NAME     : $Workfile: Globals.vb $
' ARCHIVE       : $Archive: /Source/Macromatix.root/Macromatix/LiveLink/Globals.vb $
' REVISION      : $Revision: 2 $
'
' Copyright (c) 2006 MacromatiX 
' 
' $History: Globals.vb $
'  
'  *****************  Version 2  *****************
'  User: Ricky        Date: 22/04/08   Time: 1:19p
'  Updated in $/Source/Macromatix.root/Macromatix/LiveLink
'  Issue: Livelink - save tax
'  Description: Save tax value to tbSalesMain. Added date culture for date
'  parsing
'  Reviewed by: Saul
'  Type:  Change
'  Release Notes: Internal
'  Support Ticket No: N/A
'  Status: Complete
'  
'  *****************  Version 1  *****************
'  User: Colin        Date: 18.04.08   Time: 16:22
'  Created in $/Source/Macromatix.root/Macromatix/LiveLink
'  Issue: Livelink not handling local culture for numbers
'  Description: Added setting to use local culture when parsing number
'  (intouch only)
'  Reviewed by: Saul
'  Type:  Bug Fix
'  Release Notes: Internal 
'  Support Ticket No: N/A
'  Status: Complete
'  
'-------------------------------------------------------------------------------------------

#End Region

Option Strict On
Imports System.Globalization

Public Module Globals

    Private _numberCulture As CultureInfo
    Private _dateCulture As CultureInfo

    Public Property NumberCulture() As CultureInfo
        Get
            Return _numberCulture
        End Get
        Set(ByVal value As CultureInfo)
            _numberCulture = value
        End Set
    End Property

    Public Property DateCulture() As CultureInfo
        Get
            Return _dateCulture
        End Get
        Set(ByVal value As CultureInfo)
            _dateCulture = value
        End Set
    End Property

End Module
