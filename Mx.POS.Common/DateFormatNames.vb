﻿Public Class DateFormatNames
    Public Const DisplayDateFormat As String = "dd-MMM-yyyy"
    Public Const DisplayDateTimeFormat As String = "dd-MMM-yyyy hh:mm tt"
    Public Const DisplayDateTimeFormat24Hour As String = "dd-MMM-yyyy HH:mm"
    Public Const DisplayTimeFormat24Hour As String = "HH:mm"
    Public Const DatabaseDateFormat As String = "dd-MMM-yyyy"
    Public Const DatabaseDateTimeFormat As String = "dd-MMM-yyyy HH:mm"
    Public Const DatabaseDateTimeFormatWithSecond As String = "dd-MMM-yyyy HH:mm:ss"
    Public Const DatabaseDateTimeFormatStandard As String = "yyyyMMdd HH:mm:ss"

    Public Const DisplayShortDateFormat_DMY As String = "dd/MM/yy"
    Public Const FileNameTimeStamp As String = "yyyyMMdd_HHmmss"

    Public Const DatabaseNeutralDateFormat As String = "yyyy-MM-dd"
    Public Const DatabaseNeutralDateTimeFormat As String = "yyyy-MM-dd HH:mm"
    Public Const DatabaseNeutralDateFormatWithSecond As String = "yyyy-MM-dd HH:mm:ss"
    Public Const DatabaseNeutralDateFormatWithMilliSecond = "yyyy-MM-dd HH:mm:ss.fff"
    Public Const DateTimeStamp As String = "yyyyMMddHHmmss"
    Public Const DateTimeStampWithoutSeconds As String = "yyyyMMddHHmm"

End Class
