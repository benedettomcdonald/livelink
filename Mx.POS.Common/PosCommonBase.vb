﻿Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.ComponentModel
Imports Mx.BusinessObjects.LiveLink
Imports Mx.POS.Common.BusinessObjects
Imports Mx.POS.Common.Interfaces

Public MustInherit Class PosCommonBase
    Implements IPosCommon

#Region "Common Base Functions"

    Protected Sub RaiseStatusChanged(eventType As String, eventMsg As String)
        Dim status = New KeyValuePair(Of String, String)(eventType, eventMsg)
        RaiseStatusChanged(New Progress(status))
    End Sub

    Protected Sub RaiseStatusChanged(e As Progress)
        RaiseEvent StatusChangedEvent(Me, e)
    End Sub

    Protected _serviceMappings As Dictionary(Of String, SalesServiceType)

    ''' <summary>
    ''' Save the config string to a mapping dictionary
    ''' </summary>
    ''' <param name="serviceTypeMappings">
    ''' From livelink configuration. Ex :
    ''' CarryOut=TAKE AWAY;Delivery=HOME SERVICE;EatIn=EAT IN;DriveThru=Drive Thru;Custom1=SOFT SERVE
    ''' </param>
    Protected Sub PrepareServiceTypeMappings(ByVal serviceTypeMappings As String)
        If _serviceMappings IsNot Nothing Then Return

        _serviceMappings = New Dictionary(Of String, SalesServiceType)()

        Dim serviceMappings() As String = serviceTypeMappings.Split(";"c)
        For Each mapping As String In serviceMappings
            Dim serviceMapping() As String = mapping.Split("="c)
            If serviceMapping.Length = 2 Then
                Try
                    _serviceMappings(serviceMapping(1).ToUpper) = CType([Enum].Parse(GetType(SalesServiceType), serviceMapping(0), True), SalesServiceType)
                Catch ex As ArgumentException
                    LogEvent(EventLogEntryType.Warning, "Service mappings in configuration file are invalid.{0} is not a valid key, (value is {1}), Error: {2}", serviceMapping(0), serviceMapping(1), ex)
                End Try
            End If
        Next
    End Sub

    Protected Overridable Function GetServiceType(ByVal value As String) As SalesServiceType
        If _serviceMappings.ContainsKey(value.ToUpper) Then
            Return _serviceMappings(value.ToUpper)
        Else
            Return SalesServiceType.AllNotUsed
        End If
    End Function

#End Region

#Region "Implement functions"
    Public Event StatusChangedEvent As EventHandler(Of Progress) Implements IPosCommon.StatusChangedEvent
    Public MustOverride ReadOnly Property PosName() As String Implements IPosCommon.PosName
    Public MustOverride ReadOnly Property GatewayEnabled() As Boolean Implements IPosCommon.GatewayEnabled
    Public MustOverride ReadOnly Property PollInterval() As Integer Implements IPosCommon.PollInterval
    Public Property LastPollDate() As Date Implements IPosCommon.LastPollDate
    Public MustOverride Sub SetupDatabase() Implements IPosCommon.SetupDatabase
    Public MustOverride Sub ProcessData(entityId As Integer) Implements IPosCommon.ProcessData
    Public MustOverride Sub PurgeOldData(ByVal daysBeforeObsolete As Integer) Implements IPosCommon.PurgeOldData
#End Region

End Class


