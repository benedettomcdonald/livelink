Public Class LiveLinkWebService
    Inherits eRetailer.eRetailer

    Protected Overrides Function GetWebRequest(ByVal uri As Uri) As System.Net.WebRequest
        Dim webRequest As System.Net.HttpWebRequest
        webRequest = CType(MyBase.GetWebRequest(uri), System.Net.HttpWebRequest)
        webRequest.KeepAlive = LiveLinkConfiguration.WebRequestKeepAlive
        GetWebRequest = webRequest
    End Function
End Class
