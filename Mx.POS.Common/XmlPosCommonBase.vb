﻿Imports System.IO
Imports MMS.Utilities.MMSUtils
Imports Mx.POS.Common.Interfaces
Imports Mx.BusinessObjects.LiveLink
Imports Mx.POS.Common.Exceptions
Imports Mx.Services.LiveLink
Imports MMS.Utilities
Imports System.Threading

Public MustInherit Class XmlPosCommonBase
    Inherits PosCommonBase

    Public MustOverride ReadOnly Property XmlNoOfFilesPerProcess() As Integer
    Public MustOverride ReadOnly Property XmlFilePath() As String
    Public MustOverride ReadOnly Property XmlFileMask() As String
    Public MustOverride ReadOnly Property CompressArchive() As Boolean
    Public MustOverride ReadOnly Property MillisecondsDelayProcessBetweenFiles() As Integer

    Public Overrides Sub SetupDatabase()
        'Do nothing, XML POS don't have specific database
    End Sub

    Public MustOverride Function ProcessXmlSpecific(ByVal entityId As Integer, ByVal fileName As String) As Integer

    Public Overrides Sub ProcessData(ByVal entityId As Integer)
        RaiseStatusChanged("", String.Format("Importing {0} data process begins", PosName))

        Dim xmlFileList As FileInfo() = XMLFilesService.GetXmlFileList(XmlFilePath, XmlFileMask)
        Dim totalChecksProcessed As Integer = 0
        If xmlFileList Is Nothing OrElse xmlFileList.Length <= 0 Then
            Return
        End If

        ' mumberOfFilePerLoad is added to limit the data load.
        Dim mumberOfFilePerLoad As Integer = xmlFileList.Length
        If mumberOfFilePerLoad > XmlNoOfFilesPerProcess Then
            mumberOfFilePerLoad = XmlNoOfFilesPerProcess
        End If

        For fileIndex As Integer = 0 To mumberOfFilePerLoad - 1
            Dim status As Boolean = True
            Try
                totalChecksProcessed += ProcessXmlSpecific(entityId, xmlFileList(fileIndex).FullName)

            Catch fileEx As XmlFileProcessException
                LogError(LogCategory.MxClient, "Exception occurred processing xml file. Exception: {0}", fileEx.ToString())
                SalesMainService.InsertAlertMessage(entityId, AlertRecordSubType.[Error], "File Process Error", String.Format("File: {0}. Exception: {1}", xmlFileList(fileIndex).Name, fileEx))
                status = False
            Catch ex As Exception
                LogError(LogCategory.MxClient, "Exception occurred processing xml data. Exception: {0}", ex.ToString())
                SalesMainService.InsertAlertMessage(entityId, AlertRecordSubType.[Error], "Process Error", String.Format("File: {0}. Exception: {1}", xmlFileList(fileIndex).Name, ex))
                status = False
            Finally
                Dim archiveFileName As String = ManageFiles.CreateArchiveFileName(xmlFileList(fileIndex).Name, ManageFiles.ArchiveFileNameStyle.orig_A_yyyyMMdd_HHmmss)
                If status Then
                    XMLFilesService.ArchiveXmlFile(xmlFileList(fileIndex), Path.Combine(XmlFilePath, "Archive"), CompressArchive, archiveFileName)
                Else
                    XMLFilesService.ArchiveXmlFile(xmlFileList(fileIndex), Path.Combine(XmlFilePath, "Error"), False, archiveFileName)
                End If
            End Try
            ' sleep between each file
            Thread.Sleep(MillisecondsDelayProcessBetweenFiles)
        Next

        RaiseStatusChanged("", String.Format("Importing {0} data completed : [{1} check(s) processed]", PosName, totalChecksProcessed))

    End Sub

    Public Overrides Sub PurgeOldData(ByVal daysBeforeObsolete As Integer)
        XMLFilesService.DeleteArchivedXmlFiles(Path.Combine(XmlFilePath, "Archive"), daysBeforeObsolete)
        XMLFilesService.DeleteArchivedXmlFiles(Path.Combine(XmlFilePath, "Error"), daysBeforeObsolete)
    End Sub

End Class
