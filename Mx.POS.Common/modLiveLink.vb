Imports System.IO
Imports MXOleDBServer
Imports MXODBCServer
Imports MXServer
Imports System.Management
Imports Microsoft.Win32
Imports MMS.Generic
Imports MMS.Utilities
Imports Mx.Services.BaseSystem.Polling
Imports System.Windows.Forms

Public Module modLiveLink
    Public CancelFlag As Boolean = False
    Public Const cProTouchIniFile = "C:\touchtec\bin\TMacrox.ini"
    Private Const SubKey = "SOFTWARE\MacromatiX\LiveLink\"
    Private Const MachineId = "MachineId"
    Private Const PendingMachineId = "PendingMachineId"

    'Data access functions 
    '---------------------
    'First group for use with ODBC
    'Second group with SQL Server Ole DB
    'Const LiveLinkConfiguration.ConnectionString As String = "server=(local);Integrated Security=SSPI;Database=dbName"
    'ODBC syntax - "DSN=dsnname;uid=;pwd=;dsq="

#Region "    Connection Strings   "

    ' Note - the shared db connection functionality has only been included for certain native and OleDB connections so far
    Private SharedSQLConection As SqlClient.SqlConnection
    Private SharedOleDBConection As OleDb.OleDbConnection

    Public Enum ConnectionType
        All
        NativeSQLOnly
        OleDBOnly
        ODBCOnly
    End Enum

    Public Function CloseSharedConnection(Optional ByVal dbConnectionType As ConnectionType = ConnectionType.All)
        ' Call this function to close the Shared SQL connection
        Select Case dbConnectionType
            Case ConnectionType.All

                Try ' Close the Native SQL connection
                    If Not (SharedSQLConection Is Nothing) Then
                        SharedSQLConection.Dispose() ' Dispose closes the connection first 
                        SharedSQLConection = Nothing
                    End If
                Catch ex As Exception
                    LogEvent("Close Shared Connection error (Native): " & ex.Message, EventLogEntryType.Warning)
                End Try

                Try ' Close the OleDB connection
                    If Not (SharedOleDBConection Is Nothing) Then
                        SharedOleDBConection.Dispose() ' Dispose closes the connection first 
                        SharedOleDBConection = Nothing
                    End If
                Catch ex As Exception
                    LogEvent("Close Shared Connection error (OleDB): " & ex.Message, EventLogEntryType.Warning)
                End Try

            Case ConnectionType.NativeSQLOnly
                Try ' Close the Native SQL connection
                    If Not (SharedSQLConection Is Nothing) Then
                        SharedSQLConection.Dispose() ' Dispose closes the connection first 
                        SharedSQLConection = Nothing
                    End If
                Catch ex As Exception
                    LogEvent("Close Shared Connection error (Native): " & ex.Message, EventLogEntryType.Warning)
                End Try
            Case ConnectionType.OleDBOnly
                Try ' Close the Native SQL connection
                    If Not (SharedOleDBConection Is Nothing) Then
                        SharedOleDBConection.Dispose() ' Dispose closes the connection first 
                        SharedOleDBConection = Nothing
                    End If
                Catch ex As Exception
                    LogEvent("Close Shared Connection error (OleDB): " & ex.Message, EventLogEntryType.Warning)
                End Try
            Case ConnectionType.ODBCOnly
                ' not yet implemented
        End Select
    End Function
#End Region

#Region "    First Group - ODBC only     "

    Public Function MMSLocalGeneric_ListAll(ByVal sConnect As String, ByVal sQry As String, ByVal sTableName As String, ByRef ds As DataSet, ByRef sError As String) As Boolean
        Dim myServer As New MXODBCServer.MXODBCServer
        Return myServer.svrMXODBC_ListAll(sConnect, sQry, sTableName, ds, sError)
    End Function
    Public Function MMSLocalGeneric_ListAll_MSA(ByVal sConnect As String, ByVal sQry As String, ByVal sTableName As String, ByRef ds As DataSet, ByRef sError As String, ByVal bNoMSA As Boolean) As Boolean
        'To accomodate no missing schema action - add with key is default action
        Dim myServer As New MXODBCServer.MXODBCServer
        If bNoMSA Then
            Return myServer.svrMXODBC_ListAll_NoMSA(sConnect, sQry, sTableName, ds, sError)
        Else
            Return myServer.svrMXODBC_ListAll(sConnect, sQry, sTableName, ds, sError)
        End If
    End Function
    Public Function MMSLocalGeneric_ListAllWithMaxRecords(ByVal sConnect As String, ByVal sQry As String, ByVal sTableName As String, ByVal iStartRecord As Integer, ByVal iMaxRecords As Integer, ByRef ds As DataSet, ByRef sError As String) As Boolean
        Dim myServer As New MXODBCServer.MXODBCServer
        Return myServer.svrMXODBC_ListAllWithMaxRecords(sConnect, sQry, sTableName, iStartRecord, iMaxRecords, ds, sError)
    End Function
    Public Function MMSLocalGeneric_Execute(ByVal sConnect As String, ByVal sQry As String, ByRef sError As String) As Boolean
        Dim myServer As New MXODBCServer.MXODBCServer
        Return myServer.svrMXODBC_Execute(sConnect, sQry, sError)
    End Function
    Public Function MMSLocalGeneric_Update(ByVal sConnect As String, ByVal sQry As String, ByVal sTableName As String, ByVal ds As DataSet, ByRef sError As String) As Boolean
        Dim myServer As New MXODBCServer.MXODBCServer
        Return myServer.svrMXODBC_Update(sConnect, sQry, sTableName, ds, sError)
    End Function
    Public Function MMSLocalGeneric_Insert(ByVal sConnect As String, ByVal sQry As String, ByRef sID As String, ByRef sError As String) As Boolean
        Dim myServer As New MXODBCServer.MXODBCServer
        Return myServer.svrMXODBC_Insert(sConnect, sQry, sID, sError)
    End Function

    Public Function SelectLocalDataSetMulti(ByVal sConnect As String, ByVal saSelect() As String, ByVal saTableName() As String, ByVal iRowCount As Integer, ByRef ds As DataSet, ByRef sError As String) As Boolean
        Dim myServer As New MXODBCServer.MXODBCServer
        Return myServer.svrMXODBC_SelectDataSetMulti(sConnect, saSelect, saTableName, iRowCount, ds, sError)
    End Function

#End Region

#Region "    Second Group - Native SQL only     "
    'Second group with native SQL Server 

    Public Function MMSGeneric_ListAll(ByVal sQry As String, ByVal sTableName As String, ByRef ds As DataSet, ByRef sError As String) As Boolean
        Dim myServer As New MXServer.MXServer
        If LiveLinkConfiguration.Share_DB_Connection Then
            Return myServer.svrMX_ListAll(LiveLinkConfiguration.ConnectionString, sQry, sTableName, ds, sError, SharedSQLConection)
        Else
            Return myServer.svrMX_ListAll(LiveLinkConfiguration.ConnectionString, sQry, sTableName, ds, sError)
        End If
    End Function

    Public Function MMSGeneric_Execute(ByVal sQry As String, ByRef sError As String) As Boolean
        Dim myServer As New MXServer.MXServer
        If LiveLinkConfiguration.Share_DB_Connection Then
            Return myServer.svrMX_Execute(LiveLinkConfiguration.ConnectionString, sQry, sError, SharedSQLConection)
        Else
            Return myServer.svrMX_Execute(LiveLinkConfiguration.ConnectionString, sQry, sError)
        End If
    End Function

    Public Function MMSGeneric_Update(ByVal sQry As String, ByVal sTableName As String, ByVal ds As DataSet, ByRef sError As String) As Boolean
        Dim myServer As New MXServer.MXServer
        Return myServer.svrMX_Update(LiveLinkConfiguration.ConnectionString, sQry, sTableName, ds, sError)
    End Function

    Public Function MMSGeneric_Insert(ByVal sQry As String, ByRef sID As String, ByRef sError As String) As Boolean
        Dim myServer As New MXServer.MXServer
        Return myServer.svrMX_Insert(LiveLinkConfiguration.ConnectionString, sQry, sID, sError)
    End Function

    Public Function SelectDataSetMulti(ByVal saSelect() As String, ByVal saTableName() As String, ByVal iRowCount As Integer, ByRef ds As DataSet, ByRef sError As String) As Boolean
        Dim myServer As New MXServer.MXServer
        Return myServer.svrMX_SelectDataSetMulti(LiveLinkConfiguration.ConnectionString, saSelect, saTableName, iRowCount, ds, sError)
    End Function


#End Region

#Region "    Third Group - Combined ODBC and Native SQL     "
    ' 3rd group - Single function that can be used for both Native SQL and ODBC connections

    Public Function MMSGeneric_ListAll(ByVal UseODBC As Boolean, ByVal sQry As String, ByVal sTableName As String, ByRef ds As DataSet, ByRef sError As String, ByVal bNoMSA As Boolean) As Boolean
        ' Overloaded function to handle both ODBC and Native SQL, and use a shared database connection (Native only)
        If UseODBC Then
            Dim myServer As New MXODBCServer.MXODBCServer
            If bNoMSA Then
                Return myServer.svrMXODBC_ListAll_NoMSA(LiveLinkConfiguration.ODBC_DB_Connect, sQry, sTableName, ds, sError)
            Else
                Return myServer.svrMXODBC_ListAll(LiveLinkConfiguration.ODBC_DB_Connect, sQry, sTableName, ds, sError)
            End If
        Else
            Dim myServer As New MXServer.MXServer
            If LiveLinkConfiguration.Share_DB_Connection Then
                Return myServer.svrMX_ListAll(LiveLinkConfiguration.ConnectionString, sQry, sTableName, ds, sError, SharedSQLConection)
            Else
                Return myServer.svrMX_ListAll(LiveLinkConfiguration.ConnectionString, sQry, sTableName, ds, sError)
            End If
        End If
    End Function

    Public Function MMSGeneric_Execute(ByVal UseODBC As Boolean, ByVal sQry As String, ByRef sError As String) As Boolean
        ' Overloaded function to handle both ODBC and Native SQL, plus use a shared database connection (Native Only)
        If UseODBC Then
            Dim myServer As New MXODBCServer.MXODBCServer
            Return myServer.svrMXODBC_Execute(LiveLinkConfiguration.ODBC_DB_Connect, sQry, sError)
        Else
            Dim myServer As New MXServer.MXServer
            If LiveLinkConfiguration.Share_DB_Connection Then
                Return myServer.svrMX_Execute(LiveLinkConfiguration.ConnectionString, sQry, sError, SharedSQLConection)
            Else
                Return myServer.svrMX_Execute(LiveLinkConfiguration.ConnectionString, sQry, sError)
            End If
        End If
    End Function

#End Region

#Region "    4th  Group - OleDB Connections Only     "
    ' 4th group - OleDB Connections Only

    Public Function MMSGeneric_OleDB_ListAll(ByVal sQry As String, ByVal sTableName As String, ByRef ds As DataSet, ByRef sError As String) As Boolean
        Dim myServer As New MXOleDBServer.MXOleDBServer
        If LiveLinkConfiguration.Share_DB_Connection Then
            Return myServer.svrMXOleDB_ListAll(LiveLinkConfiguration.OleDB_DB_Connect, sQry, sTableName, ds, sError, SharedOleDBConection)
        Else
            Return myServer.svrMXOleDB_ListAll(LiveLinkConfiguration.OleDB_DB_Connect, sQry, sTableName, ds, sError)
        End If
    End Function

    Public Function MMSGeneric_OleDB_ListAll(ByVal sQry As String, ByVal sTableName As String, ByRef ds As DataSet, ByRef sError As String, ByVal bNoMSA As Boolean) As Boolean
        Dim myServer As New MXOleDBServer.MXOleDBServer
        If bNoMSA Then
            Return myServer.svrMXOleDB_ListAll_NoMSA(LiveLinkConfiguration.OleDB_DB_Connect, sQry, sTableName, ds, sError)
        Else
            If LiveLinkConfiguration.Share_DB_Connection Then
                Return myServer.svrMXOleDB_ListAll(LiveLinkConfiguration.OleDB_DB_Connect, sQry, sTableName, ds, sError, SharedOleDBConection)
            Else
                Return myServer.svrMXOleDB_ListAll(LiveLinkConfiguration.OleDB_DB_Connect, sQry, sTableName, ds, sError)
            End If
        End If
    End Function

    Public Function MMSGeneric_OleDB_ListAll(ByVal sConnect As String, ByVal sQry As String, ByVal sTableName As String, ByRef ds As DataSet, ByRef sError As String) As Boolean
        ' Overloaded - allows a connection string and connection object to be passed in
        Dim myServer As New MXOleDBServer.MXOleDBServer
        If LiveLinkConfiguration.Share_DB_Connection Then
            Return myServer.svrMXOleDB_ListAll(sConnect, sQry, sTableName, ds, sError, SharedOleDBConection)
        Else
            Return myServer.svrMXOleDB_ListAll(sConnect, sQry, sTableName, ds, sError)
        End If
    End Function

    Public Function MMSGeneric_OleDB_ListAll(ByVal sConnect As String, ByVal sQry As String, ByVal sTableName As String, ByRef ds As DataSet, ByRef sError As String, ByVal bNoMSA As Boolean) As Boolean
        ' Overloaded - allows a connection string to be passed in
        Dim myServer As New MXOleDBServer.MXOleDBServer
        If bNoMSA Then
            Return myServer.svrMXOleDB_ListAll_NoMSA(sConnect, sQry, sTableName, ds, sError)
        Else
            If LiveLinkConfiguration.Share_DB_Connection Then
                Return myServer.svrMXOleDB_ListAll(sConnect, sQry, sTableName, ds, sError, SharedOleDBConection)
            Else
                Return myServer.svrMXOleDB_ListAll(sConnect, sQry, sTableName, ds, sError)
            End If
        End If
    End Function

    Public Function MMSGeneric_OleDB_Execute(ByVal sQry As String, ByRef sError As String) As Boolean
        Dim myServer As New MXOleDBServer.MXOleDBServer
        If LiveLinkConfiguration.Share_DB_Connection Then
            Return myServer.svrMXOleDB_Execute(LiveLinkConfiguration.OleDB_DB_Connect, sQry, sError, SharedOleDBConection)
        Else
            Return myServer.svrMXOleDB_Execute(LiveLinkConfiguration.OleDB_DB_Connect, sQry, sError)
        End If
    End Function

    Public Function MMSGeneric_OleDB_Execute(ByVal sConnect As String, ByVal sQry As String, ByRef sError As String) As Boolean
        ' Overloaded - allows a connection string to be passed in
        Dim myServer As New MXOleDBServer.MXOleDBServer
        If LiveLinkConfiguration.Share_DB_Connection Then
            Return myServer.svrMXOleDB_Execute(sConnect, sQry, sError, SharedOleDBConection)
        Else
            Return myServer.svrMXOleDB_Execute(sConnect, sQry, sError)
        End If
    End Function

#End Region

    Public Function ValCLng(ByVal sNumberText As String) As Integer
        Dim nNumberText As Integer
        Try
            nNumberText = CLng(sNumberText)
        Catch
            nNumberText = Val(sNumberText)
        End Try
        Return nNumberText
    End Function

    Public Function EvalNull(ByVal dbField As Object) As String
        If dbField Is DBNull.Value Then
            Return ""
        Else
            Return dbField
        End If
    End Function

    Public Function EvalNull(ByVal dbField As Object, ByVal sDefault As String) As String
        If dbField Is DBNull.Value Then
            Return sDefault
        Else
            Return dbField
        End If
    End Function

    ' use the new LiveLinkConfiguration class to get the web proxy settings
    Public Function ConfigProxy() As Net.IWebProxy
        Dim proxy As Net.IWebProxy = Nothing
        Try
            proxy = HttpDownload.Proxy(LiveLinkConfiguration.WebService_Proxy, LiveLinkConfiguration.WebService_Proxy_Auth, _
               LiveLinkConfiguration.WebService_Proxy_User, LiveLinkConfiguration.WebService_Proxy_Password, _
               LiveLinkConfiguration.WebService_Proxy_Domain, LiveLinkConfiguration.WebService_URL)
        Catch ex As Exception
            proxy = Nothing
        End Try
        Return proxy
    End Function

    Public Function GetEntityId() As Integer
        ' This function returns the EntityId that is used to populate tbSalesMain
        ' It differs for each POS type

        Dim sStoreID As String = If(LiveLinkConfiguration.StoreID_KeyExists(), LiveLinkConfiguration.StoreID.ToString(), "")
        Dim Use_ODBC As Boolean = LiveLinkConfiguration.Use_ODBC_DB_Connect
        Dim sConnect_ODBC As String = LiveLinkConfiguration.ODBC_DB_Connect
        Dim sPOSType As String = LiveLinkConfiguration.POSType
        Dim sSql As String = String.Empty
        Dim WSDataSet As DataSet
        Dim sError As String = String.Empty

        GetEntityId = -1
        Try
            Select Case sPOSType.ToUpper.Trim
                Case "PARJOURNAL", "PAN7700", "CRATOS", "ALOHA", "SIGNATURE", "PARINFUSION", "COMPRIS", "INTOUCH", "PAREXALT4", "OCTANE", "ICG", "MICROSOFTRMS", "NEWPOS", "IPOS", "UNIWELL"
                    ' Entity Id is retrieved from the tbStoreInfo in the MacromatiX database
                    sSql = "Select Top 1 IsNull(StoreId, -1) As EntityId From tbStoreInfo "
                Case "PIXELPOINT"
                    ' Entity Id is retrieved from the tbStoreInfo in the MacromatiX database
                    sSql = "Select First IsNull(StoreId, -1) As EntityId From tbStoreInfo"
                Case "MICROS"
                    ' Entity Id is retrieved from the tbStoreInfo table in the Micros Sybase database
                    sSql = "SELECT Top 1 IsNull(StoreId, -1) AS EntityId From custom.tbStoreInfo"
                Case "QUICKEN"
                    ' Entity Id is not set anywhere on the POS.  The unique register ID is passed through to
                    ' the web service.  Retrive the Entity Id from mxlivelink.config as a double check only.
                    If IsNumeric(sStoreID) Then
                        GetEntityId = CInt(sStoreID)
                    Else
                        GetEntityId = -1
                    End If
                Case "PROTOUCH"
                    ' Read the EntityId from the Protouch ini file
                    Dim ProTouchIniFile As String = LiveLinkConfiguration.ProTouchIniFile
                    If ProTouchIniFile = "" Then
                        ProTouchIniFile = cProTouchIniFile
                    End If
                    Dim objIniFile As New IniFile(ProTouchIniFile)
                    GetEntityId = objIniFile.GetInteger("MacroMatix", "EntityId", "-1")
                Case Else
                    If Use_ODBC = "1" Then
                        ' default to getting the Entity Id from mxlivelink.config
                        If IsNumeric(sStoreID) Then
                            GetEntityId = CInt(sStoreID)
                        Else
                            GetEntityId = -1
                        End If
                    Else
                        ' default to getting the Entity Id from tbStoreInfo
                        sSql = "Select Top 1 IsNull(StoreId, -1) As EntityId From tbStoreInfo"
                    End If
            End Select

            If sSql <> String.Empty Then
                If Use_ODBC Then
                    If MMSLocalGeneric_ListAll(sConnect_ODBC, sSql, "tbStoreInfo", WSDataSet, sError) Then
                        If WSDataSet.Tables("tbStoreinfo").Rows.Count = 1 Then
                            GetEntityId = WSDataSet.Tables("tbStoreInfo").Rows(0).Item("EntityId")
                        End If
                    Else
                        LogEvent("Unable to retrieve store number" & vbCrLf & "Error: " & sError & vbCrLf & "SQL: " & sSql, EventLogEntryType.Error)
                    End If
                Else
                    If MMSGeneric_ListAll(sSql, "tbStoreInfo", WSDataSet, sError) Then
                        If WSDataSet.Tables("tbStoreinfo").Rows.Count = 1 Then
                            GetEntityId = WSDataSet.Tables("tbStoreInfo").Rows(0).Item("EntityId")
                        End If
                    Else
                        LogEvent("Unable to retrieve store number" & vbCrLf & "Error: " & sError & vbCrLf & "SQL: " & sSql, EventLogEntryType.Error)
                    End If
                End If
            End If

        Catch ex As Exception
            LogEvent(EventLogEntryType.Error, "Exception occurred retrieving EntityID from store setup. Exception: {0}", ex.ToString)
            GetEntityId = -1
        End Try
    End Function

    Public Function RegRead(ByVal Hive As RegistryKey, ByVal SubKey As String, ByVal Name As String, Optional ByVal DefaultValue As String = "") As String
        Try
            Dim rk As RegistryKey = Hive.OpenSubKey(SubKey, False)
            If rk Is Nothing Then
                Return DefaultValue
            Else
                Return rk.GetValue(Name, DefaultValue)
            End If
        Catch ex As Exception
            LogEvent(EventLogEntryType.Error, "Exception: Occurred reading registry key. KEY:[{0}] NAME:[{1}].{2}{2}Exception: {3}", SubKey, Name, vbCrLf, ex.ToString)
            Return DefaultValue
        End Try
    End Function

    Public Function RegWrite(ByVal Hive As RegistryKey, ByVal SubKey As String, ByVal Name As String, ByVal Value As String) As Boolean
        Try
            Dim rk As RegistryKey = Hive.OpenSubKey(SubKey, True)
            If rk Is Nothing Then
                rk = Hive.CreateSubKey(SubKey)
            End If
            rk.SetValue(Name, Value)
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Function RegDelete(ByVal Hive As RegistryKey, ByVal SubKey As String) As Boolean
        Try
            Hive.DeleteSubKey(SubKey)
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Function RegDeleteValue(ByVal Hive As RegistryKey, ByVal SubKey As String, ByVal Value As String) As Boolean
        Try
            Using rk As RegistryKey = Hive.OpenSubKey(SubKey, True)
                rk.DeleteValue(Value, False)
            End Using
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    ' delete the unique machineid from the registry and create a new one
    Public Function RecreateUniqueMachineId(ByRef Identity As String) As String
        If RegDelete(Registry.LocalMachine, SubKey) Then
            Identity = GetUniqueMachineId()
            Return True
        End If
        Return False
    End Function

    Public Function GetUniqueMachineId(Optional ByRef comExceptionThrown As Boolean = False) As String
        Dim SysInfo As New clsSysInfo("!CPU!_!DriveSerial!")
        Dim myGUID As String = String.Empty

        ' check if any COM exceptions were thrown getting information from WMI
        comExceptionThrown = SysInfo.COMExceptionThrown

        ' Due to problems with certain PAR setups, the "soft" MAC addresses created by PAR were identical across machines 
        ' and were causing problems.  This new process creates a GUID and stores it in the registry if one does not already exist.
        Dim UniqueMachineId As String = String.Empty
        Try
            Dim RegName As String = MachineId
            '-------------------------------------------------------------

            myGUID = RegRead(Registry.LocalMachine, SubKey, RegName, String.Empty)
            If myGUID = String.Empty Then
                Try
                    myGUID = Guid.NewGuid.ToString
                    RegWrite(Registry.LocalMachine, SubKey, RegName, myGUID)
                Catch ex As Exception
                End Try

                ' Double check that the registry write succeeded
                If RegRead(Registry.LocalMachine, SubKey, RegName, String.Empty) <> myGUID Then
                    ' We were unable to successfuly store the GUID in the registry
                    myGUID = String.Empty
                End If
            End If

            ' Now return the SHA1 hash of the three concatenated values (ensure all 3 are valid values before generating the machine ID)
            If Not String.IsNullOrEmpty(myGUID) AndAlso _
                Not String.IsNullOrEmpty(SysInfo.CPU) AndAlso _
                Not String.IsNullOrEmpty(SysInfo.DriveSerial) Then
                ' generate a machine ID that is unique to the CPU and driveSerial
                UniqueMachineId = Polling.SHA1(myGUID & "-" & SysInfo.CPU & "-" & SysInfo.DriveSerial)
            Else
                MxLogger.LogInformationToTrace("Failed to get LiveLink ID. GUID:[{0}] CPU:[{1}] DRIVE:[{2}]", myGUID, SysInfo.CPU, SysInfo.DriveSerial)
            End If
        Catch ex As Exception
            MxLogger.LogInformationToTrace("Exception getting LiveLink ID: {0}", ex.ToString())
            UniqueMachineId = String.Empty
        End Try

        Return UniqueMachineId
    End Function

    Function UpgradeMachineId() As String
        ' This function copies the pending machine id guid to the current machineid guid
        Dim NewMachineId As String = RegRead(Registry.LocalMachine, SubKey, PendingMachineId)
        If NewMachineId <> String.Empty Then
            RegWrite(Registry.LocalMachine, SubKey, MachineId, NewMachineId)
        End If
        Return RegRead(Registry.LocalMachine, SubKey, MachineId)
    End Function

    Public Function SetRegistryLiveLinkAutoStarts(ByVal autoStart As Boolean)
        Const liveLinkKeyName As String = "MacromatiX LiveLink"
        Const autoStartSubKey As String = "Software\Microsoft\Windows\CurrentVersion\Run"
        Dim hive As RegistryKey = Registry.LocalMachine
        Dim appPath As String = String.Empty

        If autoStart Then
            appPath = RegRead(hive, autoStartSubKey, liveLinkKeyName)
            If appPath = String.Empty OrElse appPath <> Application.ExecutablePath Then
                If RegWrite(hive, autoStartSubKey, liveLinkKeyName, Application.ExecutablePath) Then
                    LogEvent("LiveLink set to autostart in registry", EventLogEntryType.Information)
                    ' Now remove any shortcuts in the Startup folders
                    Dim startupFolder As String = Environment.GetFolderPath(Environment.SpecialFolder.Startup)
                    ' Remove from current user's profile
                    RemoveLiveLinkShortCuts(startupFolder)
                    ' Remove from All User's profile
                    RemoveLiveLinkShortCuts(startupFolder.ToLower.Replace("\" & Environment.UserName.ToLower & "\", "\All Users\"))
                Else
                    LogEvent("Unable to set LiveLink to autostart in registry", EventLogEntryType.Warning)
                End If
            End If
        Else
            If RegRead(hive, autoStartSubKey, liveLinkKeyName) <> String.Empty Then
                If RegDeleteValue(hive, autoStartSubKey, liveLinkKeyName) Then
                    LogEvent("LiveLink autostart disabled in registry", EventLogEntryType.Information)
                Else
                    LogEvent("Unable to disable LiveLink autostart in registry", EventLogEntryType.Warning)
                End If
            End If
        End If
    End Function


    Public Sub RemoveLiveLinkShortCuts(ByVal path As String)
        ' Remove any shortcut files (with a .lnk extension that contain the word livelink)
        Try
            Dim di As New System.IO.DirectoryInfo(path)
            Dim fi As System.IO.FileInfo() = di.GetFiles("*livelink*.lnk")
            For Each f As System.IO.FileInfo In fi
                If f.Name.ToLower.Contains("livelink") Then
                    f.Delete()
                End If
            Next
        Catch ex As Exception
            ' Do nothing
        End Try
    End Sub


    Function LogEvent(ByVal Description As String, Optional ByVal LogType As EventLogEntryType = EventLogEntryType.Error, Optional ByVal EventId As Integer = 0)
        ' Logs the error/warning to the Application Event Log (EventVwr.exe)
        ' Surround with a try catch to avoid errors occurring while logging if the EventLog is full
        Try
            EventLog.WriteEntry("MxLiveLink", Description, LogType, EventId)
        Catch ex As Exception
            Try
                LogToTrace(EventLogEntryType.Error, ex.Message)
            Catch exception As Exception
            End Try
        End Try

        Try
            LogToTrace(LogType, Description)
        Catch ex As Exception
        End Try
    End Function

    Function LogEvent(ByVal LogType As EventLogEntryType, ByVal Format As String, ByVal ParamArray Args() As Object)
        ' Logs the error/warning to the Application Event Log (EventVwr.exe)
        ' Surround with a try catch to avoid errors occurring while logging if the EventLog is full
        Try
            EventLog.WriteEntry("MxLiveLink", String.Format(Format, Args), LogType)
        Catch ex As Exception
            Try
                LogToTrace(EventLogEntryType.Error, ex.Message)
            Catch exception As Exception
            End Try
        End Try

        Try
            LogToTrace(LogType, String.Format(Format, Args))
        Catch ex As Exception
        End Try
    End Function

    Private Sub LogToTrace(ByVal LogType As EventLogEntryType, ByVal description As String)
        description = description.Replace(vbCrLf, " ").Replace(vbCr, " ").Replace(vbLf, " ")

        Select Case LogType
            Case EventLogEntryType.Error, EventLogEntryType.FailureAudit
                MMS.Utilities.MxLogger.LogErrorToTrace(description)
            Case EventLogEntryType.Information, EventLogEntryType.SuccessAudit
                MMS.Utilities.MxLogger.LogInformationToTrace(description)
            Case EventLogEntryType.Warning
                MMS.Utilities.MxLogger.LogWarningToTrace(description)
        End Select
    End Sub

    Public Function ClearFileAttribute(ByVal FileName As String, ByVal Attribute As FileAttributes)
        Try
            File.SetAttributes(FileName, (File.GetAttributes(FileName) And Not Attribute))
            ClearFileAttribute = True
        Catch ex As Exception
            ClearFileAttribute = False
        End Try
    End Function
End Module





