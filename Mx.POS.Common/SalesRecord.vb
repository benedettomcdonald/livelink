Imports System.Data.SqlClient

Public Class SalesRecordType

    Public Const Control As String = "C"
    Public Const Financial As String = "F"
    Public Const Header As String = "H"
    Public Const Item As String = "I"
    Public Const ItemModifier As String = "IM"
    Public Const Time As String = "T"
    Public Const Service As String = "S"
    Public Const CustomerInfo As String = "M"
    Public Const StoreInfo As String = "SI"
    Public Const ItemVoid As String = "IV"
End Class

Public Class StoreInfoRecordSubType
    Public Const Name As String = "Name"
    Public Const StoreNumber As String = "Number"
    Public Const Address As String = "Address"
    Public Const City As String = "City"
    Public Const State As String = "State"
    Public Const PostCode As String = "PostCode"
    Public Const PhoneNumber As String = "PhoneNumber"
End Class

Public Class SalesFinancialRecordSubType

    Public Const AllVoids As String = "A"
    Public Const Compliments As String = "C"
    Public Const Discount As String = "D" 'Discounts
    Public Const Free As String = "F" 'Promo
    Public Const Tips As String = "G"
    Public Const Training As String = "M" 'Training
    Public Const Other As String = "O"
    Public Const Payment As String = "P"
    Public Const TaxAmount As String = "T" 'Tax
    Public Const ItemVoid As String = "V"
    Public Const Waste As String = "W"
    Public Const Rounding As String = "9" ' Rounding
    Public Const OrderTotalVariance As String = "U" ' Difference between calculated Item sales and Gross Order Total (inc GST)
    Public Const VatTaxReduction As String = "VatTaxReduction"
    Public Const CalcVatTaxReduction As String = "CalcVatTaxReduction"
    Public Const GiftCardFailedActivationRefund As String = "GiftCardFailedActivationRefund"
End Class

Public Class SalesItemRecordSubType
    Public Const NonSalesItem As String = "NonSalesItem"
End Class

Public Class SalesControlRecordSubType

    Public Const EFTTenderKey = "0" 'TODO - Assign correct code.
    Public Const GiftCertificateSold = "1" 'TODO - Assign correct code.
    Public Const GiftCertificateRedeemed = "G" 'TODO - Assign correct code.
    Public Const MiscellaneousIncome = "2" 'TODO - Assign correct code.
    Public Const OverringPerformedIndicator = "3" 'TODO - Assign correct code.
    Public Const FixedVoucherKey = "4" 'TODO - Assign correct code.
    Public Const DrawerPull = "5" 'TODO - Assign correct code.
    Public Const DeletionBefore = "6" 'TODO - Assign correct code.
    Public Const DeletionAfter = "7" 'TODO - Assign correct code.
    Public Const OrderClosedIndicator = "8" 'TODO - Assign correct code.
    Public Const PreDiscountedOrderTotal = "B" 'TODO - Assign correct code.
    Public Const VoucherExcess = "E" 'TODO - Assign correct code.
    Public Const ShiftClose = "H" 'TODO - Assign correct code.
    Public Const PromoGiftCertificateSold = "J" 'TODO - Assign correct code.
    Public Const OpenVoucherKey = "K" 'TODO - Assign correct code.
    Public Const LoyaltyCard = "L" 'TODO - Assign correct code.
    Public Const SoldOrderWaste = "N" 'TODO - Assign correct code.
    Public Const GrossOrderTotal = "Q" 'TODO - Assign correct code.
    Public Const RefundedOrderIndicator = "R" 'TODO - Assign correct code.
    Public Const TimeStamp = "S"
    Public Const PaidOut = "U" 'TODO - Assign correct code.
    Public Const PaidIn = "PaidIn"
    Public Const CreditCardKey = "X" 'TODO - Assign correct code.
    Public Const RegisterType = "Y" 'TODO - Assign correct code.
    Public Const UnSoldOrderWaste = "Z" 'TODO - Assign correct code.
    Public Const StartOfBusiness = "SOD"
    Public Const EndOfBusiness = "EOD"
    Public Const Cancelled = "Cancelled"
    Public Const DayClose = "DayClose"
    Public Const DayOpen = "DayOpen"
    Public Const CodeRegister = "CodeRegister"

    'Public Const AmountTendered = "A" 'TODO - Assign correct code.
    'Public Const ChangeAmount = "C" 'TODO - Assign correct code.
    'Public Const OrderTotaledIndicator = "T" 'TODO - Assign correct code.


    Public Const DrawerLoan = "DL"
    Public Const TillBalance = "TB"
    Public Const CashierOpen = "COP"
    Public Const CashierClose = "CCL"
    Public Const CashierOn = "CON"
    Public Const CashierOff = "COF"
    Public Const Reduction = "RED"

    Public Const OpeningReading = "OpeningReading"
    Public Const ClosingReading = "ClosingReading"
    Public Const ForeverOpeningReading = "ForeverOpeningReading"
    Public Const ForeverClosingReading = "ForeverClosingReading"

End Class

Public Class SalesServiceRecordSubType

    Public Const Ticket = "N"
    Public Const FirstItem = "F"
    Public Const OrderTotal = "T"
    Public Const AmountTender = "A"
    Public Const DrawerClose = "D"
    Public Const AssemblyBump = "B"
    Public Const ServedOrder = "C"
    Public Const Dispatch = "U"
    Public Const Delivery = "V"
    Public Const Miscellaneous = "M"
    Public Const StoredOrder = "S"
    Public Const RecalledOrder = "R"
    Public Const ParkedOrder = "P"
    Public Const OverringOrder = "O"
    Public Const OrderAssigned = "OrderAssigned"
End Class

Public Enum CustomerInfoRecordSubType
    FullName
    FirstName
    LastName
    Address1
    Address2
    Address3
    City
    State
    Zip
    Country
    Phone
    Fax
    Email
    TaxID
End Enum

Public Class TimeRecordSubType

    Public Const ShiftStart = "S"
    Public Const BreakStart = "B"
    Public Const BreakEnd = "K"
    Public Const ShiftEnd = "E"
End Class

Public Class SalesFinancialSubTypeDescription

    Public Const Discount As String = "Discount" 'Discounts
    Public Const Waste As String = "Waste"
    Public Const Compliments As String = "Compliments"
    Public Const Free As String = "Free" 'Promo
    Public Const TaxAmount As String = "TaxAmount" 'Tax
    Public Const Payment As String = "Payment"
    Public Const Tips As String = "Tips"
    Public Const AllVoids As String = "AllVoids"
    Public Const Training As String = "Training" 'Training
    Public Const ItemVoid As String = "ItemVoid"
    Public Const Other As String = "Other"
    Public Const Rounding As String = "Rounding"
    Public Const OrderTotalVariance As String = "OrderTotalVariance"
    Public Const VatTaxReduction As String = "VatTaxReduction"
    Public Const CalcVatTaxReduction As String = "CalcVatTaxReduction"
    Public Const GiftCardFailedActivationRefund As String = "GiftCardFailedActivationRefund"
End Class

Public Class SalesControlSubTypeDescription
    Public Const TimeStamp = "TimeStamp"
    Public Const PreDiscountedOrderTotal = "PreDiscountedOrderTotal" 'TODO - Assign correct code.
    Public Const GrossOrderTotal = "GrossOrderTotal" 'TODO - Assign correct code.
    'Public Const AmountTendered = "A" 'TODO - Assign correct code.
    'Public Const ChangeAmount = "C" 'TODO - Assign correct code.
    'Public Const OrderTotaledIndicator = "T" 'TODO - Assign correct code.
    Public Const OverringPerformedIndicator = "OverringPerformedIndicator" 'TODO - Assign correct code.
    Public Const CreditCardKey = "CreditCardKey" 'TODO - Assign correct code.
    Public Const FixedVoucherKey = "FixedVoucherKey" 'TODO - Assign correct code.
    Public Const OpenVoucherKey = "OpenVoucherKey" 'TODO - Assign correct code.
    Public Const VoucherExcess = "VoucherExcess" 'TODO - Assign correct code.
    Public Const RefundedOrderIndicator = "RefundedOrderIndicator" 'TODO - Assign correct code.
    Public Const LoyaltyCard = "LoyaltyCard" 'TODO - Assign correct code.
    Public Const ShiftClose = "ShiftClose" 'TODO - Assign correct code.
    Public Const RegisterType = "RegisterType" 'TODO - Assign correct code.
    Public Const GiftCertificateSold = "GiftCertificateSold" 'TODO - Assign correct code.
    Public Const GiftCertificateRedeemed = "GiftCertificateRedeemed" 'TODO - Assign correct code.
    Public Const PromoGiftCertificateSold = "PromoGiftCertificateSold" 'TODO - Assign correct code.
    Public Const SoldOrderWaste = "SoldOrderWaste" 'TODO - Assign correct code.
    Public Const UnSoldOrderWaste = "UnSoldOrderWaste" 'TODO - Assign correct code.
    Public Const EFTTenderKey = "EFTTenderKey" 'TODO - Assign correct code.
    Public Const MiscellaneousIncome = "MiscellaneousIncome" 'TODO - Assign correct code.
    Public Const PaidOut = "PaidOut" 'TODO - Assign correct code.
    Public Const PaidIn = "PaidIn"
    Public Const DrawerPull = "DrawerPull" 'TODO - Assign correct code.
    Public Const DeletionBefore = "DeletionBefore" 'TODO - Assign correct code.
    Public Const DeletionAfter = "DeletionAfter" 'TODO - Assign correct code.
    Public Const OrderClosedIndicator = "OrderClosedIndicator"
    Public Const StartOfBusiness = "StartOfBusiness"
    Public Const EndOfBusiness = "EndOfBusiness"
    Public Const Cancelled = "Cancelled"

    Public Const DrawerLoan = "DrawerLoan"
    Public Const TillBalance = "TillBalance"
    Public Const CashierOpen = "CashierOpen"
    Public Const CashierClose = "CashierClose"
    Public Const CashierOn = "CashierOn"
    Public Const CashierOff = "CashierOff"
    Public Const Reduction = "Reduction"

    Public Const OpeningReading = "OpeningReading"
    Public Const ClosingReading = "ClosingReading"
    Public Const ForeverOpeningReading = "ForeverOpeningReading"
    Public Const ForeverClosingReading = "ForeverClosingReading"
End Class

Public Class SalesServiceSubTypeDescription

    Public Const Ticket = "Ticket"
    Public Const FirstItem = "FirstItem"
    Public Const OrderTotal = "OrderTotal"
    Public Const AmountTender = "AmountTender"
    Public Const DrawerClose = "DrawerClose"
    Public Const AssemblyBump = "AssemblyBump"
    Public Const ServedOrder = "ServedOrder"
    Public Const Dispatch = "Dispatch"
    Public Const Delivery = "Delivery"
    Public Const Miscellaneous = "Miscellaneous"
    Public Const StoredOrder = "StoredOrder"
    Public Const RecalledOrder = "RecalledOrder"
    Public Const ParkedOrder = "ParkedOrder"
    Public Const OverringOrder = "OverringOrder"
    Public Const OrderAssigned = "OrderAssigned"
    Public Const HeldOrder = "HeldOrder"

End Class

Public Class TimeSubTypeDescription

    Public Const ShiftStart = "ShiftStart"
    Public Const BreakStart = "BreakStart"
    Public Const BreakEnd = "BreakEnd"
    Public Const ShiftEnd = "ShiftEnd"
End Class

Public MustInherit Class SalesRecord

    Public Const DateTimeMinValue As DateTime = #12:00:00 AM#

    Public MustOverride ReadOnly Property RecordType() As String

    Public TransactionID As Long = 0
    Public POSTransactionID As Single = 0
    Public RecordSubType As String = ""
    Public EntityId As Single = 0.0
    Public RegisterId As Single = 0.0
    Public PollDate As DateTime = DateTimeMinValue
    Public PollCount As Single = 0.0
    Public PollAmount As Decimal = 0.0
    Public ClerkId As Single = 0.0
    Public ClerkName As String = ""
    Public CustomerId As String = ""
    Public Synced As Integer = 100 ' Preparing transaction
    Public PluCodeId As Integer = 0
    Public PluCode As String = ""
    Public SequenceNo As Integer = 0
    Public Flag As Integer = 0
    Public ApplyTax As Integer = 0
    Public DateAdded As DateTime = DateTimeMinValue
    Public SubTypeDescription As String = ""

    Public Sub Import(ByVal Connection As SqlConnection)

        Dim cmdInsert As SqlCommand

        cmdInsert = New SqlCommand("INSERT INTO [tbSalesMain] (RecordType, TransactionID, RecordSubType, EntityID, RegisterID, PollDate, PollCount, PollAmount, ClerkID, ClerkName, CustomerID, Synced, PluCodeID, PluCode, SequenceNO, Flag, ApplyTax, DateAdded, SubTypeDescription, POSTransactionID) VALUES (@RecordType, @TransactionID, @RecordSubType, @EntityID, @RegisterID, @PollDate, @PollCount, @PollAmount, @ClerkID, @ClerkName, @CustomerID, @Synced, @PluCodeID, @PluCode, @SequenceNo, @Flag, @ApplyTax, getdate(), @SubTypeDescription, @POSTransactionID)", Connection)

        cmdInsert.Parameters.Add("@RecordType", Me.RecordType)
        cmdInsert.Parameters.Add("@TransactionID", Me.TransactionID)
        cmdInsert.Parameters.Add("@RecordSubType", Me.RecordSubType)
        cmdInsert.Parameters.Add("@EntityID", Me.EntityId)
        cmdInsert.Parameters.Add("@RegisterID", Me.RegisterId)
        cmdInsert.Parameters.Add("@PollDate", Me.PollDate.ToString(DateFormatNames.DatabaseDateTimeFormatWithSecond))
        cmdInsert.Parameters.Add("@PollCount", Me.PollCount)
        cmdInsert.Parameters.Add("@PollAmount", Me.PollAmount)
        cmdInsert.Parameters.Add("@ClerkID", Me.ClerkId)
        cmdInsert.Parameters.Add("@ClerkName", Me.ClerkName)
        cmdInsert.Parameters.Add("@CustomerID", Me.CustomerId)
        cmdInsert.Parameters.Add("@Synced", Me.Synced)
        cmdInsert.Parameters.Add("@PluCodeID", Me.PluCodeId)
        cmdInsert.Parameters.Add("@PluCode", Me.PluCode)
        cmdInsert.Parameters.Add("@SequenceNo", Me.SequenceNo)
        cmdInsert.Parameters.Add("@Flag", Me.Flag)
        cmdInsert.Parameters.Add("@ApplyTax", Me.ApplyTax)
        cmdInsert.Parameters.Add("@SubTypeDescription", Me.SubTypeDescription)
        cmdInsert.Parameters.Add("@POSTransactionID", Me.POSTransactionID)
        cmdInsert.ExecuteNonQuery()

    End Sub

End Class

