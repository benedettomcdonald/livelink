﻿Imports MMS.Utilities
Imports Mx.Services.LiveLink
Imports Mx.BusinessObjects.LiveLink

Public Class POSHelper
    ''' <summary>
    ''' Log Error and send error alert.
    ''' </summary>
    ''' <param name="entityID"></param>
    ''' <param name="msgTitle"></param>
    ''' <param name="msgDetail"></param>
    ''' <remarks></remarks>
    Public Shared Sub OnError(entityID As Long, msgTitle As String, msgDetail As String)
        MxLogger.LogError(MxLogger.LogCategory.MxClient, msgDetail)

        If LiveLinkConfiguration.SendDataType = "Alert" Then
            SalesMainService.InsertAlertMessage(entityID, AlertRecordSubType.Error, msgTitle, msgDetail)
        End If
    End Sub

    ''' <summary>
    ''' Log Warning and send warning alert
    ''' </summary>
    ''' <param name="entityID"></param>
    ''' <param name="msgTitle"></param>
    ''' <param name="msgDetail"></param>
    ''' <remarks></remarks>
    Public Shared Sub OnWarning(entityID As Long, msgTitle As String, msgDetail As String)
        MxLogger.LogWarning(MxLogger.LogCategory.MxClient, msgDetail)

        If LiveLinkConfiguration.SendDataType = "Alert" Then
            SalesMainService.InsertAlertMessage(entityID, AlertRecordSubType.Warning, msgTitle, msgDetail)
        End If
    End Sub
End Class
