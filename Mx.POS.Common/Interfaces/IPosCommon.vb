﻿Imports System
Imports System.Collections.Generic
Imports System.Text
Imports Mx.POS.Common.BusinessObjects

Namespace Interfaces
    Public Interface IPosCommon
        ReadOnly Property PosName() As String
        ReadOnly Property GatewayEnabled() As Boolean
        ReadOnly Property PollInterval() As Integer
        Property LastPollDate() As DateTime

        Sub SetupDatabase()
        Sub ProcessData(ByVal entityId As Integer)
        Sub PurgeOldData(ByVal daysBeforeObsolete As Integer)

        Event StatusChangedEvent As EventHandler(Of Progress)
    End Interface
End Namespace