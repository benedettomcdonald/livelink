﻿Namespace BusinessObjects
    Public Class Progress
        Inherits EventArgs

        Private _statusMsg As KeyValuePair(Of String, String)

        Public Property StatusMsg() As KeyValuePair(Of String, String)
            Get
                Return _statusMsg
            End Get
            Private Set(value As KeyValuePair(Of String, String))
                _statusMsg = value
            End Set
        End Property

        Public Sub New(status As KeyValuePair(Of String, String))
            _statusMsg = status
        End Sub
    End Class
End Namespace