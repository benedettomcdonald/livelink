Imports System.Management
Imports MMS.Utilities


Public Class clsSysInfo
    Private objOS As ManagementObjectSearcher
    Private objCS As ManagementObjectSearcher
    Private objLD As ManagementObjectSearcher
    Private objMgmt As ManagementObject
    Private m_strComputerName As String
    Private m_strManufacturer As String
    Private m_StrModel As String
    Private m_strOSName As String
    Private m_strOSVersion As String
    Private m_strSystemType As String
    Private m_strTPM As String
    Private m_strWindowsDir As String
    Private m_strFixedDisk1 As String
    Private m_strFixedDisk2 As String
    Private m_strFixedDisk3 As String
    Private m_strCPU As String
    Private m_strDriveSerial As String
    Private m_strMAC As String
    Private _comExceptionThrown As Boolean = False

    Public Sub New()
        Const cUnknown = "Unknown"

        objOS = New ManagementObjectSearcher("SELECT * FROM Win32_OperatingSystem")
        objCS = New ManagementObjectSearcher("SELECT * FROM Win32_ComputerSystem")
        objLD = New ManagementObjectSearcher("SELECT * FROM Win32_LogicalDisk")

        For Each objMgmt In objOS.Get
            Try
                m_strOSName = objMgmt("name").ToString()
            Catch ex As Exception
                m_strOSName = cUnknown
            End Try
            Try
                m_strOSVersion = objMgmt("version").ToString()
            Catch ex As Exception
                m_strOSVersion = cUnknown
            End Try
            Try
                m_strComputerName = objMgmt("csname").ToString()
            Catch ex As Exception
                m_strComputerName = cUnknown
            End Try
            Try
                m_strWindowsDir = objMgmt("windowsdirectory").ToString()
            Catch ex As Exception
                m_strWindowsDir = cUnknown
            End Try
        Next

        For Each objMgmt In objCS.Get
            Try
                m_strManufacturer = objMgmt("manufacturer").ToString()
            Catch ex As Exception
                m_strManufacturer = cUnknown
            End Try
            Try
                m_StrModel = objMgmt("model").ToString()
            Catch ex As Exception
                m_StrModel = cUnknown
            End Try
            Try
                m_strSystemType = objMgmt("systemtype").ToString()
            Catch ex As Exception
                m_strSystemType = cUnknown
            End Try
            Try
                m_strTPM = objMgmt("totalphysicalmemory").ToString()
            Catch ex As Exception
                m_strTPM = cUnknown
            End Try
        Next


        Dim DiskNum As Integer = 1
        For Each objMgmt In objLD.Get
            Dim DiskInfo As String
            Dim DiskBytes As Int64
            Dim strDiskBytes As String

            Try
                If objMgmt("MediaType").ToString() = "12" Then ' Fixed hard disk
                    Try
                        DiskBytes = Val(objMgmt("Size").ToString)
                        If DiskBytes < 1024 * 1024 * 1024 Then 'If less than 1 GB
                            strDiskBytes = Format(DiskBytes / 1024 / 1024, "N") & " mb"
                        Else
                            strDiskBytes = Format(DiskBytes / 1024 / 1024 / 1024, "N") & " GB"
                        End If

                        DiskInfo = "Drive:[" & objMgmt("DeviceId").ToString() & "] Size:[" & strDiskBytes & "] "

                        DiskBytes = Val(objMgmt("FreeSpace").ToString)
                        If DiskBytes < 1024 * 1024 * 1024 Then
                            strDiskBytes = Format(DiskBytes / 1024 / 1024, "N") & " mb"
                        Else
                            strDiskBytes = Format(DiskBytes / 1024 / 1024 / 1024, "N") & " GB"
                        End If
                        DiskInfo = DiskInfo + "Free space:[" & strDiskBytes & "]"
                    Catch ex As Exception
                        DiskInfo = "Unknown"
                    End Try
                    Select Case DiskNum
                        Case 1
                            m_strFixedDisk1 = DiskInfo
                        Case 2
                            m_strFixedDisk2 = DiskInfo
                        Case 3
                            m_strFixedDisk3 = DiskInfo
                    End Select
                    DiskNum += 1
                End If
            Catch ex As Exception
            End Try
        Next
    End Sub

    Public Sub New(ByVal Filter As String)
        Try
            Dim mc As System.Management.ManagementClass
            Dim mo As ManagementObject
            Dim moc As ManagementObjectCollection

            If InStr(Filter.ToUpper, "!MAC!") > 0 Then

                Try
                    mc = New ManagementClass("Win32_NetworkAdapterConfiguration")
                    moc = mc.GetInstances()
                    For Each mo In moc
                        If mo.Item("IPEnabled") = True Then
                            m_strMAC = "MAC:" & mo.Item("MacAddress").ToString()
                            Exit For
                        End If
                    Next
                Catch comEx As Runtime.InteropServices.COMException
                    MxLogger.LogInformationToTrace("COMException getting MacAddress: {0}", comEx.ToString())
                    _comExceptionThrown = True
                Catch ex As Exception
                    MxLogger.LogInformationToTrace("Exception getting MacAddress: {0}", ex.ToString())
                End Try
                mc = Nothing
            End If

            If InStr(Filter.ToUpper, "!CPU!") > 0 Then
                Try
                    mc = New Management.ManagementClass("Win32_Processor")
                    moc = mc.GetInstances()
                    Dim CPUid As String
                    For Each mo In moc
                        CPUid = mo.Properties("ProcessorId").Value.ToString()
                        If CPUid <> String.Empty Then
                            m_strCPU = "CPU:" & CPUid
                            Exit For
                        End If
                    Next
                Catch comEx As Runtime.InteropServices.COMException
                    MxLogger.LogInformationToTrace("COMException getting CPU ProcessorId: {0}", comEx.ToString())
                    _comExceptionThrown = True
                Catch ex As Exception
                    MxLogger.LogInformationToTrace("Exception getting CPU ProcessorId: {0}", ex.ToString())
                End Try
                mc = Nothing
            End If

            If InStr(Filter.ToUpper, "!DRIVESERIAL!") > 0 Then
                Try
                    mc = New Management.ManagementClass("Win32_LogicalDisk")
                    moc = mc.GetInstances()
                    For Each mo In moc
                        If mo.Item("deviceid").ToString() = "C:" Then
                            m_strDriveSerial = "Drive:" & mo.Item("VolumeSerialNumber").ToString()
                            Exit For
                        End If
                    Next
                Catch comEx As Runtime.InteropServices.COMException
                    MxLogger.LogInformationToTrace("COMException getting Drive C: Serial: {0}", comEx.ToString())
                    _comExceptionThrown = True
                Catch ex As Exception
                    MxLogger.LogInformationToTrace("Exception getting Drive C: Serial: {0}", ex.ToString())
                End Try
                mc = Nothing
            End If
        Catch ex As Exception
            MxLogger.LogInformationToTrace("Exception getting filtered SysInfo (filter={0}). Exception: {1}", Filter, ex.ToString())
        End Try
    End Sub

    Public ReadOnly Property COMExceptionThrown
        Get
            Return _comExceptionThrown
        End Get
    End Property

    Public ReadOnly Property ComputerName()
        Get
            ComputerName = m_strComputerName
        End Get

    End Property
    Public ReadOnly Property Manufacturer()
        Get
            Manufacturer = m_strManufacturer
        End Get

    End Property
    Public ReadOnly Property Model()
        Get
            Model = m_StrModel
        End Get

    End Property
    Public ReadOnly Property OsName()
        Get
            OsName = m_strOSName
        End Get

    End Property

    Public ReadOnly Property OSVersion()
        Get
            OSVersion = m_strOSVersion
        End Get

    End Property

    Public ReadOnly Property SystemType()
        Get
            SystemType = m_strSystemType
        End Get

    End Property

    Public ReadOnly Property TotalPhysicalMemory()
        Get
            TotalPhysicalMemory = m_strTPM
        End Get

    End Property

    Public ReadOnly Property WindowsDirectory()
        Get
            WindowsDirectory = m_strWindowsDir
        End Get

    End Property

    Public ReadOnly Property FixedDisk1()
        Get
            FixedDisk1 = m_strFixedDisk1
        End Get
    End Property

    Public ReadOnly Property FixedDisk2()
        Get
            FixedDisk2 = m_strFixedDisk2
        End Get
    End Property

    Public ReadOnly Property FixedDisk3()
        Get
            FixedDisk3 = m_strFixedDisk3
        End Get
    End Property

    Public ReadOnly Property CPU()
        Get
            CPU = m_strCPU
        End Get
    End Property

    Public ReadOnly Property MAC()
        Get
            MAC = m_strMAC
        End Get
    End Property

    Public ReadOnly Property DriveSerial()
        Get
            DriveSerial = m_strDriveSerial
        End Get
    End Property

End Class