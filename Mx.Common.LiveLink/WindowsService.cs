﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Microsoft.Win32;

namespace Mx.Common.LiveLink
{
    public class WindowsService
    {

        #region Command Types

        public enum LiveLinkServiceCommandType : int
        {
            ServiceUpdate = 128,
            ServiceRestart = 129,
            ImportData = 130,
            SendData = 131,
            SendSystemInfo = 132,
            ProcessAwake = 133,
            RetrieveMessage = 134,
            RefreshEntity = 135,
            RecreateLiveLinkID = 136,
            ReActivateLiveLink = 137,
            RefreshConfiguration = 138,
        }

        #endregion

        #region Public Properties

        public static string LiveLinkServiceName { get { return "MXLIVELINK"; } }
        public static string LiveLinkServiceUpdaterName { get { return "MXLIVELINKUPDATER"; } }
        public static string LiveLinkServiceRegistryKey = @"SOFTWARE\MacromatiX\LiveLinkService\";
        public static string LiveLinkServiceRegistryKeyNameStoreNumber = "StoreNumber";

        public static string LiveLinkProcessName { get { return "LiveLink"; } }
        public static string LiveLinkServiceHelperProcessName { get { return "LiveLinkSericeHelper"; } }

        #endregion

        #region Private Variables

        private const string _livelinkServiceLocationRegistryKey = @"SYSTEM\CurrentControlSet\services\{0}\";
        private const string _livelinkServiceLocationRegistryKeyName = "ImagePath";

        #endregion

        #region Service Information

        public static string GetServicePath(string serviceName)
        {
            RegistryKey rk = Registry.LocalMachine.OpenSubKey(string.Format(_livelinkServiceLocationRegistryKey, serviceName));
            if (rk == null)
                return null;

            object keyResult = rk.GetValue(_livelinkServiceLocationRegistryKeyName);
            if (keyResult == null)
                return null;

            // get the service execution path from the registry value
            // exclude the executable from the path. e.g. "C:\Program Files\MacromatiX\LiveLinkService\LiveLinkService.exe"
            // remove the LiveLinkService.exe from the path and strip out the extra " characters
            string servicePath = Convert.ToString(keyResult);
            return Path.GetDirectoryName(servicePath.Replace('"', ' ').Trim());
        }

        #endregion


        #region Process Information

        public static bool CheckProcessRunning(string processName)
        {
            if (System.Diagnostics.Process.GetProcessesByName(processName).Length > 0)
                return true;
            return false;
        }

        public static bool CheckAlreadyRunning(string processName)
        {
            if (System.Diagnostics.Process.GetProcessesByName(processName).Length > 1)
                return true;
            return false;
        }

        #endregion
    }
}
