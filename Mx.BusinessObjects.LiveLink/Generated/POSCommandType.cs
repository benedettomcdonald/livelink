
/*
*----------------------------------------------------------------------------------------------------------------
* <autogenerated>
*	This code was generated from a custom tool 
*
*		Tool		: TransformCodeGenerator, Version=1.0.0.2, Culture=neutral, PublicKeyToken=null
*		Source File : POSCommandType.xml
*		Template	: MakeBusinessObject.xslt
*
*		Date		: Wednesday, October 26, 2011
*
*  Changes to this file may cause incorrect behavior and will be lost if
*  the code is regenerated.
*
*
*  To extend this class create or update the partial class POSCommandType in ..\Custom\POSCommandType.vb
*
* </autogenerated>
*----------------------------------------------------------------------------------------------------------------
*/

  using System;
  
  using System.Collections.Generic;
  using System.Globalization;
  using System.Collections.ObjectModel;
  using System.Data.SqlTypes;
  
  using Mx.Common.Domain;
  using Mx.Common.ValidationAttributes;

  namespace Mx.BusinessObjects.LiveLink
  {
    
    [Serializable]
    public partial class POSCommandType : EntityBase
    {



#region Private Variables
    
    [System.Diagnostics.DebuggerBrowsable(System.Diagnostics.DebuggerBrowsableState.Never)]
    private short? _pOSCommandTypeID;

    [System.Diagnostics.DebuggerBrowsable(System.Diagnostics.DebuggerBrowsableState.Never)]
    private string _type;

    [System.Diagnostics.DebuggerBrowsable(System.Diagnostics.DebuggerBrowsableState.Never)]
    private string _description;

    [System.Diagnostics.DebuggerBrowsable(System.Diagnostics.DebuggerBrowsableState.Never)]
    private static POSCommandType _empty;
#endregion


#region Properties
		
    
	  
      
    public virtual short? POSCommandTypeID
    {
        get
        {
            return _pOSCommandTypeID;
        }
        set
        {
            
            if ((!_pOSCommandTypeID.HasValue && value.HasValue) || ( _pOSCommandTypeID.HasValue && _pOSCommandTypeID.Value != value))
            {
                  _pOSCommandTypeID = value;
                  OnPropertyChanged("POSCommandTypeID");
                  _items = null;
            }                        
            
        }
    }

    
	  
      
    public virtual string Type
    {
        get
        {
            return _type;
        }
        set
        {
            
            if ((((object)_type)==null && ((object)value) !=null) || ((object)value)!=null && ! _type.Equals(value) || ((object)value)==null && ((object)_type) != null)
            {
                  _type = value;
	                OnPropertyChanged("Type");
                  _items = null;
            }
              
        }
    }

    
	  
      
    public virtual string Description
    {
        get
        {
            return _description;
        }
        set
        {
            
            if ((((object)_description)==null && ((object)value) !=null) || ((object)value)!=null && ! _description.Equals(value) || ((object)value)==null && ((object)_description) != null)
            {
                  _description = value;
	                OnPropertyChanged("Description");
                  _items = null;
            }
              
        }
    }

#endregion


#region Constructors
    
    
      
    public POSCommandType()
    {
        NewExtended();
    }

    public POSCommandType(short? pOSCommandTypeID, string type, string description) : this()
    {
        
        _pOSCommandTypeID = pOSCommandTypeID;

        _type = type;

        _description = description;

        NewExtended(pOSCommandTypeID, type, description);
    }

    
    public POSCommandType(System.Data.IDataReader reader, bool closeAfterRead) : this()
    {
        
		_pOSCommandTypeID = Mx.Common.Data.DataHelper.TryGetValueFromReader<short?>(reader, "POSCommandTypeID", null);

		_type = Mx.Common.Data.DataHelper.TryGetValueFromReader<string>(reader, "Type", null);

		_description = Mx.Common.Data.DataHelper.TryGetValueFromReader<string>(reader, "Description", null);

        NewExtended(reader);
        if (closeAfterRead) reader.Close();
    }

    public POSCommandType(System.Data.IDataReader reader) : this(reader, true){}



    public POSCommandType(System.Collections.IDictionary dictionary) : this()
    {
        
		_pOSCommandTypeID = Mx.Common.Data.DataHelper.TryGetValueFromDictionary<short?>(dictionary, "POSCommandTypeID", null);


		_type = Mx.Common.Data.DataHelper.TryGetValueFromDictionary<string>(dictionary, "Type", null);


		_description = Mx.Common.Data.DataHelper.TryGetValueFromDictionary<string>(dictionary, "Description", null);


        NewExtended(dictionary);
    }


    
		
#endregion


#region Custom Constructors
    // Implement the following in your partial class to enable Custom Contruction of POSCommandType.
    partial void NewExtended();
    partial void NewExtended(short? pOSCommandTypeID, string type, string description);
    
    partial void NewExtended(System.Data.IDataReader reader);
    
    partial void NewExtended(System.Collections.IDictionary dictionary);
    
#endregion
  
#region Items

    private Dictionary<string, object> _items = null;

    [System.Xml.Serialization.XmlIgnoreAttribute]
    public override Dictionary<string, object> Items
    {
        get
        {
            if (_items == null)
            {
                _items = new Dictionary<string, object>();
                _items.Add("POSCommandTypeID", _pOSCommandTypeID);
                _items.Add("Type", _type);
                _items.Add("Description", _description);
                
			}
			return _items;
		}
	}

#endregion



#region Empty

    public static POSCommandType Empty
    {
        get
        {
            if (_empty == null)
            {
                _empty = new POSCommandType();
                
                _empty.POSCommandTypeID = null;
                _empty.Type = null;
                _empty.Description = null;
            }
            return _empty;
        }
    }
	
#endregion	


#region PropertyInformation

  public static MMS.Utilities.PropertyInfoCollection PropertyInformation()
  {
    MMS.Utilities.PropertyInfoCollection pinfos = new MMS.Utilities.PropertyInfoCollection();
    System.Type t = typeof(POSCommandType);
    
    pinfos.Add(t.GetProperty("POSCommandTypeID"));
    
    pinfos.Add(t.GetProperty("Type"));
    
    pinfos.Add(t.GetProperty("Description"));
    
    return pinfos;
  }

	public static MMS.Utilities.PropertyInfoCollection ReaderPropertyInformation()
  {
    
		return POSCommandType.PropertyInformation();
  }
    
#endregion



}


#region Collections - List<T>

[Serializable]
public partial class POSCommandTypeList : Mx.Common.Collections.ValidatableList<POSCommandType>
{
	public POSCommandTypeList() : base(){}

    public POSCommandTypeList(System.Collections.Generic.IEnumerable<POSCommandType> collection) : base(collection)
    {
    }

    
    public POSCommandTypeList(System.Data.IDataReader reader, bool closeAfterRead)
    {
		while (reader.Read())
        {
			Add(new POSCommandType(reader, false));
		}
		if ((!reader.IsClosed) && (closeAfterRead))
        {
			reader.Close();
        }
	}
    public POSCommandTypeList(System.Data.IDataReader reader) : this(reader, true){}
    
    
    public List<EntityBase> ToBusinessObjectList()
    {
        return new List<EntityBase>(this.ToArray());
    }

    public MMS.Utilities.MxBusinessObjectDataReader<POSCommandType> CreateReader()
    {
        return new MMS.Utilities.MxBusinessObjectDataReader<Mx.BusinessObjects.LiveLink.POSCommandType>(this, Mx.BusinessObjects.LiveLink.POSCommandType.ReaderPropertyInformation());
    }
	
}

#endregion
}