
/*
*----------------------------------------------------------------------------------------------------------------
* <autogenerated>
*	This code was generated from a custom tool 
*
*		Tool		: TransformCodeGenerator, Version=1.0.0.2, Culture=neutral, PublicKeyToken=null
*		Source File : LiveSync.xml
*		Template	: MakeBusinessObject.xslt
*
*		Date		: Wednesday, October 26, 2011
*
*  Changes to this file may cause incorrect behavior and will be lost if
*  the code is regenerated.
*
*
*  To extend this class create or update the partial class LiveSync in ..\Custom\LiveSync.vb
*
* </autogenerated>
*----------------------------------------------------------------------------------------------------------------
*/

  using System;
  
  using System.Collections.Generic;
  using System.Globalization;
  using System.Collections.ObjectModel;
  using System.Data.SqlTypes;
  
  using Mx.Common.Domain;
  using Mx.Common.ValidationAttributes;

  namespace Mx.BusinessObjects.LiveLink
  {
    
    [Serializable]
    public partial class LiveSync : EntityBase
    {



#region Private Variables
    
    [System.Diagnostics.DebuggerBrowsable(System.Diagnostics.DebuggerBrowsableState.Never)]
    private int? _liveSyncID;

    [System.Diagnostics.DebuggerBrowsable(System.Diagnostics.DebuggerBrowsableState.Never)]
    private long _entityID;

    [System.Diagnostics.DebuggerBrowsable(System.Diagnostics.DebuggerBrowsableState.Never)]
    private string _type;

    [System.Diagnostics.DebuggerBrowsable(System.Diagnostics.DebuggerBrowsableState.Never)]
    private int _headerID;

    [System.Diagnostics.DebuggerBrowsable(System.Diagnostics.DebuggerBrowsableState.Never)]
    private byte _synced;

    [System.Diagnostics.DebuggerBrowsable(System.Diagnostics.DebuggerBrowsableState.Never)]
    private string _syncToken;

    [System.Diagnostics.DebuggerBrowsable(System.Diagnostics.DebuggerBrowsableState.Never)]
    private DateTime _created;

    [System.Diagnostics.DebuggerBrowsable(System.Diagnostics.DebuggerBrowsableState.Never)]
    private DateTime? _updated;

    [System.Diagnostics.DebuggerBrowsable(System.Diagnostics.DebuggerBrowsableState.Never)]
    private static LiveSync _empty;
#endregion


#region Properties
		
    
	  
      
    public virtual int? LiveSyncID
    {
        get
        {
            return _liveSyncID;
        }
        set
        {
            
            if ((!_liveSyncID.HasValue && value.HasValue) || ( _liveSyncID.HasValue && _liveSyncID.Value != value))
            {
                  _liveSyncID = value;
                  OnPropertyChanged("LiveSyncID");
                  _items = null;
            }                        
            
        }
    }

    
	  
      
    public virtual long EntityID
    {
        get
        {
            return _entityID;
        }
        set
        {
            
            if ((((object)_entityID)==null && ((object)value) !=null) || ((object)value)!=null && ! _entityID.Equals(value) || ((object)value)==null && ((object)_entityID) != null)
            {
                  _entityID = value;
	                OnPropertyChanged("EntityID");
                  _items = null;
            }
              
        }
    }

    
	  
      
    public virtual string Type
    {
        get
        {
            return _type;
        }
        set
        {
            
            if ((((object)_type)==null && ((object)value) !=null) || ((object)value)!=null && ! _type.Equals(value) || ((object)value)==null && ((object)_type) != null)
            {
                  _type = value;
	                OnPropertyChanged("Type");
                  _items = null;
            }
              
        }
    }

    
	  
      
    public virtual int HeaderID
    {
        get
        {
            return _headerID;
        }
        set
        {
            
            if ((((object)_headerID)==null && ((object)value) !=null) || ((object)value)!=null && ! _headerID.Equals(value) || ((object)value)==null && ((object)_headerID) != null)
            {
                  _headerID = value;
	                OnPropertyChanged("HeaderID");
                  _items = null;
            }
              
        }
    }

    
	  
      
    public virtual byte Synced
    {
        get
        {
            return _synced;
        }
        set
        {
            
            if ((((object)_synced)==null && ((object)value) !=null) || ((object)value)!=null && ! _synced.Equals(value) || ((object)value)==null && ((object)_synced) != null)
            {
                  _synced = value;
	                OnPropertyChanged("Synced");
                  _items = null;
            }
              
        }
    }

    
	  
      
    public virtual string SyncToken
    {
        get
        {
            return _syncToken;
        }
        set
        {
            
            if ((((object)_syncToken)==null && ((object)value) !=null) || ((object)value)!=null && ! _syncToken.Equals(value) || ((object)value)==null && ((object)_syncToken) != null)
            {
                  _syncToken = value;
	                OnPropertyChanged("SyncToken");
                  _items = null;
            }
              
        }
    }

    
	  
      
    public virtual DateTime Created
    {
        get
        {
            return _created;
        }
        set
        {
            
            if ((((object)_created)==null && ((object)value) !=null) || ((object)value)!=null && ! _created.Equals(value) || ((object)value)==null && ((object)_created) != null)
            {
                  _created = value;
	                OnPropertyChanged("Created");
                  _items = null;
            }
              
        }
    }

    
	  
      
    public virtual DateTime? Updated
    {
        get
        {
            return _updated;
        }
        set
        {
            
            if ((!_updated.HasValue && value.HasValue) || ( _updated.HasValue && _updated.Value != value))
            {
                  _updated = value;
                  OnPropertyChanged("Updated");
                  _items = null;
            }                        
            
        }
    }

#endregion


#region Constructors
    
    
      
    public LiveSync()
    {
        NewExtended();
    }

    public LiveSync(int? liveSyncID, long entityID, string type, int headerID, byte synced, string syncToken, DateTime created, DateTime? updated) : this()
    {
        
        _liveSyncID = liveSyncID;

        _entityID = entityID;

        _type = type;

        _headerID = headerID;

        _synced = synced;

        _syncToken = syncToken;

        _created = created;

        _updated = updated;

        NewExtended(liveSyncID, entityID, type, headerID, synced, syncToken, created, updated);
    }

    
    public LiveSync(System.Data.IDataReader reader, bool closeAfterRead) : this()
    {
        
		_liveSyncID = Mx.Common.Data.DataHelper.TryGetValueFromReader<int?>(reader, "LiveSyncID", null);

		_entityID = Mx.Common.Data.DataHelper.TryGetValueFromReader<long>(reader, "EntityID", MMS.Utilities.MxConstants.LONG_NULL);

		_type = Mx.Common.Data.DataHelper.TryGetValueFromReader<string>(reader, "Type", null);

		_headerID = Mx.Common.Data.DataHelper.TryGetValueFromReader<int>(reader, "HeaderID", MMS.Utilities.MxConstants.INTEGER_NULL);

		_synced = Mx.Common.Data.DataHelper.TryGetValueFromReader<byte>(reader, "Synced", MMS.Utilities.MxConstants.BYTE_NULL);

		_syncToken = Mx.Common.Data.DataHelper.TryGetValueFromReader<string>(reader, "SyncToken", null);

		_created = Mx.Common.Data.DataHelper.TryGetValueFromReader<DateTime>(reader, "Created", MMS.Utilities.MxConstants.DATETIME_NULL);

		_updated = Mx.Common.Data.DataHelper.TryGetValueFromReader<DateTime?>(reader, "Updated", null);

        NewExtended(reader);
        if (closeAfterRead) reader.Close();
    }

    public LiveSync(System.Data.IDataReader reader) : this(reader, true){}



    public LiveSync(System.Collections.IDictionary dictionary) : this()
    {
        
		_liveSyncID = Mx.Common.Data.DataHelper.TryGetValueFromDictionary<int?>(dictionary, "LiveSyncID", null);


		_entityID = Mx.Common.Data.DataHelper.TryGetValueFromDictionary<long>(dictionary, "EntityID", MMS.Utilities.MxConstants.LONG_NULL);


		_type = Mx.Common.Data.DataHelper.TryGetValueFromDictionary<string>(dictionary, "Type", null);


		_headerID = Mx.Common.Data.DataHelper.TryGetValueFromDictionary<int>(dictionary, "HeaderID", MMS.Utilities.MxConstants.INTEGER_NULL);


		_synced = Mx.Common.Data.DataHelper.TryGetValueFromDictionary<byte>(dictionary, "Synced", MMS.Utilities.MxConstants.BYTE_NULL);


		_syncToken = Mx.Common.Data.DataHelper.TryGetValueFromDictionary<string>(dictionary, "SyncToken", null);


		_created = Mx.Common.Data.DataHelper.TryGetValueFromDictionary<DateTime>(dictionary, "Created", MMS.Utilities.MxConstants.DATETIME_NULL);


		_updated = Mx.Common.Data.DataHelper.TryGetValueFromDictionary<DateTime?>(dictionary, "Updated", null);


        NewExtended(dictionary);
    }


    
		
#endregion


#region Custom Constructors
    // Implement the following in your partial class to enable Custom Contruction of LiveSync.
    partial void NewExtended();
    partial void NewExtended(int? liveSyncID, long entityID, string type, int headerID, byte synced, string syncToken, DateTime created, DateTime? updated);
    
    partial void NewExtended(System.Data.IDataReader reader);
    
    partial void NewExtended(System.Collections.IDictionary dictionary);
    
#endregion
  
#region Items

    private Dictionary<string, object> _items = null;

    [System.Xml.Serialization.XmlIgnoreAttribute]
    public override Dictionary<string, object> Items
    {
        get
        {
            if (_items == null)
            {
                _items = new Dictionary<string, object>();
                _items.Add("LiveSyncID", _liveSyncID);
                _items.Add("EntityID", _entityID);
                _items.Add("Type", _type);
                _items.Add("HeaderID", _headerID);
                _items.Add("Synced", _synced);
                _items.Add("SyncToken", _syncToken);
                _items.Add("Created", _created);
                _items.Add("Updated", _updated);
                
			}
			return _items;
		}
	}

#endregion



#region Empty

    public static LiveSync Empty
    {
        get
        {
            if (_empty == null)
            {
                _empty = new LiveSync();
                
                _empty.LiveSyncID = null;
                _empty.EntityID = MMS.Utilities.MxConstants.LONG_NULL;
                _empty.Type = null;
                _empty.HeaderID = MMS.Utilities.MxConstants.INTEGER_NULL;
                _empty.Synced = MMS.Utilities.MxConstants.BYTE_NULL;
                _empty.SyncToken = null;
                _empty.Created = MMS.Utilities.MxConstants.DATETIME_NULL;
                _empty.Updated = null;
            }
            return _empty;
        }
    }
	
#endregion	


#region PropertyInformation

  public static MMS.Utilities.PropertyInfoCollection PropertyInformation()
  {
    MMS.Utilities.PropertyInfoCollection pinfos = new MMS.Utilities.PropertyInfoCollection();
    System.Type t = typeof(LiveSync);
    
    pinfos.Add(t.GetProperty("LiveSyncID"));
    
    pinfos.Add(t.GetProperty("EntityID"));
    
    pinfos.Add(t.GetProperty("Type"));
    
    pinfos.Add(t.GetProperty("HeaderID"));
    
    pinfos.Add(t.GetProperty("Synced"));
    
    pinfos.Add(t.GetProperty("SyncToken"));
    
    pinfos.Add(t.GetProperty("Created"));
    
    pinfos.Add(t.GetProperty("Updated"));
    
    return pinfos;
  }

	public static MMS.Utilities.PropertyInfoCollection ReaderPropertyInformation()
  {
    
		return LiveSync.PropertyInformation();
  }
    
#endregion



}


#region Collections - List<T>

[Serializable]
public partial class LiveSyncList : Mx.Common.Collections.ValidatableList<LiveSync>
{
	public LiveSyncList() : base(){}

    public LiveSyncList(System.Collections.Generic.IEnumerable<LiveSync> collection) : base(collection)
    {
    }

    
    public LiveSyncList(System.Data.IDataReader reader, bool closeAfterRead)
    {
		while (reader.Read())
        {
			Add(new LiveSync(reader, false));
		}
		if ((!reader.IsClosed) && (closeAfterRead))
        {
			reader.Close();
        }
	}
    public LiveSyncList(System.Data.IDataReader reader) : this(reader, true){}
    
    
    public List<EntityBase> ToBusinessObjectList()
    {
        return new List<EntityBase>(this.ToArray());
    }

    public MMS.Utilities.MxBusinessObjectDataReader<LiveSync> CreateReader()
    {
        return new MMS.Utilities.MxBusinessObjectDataReader<Mx.BusinessObjects.LiveLink.LiveSync>(this, Mx.BusinessObjects.LiveLink.LiveSync.ReaderPropertyInformation());
    }
	
}

#endregion
}