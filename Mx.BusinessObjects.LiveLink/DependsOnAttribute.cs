﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Mx.BusinessObjects
{
    /// <summary>
    /// Indicates that a property is dependant on another property. Note that the dependor property must 
    /// exist in the same object or a runtime exception may occur.
    /// </summary>
    /// <remarks>
    /// If the MxBusinessObject implements INotifyPropertyChanged then a PropertyChanged event will be raised
    /// for the marked property when any of the properties it depends on changes.
    /// </remarks>
    [AttributeUsage(AttributeTargets.Property, Inherited = false, AllowMultiple = false)]
    public class DependsOnAttribute : Attribute
    {

        private Collection<string> _dependsOn;
        private string _propertyName;

        public string PropertyName
        {
            get { return _propertyName; }
            set { _propertyName = value; }
        }



        public DependsOnAttribute(params string[] dependsOn)
        {

            if ((dependsOn != null))
            {
                _dependsOn = new Collection<string>();
                foreach (string dependency in dependsOn)
                {
                    _dependsOn.Add(dependency);
                }

            }
        }

        public Collection<string> DependsOn
        {
            get { return _dependsOn; }
            set { _dependsOn = value; }
        }

    }
}