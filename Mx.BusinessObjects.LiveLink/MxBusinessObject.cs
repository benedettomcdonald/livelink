﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;

namespace Mx.BusinessObjects
{
    [Serializable()]
    public abstract class MxBusinessObject
    {

        private static Dictionary<string, List<string>> _propertyDependencies;

        public MxBusinessObject()
        {
        }

        public MxBusinessObject(IDictionary values)
        {
        }

        [System.Xml.Serialization.XmlIgnore()]
        public virtual Dictionary<string, object> Items
        {
            get { return null; }
        }

        public static bool operator ==(MxBusinessObject a, MxBusinessObject b)
        {
            if ((object)a == null && (object)b == null) return true;
            if ((object)a == null || (object)b == null) return false;

            foreach (string key in a.Items.Keys)
            {
                if (a.Items[key] == null && b.Items[key] == null) continue;
                if (a.Items[key] == null) return false;
                if (b.Items[key] == null) return false;
                if (!a.Items[key].Equals(b.Items[key])) return false;
            }
            return true;
        }

        public static bool operator !=(MxBusinessObject a, MxBusinessObject b)
        {
            return !(a == b);
        }

        protected static Dictionary<string, List<string>> PropertyDependencies(Type type)
        {
            if ((_propertyDependencies == null))
            {
                SetupPropertyDependencies(type);
            }
            return _propertyDependencies;
        }

        private static void SetupPropertyDependencies(Type type)
        {
            // turn around the Dependencies into dictionary of Dependee, List (of Dependors)
            // e.g given the following declarations:
            // <DependsOn("A", "G")> _
            // property B() As string
            // 
            // end property
            //
            // <DependsOn("A")> _
            // property C() As string
            // 
            // end property ' 
            //
            // A key will be added into the dictionary as follows: 
            // Entry: Key="A", Value="B","C"
            // Entry: Key="G", Value="B"


            _propertyDependencies = new Dictionary<string, List<string>>();

            foreach (PropertyInfo pi in type.GetProperties(BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public))
            {
                object[] attributes = pi.GetCustomAttributes(typeof(DependsOnAttribute), true);

                if ((attributes.Length > 0))
                {
                    DependsOnAttribute dependsOn = (DependsOnAttribute)attributes[0];

                    foreach (string dependor in dependsOn.DependsOn)
                    {
                        if ((!_propertyDependencies.ContainsKey(dependor)))
                        {
                            _propertyDependencies.Add(dependor, new List<string>());
                        }
                        _propertyDependencies[dependor].Add(pi.Name);

                    }

                }
            }
        }
    }
}