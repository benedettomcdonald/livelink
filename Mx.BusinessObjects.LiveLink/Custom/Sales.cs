﻿using System;
using MMS.Utilities;

namespace Mx.BusinessObjects.LiveLink
{
    public enum SalesRecordType : short
    {
        [EnumClassName("H")]Header = 0,
        [EnumClassName("I")]Item = 1,
        [EnumClassName("IM")]ItemModifier = 2,
        [EnumClassName("F")]Financial = 3,
        [EnumClassName("C")]Control = 4,
        [EnumClassName("S")]SpeedOfService = 5,
        [EnumClassName("M")]LoyaltyMember = 6,
        [EnumClassName("T")]TimeAndAttendance = 7,
        [EnumClassName("P")]Product = 8,
        [EnumClassName("V")]Inventory = 9,
        [EnumClassName("SI")]Storeinformation = 10,
        [EnumClassName("TLD")]CompressedXML = 11,
        [EnumClassName("FUEL")]FUEL = 12,
        [EnumClassName("ALERT")]Alert = 13,
        [EnumClassName("VALIDATION")]VALIDATION = 14,
        [EnumClassName("IV")]ItemVoid = 15,
    }

    public enum HeaderRecordSubType : short
    {
        // Newly Added
        [EnumClassName("Sales")]Sales = 0,
        [EnumClassName("FinancialEvent")]FinancialEvent = 1,
        [EnumClassName("OtherEvent")]OtherEvent = 2,
        [EnumClassName("FuelEvent")]FuelEvent = 3,
        [EnumClassName("ProductEvent")]ProductEvent = 4,
        [EnumClassName("OperatorEvent")]OperatorEvent = 5,
        [EnumClassName("DayEvent")]DayEvent = 6,
        [EnumClassName("SOS")]SOS = 7,
        [EnumClassName("ReverseSales")]ReverseSales = 8,
    }

    public enum ItemRecordSubType : short
    {
        // Newly Added
        [EnumClassName("Combo")]Combo = 0,
        [EnumClassName("Item")]Item = 1,
        [EnumClassName("Condiment")]Condiment = 2,
        [EnumClassName("Modifier")]Modifier = 3,
        [EnumClassName("Text")]Text = 4,
        [EnumClassName("ContainerDeposit")]ContainerDeposit = 5,
        [EnumClassName("NonSalesItem")]NonSalesItem = 6,
        [EnumClassName("MoneyOrder")]MoneyOrder = 7,
        [EnumClassName("Fuel")]Fuel = 8,
        [EnumClassName("Component")]Component = 9,
    }

    public enum FinancialRecordSubType : short
    {
        [EnumClassName("Discount")]Discount = 0,
        [EnumClassName("Free")]Free = 1,
        [EnumClassName("Compliments")]Compliments = 2,
        [EnumClassName("Promo")]Promo = 3,
        [EnumClassName("Payment")]Payment = 4,
        [EnumClassName("Rounding")]Rounding = 5,
        [EnumClassName("Tax")]Tax = 6,
        [EnumClassName("Refunds")]Refunds = 7,
        [EnumClassName("ItemVoid")]ItemVoid = 8,
        [EnumClassName("Tips")]Tips = 9,
        [EnumClassName("Training")]Training = 10,
        [EnumClassName("Waste")]Waste = 11,
        [EnumClassName("DeletionBefore")]DeletionBefore = 12,
        [EnumClassName("DeletionAfter")]DeletionAfter = 13,
        [EnumClassName("Covers")]Covers = 14,
        [EnumClassName("DrawerSkim")]DrawerSkim = 15,
        [EnumClassName("PaidOut")]PaidOut = 16,
        [EnumClassName("PaidIn")]PaidIn = 17,
        // Newly Added
        [EnumClassName("DrawerLoan")]DrawerLoan = 18,
        [EnumClassName("BankDeposit")]BankDeposit = 19,
        [EnumClassName("Coupon")]Coupon = 20,
        [EnumClassName("ServiceCharge")]ServiceCharge = 21,
        [EnumClassName("DebitFee")]DebitFee = 22,
        [EnumClassName("CashBack")]CashBack = 23,
        [EnumClassName("ItemDiscount")]ItemDiscount = 24,
        [EnumClassName("ItemCoupon")]ItemCoupon = 25,
        [EnumClassName("OrderDiscount")]OrderDiscount = 26,
        [EnumClassName("OrderCoupon")]OrderCoupon = 27,
        [EnumClassName("EFTDetail")]EFTDetail = 28,
        [EnumClassName("DeclaredTips")]DeclaredTips = 29,
        [EnumClassName("CancelOrder")]CancelOrder = 30,
        [EnumClassName("TaxExempt")]TaxExempt = 31,
        [EnumClassName("Counts")]Counts = 32,
        [EnumClassName("DeletionAfterTender")]DeletionAfterTender = 33,
        [EnumClassName("RetrieveandRefund")]RetrieveandRefund = 34,
        // Add new financial subtype for eod checksum
        [EnumClassName("Checksum")]Checksum = 35,
        [EnumClassName("ChecksumDebitTotal")]ChecksumDebitTotal = 36,
        [EnumClassName("ChecksumTranCount")]ChecksumTranCount = 37,
        [EnumClassName("ChecksumSummary")]ChecksumSummary = 38,
        [EnumClassName("GiftCardSold")]GiftCardSold = 39,
        [EnumClassName("UnfinalizedOrder")]UnfinalizedOrder = 40,
        [EnumClassName("Batch")]Batch = 41,
        [EnumClassName("DriveOff")]DriveOff = 42,
        [EnumClassName("PriceOverride")]PriceOverride = 43,
        [EnumClassName("SafeDrop")]SafeDrop = 44,
        [EnumClassName("SafeLoan")]SafeLoan = 45,
        [EnumClassName("CountDrawer")]CountDrawer = 46,
        [EnumClassName("GiftCardReload")]GiftCardReload = 47,
        [EnumClassName("GiftCardActivate")]GiftCardActivate = 48,
        [EnumClassName("OfflineCreditDecline")]OfflineCreditDecline = 49,
        [EnumClassName("AccountPaidIn")]AccountPaidIn = 50,
        [EnumClassName("EndofShiftCounts")]EndofShiftCounts = 51,
        [EnumClassName("ServiceType")]ServiceType = 52,
        [EnumClassName("TipsSummary")]TipsSummary = 53,
        [EnumClassName("Reduction")]Reduction = 54,
        [EnumClassName("CashierProductSales")]CashierProductSales = 55,
        [EnumClassName("CashierNonProductSales")]CashierNonProductSales = 56,
        [EnumClassName("CashierGiftCouponSales")]CashierGiftCouponSales = 57,
        [EnumClassName("CashierSalesTax")] CashierSalesTax = 58,
        [EnumClassName("Surcharge")] Surcharge = 59
    }

    public enum ControlRecordSubType : short
    {
        [EnumClassName("CashierOpen")]CashierOpen = 0,
        [EnumClassName("CashierClose")]CashierClose = 1,
        [EnumClassName("RegisterClose")]RegisterClose = 2,
        [EnumClassName("DayOpen")]DayOpen = 3,
        [EnumClassName("DayClose")]DayClose = 4,
        [EnumClassName("NoSale")]NoSale = 5,
        // Newly added
        [EnumClassName("ServerOpen")]ServerOpen = 6,
        [EnumClassName("ServerClose")]ServerClose = 7,
        [EnumClassName("CashierSignedOn")]CashierSignedOn = 8,
        [EnumClassName("CashierSignedOff")]CashierSignedOff = 9,
        [EnumClassName("ServerSignedOn")]ServerSignedOn = 10,
        [EnumClassName("ServerSignedOff")]ServerSignedOff = 11,
        [EnumClassName("ShiftClose")]ShiftClose = 12,
        [EnumClassName("BatchTran")]BatchTran = 13,
        [EnumClassName("SecurityOverride")]SecurityOverride = 14,
        [EnumClassName("OperatorShift")]OperatorShift = 15,
        [EnumClassName("ShiftOpen")]ShiftOpen = 16,
        [EnumClassName("RegisterOpen")]RegisterOpen = 17,
        [EnumClassName("FinalPickup")]FinalPickup = 18,
        [EnumClassName("ShiftValidate")]ShiftValidate = 19,
        [EnumClassName("OnAccountCustomer")]OnAccountCustomer = 20,
        [EnumClassName("CodeRegister")]CodeRegister = 21,
    }

    public enum SOSRecordSubType : short
    {
        [EnumClassName("OrderStart")]OrderStart = 0,
        [EnumClassName("FirstItem")]FirstItem = 1,
        [EnumClassName("OrderTotal")]OrderTotal = 2,
        [EnumClassName("Tendered")]OrderTender = 3,
        [EnumClassName("BumpTime")]KitOrderBump = 4,
        [EnumClassName("DriverDispatched")]DriverDispatched = 5,
        // Newly Added 
        [EnumClassName("DriverReturned")]DriverReturned = 6,
        [EnumClassName("OrderStore")]OrderStore = 7,
        [EnumClassName("OrderRecall")]OrderRecall = 8,
        [EnumClassName("DrawerClose")]DrawerClose = 9,
        [EnumClassName("KitchenArrive")]KitOrderArrive = 10,
        [EnumClassName("OrderCancel")]OrderCancel = 11,
    }

    public enum TAARecordSubType : short
    {
        // Newly Added
        [EnumClassName("S")]ClockIn = 0,
        [EnumClassName("E")]ClockOut = 1,
        [EnumClassName("B")]BreakIn = 2,
        [EnumClassName("K")]BreakOut = 3,
        [EnumClassName("ME")]MealIn = 4,
        [EnumClassName("MS")]MealOut = 5,
        [EnumClassName("N")]NewEmployee = 6,
        [EnumClassName("U")]UpdateEmployeeName = 7,
        [EnumClassName("T")]TerminateEmployee = 8,
        [EnumClassName("P")]Tips = 9,
        [EnumClassName("JT")]JobTransfer = 10,
    }

    public enum FuelEventRecordSubType : short
    {
        [EnumClassName("TankDip")]TankDip = 1,
        [EnumClassName("PumpTest")]PumpTest = 2,
        [EnumClassName("Delivery")]Delivery = 3,
        [EnumClassName("MeterReading")]MeterReading = 4,
        [EnumClassName("PriceChange")]PriceChange = 5,
        [EnumClassName("ManualMeterReading")]ManualMeterReading = 6,
    }

    public enum ProductRecordSubType : short
    {
        [EnumClassName("Production")]Production = 0,
        [EnumClassName("Waste")]Waste = 1,
        [EnumClassName("Stock")]Stock = 2,
        [EnumClassName("Sampling")]Sampling = 3,
    }

    public enum AlertRecordSubType : short
    {
        [EnumClassName("Information")]Information = 0,
        [EnumClassName("Warning")]Warning = 1,
        [EnumClassName("Error")]Error = 2,
    }

    public enum ValidationRecordSubType : short
    {
        [EnumClassName("DayValidate")]DayValidate = 0,
        [EnumClassName("ShiftValidate")]ShiftValidate = 1,
        [EnumClassName("CashierValidate")]CashierValidate = 2,
        [EnumClassName("RegisterValidate")]RegisterValidate = 3,
    }

    public enum ValidationDataType : short
    {
        [EnumClassName("NetSalesTotal")]NetSalesTotal = 0,
        [EnumClassName("TaxTotal")]TaxTotal = 1,
        [EnumClassName("DiscountTotal")]DiscountTotal = 2,
        [EnumClassName("RoundingTotal")]RoundingTotal = 3,
        [EnumClassName("CreditTotal")]CreditTotal = 4,
        [EnumClassName("ServiceTotal")]ServiceTotal = 5,
        [EnumClassName("VoidTotal")]VoidTotal = 6,
        [EnumClassName("ReturnTotal")]ReturnTotal = 7,
        [EnumClassName("ManagerVoidTotal")]ManagerVoidTotal = 8,
        [EnumClassName("ErrorCorrectTotal")]ErrorCorrectTotal = 9,
        [EnumClassName("CheckBeginTotal")]CheckBeginTotal = 10,
        [EnumClassName("CheckPaidTotal")]CheckPaidTotal = 11,
        [EnumClassName("GrandTotal")]GrandTotal = 12,
        [EnumClassName("GrossSalesTotal")]GrossSalesTotal = 13,
        [EnumClassName("Checksum")]Checksum = 14,
    }

    public enum LoyaltyMemberRecordSubType : short
    {
        [EnumClassName("FullName")]FullName = 0,
        [EnumClassName("FirstName")]FirstName = 1,
        [EnumClassName("LastName")]LastName = 2,
        [EnumClassName("Address1")]Address1 = 3,
        [EnumClassName("Address2")]Address2 = 4,
        [EnumClassName("Address3")]Address3 = 5,
        [EnumClassName("City")]City = 6,
        [EnumClassName("State")]State = 7,
        [EnumClassName("Zip")]Zip = 8,
        [EnumClassName("Country")]Country = 9,
        [EnumClassName("Phone")]Phone = 10,
        [EnumClassName("Fax")]Fax = 11,
        [EnumClassName("Email")]Email = 12,
        [EnumClassName("TaxID")]TaxID = 13,
        [EnumClassName("LoyaltyCardNumber")]LoyaltyCardNumber = 14,
        [EnumClassName("TaxExemptAccountName")]TaxExemptAccountName = 15,
    }

    // These SalesFlag enums are the same as the PosServiceType enums defined in Mx.BusinessObjects\Custom\MxEnums.vb
    // Update both place if new enum is added
    public enum SalesServiceType : short
    {
        [EnumClassName("Carry Out")]CarryOut = 0,
        [EnumClassName("Pick Up")]PickUp = 1,
        [EnumClassName("Delivery")]Delivery = 2,
        [EnumClassName("Eat In")]EatIn = 3,
        [EnumClassName("Drive Thru")]DriveThru = 4,
        [EnumClassName("Catering")]Catering = 5,
        [EnumClassName("Online Pickup")]OnlinePickup = 6,    // Used for orders placed online and picked up
        [EnumClassName("Online Delivery")]OnlineDelivery = 7,  // Used for orders placed online and delivered
        [EnumClassName("All")]AllNotUsed = 9,
        [EnumClassName("Kiosk Dine In")]KioskDineIn = 10,   // service type for mobile kiosk dine in
        [EnumClassName("Kiosk Take Out")]KioskTakeOut = 11,   // service type for mobile kiosk take out
        [EnumClassName("Full Service")]FullService = 12,
        [EnumClassName("Alt Tax Jurisdiction")]AltTaxJurisdiction = 13,
        [EnumClassName("Mobile Outside")]MobileOutside = 14,
        [EnumClassName("Mobile Dine In")]MobileDineIn = 15,
        [EnumClassName("Mobile Carry Out")]MobileCarryOut = 16,
        [EnumClassName("Mobile")]Mobile = 17,
        [EnumClassName("Custom1")]Custom1 = 18,
        [EnumClassName("Custom2")]Custom2 = 19,
        [EnumClassName("Custom3")]Custom3 = 20,
        [EnumClassName("Custom4")]Custom4 = 21,
        [EnumClassName("Custom5")]Custom5 = 22,
        [EnumClassName("Custom6")]Custom6 = 23,
        [EnumClassName("Custom7")]Custom7 = 24,
        [EnumClassName("Custom8")]Custom8 = 25,
        [EnumClassName("Custom9")]Custom9 = 26,
        [EnumClassName("Custom10")]Custom10 = 27
    }

    public partial class Sales
    {
        // Member varible to save relationship among items within an transaction. 
        public long ItemNumber { get; set; }
        public long ParentItemNumber { get; set; }

        public Sales(long salesMainID, string recordType, long transactionID, string recordSubType, int entityID, int registerID, string pollDate, double pollCount, decimal pollAmount, int clerkID, string clerkName, string customerID, int synced, long pLUCodeID, string pLUCode, int sequenceNo, int flag, int applyTax, decimal itemTax, string priceLevel, DateTime dateAdded, string subTypeDescription, long pOSTransactionID, int transactionVersion, string syncToken, long parentId, DateTime businessDate, long itemNumber, long parentItemNumber)
            : this(salesMainID, recordType, transactionID, recordSubType, entityID, registerID, pollDate, pollCount, pollAmount, clerkID, clerkName, customerID, synced, pLUCodeID, pLUCode, sequenceNo, flag, applyTax, itemTax, priceLevel, dateAdded, subTypeDescription, pOSTransactionID, transactionVersion, syncToken, parentId, businessDate, null)
        {
            ItemNumber = itemNumber;
            ParentItemNumber = parentItemNumber;
        }

        public Sales(long salesMainID, string recordType, long transactionID, string recordSubType, int entityID, int registerID, string pollDate, double pollCount, decimal pollAmount, int clerkID, string clerkName, string customerID, int synced, long pLUCodeID, string pLUCode, int sequenceNo, int flag, int applyTax, decimal itemTax, string priceLevel, DateTime dateAdded, string subTypeDescription, long pOSTransactionID, int transactionVersion, string syncToken, long parentId, DateTime businessDate)
            : this(salesMainID, recordType, transactionID, recordSubType, entityID, registerID, pollDate, pollCount, pollAmount, clerkID, clerkName, customerID, synced, pLUCodeID, pLUCode, sequenceNo, flag, applyTax, itemTax, priceLevel, dateAdded, subTypeDescription, pOSTransactionID, transactionVersion, syncToken, parentId, businessDate, null)
        {
        }

        public static string GetRecordType<T>(T recordType)
        {
            return EnumUtils<T>.GetClassName(recordType);
        }
    }

    public partial class SalesList : Mx.Common.Collections.ValidatableList<Sales>
    {
        public int EntityID { set; get; }
        public DateTime BusinessDate { set; get; }
        public int RegisterID { set; get; }
        public long TransactionID { set; get; }
        public int SequenceNo { set; get; }

        public bool UpdateParentId()
        {
            var updated = false;
            foreach (Sales salesRecord1 in this)
            {
                if (salesRecord1.ParentItemNumber != 0)
                {
                    foreach (Sales salesRecord2 in this)
                    {
                        if (salesRecord1.ParentItemNumber == salesRecord2.ItemNumber)
                        {
                            // make sure valid parent and child
                            if (salesRecord2.RecordType == EnumUtils<SalesRecordType>.GetClassName(SalesRecordType.Item) &&
                                (salesRecord1.RecordType == EnumUtils<SalesRecordType>.GetClassName(SalesRecordType.Item) ||
                                 salesRecord1.RecordType == EnumUtils<SalesRecordType>.GetClassName(SalesRecordType.ItemModifier) ||
                                 salesRecord1.RecordType == EnumUtils<SalesRecordType>.GetClassName(SalesRecordType.Financial)))
                            {
                                salesRecord1.ParentId = salesRecord2.SalesMainID;
                                updated = true;
                            }
                            else if (salesRecord2.RecordType == EnumUtils<SalesRecordType>.GetClassName(SalesRecordType.ItemModifier) &&
                                    (salesRecord1.RecordType == EnumUtils<SalesRecordType>.GetClassName(SalesRecordType.ItemModifier) ||
                                     salesRecord1.RecordType == EnumUtils<SalesRecordType>.GetClassName(SalesRecordType.Financial)))
                            {
                                salesRecord1.ParentId = salesRecord2.SalesMainID;
                                updated = true;
                            }
                            break;
                        }
                    }
                }
            }
            return updated;
        }

        public void SetVersionNumber(int versionNumber)
        {
            foreach (Sales salesRecord in this)
            {
                salesRecord.TransactionVersion = versionNumber;
            }
        }

        public void SetSyncedStatus(int syncedStatus)
        {
            foreach (Sales salesRecord in this)
            {
                salesRecord.Synced = syncedStatus;
            }
        }

        public void AddSalesRecord(Sales salesRecord)
        {
            if (salesRecord != null)
            {
                EntityID = salesRecord.EntityID;
                BusinessDate = salesRecord.BusinessDay;
                RegisterID = salesRecord.RegisterID;
                TransactionID = salesRecord.TransactionID;

                Add(salesRecord);
            }
        }

        public void AddNoneZero(Sales salesRecord)
        {
            if (salesRecord != null &&
                (salesRecord.PollCount != 0 || salesRecord.PollAmount != 0))
            {
                Add(salesRecord);
            }
        }

    }
}
