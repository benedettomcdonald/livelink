﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

namespace Mx.BusinessObjects.LiveLink
{
    public enum SessionCommandStatus : int
    {
        Executing = 0,
        Success = 1,
        Error = 2,
        Retry = 3,
        CompletedSuccess = 4,
        CompletedError = 5,
        ErrorContinue = 6,
        Unknown = 7,
    }

    public partial class SessionCommandResponse
    {
        private const string _errorXMLFormat = "<Errors><Message><![CDATA[{0}]]></Message></Errors>";
        private const string _informationXMLFormat = "<Information><Message><![CDATA[{0}]]></Message></Information>";

        public void SetResponseXML(string format, params object[] args)
        {
            string xmlFormat = string.Empty;
            switch ((SessionCommandStatus)_status)
            {
                case SessionCommandStatus.CompletedError:
                case SessionCommandStatus.Error:
                    xmlFormat = _errorXMLFormat;
                    break;

                default:
                    xmlFormat = _informationXMLFormat;
                    break;
            }

            // create the response string, setting the CDATA message inside the <Error> or <Information> tab
            _response = string.Format(xmlFormat,string.Format(format,args));
        }
    }
}
