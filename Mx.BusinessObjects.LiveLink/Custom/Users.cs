﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mx.BusinessObjects.LiveLink
{
    public partial class Users
    {
        public string FullName
        {
            get { return FirstName + " " + LastName; }
        }
    }
}
