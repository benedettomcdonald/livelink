


#Region "Event Arg Classes"


Public Class ProcessDateEventArgs
    Inherits EventArgs

    Private _importType As LiveLinkImportType
    Private _processDate As DateTime

    Public ReadOnly Property ProcessDate() As DateTime
        Get
            Return _processDate
        End Get
    End Property

    Public ReadOnly Property ImportType() As LiveLinkImportType
        Get
            Return _importType
        End Get
    End Property

    Public Sub New(ByVal processDate As DateTime)
        _processDate = processDate
    End Sub

    Public Sub New(ByVal importType As LiveLinkImportType, ByVal processDate As DateTime)
        _importType = importType
        _processDate = processDate
    End Sub

End Class

#End Region


