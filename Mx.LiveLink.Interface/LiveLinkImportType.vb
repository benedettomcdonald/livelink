Imports MMS.Utilities

Public Enum LiveLinkImportType
    <EnumClassName("Aloha")> Aloha
    <EnumClassName("Compris")> Compris
    <EnumClassName("Compris PDP Totals")> ComprisPDP
    <EnumClassName("Compris PDP Updates")> ComprisPDPUpdates
    <EnumClassName("Compris Poll Files")> ComprisPollFiles
    <EnumClassName("ICG")> ICG
    <EnumClassName("Intouch")> Intouch
    <EnumClassName("iPOS")> iPOS
    <EnumClassName("iPOS Staff Updates")> iPOSStaff
    <EnumClassName("iPOS Product Updates")> iPOSProduct
    <EnumClassName("Micros")> Micros
    <EnumClassName("Microsoft RMS")> MicrosoftRMS
    <EnumClassName("New POS")> NewPOS
    <EnumClassName("Octane")> Octane
    <EnumClassName("Offline Cashup Send")> OfflineCashupSend
    <EnumClassName("Offline Cashup Settings")> OfflineCashupSettings
    <EnumClassName("Offline Cashup Info")> OfflineCashupInfo
    <EnumClassName("PAR Exalt 4")> PARExalt4
    <EnumClassName("Panasonic")> Panasonic
    <EnumClassName("POSI")> POSI
    <EnumClassName("Radiant")> Radiant
    <EnumClassName("Uniwell")> Uniwell
    <EnumClassName("Service Report")> ServiceReport
    <EnumClassName("ZK Fingerprint")> ZKFingerprint
    <EnumClassName("ZK Firmware")> ZKFirmware
    <EnumClassName("YIS")> YIS
    <EnumClassName("POSIXML")> POSixml
    <EnumClassName("Xpient")> Xpient
    <EnumClassName("RPOS")> RPOS
    <EnumClassName("IntouchMenuPackage")> IntouchMenuPackage
    <EnumClassName("Transight")> Transight
    <EnumClassName("NewPOSTLD")> NewPOSTLD
    <EnumClassName("TimePunchImport")> TimePunchImport
    <EnumClassName("NewPOSSummary")> NewPOSSummary
	<EnumClassName("SUS")> SUS
    <EnumClassName("HomeService")> HomeService
    <EnumClassName("STM")> STM
    <EnumClassName("AlertSystemsTrafficData")> AlertSystemsTrafficData
End Enum
