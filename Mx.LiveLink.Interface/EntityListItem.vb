Public Class EntityListItem
    Private _entityId As Integer
    Private _entity As String

    Public Property EntityId() As Integer
        Get
            Return _entityId
        End Get
        Set(ByVal value As Integer)
            _entityID = value
        End Set
    End Property

    Public Property Entity() As String
        Get
            Return _entity
        End Get
        Set(ByVal value As String)
            _entity = value
        End Set
    End Property

    Public Sub New(ByVal EntityId As Integer, ByVal Entity As String)
        Me._entityId = EntityId
        Me._entity = Entity
    End Sub

    Public Overrides Function ToString() As String
        Return Me._entity
    End Function
End Class