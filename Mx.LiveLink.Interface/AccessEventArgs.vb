Public Class AccessEventArgs
    Inherits EventArgs

    Private _access As Boolean

    Public ReadOnly Property Access() As Boolean
        Get
            Return _access
        End Get
    End Property

    Public Sub New(ByVal access As Boolean)
        _access = access
    End Sub
End Class