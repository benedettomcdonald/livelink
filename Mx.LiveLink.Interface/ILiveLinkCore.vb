Imports System.Net

Public Interface ILiveLinkCore

    Event RestartEvent(ByVal sender As Object, ByVal args As EventArgs)
    Event ShutdownEvent(ByVal sender As Object, ByVal args As EventArgs)
    Event VersionUpdateEvent(ByVal sender As Object, ByVal updateArgs As VersionUpdateEventArgs)

    Event PingStatusEvent(ByVal sender As Object, ByVal pingArgs As PingEventArgs)
    Event ImportBeginEvent(ByVal sender As Object, ByVal importArgs As ImportEventArgs)
    Event ImportMessageEvent(ByVal sender As Object, ByVal importArgs As ImportEventArgs)
    Event ImportCompleteEvent(ByVal sender As Object, ByVal importArgs As ImportEventArgs)
    Event SendBeginEvent(ByVal sender As Object, ByVal args As EventArgs)
    Event SendCompleteEvent(ByVal sender As Object, ByVal args As EventArgs)
    Event ProcessDateChangedEvent(ByVal sender As Object, ByVal args As ProcessDateEventArgs)
    Event AccessEvent(ByVal sender As Object, ByVal args As AccessEventArgs)
    Event FlowChangedEvent(ByVal sender As Object, ByVal args As EventArgs)

    ' readonly properties
    ReadOnly Property Initialised() As Boolean
    ReadOnly Property LiveLinkID() As String
    ReadOnly Property SessionKey() As String
    ReadOnly Property StoreList() As List(Of EntityListItem)
    ReadOnly Property ImportConfigurationList() As Dictionary(Of LiveLinkImportType, ILiveLinkImportConfiguration)
    ReadOnly Property ImportEnabled(ByVal importType As LiveLinkImportType) As Boolean
    ReadOnly Property FormConfigurationList() As Dictionary(Of LiveLinkFormType, ILiveLinkFormConfiguration)
    ReadOnly Property FormEnabled(ByVal formType As LiveLinkFormType) As Boolean
    ReadOnly Property Proxy() As IWebProxy
    ReadOnly Property EntityName() As String

    ' read & write properties
    Property EntityID() As Long
    Property PingEnabled() As Boolean
    Property ProcessDate() As DateTime
    Property FlowInterval() As Integer
    Property AwakeInterval() As Integer

    Sub DelayedInitialiseAutoStart()
    Function Initialise() As Boolean
    Sub Start()
    Sub [Stop]()

    Sub CancelProcesing()
    Sub ForceProcessing()
    Sub SendData()
    Sub SendSystemInfo()
    Sub RetrieveMessage()
    Sub RefreshEntityID()
    Sub RefreshConfiguration()
    Function RecreateLiveLinkID() As Boolean

    Sub ForceImport()
    Sub ForceImport(ByVal importType As LiveLinkImportType)
    Sub ToggleImportEnabled(ByVal importType As LiveLinkImportType, ByVal enabled As Boolean)
    Sub LoadForm(ByVal formType As LiveLinkFormType)

End Interface