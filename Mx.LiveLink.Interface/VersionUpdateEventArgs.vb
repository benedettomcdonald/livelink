Public Class VersionUpdateEventArgs
    Inherits EventArgs

    Private _livelinkPath As String
    Private _url As String
    Private _hash As String

    Public ReadOnly Property URL() As String
        Get
            Return _url
        End Get
    End Property
    Public ReadOnly Property Hash() As String
        Get
            Return _hash
        End Get
    End Property
    Public ReadOnly Property LiveLinkPath() As String
        Get
            Return _livelinkPath
        End Get
    End Property

    Public Sub New(ByVal url As String, ByVal hash As String, ByVal liveLinkPath As String)
        _url = url
        _hash = hash
        _livelinkPath = liveLinkPath
    End Sub
End Class