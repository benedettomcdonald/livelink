Imports MMS.Utilities

Public Enum LiveLinkFormType
    <EnumClassName("Easy Clock")> EasyClock
    <EnumClassName("YIS Controller")> YISController
    <EnumClassName("ZK Users")> ZKFingerprintUsers
    <EnumClassName("Compris ActiveX")> ComprisActiveX
End Enum