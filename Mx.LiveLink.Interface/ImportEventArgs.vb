Public Class ImportEventArgs
    Inherits EventArgs

    Private _importType As LiveLinkImportType
    Private _message As String

    Public ReadOnly Property ImportType() As LiveLinkImportType
        Get
            Return _importType
        End Get
    End Property

    Public ReadOnly Property Message() As String
        Get
            Return _message
        End Get
    End Property

    Public Sub New(ByVal importType As LiveLinkImportType)
        _importType = importType
        _message = String.Empty
    End Sub

    Public Sub New(ByVal importType As LiveLinkImportType, ByVal message As String)
        _importType = importType
        _message = message
    End Sub

End Class