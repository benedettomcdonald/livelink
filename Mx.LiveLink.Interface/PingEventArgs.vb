Public Class PingEventArgs
    Inherits EventArgs

    Private _pingStatus As Boolean
    Private _message As String

    Public ReadOnly Property PingStatus() As Boolean
        Get
            Return _pingStatus
        End Get
    End Property

    Public ReadOnly Property Message() As String
        Get
            Return _message
        End Get
    End Property


    Public Sub New(ByVal pingStatus As Boolean)
        _pingStatus = pingStatus
        _message = String.Empty
    End Sub

    Public Sub New(ByVal pingStatus As Boolean, ByVal message As String)
        _pingStatus = pingStatus
        _message = message
    End Sub

End Class