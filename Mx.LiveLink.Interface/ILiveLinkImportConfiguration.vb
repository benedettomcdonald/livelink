Public Interface ILiveLinkImportConfiguration

    Event StatusMessageEvent(ByVal sender As Object, ByVal args As ImportEventArgs)
    Event ProcessDateUpdateEvent(ByVal sender As Object, ByVal args As ProcessDateEventArgs)
    ReadOnly Property ImportType() As LiveLinkImportType
    ReadOnly Property Enabled() As Boolean
    ReadOnly Property PollInterval() As Integer
    Property LastPoll() As DateTime
    Property FirstPollComplete() As Boolean
    Property GatewayEnabled() As Boolean
    Sub SetStatusMessage(ByVal status As String)
    Sub SetProcessDate(ByVal processDate As DateTime)
    Function Start(ByVal entityID As Integer, ByVal sessionKey As String, ByVal identity As String) As Boolean
    Function [Stop](ByVal entityID As Integer) As Boolean
    Function KeepAlive(ByVal entityID As Integer) As Boolean

End Interface
