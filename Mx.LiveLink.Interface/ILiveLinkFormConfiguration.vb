Option Strict On
Option Explicit On

Imports System.Threading

Public Interface ILiveLinkFormConfiguration

    ReadOnly Property FormType() As LiveLinkFormType
    ReadOnly Property Enabled() As Boolean
    ReadOnly Property AutoShow() As Boolean

    Property AutoShowComplete() As Boolean
    Property FormThread() As Thread
    
    Sub LoadForm(ByVal entityID As Long, ByVal entity As String, ByVal sessionKey As String, ByVal identity As String)

End Interface
