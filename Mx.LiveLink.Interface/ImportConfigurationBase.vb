Option Strict On
Option Explicit On

Public MustInherit Class ImportConfigurationBase
    Implements ILiveLinkImportConfiguration



#Region "Protected Variables"

    Protected _importType As LiveLinkImportType
    Protected _enabled As Boolean
    Protected _gatewayEnabled As Boolean
    Protected _firstPollComplete As Boolean
    Protected _lastPoll As DateTime
    Protected _pollInterval As Integer

#End Region

#Region "Public Interface Properties"


    Public Event ProcessDateUpdateEvent(ByVal sender As Object, ByVal args As ProcessDateEventArgs) Implements ILiveLinkImportConfiguration.ProcessDateUpdateEvent
    Public Event StatusMessageEvent(ByVal sender As Object, ByVal args As ImportEventArgs) Implements ILiveLinkImportConfiguration.StatusMessageEvent

    Public ReadOnly Property ImportType() As LiveLinkImportType Implements ILiveLinkImportConfiguration.ImportType
        Get
            Return _importType
        End Get
    End Property

    Public ReadOnly Property Enabled() As Boolean Implements ILiveLinkImportConfiguration.Enabled
        Get
            Return _enabled
        End Get
    End Property

    Public ReadOnly Property PollInterval() As Integer Implements ILiveLinkImportConfiguration.PollInterval
        Get
            Return _pollInterval
        End Get
    End Property

    Public Property FirstPollComplete() As Boolean Implements ILiveLinkImportConfiguration.FirstPollComplete
        Get
            Return _firstPollComplete
        End Get
        Set(ByVal value As Boolean)
            _firstPollComplete = value
        End Set
    End Property

    Public Property GatewayEnabled() As Boolean Implements ILiveLinkImportConfiguration.GatewayEnabled
        Get
            Return _gatewayEnabled
        End Get
        Set(ByVal value As Boolean)
            _gatewayEnabled = value
        End Set
    End Property

    Public Property LastPoll() As DateTime Implements ILiveLinkImportConfiguration.LastPoll
        Get
            Return _lastPoll
        End Get
        Set(ByVal value As Date)
            _lastPoll = value
        End Set
    End Property

#End Region

#Region "Constructors & Interface Functions"

    Public Sub New()
        _firstPollComplete = False
        _lastPoll = Nothing
    End Sub

    Public Sub SetStatusMessage(ByVal status As String) Implements ILiveLinkImportConfiguration.SetStatusMessage
        RaiseEvent StatusMessageEvent(Me, New ImportEventArgs(_importType, status))
    End Sub

    Public Sub SetProcessDate(ByVal processDate As Date) Implements ILiveLinkImportConfiguration.SetProcessDate
        RaiseEvent ProcessDateUpdateEvent(Me, New ProcessDateEventArgs(_importType, processDate))
    End Sub

    Public Overridable Function Start(ByVal entityID As Integer, ByVal sessionKey As String, ByVal identity As String) As Boolean Implements ILiveLinkImportConfiguration.Start

    End Function

    Public Overridable Function [Stop](ByVal entityID As Integer) As Boolean Implements ILiveLinkImportConfiguration.[Stop]

    End Function

    Public Overridable Function KeepAlive(ByVal entityID As Integer) As Boolean Implements ILiveLinkImportConfiguration.KeepAlive

    End Function

#End Region


End Class


