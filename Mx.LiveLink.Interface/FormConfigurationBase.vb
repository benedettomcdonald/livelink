Option Strict On
Option Explicit On

Imports System.Threading

Public MustInherit Class FormConfigurationBase
    Implements ILiveLinkFormConfiguration


#Region "Protected Variables"

    Protected _formType As LiveLinkFormType
    Protected _enabled As Boolean
    Protected _autoShow As Boolean
    Protected _autoShowComplete As Boolean
    Protected _formThread As Thread

#End Region

#Region "Public Properties"

    Public Property AutoShowComplete() As Boolean Implements ILiveLinkFormConfiguration.AutoShowComplete
        Get
            Return _autoShowComplete
        End Get
        Set(ByVal value As Boolean)
            _autoShowComplete = value
        End Set
    End Property

    Public ReadOnly Property AutoShow() As Boolean Implements ILiveLinkFormConfiguration.AutoShow
        Get
            Return _autoShow
        End Get
    End Property

    Public ReadOnly Property Enabled() As Boolean Implements ILiveLinkFormConfiguration.Enabled
        Get
            Return _enabled
        End Get
    End Property

    Public ReadOnly Property FormType() As LiveLinkFormType Implements ILiveLinkFormConfiguration.FormType
        Get
            Return _formType
        End Get
    End Property

    Public Property FormThread() As System.Threading.Thread Implements ILiveLinkFormConfiguration.FormThread
        Get
            Return _formThread
        End Get
        Set(ByVal value As System.Threading.Thread)
            _formThread = value
        End Set
    End Property
#End Region

    ' must override this function to load the required form
    Public MustOverride Sub LoadForm(ByVal entityID As Long, ByVal entity As String, ByVal sessionKey As String, ByVal identity As String) Implements ILiveLinkFormConfiguration.LoadForm

    Protected Sub StartFormThread(ByVal threadStarter As ThreadStart)
        If (threadStarter Is Nothing) Then
            Return
        End If
        ' Create a new thread to run the form from. This will make it still available when LiveLink is blocking at any point
        _formThread = New Thread(threadStarter)
        _formThread.SetApartmentState(ApartmentState.STA)
        _formThread.Name = _formType.ToString()
        _formThread.Start()
    End Sub

End Class
