﻿using System;
using System.Collections.Generic;
using System.Text;

using MMS.Utilities;
using Mx.Data.LiveLink;
using Mx.BusinessObjects.LiveLink;

namespace Mx.Services.LiveLink
{
    public static class LiveSyncService
    {
        public enum SyncStatus : byte
        {
            Unsynced=0,
            Synced=1,
            Error=2,
        }

        public static LiveSyncList ListUnsynced()
        {
            return LiveSyncData.SelectUnsynced();
        }

        public static bool UpdateSynced(int liveSyncID, SyncStatus synced)
        {
            int recordsAffected = LiveSyncData.UpdateSynced(liveSyncID, (byte)synced);
            if (recordsAffected <= 0)
                return false;
            return true;
        }

        public static void PurgeOld(int daysBeforeObsolete)
        {
            LiveSyncData.PurgeOld(DateTime.Now.AddDays(-daysBeforeObsolete));
        }

        public static bool Insert(LiveSync syncData)
        {
            int recordsAffected = LiveSyncData.Insert(syncData.EntityID, syncData.Type, syncData.HeaderID);

            if (recordsAffected<=0)
                return false;
            return true;
        }
    }
}
