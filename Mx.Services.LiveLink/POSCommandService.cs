﻿using System.Collections.Generic;
using MMS.Utilities;
using Mx.BusinessObjects.LiveLink;
using Mx.Common;
using Mx.Data.LiveLink.Interfaces;

namespace Mx.Services.LiveLink
{
    public static class POSCommandService
    {
        public static POSCommandData GetDataByType(string type) { return GetDataByType(type, null); }
        public static POSCommandData GetDataByType(string type, string options)
        {
            return IocContainer.Resolve<IPOSCommandRepository>().GetDataByType(type, options);
        }

        public static bool InsertData(POSCommandData commandData)
        {
            return IocContainer.Resolve<IPOSCommandRepository>().InsertData(commandData);
        }

        public static bool DeleteObsoleteData(int daysBeforeObsolete, string type)
        {
            return IocContainer.Resolve<IPOSCommandRepository>().DeleteObsoleteData(daysBeforeObsolete, type);
        }

        public static POSCommandSettings GetSettingsByType(string type)
        {
            return IocContainer.Resolve<IPOSCommandRepository>().GetSettingsByType(type);
        }

        public static bool InsertSettings(POSCommandSettings commandSettings)
        {
            return IocContainer.Resolve<IPOSCommandRepository>().InsertSettings(commandSettings);
        }

        public static POSCommandType GetTypeByType(string type)
        {
            return IocContainer.Resolve<IPOSCommandRepository>().GetTypeByType(type);
        }

        public static bool InsertType(POSCommandType commandType)
        {
            return IocContainer.Resolve<IPOSCommandRepository>().InsertType(commandType);
        }

        public static string GetCommand(string type, List<string> commandDataTypeList)
        {
            POSCommandSettings settings = GetSettingsByType(type);
            if (settings == null)
            {
                MxLogger.LogError(MxLogger.LogCategory.MxClient, MxLogger.EventId.DataError, 0, "POSCommandSettings", null, "Failed to get command settings for type:[{0}]", type);
                return null;
            }

            if (string.IsNullOrEmpty(settings.CommandXML))
            {
                MxLogger.LogError(MxLogger.LogCategory.MxClient, MxLogger.EventId.DataError, 0, "POSCommandSettings", null, "Command XML for type:[{0}] is empty", type);
                return null;
            }

            foreach (string commandDataType in commandDataTypeList)
            {
                if (!settings.CommandXML.Contains(commandDataType))
                    continue;

                POSCommandData commandData = GetDataByType(commandDataType);
                if (commandData == null)
                {
                    MxLogger.LogError(MxLogger.LogCategory.MxClient, MxLogger.EventId.DataError, 0, "POSCommandData", null, "Failed to get command data of type:[{0}] for command type:[{1}]", commandDataType, type);
                    // on NO value, replace place holder with an empty string
                    settings.CommandXML = settings.CommandXML.Replace(commandDataType, string.Empty);
                    continue;
                }

                // get the data from either the command text or linked file
                string commandText = GetPOSCommandDataFromObject(commandData);
                if (commandText == null)
                {
                    MxLogger.LogError(MxLogger.LogCategory.MxClient, MxLogger.EventId.DataError, 0, "POSCommandData", null, "Failed to get internal command data of type:[{0}] for command type:[{1}]", commandDataType, type);
                    // on NO value, replace place holder with an empty string
                    settings.CommandXML = settings.CommandXML.Replace(commandDataType, string.Empty);
                    continue;
                }

                // replace any occurences of the command data type, with the stored data
                // e.g. replace [MXEMPLOYEEDATA] with the list of employee information
                settings.CommandXML = settings.CommandXML.Replace(commandDataType, commandText);
            }

            // return the command XML
            return settings.CommandXML;
        }

        public static string GetPOSCommandDataFromObject(POSCommandData commandData)
        {
            string commandText = string.Empty;
            if (!string.IsNullOrEmpty(commandData.CommandData))
            {
                // store the command data value
                commandText = commandData.CommandData;
            }
            else if (!string.IsNullOrEmpty(commandData.FilePath))
            {
                if (!System.IO.File.Exists(commandData.FilePath))
                {
                    MxLogger.LogWarning(MxLogger.LogCategory.MxClient, MxLogger.EventId.DataError, 0, "POSCommandData", null, "Command data of type:[{0}] reference file:[{1}] does not exist", commandData.Type, commandData.FilePath);
                    return null;
                }
                // read the entire content of the file
                using (System.IO.TextReader reader = new System.IO.StreamReader(commandData.FilePath))
                    commandText = reader.ReadToEnd();
            }
            else
            {
                MxLogger.LogWarning(MxLogger.LogCategory.MxClient, MxLogger.EventId.DataError, 0, "POSCommandData", null, "Command data of type:[{0}] is empty", commandData.Type);
                return null;
            }
            return commandText;
        }

        public static POSCommandData GetDataByLikeCommand(string options)
        {
            return IocContainer.Resolve<IPOSCommandRepository>().GetDataByLikeCommand(options);
        }
    }
}

