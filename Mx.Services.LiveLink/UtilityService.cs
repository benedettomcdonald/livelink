﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Mx.Services.LiveLink
{
    public class UtilityService
    {
        public static bool IsNumericString(string strText)
        {
            Regex objPattern = new Regex("[^0-9]");
            return !objPattern.IsMatch(strText) && (strText != "");
        }

        public static int GetIntID(string value)
        {
            int retVal = 0;
            if (int.TryParse(value, out retVal))
            {
                return retVal;
            }
            else
            {
                return 0;
            }
        }

        public static long GetLongID(string value)
        {
            long retVal = 0;
            if (long.TryParse(value, out retVal))
            {
                return retVal;
            }
            else
            {
                return 0;
            }
        }
    }
}
