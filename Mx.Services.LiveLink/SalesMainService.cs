﻿using System;
using System.Collections.Generic;
using System.Text;
using Mx.Data.LiveLink;
using Mx.BusinessObjects.LiveLink;
using Mx.Common.Data;
using MMS.Utilities;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;

namespace Mx.Services.LiveLink
{
    public class SalesMainService
    {
        public static int DeleteSyncedByBusinessDay(DateTime businessDay, int deleteCount)
        {
            return SalesMain.DeleteSyncedByBusinessDay(businessDay, deleteCount);
        }

        public static int GetRecordCount()
        {
            return SalesMain.GetRecordCount_Scalar();
        }

        public static int GetUnsyncedRecordCount()
        {
            return SalesMain.GetUnsyncedRecordCount_Scalar();
        }

        public static int UpdateSalesMain(Sales sales, SqlTransaction transaction, int timeoutValue)
        {
            return SalesMain.UpdateSalesMain(sales.SalesMainID,
                                             sales.RecordType,
                                             sales.TransactionID,
                                             sales.RecordSubType,
                                             sales.EntityID,
                                             sales.RegisterID,
                                             sales.PollDate,
                                             sales.PollCount,
                                             sales.PollAmount,
                                             sales.ClerkID,
                                             sales.ClerkName,
                                             sales.CustomerID,
                                             sales.Synced,
                                             sales.PLUCodeID,
                                             sales.PLUCode,
                                             sales.SequenceNo,
                                             sales.Flag,
                                             sales.ApplyTax,
                                             sales.ItemTax,
                                             sales.PriceLevel,
                                             sales.DateAdded,
                                             sales.SubTypeDescription,
                                             sales.POSTransactionID,
                                             sales.TransactionVersion,
                                             sales.ParentId,
                                             sales.BusinessDay,
                                             sales.ItemDiscount,
                                             sales.SyncToken,
                                             transaction);
        }
        
        public static int UpdateSalesMain(Sales sales, SqlTransaction transaction)
        {
            return UpdateSalesMain(sales, transaction, 0);
        }
        
        public static int UpdateSalesMain(Sales sales)
        {
            return UpdateSalesMain(sales, null, 0);
        }

        public static int UpdateParentId(Sales sales)
        {
            return UpdateParentId(sales, null);
        }

        public static int UpdateParentId(Sales sales, SqlTransaction transaction)
        {
            return SalesMain.UpdateParentId(sales.SalesMainID, sales.ParentId, transaction);
        }

        public static int UpdateParentId(SalesList salesList, SqlTransaction transaction)
        {
            int recordsAffected = 0;

            foreach (Sales sales in salesList)
            {
                recordsAffected += UpdateParentId(sales, transaction);
            }

            return recordsAffected;
        }

        public static int InsertSalesMain(out long? salesMainID, Sales sales, SqlTransaction transaction)
        {
            return SalesMain.InsertSalesMain(out salesMainID,
                                   sales.RecordType,
                                   sales.TransactionID,
                                   sales.RecordSubType,
                                   sales.EntityID,
                                   sales.RegisterID,
                                   sales.PollDate,
                                   sales.PollCount,
                                   sales.PollAmount,
                                   sales.ClerkID,
                                   sales.ClerkName,
                                   sales.CustomerID,
                                   sales.Synced,
                                   sales.PLUCodeID,
                                   sales.PLUCode,
                                   sales.SequenceNo,
                                   sales.Flag,
                                   sales.ApplyTax,
                                   sales.ItemTax,
                                   sales.PriceLevel,
                                   sales.DateAdded,
                                   sales.SubTypeDescription,
                                   sales.POSTransactionID,
                                   sales.TransactionVersion,
                                   sales.ParentId,
                                   sales.BusinessDay,
                                   sales.ItemDiscount,
                                   sales.SyncToken,
                                   transaction);
        }

        public static int InsertSalesMain(out long? salesMainID, Sales sales)
        {
            return InsertSalesMain(out salesMainID, sales, null);
        }

        public static int InsertSalesMain(SalesList salesList, SqlTransaction transaction)
        {
            int recordsAffected = 0;
            long? salesMainID;

            foreach (Sales sales in salesList)
            {
                recordsAffected += InsertSalesMain(out salesMainID, sales, transaction);
                sales.SalesMainID = (long)salesMainID;
            }

            return recordsAffected;
        }
        
        public static int InsertSalesMainDetail(out long? salesMainID, string recordType, long? transactionID, string recordSubType,
                                                int? entityID, int? registerID, string pollDate, double? pollCount,
                                                decimal? pollAmount, int? clerkID, string clerkName, string customerID,
                                                int? synced, long? pLUCodeID, string pLUCode, int? sequenceNo,
                                                int? flag, int? applyTax, decimal? itemTax, string priceLevel,
                                                DateTime? dateAdded, string subTypeDescription, long? pOSTransactionID, int? transactionVersion,    
                                                string syncToken, long? parentId, DateTime? businessDay, decimal? itemDiscount, SqlTransaction transaction)
        {
            return SalesMainRepository.InsertSalesMain(out salesMainID,
                                   recordType,
                                   transactionID,
                                   recordSubType,
                                   entityID,
                                   registerID,
                                   pollDate,
                                   pollCount,
                                   pollAmount,
                                   clerkID,
                                   clerkName,
                                   customerID,
                                   synced,
                                   pLUCodeID,
                                   pLUCode,
                                   sequenceNo,
                                   flag,
                                   applyTax,
                                   itemTax,
                                   priceLevel,
                                   dateAdded,
                                   subTypeDescription,
                                   pOSTransactionID,
                                   transactionVersion,
                                   parentId,
                                   businessDay,
                                   itemDiscount,
                                   syncToken,
                                   transaction);
        }

        /// <summary>
        /// Insert the sales with parentId mappings.
        /// List of sales must belong to same transaction
        /// The Sequence must be ordered so that parent item appear first
        /// </summary>
        /// <param name="sales"></param>
        /// <returns></returns>
        public static bool Insert(List<Sales> sales, bool checkExists = true, bool commitTransaction = true, SqlTransaction transaction = null)
        {

            if (sales == null || sales.Count <= 0)
            {
                MxLogger.LogError(MxLogger.LogCategory.MxClient, "SalesMainService.Insert error : sales is null or empty");
                return false;
            }

            bool status = true;
            Sales header = sales[0];

            if (transaction == null)
                transaction = DataHelper.BeginTransaction();

            try
            {
                if (checkExists && CheckTransactionExists(header.EntityID, header.PollDate, header.TransactionID, header.RecordType, header.RecordSubType, header.TransactionVersion, transaction))
                {
                    return false;
                }

                var parentMappings = new Dictionary<int, long>();

                foreach (Sales salesLine in sales)
                {
                    long? salesParentID = null;
                    long? secondParentId = null;
                    int flag = salesLine.Flag;
                    // translate parent sequenceNumber (stored in ParentId) to a inserted SalesID
                    if (salesLine.ParentId > 0 && parentMappings.ContainsKey((int)salesLine.ParentId))
                        salesParentID = parentMappings[(int)salesLine.ParentId];

                    // Special case for IV record we want to put SalesMainID of the voided item (second parent) to Flag column
                    if ((salesLine.RecordType == EnumUtils<SalesRecordType>.GetClassName(SalesRecordType.ItemVoid)) && salesLine.Flag > 0 && parentMappings.ContainsKey(salesLine.Flag))
                    {
                        secondParentId = parentMappings[salesLine.Flag];
                        flag = (int)secondParentId;
                    }

                    long? salesId = null;
                    Mx.Services.LiveLink.SalesMainService.InsertSalesMainDetail(out salesId, salesLine.RecordType, header.TransactionID, salesLine.RecordSubType, header.EntityID, header.RegisterID, header.PollDate,
                                                                    salesLine.PollCount, salesLine.PollAmount, header.ClerkID, header.ClerkName, header.CustomerID, 0, salesLine.PLUCodeID,
                                                                    salesLine.PLUCode, salesLine.SequenceNo, flag, salesLine.ApplyTax, salesLine.ItemTax, salesLine.PriceLevel, DateTime.Now,
                                                                    salesLine.SubTypeDescription, header.POSTransactionID, header.TransactionVersion, null, salesParentID,
                                                                    header.BusinessDay, salesLine.ItemDiscount, transaction);

                    if (salesId == null || (salesId <= 0))
                    {
                        MxLogger.LogError(MxLogger.LogCategory.MxClient, "SalesMainService.InsertSalesMainDetail error : return null or invalid value");
                        status = false;
                        continue;
                    }

                    if (salesLine.SequenceNo > 0)
                    {
                        // add reference for SequenceNo to SalesID
                        if (!parentMappings.ContainsKey(salesLine.SequenceNo))
                            parentMappings.Add(salesLine.SequenceNo, salesId.Value);
                    }
                }
            }
            catch (Exception ex)
            {
                MxLogger.LogError(MxLogger.LogCategory.MxClient, "Exception occurred processing writing sales main to database. Exception: {0}", ex.ToString());
                status = false;
            }
            finally
            {
                if (transaction != null)
                {
                    if (status)
                    {
                        if (commitTransaction)
                            DataHelper.CommitTransaction(transaction);

                    }
                    else
                    {
                        DataHelper.RollbackTransaction(transaction);
                    }
                }
            }

            return status;
        }

        public static int UpdateParentId(long SalesMainID, long? ParentId, SqlTransaction transaction)
        {
            if (!ParentId.HasValue)
                ParentId = MMS.Utilities.MxConstants.LONG_NULL;
            return SalesMain.UpdateParentId(SalesMainID, ParentId.Value, transaction);
        }
        
        public static int DeleteSalesMain(Sales sales, SqlTransaction transaction)
        {
            return SalesMain.DeleteSalesMain(sales.SalesMainID, transaction);
        }

        public static int GetLastTransactionVersion(int entityID, DateTime businessDay, int registerID, long transactionID, SqlTransaction transaction)
        {
            int versionNumber = -1;

            using (IDataReader reader = SalesMain.GetLastTransactionVserion_Reader(entityID, businessDay, registerID, transactionID, transaction))
            {
                if (reader.Read())
                {
                    Sales salesRecords = new Sales(reader);
                    if (salesRecords != null)
                    {
                        return salesRecords.TransactionVersion;
                    }
                    else
                    {
                        return versionNumber;
                    }
                }
            }

            return versionNumber;
        }

        public static Sales GetLastTransactionHeader(int entityID, DateTime businessDay, int registerID, long transactionID, SqlTransaction transaction)
        {
            Sales sales = null;

            using (IDataReader reader = SalesMain.GetLastTransactionVserion_Reader(entityID, businessDay, registerID, transactionID, transaction))
            {
                if (reader.Read())
                {
                    sales = new Sales(reader);
                }
            }
            return sales;
        }

        public static SalesList GetTransactionRecords(int entityID, DateTime businessDay, int registerID, long transactionID, SqlTransaction transaction)
        {
            return new SalesList(SalesMain.GetSales_Reader(entityID, businessDay, registerID, transactionID, transaction), true);
        }

        public static SalesList GetTransactionRecords(int entityID, DateTime businessDay, int registerID, long transactionID, int transactionVersion, SqlTransaction transaction)
        {
            return new SalesList(SalesMain.GetSales_Reader(entityID, businessDay, registerID, transactionID, transactionVersion, transaction), true);
        }

        public static int DeleteTransactionRecords(int entityID, DateTime businessDay, int registerID, long transactionID, int transactionVersion, SqlTransaction transaction)
        {
            return SalesMain.DeleteTransaction(entityID, businessDay, registerID, transactionID, transactionVersion, transaction);
        }

        public static Sales GetRecordBySequenceNo(int entityID, DateTime businessDay, int registerID, long transactionID, int sequenceNo, SqlTransaction transaction)
        {
            Sales sales = null;

            using (IDataReader reader = SalesMain.GetRecordBySequenceNo_Reader(entityID, businessDay, registerID, transactionID, sequenceNo, transaction))
            {
                if (reader.Read())
                {
                    return new Sales(reader);
                }
            }

            return sales;
        }

        public static Sales GetRecordByTransaction(int entityID, DateTime businessDay, int registerID, long transactionID, SqlTransaction transaction)
        {
            Sales sales = null;

            using (IDataReader reader = SalesMainRepository.GetRecordByTransaction_Reader(entityID, businessDay, registerID, transactionID,  transaction))
            {
                if (reader.Read())
                {
                    return new Sales(reader);
                }
            }

            return sales;
        }

        public static Sales GetRecordByRecordType(int entityID, string pollDate, long transactionID, string recordType, string recordSubType, int transactionVersion, SqlTransaction transaction)
        {
            Sales sales = null;

            using (IDataReader reader = SalesMain.GetRecordByRecordType_Reader(entityID, pollDate, transactionID, recordType, recordSubType, transactionVersion, transaction))
            {
                if (reader.Read())
                {
                    return new Sales(reader);
                }
            }

            return sales;
        }

        public static Checksum GetChecksumByBusinessDate(DateTime businessDay, SqlTransaction transaction)
        {
            using (IDataReader reader = SalesMain.GetChecksumByBusinessDate_Reader(businessDay, transaction))
            {
                if (reader.Read())
                {
                    return new Checksum(reader);
                }
            }

            return null;
        }

        public static SalesList GetRegularSalesHeaderRecords(int entityID, DateTime businessDay,SqlTransaction transaction)
        {
            return new SalesList(SalesMain.GetRegularSalesHeaderRecords_Reader(entityID, businessDay, transaction), true);
        }

        #region Get DateTime

        public static DateTime? GetPreviousDayCloseTime(DateTime businessDay)
        {
            string dayCloseTime = SalesMain.GetPreviousDayCloseTime_Scalar(businessDay);

            if (!string.IsNullOrEmpty(dayCloseTime))
            {
                return Convert.ToDateTime(dayCloseTime);  
            }

            return null;
        }

        public static DateTime? GetPreviousDayLastTransactionTime(DateTime businessDay)
        {
            string dayCloseTime = SalesMain.GetPreviousDayLastTransactionTime_Scalar(businessDay);

            if (!string.IsNullOrEmpty(dayCloseTime))
            {
                return Convert.ToDateTime(dayCloseTime);
            }

            return null;
        }

        public static DateTime? GetLastTransactionTime(DateTime businessDay)
        {
            string dayCloseTime = SalesMain.GetLastTransactionTime_Scalar(businessDay);

            if (!string.IsNullOrEmpty(dayCloseTime))
            {
                return Convert.ToDateTime(dayCloseTime);
            }

            return null;
        }

        public static DateTime? GetNextDayOpenTime(DateTime businessDay)
        {
            string dayOpenTime = SalesMain.GetNextDayOpenTime_Scalar(businessDay);

            if (!string.IsNullOrEmpty(dayOpenTime))
            {
                return Convert.ToDateTime(dayOpenTime);
            }

            return null;
        }

        public static DateTime? GetFirstDayOpenDate()
        {
            return SalesMain.GetFirstDayOpenDate_Scalar();
        }

        public static DateTime? GetLatestDayOpenDate()
        {
            return SalesMain.GetLatestDayOpenDate_Scalar();
        }

        public static DateTime? GetFirstDayCloseDate()
        {
            return SalesMain.GetFirstDayCloseDate_Scalar();
        }

        public static DateTime? GetLatestDayCloseDate()
        {
            return SalesMain.GetLatestDayCloseDate_Scalar();
        }

        public static DateTime? GetOpenTime(DateTime businessDay)
        {
            string openTime = SalesMain.GetOpenTime_Scalar(businessDay);

            if (!string.IsNullOrEmpty(openTime))
            {
                return Convert.ToDateTime(openTime);
            }

            return null;
        }

        public static DateTime? GetCloseTime(DateTime businessDay)
        {
            string closeTime = SalesMain.GetCloseTime_Scalar(businessDay);

            if (!string.IsNullOrEmpty(closeTime))
            {
                return Convert.ToDateTime(closeTime);
            }

            return null;
        }

        #endregion

        public static Checksum GetChecksumByBusinessDate(DateTime businessDay)
        {
            return GetChecksumByBusinessDate(businessDay, null);
        }
		
        public static DataSet GetTableOverview()
        {
            return SalesMain.TableOverview();
        }

        public static Sales GetRecordByClerkPLU(int entityID, DateTime businessDay, long clerkID, string recordType, string recordSubType, long pluCodeID, string pluCode, SqlTransaction transaction)
        {
            Sales sales = null;

            using (IDataReader reader = SalesMain.GetRecordByClerkPLU_Reader(entityID, businessDay, clerkID, recordType, recordSubType, pluCodeID, pluCode, transaction))
            {
                if (reader.Read())
                {
                    return new Sales(reader);
                }
            }

            return sales;
        }

        #region Transaction Exists Checks

        public static bool CheckExists(int entityID, string pollDate, long transactionID, string recordType, string recordSubType)
        {
            using (IDataReader reader = SalesMain.CheckExists_Reader(entityID, pollDate, transactionID, recordType, recordSubType))
            {
                if (reader.Read())
                {
                    Sales salesRecords = new Sales(reader);
                    // check record exists
                    if (salesRecords != null)
                        return true;
                }
            }
            return false;
        }

        public static bool CheckExists(int entityID, string pollDate, long posTransactionID, long transactionVersion, SqlTransaction transaction)
        {
            using (IDataReader reader = SalesMain.CheckExists_Reader(entityID, pollDate, posTransactionID, transactionVersion, transaction))
            {
                if (reader.Read())
                {
                    Sales salesRecords = new Sales(reader);
                    // check record exists
                    if (salesRecords != null)
                        return true;
                }
            }
            return false;
        }

        public static bool CheckExists(int entityID, DateTime businessDay, string recordType, string recordSubType)
        {
            using (IDataReader reader = SalesMain.CheckExists_Reader(entityID, businessDay, recordType, recordSubType))
            {
                if (reader.Read())
                {
                    Sales salesRecords = new Sales(reader);
                    // check record exists
                    if (salesRecords != null)
                        return true;
                }
            }
            return false;
        }

        public static bool CheckExists(DateTime businessDay, string recordType, string recordSubType, int registerID,
            int clerkID, string clerkName, long pLUCodeID, string pLUCode)
        {
            using (IDataReader reader = SalesMain.CheckExists_Reader(businessDay, recordType, recordSubType, registerID, clerkID, clerkName, pLUCodeID, pLUCode))
            {
                if (reader.Read())
                {
                    Sales salesRecords = new Sales(reader);
                    // check record exists
                    if (salesRecords != null)
                        return true;
                }
            }
            return false;
        }

        public static bool CheckExists(DateTime businessDay, string recordType, string recordSubType, int registerID, int clerkID, long pLUCodeID, string pLUCode)
        {
            using (IDataReader reader = SalesMain.CheckExists_Reader(businessDay, recordType, recordSubType, registerID, clerkID, pLUCodeID, pLUCode))
            {
                if (reader.Read())
                {
                    Sales salesRecords = new Sales(reader);
                    // check record exists
                    if (salesRecords != null)
                        return true;
                }
            }
            return false;
        }

        public static bool CheckTransactionExists(int entityID, DateTime businessDay, int registerID, long transactionID, int transactionVersion)
        {
            var sales = GetTransactionRecords(entityID, businessDay, registerID, transactionID, transactionVersion, null);
            return sales != null && sales.Count > 0;
        }

        public static bool CheckTransactionExists(int entityID, string pollDate, long transactionID, string recordType, string recordSubType, int transactionVersion, SqlTransaction tran = null)
        {
            var header = GetRecordByRecordType(entityID, pollDate, transactionID, recordType, recordSubType, transactionVersion, tran);
            return header != null;
        }

        public static bool CheckTransactionExists(int entityID, DateTime businessDay, int registerID, long transactionID)
        {
            var header = GetRecordByTransaction(entityID, businessDay, registerID, transactionID, null);
            return header != null;
        }

        #endregion

        #region Compris Specific Functions

        public static bool InsertSalesCompris(Sales header, SalesList detail)
        {
            SqlTransaction transaction = null;
            bool status = true;

            try
            {
                transaction = DataHelper.BeginTransaction();

                if (transaction == null)
                    throw new MxException("Failed to begin SQL transaction. Critical error!");

                // check if sales transaction already exists
                if (CheckExists(header.EntityID, header.PollDate, header.POSTransactionID, header.TransactionVersion, transaction))
                    return true;

                long generatedTransactionID = 0;
                SalesMain.InsertHeaderCompris(header.TransactionID,
                                       header.RecordSubType,
                                       header.EntityID,
                                       header.RegisterID,
                                       header.PollDate,
                                       header.PollCount,
                                       header.PollAmount,
                                       header.ClerkID,
                                       header.ClerkName,
                                       header.CustomerID,
                                       header.PLUCodeID,
                                       header.PLUCode,
                                       header.SequenceNo,
                                       header.Flag,
                                       header.ApplyTax,
                                       header.ItemTax,
                                       header.SubTypeDescription,
                                       header.POSTransactionID,
                                       header.ParentId,
                                       header.BusinessDay,
                                       header.TransactionVersion,
                                       header.Synced,
                                       out generatedTransactionID,
                                       transaction);

                // check new TransactionID is created correctly from trigger
                if (generatedTransactionID <= 0)
                {
                    status = false;
                    return false;
                }

                // store new TransactionID
                header.TransactionID = generatedTransactionID;
                // write each row to the table (setting the TransactionID to the newly generated transactionID)
                foreach (Sales detailRow in detail)
                {
                    long? salesMainID = 0;
                    detailRow.TransactionID = header.TransactionID;
                    InsertSalesMain(out salesMainID, detailRow, transaction);
                    if (!salesMainID.HasValue || salesMainID.Value <= 0)
                    {
                        status = false;
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                MxLogger.LogError(MxLogger.LogCategory.MxClient, "Exception occurred adding compris data to database. Exception: {0}", ex.ToString());
                status = false;
            }
            finally
            {
                // commit or rollback the transaction
                if (transaction != null)
                {
                    if (status)
                        DataHelper.CommitTransaction(transaction);
                    else
                        DataHelper.RollbackTransaction(transaction);
                }
            }
            return status;
        }

        #endregion

        #region Insert Alert Message
        public static bool InsertAlertMessage(int EntityID, 
                                              AlertRecordSubType alertType,
                                              string message1,
                                              string message2)
        {
            SqlTransaction transaction = null;
            bool status = true;
            try
            {
                transaction = DataHelper.BeginTransaction();

                if (transaction == null)
                {
                    throw new MxException("Failed to begin SQL transaction. Critical error!");
                }

                Sales salesRecord = new Sales(0,
                                              EnumUtils<SalesRecordType>.GetClassName(SalesRecordType.Alert),   // recordType,
                                              0,                                                                // TransactionID,
                                              EnumUtils<AlertRecordSubType>.GetClassName(alertType),            // recordSubType,
                                              EntityID,                                                         // EntityId,
                                              0,                                                                // RegisterID,
                                              DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),                     // PollDate,
                                              0,                                                                // pollCount,
                                              0,                                                                // pollAmount,
                                              0,                                                                // ClerkID,
                                              "",                                                               // ClerkName,
                                              "",                                                               // CustomerID,
                                              0,                                                                // Synced,
                                              0,                                                                // pLUCodeID,
                                              message1.Length <= 255 ? message1 : message1.Substring(0, 255),                                                                // pLUCode,
                                              0,                                                                // SequenceNo,
                                              0,                                                                // serviceTypeId,
                                              0,                                                                // applyTax,
                                              0,                                                                // itemTax,
                                              "",                                                               // PriceLevel,
                                              DateTime.Now,                                                     // DateAdded,
                                              message2.Length <= 255 ? message2 : message2.Substring(0, 255),                                                               // subTypeDescription,
                                              0,                                                                // POSTransactionID,
                                              0,                                                                // TransactionVersion,
                                              null,                                                             // SyncToken,
                                              0,                                                                // ParentId,
                                              DateTime.Now.Date,                                                // BusinessDate, 
                                              0, 
                                              0);

                long? salesMainID = 0;
                InsertSalesMain(out salesMainID, salesRecord, transaction);
                if (!salesMainID.HasValue || salesMainID.Value <= 0)
                {
                    status = false;
                }
            }
            catch (Exception ex)
            {
                MxLogger.LogError(MxLogger.LogCategory.MxClient, "Exception occurred adding compris data to database. Exception: {0}", ex.ToString());
                status = false;
            }
            finally
            {
                // commit or rollback the transaction
                if (transaction != null)
                {
                    if (status)
                        DataHelper.CommitTransaction(transaction);
                    else
                        DataHelper.RollbackTransaction(transaction);
                }
            }
            return status;
        }


        


        #endregion

        /// <summary>
        /// This function checks if the POS transaction previously processed and validate the transaction time to decide 
        /// if transaction reverse is needed before new transaction records are added.
        /// </summary>
        /// <param name="entityID"></param>
        /// <param name="businessDay"></param>
        /// <param name="registerID"></param>
        /// <param name="transactionID"></param>
        /// <param name="transactionTime"></param>
        /// <param name="salesAmount"> </param>
        /// <param name="syncData"> </param>
        /// <param name="nextTransactionVersion"> the next transaction version number to use for insert modified transaction </param>
        /// <returns>true: if the transaction is upto date. false: need to add a new transaction </returns>
        public static bool ReversePreviousTransactionOnTransactionTimeChange(int entityID, DateTime businessDay, int registerID, long transactionID, DateTime transactionTime,  decimal salesAmount, bool syncData, ref int nextTransactionVersion)
        {
            bool status = true;
            SqlTransaction sqlTransaction = null;
            bool addTransaction = false;

            try
            {
                sqlTransaction = DataHelper.BeginTransaction();

                if (sqlTransaction == null)
                {
                    throw new MxException("Failed to begin SQL transaction. Critical error!");
                }

                var header = SalesMainService.GetLastTransactionHeader(entityID, businessDay, registerID, transactionID, sqlTransaction);
                var lastTransactionVersion = header != null ? header.TransactionVersion : -1;
                
                if (lastTransactionVersion > -1)
                {
                    // if the transaction was already processed, check if transaction date is changed.
                    if (syncData && header != null && header.RecordSubType != EnumUtils<HeaderRecordSubType>.GetClassName(HeaderRecordSubType.ReverseSales))
                    {
                        if (DateTime.Parse(header.PollDate) != transactionTime || header.PollAmount != salesAmount)
                        {
                            var transactionRecords = SalesMainService.GetTransactionRecords(entityID, businessDay, registerID, transactionID, lastTransactionVersion, sqlTransaction);
                            // if the new transaction time is different than the processed transaction time
                            // reverse the last transaction processed.
                            addTransaction = true;
                            transactionRecords.SetVersionNumber(lastTransactionVersion);

                            foreach (var salesRecord in transactionRecords)
                            {
                                salesRecord.ItemNumber = salesRecord.SalesMainID;
                                salesRecord.ParentItemNumber = salesRecord.ParentId;
                                salesRecord.TransactionVersion = lastTransactionVersion + 1;
                                salesRecord.Synced = 0;
                                salesRecord.SyncToken = null;
                                salesRecord.BusinessDay = businessDay;

                                // no reverse for operator shift record
                                if (salesRecord.RecordType == EnumUtils<SalesRecordType>.GetClassName(SalesRecordType.Control) &&
                                    salesRecord.RecordSubType == EnumUtils<ControlRecordSubType>.GetClassName(ControlRecordSubType.OperatorShift))
                                {
                                    continue;
                                }

                                salesRecord.PollCount *= -1;
                                salesRecord.PollAmount *= -1;
                                salesRecord.ItemTax *= -1;
                                salesRecord.ItemDiscount *= -1;

                                if (salesRecord.RecordType == EnumUtils<SalesRecordType>.GetClassName(SalesRecordType.Header) &&
                                    salesRecord.RecordSubType == EnumUtils<HeaderRecordSubType>.GetClassName(HeaderRecordSubType.Sales))
                                {
                                    salesRecord.RecordSubType = EnumUtils<HeaderRecordSubType>.GetClassName(HeaderRecordSubType.ReverseSales);
                                }
                            }

                            SalesMainService.InsertSalesMain(transactionRecords, sqlTransaction);

                            transactionRecords.UpdateParentId();

                            SalesMainService.UpdateParentId(transactionRecords, sqlTransaction);

                            // increase version number for actual record.
                            nextTransactionVersion = lastTransactionVersion + 2;
                        }
                    }
                }
                else
                {
                    addTransaction = true;
                    nextTransactionVersion = 0;
                }
            }
            catch (Exception ex)
            {
                MxLogger.LogError(MxLogger.LogCategory.MxClient, "Exception occurred inserting transaction records. Exception: {0}", ex.ToString());
                status = false;
            }
            finally
            {
                if (status)
                {
                    DataHelper.CommitTransaction(sqlTransaction);
                }
                else
                {
                    DataHelper.RollbackTransaction(sqlTransaction);
                    throw new MxException("Failed to submit SQL transaction. Critical error!");
                }
            }

            return addTransaction;
        }

    }
}
