﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;

using Mx.Data.LiveLink;
using Mx.Common.Data;
using MMS.Utilities;

namespace Mx.Services.LiveLink
{
    public static class LiveLinkService
    {
        public static void InstallSchema(string connectionString)
        {
            string buildOutput = string.Empty;
            List<string> buildMessages = null;
            DbInstaller.VersionCheckInstall(Assembly.GetAssembly(typeof(LiveLinkFactory)), connectionString, ref buildOutput, ref buildMessages);
            if (buildMessages != null && buildMessages.Count > 0)
            {
                // log each of the build messages
                foreach (string buildMessage in buildMessages)
                    MxLogger.LogInformation(MxLogger.LogCategory.MxClient, buildMessage);
            }
        }
    }
}
