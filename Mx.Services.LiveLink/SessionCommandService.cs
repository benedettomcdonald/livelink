﻿using Mx.BusinessObjects.LiveLink;
using Mx.Common;
using Mx.Data.LiveLink.Interfaces;

namespace Mx.Services.LiveLink
{
    public static class SessionCommandService
    {
        public static SessionCommandRequest GetRequestByID(int sessionCommandID)
        {
            return IocContainer.Resolve<ISessionCommandRepository>().GetRequestByID(sessionCommandID);
        }

        public static SessionCommandResponse GetResponseByID(int sessionCommandResponseID)
        {
            return IocContainer.Resolve<ISessionCommandRepository>().GetResponseByID(sessionCommandResponseID);
        }

        public static SessionCommandResponseList GetResponseListByCommandID(int sessionCommandID)
        {
            return IocContainer.Resolve<ISessionCommandRepository>().GetResponseListByCommandID(sessionCommandID);
        }

        public static bool InsertRequest(SessionCommandRequest request)
        {
            return IocContainer.Resolve<ISessionCommandRepository>().InsertRequest(request);
        }

        public static bool InsertResponse(SessionCommandResponse response)
        {
            return IocContainer.Resolve<ISessionCommandRepository>().InsertResponse(response);
        }

    }
}