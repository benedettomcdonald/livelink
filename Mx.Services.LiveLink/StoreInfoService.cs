﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Data;
using Mx.Data.LiveLink;
using Mx.BusinessObjects.LiveLink;

namespace Mx.Services.LiveLink
{
    public class StoreInfoService
    {
        public static int GetEntityId()
        {
            int entityId = 0;

            using (IDataReader reader = StoreInfo.GetEntityId_Reader())
            {
                if (reader.Read())
                {
                    Mx.BusinessObjects.LiveLink.Store store = new Mx.BusinessObjects.LiveLink.Store(reader);
                    return store.StoreID;
                }
            }

            return entityId;
        }

   		public static bool UpdateStatus(byte status)
        {
            int recordsAffected = StoreInfo.UpdateStatus(status);
            if (recordsAffected <= 0)
                return false;
            return true;
        }     

		public static DateTime? GetLastDate()
        {
            String lastDate = StoreInfo.GetLastDateStoreInfo_Scalar();
            if (string.IsNullOrEmpty(lastDate))
                return null;
            return DateTime.Parse(lastDate);
        }

        public static int UpdateLastDate(DateTime lastDate)
        {
            int recordsAffected = StoreInfo.UpdateLastDateStoreInfo(lastDate.ToString("yyyy-MM-dd HH:mm:ss"));
            return recordsAffected;
        }
    }
}
