﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;
using MMS.Utilities;
using System.IO.Compression;


namespace Mx.Services.LiveLink
{
    public class XMLFilesService
    {
        public static FileInfo[] GetXmlFileList(string xmlFilePath, string xmlFileMask)
        {
            FileInfo[] xmlFileList = null;

            if (xmlFilePath == null || xmlFilePath.Length == 0)
            {
                MxLogger.LogError(MxLogger.LogCategory.MxClient, "Error: Directory for XML data not set.");
            }
            else if (!Directory.Exists(xmlFilePath))
            {
                MxLogger.LogError(MxLogger.LogCategory.MxClient, "Error: Directory containing XML data doesn't exist [{0}].", xmlFilePath);
            }
            else
            {
                xmlFileList = new DirectoryInfo(Path.GetDirectoryName(xmlFilePath)).GetFiles(Path.GetFileName(xmlFileMask));
            }

            return xmlFileList;
        }

        public static string CreateDateTimeDirStructure(string filePath)
        {
            string directoryName = ""; 
            try
            {
                directoryName = Path.Combine(filePath, DateTime.Now.ToString("yyyyMMdd"));
                if (!Directory.Exists(directoryName))
                {
                    Directory.CreateDirectory(directoryName);
                }
            }
            catch(Exception ex)
            {
                directoryName = "";
                MxLogger.LogError(MxLogger.LogCategory.MxClient, "Exception: [CreateDateTimeDirStructure] - {0}", ex.ToString());
            }

            return directoryName;
        }

        // delete old archive data
        public static Boolean DeleteArchivedXmlFiles(string archivePath, int daysBeforeObsolete)
        {
            if ( archivePath == null || archivePath.Length == 0 || !Directory.Exists(archivePath) )
            {
                return false;
            }

            if (daysBeforeObsolete <= 0)
            {
                return false;
            }

            // set the date to delete directories prior to
            DateTime obsoleteDate = DateTime.Now.AddDays(-daysBeforeObsolete);
            DirectoryInfo[] archiveDirList = new DirectoryInfo(Path.GetDirectoryName(archivePath + "\\")).GetDirectories();
            
            foreach(DirectoryInfo archiveDir in archiveDirList)
            {
                try
                {
                    // check if directory is numeric. needs to be YYYYMMDD
                    if (!UtilityService.IsNumericString(archiveDir.Name))
                    {
                        continue;
                    }

                    DateTime dirDate = DateTime.ParseExact(archiveDir.Name, "yyyyMMdd", null);
                    if ( dirDate < obsoleteDate)
                    {
                        archiveDir.Delete(true);
                    }
                }
                catch ( Exception ex )
                {
                    MxLogger.LogError(MxLogger.LogCategory.MxClient, "Exception: Deleting old archive directory:[{0}]. Exception: {1}", archiveDir.FullName, ex.ToString());
                }
            }

            return true;
        }

        public static bool ArchiveXmlFile(FileInfo xmlFile, string archivePath)
        {
            return ArchiveXmlFile(xmlFile, archivePath, xmlFile.Name);
        }

        public static bool ArchiveXmlFile(FileInfo xmlFile, string archivePath, string archiveFileName)
        {
            bool archiveXmlFile = true;

            if (archivePath != null && archivePath.Length > 0)
            {
                try
                {
                    string filePath = CreateDateTimeDirStructure(archivePath);

                    if (filePath.Length > 0)
                    {
                        File.Move(xmlFile.FullName, Path.Combine(filePath, archiveFileName));
                    }
                    else
                    {
                        MxLogger.LogError(MxLogger.LogCategory.MxClient, "Error: [ArchiveXmlFile] - Failed to archive file:[{0}]", xmlFile.FullName);
                    }
                }
                catch (Exception ex)
                {
                    MxLogger.LogError(MxLogger.LogCategory.MxClient, "Exception: [ArchiveXmlFile] - {0}", ex.ToString());
                }
            }

            return archiveXmlFile;
        }

        public static bool ArchiveXmlFile(FileInfo xmlFile, string archivePath, bool compress)
        {
            if (compress)
            {
                return CompressArchiveXmlFile(xmlFile, archivePath, null);
            }
            else
            {
                return ArchiveXmlFile(xmlFile, archivePath);
            }
        }

        public static bool ArchiveXmlFile(FileInfo xmlFile, string archivePath, bool compress, string compressFileName)
        {
            if (compress)
            {
                return CompressArchiveXmlFile(xmlFile, archivePath, compressFileName);
            }
            else
            {
                if (string.IsNullOrEmpty(compressFileName))
                    return ArchiveXmlFile(xmlFile, archivePath);

                return ArchiveXmlFile(xmlFile, archivePath, compressFileName);
            }
        }

        public static bool ArchiveXmlFile(FileInfo[] fileList, string archivePath)
        {
            bool archiveXmlFile = true;

            foreach (FileInfo fi in fileList)
            {
                archiveXmlFile = ArchiveXmlFile(fi, archivePath);
            }

            return archiveXmlFile;
        }

        public static bool ArchiveXmlFile(FileInfo[] fileList, string archivePath, bool compress, string compressFileName)
        {
            if (compress)
            {
                return CompressArchiveXmlFile(fileList, archivePath, compressFileName);
            }
            else
            {
                return ArchiveXmlFile(fileList, archivePath);
            }
        }

        public static bool CompressArchiveXmlFile(FileInfo xmlFile, string archivePath, string compressFileName)
        {
            bool archiveXmlFile = true;

            if (archivePath != null && archivePath.Length > 0)
            {
                try
                {
                    string filePath = CreateDateTimeDirStructure(archivePath);

                    if (filePath.Length > 0)
                    {
                        string fileName = Path.Combine(filePath, xmlFile.Name);

                        //When input compressFileName that has extension not .zip must chane it to .zip
                        compressFileName = ConvertFileNameToZip(string.IsNullOrEmpty(compressFileName) ? fileName : compressFileName);

                        Dictionary<String, String> fileNames = new Dictionary<String, String>();

                        fileNames.Add(xmlFile.FullName, null);

                        archiveXmlFile = mod_MMS_Compression.CompressFiles(fileNames, filePath, compressFileName, true, true, true);

                        File.Delete(xmlFile.FullName);
                    }
                    else
                    {
                        MxLogger.LogError(MxLogger.LogCategory.MxClient, "Error: [ArchiveXmlFile] - Failed to archive file:[{0}]", xmlFile.FullName);
                    }
                }
                catch (Exception ex)
                {
                    MxLogger.LogError(MxLogger.LogCategory.MxClient, "Exception: [ArchiveXmlFile] - {0}", ex.ToString());
                }
            }

            return archiveXmlFile;
        }

        private static string ConvertFileNameToZip(string fileName)
        {
            var lastIndex = fileName.LastIndexOf(".", System.StringComparison.Ordinal);
            var newName = lastIndex < 0 ? fileName : fileName.Substring(0, lastIndex);
            return newName + ".zip";
        }

        public static bool CompressArchiveXmlFile(FileInfo[] fileList, string archivePath, string compressFileName)
        {
            bool archiveXmlFile = true;
            Dictionary<String, String> fileNames = new Dictionary<String, String>();
            string filePath = CreateDateTimeDirStructure(archivePath);
            
            if (filePath.Length > 0)
            {
                foreach (FileInfo fi in fileList)
                {
                    fileNames.Add(fi.FullName, null);
                }

                archiveXmlFile = mod_MMS_Compression.CompressFiles(fileNames, filePath, compressFileName, true, true, true);

                foreach (FileInfo fi in fileList)
                {
                    File.Delete(fi.FullName);
                }
            }
            else
            {
                MxLogger.LogError(MxLogger.LogCategory.MxClient, "Error: [ArchiveXmlFile] - Failed to archive files.");
            }


            return archiveXmlFile;
        }
    }
}
