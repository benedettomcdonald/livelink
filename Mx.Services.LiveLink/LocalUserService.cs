﻿using System.Data;
using Mx.Data.LiveLink;
using Mx.BusinessObjects.LiveLink;

namespace Mx.Services.LiveLink
{
    public class LocalUserService
    {
        public static Users GetUserByPosid(string posID)
        {
            Users user = null;

            using (IDataReader reader = LocalUsers.GetUserByPOSID_Reader(posID))
            {
                if (reader.Read())
                {
                    user = new Users(reader);
                }
            }

            return user;
        }

        public static int InsertLocalUsers(long? userId, string userName, string userPassword, long? entityID,
            string firstName, string lastName, string pOSIDName, long? employeeId,
            string employeeNumber)
        {
            return LocalUsers.InsertLocalUsers(userId, userName, userPassword, entityID, firstName, lastName,
            pOSIDName, employeeId, employeeNumber, null);
        }
    }
}
