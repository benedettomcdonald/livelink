﻿using System;
using System.Collections.Generic;
using System.ServiceProcess;
using System.Text;

namespace LiveLinkService
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
#if (!DEBUG)
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
                                {
                                    new CoreService()
                                };
            ServiceBase.Run(ServicesToRun);
#else
            CoreService ServiceToRun = new CoreService();
            ServiceToRun.DoStart();
            System.Threading.Thread.Sleep(System.Threading.Timeout.Infinite);
#endif
        }
    }
}
