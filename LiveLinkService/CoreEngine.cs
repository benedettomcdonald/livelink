﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ServiceProcess;
using Microsoft.Win32;

using Mx.Configuration;
using Mx.Common.LiveLink;
using MMS.Utilities;
using Mx.LiveLink.Core;
using Mx.LiveLink.Interface;
using Mx.POS.Common;


namespace LiveLinkService
{
    public class CoreEngine
    {

        #region Private Variables

        // constant definitions
        private const string _serviceType = "LiveLinkService";
        private const string _appConfigFile = "LiveLinkService.exe.config";
        // private class variables
        private LiveLinkCore _liveLinkCore;

        #endregion

        #region Constructor

        public CoreEngine()
        {
            if (Mx.Configuration.ConfigurationHelper.IsBaseConfigured())
            {
                if (LiveLinkConfiguration.IntouchUseLocalCulture)
                {
                    Globals.NumberCulture = System.Threading.Thread.CurrentThread.CurrentCulture;
                    Globals.DateCulture = System.Threading.Thread.CurrentThread.CurrentCulture;
                }

                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-AU", false);
                if (!LiveLinkConfiguration.IntouchUseLocalCulture)
                {
                    Globals.NumberCulture = System.Threading.Thread.CurrentThread.CurrentCulture;
                    Globals.DateCulture = System.Threading.Thread.CurrentThread.CurrentCulture;
                }
            }

            MxLogger.LogInformationToTrace(0, MxLogger.LogPriority.High, _serviceType, null, "Service initialising...");

            string storeNumber = modLiveLink.RegRead(Registry.LocalMachine, WindowsService.LiveLinkServiceRegistryKey, WindowsService.LiveLinkServiceRegistryKeyNameStoreNumber, "");
            List<string> serviceArguments = new List<string>();
            serviceArguments.AddRange(Environment.GetCommandLineArgs());

            if (!string.IsNullOrEmpty(storeNumber))
            {
                MxLogger.LogInformationToTrace(0, MxLogger.LogPriority.High, _serviceType, null, "Store #{0} defined in registry", storeNumber);
                serviceArguments.Add(string.Format("/s:{0}",storeNumber));
            }

            _liveLinkCore = new LiveLinkCore(serviceArguments.ToArray(), Environment.CommandLine, _appConfigFile, new StatusController(SetStatus), new LogController(LogStatus));
            _liveLinkCore.RestartEvent += new ILiveLinkCore.RestartEventEventHandler(LiveLinkCore_RestartEvent);
            _liveLinkCore.VersionUpdateEvent += new ILiveLinkCore.VersionUpdateEventEventHandler(LiveLinkCore_VersionUpdateEvent);
        }


        #endregion
        
        #region Service Start/Stop

        public void Start(string[] args)
        {
            MxLogger.LogInformationToTrace(0, MxLogger.LogPriority.High, _serviceType, null, "Engine Start");

            // check if old livelink winforms app is running (do not start service if it is).
            if (WindowsService.CheckProcessRunning(WindowsService.LiveLinkProcessName))
            {
                MxLogger.LogInformationToTrace(0, MxLogger.LogPriority.High, _serviceType, null, "The LiveLink WinForms application is already running. This must be shut down before starting the LiveLink service.");
                return;
            }
            // remove any old registry settings that AutoStart the old WinForms LiveLink
            modLiveLink.SetRegistryLiveLinkAutoStarts(false);
            // call delayed initialise and start, this will prevent any Windows Service timeouts from occurring.
            _liveLinkCore.DelayedInitialiseAutoStart();
        }

        public void Stop()
        {
            MxLogger.LogInformationToTrace(0, MxLogger.LogPriority.High, _serviceType, null, "Engine Stop");
            _liveLinkCore.Stop();
        }

        public void OnShutdown()
        {
            MxLogger.LogInformationToTrace(0, MxLogger.LogPriority.High, _serviceType, null, "Engine shutdown");
        }

        public bool CustomCommand(int command)
        {
            try
            {                
                // check if command is a defined "LiveLinkServiceCommandType"
                if (!Enum.IsDefined(typeof(WindowsService.LiveLinkServiceCommandType), command))
                {
                    MxLogger.LogInformationToTrace(0, MxLogger.LogPriority.High, _serviceType, null, "Received undefined custom command: {0}", command);
                    // if it isn't call base custom command handling
                    return false;
                }

                if (!_liveLinkCore.Initialised)
                {
                    MxLogger.LogInformationToTrace(0, MxLogger.LogPriority.High, _serviceType, null, "LiveLink is not initialised, can not run command");
                    return false;
                }

                // translate updater command type
                WindowsService.LiveLinkServiceCommandType commandType = (WindowsService.LiveLinkServiceCommandType)command;
                MxLogger.LogInformationToTrace(0, MxLogger.LogPriority.High, _serviceType, null, "Running command: {0}", commandType.ToString());

                // define a controller for the LiveLink service
                ServiceController controller = new ServiceController(WindowsService.LiveLinkServiceName);
                                
                switch (commandType)
                {
                    case WindowsService.LiveLinkServiceCommandType.ImportData:
                        _liveLinkCore.ForceImport();
                        break;

                    case WindowsService.LiveLinkServiceCommandType.SendSystemInfo:
                        _liveLinkCore.SendSystemInfo();
                        break;

                    case WindowsService.LiveLinkServiceCommandType.SendData:
                        _liveLinkCore.SendData();
                        break;
                        
                    case WindowsService.LiveLinkServiceCommandType.RecreateLiveLinkID:
                        _liveLinkCore.RecreateLiveLinkID();
                        break;
                        
                    case WindowsService.LiveLinkServiceCommandType.RefreshEntity:
                        _liveLinkCore.RefreshEntityID();
                        break;

                    case WindowsService.LiveLinkServiceCommandType.RefreshConfiguration:
                        _liveLinkCore.RefreshConfiguration();
                        break;

                    default:
                        // unhandled!
                        break;
                }
            }
            catch (Exception ex)
            {
                MxLogger.LogInformationToTrace(0, MxLogger.LogPriority.High, _serviceType, null, "Exception occurred processing custom command. Exception: {0}", ex.ToString());
            }
            return true;
        }

        #endregion

        #region Event Handlers

        protected void LiveLinkCore_VersionUpdateEvent(object sender, VersionUpdateEventArgs updateArgs)
        {
            UpdateVersion();
        }

        protected void LiveLinkCore_RestartEvent(object sender, EventArgs args)
        {
            RestartLiveLink();
        }
                
        public void UpdateVersion()
        {
            try
            {
                ServiceController controller = new ServiceController(WindowsService.LiveLinkServiceUpdaterName);
                switch (controller.Status)
                {
                    case ServiceControllerStatus.Running:
                        controller.ExecuteCommand((int)WindowsService.LiveLinkServiceCommandType.ServiceUpdate);
                        break;

                    default:
                        MxLogger.LogInformationToTrace(0, MxLogger.LogPriority.High, _serviceType, null, "Service ({0}) is not running. Can not request update.",controller.ServiceName);
                        break;
                }
            }
            catch (Exception ex)
            {
                MxLogger.LogInformationToTrace(0, MxLogger.LogPriority.High, _serviceType, null, "Exception occurred requesting update in LiveLink Updater. Exception: {0}", ex.ToString());
            }
        }


        public void RestartLiveLink()
        {
            try
            {
                ServiceController controller = new ServiceController(WindowsService.LiveLinkServiceUpdaterName);
                switch (controller.Status)
                {
                    case ServiceControllerStatus.Running:
                        controller.ExecuteCommand((int)WindowsService.LiveLinkServiceCommandType.ServiceRestart);
                        break;

                    default:
                        MxLogger.LogInformationToTrace(0, MxLogger.LogPriority.High, _serviceType, null, "Service ({0}) is not running. Can not request restart.", controller.ServiceName);
                        break;
                }
            }
            catch (Exception ex)
            {
                MxLogger.LogInformationToTrace(0, MxLogger.LogPriority.High, _serviceType, null, "Exception occurred requesting restart in LiveLink Updater. Exception: {0}", ex.ToString());
            }
        }

        #endregion

        #region Log Controllers

        private void SetStatus(string status)
        {
            MxLogger.LogInformationToTrace(0, MxLogger.LogPriority.High, "Status", null, status);
        }

        private void LogStatus(string type, string detail)
        {
            MxLogger.LogInformationToTrace(0, MxLogger.LogPriority.High, type, null, detail);
        }

        #endregion
        
    }
}
