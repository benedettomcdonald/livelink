﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.ServiceProcess;
using System.Text;
using Microsoft.Win32;

using Mx.Configuration;
using Mx.Common.LiveLink;
using MMS.Utilities;



namespace LiveLinkService
{
    public partial class CoreService : ServiceBase
    {

        #region Private Variables

        private CoreEngine _livelinkEngine;
        
        #endregion
        
        #region Constructor

        public CoreService()
        {
            InitializeComponent();
            _livelinkEngine = new CoreEngine();
        }

        #endregion
        
        #region Service Start/Stop

        protected override void OnShutdown()
        {
            // handle shutdown event
            _livelinkEngine.OnShutdown();
        }
        
        protected override void OnStart(string[] args)
        {
            _livelinkEngine.Start(args);
        }

        protected override void OnStop()
        {
            _livelinkEngine.Stop();
        }

        protected override void OnCustomCommand(int command)
        {
            if (!_livelinkEngine.CustomCommand(command))
                base.OnCustomCommand(command);
        }

#if (DEBUG)
        public void DoStart()
        {
            _livelinkEngine.Start(null);
        }
#endif

        #endregion

    }
}
