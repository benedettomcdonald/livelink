﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.ServiceProcess;
using Mx.Common.LiveLink;


namespace LiveLinkService
{
    [RunInstaller(true)]
    public partial class CoreServiceInstaller : Installer
    {
        public CoreServiceInstaller()
        {
            InitializeComponent();
            base.Committed += new InstallEventHandler(CoreServiceInstaller_Committed);
        }


        protected override void OnBeforeInstall(System.Collections.IDictionary savedState)
        {
            base.OnBeforeInstall(savedState);
            ConfigureService();
        }
        
        protected override void OnBeforeUninstall(System.Collections.IDictionary savedState)
        {
            base.OnBeforeUninstall(savedState);
            ConfigureService();
            try
            {
                // define a controller for the LiveLink service
                ServiceController controller = new ServiceController(WindowsService.LiveLinkServiceName);
                controller.Stop();
            }
            catch (Exception)
            {
            }
        }

        protected override void OnAfterInstall(IDictionary savedState)
        {
            base.OnAfterInstall(savedState);
        }

        protected override void OnCommitted(IDictionary savedState)
        {
            base.OnCommitted(savedState);
        }

        protected void CoreServiceInstaller_Committed(object sender, InstallEventArgs e)
        {
            System.Diagnostics.EventLog.WriteEntry("LiveLink Installer", "Extended Installation Prior OnCommitted", System.Diagnostics.EventLogEntryType.Information, 0);
            try
            {
                ApplyRecoveryOptionsResult result = ExtendedServiceInstaller.ApplyRecoveryOptions(WindowsService.LiveLinkServiceName);
                if (result == ApplyRecoveryOptionsResult.Success)
                    System.Diagnostics.EventLog.WriteEntry("LiveLink Installer", "Extended Installation Successfull", System.Diagnostics.EventLogEntryType.Information, 0);
                else
                    System.Diagnostics.EventLog.WriteEntry("LiveLink Installer", string.Format("Extended Installation Result:[{0}]", result.ToString()), System.Diagnostics.EventLogEntryType.Error, 0);
            }
            catch (Exception ex)
            {
                System.Diagnostics.EventLog.WriteEntry("LiveLink Installer", string.Format("Extended Installation Exception. Exception: {0}", ex.ToString()), System.Diagnostics.EventLogEntryType.Information, 0);
            }
        }


        internal System.ServiceProcess.ServiceProcessInstaller coreServiceProcessInstaller;
        internal System.Diagnostics.PerformanceCounterInstaller corePerformanceCounterInstaller;
        internal System.ServiceProcess.ServiceInstaller coreServiceInstaller;

        private void ConfigureService()
        {
            this.coreServiceProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
            this.coreServiceInstaller = new System.ServiceProcess.ServiceInstaller();
            this.corePerformanceCounterInstaller = new System.Diagnostics.PerformanceCounterInstaller();

            this.coreServiceProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
            this.coreServiceProcessInstaller.Password = null;
            this.coreServiceProcessInstaller.Username = null;

            this.coreServiceInstaller.Description = "MacromatiX LiveLink Service - Imports POS data to the MX system";
            this.coreServiceInstaller.ServiceName = WindowsService.LiveLinkServiceName;
            this.coreServiceInstaller.DisplayName = "MacromatiX LiveLink";
            this.coreServiceInstaller.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
                        
            // put a dependancy on SQL Server (this will ensure LiveLink service only starts after SQL server is running)
            string dependsOn = this.Context.Parameters["DEPENDONSERVICE"];
            this.coreServiceInstaller.ServicesDependedOn = new string[] { (string.IsNullOrEmpty(dependsOn) ? "MSSQLSERVER" : dependsOn) };

            this.Installers.AddRange(new System.Configuration.Install.Installer[] { this.coreServiceProcessInstaller, this.coreServiceInstaller });

        }
    }
}
