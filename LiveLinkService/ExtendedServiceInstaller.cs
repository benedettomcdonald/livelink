﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

namespace LiveLinkService
{
    internal enum RecoverAction
    {
        None = 0,
        Restart = 1,
        Reboot = 2,
        RunCommand = 3
    }

    internal enum ApplyRecoveryOptionsResult
    {
        Success = 0,
        GenericFailure = 1001,
        FailedToOpenServiceControlManager = 1002,
        FailedToOpenService = 1003,
        AccessDenied = 1004,
    }

    internal class ExtendedServiceInstaller
    {
        private const int SERVICE_ALL_ACCESS = 0xF01FF;
        private const int SC_MANAGER_ALL_ACCESS = 0xF003F;
        private const int SERVICE_CONFIG_DESCRIPTION = 0x1;
        private const int SERVICE_CONFIG_FAILURE_ACTIONS = 0x2;
        private const int SERVICE_NO_CHANGE = -1;
        private const int ERROR_ACCESS_DENIED = 5;

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
        public struct SERVICE_FAILURE_ACTIONS
        {
            public int dwResetPeriod;
            [MarshalAs(UnmanagedType.LPWStr)]
            public string lpRebootMsg;
            [MarshalAs(UnmanagedType.LPWStr)]
            public string lpCommand;
            public int cActions;
            public IntPtr lpsaActions;
        }

        [StructLayout(LayoutKind.Sequential)]
        public class SC_ACTION
        {
            public Int32 type;
            public UInt32 dwDelay;
        }

        // Win32 function to open the service control manager
        [DllImport("advapi32.dll")]
        public static extern IntPtr OpenSCManager(string lpMachineName, string lpDatabaseName, int dwDesiredAccess);

        // Win32 function to open a service instance
        [DllImport("advapi32.dll")]
        public static extern IntPtr OpenService(IntPtr hSCManager, string lpServiceName, int dwDesiredAccess);



        // Win32 function to change the service config for the failure actions.
        [DllImport("advapi32.dll", EntryPoint = "ChangeServiceConfig2")]
        public static extern bool ChangeServiceFailureActions(IntPtr hService, int dwInfoLevel, [MarshalAs(UnmanagedType.Struct)] ref SERVICE_FAILURE_ACTIONS lpInfo);

        [DllImport("advapi32.dll", CharSet = CharSet.Unicode, SetLastError = true, EntryPoint = "QueryServiceConfig2W")]
        public static extern Boolean QueryServiceConfig2(IntPtr hService, UInt32 dwInfoLevel, IntPtr buffer, UInt32 cbBufSize, out UInt32 pcbBytesNeeded);

        [DllImport("kernel32.dll")]
        public static extern int GetLastError();



        public static ApplyRecoveryOptionsResult ApplyRecoveryOptions(string serviceName)
        {
            ApplyRecoveryOptionsResult result = ApplyRecoveryOptionsResult.Success;
            IntPtr scmHndl = IntPtr.Zero;
            IntPtr svcHndl = IntPtr.Zero;
            IntPtr tmpBuf = IntPtr.Zero;
            IntPtr svcLock = IntPtr.Zero;
            bool rslt = false;

            // Place all our code in a try block
            try
            {
                // Open the service control manager
                scmHndl = OpenSCManager(null, null, SC_MANAGER_ALL_ACCESS);

                if (scmHndl.ToInt32() <= 0)
                    return ApplyRecoveryOptionsResult.FailedToOpenServiceControlManager;

                // Open the service
                svcHndl = OpenService(scmHndl, "AService1", SERVICE_ALL_ACCESS);

                if (svcHndl.ToInt32() <= 0)
                    return ApplyRecoveryOptionsResult.FailedToOpenService;

                ArrayList FailureActions = new ArrayList();
                // First Failure Actions and Delay (msec)
                FailureActions.Add(new FailureAction(RecoverAction.Restart, 60000));
                // Second Failure Actions and Delay (msec)
                FailureActions.Add(new FailureAction(RecoverAction.Restart, 120000));
                // Subsequent Failures Actions and Delay (msec)
                FailureActions.Add(new FailureAction(RecoverAction.None, 0));

                int numActions = FailureActions.Count;
                int[] myActions = new int[numActions * 2];
                int currInd = 0;

                foreach (FailureAction fa in FailureActions)
                {
                    myActions[currInd] = (int)fa.Type;
                    myActions[++currInd] = fa.Delay;
                    currInd++;
                }

                // Need to pack 8 bytes per struct
                tmpBuf = Marshal.AllocHGlobal(numActions * 8);

                // Move array into marshallable pointer
                Marshal.Copy(myActions, 0, tmpBuf, numActions * 2);

                // Set the SERVICE_FAILURE_ACTIONS struct
                SERVICE_FAILURE_ACTIONS sfa = new SERVICE_FAILURE_ACTIONS();

                sfa.cActions = 3;
                sfa.dwResetPeriod = 0;
                sfa.lpCommand = null;
                sfa.lpRebootMsg = null;
                sfa.lpsaActions = new IntPtr(tmpBuf.ToInt32());

                // Call the ChangeServiceFailureActions() abstraction of ChangeServiceConfig2()
                rslt = ChangeServiceFailureActions(svcHndl, SERVICE_CONFIG_FAILURE_ACTIONS, ref sfa);

                //Check the return
                if (!rslt)
                {
                    int err = GetLastError();
                    if (err == ERROR_ACCESS_DENIED)
                        return ApplyRecoveryOptionsResult.AccessDenied;
                }

                // Free the memory
                Marshal.FreeHGlobal(tmpBuf); tmpBuf = IntPtr.Zero;
            }
            catch (Exception ex)
            {
                result = ApplyRecoveryOptionsResult.GenericFailure;
            }

            return result;
        }
    }

    internal class FailureAction
    {
        private RecoverAction type = RecoverAction.None;
        private int delay = 0;

        // Default constructor
        public FailureAction() { }

        // Constructor
        public FailureAction(RecoverAction actionType, int actionDelay)
        {
            this.type = actionType;
            this.delay = actionDelay;
        }

        // Property to set recover action type
        public RecoverAction Type { get { return type; } set { type = value; } }

        // Property to set recover action delay
        public int Delay { get { return delay; } set { delay = value; } }
    }
}
