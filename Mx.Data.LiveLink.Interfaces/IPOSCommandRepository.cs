﻿using System;
using System.Collections.Generic;
using System.Text;
using Mx.Common;
using Mx.BusinessObjects.LiveLink;

namespace Mx.Data.LiveLink.Interfaces
{
    public interface IPOSCommandRepository
    {
        POSCommandData GetDataByType(string type);
        POSCommandData GetDataByType(string type, string options);
        bool InsertData(POSCommandData commandData);
        bool DeleteObsoleteData(int daysBeforeObsolete, string type);

        POSCommandSettings GetSettingsByType(string type);
        POSCommandSettings GetSettingsByID(int posCommandSettingsID);
        bool InsertSettings(POSCommandSettings commandSettings);

        POSCommandType GetTypeByType(string type);
        bool InsertType(POSCommandType commandType);
        POSCommandData GetDataByLikeCommand(string options);
    }
}
