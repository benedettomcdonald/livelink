﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mx.Data.LiveLink.Interfaces
{
    public interface ILiveLinkFactory
    {
        IPOSCommandRepository GetPOSCommandRepository();
        ISessionCommandRepository GetSessionCommandRepository();
    }
}
