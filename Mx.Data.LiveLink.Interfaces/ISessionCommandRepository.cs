﻿using System;
using System.Collections.Generic;
using System.Text;

using Mx.BusinessObjects.LiveLink;

namespace Mx.Data.LiveLink.Interfaces
{
    public interface ISessionCommandRepository
    {
        SessionCommandRequest GetRequestByID(int sessionCommandID);
        bool InsertRequest(SessionCommandRequest request);

        SessionCommandResponse GetResponseByID(int sessionCommandResponseID);
        SessionCommandResponseList GetResponseListByCommandID(int sessionCommandID);
        bool InsertResponse(SessionCommandResponse response);
    }
}
