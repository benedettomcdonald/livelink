Imports Mx.POS.Common

Public Class LiveLinkAbout
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents LabelTitle As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents lblVer As System.Windows.Forms.Label
    Friend WithEvents LabelVer As System.Windows.Forms.Label
    Friend WithEvents LabelId As System.Windows.Forms.Label
    Friend WithEvents TextId As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents lblCPU As System.Windows.Forms.Label
    Friend WithEvents lblDrive As System.Windows.Forms.Label
    Friend WithEvents lblIdVersion As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lblNIC As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(LiveLinkAbout))
        Me.LabelTitle = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.lblVer = New System.Windows.Forms.Label()
        Me.LabelVer = New System.Windows.Forms.Label()
        Me.LabelId = New System.Windows.Forms.Label()
        Me.TextId = New System.Windows.Forms.TextBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.lblIdVersion = New System.Windows.Forms.Label()
        Me.lblNIC = New System.Windows.Forms.Label()
        Me.lblDrive = New System.Windows.Forms.Label()
        Me.lblCPU = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'LabelTitle
        '
        Me.LabelTitle.BackColor = System.Drawing.Color.Transparent
        Me.LabelTitle.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelTitle.ForeColor = System.Drawing.Color.Navy
        Me.LabelTitle.Location = New System.Drawing.Point(16, 24)
        Me.LabelTitle.Name = "LabelTitle"
        Me.LabelTitle.Size = New System.Drawing.Size(392, 16)
        Me.LabelTitle.TabIndex = 77
        Me.LabelTitle.Text = "Live Link"
        Me.LabelTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(345, 267)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 1
        Me.Button1.Text = "Close"
        '
        'lblVer
        '
        Me.lblVer.BackColor = System.Drawing.Color.Transparent
        Me.lblVer.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVer.ForeColor = System.Drawing.Color.Navy
        Me.lblVer.Location = New System.Drawing.Point(64, 48)
        Me.lblVer.Name = "lblVer"
        Me.lblVer.Size = New System.Drawing.Size(312, 56)
        Me.lblVer.TabIndex = 5
        Me.lblVer.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LabelVer
        '
        Me.LabelVer.BackColor = System.Drawing.Color.Transparent
        Me.LabelVer.Font = New System.Drawing.Font("Verdana", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelVer.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.LabelVer.Location = New System.Drawing.Point(144, 0)
        Me.LabelVer.Name = "LabelVer"
        Me.LabelVer.Size = New System.Drawing.Size(280, 16)
        Me.LabelVer.TabIndex = 7
        Me.LabelVer.Text = "1.1.1"
        Me.LabelVer.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'LabelId
        '
        Me.LabelId.BackColor = System.Drawing.Color.Transparent
        Me.LabelId.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelId.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.LabelId.Location = New System.Drawing.Point(64, 192)
        Me.LabelId.Name = "LabelId"
        Me.LabelId.Size = New System.Drawing.Size(312, 16)
        Me.LabelId.TabIndex = 8
        Me.LabelId.Text = "Live Link Id:"
        Me.LabelId.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TextId
        '
        Me.TextId.BackColor = System.Drawing.SystemColors.Control
        Me.TextId.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextId.ForeColor = System.Drawing.Color.Black
        Me.TextId.Location = New System.Drawing.Point(64, 208)
        Me.TextId.Name = "TextId"
        Me.TextId.ReadOnly = True
        Me.TextId.Size = New System.Drawing.Size(312, 13)
        Me.TextId.TabIndex = 2
        Me.TextId.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.TextId.WordWrap = False
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.lblIdVersion)
        Me.GroupBox1.Controls.Add(Me.lblNIC)
        Me.GroupBox1.Controls.Add(Me.lblDrive)
        Me.GroupBox1.Controls.Add(Me.lblCPU)
        Me.GroupBox1.ForeColor = System.Drawing.SystemColors.ControlDark
        Me.GroupBox1.Location = New System.Drawing.Point(24, 110)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(384, 75)
        Me.GroupBox1.TabIndex = 78
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "SysInfo"
        '
        'lblIdVersion
        '
        Me.lblIdVersion.Location = New System.Drawing.Point(200, 48)
        Me.lblIdVersion.Name = "lblIdVersion"
        Me.lblIdVersion.Size = New System.Drawing.Size(176, 16)
        Me.lblIdVersion.TabIndex = 3
        Me.lblIdVersion.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblNIC
        '
        Me.lblNIC.Location = New System.Drawing.Point(16, 48)
        Me.lblNIC.Name = "lblNIC"
        Me.lblNIC.Size = New System.Drawing.Size(168, 16)
        Me.lblNIC.TabIndex = 2
        Me.lblNIC.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblDrive
        '
        Me.lblDrive.Location = New System.Drawing.Point(200, 24)
        Me.lblDrive.Name = "lblDrive"
        Me.lblDrive.Size = New System.Drawing.Size(176, 16)
        Me.lblDrive.TabIndex = 1
        Me.lblDrive.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblCPU
        '
        Me.lblCPU.Location = New System.Drawing.Point(16, 24)
        Me.lblCPU.Name = "lblCPU"
        Me.lblCPU.Size = New System.Drawing.Size(168, 16)
        Me.lblCPU.TabIndex = 0
        Me.lblCPU.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Navy
        Me.Label1.Location = New System.Drawing.Point(64, 237)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(312, 27)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Franchise and Multi-Store management software.  Live on the Net,  works the way y" & _
    "ou work."
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LiveLinkAbout
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.ClientSize = New System.Drawing.Size(432, 293)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.TextId)
        Me.Controls.Add(Me.LabelId)
        Me.Controls.Add(Me.LabelVer)
        Me.Controls.Add(Me.lblVer)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.LabelTitle)
        Me.Controls.Add(Me.Label1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "LiveLinkAbout"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "About Live Link"
        Me.GroupBox1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

    Private Sub LiveLinkAbout_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim UniqueId As String
        Dim Id As String
        Dim i As Byte
        Dim SysInfo As New clsSysInfo("!MAC!_!CPU!_!DriveSerial!")

        LabelTitle.Text = LiveLinkConfiguration.Brand
        Me.Text = LabelTitle.Text & " - LiveLink"
        LabelVer.Text = Mx.BusinessObjects.VersionUtil.GetBuild() + " (" + LiveLinkConfiguration.POSType + ")"
        lblVer.Text = Mx.BusinessObjects.VersionUtil.GetAppVersion()
        UniqueId = GetUniqueMachineId()
        For i = 1 To 5
            Id = Id & Mid(UniqueId, (i - 1) * 8 + 1, 8)
            If i < 5 Then
                Id = Id & "-"
            End If
        Next
        TextId.Text = Id
        lblCPU.Text = SysInfo.CPU
        lblNIC.Text = SysInfo.MAC
        lblDrive.Text = SysInfo.DriveSerial
        lblIdVersion.Text = "Id Version: 2"
    End Sub

End Class
