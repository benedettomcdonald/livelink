Imports Mx.POS.Common

Public Class Restart
    Private countDown As Integer = 10 ' seconds

    Private Sub ShowCountDown()
        LabelTimer.Text = String.Format("LiveLink will restart in {0} second(s)", countDown)
    End Sub

    Private Sub TimerRestart_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TimerRestart.Tick
        If countDown > 0 Then
            countDown -= 1
        Else
            RestartNow()
        End If
        ShowCountDown()
    End Sub

    Private Sub Restart_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ShowCountDown()
    End Sub

    Private Sub ButtonRestartNow_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonRestartNow.Click
        RestartNow()
    End Sub

    Private Sub RestartNow()
        LogEvent(EventLogEntryType.Information, "LiveLink is auto restarting")
        Application.Restart()
    End Sub

End Class