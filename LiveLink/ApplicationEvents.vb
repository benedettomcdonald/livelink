Imports Mx.POS.Common
Imports Microsoft.VisualBasic.Devices
Imports Microsoft.VisualBasic.ApplicationServices
Imports System.Text
Imports System.Threading
Imports System.Globalization
Imports System.IO
Imports System.Xml

Namespace My

    Class MyApplication

        'NetworkAvailabilityChanged: Raised when the network 
        'connection is connected or disconnected.
        Private Sub MyApplication_NetworkAvailabilityChanged(ByVal sender As Object, _
          ByVal e As NetworkAvailableEventArgs) _
          Handles Me.NetworkAvailabilityChanged
        End Sub

        'Shutdown: Raised after all application forms are closed.  
        'This event is not raised if the application is terminating abnormally.
        Private Sub MyApplication_Shutdown(ByVal sender As Object, _
           ByVal e As System.EventArgs) Handles Me.Shutdown
        End Sub

        'Startup: Raised when the application starts, 
        'before the startup form is created.
        Private Sub MyApplication_Startup(ByVal sender As Object, _
                  ByVal e As StartupEventArgs) Handles Me.Startup

            UpdateAppsettings()

            If (Mx.Configuration.ConfigurationHelper.IsBaseConfigured()) Then

                If LiveLinkConfiguration.IntouchUseLocalCulture Then
                    Globals.NumberCulture = Thread.CurrentThread.CurrentCulture
                    Globals.DateCulture = Thread.CurrentThread.CurrentCulture
                End If

                Thread.CurrentThread.SetApartmentState(ApartmentState.STA)
                Thread.CurrentThread.CurrentCulture = New CultureInfo("en-AU", False)
                If Not LiveLinkConfiguration.IntouchUseLocalCulture Then
                    Globals.NumberCulture = Thread.CurrentThread.CurrentCulture
                    Globals.DateCulture = Thread.CurrentThread.CurrentCulture
                End If

            End If

        End Sub



        Private Sub UpdateAppsettings()
            Dim appPath As String = System.Windows.Forms.Application.StartupPath


            ' if the file apsettings.config does not exist, create it and copy the values from MxLiveLink.config and rename
            ' MxLiveLink.config to MxLiveLink.config.old


            If File.Exists(Path.Combine(appPath, "appSettings.config")) OrElse Not File.Exists(Path.Combine(appPath, "MXLiveLink.config")) Then Exit Sub

            Dim oldConfig As New XmlDocument()
            oldConfig.Load(Path.Combine(appPath, "MXLiveLink.config"))
            Dim appSettingsNode As XmlNode = oldConfig.SelectSingleNode("configuration/appSettings")

            Dim newConfig As New XmlDocument()
            newConfig.LoadXml(appSettingsNode.OuterXml)
            newConfig.Save(Path.Combine(appPath, "appSettings.config"))

            Dim renPath As String = Path.Combine(appPath, "MXLiveLink.config.old - use appSettings.config file instead")
            If (Not File.Exists(renPath)) Then
                File.Move(Path.Combine(appPath, "MXLiveLink.config"), renPath)
            End If

        End Sub

        'StartupNextInstance: Raised when launching a single-instance 
        'application and the application is already active. 
        Private Sub MyApplication_StartupNextInstance(ByVal sender As Object, _
              ByVal e As StartupNextInstanceEventArgs) _
              Handles Me.StartupNextInstance
            LogEvent(EventLogEntryType.Information, "An attempt was made to start LiveLink but LiveLink was already running")
            Try
                If LiveLinkConfiguration.SecondInstanceAttemptKeepMinimized Then
                    ' There is a problem when trying to keep the form minimised.
                    ' It comes up Normalised by default, so hide it and then minimize it with a timer.
                    Me.MainForm.Hide()
                    LiveLinkMain.minimiseTimer.Enabled = True
                Else
                    LiveLinkMain.WindowState = FormWindowState.Normal
                End If
                If LiveLinkMain.LaunchHidden Then
                    ' Ensure that the system tray icon is present (it sometimes disappears - not sure why yet)
                    LiveLinkMain.NotifyIcon1.Visible = False
                    LiveLinkMain.NotifyIcon1.Visible = True
                End If
            Catch ex As Exception
                LogEvent(EventLogEntryType.Warning, "Unable to normalise LiveLink from StartUpNextInstance - {0}", ex.Message)
            End Try
        End Sub

        'Unhandled exception: 
        Private Sub MyApplication_UnhandledException(ByVal sender As Object, _
                  ByVal e As UnhandledExceptionEventArgs) _
                  Handles Me.UnhandledException
            Dim Msg As New StringBuilder

            ' Note that this event will not fire when running in the debugger.
            Msg.AppendFormat("Unhandled Exception: {0}", e.Exception.Message)
            Msg.AppendLine()
            Msg.AppendLine()
            Msg.AppendFormat("Stack trace: {0}", vbCrLf & e.Exception.StackTrace)
            LogEvent(Msg.ToString, EventLogEntryType.Error)
            Dim restartForm As New Restart
            restartForm.ShowDialog()
            LogEvent("Live Link Shut Down - Unhandled exception", EventLogEntryType.Warning)
        End Sub

    End Class

End Namespace