<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Restart
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Restart))
        Me.LabelMsg = New System.Windows.Forms.Label
        Me.ButtonRestartNow = New System.Windows.Forms.Button
        Me.ButtonCancel = New System.Windows.Forms.Button
        Me.TimerRestart = New System.Windows.Forms.Timer(Me.components)
        Me.LabelTimer = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'LabelMsg
        '
        Me.LabelMsg.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelMsg.Location = New System.Drawing.Point(60, 24)
        Me.LabelMsg.Name = "LabelMsg"
        Me.LabelMsg.Size = New System.Drawing.Size(302, 48)
        Me.LabelMsg.TabIndex = 0
        Me.LabelMsg.Text = "A LiveLink error has occurred and the application needs to shut down."
        Me.LabelMsg.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ButtonRestartNow
        '
        Me.ButtonRestartNow.Location = New System.Drawing.Point(134, 138)
        Me.ButtonRestartNow.Name = "ButtonRestartNow"
        Me.ButtonRestartNow.Size = New System.Drawing.Size(75, 23)
        Me.ButtonRestartNow.TabIndex = 1
        Me.ButtonRestartNow.Text = "&Restart Now"
        Me.ButtonRestartNow.UseVisualStyleBackColor = True
        '
        'ButtonCancel
        '
        Me.ButtonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.ButtonCancel.Location = New System.Drawing.Point(245, 138)
        Me.ButtonCancel.Name = "ButtonCancel"
        Me.ButtonCancel.Size = New System.Drawing.Size(75, 23)
        Me.ButtonCancel.TabIndex = 2
        Me.ButtonCancel.Text = "&Cancel"
        Me.ButtonCancel.UseVisualStyleBackColor = True
        '
        'TimerRestart
        '
        Me.TimerRestart.Enabled = True
        Me.TimerRestart.Interval = 1000
        '
        'LabelTimer
        '
        Me.LabelTimer.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelTimer.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.LabelTimer.Location = New System.Drawing.Point(1, 92)
        Me.LabelTimer.Name = "LabelTimer"
        Me.LabelTimer.Size = New System.Drawing.Size(447, 23)
        Me.LabelTimer.TabIndex = 3
        Me.LabelTimer.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Restart
        '
        Me.AcceptButton = Me.ButtonRestartNow
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.CancelButton = Me.ButtonCancel
        Me.ClientSize = New System.Drawing.Size(449, 181)
        Me.Controls.Add(Me.LabelTimer)
        Me.Controls.Add(Me.ButtonCancel)
        Me.Controls.Add(Me.ButtonRestartNow)
        Me.Controls.Add(Me.LabelMsg)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Restart"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Error - Restart LiveLink?"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents LabelMsg As System.Windows.Forms.Label
    Friend WithEvents ButtonRestartNow As System.Windows.Forms.Button
    Friend WithEvents ButtonCancel As System.Windows.Forms.Button
    Friend WithEvents TimerRestart As System.Windows.Forms.Timer
    Friend WithEvents LabelTimer As System.Windows.Forms.Label
End Class
