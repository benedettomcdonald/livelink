CREATE PROCEDURE [dbo].[mxLiveLink_ICG_GetDiscounts]
	@PosRegisterID varchar(10)
,	@TransactionID bigint
,	@Update varchar(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	select
		round(discount.totdtocomerc,2)	Amount				
	,	discount.coddto					TypeID
	,	isnull(discount.Descripcion,'')	Description
	,	discount.esgasto				Paid
	from
		albventatot discount with (nolock)
	where
		discount.serie=@PosRegisterID
	and	discount.numero=@TransactionID
	and	discount.n=@Update
	and	discount.totdtocomerc<>0


END
