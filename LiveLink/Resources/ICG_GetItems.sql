CREATE PROCEDURE [dbo].[mxLiveLink_ICG_GetItems]
	@PosRegisterID varchar(10)
,	@TransactionID bigint
,	@Update varchar(1)
WITH RECOMPILE
AS
BEGIN

	/* items */
	select
		line.numlin				SequenceNo
	,	line.numlin				ParentSequenceNo
	,	0						ModifierSequenceNo
	,	line.codarticulo		ItemID
	,	line.descripcion		Description
	,	line.unidadestotal		Quantity
	,	round(line.precio,2)	NetAmount
	,	round(line.precioiva,2)	GrossAmount
	,	line.iva				TaxRate
	,	line.tipo				ItemType
	,	round(line.preciodefecto,2)	DefaultPrice
	,	0						[Level]
	from
		albventalin line with (nolock)
	where
		line.numserie=@PosRegisterID
	and	line.numalbaran=@TransactionID
	and	line.n=@Update

	union all

	/* Modifiers */
	select
		0						SequenceNo
	,	modifier.numlinea		ParentSequenceNo
	,	modifier.nummodif		ModifierSequenceNo
	,	modifier.codarticulo	ItemID
	,	modifier.descripcion	Description
	,	1						Quantity
	,	0.0						NetAmount
	,	0.0						GrossAmount
	,	0						TaxRate
	,	line.tipo				ItemType
	,	0.0						DefaultPrice
	,	modifier.nivel			[Level]
	from
		albventalin line with (nolock)
	join albventamodif modifier with (nolock)
	on	line.numserie=modifier.numserie
	and	line.numalbaran=modifier.numalbaran
	and	line.numlin=modifier.numlinea
	and	line.numserie=@PosRegisterID
	and	line.numalbaran=@TransactionID
	and	line.n=@Update
	where
		((modifier.codarticulo = 0 AND modifier.descripcion NOT like '#%')	-- To remove clerk added text comments
	OR modifier.codarticulo <> 0)

	order by
		ParentSequenceNo
	,	ModifierSequenceNO
	
END



