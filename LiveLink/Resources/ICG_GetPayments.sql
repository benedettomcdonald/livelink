CREATE PROCEDURE [dbo].[mxLiveLink_ICG_GetPayments]
	@PosRegisterID varchar(10)
,	@TransactionID bigint
,	@Update varchar(10)
AS
BEGIN

	SET NOCOUNT ON;

	select
		payment.importe								Amount
	,	payment.codformapago						TypeID
	,	isnull(paymenttype.Descripcion,'ROUNDING')	Description
	,	payment.fechamodificado						ModificationTime
	from
		tesoreria payment with (nolock)
	left join formaspago paymenttype with (nolock)
	on	payment.codformapago=paymenttype.codformapago
	where
		payment.serie=@PosRegisterID
	and	payment.numero=@TransactionID
	and	payment.n=@Update
	and	payment.tipodocumento='F'

END
