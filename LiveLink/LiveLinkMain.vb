'(C) MacromatiX Pty Ltd 2000-2004

Imports System.IO
Imports System.Net
Imports System.Math
Imports System.Text
Imports System.Threading
Imports System.Xml
Imports Mx.BusinessObjects.LiveLink
Imports Mx.POS.Common.Composite
Imports Mx.LiveLink.Core
Imports Mx.POS.Common.BusinessObjects
Imports Mx.POS.Common
Imports Microsoft.Win32
Imports System.Security.Principal.WindowsIdentity
Imports System.Xml.Serialization
Imports System.Collections.Generic
Imports Mx.Configuration
Imports MMS.Utilities
Imports MMS.Utilities.MMSUtils.ManageFiles
Imports Mx.Services
Imports Mx.Services.BaseSystem.Polling
Imports Mx.NewPOS
Imports Mx.BusinessObjects
Imports Mx.YIS
Imports Mx.BusinessObjects.YIS
Imports Mx.POS.Breeze
Imports Mx.POS.Xpient
Imports Mx.POS.Retalix
Imports Mx.POS.ARTS
Imports Mx.POS.Compris
Imports Mx.POS.Aloha
Imports Mx.POS.PAR
Imports Mx.POS.POSI
Imports Mx.POS.InTouch
Imports Mx.POS.Octane
Imports Mx.POS.ICG
Imports Mx.POS.MicrosoftRMS
Imports Mx.POS.iPOS
Imports Mx.POS.ZKSoftware
Imports Mx.POS.EasyClock
Imports Mx.POS.Micros
Imports Mx.POS.Uniwell
Imports Mx.POS.SpeedOfService
Imports Mx.LiveLink.Data
Imports Mx.POS.POSixml
Imports Mx.POS.RPOS
Imports Mx.LiveLink.Interface
Imports Mx.POS.TimePunchImport
Imports Mx.POS.SUS
Imports Mx.TrafficData.AlertSystems

' TODO: Enable GZip support: Imports MMS.WebServices

Public Class LiveLinkMain
    Inherits System.Windows.Forms.Form

#Region " Constants and Global Variables "

#Region "Constants"
    Private Const Transight As String = "Transight"
#End Region

#Region "Common POS Private Properties"

    Private _posCommon As ICompositePOS

#End Region

    Private _livelinkCore As LiveLinkCore
    ' enum for command line options (set the ClassName value to the expected switch. For example: /s /h /? )
    ' also add the handling of this value in the function "ParseCommandLine()"
    Private Enum CommandLineArguments
        <EnumClassName("/s")> StoreNumber
    End Enum

    Private Const _commandLineUsage As String = "LiveLink.exe [/s:StoreNumber]"
    Private _argumentStoreNumber As String = String.Empty

    Const MaxCommsBackoffMinutes As Integer = 60 * 4 ' 4 hours
    Const cLogDataFormat = "Information"
    Const GrindProcessTimeout = 120 ' (seconds) Assume that if the Grind process (Aloha) is still running after two minutes it has a Windows MsgBox on the screen and is locked

    Const cSalesRecordTypes = "'H', 'I', 'F', 'C', 'X', 'S', 'V', 'M', 'IM', 'SI', 'TLD', 'FUEL', 'VALIDATION', 'P', 'IV','STOREDVALUE'" ' Header, Item, Financial, Control, Miscellaneous, Speed of Service, Inventory, Customer Information, Item Modifier, Store Information, Compressed Transaction Level Data (TLD), Fuel, Product, Validation level data,Item Void,StoredValue
    Const AutoStartOnLogin As String = "AutoStartOnLogin"

    ' LiveLink restart/shutdown behaviour variables
    Private LiveLinkShutDownFile As String = Path.Combine(Application.StartupPath, "Shutdown.txt")
    Private LiveLinkRestartFrequency As Integer = 0
    Private LiveLinkStartTime As DateTime = DateTime.Now
    Private lastKeepAlive As DateTime = DateTime.Now

    Public MySalesData As DataSet
    Public CurrentCount As Long = 0
    Public AwakeCount As Long = 0
    Public DCACount As Long = 0
    'Public CancelFlag As Boolean = False

    Public CurrentDayPAR As Date = Format(Now, "dd-MMM-yyyy")
    Public CurrentDayAloha As Date = Format(Now, "dd-MMM-yyyy")

    Public UserAccessStatus As Boolean = False
    Public bNoMSA_Flag As Boolean = False
    Private MyConnectionString As String
    Public bTokenMode As Boolean = 0
    Private _okToSend As Boolean = True ' LiveLink will only attempt communications if this flag is true
    Public CommsBackoffMinutes As Integer = 1 ' Start with a backoff time of 1 minute if there is a comms error
    Public CommsBackoffCount As Integer = 0 ' Counter used in backoff timer
    Private _identity As String = String.Empty
    Private mySessionKey As String = String.Empty
    Private myConditionalSessionKey As String = String.Empty ' Session key to use when site is not activated (allows for limited comms)
    Private myWebService As String
    Private myFlowTxInBatch As Integer ' Number of transaction to batch in a single web service call
    Private myFlowIterations As Integer ' Number of loops to perform when sending transactions
    Private myFlowMaxRecords As Integer ' Maximum number of records (line items) in a single call
    Private myFlowInterval As Integer ' Number of minutes to wait before sending the next batch of data
    Private myAwakeInterval As Integer ' Number of minutes between sending of heartbeat messages 
    Private myAwakeCounterVerbose As Integer = 8 ' Number of heartbeat messages between verbose awake messages
    Private LeaseExpired As Boolean = True ' Has session lease expired - if so obtain new lease
    Private UseCompression As Boolean = True
    Private TokenMode As Boolean = True
    Public LaunchHidden As Boolean = False
    Private StartupSent As Boolean = False
    Private AwakeCounter As Integer = 0 ' Used to count each Awake Message sent
    Private MessageWaiting As Boolean = False
    Private DebugMode As DebugModes = DebugModes.Off
    Private Processing As Boolean = False
    Private UseMxGetProcessesbyName As Boolean = False ' There is a known bug in DOT NET where certain machines fail on the Process.GetProcessesByName function.
    Private _pingEnabled As Boolean = False ' If this is true, LiveLink will poll the web server every 30 seconds to see if it is there

    Private MyDontProcessBefore As String = String.Empty

    ' Pre-Send Processing variables
    Private MyPreSendProcess As String
    ' Pre-Send Process - PLUMAP - PLU Mapping variables
    Private Const MyPLUMapFile As String = "PLUMap.XML"
    Private MyPLUMapXmlDoc As XmlDocument

    ' POS Command variables
    Private MyPOSMessage As ComprisPOSMessage

    ' Logo branding variables
    Private MyLogoImage As String
    Private MyLogoColor As String

    ' Live Sync variables
    Private liveSyncLastPoll As DateTime

    ' YIS interface variables
    Private yisConsumerPOSControl As Mx.YIS.Consumer.POSControlResponse
    Private yisConsumerRDSDisplay As Mx.YIS.Consumer.POSControlResponse
    Private yisConsumerHMETimer As Mx.YIS.Consumer.SOSTimer
    Private yisConsumerQSRTimer As Mx.YIS.Consumer.SOSTimer

    ' YIS Controller application
    Private yisControl As YISController
    Private yisThread As Thread


    ' PAR Gateway variables.

    Private WithEvents MyParFile As ParFile
    Private WithEvents MyParTLDServer As ParTLDServer
    Private WithEvents MyParTLDDayServer As ParTLDDayServer

    Private MyParTLDListen As Boolean
    Private MyParUseTLDServer As Boolean
    Private MyParTLDNumberOfDaysToRecover As Integer
    Private MyParEnabled As Boolean = False
    Private MyParGetTLDExe As String
    Private MyParGetAllTLDsExe As String
    Private MyParImportFilePath As String
    Private MyParArchiveFolderPath As String
    Private MyParPollInterval As Integer

    Private MyParPerformFullImport As Boolean = True
    Private MyParLastPoll As DateTime = DateTime.MinValue 'DateTime.Now
    Private MyParFirstPollDone As Boolean = False

    Private MyParDateStart As DateTime
    Private MyParImportedCount As Integer
    Private MyParImportedTotalCount As Integer
    Private MyParSkippedCount As Integer
    Private MyParSkippedTotalCount As Integer

    ' POSI Gateway variables.

    Private WithEvents MyPosiDatabase As PosiDatabase

    Private MyPosiEnabled As Boolean = False
    Private MyPosiConnectionString As String
    Private MyPosiPollInterval As Integer
    Private MyDCAEnabled As Boolean = False

    Private MyPosiLastPoll As DateTime = DateTime.Now
    Private MyPosiFirstPollDone As Boolean = False

    Private MyPosiImportedCount As Integer
    Private MyPosiSkippedCount As Integer

    Private MyAloha As AlohaV2 = Nothing
    Private MyAlohaEnabled As Boolean = False
    Private MyAlohaGrindExe As String
    Private MyAlohaImportFilePath As String
    Private MyAlohaPollInterval As Integer
    Private MyAlohaPreviousDaysToCheck As Integer
    Private MyAlohaPerformFullImport As Boolean = True
    Private MyAlohaLastPoll As DateTime = DateTime.MinValue 'DateTime.Now
    Private MyAlohaFirstPollDone As Boolean = False
    Private MyAlohaServiceType As String
    Private MyAlohaQuickServiceSOS As Boolean = False
    Private MyAlohaTimeAttendanceEnabled As Boolean = True
    Private MyAlohaVersion As String = String.Empty
    Private MyAlohaServiceTypeMapping As String

    Private MyMicrosEnabled As Boolean = False
    Private MyMicrosGatewayVersion As String
    Private MyMicrosPollInterval As Integer
    Private MyMicrosRevenueCentreFilter As String ' comma delimited list of revenue centres to include.  If "0" or nothing or missing, no filter is applied and all records are imported.  This was done specifically for Sumo Salad Airport T2 which has a single Micros system and different companies.
    Private MyMicrosPreviousDaysToCheck As Integer
    Private MyMicrosPerformFullImport As Boolean = True
    Private MyMicrosLastPoll As DateTime = DateTime.MinValue 'DateTime.Now
    Private MyMicrosFirstPollDone As Boolean = False

    ' COMPRIS Variables

    Private MyComprisEnabled As Boolean = False
    Private MyCompris As MxCompris.Compris
    Private WithEvents MyComprisListener As MxCompris.ComprisListener
    Private MyComprisXMLFilePath As String
    Private MyComprisPollInterval As Integer
    Private MyComprisLastPoll As DateTime = DateTime.MinValue
    Private MyComprisFirstPollDone As Boolean = False
    Private MyComprisActive As Boolean = False
    Private MyComprisActiveAccessCode As String
    Private MyComprisActiveMeKeySOD As Integer
    Private MyComprisActiveMeKeyEOD As Integer
    Private MyComprisActiveScriptPreSOD As String
    Private MyComprisActiveScriptPostSOD As String
    Private MyComprisActiveScriptPreEOD As String
    Private MyComprisActiveScriptPostEOD As String
    Private MyComprisActiveRegisterCount As Integer
    Private MyComprisActiveMessages As ComprisActiveMessages = Nothing
    Private MyComprisErrorDialog As ComprisPOSMessage = Nothing

    ' COMPRIS - PDP data access
    Private MyComprisPDPLastPoll As DateTime
    Private MyComprisPDPUpdatesLastPoll As DateTime
    Private MyComprisPollFilesLastPoll As DateTime

    ' PANASONIC Variables

    Private MyPanasonicEnabled As Boolean = False
    Private MyPanasonicPollInterval As Integer
    Private MyPanasonicLastPoll As DateTime = DateTime.MinValue
    Private MyPanasonicFirstPollDone As Boolean = False
    Private MyPanasonicDatabaseName As String

    ' INTOUCH Variables

    Private MyIntouch As Intouch
    Private MyIntouchAutomatedExport As Boolean
    Private MyIntouchDrawerPullsEnabled As Boolean
    Private MyIntouchExportExe As String
    Private MyIntouchExportFilePath As String
    Private MyIntouchItemCodeField As String
    Private MyIntouchItemDescriptionField As String
    Private MyIntouchModifierCodeField As String
    Private MyIntouchModifierDescriptionField As String
    Private MyIntouchServiceTypeMappings As String
    Private MyIntouchEnabled As Boolean = False
    Private MyIntouchPollInterval As Integer
    Private MyIntouchLastPoll As DateTime = DateTime.MinValue
    Private MyIntouchLatestExport As DateTime = DateTime.MinValue
    Private MyIntouchLatestExportChanged As Boolean = False
    Private MyIntouchFirstPollDone As Boolean = False


    ' OCTANE Variables
    Private MyOctane As Octane
    Private MyOctaneFirstPollDone As Boolean = False
    Private MyOctaneLastPoll As DateTime = DateTime.MinValue

    ' ARTS Variables
    Private MyArtsFirstPollDone As Boolean = False
    Private MyArtsLastPoll As DateTime = DateTime.MinValue

    ' ICG Variables
    Private MyICG As ICG.ICGProcess
    Private MyICGLastPoll As DateTime = DateTime.MinValue
    Private MyICGFirstPollDone As Boolean = False

    ' Microsoft RMS Variables
    Private MyRMS As MicrosoftRMS.RMS
    Private MyRMSLastPoll As DateTime = DateTime.MinValue
    Private MyRMSFirstPollDone As Boolean = False

    ' NewPOS variables
    Private MyNewPOS As NewPOS
    Private MyNewPOSFirstPollDone As Boolean = False
    Private MyNewPOSLastPoll As DateTime = DateTime.MinValue

    ' iPOS variables
    Private MyiPOS As iPOS
    Private MyiPOSFirstPollDone As Boolean = False
    Private MyiPOSLastPoll As DateTime = DateTime.MinValue
    Private MyiPOSUpdatingiPOSDatabase As Boolean = False

    ' Uniwell variables
    Private MyUniwellFirstPollDone As Boolean = False
    Private MyUniwellLastPoll As DateTime = DateTime.MinValue

    ' Radiant variables
    Private MyRadiantFirstPollDone As Boolean = False
    Private MyRadiantLastPoll As DateTime = DateTime.MinValue

    ' NAXML variables
    Private MyNAXMLFirstPollDone As Boolean = False
    Private MyNAXMLLastPoll As DateTime = DateTime.MinValue

    ' Xpient variables
    Private MyXpientFirstPollDone As Boolean = False
    Private MyXpientLastPoll As DateTime = DateTime.MinValue
    Private MyXpientPOS As XpientPOS

    ' Retalix variables
    Private MyRetalixFirstPollDone As Boolean = False
    Private MyRetalixLastPoll As DateTime = DateTime.MinValue
    Private MyRetalixPOS As RetalixPOS

    ' Breeze variables
    Private breezeImport As BreezePOS
    Private MyBreezeFirstPollDone As Boolean = False
    Private MyBreezeLastPoll As DateTime = DateTime.MinValue

    ' POSixml Variables
    Private MyPOSixmlFirstPollDone As Boolean = False
    Private MyPOSixmlLastPoll As DateTime = DateTime.MinValue

    ' RPOS Variables
    Private MyRPOSFirstPollDone As Boolean = False
    Private MyRPOSLastPoll As DateTime = DateTime.MinValue

    ' ZK Fingerprint Device
    Private MyZKFingerprint As zkFingerPrint
    Private MyZKFingerprintFirstPollDone As Boolean = False
    Private MyZKFingerprintLastPoll As DateTime = DateTime.MinValue
    Private MyZKUsersView As zkUsersView

    Private MyZKServer As zkCommunications.zkServer
    Private MyZKThread As Thread

    ' Time Punch Import variables
    Private MyTimePunchImportLastPoll As DateTime = DateTime.MinValue
    Private MyTimePunchImportPOS As TimePunchImportPOS

    ' SUS POS Import variables
    Private MySusPosEnabled As Boolean
    Private MySusPosGatewayEnabled As Boolean
    Private MySUSPosPollInterval As Integer
    Private MySUSPOSImportLastPoll As DateTime = DateTime.MinValue
    Private MySUSPOSImportPOS As SUSPOS
    Private MySUSPOSFirstPollDone As Boolean = False

    ' Alert Systems Traffic Data
    Private MyAlertSystemsTrafficDataTransferEnabled As Boolean
    Private MyAlertSystemsGatewayEnabled As Boolean
    Private MyAlertSystemsTrafficDataPollInterval As Integer
    Private MyAlertSystemsTrafficDataFirstPollDone As Boolean = False
    Private MyAlertSystemsTrafficDataLastPoll As DateTime = DateTime.MinValue

    ' Speed of service reporting
    Private MyServiceReportLastPoll As DateTime = DateTime.MinValue

    ' Menu package 
    Private MyMenuPackageLastPoll As DateTime = DateTime.MinValue

    ' TimeClock variables
    Private WithEvents MyTimeClockInstance As IEasyClock
    Private MyTimeClockAvailable As Boolean = False
    Private MyTimeClockSyncInterval As Integer = 12
    Private MyTimeClockIntervalCount As Integer = 0
    Private MyTimeClockForceUserSync As Boolean = False
    Private MyTimeClockLoginWindow As Integer
    Private MyTimeClockAllowBreaks As Boolean = False
    Private MyTimeClockAllowNewEmployees As Boolean = True
    Private MyTimeClockAllowClose As Boolean = True
    Friend WithEvents ButtonNewMachineID As System.Windows.Forms.Button
    Friend WithEvents btnReceiveMessage As System.Windows.Forms.Button
    Private MyTimeClockAutoShow As Boolean = False
    Friend WithEvents chkAutoStartOnLogin As System.Windows.Forms.CheckBox
    Friend WithEvents LabelAlohaLastDateProcessed As System.Windows.Forms.Label
    Friend WithEvents txtAlohaProcessDate As System.Windows.Forms.Label
    Friend WithEvents ButtonAlohaProcessDateApply As System.Windows.Forms.Button
    Friend WithEvents Label38 As System.Windows.Forms.Label
    Friend WithEvents dtpAlohaProcessDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents tabOctane As System.Windows.Forms.TabPage
    Friend WithEvents CheckBoxOctaneEnabled As System.Windows.Forms.CheckBox
    Friend WithEvents ButtonOctaneImport As System.Windows.Forms.Button
    Friend WithEvents Label42 As System.Windows.Forms.Label
    Friend WithEvents txtOctaneNextPoll As System.Windows.Forms.Label
    Friend WithEvents Label46 As System.Windows.Forms.Label
    Friend WithEvents txtOctaneLastPoll As System.Windows.Forms.Label
    Friend WithEvents Label49 As System.Windows.Forms.Label
    Friend WithEvents txtOctanePollInterval As System.Windows.Forms.Label
    Friend WithEvents txtOctaneCurrentTime As System.Windows.Forms.Label
    Friend WithEvents Label52 As System.Windows.Forms.Label
    Friend WithEvents tabOfflineCashManager As System.Windows.Forms.TabPage
    Friend WithEvents Label45 As System.Windows.Forms.Label
    Friend WithEvents cmdRefreshSettings As System.Windows.Forms.Button
    Friend WithEvents tabZKFingerprint As System.Windows.Forms.TabPage
    Friend WithEvents CheckBoxZKEnabled As System.Windows.Forms.CheckBox
    Friend WithEvents ButtonZKFingerprintImport As System.Windows.Forms.Button
    Friend WithEvents lbZKNextPoll As System.Windows.Forms.Label
    Friend WithEvents txtZKNextPoll As System.Windows.Forms.Label
    Friend WithEvents lbZKLastPoll As System.Windows.Forms.Label
    Friend WithEvents txtZKLastPoll As System.Windows.Forms.Label
    Friend WithEvents lbZKPollInterval As System.Windows.Forms.Label
    Friend WithEvents txtZKPollInterval As System.Windows.Forms.Label
    Friend WithEvents txtZKCurrentTime As System.Windows.Forms.Label
    Friend WithEvents lbZKCurrentTime As System.Windows.Forms.Label
    Friend WithEvents lbZKDevicePort As System.Windows.Forms.Label
    Friend WithEvents lbZKDeviceIP As System.Windows.Forms.Label
    Friend WithEvents txtZKDevicePort As System.Windows.Forms.TextBox
    Friend WithEvents txtZKDeviceIP As System.Windows.Forms.TextBox
    Friend WithEvents tmrPing As System.Windows.Forms.Timer
    Friend WithEvents PictureBoxBackoff As System.Windows.Forms.PictureBox
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents PictureBoxPingEnabled As System.Windows.Forms.PictureBox
    Friend WithEvents tabICG As System.Windows.Forms.TabPage
    Friend WithEvents CheckBoxICGEnabled As System.Windows.Forms.CheckBox
    Friend WithEvents lbICGNextPoll As System.Windows.Forms.Label
    Friend WithEvents txtICGNextPoll As System.Windows.Forms.Label
    Friend WithEvents lbICGLastPoll As System.Windows.Forms.Label
    Friend WithEvents txtICGLastPoll As System.Windows.Forms.Label
    Friend WithEvents lbICGPollInterval As System.Windows.Forms.Label
    Friend WithEvents txtICGPollInterval As System.Windows.Forms.Label
    Friend WithEvents txtICGCurrentTime As System.Windows.Forms.Label
    Friend WithEvents lbICGCurrentTime As System.Windows.Forms.Label
    Friend WithEvents ButtonICGImport As System.Windows.Forms.Button
    Friend WithEvents tabRMS As System.Windows.Forms.TabPage
    Friend WithEvents CheckBoxRMSEnabled As System.Windows.Forms.CheckBox
    Friend WithEvents lbRMSNextPoll As System.Windows.Forms.Label
    Friend WithEvents txtRMSNextPoll As System.Windows.Forms.Label
    Friend WithEvents lbRMSLastPoll As System.Windows.Forms.Label
    Friend WithEvents txtRMSLastPoll As System.Windows.Forms.Label
    Friend WithEvents lbRMSPollInterval As System.Windows.Forms.Label
    Friend WithEvents txtRMSPollInterval As System.Windows.Forms.Label
    Friend WithEvents txtRMSCurrentTime As System.Windows.Forms.Label
    Friend WithEvents lbRMSCurrentTime As System.Windows.Forms.Label
    Friend WithEvents ButtonRMSImport As System.Windows.Forms.Button
    Friend WithEvents tabProxy As System.Windows.Forms.TabPage
    Friend WithEvents chkUseProxy As System.Windows.Forms.CheckBox
    Friend WithEvents grpProxy As System.Windows.Forms.GroupBox
    Friend WithEvents radCustomProxy As System.Windows.Forms.RadioButton
    Friend WithEvents radUseIEProxy As System.Windows.Forms.RadioButton
    Friend WithEvents lblProxyServer As System.Windows.Forms.Label
    Friend WithEvents grpProxyAuth As System.Windows.Forms.GroupBox
    Friend WithEvents Label51 As System.Windows.Forms.Label
    Friend WithEvents Label50 As System.Windows.Forms.Label
    Friend WithEvents Label48 As System.Windows.Forms.Label
    Friend WithEvents txtCustomProxy As System.Windows.Forms.TextBox
    Friend WithEvents txtSystemProxy As System.Windows.Forms.TextBox
    Friend WithEvents cboProxyAuth As System.Windows.Forms.ComboBox
    Friend WithEvents txtProxyDomain As System.Windows.Forms.TextBox
    Friend WithEvents txtProxyUserName As System.Windows.Forms.TextBox
    Friend WithEvents btnSaveProxySettings As System.Windows.Forms.Button
    Friend WithEvents txtProxyPassword As System.Windows.Forms.TextBox
    Friend WithEvents tabNewPOS As System.Windows.Forms.TabPage
    Friend WithEvents CheckBoxNewPOSEnabled As System.Windows.Forms.CheckBox
    Friend WithEvents lbNewPOSNextPoll As System.Windows.Forms.Label
    Friend WithEvents txtNewPOSNextPoll As System.Windows.Forms.Label
    Friend WithEvents lbNewPOSLastPoll As System.Windows.Forms.Label
    Friend WithEvents txtNewPOSLastPoll As System.Windows.Forms.Label
    Friend WithEvents lbNewPOSPollInterval As System.Windows.Forms.Label
    Friend WithEvents txtNewPOSPollInterval As System.Windows.Forms.Label
    Friend WithEvents txtNewPOSCurrentTime As System.Windows.Forms.Label
    Friend WithEvents lbNewPOSCurrentTime As System.Windows.Forms.Label
    Friend WithEvents ButtonNewPOSImport As System.Windows.Forms.Button

    Private mxOfflineCashupSetupSettingsRetrieved As Boolean = True
    Friend WithEvents tabiPOS As System.Windows.Forms.TabPage
    Friend WithEvents CheckBoxiPOSEnabled As System.Windows.Forms.CheckBox
    Friend WithEvents lbiPOSNextPoll As System.Windows.Forms.Label
    Friend WithEvents txtiPOSNextPoll As System.Windows.Forms.Label
    Friend WithEvents lbiPOSLastPoll As System.Windows.Forms.Label
    Friend WithEvents txtiPOSLastPoll As System.Windows.Forms.Label
    Friend WithEvents lbiPOSPollInterval As System.Windows.Forms.Label
    Friend WithEvents txtiPOSPollInterval As System.Windows.Forms.Label
    Friend WithEvents txtiPOSCurrentTime As System.Windows.Forms.Label
    Friend WithEvents lbiPOSCurrentTime As System.Windows.Forms.Label
    Friend WithEvents ButtoniPOSImport As System.Windows.Forms.Button
    Friend WithEvents MenuItemTrayZKViewUsers As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemZKViewUsers As System.Windows.Forms.MenuItem
    Friend WithEvents tabUniwell As System.Windows.Forms.TabPage
    Friend WithEvents CheckBoxUniwellEnabled As System.Windows.Forms.CheckBox
    Friend WithEvents lbUniwellNextPoll As System.Windows.Forms.Label
    Friend WithEvents txtUniwellNextPoll As System.Windows.Forms.Label
    Friend WithEvents lbUniwellLastPoll As System.Windows.Forms.Label
    Friend WithEvents txtUniwellLastpoll As System.Windows.Forms.Label
    Friend WithEvents lbUniwellPollInterval As System.Windows.Forms.Label
    Friend WithEvents txtUniwellPollInterval As System.Windows.Forms.Label
    Friend WithEvents txtUniwellCurrentTime As System.Windows.Forms.Label
    Friend WithEvents lbUniwellCurrentTime As System.Windows.Forms.Label
    Friend WithEvents ButtonUniwellImport As System.Windows.Forms.Button
    Friend WithEvents ButtoniPOSUpdateStaff As System.Windows.Forms.Button
    Friend WithEvents ButtoniPOSUpdateProducts As System.Windows.Forms.Button
    Friend WithEvents ButtonZKUpdateFirmware As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox5 As System.Windows.Forms.TextBox
    Friend WithEvents Label53 As System.Windows.Forms.Label
    Friend WithEvents Label54 As System.Windows.Forms.Label
    Friend WithEvents CheckBox2 As System.Windows.Forms.CheckBox
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents Label55 As System.Windows.Forms.Label
    Friend WithEvents Label56 As System.Windows.Forms.Label
    Friend WithEvents Label57 As System.Windows.Forms.Label
    Friend WithEvents Label58 As System.Windows.Forms.Label
    Friend WithEvents Label59 As System.Windows.Forms.Label
    Friend WithEvents Label60 As System.Windows.Forms.Label
    Friend WithEvents Label61 As System.Windows.Forms.Label
    Friend WithEvents Label62 As System.Windows.Forms.Label
    Friend WithEvents textZKAuthServerStatus As System.Windows.Forms.TextBox
    Friend WithEvents LabelZKAuthServerStatus As System.Windows.Forms.Label
    Friend WithEvents minimiseTimer As System.Windows.Forms.Timer
    Friend WithEvents ButtoniPOSUpdateProcessDate As System.Windows.Forms.Button
    Friend WithEvents lbiPOSLastDate As System.Windows.Forms.Label
    Friend WithEvents dtiPOSProcessDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents grpiPOSInformation As System.Windows.Forms.GroupBox
    Friend WithEvents grpiPOSProcess As System.Windows.Forms.GroupBox
    Friend WithEvents grpiPOSUpdates As System.Windows.Forms.GroupBox
    Friend WithEvents txtiPOSProcessDate As System.Windows.Forms.Label
    Friend WithEvents lbiPOSProcessDate As System.Windows.Forms.Label
    Friend WithEvents GroupComprisPollingInformation As System.Windows.Forms.GroupBox
    Friend WithEvents GroupComprisPDPUpdates As System.Windows.Forms.GroupBox
    Friend WithEvents GroupComprisPollFiles As System.Windows.Forms.GroupBox
    Friend WithEvents ButtonComprisPollFilesProcess As System.Windows.Forms.Button
    Friend WithEvents ButtonComprisPDPUpdatesProcess As System.Windows.Forms.Button
    Friend WithEvents GroupComprisPDP As System.Windows.Forms.GroupBox
    Friend WithEvents ButtonComprisPDPProcess As System.Windows.Forms.Button
    Friend WithEvents tabRadiant As System.Windows.Forms.TabPage
    Friend WithEvents CheckBoxRadiantEnabled As System.Windows.Forms.CheckBox
    Friend WithEvents Label63 As System.Windows.Forms.Label
    Friend WithEvents txtRadiantNextPoll As System.Windows.Forms.Label
    Friend WithEvents Label65 As System.Windows.Forms.Label
    Friend WithEvents txtRadiantLastPoll As System.Windows.Forms.Label
    Friend WithEvents Label67 As System.Windows.Forms.Label
    Friend WithEvents txtRadiantPollInterval As System.Windows.Forms.Label
    Friend WithEvents txtRadiantCurrentTime As System.Windows.Forms.Label
    Friend WithEvents Label70 As System.Windows.Forms.Label
    Friend WithEvents ButtonRadiantImport As System.Windows.Forms.Button
    Friend WithEvents MenuItemYISController As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemTrayYISController As System.Windows.Forms.MenuItem
    Friend WithEvents tabNAXML As System.Windows.Forms.TabPage
    Friend WithEvents CheckBoxNAXMLEnabled As System.Windows.Forms.CheckBox
    Friend WithEvents Label64 As System.Windows.Forms.Label
    Friend WithEvents txtNAXMLNextPoll As System.Windows.Forms.Label
    Friend WithEvents Label68 As System.Windows.Forms.Label
    Friend WithEvents txtNAXMLLastPoll As System.Windows.Forms.Label
    Friend WithEvents Label71 As System.Windows.Forms.Label
    Friend WithEvents txtNAXMLPollInterval As System.Windows.Forms.Label
    Friend WithEvents txtNAXMLCurrentTime As System.Windows.Forms.Label
    Friend WithEvents Label74 As System.Windows.Forms.Label
    Friend WithEvents ButtonNAXMLImport As System.Windows.Forms.Button
    Friend WithEvents tabBreeze As System.Windows.Forms.TabPage
    Friend WithEvents CheckBoxBreezeEnabled As System.Windows.Forms.CheckBox
    Friend WithEvents lbBreezeNextPoll As System.Windows.Forms.Label
    Friend WithEvents txtBreezeNextPoll As System.Windows.Forms.Label
    Friend WithEvents lbBreezeLastPoll As System.Windows.Forms.Label
    Friend WithEvents txtBreezeLastPoll As System.Windows.Forms.Label
    Friend WithEvents lbBreezePollInterval As System.Windows.Forms.Label
    Friend WithEvents txtBreezePollInterval As System.Windows.Forms.Label
    Friend WithEvents txtBreezeCurrentTime As System.Windows.Forms.Label
    Friend WithEvents lbBreezeCurrentTime As System.Windows.Forms.Label
    Friend WithEvents BreezeProcessGroup As System.Windows.Forms.GroupBox
    Friend WithEvents Label66 As System.Windows.Forms.Label
    Friend WithEvents ButtonBreezeImport As System.Windows.Forms.Button
    Friend WithEvents ButtonBreezeUpdateProcessDate As System.Windows.Forms.Button
    Friend WithEvents dtBreezeProcessDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lbBreezeProcessDate As System.Windows.Forms.Label
    Friend WithEvents txtBreezeProcessDate As System.Windows.Forms.Label
    Friend WithEvents tabXpient As System.Windows.Forms.TabPage
    Friend WithEvents tabRetalix As System.Windows.Forms.TabPage
    Friend WithEvents CheckBoxXpientEnabled As System.Windows.Forms.CheckBox
    Friend WithEvents Label69 As System.Windows.Forms.Label
    Friend WithEvents txtXpientNextPoll As System.Windows.Forms.Label
    Friend WithEvents Label73 As System.Windows.Forms.Label
    Friend WithEvents txtXpientLastPoll As System.Windows.Forms.Label
    Friend WithEvents Label76 As System.Windows.Forms.Label
    Friend WithEvents txtXpientPollInterval As System.Windows.Forms.Label
    Friend WithEvents txtXpientCurrentTime As System.Windows.Forms.Label
    Friend WithEvents Label79 As System.Windows.Forms.Label
    Friend WithEvents ButtonXpientImport As System.Windows.Forms.Button
    Friend WithEvents CheckBoxRetalixEnabled As System.Windows.Forms.CheckBox
    Friend WithEvents Label80 As System.Windows.Forms.Label
    Friend WithEvents txtRetalixNextPoll As System.Windows.Forms.Label
    Friend WithEvents Label82 As System.Windows.Forms.Label
    Friend WithEvents txtRetalixLastPoll As System.Windows.Forms.Label
    Friend WithEvents Label84 As System.Windows.Forms.Label
    Friend WithEvents txtRetalixPollInterval As System.Windows.Forms.Label
    Friend WithEvents txtRetalixCurrentTime As System.Windows.Forms.Label
    Friend WithEvents Label87 As System.Windows.Forms.Label
    Friend WithEvents ButtonRetalixImport As System.Windows.Forms.Button

    Private _isRunning As Boolean = False
    Friend WithEvents tabArts As System.Windows.Forms.TabPage
    Friend WithEvents CheckBoxArtsEnabled As System.Windows.Forms.CheckBox
    Friend WithEvents ButtonArtsImport As System.Windows.Forms.Button
    Friend WithEvents Label72 As System.Windows.Forms.Label
    Friend WithEvents txtArtsNextPoll As System.Windows.Forms.Label
    Friend WithEvents Label77 As System.Windows.Forms.Label
    Friend WithEvents txtArtsLastPoll As System.Windows.Forms.Label
    Friend WithEvents Label81 As System.Windows.Forms.Label
    Friend WithEvents txtArtsPollInterval As System.Windows.Forms.Label
    Friend WithEvents txtArtsCurrentTime As System.Windows.Forms.Label
    Friend WithEvents Label86 As System.Windows.Forms.Label
    Friend WithEvents tabPOSixml As System.Windows.Forms.TabPage
    Friend WithEvents CheckBoxPOSixmlEnabled As System.Windows.Forms.CheckBox
    Friend WithEvents ButtonPOSixmlImport As System.Windows.Forms.Button
    Friend WithEvents Label75 As System.Windows.Forms.Label
    Friend WithEvents txtPOSixmlNextPoll As System.Windows.Forms.Label
    Friend WithEvents Label83 As System.Windows.Forms.Label
    Friend WithEvents txtPOSixmlLastPoll As System.Windows.Forms.Label
    Friend WithEvents Label88 As System.Windows.Forms.Label
    Friend WithEvents txtPOSixmlPollInterval As System.Windows.Forms.Label
    Friend WithEvents txtPOSixmlCurrentTime As System.Windows.Forms.Label
    Friend WithEvents Label91 As System.Windows.Forms.Label
    Friend WithEvents tabRPOS As System.Windows.Forms.TabPage
    Friend WithEvents CheckBoxRPOSEnabled As System.Windows.Forms.CheckBox
    Friend WithEvents ButtonRPOSImport As System.Windows.Forms.Button
    Friend WithEvents Label78 As System.Windows.Forms.Label
    Friend WithEvents txtRPOSNextPoll As System.Windows.Forms.Label
    Friend WithEvents Label89 As System.Windows.Forms.Label
    Friend WithEvents txtRPOSLastPoll As System.Windows.Forms.Label
    Friend WithEvents Label92 As System.Windows.Forms.Label
    Friend WithEvents txtRPOSPollInterval As System.Windows.Forms.Label
    Friend WithEvents txtRPOSCurrentTime As System.Windows.Forms.Label
    Friend WithEvents Label95 As System.Windows.Forms.Label
    Friend WithEvents tabPosCommon As System.Windows.Forms.TabPage
    Friend WithEvents chkEnableGatewayCommon As System.Windows.Forms.CheckBox
    Friend WithEvents btImportCommon As System.Windows.Forms.Button
    Friend WithEvents Label85 As System.Windows.Forms.Label
    Friend WithEvents txtNextPoll As System.Windows.Forms.Label
    Friend WithEvents Label93 As System.Windows.Forms.Label
    Friend WithEvents txtLastPoll As System.Windows.Forms.Label
    Friend WithEvents Label96 As System.Windows.Forms.Label
    Friend WithEvents txtPollInterval As System.Windows.Forms.Label
    Friend WithEvents txtCurrentTime As System.Windows.Forms.Label
    Friend WithEvents Label99 As System.Windows.Forms.Label
    Friend WithEvents TimerStatusUpdate As System.Windows.Forms.Timer
    Friend WithEvents TabSUSPOS As System.Windows.Forms.TabPage
    Friend WithEvents CheckBox5 As System.Windows.Forms.CheckBox
    Friend WithEvents Button11 As System.Windows.Forms.Button
    Friend WithEvents Label90 As System.Windows.Forms.Label
    Friend WithEvents Label94 As System.Windows.Forms.Label
    Friend WithEvents Label97 As System.Windows.Forms.Label
    Friend WithEvents Label98 As System.Windows.Forms.Label
    Friend WithEvents Label100 As System.Windows.Forms.Label
    Friend WithEvents Label101 As System.Windows.Forms.Label
    Friend WithEvents Label102 As System.Windows.Forms.Label
    Friend WithEvents Label103 As System.Windows.Forms.Label
    Friend WithEvents tabTrafficData As System.Windows.Forms.TabPage
    Friend WithEvents CheckBox6 As System.Windows.Forms.CheckBox
    Friend WithEvents btnTrafficDataProcess As System.Windows.Forms.Button
    Friend WithEvents Label104 As System.Windows.Forms.Label
    Friend WithEvents Label105 As System.Windows.Forms.Label
    Friend WithEvents Label106 As System.Windows.Forms.Label
    Friend WithEvents Label107 As System.Windows.Forms.Label
    Friend WithEvents Label108 As System.Windows.Forms.Label
    Friend WithEvents Label109 As System.Windows.Forms.Label
    Friend WithEvents Label110 As System.Windows.Forms.Label
    Friend WithEvents Label111 As System.Windows.Forms.Label
    Private _isConnected As Boolean = False

    Public Property IsRunning() As Boolean
        Get
            Return _isRunning
        End Get
        Set(ByVal value As Boolean)
            _isRunning = value
        End Set
    End Property


    Public Property PingEnabled() As Boolean
        Get
            Return _pingEnabled
        End Get
        Set(ByVal value As Boolean)
            ' Only set to true if using offline cashmanager and a ping interval has been specified
            _pingEnabled = LiveLinkConfiguration.PingInterval_KeyExists AndAlso value
            StartStopPingTimer(_pingEnabled AndAlso IsRunning)
        End Set
    End Property

    Private Property OK_To_Send() As Boolean
        Get
            Return _okToSend
        End Get
        Set(ByVal value As Boolean)
            _okToSend = value
            PictureBoxBackoff.Visible = Not _okToSend
        End Set
    End Property

    Private Sub StartStopPingTimer(ByVal start As Boolean)
        If start AndAlso PingEnabled AndAlso Not tmrPing.Enabled Then
            ' Start the ping timer.  Use a minimum value of 15 seconds
            tmrPing.Interval = Max(15000, LiveLinkConfiguration.PingInterval * 1000)
            tmrPing.Enabled = True
            ToolTip1.SetToolTip(PictureBoxPingEnabled, String.Format("LiveLink is checking server availability every {0} seconds.", tmrPing.Interval / 1000))
            PictureBoxPingEnabled.Visible = True
        ElseIf Not start Then
            tmrPing.Enabled = False
            PictureBoxPingEnabled.Visible = False
        End If
    End Sub

    Private Enum DebugModes
        Off
        Simple
        Verbose
    End Enum

    Private Enum DataType
        SalesData
        AttendanceData
        SpeedOfServiceData
    End Enum

#End Region

#Region "Class Variables"

    Private ReadOnly Property MyIdentity() As String
        Get
            If (String.IsNullOrEmpty(_identity)) Then
                SetLiveLinkID()
            End If
            Return _identity
        End Get
    End Property

    Private Sub SetLiveLinkID(Optional ByRef comExceptionThrown As Boolean = False)
        _identity = GetUniqueMachineId(comExceptionThrown)
    End Sub

    Public Function RefreshLiveLinkID() As Boolean
        ' attempt to get LiveLink ID information from WMI and registry.
        Dim comExceptionThrown As Boolean = False
        SetLiveLinkID(comExceptionThrown)

        ' if WMI error occurs on initial startup, then restart LiveLink.
        If (LiveLinkConfiguration.RestartOnInitialWMIAccessError AndAlso comExceptionThrown) Then
            LogEvent(EventLogEntryType.Information, "WMI Exception occurred during LiveLink startup. Restarting LiveLink...")
            Return False
        End If
        MxLogger.LogInformationToTrace(EventId.BusinessLogic, 0, "LiveLink ID", Nothing, "LiveLink ID: {0}", MyIdentity)
        Return True
    End Function

#End Region

#Region " Windows Form Designer generated code "

    Public Sub New()

        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        ' parse any command line parameters
        ParseCommandLine(Environment.GetCommandLineArgs(), Environment.CommandLine)

        'Add any initialization after the InitializeComponent() call
        If (Not ConfigurationHelper.IsBaseConfigured()) Then
            Return
        End If

        If (Not (LiveLinkConfiguration.UseOfflineCashup_KeyExists AndAlso LiveLinkConfiguration.UseOfflineCashup)) Then
            TabControl1.TabPages.Remove(tabOfflineCashManager)
        End If

    End Sub


    Private Shared Sub LogConfigMigration(ByVal sender As Object, ByVal e As Mx.Configuration.MigrationStepEventArgs)
        MMS.Utilities.MxClientLogger.LogInfo(e.Message)
    End Sub


    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents LabelTitle As System.Windows.Forms.Label
    Friend WithEvents PictureBoxOff As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBoxON As System.Windows.Forms.PictureBox
    Friend WithEvents MainMenu1 As System.Windows.Forms.MainMenu
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem4 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem5 As System.Windows.Forms.MenuItem
    Friend WithEvents LabelStatus As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents DataGrid1 As System.Windows.Forms.DataGrid
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents TextBox4 As System.Windows.Forms.TextBox
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents TextBoxAccessCode As System.Windows.Forms.TextBox
    Friend WithEvents Button7 As System.Windows.Forms.Button
    Friend WithEvents Button8 As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents CheckBoxParEnabled As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBoxPosiEnabled As System.Windows.Forms.CheckBox
    Friend WithEvents txtParStatus As System.Windows.Forms.TextBox
    Friend WithEvents txtParNextPoll As System.Windows.Forms.Label
    Friend WithEvents txtParLastPoll As System.Windows.Forms.Label
    Friend WithEvents txtParPollInterval As System.Windows.Forms.Label
    Friend WithEvents txtParCurrentTime As System.Windows.Forms.Label
    Friend WithEvents btnParImport As System.Windows.Forms.Button
    Friend WithEvents btnParGetTld As System.Windows.Forms.Button
    Friend WithEvents btnPosiImport As System.Windows.Forms.Button
    Friend WithEvents txtPosiStatus As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txtPosiNextPoll As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents txtPosiLastPoll As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents txtPosiPollInterval As System.Windows.Forms.Label
    Friend WithEvents txtPosiCurrentTime As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents LabelEndOfDayStatus As System.Windows.Forms.Label
    Friend WithEvents TextBoxAwake As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents tmrCommsBackoff As System.Windows.Forms.Timer
    Friend WithEvents TabControl2 As System.Windows.Forms.TabControl
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents CheckBox3 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox4 As System.Windows.Forms.CheckBox
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Button9 As System.Windows.Forms.Button
    Friend WithEvents TextBoxDCAPriceSetCheckInterval As System.Windows.Forms.TextBox
    Friend WithEvents TextBoxDCAPriceSetImportFileConnectionString As System.Windows.Forms.TextBox
    Friend WithEvents TextBoxDCAPriceSetImportFileName As System.Windows.Forms.TextBox
    Friend WithEvents TextBoxDCAPriceSetPOSFileConnectionString As System.Windows.Forms.TextBox
    Friend WithEvents Button10 As System.Windows.Forms.Button
    Friend WithEvents CheckBoxDCAActive As System.Windows.Forms.CheckBox
    Friend WithEvents tabSetup As System.Windows.Forms.TabPage
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents txtEntityId As System.Windows.Forms.TextBox
    Friend WithEvents cboStores As System.Windows.Forms.ComboBox
    Friend WithEvents btnUpdateStore As System.Windows.Forms.Button
    Friend WithEvents btnLoadStores As System.Windows.Forms.Button
    Friend WithEvents tmrSessionLease As System.Windows.Forms.Timer
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents txtConfiguredStore As System.Windows.Forms.TextBox
    Friend WithEvents NotifyIcon1 As System.Windows.Forms.NotifyIcon
    Friend WithEvents txtFlowInterval As System.Windows.Forms.TextBox
    Friend WithEvents menuTrayIcon As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuShowLiveLink As System.Windows.Forms.MenuItem
    Friend WithEvents mnuTrayExit As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents CheckBoxAlohaEnabled As System.Windows.Forms.CheckBox
    Friend WithEvents btnAlohaImport As System.Windows.Forms.Button
    Friend WithEvents txtAlohaStatus As System.Windows.Forms.TextBox
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents txtAlohaNextPoll As System.Windows.Forms.Label
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents txtAlohaLastPoll As System.Windows.Forms.Label
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents txtAlohaPollInterval As System.Windows.Forms.Label
    Friend WithEvents txtAlohaCurrentTime As System.Windows.Forms.Label
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents tabMicros As System.Windows.Forms.TabPage
    Friend WithEvents CheckBoxMicrosEnabled As System.Windows.Forms.CheckBox
    Friend WithEvents btnMicrosImport As System.Windows.Forms.Button
    Public Shared WithEvents txtMicrosStatus As System.Windows.Forms.TextBox
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents txtMicrosNextPoll As System.Windows.Forms.Label
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents txtMicrosLastPoll As System.Windows.Forms.Label
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents txtMicrosPollInterval As System.Windows.Forms.Label
    Friend WithEvents txtMicrosCurrentTime As System.Windows.Forms.Label
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents tabAloha As System.Windows.Forms.TabPage
    Friend WithEvents tabSetPrice As System.Windows.Forms.TabPage
    Friend WithEvents tabStatus As System.Windows.Forms.TabPage
    Friend WithEvents tabPAR As System.Windows.Forms.TabPage
    Friend WithEvents tabTest As System.Windows.Forms.TabPage
    Friend WithEvents tabPOSI As System.Windows.Forms.TabPage
    Friend WithEvents tabSchedule As System.Windows.Forms.TabPage
    Friend WithEvents tabSetPriceDCA As System.Windows.Forms.TabPage
    Friend WithEvents tabSetPricePanasonic As System.Windows.Forms.TabPage
    Friend WithEvents tabSetPricePAR As System.Windows.Forms.TabPage
    Friend WithEvents btnProcessNow As System.Windows.Forms.Button
    Friend WithEvents btnAccessCode As System.Windows.Forms.Button
    Friend WithEvents btnEndOfDay As System.Windows.Forms.Button
    Friend WithEvents btnSysInfo As System.Windows.Forms.Button
    Friend WithEvents chkSkipGrind As System.Windows.Forms.CheckBox
    Friend WithEvents chkSkipData As System.Windows.Forms.CheckBox
    Friend WithEvents MenuItemRunTimeClock As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemTrayRunTimeClock As System.Windows.Forms.MenuItem
    Friend WithEvents mnuClockSpacer As System.Windows.Forms.MenuItem
    Friend WithEvents CheckBoxParTLDServer As System.Windows.Forms.CheckBox
    Friend WithEvents ButtonParTLDListener As System.Windows.Forms.Button
    Friend WithEvents ImageListStatus As System.Windows.Forms.ImageList
    Friend WithEvents LabelParTLDStatus As System.Windows.Forms.Label
    Friend WithEvents MenuItemRunComprisActive As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItemTrayRunComprisActive As System.Windows.Forms.MenuItem
    Friend WithEvents ButtonComprisImport As System.Windows.Forms.Button
    Friend WithEvents CheckBoxComprisEnabled As System.Windows.Forms.CheckBox
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents txtComprisNextPoll As System.Windows.Forms.Label
    Friend WithEvents Label33 As System.Windows.Forms.Label
    Friend WithEvents txtComprisLastPoll As System.Windows.Forms.Label
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents txtComprisPollInterval As System.Windows.Forms.Label
    Friend WithEvents txtComprisCurrentTime As System.Windows.Forms.Label
    Friend WithEvents Label39 As System.Windows.Forms.Label
    Friend WithEvents tabCompris As System.Windows.Forms.TabPage
    Friend WithEvents tabPanasonic As System.Windows.Forms.TabPage
    Friend WithEvents CheckBoxPanasonicEnabled As System.Windows.Forms.CheckBox
    Friend WithEvents ButtonPanasonicImport As System.Windows.Forms.Button
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents txtPanasonicNextPoll As System.Windows.Forms.Label
    Friend WithEvents Label37 As System.Windows.Forms.Label
    Friend WithEvents txtPanasonicLastPoll As System.Windows.Forms.Label
    Friend WithEvents Label40 As System.Windows.Forms.Label
    Friend WithEvents txtPanasonicPollInterval As System.Windows.Forms.Label
    Friend WithEvents txtPanasonicCurrentTime As System.Windows.Forms.Label
    Friend WithEvents Label43 As System.Windows.Forms.Label
    Friend WithEvents tabIntouch As System.Windows.Forms.TabPage
    Friend WithEvents CheckBoxIntouchEnabled As System.Windows.Forms.CheckBox
    Friend WithEvents ButtonIntouchImport As System.Windows.Forms.Button
    Friend WithEvents Label34 As System.Windows.Forms.Label
    Friend WithEvents txtIntouchNextPoll As System.Windows.Forms.Label
    Friend WithEvents Label41 As System.Windows.Forms.Label
    Friend WithEvents txtIntouchLastPoll As System.Windows.Forms.Label
    Friend WithEvents Label44 As System.Windows.Forms.Label
    Friend WithEvents txtIntouchPollInterval As System.Windows.Forms.Label
    Friend WithEvents txtIntouchCurrentTime As System.Windows.Forms.Label
    Friend WithEvents Label47 As System.Windows.Forms.Label


    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(LiveLinkMain))
        Me.Button1 = New System.Windows.Forms.Button()
        Me.LabelTitle = New System.Windows.Forms.Label()
        Me.MainMenu1 = New System.Windows.Forms.MainMenu(Me.components)
        Me.MenuItem1 = New System.Windows.Forms.MenuItem()
        Me.MenuItemRunTimeClock = New System.Windows.Forms.MenuItem()
        Me.MenuItemRunComprisActive = New System.Windows.Forms.MenuItem()
        Me.MenuItemYISController = New System.Windows.Forms.MenuItem()
        Me.MenuItemZKViewUsers = New System.Windows.Forms.MenuItem()
        Me.MenuItem5 = New System.Windows.Forms.MenuItem()
        Me.MenuItem2 = New System.Windows.Forms.MenuItem()
        Me.MenuItem4 = New System.Windows.Forms.MenuItem()
        Me.LabelStatus = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.btnProcessNow = New System.Windows.Forms.Button()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.tabStatus = New System.Windows.Forms.TabPage()
        Me.TextBox3 = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.tabIntouch = New System.Windows.Forms.TabPage()
        Me.CheckBoxIntouchEnabled = New System.Windows.Forms.CheckBox()
        Me.ButtonIntouchImport = New System.Windows.Forms.Button()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.txtIntouchNextPoll = New System.Windows.Forms.Label()
        Me.Label41 = New System.Windows.Forms.Label()
        Me.txtIntouchLastPoll = New System.Windows.Forms.Label()
        Me.Label44 = New System.Windows.Forms.Label()
        Me.txtIntouchPollInterval = New System.Windows.Forms.Label()
        Me.txtIntouchCurrentTime = New System.Windows.Forms.Label()
        Me.Label47 = New System.Windows.Forms.Label()
        Me.tabOctane = New System.Windows.Forms.TabPage()
        Me.CheckBoxOctaneEnabled = New System.Windows.Forms.CheckBox()
        Me.ButtonOctaneImport = New System.Windows.Forms.Button()
        Me.Label42 = New System.Windows.Forms.Label()
        Me.txtOctaneNextPoll = New System.Windows.Forms.Label()
        Me.Label46 = New System.Windows.Forms.Label()
        Me.txtOctaneLastPoll = New System.Windows.Forms.Label()
        Me.Label49 = New System.Windows.Forms.Label()
        Me.txtOctanePollInterval = New System.Windows.Forms.Label()
        Me.txtOctaneCurrentTime = New System.Windows.Forms.Label()
        Me.Label52 = New System.Windows.Forms.Label()
        Me.tabAloha = New System.Windows.Forms.TabPage()
        Me.ButtonAlohaProcessDateApply = New System.Windows.Forms.Button()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.dtpAlohaProcessDate = New System.Windows.Forms.DateTimePicker()
        Me.LabelAlohaLastDateProcessed = New System.Windows.Forms.Label()
        Me.txtAlohaProcessDate = New System.Windows.Forms.Label()
        Me.chkSkipData = New System.Windows.Forms.CheckBox()
        Me.chkSkipGrind = New System.Windows.Forms.CheckBox()
        Me.CheckBoxAlohaEnabled = New System.Windows.Forms.CheckBox()
        Me.btnAlohaImport = New System.Windows.Forms.Button()
        Me.txtAlohaStatus = New System.Windows.Forms.TextBox()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.txtAlohaNextPoll = New System.Windows.Forms.Label()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.txtAlohaLastPoll = New System.Windows.Forms.Label()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.txtAlohaPollInterval = New System.Windows.Forms.Label()
        Me.txtAlohaCurrentTime = New System.Windows.Forms.Label()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.tabPanasonic = New System.Windows.Forms.TabPage()
        Me.CheckBoxPanasonicEnabled = New System.Windows.Forms.CheckBox()
        Me.ButtonPanasonicImport = New System.Windows.Forms.Button()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.txtPanasonicNextPoll = New System.Windows.Forms.Label()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.txtPanasonicLastPoll = New System.Windows.Forms.Label()
        Me.Label40 = New System.Windows.Forms.Label()
        Me.txtPanasonicPollInterval = New System.Windows.Forms.Label()
        Me.txtPanasonicCurrentTime = New System.Windows.Forms.Label()
        Me.Label43 = New System.Windows.Forms.Label()
        Me.tabSchedule = New System.Windows.Forms.TabPage()
        Me.TextBoxAwake = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.txtFlowInterval = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.CheckBox1 = New System.Windows.Forms.CheckBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.tabMicros = New System.Windows.Forms.TabPage()
        Me.CheckBoxMicrosEnabled = New System.Windows.Forms.CheckBox()
        Me.btnMicrosImport = New System.Windows.Forms.Button()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.txtMicrosNextPoll = New System.Windows.Forms.Label()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.txtMicrosLastPoll = New System.Windows.Forms.Label()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.txtMicrosPollInterval = New System.Windows.Forms.Label()
        Me.txtMicrosCurrentTime = New System.Windows.Forms.Label()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.tabSetup = New System.Windows.Forms.TabPage()
        Me.btnReceiveMessage = New System.Windows.Forms.Button()
        Me.btnSysInfo = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.chkAutoStartOnLogin = New System.Windows.Forms.CheckBox()
        Me.ButtonNewMachineID = New System.Windows.Forms.Button()
        Me.txtConfiguredStore = New System.Windows.Forms.TextBox()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.btnUpdateStore = New System.Windows.Forms.Button()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.txtEntityId = New System.Windows.Forms.TextBox()
        Me.btnLoadStores = New System.Windows.Forms.Button()
        Me.cboStores = New System.Windows.Forms.ComboBox()
        Me.tabCompris = New System.Windows.Forms.TabPage()
        Me.GroupComprisPollFiles = New System.Windows.Forms.GroupBox()
        Me.ButtonComprisPollFilesProcess = New System.Windows.Forms.Button()
        Me.GroupComprisPDPUpdates = New System.Windows.Forms.GroupBox()
        Me.ButtonComprisPDPUpdatesProcess = New System.Windows.Forms.Button()
        Me.GroupComprisPDP = New System.Windows.Forms.GroupBox()
        Me.ButtonComprisPDPProcess = New System.Windows.Forms.Button()
        Me.GroupComprisPollingInformation = New System.Windows.Forms.GroupBox()
        Me.Label39 = New System.Windows.Forms.Label()
        Me.ButtonComprisImport = New System.Windows.Forms.Button()
        Me.CheckBoxComprisEnabled = New System.Windows.Forms.CheckBox()
        Me.txtComprisCurrentTime = New System.Windows.Forms.Label()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.txtComprisPollInterval = New System.Windows.Forms.Label()
        Me.txtComprisNextPoll = New System.Windows.Forms.Label()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.txtComprisLastPoll = New System.Windows.Forms.Label()
        Me.tabPOSI = New System.Windows.Forms.TabPage()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txtPosiNextPoll = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txtPosiLastPoll = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.txtPosiPollInterval = New System.Windows.Forms.Label()
        Me.txtPosiCurrentTime = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.txtPosiStatus = New System.Windows.Forms.TextBox()
        Me.btnPosiImport = New System.Windows.Forms.Button()
        Me.CheckBoxPosiEnabled = New System.Windows.Forms.CheckBox()
        Me.tabTest = New System.Windows.Forms.TabPage()
        Me.Button8 = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.TextBox4 = New System.Windows.Forms.TextBox()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.DataGrid1 = New System.Windows.Forms.DataGrid()
        Me.tabPAR = New System.Windows.Forms.TabPage()
        Me.LabelParTLDStatus = New System.Windows.Forms.Label()
        Me.ButtonParTLDListener = New System.Windows.Forms.Button()
        Me.CheckBoxParTLDServer = New System.Windows.Forms.CheckBox()
        Me.CheckBoxParEnabled = New System.Windows.Forms.CheckBox()
        Me.btnParImport = New System.Windows.Forms.Button()
        Me.btnParGetTld = New System.Windows.Forms.Button()
        Me.txtParStatus = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtParNextPoll = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtParLastPoll = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtParPollInterval = New System.Windows.Forms.Label()
        Me.txtParCurrentTime = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.tabSetPrice = New System.Windows.Forms.TabPage()
        Me.TabControl2 = New System.Windows.Forms.TabControl()
        Me.tabSetPriceDCA = New System.Windows.Forms.TabPage()
        Me.Button10 = New System.Windows.Forms.Button()
        Me.Button9 = New System.Windows.Forms.Button()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.TextBoxDCAPriceSetPOSFileConnectionString = New System.Windows.Forms.TextBox()
        Me.TextBoxDCAPriceSetImportFileName = New System.Windows.Forms.TextBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.TextBoxDCAPriceSetImportFileConnectionString = New System.Windows.Forms.TextBox()
        Me.TextBoxDCAPriceSetCheckInterval = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.CheckBoxDCAActive = New System.Windows.Forms.CheckBox()
        Me.tabSetPricePanasonic = New System.Windows.Forms.TabPage()
        Me.CheckBox3 = New System.Windows.Forms.CheckBox()
        Me.tabSetPricePAR = New System.Windows.Forms.TabPage()
        Me.CheckBox4 = New System.Windows.Forms.CheckBox()
        Me.tabOfflineCashManager = New System.Windows.Forms.TabPage()
        Me.Label45 = New System.Windows.Forms.Label()
        Me.cmdRefreshSettings = New System.Windows.Forms.Button()
        Me.tabZKFingerprint = New System.Windows.Forms.TabPage()
        Me.textZKAuthServerStatus = New System.Windows.Forms.TextBox()
        Me.LabelZKAuthServerStatus = New System.Windows.Forms.Label()
        Me.ButtonZKUpdateFirmware = New System.Windows.Forms.Button()
        Me.txtZKDevicePort = New System.Windows.Forms.TextBox()
        Me.txtZKDeviceIP = New System.Windows.Forms.TextBox()
        Me.lbZKDevicePort = New System.Windows.Forms.Label()
        Me.lbZKDeviceIP = New System.Windows.Forms.Label()
        Me.CheckBoxZKEnabled = New System.Windows.Forms.CheckBox()
        Me.ButtonZKFingerprintImport = New System.Windows.Forms.Button()
        Me.lbZKNextPoll = New System.Windows.Forms.Label()
        Me.txtZKNextPoll = New System.Windows.Forms.Label()
        Me.lbZKLastPoll = New System.Windows.Forms.Label()
        Me.txtZKLastPoll = New System.Windows.Forms.Label()
        Me.lbZKPollInterval = New System.Windows.Forms.Label()
        Me.txtZKPollInterval = New System.Windows.Forms.Label()
        Me.txtZKCurrentTime = New System.Windows.Forms.Label()
        Me.lbZKCurrentTime = New System.Windows.Forms.Label()
        Me.tabICG = New System.Windows.Forms.TabPage()
        Me.CheckBoxICGEnabled = New System.Windows.Forms.CheckBox()
        Me.lbICGNextPoll = New System.Windows.Forms.Label()
        Me.txtICGNextPoll = New System.Windows.Forms.Label()
        Me.lbICGLastPoll = New System.Windows.Forms.Label()
        Me.txtICGLastPoll = New System.Windows.Forms.Label()
        Me.lbICGPollInterval = New System.Windows.Forms.Label()
        Me.txtICGPollInterval = New System.Windows.Forms.Label()
        Me.txtICGCurrentTime = New System.Windows.Forms.Label()
        Me.lbICGCurrentTime = New System.Windows.Forms.Label()
        Me.ButtonICGImport = New System.Windows.Forms.Button()
        Me.tabRMS = New System.Windows.Forms.TabPage()
        Me.CheckBoxRMSEnabled = New System.Windows.Forms.CheckBox()
        Me.lbRMSNextPoll = New System.Windows.Forms.Label()
        Me.txtRMSNextPoll = New System.Windows.Forms.Label()
        Me.lbRMSLastPoll = New System.Windows.Forms.Label()
        Me.txtRMSLastPoll = New System.Windows.Forms.Label()
        Me.lbRMSPollInterval = New System.Windows.Forms.Label()
        Me.txtRMSPollInterval = New System.Windows.Forms.Label()
        Me.txtRMSCurrentTime = New System.Windows.Forms.Label()
        Me.lbRMSCurrentTime = New System.Windows.Forms.Label()
        Me.ButtonRMSImport = New System.Windows.Forms.Button()
        Me.tabNewPOS = New System.Windows.Forms.TabPage()
        Me.CheckBoxNewPOSEnabled = New System.Windows.Forms.CheckBox()
        Me.lbNewPOSNextPoll = New System.Windows.Forms.Label()
        Me.txtNewPOSNextPoll = New System.Windows.Forms.Label()
        Me.lbNewPOSLastPoll = New System.Windows.Forms.Label()
        Me.txtNewPOSLastPoll = New System.Windows.Forms.Label()
        Me.lbNewPOSPollInterval = New System.Windows.Forms.Label()
        Me.txtNewPOSPollInterval = New System.Windows.Forms.Label()
        Me.txtNewPOSCurrentTime = New System.Windows.Forms.Label()
        Me.lbNewPOSCurrentTime = New System.Windows.Forms.Label()
        Me.ButtonNewPOSImport = New System.Windows.Forms.Button()
        Me.tabProxy = New System.Windows.Forms.TabPage()
        Me.btnSaveProxySettings = New System.Windows.Forms.Button()
        Me.grpProxy = New System.Windows.Forms.GroupBox()
        Me.cboProxyAuth = New System.Windows.Forms.ComboBox()
        Me.txtCustomProxy = New System.Windows.Forms.TextBox()
        Me.txtSystemProxy = New System.Windows.Forms.TextBox()
        Me.grpProxyAuth = New System.Windows.Forms.GroupBox()
        Me.txtProxyPassword = New System.Windows.Forms.TextBox()
        Me.txtProxyDomain = New System.Windows.Forms.TextBox()
        Me.txtProxyUserName = New System.Windows.Forms.TextBox()
        Me.Label51 = New System.Windows.Forms.Label()
        Me.Label50 = New System.Windows.Forms.Label()
        Me.Label48 = New System.Windows.Forms.Label()
        Me.radCustomProxy = New System.Windows.Forms.RadioButton()
        Me.radUseIEProxy = New System.Windows.Forms.RadioButton()
        Me.lblProxyServer = New System.Windows.Forms.Label()
        Me.chkUseProxy = New System.Windows.Forms.CheckBox()
        Me.tabiPOS = New System.Windows.Forms.TabPage()
        Me.grpiPOSInformation = New System.Windows.Forms.GroupBox()
        Me.txtiPOSProcessDate = New System.Windows.Forms.Label()
        Me.lbiPOSProcessDate = New System.Windows.Forms.Label()
        Me.lbiPOSCurrentTime = New System.Windows.Forms.Label()
        Me.txtiPOSCurrentTime = New System.Windows.Forms.Label()
        Me.txtiPOSPollInterval = New System.Windows.Forms.Label()
        Me.CheckBoxiPOSEnabled = New System.Windows.Forms.CheckBox()
        Me.lbiPOSPollInterval = New System.Windows.Forms.Label()
        Me.lbiPOSNextPoll = New System.Windows.Forms.Label()
        Me.txtiPOSLastPoll = New System.Windows.Forms.Label()
        Me.txtiPOSNextPoll = New System.Windows.Forms.Label()
        Me.lbiPOSLastPoll = New System.Windows.Forms.Label()
        Me.grpiPOSProcess = New System.Windows.Forms.GroupBox()
        Me.lbiPOSLastDate = New System.Windows.Forms.Label()
        Me.ButtoniPOSImport = New System.Windows.Forms.Button()
        Me.ButtoniPOSUpdateProcessDate = New System.Windows.Forms.Button()
        Me.dtiPOSProcessDate = New System.Windows.Forms.DateTimePicker()
        Me.grpiPOSUpdates = New System.Windows.Forms.GroupBox()
        Me.ButtoniPOSUpdateProducts = New System.Windows.Forms.Button()
        Me.ButtoniPOSUpdateStaff = New System.Windows.Forms.Button()
        Me.tabUniwell = New System.Windows.Forms.TabPage()
        Me.CheckBoxUniwellEnabled = New System.Windows.Forms.CheckBox()
        Me.lbUniwellNextPoll = New System.Windows.Forms.Label()
        Me.txtUniwellNextPoll = New System.Windows.Forms.Label()
        Me.lbUniwellLastPoll = New System.Windows.Forms.Label()
        Me.txtUniwellLastpoll = New System.Windows.Forms.Label()
        Me.lbUniwellPollInterval = New System.Windows.Forms.Label()
        Me.txtUniwellPollInterval = New System.Windows.Forms.Label()
        Me.txtUniwellCurrentTime = New System.Windows.Forms.Label()
        Me.lbUniwellCurrentTime = New System.Windows.Forms.Label()
        Me.ButtonUniwellImport = New System.Windows.Forms.Button()
        Me.tabRadiant = New System.Windows.Forms.TabPage()
        Me.CheckBoxRadiantEnabled = New System.Windows.Forms.CheckBox()
        Me.Label63 = New System.Windows.Forms.Label()
        Me.txtRadiantNextPoll = New System.Windows.Forms.Label()
        Me.Label65 = New System.Windows.Forms.Label()
        Me.txtRadiantLastPoll = New System.Windows.Forms.Label()
        Me.Label67 = New System.Windows.Forms.Label()
        Me.txtRadiantPollInterval = New System.Windows.Forms.Label()
        Me.txtRadiantCurrentTime = New System.Windows.Forms.Label()
        Me.Label70 = New System.Windows.Forms.Label()
        Me.ButtonRadiantImport = New System.Windows.Forms.Button()
        Me.tabNAXML = New System.Windows.Forms.TabPage()
        Me.CheckBoxNAXMLEnabled = New System.Windows.Forms.CheckBox()
        Me.Label64 = New System.Windows.Forms.Label()
        Me.txtNAXMLNextPoll = New System.Windows.Forms.Label()
        Me.Label68 = New System.Windows.Forms.Label()
        Me.txtNAXMLLastPoll = New System.Windows.Forms.Label()
        Me.Label71 = New System.Windows.Forms.Label()
        Me.txtNAXMLPollInterval = New System.Windows.Forms.Label()
        Me.txtNAXMLCurrentTime = New System.Windows.Forms.Label()
        Me.Label74 = New System.Windows.Forms.Label()
        Me.ButtonNAXMLImport = New System.Windows.Forms.Button()
        Me.tabBreeze = New System.Windows.Forms.TabPage()
        Me.txtBreezeProcessDate = New System.Windows.Forms.Label()
        Me.lbBreezeProcessDate = New System.Windows.Forms.Label()
        Me.BreezeProcessGroup = New System.Windows.Forms.GroupBox()
        Me.Label66 = New System.Windows.Forms.Label()
        Me.ButtonBreezeImport = New System.Windows.Forms.Button()
        Me.ButtonBreezeUpdateProcessDate = New System.Windows.Forms.Button()
        Me.dtBreezeProcessDate = New System.Windows.Forms.DateTimePicker()
        Me.CheckBoxBreezeEnabled = New System.Windows.Forms.CheckBox()
        Me.lbBreezeNextPoll = New System.Windows.Forms.Label()
        Me.txtBreezeNextPoll = New System.Windows.Forms.Label()
        Me.lbBreezeLastPoll = New System.Windows.Forms.Label()
        Me.txtBreezeLastPoll = New System.Windows.Forms.Label()
        Me.lbBreezePollInterval = New System.Windows.Forms.Label()
        Me.txtBreezePollInterval = New System.Windows.Forms.Label()
        Me.txtBreezeCurrentTime = New System.Windows.Forms.Label()
        Me.lbBreezeCurrentTime = New System.Windows.Forms.Label()
        Me.tabXpient = New System.Windows.Forms.TabPage()
        Me.CheckBoxXpientEnabled = New System.Windows.Forms.CheckBox()
        Me.Label69 = New System.Windows.Forms.Label()
        Me.txtXpientNextPoll = New System.Windows.Forms.Label()
        Me.Label73 = New System.Windows.Forms.Label()
        Me.txtXpientLastPoll = New System.Windows.Forms.Label()
        Me.Label76 = New System.Windows.Forms.Label()
        Me.txtXpientPollInterval = New System.Windows.Forms.Label()
        Me.txtXpientCurrentTime = New System.Windows.Forms.Label()
        Me.Label79 = New System.Windows.Forms.Label()
        Me.ButtonXpientImport = New System.Windows.Forms.Button()
        Me.tabRetalix = New System.Windows.Forms.TabPage()
        Me.CheckBoxRetalixEnabled = New System.Windows.Forms.CheckBox()
        Me.Label80 = New System.Windows.Forms.Label()
        Me.txtRetalixNextPoll = New System.Windows.Forms.Label()
        Me.Label82 = New System.Windows.Forms.Label()
        Me.txtRetalixLastPoll = New System.Windows.Forms.Label()
        Me.Label84 = New System.Windows.Forms.Label()
        Me.txtRetalixPollInterval = New System.Windows.Forms.Label()
        Me.txtRetalixCurrentTime = New System.Windows.Forms.Label()
        Me.Label87 = New System.Windows.Forms.Label()
        Me.ButtonRetalixImport = New System.Windows.Forms.Button()
        Me.tabArts = New System.Windows.Forms.TabPage()
        Me.CheckBoxArtsEnabled = New System.Windows.Forms.CheckBox()
        Me.ButtonArtsImport = New System.Windows.Forms.Button()
        Me.Label72 = New System.Windows.Forms.Label()
        Me.txtArtsNextPoll = New System.Windows.Forms.Label()
        Me.Label77 = New System.Windows.Forms.Label()
        Me.txtArtsLastPoll = New System.Windows.Forms.Label()
        Me.Label81 = New System.Windows.Forms.Label()
        Me.txtArtsPollInterval = New System.Windows.Forms.Label()
        Me.txtArtsCurrentTime = New System.Windows.Forms.Label()
        Me.Label86 = New System.Windows.Forms.Label()
        Me.tabPOSixml = New System.Windows.Forms.TabPage()
        Me.CheckBoxPOSixmlEnabled = New System.Windows.Forms.CheckBox()
        Me.ButtonPOSixmlImport = New System.Windows.Forms.Button()
        Me.Label75 = New System.Windows.Forms.Label()
        Me.txtPOSixmlNextPoll = New System.Windows.Forms.Label()
        Me.Label83 = New System.Windows.Forms.Label()
        Me.txtPOSixmlLastPoll = New System.Windows.Forms.Label()
        Me.Label88 = New System.Windows.Forms.Label()
        Me.txtPOSixmlPollInterval = New System.Windows.Forms.Label()
        Me.txtPOSixmlCurrentTime = New System.Windows.Forms.Label()
        Me.Label91 = New System.Windows.Forms.Label()
        Me.tabRPOS = New System.Windows.Forms.TabPage()
        Me.CheckBoxRPOSEnabled = New System.Windows.Forms.CheckBox()
        Me.ButtonRPOSImport = New System.Windows.Forms.Button()
        Me.Label78 = New System.Windows.Forms.Label()
        Me.txtRPOSNextPoll = New System.Windows.Forms.Label()
        Me.Label89 = New System.Windows.Forms.Label()
        Me.txtRPOSLastPoll = New System.Windows.Forms.Label()
        Me.Label92 = New System.Windows.Forms.Label()
        Me.txtRPOSPollInterval = New System.Windows.Forms.Label()
        Me.txtRPOSCurrentTime = New System.Windows.Forms.Label()
        Me.Label95 = New System.Windows.Forms.Label()
        Me.tabPosCommon = New System.Windows.Forms.TabPage()
        Me.chkEnableGatewayCommon = New System.Windows.Forms.CheckBox()
        Me.btImportCommon = New System.Windows.Forms.Button()
        Me.Label85 = New System.Windows.Forms.Label()
        Me.txtNextPoll = New System.Windows.Forms.Label()
        Me.Label93 = New System.Windows.Forms.Label()
        Me.txtLastPoll = New System.Windows.Forms.Label()
        Me.Label96 = New System.Windows.Forms.Label()
        Me.txtPollInterval = New System.Windows.Forms.Label()
        Me.txtCurrentTime = New System.Windows.Forms.Label()
        Me.Label99 = New System.Windows.Forms.Label()
        Me.TabSUSPOS = New System.Windows.Forms.TabPage()
        Me.CheckBox5 = New System.Windows.Forms.CheckBox()
        Me.Button11 = New System.Windows.Forms.Button()
        Me.Label90 = New System.Windows.Forms.Label()
        Me.Label94 = New System.Windows.Forms.Label()
        Me.Label97 = New System.Windows.Forms.Label()
        Me.Label98 = New System.Windows.Forms.Label()
        Me.Label100 = New System.Windows.Forms.Label()
        Me.Label101 = New System.Windows.Forms.Label()
        Me.Label102 = New System.Windows.Forms.Label()
        Me.Label103 = New System.Windows.Forms.Label()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.TextBoxAccessCode = New System.Windows.Forms.TextBox()
        Me.btnAccessCode = New System.Windows.Forms.Button()
        Me.Button7 = New System.Windows.Forms.Button()
        Me.btnEndOfDay = New System.Windows.Forms.Button()
        Me.LabelEndOfDayStatus = New System.Windows.Forms.Label()
        Me.tmrCommsBackoff = New System.Windows.Forms.Timer(Me.components)
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.tmrSessionLease = New System.Windows.Forms.Timer(Me.components)
        Me.NotifyIcon1 = New System.Windows.Forms.NotifyIcon(Me.components)
        Me.menuTrayIcon = New System.Windows.Forms.ContextMenu()
        Me.MenuItemTrayRunTimeClock = New System.Windows.Forms.MenuItem()
        Me.MenuItemTrayRunComprisActive = New System.Windows.Forms.MenuItem()
        Me.MenuItemTrayYISController = New System.Windows.Forms.MenuItem()
        Me.MenuItemTrayZKViewUsers = New System.Windows.Forms.MenuItem()
        Me.mnuClockSpacer = New System.Windows.Forms.MenuItem()
        Me.mnuShowLiveLink = New System.Windows.Forms.MenuItem()
        Me.MenuItem3 = New System.Windows.Forms.MenuItem()
        Me.mnuTrayExit = New System.Windows.Forms.MenuItem()
        Me.ImageListStatus = New System.Windows.Forms.ImageList(Me.components)
        Me.tmrPing = New System.Windows.Forms.Timer(Me.components)
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.PictureBoxBackoff = New System.Windows.Forms.PictureBox()
        Me.PictureBoxPingEnabled = New System.Windows.Forms.PictureBox()
        Me.PictureBoxOff = New System.Windows.Forms.PictureBox()
        Me.PictureBoxON = New System.Windows.Forms.PictureBox()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.TextBox5 = New System.Windows.Forms.TextBox()
        Me.Label53 = New System.Windows.Forms.Label()
        Me.Label54 = New System.Windows.Forms.Label()
        Me.CheckBox2 = New System.Windows.Forms.CheckBox()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.Label55 = New System.Windows.Forms.Label()
        Me.Label56 = New System.Windows.Forms.Label()
        Me.Label57 = New System.Windows.Forms.Label()
        Me.Label58 = New System.Windows.Forms.Label()
        Me.Label59 = New System.Windows.Forms.Label()
        Me.Label60 = New System.Windows.Forms.Label()
        Me.Label61 = New System.Windows.Forms.Label()
        Me.Label62 = New System.Windows.Forms.Label()
        Me.minimiseTimer = New System.Windows.Forms.Timer(Me.components)
        Me.tabTrafficData = New System.Windows.Forms.TabPage()
        Me.CheckBox6 = New System.Windows.Forms.CheckBox()
        Me.btnTrafficDataProcess = New System.Windows.Forms.Button()
        Me.Label104 = New System.Windows.Forms.Label()
        Me.Label105 = New System.Windows.Forms.Label()
        Me.Label106 = New System.Windows.Forms.Label()
        Me.Label107 = New System.Windows.Forms.Label()
        Me.Label108 = New System.Windows.Forms.Label()
        Me.Label109 = New System.Windows.Forms.Label()
        Me.Label110 = New System.Windows.Forms.Label()
        Me.Label111 = New System.Windows.Forms.Label()
        Me.TabControl1.SuspendLayout()
        Me.tabStatus.SuspendLayout()
        Me.tabIntouch.SuspendLayout()
        Me.tabOctane.SuspendLayout()
        Me.tabAloha.SuspendLayout()
        Me.tabPanasonic.SuspendLayout()
        Me.tabSchedule.SuspendLayout()
        Me.tabMicros.SuspendLayout()
        Me.tabSetup.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.tabCompris.SuspendLayout()
        Me.GroupComprisPollFiles.SuspendLayout()
        Me.GroupComprisPDPUpdates.SuspendLayout()
        Me.GroupComprisPDP.SuspendLayout()
        Me.GroupComprisPollingInformation.SuspendLayout()
        Me.tabPOSI.SuspendLayout()
        Me.tabTest.SuspendLayout()
        CType(Me.DataGrid1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabPAR.SuspendLayout()
        Me.tabSetPrice.SuspendLayout()
        Me.TabControl2.SuspendLayout()
        Me.tabSetPriceDCA.SuspendLayout()
        Me.tabSetPricePanasonic.SuspendLayout()
        Me.tabSetPricePAR.SuspendLayout()
        Me.tabOfflineCashManager.SuspendLayout()
        Me.tabZKFingerprint.SuspendLayout()
        Me.tabICG.SuspendLayout()
        Me.tabRMS.SuspendLayout()
        Me.tabNewPOS.SuspendLayout()
        Me.tabProxy.SuspendLayout()
        Me.grpProxy.SuspendLayout()
        Me.grpProxyAuth.SuspendLayout()
        Me.tabiPOS.SuspendLayout()
        Me.grpiPOSInformation.SuspendLayout()
        Me.grpiPOSProcess.SuspendLayout()
        Me.grpiPOSUpdates.SuspendLayout()
        Me.tabUniwell.SuspendLayout()
        Me.tabRadiant.SuspendLayout()
        Me.tabNAXML.SuspendLayout()
        Me.tabBreeze.SuspendLayout()
        Me.BreezeProcessGroup.SuspendLayout()
        Me.tabXpient.SuspendLayout()
        Me.tabRetalix.SuspendLayout()
        Me.tabArts.SuspendLayout()
        Me.tabPOSixml.SuspendLayout()
        Me.tabRPOS.SuspendLayout()
        Me.tabPosCommon.SuspendLayout()
        Me.TabSUSPOS.SuspendLayout()
        CType(Me.PictureBoxBackoff, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBoxPingEnabled, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBoxOff, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBoxON, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabTrafficData.SuspendLayout()
        Me.SuspendLayout()
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(392, 43)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(92, 23)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = "Start"
        '
        'LabelTitle
        '
        Me.LabelTitle.Font = New System.Drawing.Font("Verdana", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelTitle.ForeColor = System.Drawing.Color.Navy
        Me.LabelTitle.Location = New System.Drawing.Point(8, 9)
        Me.LabelTitle.Name = "LabelTitle"
        Me.LabelTitle.Size = New System.Drawing.Size(318, 18)
        Me.LabelTitle.TabIndex = 1
        Me.LabelTitle.Text = "The Franchise Suite"
        '
        'MainMenu1
        '
        Me.MainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem1, Me.MenuItem2})
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 0
        Me.MenuItem1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItemRunTimeClock, Me.MenuItemRunComprisActive, Me.MenuItemYISController, Me.MenuItemZKViewUsers, Me.MenuItem5})
        Me.MenuItem1.Text = "&File"
        '
        'MenuItemRunTimeClock
        '
        Me.MenuItemRunTimeClock.Index = 0
        Me.MenuItemRunTimeClock.Text = "&Run Easy Clock"
        Me.MenuItemRunTimeClock.Visible = False
        '
        'MenuItemRunComprisActive
        '
        Me.MenuItemRunComprisActive.Index = 1
        Me.MenuItemRunComprisActive.Text = "&Run Compris Active"
        Me.MenuItemRunComprisActive.Visible = False
        '
        'MenuItemYISController
        '
        Me.MenuItemYISController.Index = 2
        Me.MenuItemYISController.Text = "&Run YIS Controller"
        Me.MenuItemYISController.Visible = False
        '
        'MenuItemZKViewUsers
        '
        Me.MenuItemZKViewUsers.Index = 3
        Me.MenuItemZKViewUsers.Text = "View Clock Users"
        Me.MenuItemZKViewUsers.Visible = False
        '
        'MenuItem5
        '
        Me.MenuItem5.Index = 4
        Me.MenuItem5.MergeOrder = 1
        Me.MenuItem5.MergeType = System.Windows.Forms.MenuMerge.MergeItems
        Me.MenuItem5.Text = "E&xit"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 1
        Me.MenuItem2.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem4})
        Me.MenuItem2.Text = "&Help"
        '
        'MenuItem4
        '
        Me.MenuItem4.Index = 0
        Me.MenuItem4.Text = "&About"
        '
        'LabelStatus
        '
        Me.LabelStatus.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelStatus.ForeColor = System.Drawing.Color.Red
        Me.LabelStatus.Location = New System.Drawing.Point(416, 71)
        Me.LabelStatus.Name = "LabelStatus"
        Me.LabelStatus.Size = New System.Drawing.Size(68, 20)
        Me.LabelStatus.TabIndex = 10
        Me.LabelStatus.Text = "Stopped"
        Me.LabelStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Panel1
        '
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel1.Location = New System.Drawing.Point(8, 32)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(476, 4)
        Me.Panel1.TabIndex = 11
        '
        'btnProcessNow
        '
        Me.btnProcessNow.Location = New System.Drawing.Point(392, 95)
        Me.btnProcessNow.Name = "btnProcessNow"
        Me.btnProcessNow.Size = New System.Drawing.Size(92, 23)
        Me.btnProcessNow.TabIndex = 1
        Me.btnProcessNow.Text = "Process Now"
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.tabStatus)
        Me.TabControl1.Controls.Add(Me.tabIntouch)
        Me.TabControl1.Controls.Add(Me.tabOctane)
        Me.TabControl1.Controls.Add(Me.tabAloha)
        Me.TabControl1.Controls.Add(Me.tabPanasonic)
        Me.TabControl1.Controls.Add(Me.tabSchedule)
        Me.TabControl1.Controls.Add(Me.tabMicros)
        Me.TabControl1.Controls.Add(Me.tabSetup)
        Me.TabControl1.Controls.Add(Me.tabCompris)
        Me.TabControl1.Controls.Add(Me.tabPOSI)
        Me.TabControl1.Controls.Add(Me.tabTest)
        Me.TabControl1.Controls.Add(Me.tabPAR)
        Me.TabControl1.Controls.Add(Me.tabSetPrice)
        Me.TabControl1.Controls.Add(Me.tabOfflineCashManager)
        Me.TabControl1.Controls.Add(Me.tabZKFingerprint)
        Me.TabControl1.Controls.Add(Me.tabICG)
        Me.TabControl1.Controls.Add(Me.tabRMS)
        Me.TabControl1.Controls.Add(Me.tabNewPOS)
        Me.TabControl1.Controls.Add(Me.tabProxy)
        Me.TabControl1.Controls.Add(Me.tabiPOS)
        Me.TabControl1.Controls.Add(Me.tabUniwell)
        Me.TabControl1.Controls.Add(Me.tabRadiant)
        Me.TabControl1.Controls.Add(Me.tabNAXML)
        Me.TabControl1.Controls.Add(Me.tabBreeze)
        Me.TabControl1.Controls.Add(Me.tabXpient)
        Me.TabControl1.Controls.Add(Me.tabRetalix)
        Me.TabControl1.Controls.Add(Me.tabArts)
        Me.TabControl1.Controls.Add(Me.tabPOSixml)
        Me.TabControl1.Controls.Add(Me.tabRPOS)
        Me.TabControl1.Controls.Add(Me.tabPosCommon)
        Me.TabControl1.Controls.Add(Me.TabSUSPOS)
        Me.TabControl1.Controls.Add(Me.tabTrafficData)
        Me.TabControl1.Location = New System.Drawing.Point(7, 45)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(372, 224)
        Me.TabControl1.TabIndex = 13
        '
        'tabStatus
        '
        Me.tabStatus.Controls.Add(Me.TextBox3)
        Me.tabStatus.Controls.Add(Me.Label3)
        Me.tabStatus.Controls.Add(Me.TextBox1)
        Me.tabStatus.Controls.Add(Me.Label1)
        Me.tabStatus.Location = New System.Drawing.Point(4, 22)
        Me.tabStatus.Name = "tabStatus"
        Me.tabStatus.Size = New System.Drawing.Size(364, 198)
        Me.tabStatus.TabIndex = 0
        Me.tabStatus.Text = "Status"
        Me.tabStatus.UseVisualStyleBackColor = True
        '
        'TextBox3
        '
        Me.TextBox3.BackColor = System.Drawing.Color.White
        Me.TextBox3.Location = New System.Drawing.Point(8, 64)
        Me.TextBox3.Multiline = True
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.ReadOnly = True
        Me.TextBox3.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.TextBox3.Size = New System.Drawing.Size(352, 128)
        Me.TextBox3.TabIndex = 5
        Me.TextBox3.WordWrap = False
        '
        'Label3
        '
        Me.Label3.Location = New System.Drawing.Point(8, 49)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(100, 16)
        Me.Label3.TabIndex = 14
        Me.Label3.Text = "Previous Actions"
        '
        'TextBox1
        '
        Me.TextBox1.BackColor = System.Drawing.Color.White
        Me.TextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox1.Location = New System.Drawing.Point(8, 22)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.ReadOnly = True
        Me.TextBox1.Size = New System.Drawing.Size(240, 20)
        Me.TextBox1.TabIndex = 11
        Me.TextBox1.TabStop = False
        Me.TextBox1.Text = "Waiting ..."
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(8, 8)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(100, 16)
        Me.Label1.TabIndex = 10
        Me.Label1.Text = "Current Action"
        '
        'tabIntouch
        '
        Me.tabIntouch.Controls.Add(Me.CheckBoxIntouchEnabled)
        Me.tabIntouch.Controls.Add(Me.ButtonIntouchImport)
        Me.tabIntouch.Controls.Add(Me.Label34)
        Me.tabIntouch.Controls.Add(Me.txtIntouchNextPoll)
        Me.tabIntouch.Controls.Add(Me.Label41)
        Me.tabIntouch.Controls.Add(Me.txtIntouchLastPoll)
        Me.tabIntouch.Controls.Add(Me.Label44)
        Me.tabIntouch.Controls.Add(Me.txtIntouchPollInterval)
        Me.tabIntouch.Controls.Add(Me.txtIntouchCurrentTime)
        Me.tabIntouch.Controls.Add(Me.Label47)
        Me.tabIntouch.Location = New System.Drawing.Point(4, 22)
        Me.tabIntouch.Name = "tabIntouch"
        Me.tabIntouch.Size = New System.Drawing.Size(364, 198)
        Me.tabIntouch.TabIndex = 12
        Me.tabIntouch.Text = "Intouch"
        Me.tabIntouch.UseVisualStyleBackColor = True
        '
        'CheckBoxIntouchEnabled
        '
        Me.CheckBoxIntouchEnabled.Location = New System.Drawing.Point(24, 120)
        Me.CheckBoxIntouchEnabled.Name = "CheckBoxIntouchEnabled"
        Me.CheckBoxIntouchEnabled.Size = New System.Drawing.Size(136, 24)
        Me.CheckBoxIntouchEnabled.TabIndex = 66
        Me.CheckBoxIntouchEnabled.Text = "Enable Gateway"
        '
        'ButtonIntouchImport
        '
        Me.ButtonIntouchImport.Location = New System.Drawing.Point(249, 8)
        Me.ButtonIntouchImport.Name = "ButtonIntouchImport"
        Me.ButtonIntouchImport.Size = New System.Drawing.Size(98, 23)
        Me.ButtonIntouchImport.TabIndex = 65
        Me.ButtonIntouchImport.Text = "Process"
        '
        'Label34
        '
        Me.Label34.Location = New System.Drawing.Point(24, 96)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(80, 16)
        Me.Label34.TabIndex = 63
        Me.Label34.Text = "Next Poll:"
        '
        'txtIntouchNextPoll
        '
        Me.txtIntouchNextPoll.Location = New System.Drawing.Point(112, 96)
        Me.txtIntouchNextPoll.Name = "txtIntouchNextPoll"
        Me.txtIntouchNextPoll.Size = New System.Drawing.Size(88, 16)
        Me.txtIntouchNextPoll.TabIndex = 64
        '
        'Label41
        '
        Me.Label41.Location = New System.Drawing.Point(24, 72)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(80, 16)
        Me.Label41.TabIndex = 61
        Me.Label41.Text = "Last Poll:"
        '
        'txtIntouchLastPoll
        '
        Me.txtIntouchLastPoll.Location = New System.Drawing.Point(112, 73)
        Me.txtIntouchLastPoll.Name = "txtIntouchLastPoll"
        Me.txtIntouchLastPoll.Size = New System.Drawing.Size(88, 16)
        Me.txtIntouchLastPoll.TabIndex = 62
        '
        'Label44
        '
        Me.Label44.Location = New System.Drawing.Point(24, 48)
        Me.Label44.Name = "Label44"
        Me.Label44.Size = New System.Drawing.Size(80, 16)
        Me.Label44.TabIndex = 59
        Me.Label44.Text = "Poll Interval:"
        '
        'txtIntouchPollInterval
        '
        Me.txtIntouchPollInterval.Location = New System.Drawing.Point(112, 48)
        Me.txtIntouchPollInterval.Name = "txtIntouchPollInterval"
        Me.txtIntouchPollInterval.Size = New System.Drawing.Size(88, 16)
        Me.txtIntouchPollInterval.TabIndex = 60
        '
        'txtIntouchCurrentTime
        '
        Me.txtIntouchCurrentTime.Location = New System.Drawing.Point(112, 24)
        Me.txtIntouchCurrentTime.Name = "txtIntouchCurrentTime"
        Me.txtIntouchCurrentTime.Size = New System.Drawing.Size(88, 16)
        Me.txtIntouchCurrentTime.TabIndex = 58
        '
        'Label47
        '
        Me.Label47.Location = New System.Drawing.Point(24, 24)
        Me.Label47.Name = "Label47"
        Me.Label47.Size = New System.Drawing.Size(80, 16)
        Me.Label47.TabIndex = 57
        Me.Label47.Text = "Current Time:"
        '
        'tabOctane
        '
        Me.tabOctane.Controls.Add(Me.CheckBoxOctaneEnabled)
        Me.tabOctane.Controls.Add(Me.ButtonOctaneImport)
        Me.tabOctane.Controls.Add(Me.Label42)
        Me.tabOctane.Controls.Add(Me.txtOctaneNextPoll)
        Me.tabOctane.Controls.Add(Me.Label46)
        Me.tabOctane.Controls.Add(Me.txtOctaneLastPoll)
        Me.tabOctane.Controls.Add(Me.Label49)
        Me.tabOctane.Controls.Add(Me.txtOctanePollInterval)
        Me.tabOctane.Controls.Add(Me.txtOctaneCurrentTime)
        Me.tabOctane.Controls.Add(Me.Label52)
        Me.tabOctane.Location = New System.Drawing.Point(4, 22)
        Me.tabOctane.Name = "tabOctane"
        Me.tabOctane.Size = New System.Drawing.Size(364, 198)
        Me.tabOctane.TabIndex = 13
        Me.tabOctane.Text = "Octane"
        Me.tabOctane.UseVisualStyleBackColor = True
        '
        'CheckBoxOctaneEnabled
        '
        Me.CheckBoxOctaneEnabled.Location = New System.Drawing.Point(24, 120)
        Me.CheckBoxOctaneEnabled.Name = "CheckBoxOctaneEnabled"
        Me.CheckBoxOctaneEnabled.Size = New System.Drawing.Size(136, 24)
        Me.CheckBoxOctaneEnabled.TabIndex = 76
        Me.CheckBoxOctaneEnabled.Text = "Enable Gateway"
        '
        'ButtonOctaneImport
        '
        Me.ButtonOctaneImport.Location = New System.Drawing.Point(249, 8)
        Me.ButtonOctaneImport.Name = "ButtonOctaneImport"
        Me.ButtonOctaneImport.Size = New System.Drawing.Size(98, 23)
        Me.ButtonOctaneImport.TabIndex = 75
        Me.ButtonOctaneImport.Text = "Process"
        '
        'Label42
        '
        Me.Label42.Location = New System.Drawing.Point(24, 96)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(80, 16)
        Me.Label42.TabIndex = 73
        Me.Label42.Text = "Next Poll:"
        '
        'txtOctaneNextPoll
        '
        Me.txtOctaneNextPoll.Location = New System.Drawing.Point(112, 96)
        Me.txtOctaneNextPoll.Name = "txtOctaneNextPoll"
        Me.txtOctaneNextPoll.Size = New System.Drawing.Size(88, 16)
        Me.txtOctaneNextPoll.TabIndex = 74
        '
        'Label46
        '
        Me.Label46.Location = New System.Drawing.Point(24, 72)
        Me.Label46.Name = "Label46"
        Me.Label46.Size = New System.Drawing.Size(80, 16)
        Me.Label46.TabIndex = 71
        Me.Label46.Text = "Last Poll:"
        '
        'txtOctaneLastPoll
        '
        Me.txtOctaneLastPoll.Location = New System.Drawing.Point(112, 72)
        Me.txtOctaneLastPoll.Name = "txtOctaneLastPoll"
        Me.txtOctaneLastPoll.Size = New System.Drawing.Size(88, 16)
        Me.txtOctaneLastPoll.TabIndex = 72
        '
        'Label49
        '
        Me.Label49.Location = New System.Drawing.Point(24, 48)
        Me.Label49.Name = "Label49"
        Me.Label49.Size = New System.Drawing.Size(80, 16)
        Me.Label49.TabIndex = 69
        Me.Label49.Text = "Poll Interval:"
        '
        'txtOctanePollInterval
        '
        Me.txtOctanePollInterval.Location = New System.Drawing.Point(112, 48)
        Me.txtOctanePollInterval.Name = "txtOctanePollInterval"
        Me.txtOctanePollInterval.Size = New System.Drawing.Size(88, 16)
        Me.txtOctanePollInterval.TabIndex = 70
        '
        'txtOctaneCurrentTime
        '
        Me.txtOctaneCurrentTime.Location = New System.Drawing.Point(112, 24)
        Me.txtOctaneCurrentTime.Name = "txtOctaneCurrentTime"
        Me.txtOctaneCurrentTime.Size = New System.Drawing.Size(88, 16)
        Me.txtOctaneCurrentTime.TabIndex = 68
        '
        'Label52
        '
        Me.Label52.Location = New System.Drawing.Point(24, 24)
        Me.Label52.Name = "Label52"
        Me.Label52.Size = New System.Drawing.Size(80, 16)
        Me.Label52.TabIndex = 67
        Me.Label52.Text = "Current Time:"
        '
        'tabAloha
        '
        Me.tabAloha.Controls.Add(Me.ButtonAlohaProcessDateApply)
        Me.tabAloha.Controls.Add(Me.Label38)
        Me.tabAloha.Controls.Add(Me.dtpAlohaProcessDate)
        Me.tabAloha.Controls.Add(Me.LabelAlohaLastDateProcessed)
        Me.tabAloha.Controls.Add(Me.txtAlohaProcessDate)
        Me.tabAloha.Controls.Add(Me.chkSkipData)
        Me.tabAloha.Controls.Add(Me.chkSkipGrind)
        Me.tabAloha.Controls.Add(Me.CheckBoxAlohaEnabled)
        Me.tabAloha.Controls.Add(Me.btnAlohaImport)
        Me.tabAloha.Controls.Add(Me.txtAlohaStatus)
        Me.tabAloha.Controls.Add(Me.Label24)
        Me.tabAloha.Controls.Add(Me.txtAlohaNextPoll)
        Me.tabAloha.Controls.Add(Me.Label26)
        Me.tabAloha.Controls.Add(Me.txtAlohaLastPoll)
        Me.tabAloha.Controls.Add(Me.Label28)
        Me.tabAloha.Controls.Add(Me.txtAlohaPollInterval)
        Me.tabAloha.Controls.Add(Me.txtAlohaCurrentTime)
        Me.tabAloha.Controls.Add(Me.Label31)
        Me.tabAloha.Location = New System.Drawing.Point(4, 22)
        Me.tabAloha.Name = "tabAloha"
        Me.tabAloha.Size = New System.Drawing.Size(364, 198)
        Me.tabAloha.TabIndex = 8
        Me.tabAloha.Text = "Aloha"
        Me.tabAloha.UseVisualStyleBackColor = True
        '
        'ButtonAlohaProcessDateApply
        '
        Me.ButtonAlohaProcessDateApply.Location = New System.Drawing.Point(243, 72)
        Me.ButtonAlohaProcessDateApply.Name = "ButtonAlohaProcessDateApply"
        Me.ButtonAlohaProcessDateApply.Size = New System.Drawing.Size(91, 23)
        Me.ButtonAlohaProcessDateApply.TabIndex = 45
        Me.ButtonAlohaProcessDateApply.Text = "Apply"
        '
        'Label38
        '
        Me.Label38.Location = New System.Drawing.Point(240, 24)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(74, 16)
        Me.Label38.TabIndex = 44
        Me.Label38.Text = "Process From:"
        '
        'dtpAlohaProcessDate
        '
        Me.dtpAlohaProcessDate.CustomFormat = "dd-MMM-yyyy"
        Me.dtpAlohaProcessDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpAlohaProcessDate.Location = New System.Drawing.Point(243, 48)
        Me.dtpAlohaProcessDate.Name = "dtpAlohaProcessDate"
        Me.dtpAlohaProcessDate.Size = New System.Drawing.Size(91, 20)
        Me.dtpAlohaProcessDate.TabIndex = 43
        Me.dtpAlohaProcessDate.Value = New Date(2007, 9, 1, 0, 0, 0, 0)
        '
        'LabelAlohaLastDateProcessed
        '
        Me.LabelAlohaLastDateProcessed.Location = New System.Drawing.Point(24, 120)
        Me.LabelAlohaLastDateProcessed.Name = "LabelAlohaLastDateProcessed"
        Me.LabelAlohaLastDateProcessed.Size = New System.Drawing.Size(80, 16)
        Me.LabelAlohaLastDateProcessed.TabIndex = 42
        Me.LabelAlohaLastDateProcessed.Text = "Process Date:"
        '
        'txtAlohaProcessDate
        '
        Me.txtAlohaProcessDate.Location = New System.Drawing.Point(108, 120)
        Me.txtAlohaProcessDate.Name = "txtAlohaProcessDate"
        Me.txtAlohaProcessDate.Size = New System.Drawing.Size(88, 16)
        Me.txtAlohaProcessDate.TabIndex = 41
        '
        'chkSkipData
        '
        Me.chkSkipData.Location = New System.Drawing.Point(243, 143)
        Me.chkSkipData.Name = "chkSkipData"
        Me.chkSkipData.Size = New System.Drawing.Size(104, 24)
        Me.chkSkipData.TabIndex = 37
        Me.chkSkipData.Text = "Skip DATA dir"
        '
        'chkSkipGrind
        '
        Me.chkSkipGrind.Location = New System.Drawing.Point(243, 171)
        Me.chkSkipGrind.Name = "chkSkipGrind"
        Me.chkSkipGrind.Size = New System.Drawing.Size(104, 24)
        Me.chkSkipGrind.TabIndex = 36
        Me.chkSkipGrind.Text = "Do Not Grind"
        '
        'CheckBoxAlohaEnabled
        '
        Me.CheckBoxAlohaEnabled.Location = New System.Drawing.Point(27, 143)
        Me.CheckBoxAlohaEnabled.Name = "CheckBoxAlohaEnabled"
        Me.CheckBoxAlohaEnabled.Size = New System.Drawing.Size(136, 24)
        Me.CheckBoxAlohaEnabled.TabIndex = 35
        Me.CheckBoxAlohaEnabled.Text = "Enable Gateway"
        '
        'btnAlohaImport
        '
        Me.btnAlohaImport.Location = New System.Drawing.Point(243, 100)
        Me.btnAlohaImport.Name = "btnAlohaImport"
        Me.btnAlohaImport.Size = New System.Drawing.Size(91, 23)
        Me.btnAlohaImport.TabIndex = 34
        Me.btnAlohaImport.Text = "Process"
        '
        'txtAlohaStatus
        '
        Me.txtAlohaStatus.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtAlohaStatus.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtAlohaStatus.Location = New System.Drawing.Point(27, 171)
        Me.txtAlohaStatus.Multiline = True
        Me.txtAlohaStatus.Name = "txtAlohaStatus"
        Me.txtAlohaStatus.ReadOnly = True
        Me.txtAlohaStatus.Size = New System.Drawing.Size(210, 25)
        Me.txtAlohaStatus.TabIndex = 32
        '
        'Label24
        '
        Me.Label24.Location = New System.Drawing.Point(24, 96)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(80, 16)
        Me.Label24.TabIndex = 30
        Me.Label24.Text = "Next Poll:"
        '
        'txtAlohaNextPoll
        '
        Me.txtAlohaNextPoll.Location = New System.Drawing.Point(108, 96)
        Me.txtAlohaNextPoll.Name = "txtAlohaNextPoll"
        Me.txtAlohaNextPoll.Size = New System.Drawing.Size(88, 16)
        Me.txtAlohaNextPoll.TabIndex = 31
        '
        'Label26
        '
        Me.Label26.Location = New System.Drawing.Point(24, 72)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(80, 16)
        Me.Label26.TabIndex = 28
        Me.Label26.Text = "Last Poll:"
        '
        'txtAlohaLastPoll
        '
        Me.txtAlohaLastPoll.Location = New System.Drawing.Point(108, 72)
        Me.txtAlohaLastPoll.Name = "txtAlohaLastPoll"
        Me.txtAlohaLastPoll.Size = New System.Drawing.Size(88, 16)
        Me.txtAlohaLastPoll.TabIndex = 29
        '
        'Label28
        '
        Me.Label28.Location = New System.Drawing.Point(24, 48)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(80, 16)
        Me.Label28.TabIndex = 26
        Me.Label28.Text = "Poll Interval:"
        '
        'txtAlohaPollInterval
        '
        Me.txtAlohaPollInterval.Location = New System.Drawing.Point(108, 48)
        Me.txtAlohaPollInterval.Name = "txtAlohaPollInterval"
        Me.txtAlohaPollInterval.Size = New System.Drawing.Size(88, 16)
        Me.txtAlohaPollInterval.TabIndex = 27
        '
        'txtAlohaCurrentTime
        '
        Me.txtAlohaCurrentTime.Location = New System.Drawing.Point(108, 24)
        Me.txtAlohaCurrentTime.Name = "txtAlohaCurrentTime"
        Me.txtAlohaCurrentTime.Size = New System.Drawing.Size(88, 16)
        Me.txtAlohaCurrentTime.TabIndex = 25
        '
        'Label31
        '
        Me.Label31.Location = New System.Drawing.Point(24, 24)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(80, 16)
        Me.Label31.TabIndex = 24
        Me.Label31.Text = "Current Time:"
        '
        'tabPanasonic
        '
        Me.tabPanasonic.Controls.Add(Me.CheckBoxPanasonicEnabled)
        Me.tabPanasonic.Controls.Add(Me.ButtonPanasonicImport)
        Me.tabPanasonic.Controls.Add(Me.Label30)
        Me.tabPanasonic.Controls.Add(Me.txtPanasonicNextPoll)
        Me.tabPanasonic.Controls.Add(Me.Label37)
        Me.tabPanasonic.Controls.Add(Me.txtPanasonicLastPoll)
        Me.tabPanasonic.Controls.Add(Me.Label40)
        Me.tabPanasonic.Controls.Add(Me.txtPanasonicPollInterval)
        Me.tabPanasonic.Controls.Add(Me.txtPanasonicCurrentTime)
        Me.tabPanasonic.Controls.Add(Me.Label43)
        Me.tabPanasonic.Location = New System.Drawing.Point(4, 22)
        Me.tabPanasonic.Name = "tabPanasonic"
        Me.tabPanasonic.Size = New System.Drawing.Size(364, 198)
        Me.tabPanasonic.TabIndex = 11
        Me.tabPanasonic.Text = "Panasonic"
        Me.tabPanasonic.UseVisualStyleBackColor = True
        '
        'CheckBoxPanasonicEnabled
        '
        Me.CheckBoxPanasonicEnabled.Location = New System.Drawing.Point(24, 120)
        Me.CheckBoxPanasonicEnabled.Name = "CheckBoxPanasonicEnabled"
        Me.CheckBoxPanasonicEnabled.Size = New System.Drawing.Size(136, 24)
        Me.CheckBoxPanasonicEnabled.TabIndex = 56
        Me.CheckBoxPanasonicEnabled.Text = "Enable Gateway"
        '
        'ButtonPanasonicImport
        '
        Me.ButtonPanasonicImport.Location = New System.Drawing.Point(272, 8)
        Me.ButtonPanasonicImport.Name = "ButtonPanasonicImport"
        Me.ButtonPanasonicImport.Size = New System.Drawing.Size(75, 23)
        Me.ButtonPanasonicImport.TabIndex = 55
        Me.ButtonPanasonicImport.Text = "Process"
        '
        'Label30
        '
        Me.Label30.Location = New System.Drawing.Point(24, 96)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(80, 16)
        Me.Label30.TabIndex = 53
        Me.Label30.Text = "Next Poll:"
        '
        'txtPanasonicNextPoll
        '
        Me.txtPanasonicNextPoll.Location = New System.Drawing.Point(112, 96)
        Me.txtPanasonicNextPoll.Name = "txtPanasonicNextPoll"
        Me.txtPanasonicNextPoll.Size = New System.Drawing.Size(88, 16)
        Me.txtPanasonicNextPoll.TabIndex = 54
        '
        'Label37
        '
        Me.Label37.Location = New System.Drawing.Point(24, 72)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(80, 16)
        Me.Label37.TabIndex = 51
        Me.Label37.Text = "Last Poll:"
        '
        'txtPanasonicLastPoll
        '
        Me.txtPanasonicLastPoll.Location = New System.Drawing.Point(112, 72)
        Me.txtPanasonicLastPoll.Name = "txtPanasonicLastPoll"
        Me.txtPanasonicLastPoll.Size = New System.Drawing.Size(88, 16)
        Me.txtPanasonicLastPoll.TabIndex = 52
        '
        'Label40
        '
        Me.Label40.Location = New System.Drawing.Point(24, 48)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(80, 16)
        Me.Label40.TabIndex = 49
        Me.Label40.Text = "Poll Interval:"
        '
        'txtPanasonicPollInterval
        '
        Me.txtPanasonicPollInterval.Location = New System.Drawing.Point(112, 48)
        Me.txtPanasonicPollInterval.Name = "txtPanasonicPollInterval"
        Me.txtPanasonicPollInterval.Size = New System.Drawing.Size(88, 16)
        Me.txtPanasonicPollInterval.TabIndex = 50
        '
        'txtPanasonicCurrentTime
        '
        Me.txtPanasonicCurrentTime.Location = New System.Drawing.Point(112, 24)
        Me.txtPanasonicCurrentTime.Name = "txtPanasonicCurrentTime"
        Me.txtPanasonicCurrentTime.Size = New System.Drawing.Size(88, 16)
        Me.txtPanasonicCurrentTime.TabIndex = 48
        '
        'Label43
        '
        Me.Label43.Location = New System.Drawing.Point(24, 24)
        Me.Label43.Name = "Label43"
        Me.Label43.Size = New System.Drawing.Size(80, 16)
        Me.Label43.TabIndex = 47
        Me.Label43.Text = "Current Time:"
        '
        'tabSchedule
        '
        Me.tabSchedule.Controls.Add(Me.TextBoxAwake)
        Me.tabSchedule.Controls.Add(Me.Label11)
        Me.tabSchedule.Controls.Add(Me.Label13)
        Me.tabSchedule.Controls.Add(Me.txtFlowInterval)
        Me.tabSchedule.Controls.Add(Me.Label6)
        Me.tabSchedule.Controls.Add(Me.Label5)
        Me.tabSchedule.Controls.Add(Me.CheckBox1)
        Me.tabSchedule.Controls.Add(Me.Label4)
        Me.tabSchedule.Location = New System.Drawing.Point(4, 22)
        Me.tabSchedule.Name = "tabSchedule"
        Me.tabSchedule.Size = New System.Drawing.Size(364, 198)
        Me.tabSchedule.TabIndex = 1
        Me.tabSchedule.Text = "Schedule"
        Me.tabSchedule.UseVisualStyleBackColor = True
        '
        'TextBoxAwake
        '
        Me.TextBoxAwake.Location = New System.Drawing.Point(190, 118)
        Me.TextBoxAwake.Name = "TextBoxAwake"
        Me.TextBoxAwake.Size = New System.Drawing.Size(40, 20)
        Me.TextBoxAwake.TabIndex = 18
        Me.TextBoxAwake.Text = "30"
        Me.TextBoxAwake.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label11
        '
        Me.Label11.Location = New System.Drawing.Point(238, 120)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(81, 16)
        Me.Label11.TabIndex = 17
        Me.Label11.Text = "minutes."
        '
        'Label13
        '
        Me.Label13.Location = New System.Drawing.Point(44, 120)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(152, 16)
        Me.Label13.TabIndex = 16
        Me.Label13.Text = "Send awake message every"
        '
        'txtFlowInterval
        '
        Me.txtFlowInterval.Location = New System.Drawing.Point(190, 82)
        Me.txtFlowInterval.Name = "txtFlowInterval"
        Me.txtFlowInterval.Size = New System.Drawing.Size(40, 20)
        Me.txtFlowInterval.TabIndex = 15
        Me.txtFlowInterval.Text = "10"
        Me.txtFlowInterval.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label6
        '
        Me.Label6.Location = New System.Drawing.Point(238, 84)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(81, 16)
        Me.Label6.TabIndex = 14
        Me.Label6.Text = "minutes."
        '
        'Label5
        '
        Me.Label5.Location = New System.Drawing.Point(142, 84)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(50, 16)
        Me.Label5.TabIndex = 13
        Me.Label5.Text = "or Every"
        '
        'CheckBox1
        '
        Me.CheckBox1.Location = New System.Drawing.Point(46, 80)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(96, 24)
        Me.CheckBox1.TabIndex = 12
        Me.CheckBox1.Text = "Live Transfer"
        '
        'Label4
        '
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(8, 16)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(144, 16)
        Me.Label4.TabIndex = 11
        Me.Label4.Text = "Data Transfer Schedule"
        '
        'tabMicros
        '
        Me.tabMicros.Controls.Add(Me.CheckBoxMicrosEnabled)
        Me.tabMicros.Controls.Add(Me.btnMicrosImport)
        Me.tabMicros.Controls.Add(Me.Label25)
        Me.tabMicros.Controls.Add(Me.txtMicrosNextPoll)
        Me.tabMicros.Controls.Add(Me.Label29)
        Me.tabMicros.Controls.Add(Me.txtMicrosLastPoll)
        Me.tabMicros.Controls.Add(Me.Label32)
        Me.tabMicros.Controls.Add(Me.txtMicrosPollInterval)
        Me.tabMicros.Controls.Add(Me.txtMicrosCurrentTime)
        Me.tabMicros.Controls.Add(Me.Label35)
        Me.tabMicros.Location = New System.Drawing.Point(4, 22)
        Me.tabMicros.Name = "tabMicros"
        Me.tabMicros.Size = New System.Drawing.Size(364, 198)
        Me.tabMicros.TabIndex = 9
        Me.tabMicros.Text = "Micros"
        Me.tabMicros.UseVisualStyleBackColor = True
        '
        'CheckBoxMicrosEnabled
        '
        Me.CheckBoxMicrosEnabled.Location = New System.Drawing.Point(24, 120)
        Me.CheckBoxMicrosEnabled.Name = "CheckBoxMicrosEnabled"
        Me.CheckBoxMicrosEnabled.Size = New System.Drawing.Size(136, 24)
        Me.CheckBoxMicrosEnabled.TabIndex = 46
        Me.CheckBoxMicrosEnabled.Text = "Enable Gateway"
        '
        'btnMicrosImport
        '
        Me.btnMicrosImport.Location = New System.Drawing.Point(272, 8)
        Me.btnMicrosImport.Name = "btnMicrosImport"
        Me.btnMicrosImport.Size = New System.Drawing.Size(75, 23)
        Me.btnMicrosImport.TabIndex = 45
        Me.btnMicrosImport.Text = "Process"
        '
        'Label25
        '
        Me.Label25.Location = New System.Drawing.Point(24, 96)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(80, 16)
        Me.Label25.TabIndex = 42
        Me.Label25.Text = "Next Poll:"
        '
        'txtMicrosNextPoll
        '
        Me.txtMicrosNextPoll.Location = New System.Drawing.Point(112, 96)
        Me.txtMicrosNextPoll.Name = "txtMicrosNextPoll"
        Me.txtMicrosNextPoll.Size = New System.Drawing.Size(88, 16)
        Me.txtMicrosNextPoll.TabIndex = 43
        '
        'Label29
        '
        Me.Label29.Location = New System.Drawing.Point(24, 72)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(80, 16)
        Me.Label29.TabIndex = 40
        Me.Label29.Text = "Last Poll:"
        '
        'txtMicrosLastPoll
        '
        Me.txtMicrosLastPoll.Location = New System.Drawing.Point(112, 72)
        Me.txtMicrosLastPoll.Name = "txtMicrosLastPoll"
        Me.txtMicrosLastPoll.Size = New System.Drawing.Size(88, 16)
        Me.txtMicrosLastPoll.TabIndex = 41
        '
        'Label32
        '
        Me.Label32.Location = New System.Drawing.Point(24, 48)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(80, 16)
        Me.Label32.TabIndex = 38
        Me.Label32.Text = "Poll Interval:"
        '
        'txtMicrosPollInterval
        '
        Me.txtMicrosPollInterval.Location = New System.Drawing.Point(112, 48)
        Me.txtMicrosPollInterval.Name = "txtMicrosPollInterval"
        Me.txtMicrosPollInterval.Size = New System.Drawing.Size(88, 16)
        Me.txtMicrosPollInterval.TabIndex = 39
        '
        'txtMicrosCurrentTime
        '
        Me.txtMicrosCurrentTime.Location = New System.Drawing.Point(112, 24)
        Me.txtMicrosCurrentTime.Name = "txtMicrosCurrentTime"
        Me.txtMicrosCurrentTime.Size = New System.Drawing.Size(88, 16)
        Me.txtMicrosCurrentTime.TabIndex = 37
        '
        'Label35
        '
        Me.Label35.Location = New System.Drawing.Point(24, 24)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(80, 16)
        Me.Label35.TabIndex = 36
        Me.Label35.Text = "Current Time:"
        '
        'tabSetup
        '
        Me.tabSetup.Controls.Add(Me.btnReceiveMessage)
        Me.tabSetup.Controls.Add(Me.btnSysInfo)
        Me.tabSetup.Controls.Add(Me.GroupBox1)
        Me.tabSetup.Location = New System.Drawing.Point(4, 22)
        Me.tabSetup.Name = "tabSetup"
        Me.tabSetup.Size = New System.Drawing.Size(364, 198)
        Me.tabSetup.TabIndex = 7
        Me.tabSetup.Text = "Setup"
        Me.tabSetup.UseVisualStyleBackColor = True
        '
        'btnReceiveMessage
        '
        Me.btnReceiveMessage.Location = New System.Drawing.Point(35, 160)
        Me.btnReceiveMessage.Name = "btnReceiveMessage"
        Me.btnReceiveMessage.Size = New System.Drawing.Size(138, 24)
        Me.btnReceiveMessage.TabIndex = 7
        Me.btnReceiveMessage.Text = "&Receive Messages"
        '
        'btnSysInfo
        '
        Me.btnSysInfo.Location = New System.Drawing.Point(206, 160)
        Me.btnSysInfo.Name = "btnSysInfo"
        Me.btnSysInfo.Size = New System.Drawing.Size(138, 24)
        Me.btnSysInfo.TabIndex = 6
        Me.btnSysInfo.Text = "Send System &Info"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.chkAutoStartOnLogin)
        Me.GroupBox1.Controls.Add(Me.ButtonNewMachineID)
        Me.GroupBox1.Controls.Add(Me.txtConfiguredStore)
        Me.GroupBox1.Controls.Add(Me.Label23)
        Me.GroupBox1.Controls.Add(Me.btnUpdateStore)
        Me.GroupBox1.Controls.Add(Me.Label22)
        Me.GroupBox1.Controls.Add(Me.Label21)
        Me.GroupBox1.Controls.Add(Me.txtEntityId)
        Me.GroupBox1.Controls.Add(Me.btnLoadStores)
        Me.GroupBox1.Controls.Add(Me.cboStores)
        Me.GroupBox1.Location = New System.Drawing.Point(16, 16)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(336, 138)
        Me.GroupBox1.TabIndex = 5
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Set store number"
        '
        'chkAutoStartOnLogin
        '
        Me.chkAutoStartOnLogin.AutoSize = True
        Me.chkAutoStartOnLogin.Location = New System.Drawing.Point(19, 107)
        Me.chkAutoStartOnLogin.Name = "chkAutoStartOnLogin"
        Me.chkAutoStartOnLogin.Size = New System.Drawing.Size(116, 17)
        Me.chkAutoStartOnLogin.TabIndex = 15
        Me.chkAutoStartOnLogin.Text = "Auto Start LiveLink"
        Me.chkAutoStartOnLogin.UseVisualStyleBackColor = True
        '
        'ButtonNewMachineID
        '
        Me.ButtonNewMachineID.Location = New System.Drawing.Point(190, 107)
        Me.ButtonNewMachineID.Name = "ButtonNewMachineID"
        Me.ButtonNewMachineID.Size = New System.Drawing.Size(138, 23)
        Me.ButtonNewMachineID.TabIndex = 14
        Me.ButtonNewMachineID.Text = "New &Machine ID"
        Me.ButtonNewMachineID.UseVisualStyleBackColor = True
        '
        'txtConfiguredStore
        '
        Me.txtConfiguredStore.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtConfiguredStore.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.txtConfiguredStore.Location = New System.Drawing.Point(112, 81)
        Me.txtConfiguredStore.Name = "txtConfiguredStore"
        Me.txtConfiguredStore.ReadOnly = True
        Me.txtConfiguredStore.Size = New System.Drawing.Size(72, 20)
        Me.txtConfiguredStore.TabIndex = 12
        Me.txtConfiguredStore.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label23
        '
        Me.Label23.Location = New System.Drawing.Point(16, 78)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(96, 23)
        Me.Label23.TabIndex = 11
        Me.Label23.Text = "Configured Store"
        Me.Label23.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnUpdateStore
        '
        Me.btnUpdateStore.Enabled = False
        Me.btnUpdateStore.Location = New System.Drawing.Point(190, 78)
        Me.btnUpdateStore.Name = "btnUpdateStore"
        Me.btnUpdateStore.Size = New System.Drawing.Size(138, 23)
        Me.btnUpdateStore.TabIndex = 10
        Me.btnUpdateStore.Text = "&Save Store Definition"
        '
        'Label22
        '
        Me.Label22.Location = New System.Drawing.Point(16, 52)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(80, 23)
        Me.Label22.TabIndex = 9
        Me.Label22.Text = "Store Number"
        Me.Label22.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label21
        '
        Me.Label21.Location = New System.Drawing.Point(16, 22)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(72, 23)
        Me.Label21.TabIndex = 8
        Me.Label21.Text = "Store Name"
        Me.Label21.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtEntityId
        '
        Me.txtEntityId.Location = New System.Drawing.Point(112, 52)
        Me.txtEntityId.Name = "txtEntityId"
        Me.txtEntityId.ReadOnly = True
        Me.txtEntityId.Size = New System.Drawing.Size(72, 20)
        Me.txtEntityId.TabIndex = 7
        Me.txtEntityId.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'btnLoadStores
        '
        Me.btnLoadStores.Location = New System.Drawing.Point(190, 48)
        Me.btnLoadStores.Name = "btnLoadStores"
        Me.btnLoadStores.Size = New System.Drawing.Size(138, 24)
        Me.btnLoadStores.TabIndex = 6
        Me.btnLoadStores.Text = "&Load Stores"
        '
        'cboStores
        '
        Me.cboStores.DisplayMember = "Entity"
        Me.cboStores.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboStores.Location = New System.Drawing.Point(112, 24)
        Me.cboStores.Name = "cboStores"
        Me.cboStores.Size = New System.Drawing.Size(216, 21)
        Me.cboStores.Sorted = True
        Me.cboStores.TabIndex = 5
        Me.cboStores.ValueMember = "EntityId"
        '
        'tabCompris
        '
        Me.tabCompris.Controls.Add(Me.GroupComprisPollFiles)
        Me.tabCompris.Controls.Add(Me.GroupComprisPDPUpdates)
        Me.tabCompris.Controls.Add(Me.GroupComprisPDP)
        Me.tabCompris.Controls.Add(Me.GroupComprisPollingInformation)
        Me.tabCompris.Location = New System.Drawing.Point(4, 22)
        Me.tabCompris.Name = "tabCompris"
        Me.tabCompris.Size = New System.Drawing.Size(364, 198)
        Me.tabCompris.TabIndex = 10
        Me.tabCompris.Text = "Compris"
        Me.tabCompris.UseVisualStyleBackColor = True
        '
        'GroupComprisPollFiles
        '
        Me.GroupComprisPollFiles.Controls.Add(Me.ButtonComprisPollFilesProcess)
        Me.GroupComprisPollFiles.Location = New System.Drawing.Point(205, 135)
        Me.GroupComprisPollFiles.Name = "GroupComprisPollFiles"
        Me.GroupComprisPollFiles.Size = New System.Drawing.Size(152, 56)
        Me.GroupComprisPollFiles.TabIndex = 51
        Me.GroupComprisPollFiles.TabStop = False
        Me.GroupComprisPollFiles.Text = "Poll Files"
        Me.GroupComprisPollFiles.Visible = False
        '
        'ButtonComprisPollFilesProcess
        '
        Me.ButtonComprisPollFilesProcess.Location = New System.Drawing.Point(18, 21)
        Me.ButtonComprisPollFilesProcess.Name = "ButtonComprisPollFilesProcess"
        Me.ButtonComprisPollFilesProcess.Size = New System.Drawing.Size(114, 24)
        Me.ButtonComprisPollFilesProcess.TabIndex = 0
        Me.ButtonComprisPollFilesProcess.Text = "Process Files"
        Me.ButtonComprisPollFilesProcess.UseVisualStyleBackColor = True
        '
        'GroupComprisPDPUpdates
        '
        Me.GroupComprisPDPUpdates.Controls.Add(Me.ButtonComprisPDPUpdatesProcess)
        Me.GroupComprisPDPUpdates.Location = New System.Drawing.Point(205, 71)
        Me.GroupComprisPDPUpdates.Name = "GroupComprisPDPUpdates"
        Me.GroupComprisPDPUpdates.Size = New System.Drawing.Size(152, 56)
        Me.GroupComprisPDPUpdates.TabIndex = 50
        Me.GroupComprisPDPUpdates.TabStop = False
        Me.GroupComprisPDPUpdates.Text = "PDP Data Updates"
        Me.GroupComprisPDPUpdates.Visible = False
        '
        'ButtonComprisPDPUpdatesProcess
        '
        Me.ButtonComprisPDPUpdatesProcess.Location = New System.Drawing.Point(18, 21)
        Me.ButtonComprisPDPUpdatesProcess.Name = "ButtonComprisPDPUpdatesProcess"
        Me.ButtonComprisPDPUpdatesProcess.Size = New System.Drawing.Size(114, 24)
        Me.ButtonComprisPDPUpdatesProcess.TabIndex = 0
        Me.ButtonComprisPDPUpdatesProcess.Text = "Process Updates"
        Me.ButtonComprisPDPUpdatesProcess.UseVisualStyleBackColor = True
        '
        'GroupComprisPDP
        '
        Me.GroupComprisPDP.Controls.Add(Me.ButtonComprisPDPProcess)
        Me.GroupComprisPDP.Location = New System.Drawing.Point(205, 7)
        Me.GroupComprisPDP.Name = "GroupComprisPDP"
        Me.GroupComprisPDP.Size = New System.Drawing.Size(152, 56)
        Me.GroupComprisPDP.TabIndex = 49
        Me.GroupComprisPDP.TabStop = False
        Me.GroupComprisPDP.Text = "PDP Data"
        Me.GroupComprisPDP.Visible = False
        '
        'ButtonComprisPDPProcess
        '
        Me.ButtonComprisPDPProcess.Location = New System.Drawing.Point(18, 21)
        Me.ButtonComprisPDPProcess.Name = "ButtonComprisPDPProcess"
        Me.ButtonComprisPDPProcess.Size = New System.Drawing.Size(114, 24)
        Me.ButtonComprisPDPProcess.TabIndex = 0
        Me.ButtonComprisPDPProcess.Text = "Process PDP"
        Me.ButtonComprisPDPProcess.UseVisualStyleBackColor = True
        '
        'GroupComprisPollingInformation
        '
        Me.GroupComprisPollingInformation.Controls.Add(Me.Label39)
        Me.GroupComprisPollingInformation.Controls.Add(Me.ButtonComprisImport)
        Me.GroupComprisPollingInformation.Controls.Add(Me.CheckBoxComprisEnabled)
        Me.GroupComprisPollingInformation.Controls.Add(Me.txtComprisCurrentTime)
        Me.GroupComprisPollingInformation.Controls.Add(Me.Label27)
        Me.GroupComprisPollingInformation.Controls.Add(Me.txtComprisPollInterval)
        Me.GroupComprisPollingInformation.Controls.Add(Me.txtComprisNextPoll)
        Me.GroupComprisPollingInformation.Controls.Add(Me.Label36)
        Me.GroupComprisPollingInformation.Controls.Add(Me.Label33)
        Me.GroupComprisPollingInformation.Controls.Add(Me.txtComprisLastPoll)
        Me.GroupComprisPollingInformation.Location = New System.Drawing.Point(5, 7)
        Me.GroupComprisPollingInformation.Name = "GroupComprisPollingInformation"
        Me.GroupComprisPollingInformation.Size = New System.Drawing.Size(188, 184)
        Me.GroupComprisPollingInformation.TabIndex = 45
        Me.GroupComprisPollingInformation.TabStop = False
        Me.GroupComprisPollingInformation.Text = "Polling Information"
        '
        'Label39
        '
        Me.Label39.Location = New System.Drawing.Point(11, 23)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(80, 16)
        Me.Label39.TabIndex = 36
        Me.Label39.Text = "Current Time:"
        '
        'ButtonComprisImport
        '
        Me.ButtonComprisImport.Location = New System.Drawing.Point(13, 149)
        Me.ButtonComprisImport.Name = "ButtonComprisImport"
        Me.ButtonComprisImport.Size = New System.Drawing.Size(162, 24)
        Me.ButtonComprisImport.TabIndex = 0
        Me.ButtonComprisImport.Text = "Process"
        '
        'CheckBoxComprisEnabled
        '
        Me.CheckBoxComprisEnabled.Location = New System.Drawing.Point(14, 119)
        Me.CheckBoxComprisEnabled.Name = "CheckBoxComprisEnabled"
        Me.CheckBoxComprisEnabled.Size = New System.Drawing.Size(105, 26)
        Me.CheckBoxComprisEnabled.TabIndex = 44
        Me.CheckBoxComprisEnabled.Text = "Enable Gateway"
        '
        'txtComprisCurrentTime
        '
        Me.txtComprisCurrentTime.Location = New System.Drawing.Point(87, 23)
        Me.txtComprisCurrentTime.Name = "txtComprisCurrentTime"
        Me.txtComprisCurrentTime.Size = New System.Drawing.Size(88, 16)
        Me.txtComprisCurrentTime.TabIndex = 37
        '
        'Label27
        '
        Me.Label27.Location = New System.Drawing.Point(11, 95)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(80, 16)
        Me.Label27.TabIndex = 42
        Me.Label27.Text = "Next Poll:"
        '
        'txtComprisPollInterval
        '
        Me.txtComprisPollInterval.Location = New System.Drawing.Point(87, 47)
        Me.txtComprisPollInterval.Name = "txtComprisPollInterval"
        Me.txtComprisPollInterval.Size = New System.Drawing.Size(88, 16)
        Me.txtComprisPollInterval.TabIndex = 39
        '
        'txtComprisNextPoll
        '
        Me.txtComprisNextPoll.Location = New System.Drawing.Point(87, 95)
        Me.txtComprisNextPoll.Name = "txtComprisNextPoll"
        Me.txtComprisNextPoll.Size = New System.Drawing.Size(88, 16)
        Me.txtComprisNextPoll.TabIndex = 43
        '
        'Label36
        '
        Me.Label36.Location = New System.Drawing.Point(11, 47)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(80, 16)
        Me.Label36.TabIndex = 38
        Me.Label36.Text = "Poll Interval:"
        '
        'Label33
        '
        Me.Label33.Location = New System.Drawing.Point(11, 71)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(80, 16)
        Me.Label33.TabIndex = 40
        Me.Label33.Text = "Last Poll:"
        '
        'txtComprisLastPoll
        '
        Me.txtComprisLastPoll.Location = New System.Drawing.Point(87, 71)
        Me.txtComprisLastPoll.Name = "txtComprisLastPoll"
        Me.txtComprisLastPoll.Size = New System.Drawing.Size(88, 16)
        Me.txtComprisLastPoll.TabIndex = 41
        '
        'tabPOSI
        '
        Me.tabPOSI.Controls.Add(Me.Label10)
        Me.tabPOSI.Controls.Add(Me.txtPosiNextPoll)
        Me.tabPOSI.Controls.Add(Me.Label12)
        Me.tabPOSI.Controls.Add(Me.txtPosiLastPoll)
        Me.tabPOSI.Controls.Add(Me.Label14)
        Me.tabPOSI.Controls.Add(Me.txtPosiPollInterval)
        Me.tabPOSI.Controls.Add(Me.txtPosiCurrentTime)
        Me.tabPOSI.Controls.Add(Me.Label17)
        Me.tabPOSI.Controls.Add(Me.txtPosiStatus)
        Me.tabPOSI.Controls.Add(Me.btnPosiImport)
        Me.tabPOSI.Controls.Add(Me.CheckBoxPosiEnabled)
        Me.tabPOSI.Location = New System.Drawing.Point(4, 22)
        Me.tabPOSI.Name = "tabPOSI"
        Me.tabPOSI.Size = New System.Drawing.Size(364, 198)
        Me.tabPOSI.TabIndex = 5
        Me.tabPOSI.Text = "POSI Gateway"
        Me.tabPOSI.UseVisualStyleBackColor = True
        '
        'Label10
        '
        Me.Label10.Location = New System.Drawing.Point(24, 96)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(80, 16)
        Me.Label10.TabIndex = 33
        Me.Label10.Text = "Next Poll:"
        '
        'txtPosiNextPoll
        '
        Me.txtPosiNextPoll.Location = New System.Drawing.Point(112, 96)
        Me.txtPosiNextPoll.Name = "txtPosiNextPoll"
        Me.txtPosiNextPoll.Size = New System.Drawing.Size(88, 16)
        Me.txtPosiNextPoll.TabIndex = 34
        '
        'Label12
        '
        Me.Label12.Location = New System.Drawing.Point(24, 72)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(80, 16)
        Me.Label12.TabIndex = 31
        Me.Label12.Text = "Last Poll:"
        '
        'txtPosiLastPoll
        '
        Me.txtPosiLastPoll.Location = New System.Drawing.Point(112, 72)
        Me.txtPosiLastPoll.Name = "txtPosiLastPoll"
        Me.txtPosiLastPoll.Size = New System.Drawing.Size(88, 16)
        Me.txtPosiLastPoll.TabIndex = 32
        '
        'Label14
        '
        Me.Label14.Location = New System.Drawing.Point(24, 48)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(80, 16)
        Me.Label14.TabIndex = 29
        Me.Label14.Text = "Poll Interval:"
        '
        'txtPosiPollInterval
        '
        Me.txtPosiPollInterval.Location = New System.Drawing.Point(112, 48)
        Me.txtPosiPollInterval.Name = "txtPosiPollInterval"
        Me.txtPosiPollInterval.Size = New System.Drawing.Size(88, 16)
        Me.txtPosiPollInterval.TabIndex = 30
        '
        'txtPosiCurrentTime
        '
        Me.txtPosiCurrentTime.Location = New System.Drawing.Point(112, 24)
        Me.txtPosiCurrentTime.Name = "txtPosiCurrentTime"
        Me.txtPosiCurrentTime.Size = New System.Drawing.Size(88, 16)
        Me.txtPosiCurrentTime.TabIndex = 28
        '
        'Label17
        '
        Me.Label17.Location = New System.Drawing.Point(24, 24)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(80, 16)
        Me.Label17.TabIndex = 27
        Me.Label17.Text = "Current Time:"
        '
        'txtPosiStatus
        '
        Me.txtPosiStatus.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtPosiStatus.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtPosiStatus.Location = New System.Drawing.Point(8, 160)
        Me.txtPosiStatus.Multiline = True
        Me.txtPosiStatus.Name = "txtPosiStatus"
        Me.txtPosiStatus.ReadOnly = True
        Me.txtPosiStatus.Size = New System.Drawing.Size(352, 32)
        Me.txtPosiStatus.TabIndex = 26
        '
        'btnPosiImport
        '
        Me.btnPosiImport.Location = New System.Drawing.Point(272, 40)
        Me.btnPosiImport.Name = "btnPosiImport"
        Me.btnPosiImport.Size = New System.Drawing.Size(80, 23)
        Me.btnPosiImport.TabIndex = 25
        Me.btnPosiImport.Text = "Import"
        '
        'CheckBoxPosiEnabled
        '
        Me.CheckBoxPosiEnabled.Location = New System.Drawing.Point(24, 120)
        Me.CheckBoxPosiEnabled.Name = "CheckBoxPosiEnabled"
        Me.CheckBoxPosiEnabled.Size = New System.Drawing.Size(136, 24)
        Me.CheckBoxPosiEnabled.TabIndex = 24
        Me.CheckBoxPosiEnabled.Text = "Enable Gateway"
        '
        'tabTest
        '
        Me.tabTest.Controls.Add(Me.Button8)
        Me.tabTest.Controls.Add(Me.Button4)
        Me.tabTest.Controls.Add(Me.Button3)
        Me.tabTest.Controls.Add(Me.TextBox4)
        Me.tabTest.Controls.Add(Me.Button5)
        Me.tabTest.Controls.Add(Me.DataGrid1)
        Me.tabTest.Location = New System.Drawing.Point(4, 22)
        Me.tabTest.Name = "tabTest"
        Me.tabTest.Size = New System.Drawing.Size(364, 198)
        Me.tabTest.TabIndex = 3
        Me.tabTest.Text = "Test"
        Me.tabTest.UseVisualStyleBackColor = True
        '
        'Button8
        '
        Me.Button8.Location = New System.Drawing.Point(290, 104)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(70, 23)
        Me.Button8.TabIndex = 11
        Me.Button8.Text = "Get Local"
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(290, 8)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(70, 23)
        Me.Button4.TabIndex = 10
        Me.Button4.Text = "Get Data"
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(290, 72)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(70, 23)
        Me.Button3.TabIndex = 9
        Me.Button3.Text = "Clear"
        '
        'TextBox4
        '
        Me.TextBox4.ForeColor = System.Drawing.Color.Purple
        Me.TextBox4.Location = New System.Drawing.Point(290, 136)
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.Size = New System.Drawing.Size(70, 20)
        Me.TextBox4.TabIndex = 8
        '
        'Button5
        '
        Me.Button5.Location = New System.Drawing.Point(290, 40)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(70, 23)
        Me.Button5.TabIndex = 7
        Me.Button5.Text = "Send Data"
        '
        'DataGrid1
        '
        Me.DataGrid1.DataMember = ""
        Me.DataGrid1.HeaderForeColor = System.Drawing.SystemColors.ControlText
        Me.DataGrid1.Location = New System.Drawing.Point(8, 8)
        Me.DataGrid1.Name = "DataGrid1"
        Me.DataGrid1.Size = New System.Drawing.Size(280, 184)
        Me.DataGrid1.TabIndex = 2
        '
        'tabPAR
        '
        Me.tabPAR.Controls.Add(Me.LabelParTLDStatus)
        Me.tabPAR.Controls.Add(Me.ButtonParTLDListener)
        Me.tabPAR.Controls.Add(Me.CheckBoxParTLDServer)
        Me.tabPAR.Controls.Add(Me.CheckBoxParEnabled)
        Me.tabPAR.Controls.Add(Me.btnParImport)
        Me.tabPAR.Controls.Add(Me.btnParGetTld)
        Me.tabPAR.Controls.Add(Me.txtParStatus)
        Me.tabPAR.Controls.Add(Me.Label9)
        Me.tabPAR.Controls.Add(Me.txtParNextPoll)
        Me.tabPAR.Controls.Add(Me.Label8)
        Me.tabPAR.Controls.Add(Me.txtParLastPoll)
        Me.tabPAR.Controls.Add(Me.Label7)
        Me.tabPAR.Controls.Add(Me.txtParPollInterval)
        Me.tabPAR.Controls.Add(Me.txtParCurrentTime)
        Me.tabPAR.Controls.Add(Me.Label2)
        Me.tabPAR.Location = New System.Drawing.Point(4, 22)
        Me.tabPAR.Name = "tabPAR"
        Me.tabPAR.Size = New System.Drawing.Size(364, 198)
        Me.tabPAR.TabIndex = 4
        Me.tabPAR.Text = "PAR Gateway"
        Me.tabPAR.UseVisualStyleBackColor = True
        '
        'LabelParTLDStatus
        '
        Me.LabelParTLDStatus.Image = CType(resources.GetObject("LabelParTLDStatus.Image"), System.Drawing.Image)
        Me.LabelParTLDStatus.Location = New System.Drawing.Point(248, 144)
        Me.LabelParTLDStatus.Name = "LabelParTLDStatus"
        Me.LabelParTLDStatus.Size = New System.Drawing.Size(20, 23)
        Me.LabelParTLDStatus.TabIndex = 26
        '
        'ButtonParTLDListener
        '
        Me.ButtonParTLDListener.Location = New System.Drawing.Point(272, 144)
        Me.ButtonParTLDListener.Name = "ButtonParTLDListener"
        Me.ButtonParTLDListener.Size = New System.Drawing.Size(80, 23)
        Me.ButtonParTLDListener.TabIndex = 25
        Me.ButtonParTLDListener.Text = "Start Listener"
        '
        'CheckBoxParTLDServer
        '
        Me.CheckBoxParTLDServer.Location = New System.Drawing.Point(24, 144)
        Me.CheckBoxParTLDServer.Name = "CheckBoxParTLDServer"
        Me.CheckBoxParTLDServer.Size = New System.Drawing.Size(136, 24)
        Me.CheckBoxParTLDServer.TabIndex = 24
        Me.CheckBoxParTLDServer.Text = "Use TLD Server"
        '
        'CheckBoxParEnabled
        '
        Me.CheckBoxParEnabled.Location = New System.Drawing.Point(24, 120)
        Me.CheckBoxParEnabled.Name = "CheckBoxParEnabled"
        Me.CheckBoxParEnabled.Size = New System.Drawing.Size(136, 24)
        Me.CheckBoxParEnabled.TabIndex = 23
        Me.CheckBoxParEnabled.Text = "Enable Gateway"
        '
        'btnParImport
        '
        Me.btnParImport.Location = New System.Drawing.Point(272, 40)
        Me.btnParImport.Name = "btnParImport"
        Me.btnParImport.Size = New System.Drawing.Size(80, 23)
        Me.btnParImport.TabIndex = 22
        Me.btnParImport.Text = "Import"
        '
        'btnParGetTld
        '
        Me.btnParGetTld.Location = New System.Drawing.Point(272, 8)
        Me.btnParGetTld.Name = "btnParGetTld"
        Me.btnParGetTld.Size = New System.Drawing.Size(80, 23)
        Me.btnParGetTld.TabIndex = 21
        Me.btnParGetTld.Text = "Get TLD"
        '
        'txtParStatus
        '
        Me.txtParStatus.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtParStatus.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtParStatus.Location = New System.Drawing.Point(8, 176)
        Me.txtParStatus.Multiline = True
        Me.txtParStatus.Name = "txtParStatus"
        Me.txtParStatus.ReadOnly = True
        Me.txtParStatus.Size = New System.Drawing.Size(352, 24)
        Me.txtParStatus.TabIndex = 20
        '
        'Label9
        '
        Me.Label9.Location = New System.Drawing.Point(24, 96)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(80, 16)
        Me.Label9.TabIndex = 18
        Me.Label9.Text = "Next Poll:"
        '
        'txtParNextPoll
        '
        Me.txtParNextPoll.Location = New System.Drawing.Point(112, 96)
        Me.txtParNextPoll.Name = "txtParNextPoll"
        Me.txtParNextPoll.Size = New System.Drawing.Size(88, 16)
        Me.txtParNextPoll.TabIndex = 19
        '
        'Label8
        '
        Me.Label8.Location = New System.Drawing.Point(24, 72)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(80, 16)
        Me.Label8.TabIndex = 16
        Me.Label8.Text = "Last Poll:"
        '
        'txtParLastPoll
        '
        Me.txtParLastPoll.Location = New System.Drawing.Point(112, 72)
        Me.txtParLastPoll.Name = "txtParLastPoll"
        Me.txtParLastPoll.Size = New System.Drawing.Size(88, 16)
        Me.txtParLastPoll.TabIndex = 17
        '
        'Label7
        '
        Me.Label7.Location = New System.Drawing.Point(24, 48)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(80, 16)
        Me.Label7.TabIndex = 14
        Me.Label7.Text = "Poll Interval:"
        '
        'txtParPollInterval
        '
        Me.txtParPollInterval.Location = New System.Drawing.Point(112, 48)
        Me.txtParPollInterval.Name = "txtParPollInterval"
        Me.txtParPollInterval.Size = New System.Drawing.Size(88, 16)
        Me.txtParPollInterval.TabIndex = 15
        '
        'txtParCurrentTime
        '
        Me.txtParCurrentTime.Location = New System.Drawing.Point(112, 24)
        Me.txtParCurrentTime.Name = "txtParCurrentTime"
        Me.txtParCurrentTime.Size = New System.Drawing.Size(88, 16)
        Me.txtParCurrentTime.TabIndex = 13
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(24, 24)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(80, 16)
        Me.Label2.TabIndex = 12
        Me.Label2.Text = "Current Time:"
        '
        'tabSetPrice
        '
        Me.tabSetPrice.Controls.Add(Me.TabControl2)
        Me.tabSetPrice.Location = New System.Drawing.Point(4, 22)
        Me.tabSetPrice.Name = "tabSetPrice"
        Me.tabSetPrice.Size = New System.Drawing.Size(364, 198)
        Me.tabSetPrice.TabIndex = 6
        Me.tabSetPrice.Text = "Set Price"
        Me.tabSetPrice.UseVisualStyleBackColor = True
        '
        'TabControl2
        '
        Me.TabControl2.Controls.Add(Me.tabSetPriceDCA)
        Me.TabControl2.Controls.Add(Me.tabSetPricePanasonic)
        Me.TabControl2.Controls.Add(Me.tabSetPricePAR)
        Me.TabControl2.Location = New System.Drawing.Point(0, 8)
        Me.TabControl2.Name = "TabControl2"
        Me.TabControl2.SelectedIndex = 0
        Me.TabControl2.Size = New System.Drawing.Size(365, 190)
        Me.TabControl2.TabIndex = 13
        '
        'tabSetPriceDCA
        '
        Me.tabSetPriceDCA.Controls.Add(Me.Button10)
        Me.tabSetPriceDCA.Controls.Add(Me.Button9)
        Me.tabSetPriceDCA.Controls.Add(Me.Label15)
        Me.tabSetPriceDCA.Controls.Add(Me.TextBoxDCAPriceSetPOSFileConnectionString)
        Me.tabSetPriceDCA.Controls.Add(Me.TextBoxDCAPriceSetImportFileName)
        Me.tabSetPriceDCA.Controls.Add(Me.Label20)
        Me.tabSetPriceDCA.Controls.Add(Me.Label19)
        Me.tabSetPriceDCA.Controls.Add(Me.TextBoxDCAPriceSetImportFileConnectionString)
        Me.tabSetPriceDCA.Controls.Add(Me.TextBoxDCAPriceSetCheckInterval)
        Me.tabSetPriceDCA.Controls.Add(Me.Label16)
        Me.tabSetPriceDCA.Controls.Add(Me.Label18)
        Me.tabSetPriceDCA.Controls.Add(Me.CheckBoxDCAActive)
        Me.tabSetPriceDCA.Location = New System.Drawing.Point(4, 22)
        Me.tabSetPriceDCA.Name = "tabSetPriceDCA"
        Me.tabSetPriceDCA.Size = New System.Drawing.Size(357, 164)
        Me.tabSetPriceDCA.TabIndex = 0
        Me.tabSetPriceDCA.Text = "DCA"
        '
        'Button10
        '
        Me.Button10.Location = New System.Drawing.Point(272, 136)
        Me.Button10.Name = "Button10"
        Me.Button10.Size = New System.Drawing.Size(75, 23)
        Me.Button10.TabIndex = 26
        Me.Button10.Text = "Update"
        '
        'Button9
        '
        Me.Button9.Location = New System.Drawing.Point(321, 54)
        Me.Button9.Name = "Button9"
        Me.Button9.Size = New System.Drawing.Size(24, 20)
        Me.Button9.TabIndex = 25
        Me.Button9.Text = "..."
        '
        'Label15
        '
        Me.Label15.Location = New System.Drawing.Point(8, 120)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(208, 16)
        Me.Label15.TabIndex = 24
        Me.Label15.Text = "Connection String - POS File"
        '
        'TextBoxDCAPriceSetPOSFileConnectionString
        '
        Me.TextBoxDCAPriceSetPOSFileConnectionString.Location = New System.Drawing.Point(8, 136)
        Me.TextBoxDCAPriceSetPOSFileConnectionString.Name = "TextBoxDCAPriceSetPOSFileConnectionString"
        Me.TextBoxDCAPriceSetPOSFileConnectionString.Size = New System.Drawing.Size(256, 20)
        Me.TextBoxDCAPriceSetPOSFileConnectionString.TabIndex = 23
        '
        'TextBoxDCAPriceSetImportFileName
        '
        Me.TextBoxDCAPriceSetImportFileName.Location = New System.Drawing.Point(8, 54)
        Me.TextBoxDCAPriceSetImportFileName.Name = "TextBoxDCAPriceSetImportFileName"
        Me.TextBoxDCAPriceSetImportFileName.Size = New System.Drawing.Size(312, 20)
        Me.TextBoxDCAPriceSetImportFileName.TabIndex = 22
        '
        'Label20
        '
        Me.Label20.Location = New System.Drawing.Point(8, 80)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(176, 16)
        Me.Label20.TabIndex = 21
        Me.Label20.Text = "Connection String - Import File"
        '
        'Label19
        '
        Me.Label19.Location = New System.Drawing.Point(8, 38)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(176, 16)
        Me.Label19.TabIndex = 20
        Me.Label19.Text = "Pricing Update File Location"
        '
        'TextBoxDCAPriceSetImportFileConnectionString
        '
        Me.TextBoxDCAPriceSetImportFileConnectionString.Location = New System.Drawing.Point(8, 96)
        Me.TextBoxDCAPriceSetImportFileConnectionString.Name = "TextBoxDCAPriceSetImportFileConnectionString"
        Me.TextBoxDCAPriceSetImportFileConnectionString.Size = New System.Drawing.Size(256, 20)
        Me.TextBoxDCAPriceSetImportFileConnectionString.TabIndex = 19
        '
        'TextBoxDCAPriceSetCheckInterval
        '
        Me.TextBoxDCAPriceSetCheckInterval.Location = New System.Drawing.Point(225, 10)
        Me.TextBoxDCAPriceSetCheckInterval.Name = "TextBoxDCAPriceSetCheckInterval"
        Me.TextBoxDCAPriceSetCheckInterval.Size = New System.Drawing.Size(38, 20)
        Me.TextBoxDCAPriceSetCheckInterval.TabIndex = 18
        Me.TextBoxDCAPriceSetCheckInterval.Text = "10"
        Me.TextBoxDCAPriceSetCheckInterval.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label16
        '
        Me.Label16.Location = New System.Drawing.Point(265, 12)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(81, 16)
        Me.Label16.TabIndex = 17
        Me.Label16.Text = "minutes."
        '
        'Label18
        '
        Me.Label18.Location = New System.Drawing.Point(97, 12)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(128, 16)
        Me.Label18.TabIndex = 16
        Me.Label18.Text = "Check for updates every"
        '
        'CheckBoxDCAActive
        '
        Me.CheckBoxDCAActive.Location = New System.Drawing.Point(8, 8)
        Me.CheckBoxDCAActive.Name = "CheckBoxDCAActive"
        Me.CheckBoxDCAActive.Size = New System.Drawing.Size(56, 24)
        Me.CheckBoxDCAActive.TabIndex = 12
        Me.CheckBoxDCAActive.Text = "Active"
        '
        'tabSetPricePanasonic
        '
        Me.tabSetPricePanasonic.Controls.Add(Me.CheckBox3)
        Me.tabSetPricePanasonic.Location = New System.Drawing.Point(4, 22)
        Me.tabSetPricePanasonic.Name = "tabSetPricePanasonic"
        Me.tabSetPricePanasonic.Size = New System.Drawing.Size(357, 164)
        Me.tabSetPricePanasonic.TabIndex = 1
        Me.tabSetPricePanasonic.Text = "Panasonic"
        '
        'CheckBox3
        '
        Me.CheckBox3.Location = New System.Drawing.Point(8, 8)
        Me.CheckBox3.Name = "CheckBox3"
        Me.CheckBox3.Size = New System.Drawing.Size(104, 24)
        Me.CheckBox3.TabIndex = 13
        Me.CheckBox3.Text = "Active"
        '
        'tabSetPricePAR
        '
        Me.tabSetPricePAR.Controls.Add(Me.CheckBox4)
        Me.tabSetPricePAR.Location = New System.Drawing.Point(4, 22)
        Me.tabSetPricePAR.Name = "tabSetPricePAR"
        Me.tabSetPricePAR.Size = New System.Drawing.Size(357, 164)
        Me.tabSetPricePAR.TabIndex = 2
        Me.tabSetPricePAR.Text = "PAR"
        '
        'CheckBox4
        '
        Me.CheckBox4.Location = New System.Drawing.Point(8, 8)
        Me.CheckBox4.Name = "CheckBox4"
        Me.CheckBox4.Size = New System.Drawing.Size(104, 24)
        Me.CheckBox4.TabIndex = 13
        Me.CheckBox4.Text = "Active"
        '
        'tabOfflineCashManager
        '
        Me.tabOfflineCashManager.Controls.Add(Me.Label45)
        Me.tabOfflineCashManager.Controls.Add(Me.cmdRefreshSettings)
        Me.tabOfflineCashManager.Location = New System.Drawing.Point(4, 22)
        Me.tabOfflineCashManager.Name = "tabOfflineCashManager"
        Me.tabOfflineCashManager.Padding = New System.Windows.Forms.Padding(3)
        Me.tabOfflineCashManager.Size = New System.Drawing.Size(364, 198)
        Me.tabOfflineCashManager.TabIndex = 14
        Me.tabOfflineCashManager.Text = "Offline Cash Manager"
        Me.tabOfflineCashManager.UseVisualStyleBackColor = True
        '
        'Label45
        '
        Me.Label45.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label45.Location = New System.Drawing.Point(15, 8)
        Me.Label45.Name = "Label45"
        Me.Label45.Size = New System.Drawing.Size(224, 16)
        Me.Label45.TabIndex = 12
        Me.Label45.Text = "Offline Cash Manager Options"
        '
        'cmdRefreshSettings
        '
        Me.cmdRefreshSettings.Location = New System.Drawing.Point(18, 41)
        Me.cmdRefreshSettings.Name = "cmdRefreshSettings"
        Me.cmdRefreshSettings.Size = New System.Drawing.Size(128, 23)
        Me.cmdRefreshSettings.TabIndex = 0
        Me.cmdRefreshSettings.Text = "Refresh Settings"
        Me.cmdRefreshSettings.UseVisualStyleBackColor = True
        '
        'tabZKFingerprint
        '
        Me.tabZKFingerprint.Controls.Add(Me.textZKAuthServerStatus)
        Me.tabZKFingerprint.Controls.Add(Me.LabelZKAuthServerStatus)
        Me.tabZKFingerprint.Controls.Add(Me.ButtonZKUpdateFirmware)
        Me.tabZKFingerprint.Controls.Add(Me.txtZKDevicePort)
        Me.tabZKFingerprint.Controls.Add(Me.txtZKDeviceIP)
        Me.tabZKFingerprint.Controls.Add(Me.lbZKDevicePort)
        Me.tabZKFingerprint.Controls.Add(Me.lbZKDeviceIP)
        Me.tabZKFingerprint.Controls.Add(Me.CheckBoxZKEnabled)
        Me.tabZKFingerprint.Controls.Add(Me.ButtonZKFingerprintImport)
        Me.tabZKFingerprint.Controls.Add(Me.lbZKNextPoll)
        Me.tabZKFingerprint.Controls.Add(Me.txtZKNextPoll)
        Me.tabZKFingerprint.Controls.Add(Me.lbZKLastPoll)
        Me.tabZKFingerprint.Controls.Add(Me.txtZKLastPoll)
        Me.tabZKFingerprint.Controls.Add(Me.lbZKPollInterval)
        Me.tabZKFingerprint.Controls.Add(Me.txtZKPollInterval)
        Me.tabZKFingerprint.Controls.Add(Me.txtZKCurrentTime)
        Me.tabZKFingerprint.Controls.Add(Me.lbZKCurrentTime)
        Me.tabZKFingerprint.Location = New System.Drawing.Point(4, 22)
        Me.tabZKFingerprint.Name = "tabZKFingerprint"
        Me.tabZKFingerprint.Size = New System.Drawing.Size(364, 198)
        Me.tabZKFingerprint.TabIndex = 15
        Me.tabZKFingerprint.Text = "ZK Fingerprint"
        Me.tabZKFingerprint.UseVisualStyleBackColor = True
        '
        'textZKAuthServerStatus
        '
        Me.textZKAuthServerStatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.textZKAuthServerStatus.Location = New System.Drawing.Point(182, 124)
        Me.textZKAuthServerStatus.Name = "textZKAuthServerStatus"
        Me.textZKAuthServerStatus.ReadOnly = True
        Me.textZKAuthServerStatus.Size = New System.Drawing.Size(160, 20)
        Me.textZKAuthServerStatus.TabIndex = 94
        Me.textZKAuthServerStatus.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'LabelZKAuthServerStatus
        '
        Me.LabelZKAuthServerStatus.Location = New System.Drawing.Point(179, 105)
        Me.LabelZKAuthServerStatus.Name = "LabelZKAuthServerStatus"
        Me.LabelZKAuthServerStatus.Size = New System.Drawing.Size(163, 16)
        Me.LabelZKAuthServerStatus.TabIndex = 93
        Me.LabelZKAuthServerStatus.Text = "Authentication Server Status"
        '
        'ButtonZKUpdateFirmware
        '
        Me.ButtonZKUpdateFirmware.Location = New System.Drawing.Point(182, 150)
        Me.ButtonZKUpdateFirmware.Name = "ButtonZKUpdateFirmware"
        Me.ButtonZKUpdateFirmware.Size = New System.Drawing.Size(160, 23)
        Me.ButtonZKUpdateFirmware.TabIndex = 92
        Me.ButtonZKUpdateFirmware.Text = "Update Firmware"
        '
        'txtZKDevicePort
        '
        Me.txtZKDevicePort.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtZKDevicePort.Location = New System.Drawing.Point(251, 72)
        Me.txtZKDevicePort.Name = "txtZKDevicePort"
        Me.txtZKDevicePort.ReadOnly = True
        Me.txtZKDevicePort.Size = New System.Drawing.Size(91, 20)
        Me.txtZKDevicePort.TabIndex = 91
        Me.txtZKDevicePort.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtZKDeviceIP
        '
        Me.txtZKDeviceIP.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtZKDeviceIP.Location = New System.Drawing.Point(251, 46)
        Me.txtZKDeviceIP.Name = "txtZKDeviceIP"
        Me.txtZKDeviceIP.ReadOnly = True
        Me.txtZKDeviceIP.Size = New System.Drawing.Size(91, 20)
        Me.txtZKDeviceIP.TabIndex = 90
        Me.txtZKDeviceIP.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lbZKDevicePort
        '
        Me.lbZKDevicePort.Location = New System.Drawing.Point(179, 76)
        Me.lbZKDevicePort.Name = "lbZKDevicePort"
        Me.lbZKDevicePort.Size = New System.Drawing.Size(66, 16)
        Me.lbZKDevicePort.TabIndex = 89
        Me.lbZKDevicePort.Text = "Device Port:"
        '
        'lbZKDeviceIP
        '
        Me.lbZKDeviceIP.Location = New System.Drawing.Point(179, 50)
        Me.lbZKDeviceIP.Name = "lbZKDeviceIP"
        Me.lbZKDeviceIP.Size = New System.Drawing.Size(66, 16)
        Me.lbZKDeviceIP.TabIndex = 87
        Me.lbZKDeviceIP.Text = "Device IP:"
        '
        'CheckBoxZKEnabled
        '
        Me.CheckBoxZKEnabled.Location = New System.Drawing.Point(27, 122)
        Me.CheckBoxZKEnabled.Name = "CheckBoxZKEnabled"
        Me.CheckBoxZKEnabled.Size = New System.Drawing.Size(108, 24)
        Me.CheckBoxZKEnabled.TabIndex = 86
        Me.CheckBoxZKEnabled.Text = "Enable Gateway"
        '
        'ButtonZKFingerprintImport
        '
        Me.ButtonZKFingerprintImport.Location = New System.Drawing.Point(251, 17)
        Me.ButtonZKFingerprintImport.Name = "ButtonZKFingerprintImport"
        Me.ButtonZKFingerprintImport.Size = New System.Drawing.Size(91, 23)
        Me.ButtonZKFingerprintImport.TabIndex = 85
        Me.ButtonZKFingerprintImport.Text = "Process"
        '
        'lbZKNextPoll
        '
        Me.lbZKNextPoll.Location = New System.Drawing.Point(24, 96)
        Me.lbZKNextPoll.Name = "lbZKNextPoll"
        Me.lbZKNextPoll.Size = New System.Drawing.Size(80, 16)
        Me.lbZKNextPoll.TabIndex = 83
        Me.lbZKNextPoll.Text = "Next Poll:"
        '
        'txtZKNextPoll
        '
        Me.txtZKNextPoll.Location = New System.Drawing.Point(112, 96)
        Me.txtZKNextPoll.Name = "txtZKNextPoll"
        Me.txtZKNextPoll.Size = New System.Drawing.Size(66, 16)
        Me.txtZKNextPoll.TabIndex = 84
        '
        'lbZKLastPoll
        '
        Me.lbZKLastPoll.Location = New System.Drawing.Point(24, 72)
        Me.lbZKLastPoll.Name = "lbZKLastPoll"
        Me.lbZKLastPoll.Size = New System.Drawing.Size(80, 16)
        Me.lbZKLastPoll.TabIndex = 81
        Me.lbZKLastPoll.Text = "Last Poll:"
        '
        'txtZKLastPoll
        '
        Me.txtZKLastPoll.Location = New System.Drawing.Point(112, 72)
        Me.txtZKLastPoll.Name = "txtZKLastPoll"
        Me.txtZKLastPoll.Size = New System.Drawing.Size(66, 16)
        Me.txtZKLastPoll.TabIndex = 82
        '
        'lbZKPollInterval
        '
        Me.lbZKPollInterval.Location = New System.Drawing.Point(24, 48)
        Me.lbZKPollInterval.Name = "lbZKPollInterval"
        Me.lbZKPollInterval.Size = New System.Drawing.Size(80, 16)
        Me.lbZKPollInterval.TabIndex = 79
        Me.lbZKPollInterval.Text = "Poll Interval:"
        '
        'txtZKPollInterval
        '
        Me.txtZKPollInterval.Location = New System.Drawing.Point(112, 47)
        Me.txtZKPollInterval.Name = "txtZKPollInterval"
        Me.txtZKPollInterval.Size = New System.Drawing.Size(66, 16)
        Me.txtZKPollInterval.TabIndex = 80
        '
        'txtZKCurrentTime
        '
        Me.txtZKCurrentTime.Location = New System.Drawing.Point(112, 24)
        Me.txtZKCurrentTime.Name = "txtZKCurrentTime"
        Me.txtZKCurrentTime.Size = New System.Drawing.Size(66, 16)
        Me.txtZKCurrentTime.TabIndex = 78
        '
        'lbZKCurrentTime
        '
        Me.lbZKCurrentTime.Location = New System.Drawing.Point(24, 24)
        Me.lbZKCurrentTime.Name = "lbZKCurrentTime"
        Me.lbZKCurrentTime.Size = New System.Drawing.Size(80, 16)
        Me.lbZKCurrentTime.TabIndex = 77
        Me.lbZKCurrentTime.Text = "Current Time:"
        '
        'tabICG
        '
        Me.tabICG.Controls.Add(Me.CheckBoxICGEnabled)
        Me.tabICG.Controls.Add(Me.lbICGNextPoll)
        Me.tabICG.Controls.Add(Me.txtICGNextPoll)
        Me.tabICG.Controls.Add(Me.lbICGLastPoll)
        Me.tabICG.Controls.Add(Me.txtICGLastPoll)
        Me.tabICG.Controls.Add(Me.lbICGPollInterval)
        Me.tabICG.Controls.Add(Me.txtICGPollInterval)
        Me.tabICG.Controls.Add(Me.txtICGCurrentTime)
        Me.tabICG.Controls.Add(Me.lbICGCurrentTime)
        Me.tabICG.Controls.Add(Me.ButtonICGImport)
        Me.tabICG.Location = New System.Drawing.Point(4, 22)
        Me.tabICG.Name = "tabICG"
        Me.tabICG.Size = New System.Drawing.Size(364, 198)
        Me.tabICG.TabIndex = 16
        Me.tabICG.Text = "ICG"
        Me.tabICG.UseVisualStyleBackColor = True
        '
        'CheckBoxICGEnabled
        '
        Me.CheckBoxICGEnabled.Location = New System.Drawing.Point(18, 124)
        Me.CheckBoxICGEnabled.Name = "CheckBoxICGEnabled"
        Me.CheckBoxICGEnabled.Size = New System.Drawing.Size(136, 24)
        Me.CheckBoxICGEnabled.TabIndex = 54
        Me.CheckBoxICGEnabled.Text = "Enable Gateway"
        '
        'lbICGNextPoll
        '
        Me.lbICGNextPoll.Location = New System.Drawing.Point(18, 100)
        Me.lbICGNextPoll.Name = "lbICGNextPoll"
        Me.lbICGNextPoll.Size = New System.Drawing.Size(80, 16)
        Me.lbICGNextPoll.TabIndex = 52
        Me.lbICGNextPoll.Text = "Next Poll:"
        '
        'txtICGNextPoll
        '
        Me.txtICGNextPoll.Location = New System.Drawing.Point(106, 100)
        Me.txtICGNextPoll.Name = "txtICGNextPoll"
        Me.txtICGNextPoll.Size = New System.Drawing.Size(88, 16)
        Me.txtICGNextPoll.TabIndex = 53
        '
        'lbICGLastPoll
        '
        Me.lbICGLastPoll.Location = New System.Drawing.Point(18, 76)
        Me.lbICGLastPoll.Name = "lbICGLastPoll"
        Me.lbICGLastPoll.Size = New System.Drawing.Size(80, 16)
        Me.lbICGLastPoll.TabIndex = 50
        Me.lbICGLastPoll.Text = "Last Poll:"
        '
        'txtICGLastPoll
        '
        Me.txtICGLastPoll.Location = New System.Drawing.Point(106, 76)
        Me.txtICGLastPoll.Name = "txtICGLastPoll"
        Me.txtICGLastPoll.Size = New System.Drawing.Size(88, 16)
        Me.txtICGLastPoll.TabIndex = 51
        '
        'lbICGPollInterval
        '
        Me.lbICGPollInterval.Location = New System.Drawing.Point(18, 52)
        Me.lbICGPollInterval.Name = "lbICGPollInterval"
        Me.lbICGPollInterval.Size = New System.Drawing.Size(80, 16)
        Me.lbICGPollInterval.TabIndex = 48
        Me.lbICGPollInterval.Text = "Poll Interval:"
        '
        'txtICGPollInterval
        '
        Me.txtICGPollInterval.Location = New System.Drawing.Point(106, 52)
        Me.txtICGPollInterval.Name = "txtICGPollInterval"
        Me.txtICGPollInterval.Size = New System.Drawing.Size(88, 16)
        Me.txtICGPollInterval.TabIndex = 49
        '
        'txtICGCurrentTime
        '
        Me.txtICGCurrentTime.Location = New System.Drawing.Point(106, 28)
        Me.txtICGCurrentTime.Name = "txtICGCurrentTime"
        Me.txtICGCurrentTime.Size = New System.Drawing.Size(88, 16)
        Me.txtICGCurrentTime.TabIndex = 47
        '
        'lbICGCurrentTime
        '
        Me.lbICGCurrentTime.Location = New System.Drawing.Point(18, 28)
        Me.lbICGCurrentTime.Name = "lbICGCurrentTime"
        Me.lbICGCurrentTime.Size = New System.Drawing.Size(80, 16)
        Me.lbICGCurrentTime.TabIndex = 46
        Me.lbICGCurrentTime.Text = "Current Time:"
        '
        'ButtonICGImport
        '
        Me.ButtonICGImport.Location = New System.Drawing.Point(266, 12)
        Me.ButtonICGImport.Name = "ButtonICGImport"
        Me.ButtonICGImport.Size = New System.Drawing.Size(75, 23)
        Me.ButtonICGImport.TabIndex = 45
        Me.ButtonICGImport.Text = "Process"
        '
        'tabRMS
        '
        Me.tabRMS.Controls.Add(Me.CheckBoxRMSEnabled)
        Me.tabRMS.Controls.Add(Me.lbRMSNextPoll)
        Me.tabRMS.Controls.Add(Me.txtRMSNextPoll)
        Me.tabRMS.Controls.Add(Me.lbRMSLastPoll)
        Me.tabRMS.Controls.Add(Me.txtRMSLastPoll)
        Me.tabRMS.Controls.Add(Me.lbRMSPollInterval)
        Me.tabRMS.Controls.Add(Me.txtRMSPollInterval)
        Me.tabRMS.Controls.Add(Me.txtRMSCurrentTime)
        Me.tabRMS.Controls.Add(Me.lbRMSCurrentTime)
        Me.tabRMS.Controls.Add(Me.ButtonRMSImport)
        Me.tabRMS.Location = New System.Drawing.Point(4, 22)
        Me.tabRMS.Name = "tabRMS"
        Me.tabRMS.Size = New System.Drawing.Size(364, 198)
        Me.tabRMS.TabIndex = 17
        Me.tabRMS.Text = "Microsoft RMS"
        Me.tabRMS.UseVisualStyleBackColor = True
        '
        'CheckBoxRMSEnabled
        '
        Me.CheckBoxRMSEnabled.Location = New System.Drawing.Point(19, 124)
        Me.CheckBoxRMSEnabled.Name = "CheckBoxRMSEnabled"
        Me.CheckBoxRMSEnabled.Size = New System.Drawing.Size(136, 24)
        Me.CheckBoxRMSEnabled.TabIndex = 54
        Me.CheckBoxRMSEnabled.Text = "Enable Gateway"
        '
        'lbRMSNextPoll
        '
        Me.lbRMSNextPoll.Location = New System.Drawing.Point(19, 100)
        Me.lbRMSNextPoll.Name = "lbRMSNextPoll"
        Me.lbRMSNextPoll.Size = New System.Drawing.Size(80, 16)
        Me.lbRMSNextPoll.TabIndex = 52
        Me.lbRMSNextPoll.Text = "Next Poll:"
        '
        'txtRMSNextPoll
        '
        Me.txtRMSNextPoll.Location = New System.Drawing.Point(107, 100)
        Me.txtRMSNextPoll.Name = "txtRMSNextPoll"
        Me.txtRMSNextPoll.Size = New System.Drawing.Size(88, 16)
        Me.txtRMSNextPoll.TabIndex = 53
        '
        'lbRMSLastPoll
        '
        Me.lbRMSLastPoll.Location = New System.Drawing.Point(19, 76)
        Me.lbRMSLastPoll.Name = "lbRMSLastPoll"
        Me.lbRMSLastPoll.Size = New System.Drawing.Size(80, 16)
        Me.lbRMSLastPoll.TabIndex = 50
        Me.lbRMSLastPoll.Text = "Last Poll:"
        '
        'txtRMSLastPoll
        '
        Me.txtRMSLastPoll.Location = New System.Drawing.Point(107, 76)
        Me.txtRMSLastPoll.Name = "txtRMSLastPoll"
        Me.txtRMSLastPoll.Size = New System.Drawing.Size(88, 16)
        Me.txtRMSLastPoll.TabIndex = 51
        '
        'lbRMSPollInterval
        '
        Me.lbRMSPollInterval.Location = New System.Drawing.Point(19, 52)
        Me.lbRMSPollInterval.Name = "lbRMSPollInterval"
        Me.lbRMSPollInterval.Size = New System.Drawing.Size(80, 16)
        Me.lbRMSPollInterval.TabIndex = 48
        Me.lbRMSPollInterval.Text = "Poll Interval:"
        '
        'txtRMSPollInterval
        '
        Me.txtRMSPollInterval.Location = New System.Drawing.Point(107, 52)
        Me.txtRMSPollInterval.Name = "txtRMSPollInterval"
        Me.txtRMSPollInterval.Size = New System.Drawing.Size(88, 16)
        Me.txtRMSPollInterval.TabIndex = 49
        '
        'txtRMSCurrentTime
        '
        Me.txtRMSCurrentTime.Location = New System.Drawing.Point(107, 28)
        Me.txtRMSCurrentTime.Name = "txtRMSCurrentTime"
        Me.txtRMSCurrentTime.Size = New System.Drawing.Size(88, 16)
        Me.txtRMSCurrentTime.TabIndex = 47
        '
        'lbRMSCurrentTime
        '
        Me.lbRMSCurrentTime.Location = New System.Drawing.Point(19, 28)
        Me.lbRMSCurrentTime.Name = "lbRMSCurrentTime"
        Me.lbRMSCurrentTime.Size = New System.Drawing.Size(80, 16)
        Me.lbRMSCurrentTime.TabIndex = 46
        Me.lbRMSCurrentTime.Text = "Current Time:"
        '
        'ButtonRMSImport
        '
        Me.ButtonRMSImport.Location = New System.Drawing.Point(267, 12)
        Me.ButtonRMSImport.Name = "ButtonRMSImport"
        Me.ButtonRMSImport.Size = New System.Drawing.Size(75, 23)
        Me.ButtonRMSImport.TabIndex = 45
        Me.ButtonRMSImport.Text = "Process"
        '
        'tabNewPOS
        '
        Me.tabNewPOS.Controls.Add(Me.CheckBoxNewPOSEnabled)
        Me.tabNewPOS.Controls.Add(Me.lbNewPOSNextPoll)
        Me.tabNewPOS.Controls.Add(Me.txtNewPOSNextPoll)
        Me.tabNewPOS.Controls.Add(Me.lbNewPOSLastPoll)
        Me.tabNewPOS.Controls.Add(Me.txtNewPOSLastPoll)
        Me.tabNewPOS.Controls.Add(Me.lbNewPOSPollInterval)
        Me.tabNewPOS.Controls.Add(Me.txtNewPOSPollInterval)
        Me.tabNewPOS.Controls.Add(Me.txtNewPOSCurrentTime)
        Me.tabNewPOS.Controls.Add(Me.lbNewPOSCurrentTime)
        Me.tabNewPOS.Controls.Add(Me.ButtonNewPOSImport)
        Me.tabNewPOS.Location = New System.Drawing.Point(4, 22)
        Me.tabNewPOS.Name = "tabNewPOS"
        Me.tabNewPOS.Size = New System.Drawing.Size(364, 198)
        Me.tabNewPOS.TabIndex = 18
        Me.tabNewPOS.Text = "New POS"
        Me.tabNewPOS.UseVisualStyleBackColor = True
        '
        'CheckBoxNewPOSEnabled
        '
        Me.CheckBoxNewPOSEnabled.Location = New System.Drawing.Point(15, 124)
        Me.CheckBoxNewPOSEnabled.Name = "CheckBoxNewPOSEnabled"
        Me.CheckBoxNewPOSEnabled.Size = New System.Drawing.Size(136, 24)
        Me.CheckBoxNewPOSEnabled.TabIndex = 64
        Me.CheckBoxNewPOSEnabled.Text = "Enable Gateway"
        '
        'lbNewPOSNextPoll
        '
        Me.lbNewPOSNextPoll.Location = New System.Drawing.Point(15, 100)
        Me.lbNewPOSNextPoll.Name = "lbNewPOSNextPoll"
        Me.lbNewPOSNextPoll.Size = New System.Drawing.Size(80, 16)
        Me.lbNewPOSNextPoll.TabIndex = 62
        Me.lbNewPOSNextPoll.Text = "Next Poll:"
        '
        'txtNewPOSNextPoll
        '
        Me.txtNewPOSNextPoll.Location = New System.Drawing.Point(103, 100)
        Me.txtNewPOSNextPoll.Name = "txtNewPOSNextPoll"
        Me.txtNewPOSNextPoll.Size = New System.Drawing.Size(88, 16)
        Me.txtNewPOSNextPoll.TabIndex = 63
        '
        'lbNewPOSLastPoll
        '
        Me.lbNewPOSLastPoll.Location = New System.Drawing.Point(15, 76)
        Me.lbNewPOSLastPoll.Name = "lbNewPOSLastPoll"
        Me.lbNewPOSLastPoll.Size = New System.Drawing.Size(80, 16)
        Me.lbNewPOSLastPoll.TabIndex = 60
        Me.lbNewPOSLastPoll.Text = "Last Poll:"
        '
        'txtNewPOSLastPoll
        '
        Me.txtNewPOSLastPoll.Location = New System.Drawing.Point(103, 76)
        Me.txtNewPOSLastPoll.Name = "txtNewPOSLastPoll"
        Me.txtNewPOSLastPoll.Size = New System.Drawing.Size(88, 16)
        Me.txtNewPOSLastPoll.TabIndex = 61
        '
        'lbNewPOSPollInterval
        '
        Me.lbNewPOSPollInterval.Location = New System.Drawing.Point(15, 52)
        Me.lbNewPOSPollInterval.Name = "lbNewPOSPollInterval"
        Me.lbNewPOSPollInterval.Size = New System.Drawing.Size(80, 16)
        Me.lbNewPOSPollInterval.TabIndex = 58
        Me.lbNewPOSPollInterval.Text = "Poll Interval:"
        '
        'txtNewPOSPollInterval
        '
        Me.txtNewPOSPollInterval.Location = New System.Drawing.Point(103, 52)
        Me.txtNewPOSPollInterval.Name = "txtNewPOSPollInterval"
        Me.txtNewPOSPollInterval.Size = New System.Drawing.Size(88, 16)
        Me.txtNewPOSPollInterval.TabIndex = 59
        '
        'txtNewPOSCurrentTime
        '
        Me.txtNewPOSCurrentTime.Location = New System.Drawing.Point(103, 28)
        Me.txtNewPOSCurrentTime.Name = "txtNewPOSCurrentTime"
        Me.txtNewPOSCurrentTime.Size = New System.Drawing.Size(88, 16)
        Me.txtNewPOSCurrentTime.TabIndex = 57
        '
        'lbNewPOSCurrentTime
        '
        Me.lbNewPOSCurrentTime.Location = New System.Drawing.Point(15, 28)
        Me.lbNewPOSCurrentTime.Name = "lbNewPOSCurrentTime"
        Me.lbNewPOSCurrentTime.Size = New System.Drawing.Size(80, 16)
        Me.lbNewPOSCurrentTime.TabIndex = 56
        Me.lbNewPOSCurrentTime.Text = "Current Time:"
        '
        'ButtonNewPOSImport
        '
        Me.ButtonNewPOSImport.Location = New System.Drawing.Point(263, 12)
        Me.ButtonNewPOSImport.Name = "ButtonNewPOSImport"
        Me.ButtonNewPOSImport.Size = New System.Drawing.Size(75, 23)
        Me.ButtonNewPOSImport.TabIndex = 55
        Me.ButtonNewPOSImport.Text = "Process"
        '
        'tabProxy
        '
        Me.tabProxy.Controls.Add(Me.btnSaveProxySettings)
        Me.tabProxy.Controls.Add(Me.grpProxy)
        Me.tabProxy.Controls.Add(Me.chkUseProxy)
        Me.tabProxy.Location = New System.Drawing.Point(4, 22)
        Me.tabProxy.Name = "tabProxy"
        Me.tabProxy.Padding = New System.Windows.Forms.Padding(3)
        Me.tabProxy.Size = New System.Drawing.Size(364, 198)
        Me.tabProxy.TabIndex = 18
        Me.tabProxy.Text = "Proxy Server"
        Me.tabProxy.UseVisualStyleBackColor = True
        '
        'btnSaveProxySettings
        '
        Me.btnSaveProxySettings.Location = New System.Drawing.Point(283, 3)
        Me.btnSaveProxySettings.Name = "btnSaveProxySettings"
        Me.btnSaveProxySettings.Size = New System.Drawing.Size(75, 20)
        Me.btnSaveProxySettings.TabIndex = 9
        Me.btnSaveProxySettings.Text = "Save"
        Me.btnSaveProxySettings.UseVisualStyleBackColor = True
        '
        'grpProxy
        '
        Me.grpProxy.Controls.Add(Me.cboProxyAuth)
        Me.grpProxy.Controls.Add(Me.txtCustomProxy)
        Me.grpProxy.Controls.Add(Me.txtSystemProxy)
        Me.grpProxy.Controls.Add(Me.grpProxyAuth)
        Me.grpProxy.Controls.Add(Me.radCustomProxy)
        Me.grpProxy.Controls.Add(Me.radUseIEProxy)
        Me.grpProxy.Controls.Add(Me.lblProxyServer)
        Me.grpProxy.Enabled = False
        Me.grpProxy.Location = New System.Drawing.Point(8, 22)
        Me.grpProxy.Name = "grpProxy"
        Me.grpProxy.Size = New System.Drawing.Size(353, 173)
        Me.grpProxy.TabIndex = 2
        Me.grpProxy.TabStop = False
        '
        'cboProxyAuth
        '
        Me.cboProxyAuth.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboProxyAuth.FormattingEnabled = True
        Me.cboProxyAuth.Location = New System.Drawing.Point(96, 54)
        Me.cboProxyAuth.Name = "cboProxyAuth"
        Me.cboProxyAuth.Size = New System.Drawing.Size(240, 21)
        Me.cboProxyAuth.TabIndex = 5
        '
        'txtCustomProxy
        '
        Me.txtCustomProxy.Enabled = False
        Me.txtCustomProxy.Location = New System.Drawing.Point(96, 31)
        Me.txtCustomProxy.Name = "txtCustomProxy"
        Me.txtCustomProxy.Size = New System.Drawing.Size(240, 20)
        Me.txtCustomProxy.TabIndex = 4
        '
        'txtSystemProxy
        '
        Me.txtSystemProxy.Location = New System.Drawing.Point(96, 8)
        Me.txtSystemProxy.Name = "txtSystemProxy"
        Me.txtSystemProxy.ReadOnly = True
        Me.txtSystemProxy.Size = New System.Drawing.Size(240, 20)
        Me.txtSystemProxy.TabIndex = 8
        Me.txtSystemProxy.TabStop = False
        '
        'grpProxyAuth
        '
        Me.grpProxyAuth.Controls.Add(Me.txtProxyPassword)
        Me.grpProxyAuth.Controls.Add(Me.txtProxyDomain)
        Me.grpProxyAuth.Controls.Add(Me.txtProxyUserName)
        Me.grpProxyAuth.Controls.Add(Me.Label51)
        Me.grpProxyAuth.Controls.Add(Me.Label50)
        Me.grpProxyAuth.Controls.Add(Me.Label48)
        Me.grpProxyAuth.Enabled = False
        Me.grpProxyAuth.Location = New System.Drawing.Point(6, 75)
        Me.grpProxyAuth.Name = "grpProxyAuth"
        Me.grpProxyAuth.Size = New System.Drawing.Size(341, 89)
        Me.grpProxyAuth.TabIndex = 7
        Me.grpProxyAuth.TabStop = False
        '
        'txtProxyPassword
        '
        Me.txtProxyPassword.Location = New System.Drawing.Point(90, 57)
        Me.txtProxyPassword.Name = "txtProxyPassword"
        Me.txtProxyPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtProxyPassword.Size = New System.Drawing.Size(240, 20)
        Me.txtProxyPassword.TabIndex = 9
        '
        'txtProxyDomain
        '
        Me.txtProxyDomain.Location = New System.Drawing.Point(90, 35)
        Me.txtProxyDomain.Name = "txtProxyDomain"
        Me.txtProxyDomain.Size = New System.Drawing.Size(240, 20)
        Me.txtProxyDomain.TabIndex = 7
        '
        'txtProxyUserName
        '
        Me.txtProxyUserName.Location = New System.Drawing.Point(90, 13)
        Me.txtProxyUserName.Name = "txtProxyUserName"
        Me.txtProxyUserName.Size = New System.Drawing.Size(240, 20)
        Me.txtProxyUserName.TabIndex = 6
        '
        'Label51
        '
        Me.Label51.AutoSize = True
        Me.Label51.Location = New System.Drawing.Point(12, 38)
        Me.Label51.Name = "Label51"
        Me.Label51.Size = New System.Drawing.Size(43, 13)
        Me.Label51.TabIndex = 8
        Me.Label51.Text = "Domain"
        '
        'Label50
        '
        Me.Label50.AutoSize = True
        Me.Label50.Location = New System.Drawing.Point(13, 60)
        Me.Label50.Name = "Label50"
        Me.Label50.Size = New System.Drawing.Size(53, 13)
        Me.Label50.TabIndex = 7
        Me.Label50.Text = "Password"
        '
        'Label48
        '
        Me.Label48.AutoSize = True
        Me.Label48.Location = New System.Drawing.Point(13, 16)
        Me.Label48.Name = "Label48"
        Me.Label48.Size = New System.Drawing.Size(58, 13)
        Me.Label48.TabIndex = 6
        Me.Label48.Text = "User name"
        '
        'radCustomProxy
        '
        Me.radCustomProxy.AutoSize = True
        Me.radCustomProxy.Location = New System.Drawing.Point(6, 32)
        Me.radCustomProxy.Name = "radCustomProxy"
        Me.radCustomProxy.Size = New System.Drawing.Size(88, 17)
        Me.radCustomProxy.TabIndex = 3
        Me.radCustomProxy.TabStop = True
        Me.radCustomProxy.Text = "Custom proxy"
        Me.radCustomProxy.UseVisualStyleBackColor = True
        '
        'radUseIEProxy
        '
        Me.radUseIEProxy.AutoSize = True
        Me.radUseIEProxy.Location = New System.Drawing.Point(6, 9)
        Me.radUseIEProxy.Name = "radUseIEProxy"
        Me.radUseIEProxy.Size = New System.Drawing.Size(87, 17)
        Me.radUseIEProxy.TabIndex = 2
        Me.radUseIEProxy.TabStop = True
        Me.radUseIEProxy.Text = "System proxy"
        Me.radUseIEProxy.UseVisualStyleBackColor = True
        '
        'lblProxyServer
        '
        Me.lblProxyServer.AutoSize = True
        Me.lblProxyServer.Location = New System.Drawing.Point(18, 57)
        Me.lblProxyServer.Name = "lblProxyServer"
        Me.lblProxyServer.Size = New System.Drawing.Size(75, 13)
        Me.lblProxyServer.TabIndex = 1
        Me.lblProxyServer.Text = "Authentication"
        '
        'chkUseProxy
        '
        Me.chkUseProxy.AutoSize = True
        Me.chkUseProxy.Location = New System.Drawing.Point(8, 7)
        Me.chkUseProxy.Name = "chkUseProxy"
        Me.chkUseProxy.Size = New System.Drawing.Size(108, 17)
        Me.chkUseProxy.TabIndex = 1
        Me.chkUseProxy.Text = "Use Proxy Server"
        Me.chkUseProxy.UseVisualStyleBackColor = True
        '
        'tabiPOS
        '
        Me.tabiPOS.Controls.Add(Me.grpiPOSInformation)
        Me.tabiPOS.Controls.Add(Me.grpiPOSProcess)
        Me.tabiPOS.Controls.Add(Me.grpiPOSUpdates)
        Me.tabiPOS.Location = New System.Drawing.Point(4, 22)
        Me.tabiPOS.Name = "tabiPOS"
        Me.tabiPOS.Size = New System.Drawing.Size(364, 198)
        Me.tabiPOS.TabIndex = 19
        Me.tabiPOS.Text = "iPOS"
        Me.tabiPOS.UseVisualStyleBackColor = True
        '
        'grpiPOSInformation
        '
        Me.grpiPOSInformation.Controls.Add(Me.txtiPOSProcessDate)
        Me.grpiPOSInformation.Controls.Add(Me.lbiPOSProcessDate)
        Me.grpiPOSInformation.Controls.Add(Me.lbiPOSCurrentTime)
        Me.grpiPOSInformation.Controls.Add(Me.txtiPOSCurrentTime)
        Me.grpiPOSInformation.Controls.Add(Me.txtiPOSPollInterval)
        Me.grpiPOSInformation.Controls.Add(Me.CheckBoxiPOSEnabled)
        Me.grpiPOSInformation.Controls.Add(Me.lbiPOSPollInterval)
        Me.grpiPOSInformation.Controls.Add(Me.lbiPOSNextPoll)
        Me.grpiPOSInformation.Controls.Add(Me.txtiPOSLastPoll)
        Me.grpiPOSInformation.Controls.Add(Me.txtiPOSNextPoll)
        Me.grpiPOSInformation.Controls.Add(Me.lbiPOSLastPoll)
        Me.grpiPOSInformation.Location = New System.Drawing.Point(6, 4)
        Me.grpiPOSInformation.Name = "grpiPOSInformation"
        Me.grpiPOSInformation.Size = New System.Drawing.Size(188, 186)
        Me.grpiPOSInformation.TabIndex = 82
        Me.grpiPOSInformation.TabStop = False
        Me.grpiPOSInformation.Text = "Information"
        '
        'txtiPOSProcessDate
        '
        Me.txtiPOSProcessDate.Location = New System.Drawing.Point(94, 120)
        Me.txtiPOSProcessDate.Name = "txtiPOSProcessDate"
        Me.txtiPOSProcessDate.Size = New System.Drawing.Size(88, 16)
        Me.txtiPOSProcessDate.TabIndex = 76
        '
        'lbiPOSProcessDate
        '
        Me.lbiPOSProcessDate.Location = New System.Drawing.Point(6, 120)
        Me.lbiPOSProcessDate.Name = "lbiPOSProcessDate"
        Me.lbiPOSProcessDate.Size = New System.Drawing.Size(80, 16)
        Me.lbiPOSProcessDate.TabIndex = 75
        Me.lbiPOSProcessDate.Text = "Process Date:"
        '
        'lbiPOSCurrentTime
        '
        Me.lbiPOSCurrentTime.Location = New System.Drawing.Point(6, 24)
        Me.lbiPOSCurrentTime.Name = "lbiPOSCurrentTime"
        Me.lbiPOSCurrentTime.Size = New System.Drawing.Size(80, 16)
        Me.lbiPOSCurrentTime.TabIndex = 66
        Me.lbiPOSCurrentTime.Text = "Current Time:"
        '
        'txtiPOSCurrentTime
        '
        Me.txtiPOSCurrentTime.Location = New System.Drawing.Point(94, 24)
        Me.txtiPOSCurrentTime.Name = "txtiPOSCurrentTime"
        Me.txtiPOSCurrentTime.Size = New System.Drawing.Size(88, 16)
        Me.txtiPOSCurrentTime.TabIndex = 67
        '
        'txtiPOSPollInterval
        '
        Me.txtiPOSPollInterval.Location = New System.Drawing.Point(94, 48)
        Me.txtiPOSPollInterval.Name = "txtiPOSPollInterval"
        Me.txtiPOSPollInterval.Size = New System.Drawing.Size(88, 16)
        Me.txtiPOSPollInterval.TabIndex = 69
        '
        'CheckBoxiPOSEnabled
        '
        Me.CheckBoxiPOSEnabled.Location = New System.Drawing.Point(9, 152)
        Me.CheckBoxiPOSEnabled.Name = "CheckBoxiPOSEnabled"
        Me.CheckBoxiPOSEnabled.Size = New System.Drawing.Size(136, 24)
        Me.CheckBoxiPOSEnabled.TabIndex = 74
        Me.CheckBoxiPOSEnabled.Text = "Enable Gateway"
        '
        'lbiPOSPollInterval
        '
        Me.lbiPOSPollInterval.Location = New System.Drawing.Point(6, 48)
        Me.lbiPOSPollInterval.Name = "lbiPOSPollInterval"
        Me.lbiPOSPollInterval.Size = New System.Drawing.Size(80, 16)
        Me.lbiPOSPollInterval.TabIndex = 68
        Me.lbiPOSPollInterval.Text = "Poll Interval:"
        '
        'lbiPOSNextPoll
        '
        Me.lbiPOSNextPoll.Location = New System.Drawing.Point(6, 96)
        Me.lbiPOSNextPoll.Name = "lbiPOSNextPoll"
        Me.lbiPOSNextPoll.Size = New System.Drawing.Size(80, 16)
        Me.lbiPOSNextPoll.TabIndex = 72
        Me.lbiPOSNextPoll.Text = "Next Poll:"
        '
        'txtiPOSLastPoll
        '
        Me.txtiPOSLastPoll.Location = New System.Drawing.Point(94, 72)
        Me.txtiPOSLastPoll.Name = "txtiPOSLastPoll"
        Me.txtiPOSLastPoll.Size = New System.Drawing.Size(88, 16)
        Me.txtiPOSLastPoll.TabIndex = 71
        '
        'txtiPOSNextPoll
        '
        Me.txtiPOSNextPoll.Location = New System.Drawing.Point(94, 96)
        Me.txtiPOSNextPoll.Name = "txtiPOSNextPoll"
        Me.txtiPOSNextPoll.Size = New System.Drawing.Size(88, 16)
        Me.txtiPOSNextPoll.TabIndex = 73
        '
        'lbiPOSLastPoll
        '
        Me.lbiPOSLastPoll.Location = New System.Drawing.Point(6, 72)
        Me.lbiPOSLastPoll.Name = "lbiPOSLastPoll"
        Me.lbiPOSLastPoll.Size = New System.Drawing.Size(80, 16)
        Me.lbiPOSLastPoll.TabIndex = 70
        Me.lbiPOSLastPoll.Text = "Last Poll:"
        '
        'grpiPOSProcess
        '
        Me.grpiPOSProcess.Controls.Add(Me.lbiPOSLastDate)
        Me.grpiPOSProcess.Controls.Add(Me.ButtoniPOSImport)
        Me.grpiPOSProcess.Controls.Add(Me.ButtoniPOSUpdateProcessDate)
        Me.grpiPOSProcess.Controls.Add(Me.dtiPOSProcessDate)
        Me.grpiPOSProcess.Location = New System.Drawing.Point(205, 4)
        Me.grpiPOSProcess.Name = "grpiPOSProcess"
        Me.grpiPOSProcess.Size = New System.Drawing.Size(152, 131)
        Me.grpiPOSProcess.TabIndex = 81
        Me.grpiPOSProcess.TabStop = False
        Me.grpiPOSProcess.Text = "Data Process"
        '
        'lbiPOSLastDate
        '
        Me.lbiPOSLastDate.Location = New System.Drawing.Point(12, 24)
        Me.lbiPOSLastDate.Name = "lbiPOSLastDate"
        Me.lbiPOSLastDate.Size = New System.Drawing.Size(74, 16)
        Me.lbiPOSLastDate.TabIndex = 78
        Me.lbiPOSLastDate.Text = "Process Date:"
        '
        'ButtoniPOSImport
        '
        Me.ButtoniPOSImport.Location = New System.Drawing.Point(15, 96)
        Me.ButtoniPOSImport.Name = "ButtoniPOSImport"
        Me.ButtoniPOSImport.Size = New System.Drawing.Size(123, 23)
        Me.ButtoniPOSImport.TabIndex = 65
        Me.ButtoniPOSImport.Text = "Process"
        '
        'ButtoniPOSUpdateProcessDate
        '
        Me.ButtoniPOSUpdateProcessDate.Location = New System.Drawing.Point(15, 64)
        Me.ButtoniPOSUpdateProcessDate.Name = "ButtoniPOSUpdateProcessDate"
        Me.ButtoniPOSUpdateProcessDate.Size = New System.Drawing.Size(123, 23)
        Me.ButtoniPOSUpdateProcessDate.TabIndex = 79
        Me.ButtoniPOSUpdateProcessDate.Text = "Apply"
        '
        'dtiPOSProcessDate
        '
        Me.dtiPOSProcessDate.CustomFormat = "dd-MMM-yyyy"
        Me.dtiPOSProcessDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtiPOSProcessDate.Location = New System.Drawing.Point(15, 41)
        Me.dtiPOSProcessDate.Name = "dtiPOSProcessDate"
        Me.dtiPOSProcessDate.Size = New System.Drawing.Size(123, 20)
        Me.dtiPOSProcessDate.TabIndex = 77
        Me.dtiPOSProcessDate.Value = New Date(2007, 9, 1, 0, 0, 0, 0)
        '
        'grpiPOSUpdates
        '
        Me.grpiPOSUpdates.Controls.Add(Me.ButtoniPOSUpdateProducts)
        Me.grpiPOSUpdates.Controls.Add(Me.ButtoniPOSUpdateStaff)
        Me.grpiPOSUpdates.Location = New System.Drawing.Point(205, 141)
        Me.grpiPOSUpdates.Name = "grpiPOSUpdates"
        Me.grpiPOSUpdates.Size = New System.Drawing.Size(152, 49)
        Me.grpiPOSUpdates.TabIndex = 80
        Me.grpiPOSUpdates.TabStop = False
        Me.grpiPOSUpdates.Text = "Data Update"
        '
        'ButtoniPOSUpdateProducts
        '
        Me.ButtoniPOSUpdateProducts.Location = New System.Drawing.Point(15, 19)
        Me.ButtoniPOSUpdateProducts.Name = "ButtoniPOSUpdateProducts"
        Me.ButtoniPOSUpdateProducts.Size = New System.Drawing.Size(60, 23)
        Me.ButtoniPOSUpdateProducts.TabIndex = 75
        Me.ButtoniPOSUpdateProducts.Text = "&Products"
        Me.ButtoniPOSUpdateProducts.UseVisualStyleBackColor = True
        '
        'ButtoniPOSUpdateStaff
        '
        Me.ButtoniPOSUpdateStaff.Location = New System.Drawing.Point(80, 19)
        Me.ButtoniPOSUpdateStaff.Name = "ButtoniPOSUpdateStaff"
        Me.ButtoniPOSUpdateStaff.Size = New System.Drawing.Size(60, 23)
        Me.ButtoniPOSUpdateStaff.TabIndex = 76
        Me.ButtoniPOSUpdateStaff.Text = "&Staff"
        Me.ButtoniPOSUpdateStaff.UseVisualStyleBackColor = True
        '
        'tabUniwell
        '
        Me.tabUniwell.Controls.Add(Me.CheckBoxUniwellEnabled)
        Me.tabUniwell.Controls.Add(Me.lbUniwellNextPoll)
        Me.tabUniwell.Controls.Add(Me.txtUniwellNextPoll)
        Me.tabUniwell.Controls.Add(Me.lbUniwellLastPoll)
        Me.tabUniwell.Controls.Add(Me.txtUniwellLastpoll)
        Me.tabUniwell.Controls.Add(Me.lbUniwellPollInterval)
        Me.tabUniwell.Controls.Add(Me.txtUniwellPollInterval)
        Me.tabUniwell.Controls.Add(Me.txtUniwellCurrentTime)
        Me.tabUniwell.Controls.Add(Me.lbUniwellCurrentTime)
        Me.tabUniwell.Controls.Add(Me.ButtonUniwellImport)
        Me.tabUniwell.Location = New System.Drawing.Point(4, 22)
        Me.tabUniwell.Name = "tabUniwell"
        Me.tabUniwell.Size = New System.Drawing.Size(364, 198)
        Me.tabUniwell.TabIndex = 20
        Me.tabUniwell.Text = "Uniwell"
        Me.tabUniwell.UseVisualStyleBackColor = True
        '
        'CheckBoxUniwellEnabled
        '
        Me.CheckBoxUniwellEnabled.Location = New System.Drawing.Point(18, 124)
        Me.CheckBoxUniwellEnabled.Name = "CheckBoxUniwellEnabled"
        Me.CheckBoxUniwellEnabled.Size = New System.Drawing.Size(136, 24)
        Me.CheckBoxUniwellEnabled.TabIndex = 54
        Me.CheckBoxUniwellEnabled.Text = "Enable Gateway"
        '
        'lbUniwellNextPoll
        '
        Me.lbUniwellNextPoll.Location = New System.Drawing.Point(18, 100)
        Me.lbUniwellNextPoll.Name = "lbUniwellNextPoll"
        Me.lbUniwellNextPoll.Size = New System.Drawing.Size(80, 16)
        Me.lbUniwellNextPoll.TabIndex = 52
        Me.lbUniwellNextPoll.Text = "Next Poll:"
        '
        'txtUniwellNextPoll
        '
        Me.txtUniwellNextPoll.Location = New System.Drawing.Point(106, 100)
        Me.txtUniwellNextPoll.Name = "txtUniwellNextPoll"
        Me.txtUniwellNextPoll.Size = New System.Drawing.Size(88, 16)
        Me.txtUniwellNextPoll.TabIndex = 53
        '
        'lbUniwellLastPoll
        '
        Me.lbUniwellLastPoll.Location = New System.Drawing.Point(18, 76)
        Me.lbUniwellLastPoll.Name = "lbUniwellLastPoll"
        Me.lbUniwellLastPoll.Size = New System.Drawing.Size(80, 16)
        Me.lbUniwellLastPoll.TabIndex = 50
        Me.lbUniwellLastPoll.Text = "Last Poll:"
        '
        'txtUniwellLastpoll
        '
        Me.txtUniwellLastpoll.Location = New System.Drawing.Point(106, 76)
        Me.txtUniwellLastpoll.Name = "txtUniwellLastpoll"
        Me.txtUniwellLastpoll.Size = New System.Drawing.Size(88, 16)
        Me.txtUniwellLastpoll.TabIndex = 51
        '
        'lbUniwellPollInterval
        '
        Me.lbUniwellPollInterval.Location = New System.Drawing.Point(18, 52)
        Me.lbUniwellPollInterval.Name = "lbUniwellPollInterval"
        Me.lbUniwellPollInterval.Size = New System.Drawing.Size(80, 16)
        Me.lbUniwellPollInterval.TabIndex = 48
        Me.lbUniwellPollInterval.Text = "Poll Interval:"
        '
        'txtUniwellPollInterval
        '
        Me.txtUniwellPollInterval.Location = New System.Drawing.Point(106, 52)
        Me.txtUniwellPollInterval.Name = "txtUniwellPollInterval"
        Me.txtUniwellPollInterval.Size = New System.Drawing.Size(88, 16)
        Me.txtUniwellPollInterval.TabIndex = 49
        '
        'txtUniwellCurrentTime
        '
        Me.txtUniwellCurrentTime.Location = New System.Drawing.Point(106, 28)
        Me.txtUniwellCurrentTime.Name = "txtUniwellCurrentTime"
        Me.txtUniwellCurrentTime.Size = New System.Drawing.Size(88, 16)
        Me.txtUniwellCurrentTime.TabIndex = 47
        '
        'lbUniwellCurrentTime
        '
        Me.lbUniwellCurrentTime.Location = New System.Drawing.Point(18, 28)
        Me.lbUniwellCurrentTime.Name = "lbUniwellCurrentTime"
        Me.lbUniwellCurrentTime.Size = New System.Drawing.Size(80, 16)
        Me.lbUniwellCurrentTime.TabIndex = 46
        Me.lbUniwellCurrentTime.Text = "Current Time:"
        '
        'ButtonUniwellImport
        '
        Me.ButtonUniwellImport.Location = New System.Drawing.Point(266, 12)
        Me.ButtonUniwellImport.Name = "ButtonUniwellImport"
        Me.ButtonUniwellImport.Size = New System.Drawing.Size(75, 23)
        Me.ButtonUniwellImport.TabIndex = 45
        Me.ButtonUniwellImport.Text = "Process"
        '
        'tabRadiant
        '
        Me.tabRadiant.Controls.Add(Me.CheckBoxRadiantEnabled)
        Me.tabRadiant.Controls.Add(Me.Label63)
        Me.tabRadiant.Controls.Add(Me.txtRadiantNextPoll)
        Me.tabRadiant.Controls.Add(Me.Label65)
        Me.tabRadiant.Controls.Add(Me.txtRadiantLastPoll)
        Me.tabRadiant.Controls.Add(Me.Label67)
        Me.tabRadiant.Controls.Add(Me.txtRadiantPollInterval)
        Me.tabRadiant.Controls.Add(Me.txtRadiantCurrentTime)
        Me.tabRadiant.Controls.Add(Me.Label70)
        Me.tabRadiant.Controls.Add(Me.ButtonRadiantImport)
        Me.tabRadiant.Location = New System.Drawing.Point(4, 22)
        Me.tabRadiant.Name = "tabRadiant"
        Me.tabRadiant.Size = New System.Drawing.Size(364, 198)
        Me.tabRadiant.TabIndex = 21
        Me.tabRadiant.Text = "Radiant"
        Me.tabRadiant.UseVisualStyleBackColor = True
        '
        'CheckBoxRadiantEnabled
        '
        Me.CheckBoxRadiantEnabled.Location = New System.Drawing.Point(22, 131)
        Me.CheckBoxRadiantEnabled.Name = "CheckBoxRadiantEnabled"
        Me.CheckBoxRadiantEnabled.Size = New System.Drawing.Size(136, 24)
        Me.CheckBoxRadiantEnabled.TabIndex = 64
        Me.CheckBoxRadiantEnabled.Text = "Enable Gateway"
        '
        'Label63
        '
        Me.Label63.Location = New System.Drawing.Point(22, 107)
        Me.Label63.Name = "Label63"
        Me.Label63.Size = New System.Drawing.Size(80, 16)
        Me.Label63.TabIndex = 62
        Me.Label63.Text = "Next Poll:"
        '
        'txtRadiantNextPoll
        '
        Me.txtRadiantNextPoll.Location = New System.Drawing.Point(110, 107)
        Me.txtRadiantNextPoll.Name = "txtRadiantNextPoll"
        Me.txtRadiantNextPoll.Size = New System.Drawing.Size(88, 16)
        Me.txtRadiantNextPoll.TabIndex = 63
        '
        'Label65
        '
        Me.Label65.Location = New System.Drawing.Point(22, 83)
        Me.Label65.Name = "Label65"
        Me.Label65.Size = New System.Drawing.Size(80, 16)
        Me.Label65.TabIndex = 60
        Me.Label65.Text = "Last Poll:"
        '
        'txtRadiantLastPoll
        '
        Me.txtRadiantLastPoll.Location = New System.Drawing.Point(110, 83)
        Me.txtRadiantLastPoll.Name = "txtRadiantLastPoll"
        Me.txtRadiantLastPoll.Size = New System.Drawing.Size(88, 16)
        Me.txtRadiantLastPoll.TabIndex = 61
        '
        'Label67
        '
        Me.Label67.Location = New System.Drawing.Point(22, 59)
        Me.Label67.Name = "Label67"
        Me.Label67.Size = New System.Drawing.Size(80, 16)
        Me.Label67.TabIndex = 58
        Me.Label67.Text = "Poll Interval:"
        '
        'txtRadiantPollInterval
        '
        Me.txtRadiantPollInterval.Location = New System.Drawing.Point(110, 59)
        Me.txtRadiantPollInterval.Name = "txtRadiantPollInterval"
        Me.txtRadiantPollInterval.Size = New System.Drawing.Size(88, 16)
        Me.txtRadiantPollInterval.TabIndex = 59
        '
        'txtRadiantCurrentTime
        '
        Me.txtRadiantCurrentTime.Location = New System.Drawing.Point(110, 35)
        Me.txtRadiantCurrentTime.Name = "txtRadiantCurrentTime"
        Me.txtRadiantCurrentTime.Size = New System.Drawing.Size(88, 16)
        Me.txtRadiantCurrentTime.TabIndex = 57
        '
        'Label70
        '
        Me.Label70.Location = New System.Drawing.Point(22, 35)
        Me.Label70.Name = "Label70"
        Me.Label70.Size = New System.Drawing.Size(80, 16)
        Me.Label70.TabIndex = 56
        Me.Label70.Text = "Current Time:"
        '
        'ButtonRadiantImport
        '
        Me.ButtonRadiantImport.Location = New System.Drawing.Point(270, 19)
        Me.ButtonRadiantImport.Name = "ButtonRadiantImport"
        Me.ButtonRadiantImport.Size = New System.Drawing.Size(75, 23)
        Me.ButtonRadiantImport.TabIndex = 55
        Me.ButtonRadiantImport.Text = "Process"
        '
        'tabNAXML
        '
        Me.tabNAXML.Controls.Add(Me.CheckBoxNAXMLEnabled)
        Me.tabNAXML.Controls.Add(Me.Label64)
        Me.tabNAXML.Controls.Add(Me.txtNAXMLNextPoll)
        Me.tabNAXML.Controls.Add(Me.Label68)
        Me.tabNAXML.Controls.Add(Me.txtNAXMLLastPoll)
        Me.tabNAXML.Controls.Add(Me.Label71)
        Me.tabNAXML.Controls.Add(Me.txtNAXMLPollInterval)
        Me.tabNAXML.Controls.Add(Me.txtNAXMLCurrentTime)
        Me.tabNAXML.Controls.Add(Me.Label74)
        Me.tabNAXML.Controls.Add(Me.ButtonNAXMLImport)
        Me.tabNAXML.Location = New System.Drawing.Point(4, 22)
        Me.tabNAXML.Name = "tabNAXML"
        Me.tabNAXML.Padding = New System.Windows.Forms.Padding(3)
        Me.tabNAXML.Size = New System.Drawing.Size(364, 198)
        Me.tabNAXML.TabIndex = 22
        Me.tabNAXML.Text = "NAXML"
        Me.tabNAXML.UseVisualStyleBackColor = True
        '
        'CheckBoxNAXMLEnabled
        '
        Me.CheckBoxNAXMLEnabled.Location = New System.Drawing.Point(22, 131)
        Me.CheckBoxNAXMLEnabled.Name = "CheckBoxNAXMLEnabled"
        Me.CheckBoxNAXMLEnabled.Size = New System.Drawing.Size(136, 24)
        Me.CheckBoxNAXMLEnabled.TabIndex = 74
        Me.CheckBoxNAXMLEnabled.Text = "Enable Gateway"
        '
        'Label64
        '
        Me.Label64.Location = New System.Drawing.Point(22, 107)
        Me.Label64.Name = "Label64"
        Me.Label64.Size = New System.Drawing.Size(80, 16)
        Me.Label64.TabIndex = 72
        Me.Label64.Text = "Next Poll:"
        '
        'txtNAXMLNextPoll
        '
        Me.txtNAXMLNextPoll.Location = New System.Drawing.Point(110, 107)
        Me.txtNAXMLNextPoll.Name = "txtNAXMLNextPoll"
        Me.txtNAXMLNextPoll.Size = New System.Drawing.Size(88, 16)
        Me.txtNAXMLNextPoll.TabIndex = 73
        '
        'Label68
        '
        Me.Label68.Location = New System.Drawing.Point(22, 83)
        Me.Label68.Name = "Label68"
        Me.Label68.Size = New System.Drawing.Size(80, 16)
        Me.Label68.TabIndex = 70
        Me.Label68.Text = "Last Poll:"
        '
        'txtNAXMLLastPoll
        '
        Me.txtNAXMLLastPoll.Location = New System.Drawing.Point(110, 83)
        Me.txtNAXMLLastPoll.Name = "txtNAXMLLastPoll"
        Me.txtNAXMLLastPoll.Size = New System.Drawing.Size(88, 16)
        Me.txtNAXMLLastPoll.TabIndex = 71
        '
        'Label71
        '
        Me.Label71.Location = New System.Drawing.Point(22, 59)
        Me.Label71.Name = "Label71"
        Me.Label71.Size = New System.Drawing.Size(80, 16)
        Me.Label71.TabIndex = 68
        Me.Label71.Text = "Poll Interval:"
        '
        'txtNAXMLPollInterval
        '
        Me.txtNAXMLPollInterval.Location = New System.Drawing.Point(110, 59)
        Me.txtNAXMLPollInterval.Name = "txtNAXMLPollInterval"
        Me.txtNAXMLPollInterval.Size = New System.Drawing.Size(88, 16)
        Me.txtNAXMLPollInterval.TabIndex = 69
        '
        'txtNAXMLCurrentTime
        '
        Me.txtNAXMLCurrentTime.Location = New System.Drawing.Point(110, 35)
        Me.txtNAXMLCurrentTime.Name = "txtNAXMLCurrentTime"
        Me.txtNAXMLCurrentTime.Size = New System.Drawing.Size(88, 16)
        Me.txtNAXMLCurrentTime.TabIndex = 67
        '
        'Label74
        '
        Me.Label74.Location = New System.Drawing.Point(22, 35)
        Me.Label74.Name = "Label74"
        Me.Label74.Size = New System.Drawing.Size(80, 16)
        Me.Label74.TabIndex = 66
        Me.Label74.Text = "Current Time:"
        '
        'ButtonNAXMLImport
        '
        Me.ButtonNAXMLImport.Location = New System.Drawing.Point(270, 19)
        Me.ButtonNAXMLImport.Name = "ButtonNAXMLImport"
        Me.ButtonNAXMLImport.Size = New System.Drawing.Size(75, 23)
        Me.ButtonNAXMLImport.TabIndex = 65
        Me.ButtonNAXMLImport.Text = "Process"
        '
        'tabBreeze
        '
        Me.tabBreeze.Controls.Add(Me.txtBreezeProcessDate)
        Me.tabBreeze.Controls.Add(Me.lbBreezeProcessDate)
        Me.tabBreeze.Controls.Add(Me.BreezeProcessGroup)
        Me.tabBreeze.Controls.Add(Me.CheckBoxBreezeEnabled)
        Me.tabBreeze.Controls.Add(Me.lbBreezeNextPoll)
        Me.tabBreeze.Controls.Add(Me.txtBreezeNextPoll)
        Me.tabBreeze.Controls.Add(Me.lbBreezeLastPoll)
        Me.tabBreeze.Controls.Add(Me.txtBreezeLastPoll)
        Me.tabBreeze.Controls.Add(Me.lbBreezePollInterval)
        Me.tabBreeze.Controls.Add(Me.txtBreezePollInterval)
        Me.tabBreeze.Controls.Add(Me.txtBreezeCurrentTime)
        Me.tabBreeze.Controls.Add(Me.lbBreezeCurrentTime)
        Me.tabBreeze.Location = New System.Drawing.Point(4, 22)
        Me.tabBreeze.Name = "tabBreeze"
        Me.tabBreeze.Size = New System.Drawing.Size(364, 198)
        Me.tabBreeze.TabIndex = 23
        Me.tabBreeze.Text = "Breeze"
        Me.tabBreeze.UseVisualStyleBackColor = True
        '
        'txtBreezeProcessDate
        '
        Me.txtBreezeProcessDate.Location = New System.Drawing.Point(111, 129)
        Me.txtBreezeProcessDate.Name = "txtBreezeProcessDate"
        Me.txtBreezeProcessDate.Size = New System.Drawing.Size(88, 16)
        Me.txtBreezeProcessDate.TabIndex = 87
        '
        'lbBreezeProcessDate
        '
        Me.lbBreezeProcessDate.Location = New System.Drawing.Point(23, 129)
        Me.lbBreezeProcessDate.Name = "lbBreezeProcessDate"
        Me.lbBreezeProcessDate.Size = New System.Drawing.Size(80, 16)
        Me.lbBreezeProcessDate.TabIndex = 86
        Me.lbBreezeProcessDate.Text = "Process Date:"
        '
        'BreezeProcessGroup
        '
        Me.BreezeProcessGroup.Controls.Add(Me.Label66)
        Me.BreezeProcessGroup.Controls.Add(Me.ButtonBreezeImport)
        Me.BreezeProcessGroup.Controls.Add(Me.ButtonBreezeUpdateProcessDate)
        Me.BreezeProcessGroup.Controls.Add(Me.dtBreezeProcessDate)
        Me.BreezeProcessGroup.Location = New System.Drawing.Point(209, 18)
        Me.BreezeProcessGroup.Name = "BreezeProcessGroup"
        Me.BreezeProcessGroup.Size = New System.Drawing.Size(152, 131)
        Me.BreezeProcessGroup.TabIndex = 85
        Me.BreezeProcessGroup.TabStop = False
        Me.BreezeProcessGroup.Text = "Data Process"
        '
        'Label66
        '
        Me.Label66.Location = New System.Drawing.Point(12, 24)
        Me.Label66.Name = "Label66"
        Me.Label66.Size = New System.Drawing.Size(74, 16)
        Me.Label66.TabIndex = 78
        Me.Label66.Text = "Process Date:"
        '
        'ButtonBreezeImport
        '
        Me.ButtonBreezeImport.Location = New System.Drawing.Point(15, 96)
        Me.ButtonBreezeImport.Name = "ButtonBreezeImport"
        Me.ButtonBreezeImport.Size = New System.Drawing.Size(123, 23)
        Me.ButtonBreezeImport.TabIndex = 65
        Me.ButtonBreezeImport.Text = "Process"
        '
        'ButtonBreezeUpdateProcessDate
        '
        Me.ButtonBreezeUpdateProcessDate.Location = New System.Drawing.Point(15, 64)
        Me.ButtonBreezeUpdateProcessDate.Name = "ButtonBreezeUpdateProcessDate"
        Me.ButtonBreezeUpdateProcessDate.Size = New System.Drawing.Size(123, 23)
        Me.ButtonBreezeUpdateProcessDate.TabIndex = 79
        Me.ButtonBreezeUpdateProcessDate.Text = "Apply"
        '
        'dtBreezeProcessDate
        '
        Me.dtBreezeProcessDate.CustomFormat = "dd-MMM-yyyy"
        Me.dtBreezeProcessDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtBreezeProcessDate.Location = New System.Drawing.Point(15, 41)
        Me.dtBreezeProcessDate.Name = "dtBreezeProcessDate"
        Me.dtBreezeProcessDate.Size = New System.Drawing.Size(123, 20)
        Me.dtBreezeProcessDate.TabIndex = 77
        Me.dtBreezeProcessDate.Value = New Date(2010, 7, 1, 0, 0, 0, 0)
        '
        'CheckBoxBreezeEnabled
        '
        Me.CheckBoxBreezeEnabled.Location = New System.Drawing.Point(26, 156)
        Me.CheckBoxBreezeEnabled.Name = "CheckBoxBreezeEnabled"
        Me.CheckBoxBreezeEnabled.Size = New System.Drawing.Size(136, 24)
        Me.CheckBoxBreezeEnabled.TabIndex = 84
        Me.CheckBoxBreezeEnabled.Text = "Enable Gateway"
        '
        'lbBreezeNextPoll
        '
        Me.lbBreezeNextPoll.Location = New System.Drawing.Point(23, 107)
        Me.lbBreezeNextPoll.Name = "lbBreezeNextPoll"
        Me.lbBreezeNextPoll.Size = New System.Drawing.Size(80, 16)
        Me.lbBreezeNextPoll.TabIndex = 82
        Me.lbBreezeNextPoll.Text = "Next Poll:"
        '
        'txtBreezeNextPoll
        '
        Me.txtBreezeNextPoll.Location = New System.Drawing.Point(111, 107)
        Me.txtBreezeNextPoll.Name = "txtBreezeNextPoll"
        Me.txtBreezeNextPoll.Size = New System.Drawing.Size(88, 16)
        Me.txtBreezeNextPoll.TabIndex = 83
        '
        'lbBreezeLastPoll
        '
        Me.lbBreezeLastPoll.Location = New System.Drawing.Point(23, 83)
        Me.lbBreezeLastPoll.Name = "lbBreezeLastPoll"
        Me.lbBreezeLastPoll.Size = New System.Drawing.Size(80, 16)
        Me.lbBreezeLastPoll.TabIndex = 80
        Me.lbBreezeLastPoll.Text = "Last Poll:"
        '
        'txtBreezeLastPoll
        '
        Me.txtBreezeLastPoll.Location = New System.Drawing.Point(111, 83)
        Me.txtBreezeLastPoll.Name = "txtBreezeLastPoll"
        Me.txtBreezeLastPoll.Size = New System.Drawing.Size(88, 16)
        Me.txtBreezeLastPoll.TabIndex = 81
        '
        'lbBreezePollInterval
        '
        Me.lbBreezePollInterval.Location = New System.Drawing.Point(23, 59)
        Me.lbBreezePollInterval.Name = "lbBreezePollInterval"
        Me.lbBreezePollInterval.Size = New System.Drawing.Size(80, 16)
        Me.lbBreezePollInterval.TabIndex = 78
        Me.lbBreezePollInterval.Text = "Poll Interval:"
        '
        'txtBreezePollInterval
        '
        Me.txtBreezePollInterval.Location = New System.Drawing.Point(111, 59)
        Me.txtBreezePollInterval.Name = "txtBreezePollInterval"
        Me.txtBreezePollInterval.Size = New System.Drawing.Size(88, 16)
        Me.txtBreezePollInterval.TabIndex = 79
        '
        'txtBreezeCurrentTime
        '
        Me.txtBreezeCurrentTime.Location = New System.Drawing.Point(111, 35)
        Me.txtBreezeCurrentTime.Name = "txtBreezeCurrentTime"
        Me.txtBreezeCurrentTime.Size = New System.Drawing.Size(88, 16)
        Me.txtBreezeCurrentTime.TabIndex = 77
        '
        'lbBreezeCurrentTime
        '
        Me.lbBreezeCurrentTime.Location = New System.Drawing.Point(23, 35)
        Me.lbBreezeCurrentTime.Name = "lbBreezeCurrentTime"
        Me.lbBreezeCurrentTime.Size = New System.Drawing.Size(80, 16)
        Me.lbBreezeCurrentTime.TabIndex = 76
        Me.lbBreezeCurrentTime.Text = "Current Time:"
        '
        'tabXpient
        '
        Me.tabXpient.Controls.Add(Me.CheckBoxXpientEnabled)
        Me.tabXpient.Controls.Add(Me.Label69)
        Me.tabXpient.Controls.Add(Me.txtXpientNextPoll)
        Me.tabXpient.Controls.Add(Me.Label73)
        Me.tabXpient.Controls.Add(Me.txtXpientLastPoll)
        Me.tabXpient.Controls.Add(Me.Label76)
        Me.tabXpient.Controls.Add(Me.txtXpientPollInterval)
        Me.tabXpient.Controls.Add(Me.txtXpientCurrentTime)
        Me.tabXpient.Controls.Add(Me.Label79)
        Me.tabXpient.Controls.Add(Me.ButtonXpientImport)
        Me.tabXpient.Location = New System.Drawing.Point(4, 22)
        Me.tabXpient.Name = "tabXpient"
        Me.tabXpient.Size = New System.Drawing.Size(364, 198)
        Me.tabXpient.TabIndex = 24
        Me.tabXpient.Text = "Xpient"
        Me.tabXpient.UseVisualStyleBackColor = True
        '
        'CheckBoxXpientEnabled
        '
        Me.CheckBoxXpientEnabled.Location = New System.Drawing.Point(20, 131)
        Me.CheckBoxXpientEnabled.Name = "CheckBoxXpientEnabled"
        Me.CheckBoxXpientEnabled.Size = New System.Drawing.Size(136, 24)
        Me.CheckBoxXpientEnabled.TabIndex = 84
        Me.CheckBoxXpientEnabled.Text = "Enable Gateway"
        '
        'Label69
        '
        Me.Label69.Location = New System.Drawing.Point(20, 107)
        Me.Label69.Name = "Label69"
        Me.Label69.Size = New System.Drawing.Size(80, 16)
        Me.Label69.TabIndex = 82
        Me.Label69.Text = "Next Poll:"
        '
        'txtXpientNextPoll
        '
        Me.txtXpientNextPoll.Location = New System.Drawing.Point(108, 107)
        Me.txtXpientNextPoll.Name = "txtXpientNextPoll"
        Me.txtXpientNextPoll.Size = New System.Drawing.Size(88, 16)
        Me.txtXpientNextPoll.TabIndex = 83
        '
        'Label73
        '
        Me.Label73.Location = New System.Drawing.Point(20, 83)
        Me.Label73.Name = "Label73"
        Me.Label73.Size = New System.Drawing.Size(80, 16)
        Me.Label73.TabIndex = 80
        Me.Label73.Text = "Last Poll:"
        '
        'txtXpientLastPoll
        '
        Me.txtXpientLastPoll.Location = New System.Drawing.Point(108, 83)
        Me.txtXpientLastPoll.Name = "txtXpientLastPoll"
        Me.txtXpientLastPoll.Size = New System.Drawing.Size(88, 16)
        Me.txtXpientLastPoll.TabIndex = 81
        '
        'Label76
        '
        Me.Label76.Location = New System.Drawing.Point(20, 59)
        Me.Label76.Name = "Label76"
        Me.Label76.Size = New System.Drawing.Size(80, 16)
        Me.Label76.TabIndex = 78
        Me.Label76.Text = "Poll Interval:"
        '
        'txtXpientPollInterval
        '
        Me.txtXpientPollInterval.Location = New System.Drawing.Point(108, 59)
        Me.txtXpientPollInterval.Name = "txtXpientPollInterval"
        Me.txtXpientPollInterval.Size = New System.Drawing.Size(88, 16)
        Me.txtXpientPollInterval.TabIndex = 79
        '
        'txtXpientCurrentTime
        '
        Me.txtXpientCurrentTime.Location = New System.Drawing.Point(108, 35)
        Me.txtXpientCurrentTime.Name = "txtXpientCurrentTime"
        Me.txtXpientCurrentTime.Size = New System.Drawing.Size(88, 16)
        Me.txtXpientCurrentTime.TabIndex = 77
        '
        'Label79
        '
        Me.Label79.Location = New System.Drawing.Point(20, 35)
        Me.Label79.Name = "Label79"
        Me.Label79.Size = New System.Drawing.Size(80, 16)
        Me.Label79.TabIndex = 76
        Me.Label79.Text = "Current Time:"
        '
        'ButtonXpientImport
        '
        Me.ButtonXpientImport.Location = New System.Drawing.Point(268, 19)
        Me.ButtonXpientImport.Name = "ButtonXpientImport"
        Me.ButtonXpientImport.Size = New System.Drawing.Size(75, 23)
        Me.ButtonXpientImport.TabIndex = 75
        Me.ButtonXpientImport.Text = "Process"
        '
        'tabRetalix
        '
        Me.tabRetalix.Controls.Add(Me.CheckBoxRetalixEnabled)
        Me.tabRetalix.Controls.Add(Me.Label80)
        Me.tabRetalix.Controls.Add(Me.txtRetalixNextPoll)
        Me.tabRetalix.Controls.Add(Me.Label82)
        Me.tabRetalix.Controls.Add(Me.txtRetalixLastPoll)
        Me.tabRetalix.Controls.Add(Me.Label84)
        Me.tabRetalix.Controls.Add(Me.txtRetalixPollInterval)
        Me.tabRetalix.Controls.Add(Me.txtRetalixCurrentTime)
        Me.tabRetalix.Controls.Add(Me.Label87)
        Me.tabRetalix.Controls.Add(Me.ButtonRetalixImport)
        Me.tabRetalix.Location = New System.Drawing.Point(4, 22)
        Me.tabRetalix.Name = "tabRetalix"
        Me.tabRetalix.Size = New System.Drawing.Size(364, 198)
        Me.tabRetalix.TabIndex = 25
        Me.tabRetalix.Text = "Retalix"
        Me.tabRetalix.UseVisualStyleBackColor = True
        '
        'CheckBoxRetalixEnabled
        '
        Me.CheckBoxRetalixEnabled.Location = New System.Drawing.Point(25, 129)
        Me.CheckBoxRetalixEnabled.Name = "CheckBoxRetalixEnabled"
        Me.CheckBoxRetalixEnabled.Size = New System.Drawing.Size(136, 24)
        Me.CheckBoxRetalixEnabled.TabIndex = 84
        Me.CheckBoxRetalixEnabled.Text = "Enable Gateway"
        '
        'Label80
        '
        Me.Label80.Location = New System.Drawing.Point(25, 105)
        Me.Label80.Name = "Label80"
        Me.Label80.Size = New System.Drawing.Size(80, 16)
        Me.Label80.TabIndex = 82
        Me.Label80.Text = "Next Poll:"
        '
        'txtRetalixNextPoll
        '
        Me.txtRetalixNextPoll.Location = New System.Drawing.Point(113, 105)
        Me.txtRetalixNextPoll.Name = "txtRetalixNextPoll"
        Me.txtRetalixNextPoll.Size = New System.Drawing.Size(88, 16)
        Me.txtRetalixNextPoll.TabIndex = 83
        '
        'Label82
        '
        Me.Label82.Location = New System.Drawing.Point(25, 81)
        Me.Label82.Name = "Label82"
        Me.Label82.Size = New System.Drawing.Size(80, 16)
        Me.Label82.TabIndex = 80
        Me.Label82.Text = "Last Poll:"
        '
        'txtRetalixLastPoll
        '
        Me.txtRetalixLastPoll.Location = New System.Drawing.Point(113, 81)
        Me.txtRetalixLastPoll.Name = "txtRetalixLastPoll"
        Me.txtRetalixLastPoll.Size = New System.Drawing.Size(88, 16)
        Me.txtRetalixLastPoll.TabIndex = 81
        '
        'Label84
        '
        Me.Label84.Location = New System.Drawing.Point(25, 57)
        Me.Label84.Name = "Label84"
        Me.Label84.Size = New System.Drawing.Size(80, 16)
        Me.Label84.TabIndex = 78
        Me.Label84.Text = "Poll Interval:"
        '
        'txtRetalixPollInterval
        '
        Me.txtRetalixPollInterval.Location = New System.Drawing.Point(113, 57)
        Me.txtRetalixPollInterval.Name = "txtRetalixPollInterval"
        Me.txtRetalixPollInterval.Size = New System.Drawing.Size(88, 16)
        Me.txtRetalixPollInterval.TabIndex = 79
        '
        'txtRetalixCurrentTime
        '
        Me.txtRetalixCurrentTime.Location = New System.Drawing.Point(113, 33)
        Me.txtRetalixCurrentTime.Name = "txtRetalixCurrentTime"
        Me.txtRetalixCurrentTime.Size = New System.Drawing.Size(88, 16)
        Me.txtRetalixCurrentTime.TabIndex = 77
        '
        'Label87
        '
        Me.Label87.Location = New System.Drawing.Point(25, 33)
        Me.Label87.Name = "Label87"
        Me.Label87.Size = New System.Drawing.Size(80, 16)
        Me.Label87.TabIndex = 76
        Me.Label87.Text = "Current Time:"
        '
        'ButtonRetalixImport
        '
        Me.ButtonRetalixImport.Location = New System.Drawing.Point(273, 17)
        Me.ButtonRetalixImport.Name = "ButtonRetalixImport"
        Me.ButtonRetalixImport.Size = New System.Drawing.Size(75, 23)
        Me.ButtonRetalixImport.TabIndex = 75
        Me.ButtonRetalixImport.Text = "Process"
        '
        'tabArts
        '
        Me.tabArts.Controls.Add(Me.CheckBoxArtsEnabled)
        Me.tabArts.Controls.Add(Me.ButtonArtsImport)
        Me.tabArts.Controls.Add(Me.Label72)
        Me.tabArts.Controls.Add(Me.txtArtsNextPoll)
        Me.tabArts.Controls.Add(Me.Label77)
        Me.tabArts.Controls.Add(Me.txtArtsLastPoll)
        Me.tabArts.Controls.Add(Me.Label81)
        Me.tabArts.Controls.Add(Me.txtArtsPollInterval)
        Me.tabArts.Controls.Add(Me.txtArtsCurrentTime)
        Me.tabArts.Controls.Add(Me.Label86)
        Me.tabArts.Location = New System.Drawing.Point(4, 22)
        Me.tabArts.Name = "tabArts"
        Me.tabArts.Padding = New System.Windows.Forms.Padding(3)
        Me.tabArts.Size = New System.Drawing.Size(364, 198)
        Me.tabArts.TabIndex = 26
        Me.tabArts.Text = "ARTS"
        Me.tabArts.UseVisualStyleBackColor = True
        '
        'CheckBoxArtsEnabled
        '
        Me.CheckBoxArtsEnabled.Location = New System.Drawing.Point(21, 143)
        Me.CheckBoxArtsEnabled.Name = "CheckBoxArtsEnabled"
        Me.CheckBoxArtsEnabled.Size = New System.Drawing.Size(136, 24)
        Me.CheckBoxArtsEnabled.TabIndex = 86
        Me.CheckBoxArtsEnabled.Text = "Enable Gateway"
        '
        'ButtonArtsImport
        '
        Me.ButtonArtsImport.Location = New System.Drawing.Point(246, 31)
        Me.ButtonArtsImport.Name = "ButtonArtsImport"
        Me.ButtonArtsImport.Size = New System.Drawing.Size(98, 23)
        Me.ButtonArtsImport.TabIndex = 85
        Me.ButtonArtsImport.Text = "Process"
        '
        'Label72
        '
        Me.Label72.Location = New System.Drawing.Point(21, 119)
        Me.Label72.Name = "Label72"
        Me.Label72.Size = New System.Drawing.Size(80, 16)
        Me.Label72.TabIndex = 83
        Me.Label72.Text = "Next Poll:"
        '
        'txtArtsNextPoll
        '
        Me.txtArtsNextPoll.Location = New System.Drawing.Point(109, 119)
        Me.txtArtsNextPoll.Name = "txtArtsNextPoll"
        Me.txtArtsNextPoll.Size = New System.Drawing.Size(88, 16)
        Me.txtArtsNextPoll.TabIndex = 84
        '
        'Label77
        '
        Me.Label77.Location = New System.Drawing.Point(21, 95)
        Me.Label77.Name = "Label77"
        Me.Label77.Size = New System.Drawing.Size(80, 16)
        Me.Label77.TabIndex = 81
        Me.Label77.Text = "Last Poll:"
        '
        'txtArtsLastPoll
        '
        Me.txtArtsLastPoll.Location = New System.Drawing.Point(109, 95)
        Me.txtArtsLastPoll.Name = "txtArtsLastPoll"
        Me.txtArtsLastPoll.Size = New System.Drawing.Size(88, 16)
        Me.txtArtsLastPoll.TabIndex = 82
        '
        'Label81
        '
        Me.Label81.Location = New System.Drawing.Point(21, 71)
        Me.Label81.Name = "Label81"
        Me.Label81.Size = New System.Drawing.Size(80, 16)
        Me.Label81.TabIndex = 79
        Me.Label81.Text = "Poll Interval:"
        '
        'txtArtsPollInterval
        '
        Me.txtArtsPollInterval.Location = New System.Drawing.Point(109, 71)
        Me.txtArtsPollInterval.Name = "txtArtsPollInterval"
        Me.txtArtsPollInterval.Size = New System.Drawing.Size(88, 16)
        Me.txtArtsPollInterval.TabIndex = 80
        '
        'txtArtsCurrentTime
        '
        Me.txtArtsCurrentTime.Location = New System.Drawing.Point(109, 47)
        Me.txtArtsCurrentTime.Name = "txtArtsCurrentTime"
        Me.txtArtsCurrentTime.Size = New System.Drawing.Size(88, 16)
        Me.txtArtsCurrentTime.TabIndex = 78
        '
        'Label86
        '
        Me.Label86.Location = New System.Drawing.Point(21, 47)
        Me.Label86.Name = "Label86"
        Me.Label86.Size = New System.Drawing.Size(80, 16)
        Me.Label86.TabIndex = 77
        Me.Label86.Text = "Current Time:"
        '
        'tabPOSixml
        '
        Me.tabPOSixml.Controls.Add(Me.CheckBoxPOSixmlEnabled)
        Me.tabPOSixml.Controls.Add(Me.ButtonPOSixmlImport)
        Me.tabPOSixml.Controls.Add(Me.Label75)
        Me.tabPOSixml.Controls.Add(Me.txtPOSixmlNextPoll)
        Me.tabPOSixml.Controls.Add(Me.Label83)
        Me.tabPOSixml.Controls.Add(Me.txtPOSixmlLastPoll)
        Me.tabPOSixml.Controls.Add(Me.Label88)
        Me.tabPOSixml.Controls.Add(Me.txtPOSixmlPollInterval)
        Me.tabPOSixml.Controls.Add(Me.txtPOSixmlCurrentTime)
        Me.tabPOSixml.Controls.Add(Me.Label91)
        Me.tabPOSixml.Location = New System.Drawing.Point(4, 22)
        Me.tabPOSixml.Name = "tabPOSixml"
        Me.tabPOSixml.Padding = New System.Windows.Forms.Padding(3)
        Me.tabPOSixml.Size = New System.Drawing.Size(364, 198)
        Me.tabPOSixml.TabIndex = 27
        Me.tabPOSixml.Text = "POSixml"
        Me.tabPOSixml.UseVisualStyleBackColor = True
        '
        'CheckBoxPOSixmlEnabled
        '
        Me.CheckBoxPOSixmlEnabled.Location = New System.Drawing.Point(21, 143)
        Me.CheckBoxPOSixmlEnabled.Name = "CheckBoxPOSixmlEnabled"
        Me.CheckBoxPOSixmlEnabled.Size = New System.Drawing.Size(136, 24)
        Me.CheckBoxPOSixmlEnabled.TabIndex = 86
        Me.CheckBoxPOSixmlEnabled.Text = "Enable Gateway"
        '
        'ButtonPOSixmlImport
        '
        Me.ButtonPOSixmlImport.Location = New System.Drawing.Point(246, 31)
        Me.ButtonPOSixmlImport.Name = "ButtonPOSixmlImport"
        Me.ButtonPOSixmlImport.Size = New System.Drawing.Size(98, 23)
        Me.ButtonPOSixmlImport.TabIndex = 85
        Me.ButtonPOSixmlImport.Text = "Process"
        '
        'Label75
        '
        Me.Label75.Location = New System.Drawing.Point(21, 119)
        Me.Label75.Name = "Label75"
        Me.Label75.Size = New System.Drawing.Size(80, 16)
        Me.Label75.TabIndex = 83
        Me.Label75.Text = "Next Poll:"
        '
        'txtPOSixmlNextPoll
        '
        Me.txtPOSixmlNextPoll.Location = New System.Drawing.Point(109, 119)
        Me.txtPOSixmlNextPoll.Name = "txtPOSixmlNextPoll"
        Me.txtPOSixmlNextPoll.Size = New System.Drawing.Size(88, 16)
        Me.txtPOSixmlNextPoll.TabIndex = 84
        '
        'Label83
        '
        Me.Label83.Location = New System.Drawing.Point(21, 95)
        Me.Label83.Name = "Label83"
        Me.Label83.Size = New System.Drawing.Size(80, 16)
        Me.Label83.TabIndex = 81
        Me.Label83.Text = "Last Poll:"
        '
        'txtPOSixmlLastPoll
        '
        Me.txtPOSixmlLastPoll.Location = New System.Drawing.Point(109, 95)
        Me.txtPOSixmlLastPoll.Name = "txtPOSixmlLastPoll"
        Me.txtPOSixmlLastPoll.Size = New System.Drawing.Size(88, 16)
        Me.txtPOSixmlLastPoll.TabIndex = 82
        '
        'Label88
        '
        Me.Label88.Location = New System.Drawing.Point(21, 71)
        Me.Label88.Name = "Label88"
        Me.Label88.Size = New System.Drawing.Size(80, 16)
        Me.Label88.TabIndex = 79
        Me.Label88.Text = "Poll Interval:"
        '
        'txtPOSixmlPollInterval
        '
        Me.txtPOSixmlPollInterval.Location = New System.Drawing.Point(109, 71)
        Me.txtPOSixmlPollInterval.Name = "txtPOSixmlPollInterval"
        Me.txtPOSixmlPollInterval.Size = New System.Drawing.Size(88, 16)
        Me.txtPOSixmlPollInterval.TabIndex = 80
        '
        'txtPOSixmlCurrentTime
        '
        Me.txtPOSixmlCurrentTime.Location = New System.Drawing.Point(109, 47)
        Me.txtPOSixmlCurrentTime.Name = "txtPOSixmlCurrentTime"
        Me.txtPOSixmlCurrentTime.Size = New System.Drawing.Size(88, 16)
        Me.txtPOSixmlCurrentTime.TabIndex = 78
        '
        'Label91
        '
        Me.Label91.Location = New System.Drawing.Point(21, 47)
        Me.Label91.Name = "Label91"
        Me.Label91.Size = New System.Drawing.Size(80, 16)
        Me.Label91.TabIndex = 77
        Me.Label91.Text = "Current Time:"
        '
        'tabRPOS
        '
        Me.tabRPOS.Controls.Add(Me.CheckBoxRPOSEnabled)
        Me.tabRPOS.Controls.Add(Me.ButtonRPOSImport)
        Me.tabRPOS.Controls.Add(Me.Label78)
        Me.tabRPOS.Controls.Add(Me.txtRPOSNextPoll)
        Me.tabRPOS.Controls.Add(Me.Label89)
        Me.tabRPOS.Controls.Add(Me.txtRPOSLastPoll)
        Me.tabRPOS.Controls.Add(Me.Label92)
        Me.tabRPOS.Controls.Add(Me.txtRPOSPollInterval)
        Me.tabRPOS.Controls.Add(Me.txtRPOSCurrentTime)
        Me.tabRPOS.Controls.Add(Me.Label95)
        Me.tabRPOS.Location = New System.Drawing.Point(4, 22)
        Me.tabRPOS.Name = "tabRPOS"
        Me.tabRPOS.Padding = New System.Windows.Forms.Padding(3)
        Me.tabRPOS.Size = New System.Drawing.Size(364, 198)
        Me.tabRPOS.TabIndex = 28
        Me.tabRPOS.Text = "Radiant RPOS"
        Me.tabRPOS.UseVisualStyleBackColor = True
        '
        'CheckBoxRPOSEnabled
        '
        Me.CheckBoxRPOSEnabled.Location = New System.Drawing.Point(21, 143)
        Me.CheckBoxRPOSEnabled.Name = "CheckBoxRPOSEnabled"
        Me.CheckBoxRPOSEnabled.Size = New System.Drawing.Size(136, 24)
        Me.CheckBoxRPOSEnabled.TabIndex = 96
        Me.CheckBoxRPOSEnabled.Text = "Enable Gateway"
        '
        'ButtonRPOSImport
        '
        Me.ButtonRPOSImport.Location = New System.Drawing.Point(246, 31)
        Me.ButtonRPOSImport.Name = "ButtonRPOSImport"
        Me.ButtonRPOSImport.Size = New System.Drawing.Size(98, 23)
        Me.ButtonRPOSImport.TabIndex = 95
        Me.ButtonRPOSImport.Text = "Process"
        '
        'Label78
        '
        Me.Label78.Location = New System.Drawing.Point(21, 119)
        Me.Label78.Name = "Label78"
        Me.Label78.Size = New System.Drawing.Size(80, 16)
        Me.Label78.TabIndex = 93
        Me.Label78.Text = "Next Poll:"
        '
        'txtRPOSNextPoll
        '
        Me.txtRPOSNextPoll.Location = New System.Drawing.Point(109, 119)
        Me.txtRPOSNextPoll.Name = "txtRPOSNextPoll"
        Me.txtRPOSNextPoll.Size = New System.Drawing.Size(88, 16)
        Me.txtRPOSNextPoll.TabIndex = 94
        '
        'Label89
        '
        Me.Label89.Location = New System.Drawing.Point(21, 95)
        Me.Label89.Name = "Label89"
        Me.Label89.Size = New System.Drawing.Size(80, 16)
        Me.Label89.TabIndex = 91
        Me.Label89.Text = "Last Poll:"
        '
        'txtRPOSLastPoll
        '
        Me.txtRPOSLastPoll.Location = New System.Drawing.Point(109, 95)
        Me.txtRPOSLastPoll.Name = "txtRPOSLastPoll"
        Me.txtRPOSLastPoll.Size = New System.Drawing.Size(88, 16)
        Me.txtRPOSLastPoll.TabIndex = 92
        '
        'Label92
        '
        Me.Label92.Location = New System.Drawing.Point(21, 71)
        Me.Label92.Name = "Label92"
        Me.Label92.Size = New System.Drawing.Size(80, 16)
        Me.Label92.TabIndex = 89
        Me.Label92.Text = "Poll Interval:"
        '
        'txtRPOSPollInterval
        '
        Me.txtRPOSPollInterval.Location = New System.Drawing.Point(109, 71)
        Me.txtRPOSPollInterval.Name = "txtRPOSPollInterval"
        Me.txtRPOSPollInterval.Size = New System.Drawing.Size(88, 16)
        Me.txtRPOSPollInterval.TabIndex = 90
        '
        'txtRPOSCurrentTime
        '
        Me.txtRPOSCurrentTime.Location = New System.Drawing.Point(109, 47)
        Me.txtRPOSCurrentTime.Name = "txtRPOSCurrentTime"
        Me.txtRPOSCurrentTime.Size = New System.Drawing.Size(88, 16)
        Me.txtRPOSCurrentTime.TabIndex = 88
        '
        'Label95
        '
        Me.Label95.Location = New System.Drawing.Point(21, 47)
        Me.Label95.Name = "Label95"
        Me.Label95.Size = New System.Drawing.Size(80, 16)
        Me.Label95.TabIndex = 87
        Me.Label95.Text = "Current Time:"
        '
        'tabPosCommon
        '
        Me.tabPosCommon.Controls.Add(Me.chkEnableGatewayCommon)
        Me.tabPosCommon.Controls.Add(Me.btImportCommon)
        Me.tabPosCommon.Controls.Add(Me.Label85)
        Me.tabPosCommon.Controls.Add(Me.txtNextPoll)
        Me.tabPosCommon.Controls.Add(Me.Label93)
        Me.tabPosCommon.Controls.Add(Me.txtLastPoll)
        Me.tabPosCommon.Controls.Add(Me.Label96)
        Me.tabPosCommon.Controls.Add(Me.txtPollInterval)
        Me.tabPosCommon.Controls.Add(Me.txtCurrentTime)
        Me.tabPosCommon.Controls.Add(Me.Label99)
        Me.tabPosCommon.Location = New System.Drawing.Point(4, 22)
        Me.tabPosCommon.Name = "tabPosCommon"
        Me.tabPosCommon.Padding = New System.Windows.Forms.Padding(3)
        Me.tabPosCommon.Size = New System.Drawing.Size(364, 198)
        Me.tabPosCommon.TabIndex = 29
        Me.tabPosCommon.Text = "CommonPOS"
        Me.tabPosCommon.UseVisualStyleBackColor = True
        '
        'chkEnableGatewayCommon
        '
        Me.chkEnableGatewayCommon.Location = New System.Drawing.Point(21, 143)
        Me.chkEnableGatewayCommon.Name = "chkEnableGatewayCommon"
        Me.chkEnableGatewayCommon.Size = New System.Drawing.Size(136, 24)
        Me.chkEnableGatewayCommon.TabIndex = 106
        Me.chkEnableGatewayCommon.Text = "Enable Gateway"
        '
        'btImportCommon
        '
        Me.btImportCommon.Location = New System.Drawing.Point(246, 31)
        Me.btImportCommon.Name = "btImportCommon"
        Me.btImportCommon.Size = New System.Drawing.Size(98, 23)
        Me.btImportCommon.TabIndex = 105
        Me.btImportCommon.Text = "Process"
        '
        'Label85
        '
        Me.Label85.Location = New System.Drawing.Point(21, 119)
        Me.Label85.Name = "Label85"
        Me.Label85.Size = New System.Drawing.Size(80, 16)
        Me.Label85.TabIndex = 103
        Me.Label85.Text = "Next Poll:"
        '
        'txtNextPoll
        '
        Me.txtNextPoll.Location = New System.Drawing.Point(109, 119)
        Me.txtNextPoll.Name = "txtNextPoll"
        Me.txtNextPoll.Size = New System.Drawing.Size(88, 16)
        Me.txtNextPoll.TabIndex = 104
        '
        'Label93
        '
        Me.Label93.Location = New System.Drawing.Point(21, 95)
        Me.Label93.Name = "Label93"
        Me.Label93.Size = New System.Drawing.Size(80, 16)
        Me.Label93.TabIndex = 101
        Me.Label93.Text = "Last Poll:"
        '
        'txtLastPoll
        '
        Me.txtLastPoll.Location = New System.Drawing.Point(109, 95)
        Me.txtLastPoll.Name = "txtLastPoll"
        Me.txtLastPoll.Size = New System.Drawing.Size(88, 16)
        Me.txtLastPoll.TabIndex = 102
        '
        'Label96
        '
        Me.Label96.Location = New System.Drawing.Point(21, 71)
        Me.Label96.Name = "Label96"
        Me.Label96.Size = New System.Drawing.Size(80, 16)
        Me.Label96.TabIndex = 99
        Me.Label96.Text = "Poll Interval:"
        '
        'txtPollInterval
        '
        Me.txtPollInterval.Location = New System.Drawing.Point(109, 71)
        Me.txtPollInterval.Name = "txtPollInterval"
        Me.txtPollInterval.Size = New System.Drawing.Size(88, 16)
        Me.txtPollInterval.TabIndex = 100
        '
        'txtCurrentTime
        '
        Me.txtCurrentTime.Location = New System.Drawing.Point(109, 47)
        Me.txtCurrentTime.Name = "txtCurrentTime"
        Me.txtCurrentTime.Size = New System.Drawing.Size(88, 16)
        Me.txtCurrentTime.TabIndex = 98
        '
        'Label99
        '
        Me.Label99.Location = New System.Drawing.Point(21, 47)
        Me.Label99.Name = "Label99"
        Me.Label99.Size = New System.Drawing.Size(80, 16)
        Me.Label99.TabIndex = 97
        Me.Label99.Text = "Current Time:"
        '
        'TabSUSPOS
        '
        Me.TabSUSPOS.Controls.Add(Me.CheckBox5)
        Me.TabSUSPOS.Controls.Add(Me.Button11)
        Me.TabSUSPOS.Controls.Add(Me.Label90)
        Me.TabSUSPOS.Controls.Add(Me.Label94)
        Me.TabSUSPOS.Controls.Add(Me.Label97)
        Me.TabSUSPOS.Controls.Add(Me.Label98)
        Me.TabSUSPOS.Controls.Add(Me.Label100)
        Me.TabSUSPOS.Controls.Add(Me.Label101)
        Me.TabSUSPOS.Controls.Add(Me.Label102)
        Me.TabSUSPOS.Controls.Add(Me.Label103)
        Me.TabSUSPOS.Location = New System.Drawing.Point(4, 22)
        Me.TabSUSPOS.Name = "TabSUSPOS"
        Me.TabSUSPOS.Padding = New System.Windows.Forms.Padding(3)
        Me.TabSUSPOS.Size = New System.Drawing.Size(364, 198)
        Me.TabSUSPOS.TabIndex = 30
        Me.TabSUSPOS.Text = "SUS POS"
        Me.TabSUSPOS.UseVisualStyleBackColor = True
        '
        'CheckBox5
        '
        Me.CheckBox5.Location = New System.Drawing.Point(21, 143)
        Me.CheckBox5.Name = "CheckBox5"
        Me.CheckBox5.Size = New System.Drawing.Size(136, 24)
        Me.CheckBox5.TabIndex = 116
        Me.CheckBox5.Text = "Enable Gateway"
        '
        'Button11
        '
        Me.Button11.Location = New System.Drawing.Point(246, 31)
        Me.Button11.Name = "Button11"
        Me.Button11.Size = New System.Drawing.Size(98, 23)
        Me.Button11.TabIndex = 115
        Me.Button11.Text = "Process"
        '
        'Label90
        '
        Me.Label90.Location = New System.Drawing.Point(21, 119)
        Me.Label90.Name = "Label90"
        Me.Label90.Size = New System.Drawing.Size(80, 16)
        Me.Label90.TabIndex = 113
        Me.Label90.Text = "Next Poll:"
        '
        'Label94
        '
        Me.Label94.Location = New System.Drawing.Point(109, 119)
        Me.Label94.Name = "Label94"
        Me.Label94.Size = New System.Drawing.Size(88, 16)
        Me.Label94.TabIndex = 114
        '
        'Label97
        '
        Me.Label97.Location = New System.Drawing.Point(21, 95)
        Me.Label97.Name = "Label97"
        Me.Label97.Size = New System.Drawing.Size(80, 16)
        Me.Label97.TabIndex = 111
        Me.Label97.Text = "Last Poll:"
        '
        'Label98
        '
        Me.Label98.Location = New System.Drawing.Point(109, 95)
        Me.Label98.Name = "Label98"
        Me.Label98.Size = New System.Drawing.Size(88, 16)
        Me.Label98.TabIndex = 112
        '
        'Label100
        '
        Me.Label100.Location = New System.Drawing.Point(21, 71)
        Me.Label100.Name = "Label100"
        Me.Label100.Size = New System.Drawing.Size(80, 16)
        Me.Label100.TabIndex = 109
        Me.Label100.Text = "Poll Interval:"
        '
        'Label101
        '
        Me.Label101.Location = New System.Drawing.Point(109, 71)
        Me.Label101.Name = "Label101"
        Me.Label101.Size = New System.Drawing.Size(88, 16)
        Me.Label101.TabIndex = 110
        '
        'Label102
        '
        Me.Label102.Location = New System.Drawing.Point(109, 47)
        Me.Label102.Name = "Label102"
        Me.Label102.Size = New System.Drawing.Size(88, 16)
        Me.Label102.TabIndex = 108
        '
        'Label103
        '
        Me.Label103.Location = New System.Drawing.Point(21, 47)
        Me.Label103.Name = "Label103"
        Me.Label103.Size = New System.Drawing.Size(80, 16)
        Me.Label103.TabIndex = 107
        Me.Label103.Text = "Current Time:"
        '
        'Timer1
        '
        Me.Timer1.Interval = 60000
        '
        'TextBoxAccessCode
        '
        Me.TextBoxAccessCode.Location = New System.Drawing.Point(392, 248)
        Me.TextBoxAccessCode.Name = "TextBoxAccessCode"
        Me.TextBoxAccessCode.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.TextBoxAccessCode.Size = New System.Drawing.Size(92, 20)
        Me.TextBoxAccessCode.TabIndex = 3
        '
        'btnAccessCode
        '
        Me.btnAccessCode.Location = New System.Drawing.Point(392, 224)
        Me.btnAccessCode.Name = "btnAccessCode"
        Me.btnAccessCode.Size = New System.Drawing.Size(92, 23)
        Me.btnAccessCode.TabIndex = 15
        Me.btnAccessCode.Text = "Access Code"
        '
        'Button7
        '
        Me.Button7.Location = New System.Drawing.Point(392, 3)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(92, 23)
        Me.Button7.TabIndex = 4
        Me.Button7.Text = "Minimise"
        '
        'btnEndOfDay
        '
        Me.btnEndOfDay.Location = New System.Drawing.Point(392, 128)
        Me.btnEndOfDay.Name = "btnEndOfDay"
        Me.btnEndOfDay.Size = New System.Drawing.Size(92, 60)
        Me.btnEndOfDay.TabIndex = 2
        Me.btnEndOfDay.Text = "End of Day"
        Me.btnEndOfDay.Visible = False
        '
        'LabelEndOfDayStatus
        '
        Me.LabelEndOfDayStatus.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelEndOfDayStatus.ForeColor = System.Drawing.Color.Purple
        Me.LabelEndOfDayStatus.Location = New System.Drawing.Point(388, 196)
        Me.LabelEndOfDayStatus.Name = "LabelEndOfDayStatus"
        Me.LabelEndOfDayStatus.Size = New System.Drawing.Size(100, 20)
        Me.LabelEndOfDayStatus.TabIndex = 18
        Me.LabelEndOfDayStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'tmrCommsBackoff
        '
        Me.tmrCommsBackoff.Enabled = True
        Me.tmrCommsBackoff.Interval = 60000
        '
        'tmrSessionLease
        '
        Me.tmrSessionLease.Enabled = True
        Me.tmrSessionLease.Interval = 60000
        '
        'NotifyIcon1
        '
        Me.NotifyIcon1.ContextMenu = Me.menuTrayIcon
        Me.NotifyIcon1.Icon = CType(resources.GetObject("NotifyIcon1.Icon"), System.Drawing.Icon)
        Me.NotifyIcon1.Text = "MacromatiX LiveLink"
        Me.NotifyIcon1.Visible = True
        '
        'menuTrayIcon
        '
        Me.menuTrayIcon.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItemTrayRunTimeClock, Me.MenuItemTrayRunComprisActive, Me.MenuItemTrayYISController, Me.MenuItemTrayZKViewUsers, Me.mnuClockSpacer, Me.mnuShowLiveLink, Me.MenuItem3, Me.mnuTrayExit})
        '
        'MenuItemTrayRunTimeClock
        '
        Me.MenuItemTrayRunTimeClock.Index = 0
        Me.MenuItemTrayRunTimeClock.Text = "&Run Easy Clock"
        Me.MenuItemTrayRunTimeClock.Visible = False
        '
        'MenuItemTrayRunComprisActive
        '
        Me.MenuItemTrayRunComprisActive.Index = 1
        Me.MenuItemTrayRunComprisActive.Text = "&Run Compris Active"
        Me.MenuItemTrayRunComprisActive.Visible = False
        '
        'MenuItemTrayYISController
        '
        Me.MenuItemTrayYISController.Index = 2
        Me.MenuItemTrayYISController.Text = "&Run YIS Controller"
        Me.MenuItemTrayYISController.Visible = False
        '
        'MenuItemTrayZKViewUsers
        '
        Me.MenuItemTrayZKViewUsers.Index = 3
        Me.MenuItemTrayZKViewUsers.Text = "&View Clock Users"
        Me.MenuItemTrayZKViewUsers.Visible = False
        '
        'mnuClockSpacer
        '
        Me.mnuClockSpacer.Index = 4
        Me.mnuClockSpacer.Text = "-"
        '
        'mnuShowLiveLink
        '
        Me.mnuShowLiveLink.Index = 5
        Me.mnuShowLiveLink.Text = "&Show Live Link"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 6
        Me.MenuItem3.Text = "-"
        '
        'mnuTrayExit
        '
        Me.mnuTrayExit.Enabled = False
        Me.mnuTrayExit.Index = 7
        Me.mnuTrayExit.Text = "E&xit"
        '
        'ImageListStatus
        '
        Me.ImageListStatus.ImageStream = CType(resources.GetObject("ImageListStatus.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageListStatus.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageListStatus.Images.SetKeyName(0, "")
        Me.ImageListStatus.Images.SetKeyName(1, "")
        Me.ImageListStatus.Images.SetKeyName(2, "")
        '
        'tmrPing
        '
        '
        'PictureBoxBackoff
        '
        Me.PictureBoxBackoff.Image = Global.LiveLink.My.Resources.Resources.Backedoff
        Me.PictureBoxBackoff.Location = New System.Drawing.Point(332, 6)
        Me.PictureBoxBackoff.Name = "PictureBoxBackoff"
        Me.PictureBoxBackoff.Size = New System.Drawing.Size(16, 16)
        Me.PictureBoxBackoff.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.PictureBoxBackoff.TabIndex = 15
        Me.PictureBoxBackoff.TabStop = False
        Me.PictureBoxBackoff.Visible = False
        '
        'PictureBoxPingEnabled
        '
        Me.PictureBoxPingEnabled.Image = Global.LiveLink.My.Resources.Resources.Ping1
        Me.PictureBoxPingEnabled.Location = New System.Drawing.Point(361, 6)
        Me.PictureBoxPingEnabled.Name = "PictureBoxPingEnabled"
        Me.PictureBoxPingEnabled.Size = New System.Drawing.Size(16, 16)
        Me.PictureBoxPingEnabled.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.PictureBoxPingEnabled.TabIndex = 16
        Me.PictureBoxPingEnabled.TabStop = False
        Me.PictureBoxPingEnabled.Tag = "Ping1"
        Me.PictureBoxPingEnabled.Visible = False
        '
        'PictureBoxOff
        '
        Me.PictureBoxOff.Image = CType(resources.GetObject("PictureBoxOff.Image"), System.Drawing.Image)
        Me.PictureBoxOff.Location = New System.Drawing.Point(396, 71)
        Me.PictureBoxOff.Name = "PictureBoxOff"
        Me.PictureBoxOff.Size = New System.Drawing.Size(18, 18)
        Me.PictureBoxOff.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.PictureBoxOff.TabIndex = 2
        Me.PictureBoxOff.TabStop = False
        '
        'PictureBoxON
        '
        Me.PictureBoxON.Image = CType(resources.GetObject("PictureBoxON.Image"), System.Drawing.Image)
        Me.PictureBoxON.Location = New System.Drawing.Point(396, 71)
        Me.PictureBoxON.Name = "PictureBoxON"
        Me.PictureBoxON.Size = New System.Drawing.Size(18, 18)
        Me.PictureBoxON.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.PictureBoxON.TabIndex = 3
        Me.PictureBoxON.TabStop = False
        Me.PictureBoxON.Visible = False
        '
        'Button2
        '
        Me.Button2.Enabled = False
        Me.Button2.Location = New System.Drawing.Point(182, 157)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(160, 23)
        Me.Button2.TabIndex = 92
        Me.Button2.Text = "Update Firmware"
        '
        'TextBox2
        '
        Me.TextBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox2.Location = New System.Drawing.Point(251, 91)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.ReadOnly = True
        Me.TextBox2.Size = New System.Drawing.Size(91, 20)
        Me.TextBox2.TabIndex = 91
        Me.TextBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TextBox5
        '
        Me.TextBox5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox5.Location = New System.Drawing.Point(251, 68)
        Me.TextBox5.Name = "TextBox5"
        Me.TextBox5.ReadOnly = True
        Me.TextBox5.Size = New System.Drawing.Size(91, 20)
        Me.TextBox5.TabIndex = 90
        Me.TextBox5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label53
        '
        Me.Label53.Location = New System.Drawing.Point(179, 95)
        Me.Label53.Name = "Label53"
        Me.Label53.Size = New System.Drawing.Size(66, 16)
        Me.Label53.TabIndex = 89
        Me.Label53.Text = "Device Port:"
        '
        'Label54
        '
        Me.Label54.Location = New System.Drawing.Point(179, 72)
        Me.Label54.Name = "Label54"
        Me.Label54.Size = New System.Drawing.Size(66, 16)
        Me.Label54.TabIndex = 87
        Me.Label54.Text = "Device IP:"
        '
        'CheckBox2
        '
        Me.CheckBox2.Location = New System.Drawing.Point(24, 120)
        Me.CheckBox2.Name = "CheckBox2"
        Me.CheckBox2.Size = New System.Drawing.Size(108, 24)
        Me.CheckBox2.TabIndex = 86
        Me.CheckBox2.Text = "Enable Gateway"
        '
        'Button6
        '
        Me.Button6.Location = New System.Drawing.Point(251, 24)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(91, 23)
        Me.Button6.TabIndex = 85
        Me.Button6.Text = "Process"
        '
        'Label55
        '
        Me.Label55.Location = New System.Drawing.Point(24, 96)
        Me.Label55.Name = "Label55"
        Me.Label55.Size = New System.Drawing.Size(80, 16)
        Me.Label55.TabIndex = 83
        Me.Label55.Text = "Next Poll:"
        '
        'Label56
        '
        Me.Label56.Location = New System.Drawing.Point(112, 96)
        Me.Label56.Name = "Label56"
        Me.Label56.Size = New System.Drawing.Size(66, 16)
        Me.Label56.TabIndex = 84
        '
        'Label57
        '
        Me.Label57.Location = New System.Drawing.Point(24, 72)
        Me.Label57.Name = "Label57"
        Me.Label57.Size = New System.Drawing.Size(80, 16)
        Me.Label57.TabIndex = 81
        Me.Label57.Text = "Last Poll:"
        '
        'Label58
        '
        Me.Label58.Location = New System.Drawing.Point(112, 72)
        Me.Label58.Name = "Label58"
        Me.Label58.Size = New System.Drawing.Size(66, 16)
        Me.Label58.TabIndex = 82
        '
        'Label59
        '
        Me.Label59.Location = New System.Drawing.Point(24, 48)
        Me.Label59.Name = "Label59"
        Me.Label59.Size = New System.Drawing.Size(80, 16)
        Me.Label59.TabIndex = 79
        Me.Label59.Text = "Poll Interval:"
        '
        'Label60
        '
        Me.Label60.Location = New System.Drawing.Point(112, 47)
        Me.Label60.Name = "Label60"
        Me.Label60.Size = New System.Drawing.Size(66, 16)
        Me.Label60.TabIndex = 80
        '
        'Label61
        '
        Me.Label61.Location = New System.Drawing.Point(112, 24)
        Me.Label61.Name = "Label61"
        Me.Label61.Size = New System.Drawing.Size(66, 16)
        Me.Label61.TabIndex = 78
        '
        'Label62
        '
        Me.Label62.Location = New System.Drawing.Point(24, 24)
        Me.Label62.Name = "Label62"
        Me.Label62.Size = New System.Drawing.Size(80, 16)
        Me.Label62.TabIndex = 77
        Me.Label62.Text = "Current Time:"
        '
        'minimiseTimer
        '
        '
        'tabTrafficData
        '
        Me.tabTrafficData.Controls.Add(Me.CheckBox6)
        Me.tabTrafficData.Controls.Add(Me.btnTrafficDataProcess)
        Me.tabTrafficData.Controls.Add(Me.Label104)
        Me.tabTrafficData.Controls.Add(Me.Label105)
        Me.tabTrafficData.Controls.Add(Me.Label106)
        Me.tabTrafficData.Controls.Add(Me.Label107)
        Me.tabTrafficData.Controls.Add(Me.Label108)
        Me.tabTrafficData.Controls.Add(Me.Label109)
        Me.tabTrafficData.Controls.Add(Me.Label110)
        Me.tabTrafficData.Controls.Add(Me.Label111)
        Me.tabTrafficData.Location = New System.Drawing.Point(4, 22)
        Me.tabTrafficData.Name = "tabTrafficData"
        Me.tabTrafficData.Padding = New System.Windows.Forms.Padding(3)
        Me.tabTrafficData.Size = New System.Drawing.Size(364, 198)
        Me.tabTrafficData.TabIndex = 31
        Me.tabTrafficData.Text = "Traffic Data"
        Me.tabTrafficData.UseVisualStyleBackColor = True
        '
        'CheckBox6
        '
        Me.CheckBox6.Location = New System.Drawing.Point(21, 143)
        Me.CheckBox6.Name = "CheckBox6"
        Me.CheckBox6.Size = New System.Drawing.Size(136, 24)
        Me.CheckBox6.TabIndex = 116
        Me.CheckBox6.Text = "Enable Gateway"
        '
        'btnTrafficDataProcess
        '
        Me.btnTrafficDataProcess.Location = New System.Drawing.Point(246, 31)
        Me.btnTrafficDataProcess.Name = "btnTrafficDataProcess"
        Me.btnTrafficDataProcess.Size = New System.Drawing.Size(98, 23)
        Me.btnTrafficDataProcess.TabIndex = 115
        Me.btnTrafficDataProcess.Text = "Process"
        '
        'Label104
        '
        Me.Label104.Location = New System.Drawing.Point(21, 119)
        Me.Label104.Name = "Label104"
        Me.Label104.Size = New System.Drawing.Size(80, 16)
        Me.Label104.TabIndex = 113
        Me.Label104.Text = "Data Directory:"
        '
        'Label105
        '
        Me.Label105.Location = New System.Drawing.Point(109, 119)
        Me.Label105.Name = "Label105"
        Me.Label105.Size = New System.Drawing.Size(255, 16)
        Me.Label105.TabIndex = 114
        '
        'Label106
        '
        Me.Label106.Location = New System.Drawing.Point(21, 95)
        Me.Label106.Name = "Label106"
        Me.Label106.Size = New System.Drawing.Size(80, 16)
        Me.Label106.TabIndex = 111
        Me.Label106.Text = "Last Poll:"
        '
        'Label107
        '
        Me.Label107.Location = New System.Drawing.Point(109, 95)
        Me.Label107.Name = "Label107"
        Me.Label107.Size = New System.Drawing.Size(88, 16)
        Me.Label107.TabIndex = 112
        '
        'Label108
        '
        Me.Label108.Location = New System.Drawing.Point(21, 71)
        Me.Label108.Name = "Label108"
        Me.Label108.Size = New System.Drawing.Size(80, 16)
        Me.Label108.TabIndex = 109
        Me.Label108.Text = "Poll Interval:"
        '
        'Label109
        '
        Me.Label109.Location = New System.Drawing.Point(109, 71)
        Me.Label109.Name = "Label109"
        Me.Label109.Size = New System.Drawing.Size(88, 16)
        Me.Label109.TabIndex = 110
        '
        'Label110
        '
        Me.Label110.Location = New System.Drawing.Point(109, 47)
        Me.Label110.Name = "Label110"
        Me.Label110.Size = New System.Drawing.Size(131, 16)
        Me.Label110.TabIndex = 108
        '
        'Label111
        '
        Me.Label111.Location = New System.Drawing.Point(21, 47)
        Me.Label111.Name = "Label111"
        Me.Label111.Size = New System.Drawing.Size(80, 16)
        Me.Label111.TabIndex = 107
        Me.Label111.Text = "Current Time:"
        '
        'LiveLinkMain
        '
        Me.AcceptButton = Me.btnProcessNow
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(492, 279)
        Me.Controls.Add(Me.PictureBoxBackoff)
        Me.Controls.Add(Me.PictureBoxPingEnabled)
        Me.Controls.Add(Me.LabelEndOfDayStatus)
        Me.Controls.Add(Me.btnEndOfDay)
        Me.Controls.Add(Me.Button7)
        Me.Controls.Add(Me.btnAccessCode)
        Me.Controls.Add(Me.TextBoxAccessCode)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.btnProcessNow)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.LabelStatus)
        Me.Controls.Add(Me.PictureBoxOff)
        Me.Controls.Add(Me.LabelTitle)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.PictureBoxON)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Menu = Me.MainMenu1
        Me.Name = "LiveLinkMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "MacromatiX eRetailer - LiveLink"
        Me.TabControl1.ResumeLayout(False)
        Me.tabStatus.ResumeLayout(False)
        Me.tabStatus.PerformLayout()
        Me.tabIntouch.ResumeLayout(False)
        Me.tabOctane.ResumeLayout(False)
        Me.tabAloha.ResumeLayout(False)
        Me.tabAloha.PerformLayout()
        Me.tabPanasonic.ResumeLayout(False)
        Me.tabSchedule.ResumeLayout(False)
        Me.tabSchedule.PerformLayout()
        Me.tabMicros.ResumeLayout(False)
        Me.tabSetup.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.tabCompris.ResumeLayout(False)
        Me.GroupComprisPollFiles.ResumeLayout(False)
        Me.GroupComprisPDPUpdates.ResumeLayout(False)
        Me.GroupComprisPDP.ResumeLayout(False)
        Me.GroupComprisPollingInformation.ResumeLayout(False)
        Me.tabPOSI.ResumeLayout(False)
        Me.tabPOSI.PerformLayout()
        Me.tabTest.ResumeLayout(False)
        Me.tabTest.PerformLayout()
        CType(Me.DataGrid1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabPAR.ResumeLayout(False)
        Me.tabPAR.PerformLayout()
        Me.tabSetPrice.ResumeLayout(False)
        Me.TabControl2.ResumeLayout(False)
        Me.tabSetPriceDCA.ResumeLayout(False)
        Me.tabSetPriceDCA.PerformLayout()
        Me.tabSetPricePanasonic.ResumeLayout(False)
        Me.tabSetPricePAR.ResumeLayout(False)
        Me.tabOfflineCashManager.ResumeLayout(False)
        Me.tabZKFingerprint.ResumeLayout(False)
        Me.tabZKFingerprint.PerformLayout()
        Me.tabICG.ResumeLayout(False)
        Me.tabRMS.ResumeLayout(False)
        Me.tabNewPOS.ResumeLayout(False)
        Me.tabProxy.ResumeLayout(False)
        Me.tabProxy.PerformLayout()
        Me.grpProxy.ResumeLayout(False)
        Me.grpProxy.PerformLayout()
        Me.grpProxyAuth.ResumeLayout(False)
        Me.grpProxyAuth.PerformLayout()
        Me.tabiPOS.ResumeLayout(False)
        Me.grpiPOSInformation.ResumeLayout(False)
        Me.grpiPOSProcess.ResumeLayout(False)
        Me.grpiPOSUpdates.ResumeLayout(False)
        Me.tabUniwell.ResumeLayout(False)
        Me.tabRadiant.ResumeLayout(False)
        Me.tabNAXML.ResumeLayout(False)
        Me.tabBreeze.ResumeLayout(False)
        Me.BreezeProcessGroup.ResumeLayout(False)
        Me.tabXpient.ResumeLayout(False)
        Me.tabRetalix.ResumeLayout(False)
        Me.tabArts.ResumeLayout(False)
        Me.tabPOSixml.ResumeLayout(False)
        Me.tabRPOS.ResumeLayout(False)
        Me.tabPosCommon.ResumeLayout(False)
        Me.TabSUSPOS.ResumeLayout(False)
        CType(Me.PictureBoxBackoff, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBoxPingEnabled, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBoxOff, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBoxON, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabTrafficData.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region

#Region " Command Line Parsing"


    Private Sub ParseCommandLine(ByVal args As String(), ByVal commandLine As String)
        Try
            ' check if there are any command line parameters
            If (args Is Nothing OrElse args.Length <= 1) Then
                ' if no arguments then don't parse anything
                Return
            End If

            Dim firstArgument As Boolean = True
            For Each argumentValue As String In args
                ' skip the first argument, which is always the executable name
                If (firstArgument) Then
                    firstArgument = False
                    Continue For
                End If

                ' check argument is a valid value
                If (String.IsNullOrEmpty(argumentValue)) Then
                    Continue For
                End If

                ' check command line argument at least match "/X" (all arguments need to match this format)
                If (argumentValue.Length < 2) Then
                    LogEvent(EventLogEntryType.Error, "Invalid command line: {0}{1}{1}Usage: {2}{1}", commandLine, vbCrLf, _commandLineUsage)
                    Return
                End If

                ' get the option value "/X" from the value
                Dim argumentType As String = argumentValue.Substring(0, 2)
                If (String.IsNullOrEmpty(argumentType)) Then
                    LogEvent(EventLogEntryType.Error, "Invalid command line: {0}{1}{1}Usage: {2}{1}", commandLine, vbCrLf, _commandLineUsage)
                    Return
                End If

                ' match argument type with available options
                Select Case (argumentType.ToLower())
                    Case EnumUtils(Of CommandLineArguments).GetClassName(CommandLineArguments.StoreNumber)
                        ' store the command line value for the store number
                        _argumentStoreNumber = GetCommandLineArgumentValue(argumentValue, CommandLineArguments.StoreNumber)

                    Case Else
                        ' on unknown option log an error
                        LogEvent(EventLogEntryType.Error, "Invalid command line option: {0}{1}{1}Usage: {2}{1}", argumentType, vbCrLf, _commandLineUsage)
                        Return
                End Select
            Next

        Catch ex As Exception
            ' on exception log an error
            LogEvent(EventLogEntryType.Error, "Exception occurred parsing command line: {0}{1}Exception:{2}{1}{1}Usage: {3}{1}", commandLine, vbCrLf, ex.Message, _commandLineUsage)
        End Try
    End Sub

    Private Function GetCommandLineArgumentValue(ByVal argumentValue As String, ByVal argumentType As CommandLineArguments, Optional ByVal defaultValue As String = "") As String
        Dim replaceValue As String = EnumUtils(Of CommandLineArguments).GetClassName(CommandLineArguments.StoreNumber)
        If (String.IsNullOrEmpty(replaceValue)) Then
            Return defaultValue
        End If

        ' create the replacement string
        replaceValue += ":"
        If (argumentValue.Contains(replaceValue)) Then
            Return argumentValue.Replace(replaceValue, "")
        End If
        ' return a default value of empty string
        Return defaultValue
    End Function


#End Region

#Region " Application Startup and Shutdown "

    Private Sub LiveLinkMain_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Try
            Select Case e.CloseReason
                Case CloseReason.TaskManagerClosing, _
                 CloseReason.WindowsShutDown, CloseReason.ApplicationExitCall
                    ' allow closing of livelink from the task manager, and on windows shut down
                    LogEvent("LiveLink shutting down - " & e.CloseReason.ToString, EventLogEntryType.Information)
                    Application.Exit()

                Case Else
                    ' any other reason for closing should not be allowed unless the service has been stopped and the access code has been entered
                    If Not ((LabelStatus.Text = "Stopped") And (UserAccessStatus)) Then
                        LogEvent("Attempted to shut down LiveLink - Not Authorised - " & e.CloseReason.ToString, EventLogEntryType.Information)
                        Me.WindowState = FormWindowState.Minimized
                        e.Cancel = True
                    Else
                        LogEvent("LiveLink shutting down - Authorised - " & e.CloseReason.ToString, EventLogEntryType.Information)
                        Application.Exit()
                    End If
            End Select

        Catch ex As Exception
            LogEvent("LiveLink exception while closing - " & e.CloseReason.ToString & " - " & ex.Message, EventLogEntryType.Error)
        End Try
    End Sub

    Private Sub RemoveLiveLinkShortCuts(ByVal path As String)
        ' Remove any shortcut files (with a .lnk extension that contain the word livelink)
        Try
            Dim di As New DirectoryInfo(path)
            Dim fi As FileInfo() = di.GetFiles("*livelink*.lnk")
            For Each f As FileInfo In fi
                If f.Name.ToLower.Contains("livelink") Then
                    f.Delete()
                End If
            Next
        Catch ex As Exception
            ' Do nothing
        End Try
    End Sub

    Private Sub EnsureLiveLinkAutoStarts(Optional ByVal AutoStart As Boolean = True)
        ' Add LiveLink.exe to the registry so that it will auto start.
        ' add to the HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Run key
        Const cLiveLink As String = "MacromatiX LiveLink"
        Const cSubKey As String = "Software\Microsoft\Windows\CurrentVersion\Run"
        Dim hive As RegistryKey = Registry.LocalMachine
        Dim appPath As String = String.Empty

        If AutoStart Then
            appPath = RegRead(hive, cSubKey, cLiveLink)
            If appPath = String.Empty OrElse appPath <> Application.ExecutablePath Then
                If RegWrite(hive, cSubKey, cLiveLink, Application.ExecutablePath) Then
                    LogEvent("LiveLink set to autostart in registry", EventLogEntryType.Information)
                    ' Now remove any shortcuts in the Startup folders
                    Dim startupFolder As String = Environment.GetFolderPath(Environment.SpecialFolder.Startup)
                    ' Remove from current user's profile
                    RemoveLiveLinkShortCuts(startupFolder)
                    ' Remove from All User's profile
                    RemoveLiveLinkShortCuts(startupFolder.ToLower.Replace("\" & Environment.UserName.ToLower & "\", "\All Users\"))
                Else
                    LogEvent("Unable to set LiveLink to autostart in registry", EventLogEntryType.Warning)
                End If
            End If
        Else
            If RegRead(hive, cSubKey, cLiveLink) <> String.Empty Then
                If RegDeleteValue(hive, cSubKey, cLiveLink) Then
                    LogEvent("LiveLink autostart disabled in registry", EventLogEntryType.Information)
                Else
                    LogEvent("Unable to disable LiveLink autostart in registry", EventLogEntryType.Warning)
                End If
            End If
        End If
        chkAutoStartOnLogin.Checked = AutoStart
        If Not AutoStart Then
            SetStatus("Warning ...", "LiveLink is not set to auto start after reboot")
        End If
    End Sub

    Private Sub SetStatusController(ByVal status As String)
        SetStatus("", status)
    End Sub

    Private Sub LogStatusController(ByVal type As String, ByVal detail As String)
        SetStatus(type, detail)
    End Sub

    Private Sub LiveLinkMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If (Not ConfigurationHelper.IsBaseConfigured()) Then
            Dim cm As New Mx.Configuration.ConfigMigrator(Path.Combine(Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath), "LiveLink.exe.config"))
            AddHandler cm.MigrationStep, AddressOf LogConfigMigration
            cm.MigrateAppSettings(Path.Combine(Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath), "appSettings.config"))
            LogEvent("LiveLink configuration has been migrated to new format. See trace.log  in livelink directory for details.", EventLogEntryType.Information)
            Close()
            Dispose()
            RestartLiveLink()
            Return
        End If

        UpdateAppsettings()

        _livelinkCore = New LiveLinkCore(Nothing, "", "", New StatusController(AddressOf SetStatusController), New LogController(AddressOf LogStatusController))

        ' Check that LiveLink is running as the specified user
        ' This is for Windows XP environments where there are multiple users setup with different permissions that can be logged on simultaneously
        If Not String.IsNullOrEmpty(LiveLinkConfiguration.RestrictToUser.Trim) Then
            Dim windowsUserName() As String = GetCurrent.Name.Split("\"c)
            ' Strip off the machine and/or domain name
            If windowsUserName(windowsUserName.GetUpperBound(0)).Trim.ToLower <> LiveLinkConfiguration.RestrictToUser.Trim.ToLower Then
                LogEvent(String.Format("Live Link shutting down - not running as restricted user [{0}]", LiveLinkConfiguration.RestrictToUser), EventLogEntryType.Warning)
                EndApplication(True)
                Exit Sub
            End If
        End If
        If PrevInstance() Then
            ' To ensure that LiveLink is not restarting, pause for 3 seconds and then try again
            ' Set the labelStatus and the UserAcessStatus to prevent the Closing event from cancelling the close.
            Thread.Sleep(3000)
            If PrevInstance() Then
                LogEvent("Live Link shutting down - already running", EventLogEntryType.Warning)
                NotifyIcon1.ShowBalloonTip(0, "LiveLink already running", "There is another instance of LiveLink already running on this PC.  This instance is now shutting down.", ToolTipIcon.Info)
                Thread.Sleep(5000)
                EndApplication(True)
                Exit Sub
            End If
        End If

        '(C) MacromatiX Pty Ltd 2000-2012
        Dim requiredStartUpDelaySeconds As Integer = LiveLinkConfiguration.StartUpDelaySeconds
        If requiredStartUpDelaySeconds > 0 Then
            Thread.Sleep(requiredStartUpDelaySeconds * 1000)
        End If

        MyConnectionString = LiveLinkConfiguration.ConnectionString
        myWebService = LiveLinkConfiguration.WebService_URL

        ' this option sets livelink to restart over a configurable number of days
        '
        ' 0=NEVER RESTART, 1=EVERY ONE DAY, 2=EVERY TWO DAYS etc...
        '
        ' this option added to free ODBC resources used when processing Aloha data
        '
        LiveLinkRestartFrequency = LiveLinkConfiguration.RestartFrequency


        MyTimeClockAvailable = LiveLinkConfiguration.TimeClockAvailable
        If MyTimeClockAvailable = True Then
            MenuItemRunTimeClock.Visible = True
            MenuItemTrayRunTimeClock.Visible = True
            mnuClockSpacer.Visible = True
            MyTimeClockSyncInterval = LiveLinkConfiguration.TimeClockSyncInterval
            MyTimeClockLoginWindow = LiveLinkConfiguration.TimeClockLoginWindow
            MyTimeClockAllowBreaks = LiveLinkConfiguration.TimeClockAllowBreaks
            MyTimeClockAllowNewEmployees = LiveLinkConfiguration.TimeClockAllowNewEmployees
            MyTimeClockAllowClose = LiveLinkConfiguration.TimeClockAllowClose
            MyTimeClockAutoShow = LiveLinkConfiguration.TimeClockAutoShow
        Else
            MenuItemRunTimeClock.Visible = False
            MenuItemTrayRunTimeClock.Visible = False
            mnuClockSpacer.Visible = False
        End If

        ' get logo brandings... image, and back drop colour if required
        MyLogoImage = LiveLinkConfiguration.LogoImage
        MyLogoColor = LiveLinkConfiguration.LogoColor

        LabelTitle.Text = LiveLinkConfiguration.Brand
        Me.Text = LabelTitle.Text & " - LiveLink"
        Me.WindowState = FormWindowState.Minimized

        Me.ShowInTaskbar = False
        Me.WindowState = FormWindowState.Minimized
        LaunchHidden = True
        NotifyIcon1.Visible = True


        TextBoxAwake.Text = LiveLinkConfiguration.AwakeInterval

        ' Setup flow control
        myFlowTxInBatch = LiveLinkConfiguration.TransactionsInBatch
        myFlowIterations = LiveLinkConfiguration.BatchSize
        myFlowMaxRecords = LiveLinkConfiguration.MaxRecords
        myFlowInterval = LiveLinkConfiguration.SendIntervalMinutes
        txtFlowInterval.Text = myFlowInterval
        myAwakeInterval = LiveLinkConfiguration.AwakeInterval
        TextBoxAwake.Text = myAwakeInterval

        ' The number of awake iterations before sending a verbose awake message 
        ' (eg if interval = 30 minutes and counter = 8 then verbose message will be sent every 4 hours
        myAwakeCounterVerbose = Math.Max(1, LiveLinkConfiguration.AwakeCounterVerbose)

        UseCompression = LiveLinkConfiguration.UseCompression

        TokenMode = LiveLinkConfiguration.TokenMode

        ' PRE-SEND PROCESSING OPTION
        MyPreSendProcess = LiveLinkConfiguration.PreSendProcess

        SetDebugLevel()

        TabControl1.TabPages.Remove(tabTest) ' Remove the TEST tab
        TabControl1.SelectedIndex = TabControl1.TabPages.IndexOf(tabStatus)

        MyDontProcessBefore = LiveLinkConfiguration.DontProcessBefore

        ' PAR GATEWAY
        '---------------
        If LiveLinkConfiguration.PAREnabled Then
            MyParEnabled = True
            btnEndOfDay.Visible = True
            CheckBoxParEnabled.Checked = LiveLinkConfiguration.PARGatewayEnabled
            MyParGetTLDExe = LiveLinkConfiguration.PARGetTLDExe
            MyParGetAllTLDsExe = LiveLinkConfiguration.PARGetAllTLDsExe
            MyParImportFilePath = LiveLinkConfiguration.PARImportFilePath
            If Not Strings.Right(MyParImportFilePath, 1) = "\" Then
                MyParImportFilePath &= "\"
            End If
            Try ' Create the TLD Import directory
                If Not Directory.Exists(MyParImportFilePath) Then
                    Directory.CreateDirectory(MyParImportFilePath)
                End If
            Catch ex As Exception
            End Try
            MyParArchiveFolderPath = LiveLinkConfiguration.PARArchiveFolderPath
            If Not Strings.Right(MyParArchiveFolderPath, 1) = "\" Then
                MyParArchiveFolderPath &= "\"
            End If
            Try ' Create the TLD Archive directory
                If Not Directory.Exists(MyParArchiveFolderPath) Then
                    Directory.CreateDirectory(MyParArchiveFolderPath)
                End If
            Catch ex As Exception
            End Try
            MyParPollInterval = LiveLinkConfiguration.PARPollInterval
            txtParPollInterval.Text = MyParPollInterval & " min"

            MyParTLDListen = LiveLinkConfiguration.PARTLDListen
            MyParUseTLDServer = LiveLinkConfiguration.PARTLDUseServer
            MyParTLDNumberOfDaysToRecover = LiveLinkConfiguration.PARTLDNumberOfDaysToRecover


            CheckBoxParTLDServer.Checked = MyParUseTLDServer
            If MyParUseTLDServer Then
                ButtonParTLDListener.Enabled = True
                btnParGetTld.Enabled = False
            Else
                ButtonParTLDListener.Enabled = False
            End If

            UpdateParStatus()
        Else
            TabControl1.TabPages.Remove(tabPAR)
            TabControl1.SelectedIndex = TabControl1.TabPages.IndexOf(tabStatus)
        End If

        ' POSI GATEWAY
        '---------------
        If LiveLinkConfiguration.POSIEnabled Then
            MyPosiEnabled = True
            CheckBoxPosiEnabled.Checked = LiveLinkConfiguration.POSIGatewayEnabled
            MyPosiConnectionString = LiveLinkConfiguration.POSIConnectionString
            MyPosiPollInterval = LiveLinkConfiguration.POSIPollInterval
            txtPosiPollInterval.Text = MyPosiPollInterval & " min"
            UpdatePosiStatus()
        Else
            TabControl1.TabPages.Remove(tabPOSI)
            TabControl1.SelectedIndex = TabControl1.TabPages.IndexOf(tabStatus)
        End If

        If LiveLinkConfiguration.DCAPriceSetGateWayEnabled Then
            MyDCAEnabled = True
            If LiveLinkConfiguration.DCAPriceSetEnabled Then
                CheckBoxDCAActive.Checked = True
            Else
                CheckBoxDCAActive.Checked = False
            End If
            TextBoxDCAPriceSetCheckInterval.Text = LiveLinkConfiguration.DCAPriceSetCheckInterval.ToString()
            TextBoxDCAPriceSetImportFileName.Text = LiveLinkConfiguration.DCAPriceSetImportFileName
            TextBoxDCAPriceSetImportFileConnectionString.Text = LiveLinkConfiguration.DCAPriceSetImportFileConnectionString
            TextBoxDCAPriceSetPOSFileConnectionString.Text = LiveLinkConfiguration.DCAPriceSetPOSFileConnectionString
        Else
            TabControl1.TabPages.Remove(tabSetPrice)
            TabControl1.SelectedIndex = TabControl1.TabPages.IndexOf(tabStatus)
        End If

        'Aloha POS
        '---------
        If LiveLinkConfiguration.AlohaEnabled Then
            MyAlohaEnabled = True
            btnEndOfDay.Visible = True
            CheckBoxAlohaEnabled.Checked = LiveLinkConfiguration.AlohaGatewayEnabled
            MyAlohaVersion = LiveLinkConfiguration.AlohaVersion
            MyAlohaGrindExe = LiveLinkConfiguration.AlohaGrindExe
            MyAlohaServiceType = LiveLinkConfiguration.AlohaServiceType
            MyAlohaQuickServiceSOS = LiveLinkConfiguration.AlohaQuickServiceSOS
            MyAlohaImportFilePath = LiveLinkConfiguration.AlohaImportFilePath
            MyAlohaTimeAttendanceEnabled = LiveLinkConfiguration.AlohaTimeAttendanceEnabled
            MyAlohaServiceTypeMapping = LiveLinkConfiguration.AlohaServiceTypeMapping
            If Not Strings.Right(MyAlohaImportFilePath, 1) = "\" Then
                MyAlohaImportFilePath &= "\"
            End If

            'Try ' Create the TLD Import directory
            'If Not Directory.Exists(MyAlohaImportFilePath) Then
            'Directory.CreateDirectory(MyAlohaImportFilePath)
            'End If
            'Catch ex As Exception
            'End Try
            MyAlohaPollInterval = LiveLinkConfiguration.AlohaPollInterval
            txtAlohaPollInterval.Text = MyAlohaPollInterval & " min"
            dtpAlohaProcessDate.Value = DateTime.Now
            dtpAlohaProcessDate.MinDate = DateTime.Now.AddDays(CDbl(LiveLinkConfiguration.AlohaPreviousDaysToCheck) * -1)

            If LiveLinkConfiguration.AlohaSkipDataDirectory Then
                ' Do not Grind or process the current day's data
                chkSkipData.Checked = True
                chkSkipGrind.Checked = True
                chkSkipGrind.Enabled = False
            End If
            UpdateAlohaStatus()
        Else
            TabControl1.TabPages.Remove(tabAloha)
            TabControl1.SelectedIndex = TabControl1.TabPages.IndexOf(tabStatus)
        End If

        'Micros POS Gateway - for Micros systems where LiveLink populates tbSalesMain itself
        '---------
        If LiveLinkConfiguration.MicrosEnabled Then
            MyMicrosEnabled = True
            btnEndOfDay.Visible = True
            CheckBoxMicrosEnabled.Checked = LiveLinkConfiguration.MicrosGatewayEnabled
            MyMicrosGatewayVersion = LiveLinkConfiguration.MicrosGatewayVersion
            MyMicrosPollInterval = LiveLinkConfiguration.MicrosPollInterval
            MyMicrosRevenueCentreFilter = LiveLinkConfiguration.MicrosRevenueCentreFilter
            txtMicrosPollInterval.Text = MyMicrosPollInterval & " min"
            UpdateMicrosStatus()
        Else
            TabControl1.TabPages.Remove(tabMicros)
            TabControl1.SelectedIndex = TabControl1.TabPages.IndexOf(tabStatus)
        End If


        'SUS POS Gateway 
        '---------
        If LiveLinkConfiguration.SUSImportEnabled Then
            MySusPosEnabled = True
            btnEndOfDay.Visible = True
            MySusPosEnabled = LiveLinkConfiguration.SUSImportEnabled
            MySusPosGatewayEnabled = LiveLinkConfiguration.SUSImportGatewayEnabled
            MySUSPosPollInterval = LiveLinkConfiguration.SUSImportPollInterval
            CheckBox5.Checked = MySusPosGatewayEnabled
            Label101.Text = MySUSPosPollInterval & " min"
            UpdateSUSStatus()
        Else
            TabControl1.TabPages.Remove(TabSUSPOS)
            TabControl1.SelectedIndex = TabControl1.TabPages.IndexOf(tabStatus)
        End If

        ' Alert Systems Gateway
        '---------------
        If LiveLinkConfiguration.AlertSystemsTrafficDataFileTransferEnabled Then
            MyAlertSystemsTrafficDataTransferEnabled = LiveLinkConfiguration.AlertSystemsTrafficDataFileTransferEnabled
            MyAlertSystemsGatewayEnabled = LiveLinkConfiguration.AlertSystemsGatewayEnabled
            MyAlertSystemsTrafficDataPollInterval = LiveLinkConfiguration.AlertSystemsTrafficDataPollInterval
            CheckBox6.Checked = MySusPosGatewayEnabled
            UpdateAlertSystemsTrafficDataStatus()
        Else
            TabControl1.TabPages.Remove(tabTrafficData)
            TabControl1.SelectedIndex = TabControl1.TabPages.IndexOf(tabStatus)
        End If

        'COMPRIS Gateway
        '---------------
        If LiveLinkConfiguration.ComprisEnabled Then
            Try
                ' check if PddFiles is set to be auto registered
                If (LiveLinkConfiguration.ComprisAutoRegisterPddFiles) Then
                    ComprisInterop.RegisterPddFilesDLL()
                End If

                ' Generate the Interop file for the Comnprios ActiveX dll
                ComprisInterop.GenerateComprisInteropFile(Application.StartupPath)

            Catch ex As Exception
                LogEvent("Unable to generate the Compris interop file" & vbCrLf & vbCrLf & "Error: " & ex.Message, EventLogEntryType.Warning)
            End Try
            MyComprisEnabled = True
            CheckBoxComprisEnabled.Checked = LiveLinkConfiguration.ComprisGatewayEnabled

            MyComprisXMLFilePath = LiveLinkConfiguration.ComprisXMLFilePath
            MyComprisActive = LiveLinkConfiguration.ComprisActiveAvailable
            MyComprisActiveMeKeySOD = LiveLinkConfiguration.ComprisActiveMeKeySOD
            MyComprisActiveMeKeyEOD = LiveLinkConfiguration.ComprisActiveMeKeyEOD
            MyComprisActiveScriptPreSOD = LiveLinkConfiguration.ComprisActiveScriptPreSOD
            MyComprisActiveScriptPostSOD = LiveLinkConfiguration.ComprisActiveScriptPostSOD
            MyComprisActiveScriptPreEOD = LiveLinkConfiguration.ComprisActiveScriptPreEOD
            MyComprisActiveScriptPostEOD = LiveLinkConfiguration.ComprisActiveScriptPostEOD
            MyComprisActiveAccessCode = LiveLinkConfiguration.ComprisActiveAccessCode
            MyComprisActiveRegisterCount = LiveLinkConfiguration.ComprisActiveRegisterCount

            MyComprisPollInterval = LiveLinkConfiguration.ComprisPollInterval
            txtComprisPollInterval.Text = MyComprisPollInterval & " min"

            ' hide or show sections on the Compris tab, depending on enabled features
            GroupComprisPDP.Visible = LiveLinkConfiguration.ComprisPDPEnabled
            GroupComprisPDPUpdates.Visible = LiveLinkConfiguration.ComprisPDPUpdatesEnabled
            GroupComprisPollFiles.Visible = LiveLinkConfiguration.ComprisPollFilesEnabled

            If MyComprisActive = True Then
                MenuItemRunComprisActive.Visible = True
                MenuItemTrayRunComprisActive.Visible = True
                mnuClockSpacer.Visible = True
            Else
                MenuItemRunComprisActive.Visible = False
                MenuItemTrayRunComprisActive.Visible = False
            End If

            UpdateComprisStatus()
        Else
            TabControl1.TabPages.Remove(tabCompris)
            TabControl1.SelectedIndex = TabControl1.TabPages.IndexOf(tabStatus)
        End If

        'PANASONIC Gateway
        '---------------
        If LiveLinkConfiguration.PanasonicEnabled Then
            MyPanasonicEnabled = True
            CheckBoxPanasonicEnabled.Checked = LiveLinkConfiguration.PanasonicGatewayEnabled
            MyPanasonicDatabaseName = LiveLinkConfiguration.PanasonicDatabaseName

            MyPanasonicPollInterval = LiveLinkConfiguration.PanasonicPollInterval
            txtPanasonicPollInterval.Text = MyPanasonicPollInterval & " min"

            UpdatePanasonicStatus()
        Else
            TabControl1.TabPages.Remove(tabPanasonic)
            TabControl1.SelectedIndex = TabControl1.TabPages.IndexOf(tabStatus)
        End If

        'INTOUCH Gateway
        '----------------
        If LiveLinkConfiguration.IntouchEnabled Then
            MyIntouchEnabled = True
            CheckBoxIntouchEnabled.Checked = LiveLinkConfiguration.IntouchGatewayEnabled

            MyIntouchAutomatedExport = LiveLinkConfiguration.IntouchAutomatedExport
            MyIntouchDrawerPullsEnabled = LiveLinkConfiguration.IntouchDrawerPullsEnabled
            MyIntouchExportExe = LiveLinkConfiguration.IntouchExportExe
            MyIntouchExportFilePath = LiveLinkConfiguration.IntouchExportFilePath

            ' get the field names that can be configurable for items and modifiers. 
            ' to see possible values, look at the string values for the "ItemIndex" and "ModifierIndex" enums in Intouch.vb
            MyIntouchItemCodeField = LiveLinkConfiguration.IntouchItemCodeField
            MyIntouchItemDescriptionField = LiveLinkConfiguration.IntouchItemDescriptionField
            MyIntouchModifierCodeField = LiveLinkConfiguration.IntouchModifierCodeField
            MyIntouchModifierDescriptionField = LiveLinkConfiguration.IntouchModifierDescriptionField
            MyIntouchServiceTypeMappings = LiveLinkConfiguration.IntouchServiceTypeMappings

            MyIntouchPollInterval = LiveLinkConfiguration.IntouchPollInterval
            txtIntouchPollInterval.Text = MyIntouchPollInterval & " min"

            UpdateIntouchStatus()

        Else
            TabControl1.TabPages.Remove(tabIntouch)
            TabControl1.SelectedIndex = TabControl1.TabPages.IndexOf(tabStatus)
        End If


        'OCTANE Gateway
        '----------------
        If LiveLinkConfiguration.OctaneEnabled Then
            CheckBoxOctaneEnabled.Checked = LiveLinkConfiguration.OctaneGatewayEnabled
            txtOctanePollInterval.Text = LiveLinkConfiguration.OctanePollInterval & " min"

            UpdateOctaneStatus()
        Else
            TabControl1.TabPages.Remove(tabOctane)
            TabControl1.SelectedIndex = TabControl1.TabPages.IndexOf(tabStatus)
        End If

        'ARTS Gateway
        '----------------
        If LiveLinkConfiguration.ArtsEnabled Then
            CheckBoxArtsEnabled.Checked = LiveLinkConfiguration.ArtsGatewayEnabled
            txtArtsPollInterval.Text = LiveLinkConfiguration.ArtsPollInterval & " min"

            UpdateArtsStatus()
        Else
            TabControl1.TabPages.Remove(tabArts)
            TabControl1.SelectedIndex = TabControl1.TabPages.IndexOf(tabStatus)
        End If


        'ICG Gateway
        '----------------
        If LiveLinkConfiguration.ICGEnabled Then
            CheckBoxICGEnabled.Checked = LiveLinkConfiguration.ICGGatewayEnabled
            txtICGPollInterval.Text = LiveLinkConfiguration.ICGPollInterval & " min"

            UpdateICGStatus()
        Else
            TabControl1.TabPages.Remove(tabICG)
            TabControl1.SelectedIndex = TabControl1.TabPages.IndexOf(tabStatus)
        End If


        'MicrosoftRMS Gateway
        '----------------
        If LiveLinkConfiguration.RMSEnabled Then
            CheckBoxRMSEnabled.Checked = LiveLinkConfiguration.RMSGatewayEnabled
            txtRMSPollInterval.Text = LiveLinkConfiguration.RMSPollInterval & " min"

            UpdateRMSStatus()
        Else
            TabControl1.TabPages.Remove(tabRMS)
            TabControl1.SelectedIndex = TabControl1.TabPages.IndexOf(tabStatus)
        End If

        'NewPOS Gateway
        '----------------
        If LiveLinkConfiguration.NewPOSEnabled Then
            CheckBoxNewPOSEnabled.Checked = LiveLinkConfiguration.NewPOSGatewayEnabled
            txtNewPOSPollInterval.Text = LiveLinkConfiguration.NewPOSPollInterval & " min"

            UpdateNewPOSStatus()
        Else
            TabControl1.TabPages.Remove(tabNewPOS)
            TabControl1.SelectedIndex = TabControl1.TabPages.IndexOf(tabStatus)
        End If

        'iPOS Gateway
        '----------------
        If LiveLinkConfiguration.iPOSEnabled Then
            CheckBoxiPOSEnabled.Checked = LiveLinkConfiguration.iPOSGatewayEnabled
            txtiPOSPollInterval.Text = LiveLinkConfiguration.iPOSPollInterval & " min"
            ' check if manual updates are allowed
            If (Not LiveLinkConfiguration.iPOSAllowManualUpdates) Then
                ButtoniPOSUpdateProducts.Enabled = False
                ButtoniPOSUpdateStaff.Enabled = False
            End If
            UpdateiPOSStatus()
            UpdateiPOSProcessDate(DateTime.Now.Date, False)
        Else
            TabControl1.TabPages.Remove(tabiPOS)
            TabControl1.SelectedIndex = TabControl1.TabPages.IndexOf(tabStatus)
        End If

        'Uniwell Gateway
        '----------------
        If LiveLinkConfiguration.UniwellEnabled Then
            CheckBoxUniwellEnabled.Checked = LiveLinkConfiguration.UniwellGatewayEnabled
            txtUniwellPollInterval.Text = LiveLinkConfiguration.UniwellPollInterval & " min"

            UpdateUniwellStatus()
        Else
            TabControl1.TabPages.Remove(tabUniwell)
            TabControl1.SelectedIndex = TabControl1.TabPages.IndexOf(tabStatus)
        End If

        'Radiant Gateway
        '----------------
        If LiveLinkConfiguration.RadiantEnabled Then
            CheckBoxRadiantEnabled.Checked = LiveLinkConfiguration.RadiantGatewayEnabled
            txtRadiantPollInterval.Text = LiveLinkConfiguration.RadiantPollInterval & " min"

            UpdateRadiantStatus()
        Else
            TabControl1.TabPages.Remove(tabRadiant)
            TabControl1.SelectedIndex = TabControl1.TabPages.IndexOf(tabStatus)
        End If

        'NAXML Gateway
        '----------------
        If LiveLinkConfiguration.NAXMLEnabled Then
            CheckBoxNAXMLEnabled.Checked = LiveLinkConfiguration.NAXMLGatewayEnabled
            txtNAXMLPollInterval.Text = LiveLinkConfiguration.NAXMLPollInterval & " min"

            UpdateNAXMLStatus()
        Else
            TabControl1.TabPages.Remove(tabNAXML)
            TabControl1.SelectedIndex = TabControl1.TabPages.IndexOf(tabStatus)
        End If


        'Xpient Gateway
        '----------------
        If LiveLinkConfiguration.XpientEnabled Then
            CheckBoxXpientEnabled.Checked = LiveLinkConfiguration.XpientGatewayEnabled
            txtXpientPollInterval.Text = LiveLinkConfiguration.XpientPollInterval & " min"

            UpdateXpientStatus()
        Else
            TabControl1.TabPages.Remove(tabXpient)
            TabControl1.SelectedIndex = TabControl1.TabPages.IndexOf(tabStatus)
        End If


        'Retalix Gateway
        '----------------
        If LiveLinkConfiguration.RetalixEnabled Then
            CheckBoxRetalixEnabled.Checked = LiveLinkConfiguration.RetalixGatewayEnabled
            txtRetalixPollInterval.Text = LiveLinkConfiguration.RetalixPollInterval & " min"

            UpdateRetalixStatus()
        Else
            TabControl1.TabPages.Remove(tabRetalix)
            TabControl1.SelectedIndex = TabControl1.TabPages.IndexOf(tabStatus)
        End If

        'Breeze Gateway
        '----------------
        If LiveLinkConfiguration.BreezeEnabled Then
            CheckBoxBreezeEnabled.Checked = LiveLinkConfiguration.BreezeGatewayEnabled
            txtBreezePollInterval.Text = LiveLinkConfiguration.BreezePollInterval & " min"

            UpdateBreezeStatus()
        Else
            TabControl1.TabPages.Remove(tabBreeze)
            TabControl1.SelectedIndex = TabControl1.TabPages.IndexOf(tabStatus)
        End If

        'POSixml Gateway
        '----------------
        If LiveLinkConfiguration.POSixmlEnabled Then
            CheckBoxPOSixmlEnabled.Checked = LiveLinkConfiguration.POSixmlGatewayEnabled
            txtPOSixmlPollInterval.Text = LiveLinkConfiguration.POSixmlPollInterval & " min"

            UpdatePOSixmlStatus()
        Else
            TabControl1.TabPages.Remove(tabPOSixml)
            TabControl1.SelectedIndex = TabControl1.TabPages.IndexOf(tabStatus)
        End If

        'RPOS Gateway
        '----------------
        If LiveLinkConfiguration.RPOSEnabled Then
            CheckBoxRPOSEnabled.Checked = LiveLinkConfiguration.RPOSGatewayEnabled
            txtRPOSPollInterval.Text = LiveLinkConfiguration.RPOSPollInterval & " min"

            UpdateRPOSStatus()
        Else
            TabControl1.TabPages.Remove(tabRPOS)
            TabControl1.SelectedIndex = TabControl1.TabPages.IndexOf(tabStatus)
        End If

        'YIS Controllers
        '---------------
        If (LiveLinkConfiguration.YisEnabled) Then
            MenuItemYISController.Visible = True
            MenuItemYISController.Enabled = False
            MenuItemTrayYISController.Visible = True
            MenuItemTrayYISController.Enabled = False
        End If

        'ZK Fingerprint
        '----------------
        If LiveLinkConfiguration.ZKFingerprintEnabled Then
            CheckBoxZKEnabled.Checked = LiveLinkConfiguration.ZKFingerprintGatewayEnabled
            txtZKPollInterval.Text = LiveLinkConfiguration.ZKFingerprintPollInterval & " min"
            txtZKDeviceIP.Text = LiveLinkConfiguration.ZKFingerprintIP
            txtZKDevicePort.Text = LiveLinkConfiguration.ZKFingerprintPort.ToString
            UpdateZKFingerprintStatus()

            If LiveLinkConfiguration.ZKFingerprintUserFormEnabled Then
                MenuItemZKViewUsers.Enabled = True
                MenuItemZKViewUsers.Visible = True
                MenuItemTrayZKViewUsers.Enabled = True
                MenuItemTrayZKViewUsers.Visible = True
            Else
                MenuItemTrayZKViewUsers.Visible = False
                MenuItemZKViewUsers.Visible = False
            End If
        Else
            TabControl1.TabPages.Remove(tabZKFingerprint)
            TabControl1.SelectedIndex = TabControl1.TabPages.IndexOf(tabStatus)
        End If

        'For POS that use common layout : Transight,Intouch,Octane,Panasonic,Micros,POSI,ICG,MicrosoftRMS,New POS,Uniwell,Radiant,NAXML,XPient,Retalix,ARTS,POsIxml,Radiant RPOS
        'TODO : Refractor to use common UI for these POS

        _posCommon = New CompositePOS()

        If _posCommon.HasPosData() Then
            btnEndOfDay.Visible = _posCommon.HasPosData()

            chkEnableGatewayCommon.Checked = _posCommon.GatewayEnabled
            txtPollInterval.Text = _posCommon.PollInterval & " min"
            _posCommon.LastPollDate = DateTime.MinValue
            tabPosCommon.Text = _posCommon.PosName
            AddHandler _posCommon.StatusChangedEvent, AddressOf ShowStatus

            UpdatePosStatusCommon()
        Else
            TabControl1.TabPages.Remove(tabPosCommon)
            TabControl1.SelectedIndex = TabControl1.TabPages.IndexOf(tabStatus)
        End If

        If LiveLinkConfiguration.AutoStartOnLogin Then
            EnsureLiveLinkAutoStarts(True)
        Else
            EnsureLiveLinkAutoStarts(False)
        End If

        UserAccess(False)

        _livelinkCore.SetupDatabase()

        ' check if enhance schema is enabled
        If (LiveLinkConfiguration.LiveLinkSchemaEnabled) Then
            Try
                ' install livelink schema defined in Mx.LiveLink.DB
                Mx.Services.LiveLink.LiveLinkService.InstallSchema(LiveLinkConfiguration.ConnectionString)
            Catch ex As Exception
                SetStatus("Error", "Error occurred setting up new LiveLink schema")
                LogEvent(EventLogEntryType.Error, "Exception occurred installing new LiveLink schema. Exception: {0}", ex.ToString)
            End Try
        End If

        Try
            If LiveLinkConfiguration.AutoModeOnStartUp Then
                StartStop()
            End If
        Catch
            StartStop()
        End Try
        Try
            If LiveLinkConfiguration.ManualModeOnly Then
                Button1.Enabled = False
            End If
        Catch
            Button1.Enabled = True
        End Try
        If UseMxGetProcessesbyName Then
            SetStatus("", "Warning:GetProcessesByName failed - using alternate method")
        End If
        LogEvent("Live Link started", EventLogEntryType.Information)

        If Not RefreshLiveLinkID() Then
            ' could not get required LiveLink ID information from WMI. Restarting LiveLink...
            Close()
            Dispose()
            RestartLiveLink()
            Return
        End If

        ' do LiveLink auto-activation if enabled
        AutoActivateLiveLink()

        If MyTimeClockAvailable And MyTimeClockAutoShow Then
            LoadTimeClock()
        End If
        PingEnabled = True
        If (LiveLinkConfiguration.UseOfflineCashup_KeyExists AndAlso LiveLinkConfiguration.UseOfflineCashup) Then
            RetrieveOfflineCashupSettings()
        End If

    End Sub

    Private Sub SetDebugLevel()
        Dim sDebugMode As String = LiveLinkConfiguration.DebugMode
        Try
            If (sDebugMode.Length = 4) AndAlso (sDebugMode = Strings.Mid(MyIdentity, Now.Day, 4)) Then
                DebugMode = DebugModes.Simple
            ElseIf (sDebugMode.Length = 8) AndAlso (sDebugMode = Strings.Mid(MyIdentity, Now.Day, 8)) Then
                DebugMode = DebugModes.Verbose
            End If
        Catch ex As Exception
            LogEvent(String.Format("SetDebugLevel error: {0}", ex), EventLogEntryType.Warning)
            DebugMode = DebugModes.Off
        End Try

    End Sub

    Private Sub UpdateAppsettings()

        ' Check that the ConnectionString key exists.
        If Not LiveLinkConfiguration.ConnectionString_KeyExists AndAlso LiveLinkConfiguration.SQL_DB_Connect_KeyExists Then
            LiveLinkConfiguration.ConnectionString = LiveLinkConfiguration.SQL_DB_Connect
            LiveLinkConfiguration.SQL_DB_Connect = "Obsolete - Use ConnectionString"
            LiveLinkConfiguration.Save()
        End If

    End Sub

    Private Sub MenuItem5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem5.Click
        LogEvent("User attempting to shut down LiveLink from the menu", EventLogEntryType.Information)
        EndApplication()
    End Sub

    Private Sub EndApplication(Optional ByVal Force As Boolean = False)
        'Check that not running then end
        If Force Then
            LabelStatus.Text = "Stopped"
            UserAccessStatus = True
        End If
        If (LabelStatus.Text = "Stopped") And (UserAccessStatus) Then
            Application.Exit()
        End If
    End Sub

    Private Sub MenuItem4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem4.Click
        Dim FrmAbout As New LiveLinkAbout
        FrmAbout.ShowDialog()
    End Sub

    Function PrevInstance() As Boolean
        ' Even though this application is configured as a single instance application, the windows application framework
        ' will only prevent duplicates from running if the version numbers are identical.
        ' This is an additional check in case a second version of LiveLink is started with a difefrent version number
        If Process.GetProcessesByName(Process.GetCurrentProcess.ProcessName).Length > 1 Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub NotifyIcon1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles NotifyIcon1.DoubleClick
        ' Show the form when the user double clicks on the notify icon.

        ' Set the WindowState to normal if the form is minimized.
        If (Me.WindowState = FormWindowState.Minimized) Then _
         Me.WindowState = FormWindowState.Normal

        ' Activate the form.
        Me.Activate()
        Me.Show()
    End Sub

#End Region

#Region " Web Service Data Transfer "

    Private Sub tmrCommsBackoff_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmrCommsBackoff.Tick
        CommsBackoffCount = Max(CommsBackoffCount - 1, 0)
        If CommsBackoffCount = 0 Then
            OK_To_Send = True
        End If
    End Sub

    Private Sub BackOffCommunications(ByVal disablePing As Boolean, Optional ByVal ForcedBackoffInterval As Integer = -1)
        ' Reset the timer
        If disablePing Then
            PingEnabled = False
        End If
        tmrCommsBackoff.Stop()
        If ForcedBackoffInterval <> -1 Then
            ' If a value is passed in, this means that a forced back off time has been sent by
            ' the web service.
            CommsBackoffCount = Max(Min(ForcedBackoffInterval, MaxCommsBackoffMinutes), 1)
            CommsBackoffMinutes = CommsBackoffCount
        Else
            CommsBackoffCount = CommsBackoffMinutes
            ' TODO: Flush DNS cache, reset IP stack (as this back off was caused by a comms failure)
        End If
        SetStatus("Pause ...", "Sending is paused for " & CommsBackoffCount & " minute(s)")
        ' Increase the next backoff time according to the following algorithm ...
        ' 1, 2, 3, 4, 5 minutes ... then
        ' 10, 15, 20, 25, 30 minutes ... then
        ' 40, 50, 60 minutes ... then
        ' 90, 120, 150, 180 minutes ...
        ' increase by 1 hour until max (4 hours) is reached
        Dim NextBackoffMinutes As Integer
        Select Case CommsBackoffMinutes
            Case 0 To 4
                NextBackoffMinutes = CommsBackoffMinutes + 1 ' increase by 1 minute
            Case 5 To 25
                NextBackoffMinutes = CommsBackoffMinutes + 5 ' increase by 5 minutes
            Case 26 To 50
                NextBackoffMinutes = CommsBackoffMinutes + 10 ' increase by 10 minutes
            Case 51 To 150
                NextBackoffMinutes = CommsBackoffMinutes + 30 ' increase by half an hour
            Case Else
                NextBackoffMinutes = CommsBackoffMinutes + 60 ' increase by 1 hour
        End Select
        CommsBackoffMinutes = System.Math.Min(NextBackoffMinutes, MaxCommsBackoffMinutes) ' Double the next backoff time
        ToolTip1.SetToolTip(PictureBoxBackoff, String.Format("LiveLink has backed off for {0} minute(s)", CommsBackoffMinutes))
        OK_To_Send = False
        tmrCommsBackoff.Start()
    End Sub

    Private Sub SuccessfulCommunications()
        OK_To_Send = True
        CommsBackoffMinutes = 1 ' After a successful communication, reset the backoff timer to 1 minute
        UpdateLastSuccessfulCommunication()
    End Sub

    Private Sub UpdateLastSuccessfulCommunication()
        ' Stub function
        ' TODO: Add the current time to a field in a table in the database
        ' Warning - do not break other LiveLinks eg Micros - Ask Mike for help here!!
        Try
            Select Case LiveLinkConfiguration.POSType.ToUpper()
                Case "MICROS", "PIXELPOINT"
                    ' Currently exclude this from all sybase instances of LiveLink
                Case Else
                    Dim sql As String = String.Format("UPDATE tbStoreInfo SET LastAwake='{0:yyyyMMdd HH:mm:ss}'", DateTime.Now)
                    Dim errorMsg As String = String.Empty
                    If Not MMSGeneric_Execute(sql, errorMsg) Then
                        LogEvent(EventLogEntryType.Error, "Error occurred updating last communication time. SQL: {0}{0}{1}{0}", vbCrLf, errorMsg)
                    End If
            End Select
        Catch ex As Exception
            LogEvent(EventLogEntryType.Error, "Exception occurred updating last communication time. Exception: {0}{0}{1}{0}", vbCrLf, ex.ToString)
        End Try
    End Sub

    Private Function Is_OK_To_Send() As Boolean
        ' Reports back whether it is OK to send and updates the visual display.
        If OK_To_Send Then
            Is_OK_To_Send = True
        Else
            Is_OK_To_Send = False
            SetStatus("Sending skipped ...", "Communications are still paused for " & CommsBackoffCount & " minute(s)")
        End If
    End Function

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        'Send data to server
        SendDataToServer()
    End Sub

    Private Sub ProcessResponse(ByVal MyLiveLinkResponse As Mx.POS.Common.eRetailer.LiveLinkResponse, Optional ByVal Backoff As Boolean = False)
        ' Use this function to handle any responses that you do not wish to handle directly in the 
        ' routines that talk to the web service
        Select Case MyLiveLinkResponse.ResponseCode
            Case eRetailer.LiveLinkResponseCode.Backoff
                SetStatus("", "Store is temporarily offline")
                BackOffCommunications(True, MyLiveLinkResponse.ResponseNumber)
            Case eRetailer.LiveLinkResponseCode.Offline
                SetStatus("", "Store is offline")
                BackOffCommunications(True, MaxCommsBackoffMinutes)
            Case eRetailer.LiveLinkResponseCode.AwaitingActivation
                SetStatus("", "Store not yet activated")
                If Backoff Then
                    BackOffCommunications(True)
                End If
                PingEnabled = False
            Case eRetailer.LiveLinkResponseCode.LeaseExpired
                SetStatus("", "Lease expired")
                ExpireLease()
                If Backoff Then
                    BackOffCommunications(False)
                End If
            Case eRetailer.LiveLinkResponseCode.LeaseRejected
                SetStatus("", "Lease rejected")
                ExpireLease()
                If Backoff Then
                    BackOffCommunications(True)
                End If
                PingEnabled = False
            Case eRetailer.LiveLinkResponseCode.InvalidConfiguration
                SetStatus("", "Invalid configuration")
                If Backoff Then
                    BackOffCommunications(True)
                End If
                PingEnabled = False
            Case eRetailer.LiveLinkResponseCode.DataError, eRetailer.LiveLinkResponseCode.DataErrorFlagRecords
                SetStatus("", "Data Error")
                If Backoff Then
                    BackOffCommunications(True)
                End If
                PingEnabled = False
            Case eRetailer.LiveLinkResponseCode.DataErrorInvalidStore
                SetStatus("", "Data Error - Invalid store id")
                If Backoff Then
                    BackOffCommunications(True)
                End If
                PingEnabled = False
            Case eRetailer.LiveLinkResponseCode.UnknownError
                SetStatus("", "An unknown error has occured")
                If Backoff Then
                    BackOffCommunications(True)
                End If
                PingEnabled = False
        End Select
    End Sub

    Public Shared Function GettbSalesMainFields(ByVal myPOSType As String) As String
        Dim sFieldList As String = "*"
        Select Case myPOSType.Trim.ToUpper
            Case "MICROS"
                ' Note that the order of these columns is IMPORTANT.  
                ' If you change the order or insert new columns, you must update the column numbers in the function sqlSelectAllRecordsForTransaction
                sFieldList = ""
                sFieldList &= "SalesMainID, " ' 1
                sFieldList &= "RecordType, " ' 2
                sFieldList &= "TransactionID, " ' 3
                sFieldList &= "RecordSubType, " ' 4
                sFieldList &= "EntityID, " ' 5
                sFieldList &= "RegisterID, " ' 6
                sFieldList &= "convert(varchar, datepart(day, polldate)) + '-' + datename(month, polldate) + '-' + convert(varchar, datepart(year, polldate)) + ' ' + convert(varchar, datepart(hour, polldate)) + ':' + Right('0' + convert(varchar, datepart(minute, polldate)), 2) + ':' + Right('0' + convert(varchar, datepart(second, polldate)), 2) as PollDate, " ' 7
                sFieldList &= "PollCount, " ' 8
                sFieldList &= "PollAmount, " ' 9
                sFieldList &= "ClerkID, " ' 10
                sFieldList &= "ClerkName, " ' 11
                sFieldList &= "CustomerID, " ' 12
                sFieldList &= "Synced, " ' 13
                sFieldList &= "PLUCodeID, " ' 14
                sFieldList &= "PLUCode, " ' 15
                sFieldList &= "SequenceNo, " ' 16
                sFieldList &= "Flag, " ' 17
                sFieldList &= "ApplyTax, " ' 18
                sFieldList &= "PriceLevel, " ' 19
                sFieldList &= "SyncToken, " ' 20
                sFieldList &= "ParentID, " ' 21
                sFieldList &= "IFNULL(BusinessDay,NULL,convert(varchar, datepart(day, BusinessDay)) + '-' + datename(month, BusinessDay) + '-' + convert(varchar, datepart(year, BusinessDay)) + ' ' + convert(varchar, datepart(hour, BusinessDay)) + ':' + Right('0' + convert(varchar, datepart(minute, BusinessDay)), 2) + ':' + Right('0' + convert(varchar, datepart(second, BusinessDay)), 2)) as BusinessDay, " ' 22
                sFieldList &= "TransactionVersion, " ' 23
                sFieldList &= "ItemDiscount, " ' 24
                sFieldList &= "MicrosId as POSTransactionID, " ' 25
                sFieldList &= "ItemTax, " ' 26
                sFieldList &= "SubTypeDescription" ' 27
            Case "ALOHA"
                sFieldList = ""
                sFieldList &= "SalesMainID, "
                sFieldList &= "RecordType, "
                sFieldList &= "TransactionID, "
                sFieldList &= "RecordSubType, "
                sFieldList &= "SubTypeDescription, "
                sFieldList &= "EntityID, "
                sFieldList &= "RegisterID, "
                sFieldList &= "convert(varchar, datepart(day, polldate)) + '-' + datename(month, polldate) + '-' + convert(varchar, datepart(year, polldate)) + ' ' + convert(varchar, datepart(hour, polldate)) + ':' + Right('0' + convert(varchar, datepart(minute, polldate)), 2) + ':' + Right('0' + convert(varchar, datepart(second, polldate)), 2) as PollDate, "
                sFieldList &= "PollCount, "
                sFieldList &= "PollAmount, "
                sFieldList &= "ClerkID, "
                sFieldList &= "ClerkName, "
                sFieldList &= "CustomerID, "
                sFieldList &= "Synced, "
                sFieldList &= "PLUCodeID, "
                sFieldList &= "PLUCode, "
                sFieldList &= "SequenceNo, "
                sFieldList &= "Flag, "
                sFieldList &= "ApplyTax, "
                sFieldList &= "ItemTax, "
                sFieldList &= "ItemDiscount, "
                sFieldList &= "PriceLevel, "
                sFieldList &= "TransactionVersion, "
                sFieldList &= "POSTransactionID, "
                sFieldList &= "SyncToken, "
                sFieldList &= "ParentID, "
                sFieldList += "BusinessDay"
            Case Else
                sFieldList = "*"
        End Select
        Return sFieldList
    End Function

    Public Shared Function GettbSalesMainFieldsForInsert(ByVal myPOSType As String) As String
        Dim sFieldList As String = "*"
        Select Case myPOSType.Trim.ToUpper
            Case "ALOHA"
                sFieldList = ""
                'sFieldList += "SalesMainID, "
                sFieldList += "RecordType, "
                sFieldList += "TransactionID, "
                sFieldList += "RecordSubType, "
                sFieldList += "SubTypeDescription, "
                sFieldList += "EntityID, "
                sFieldList += "RegisterID, "
                sFieldList += "PollDate, "
                sFieldList += "PollCount, "
                sFieldList += "PollAmount, "
                sFieldList += "ClerkID, "
                sFieldList += "ClerkName, "
                sFieldList += "CustomerID, "
                sFieldList += "Synced, "
                sFieldList += "PLUCodeID, "
                sFieldList += "PLUCode, "
                sFieldList += "SequenceNo, "
                sFieldList += "Flag, "
                sFieldList += "ApplyTax, "
                sFieldList += "PriceLevel, "
                sFieldList += "POSTransactionID, "
                sFieldList += "DateAdded, "
                If LiveLinkConfiguration.AlohaVersion = "VER2" Then
                    sFieldList += "ParentID, "
                    sFieldList += "BusinessDay, "
                Else
                    sFieldList += "ParentID, "
                End If
                sFieldList &= "ItemTax, "
                sFieldList &= "ItemDiscount, "
                sFieldList &= "TransactionVersion "
            Case Else
                sFieldList = "*"
        End Select
        Return sFieldList
    End Function

    Private Sub CancelProcess()
        Dim POSType As String = LiveLinkConfiguration.POSType

        Try
            ' set the cancel flag
            CancelFlag = True

            ' do any specific cancelling required for different POS processing
            Select Case POSType.ToUpper
                Case "ALOHA"
                    If MyAlohaVersion = "VER2" AndAlso MyAloha IsNot Nothing Then
                        MyAloha.CancelProcess()
                    End If
            End Select
        Catch ex As Exception
        End Try
    End Sub

    Private Sub btnProcessNow_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnProcessNow.Click
        ' This is the "PROCESS NOW" button
        ' Change the current tab to the STATUS tab]
        TabControl1.SelectedTab = tabStatus
        If btnProcessNow.Text = "Cancel" Then
            CancelProcess()
            SetEndofDayStatus("Cancelling ...")
        Else
            If Not Processing Then
                Processing = True
                OK_To_Send = True ' Override the backoff timer if Process Now is clicked
                If (mySessionKey = "") And (myConditionalSessionKey <> "") Then
                    ' If the site is not yet activated, expire the lease so that a new lease is obtained.
                    ' This will ensure that the activation status is checked when the button is clicked, 
                    ' rather than waiting for the lease to expire.
                    ExpireLease()
                End If
                SendDataToServer()

                ' send any additional data type
                SendData()

                ProcessOfflineCashup()
                ProcessLiveSync(True)
                ProcessTrafficData()

                ' Check for any pending messages
                RetrieveMessage()
                Processing = False
            End If
        End If
    End Sub

    Private Sub ProcessOfflineCashup()
        Try
            If LiveLinkConfiguration.UseOfflineCashup _
             AndAlso OfflineCashup.AreAnyOfflineSyncsReady(GetEntityId()) Then
                SendOfflineCashupInfo()
            End If
        Catch ex As Exception
            SetStatus("Error", ex.Message)
            SetStatus(String.Empty, "Offlinecashup needs to be run first.")
        End Try
    End Sub

    Private Sub Button3_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        DataGrid1.DataSource = Nothing
        DataGrid1.Refresh()
        TextBox1.Text = ""
    End Sub

#End Region

#Region " Start/Stop mode functionality "

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If (UserAccessStatus) Then
            StartStop()
        End If
    End Sub

    Private Sub StartStop()
        If LabelStatus.Text = "Stopped" Then
            AutoStart()
            AutoStartPOSSpecific()
        Else
            AutoStopPOSSpecific()
            AutoStop()
        End If
        MenuItem5.Enabled = UserAccessStatus And (LabelStatus.Text = "Stopped")
        mnuTrayExit.Enabled = UserAccessStatus And (LabelStatus.Text = "Stopped")
    End Sub

    Private Sub StartStopMainTimer(ByVal start As Boolean)
        Timer1.Enabled = start
        IsRunning = start
        StartStopPingTimer(start)
    End Sub

    Private Sub AutoStart()
        'Switch on Auto Mode
        LabelStatus.Text = "Running"
        LabelStatus.ForeColor = Color.Green
        Button1.Text = "Stop"
        PictureBoxOff.Visible = True
        PictureBoxON.Visible = True
        PictureBoxON.BringToFront()
        CurrentCount = 0
        StartStopMainTimer(True)
    End Sub

    Private Sub AutoStop()
        'Switch Off Auto Mode
        Button1.Text = "Start"
        LabelStatus.Text = "Stopped"
        LabelStatus.ForeColor = Color.Red
        PictureBoxON.Visible = True
        PictureBoxOff.Visible = True
        PictureBoxOff.BringToFront()
        StartStopMainTimer(False)
    End Sub

    Private Sub AutoStartPOSSpecific()
        StartPARListener()
        StartZKServer()
        StartYISConsumers()
        StartComprisListener()
    End Sub

    Private Sub AutoStopPOSSpecific()
        StopPARListener()
        StopZKServer()
        StopYISConsumers()
        StopComprisListener()
    End Sub

    Private Sub AutoKeepAlive()
        ' only run keep alive every X minutes
        If (lastKeepAlive.AddMinutes(LiveLinkConfiguration.KeepAliveInterval) < DateTime.Now) Then
            lastKeepAlive = DateTime.Now
            ' run YIS keep alive functionality
            KeepAliveYIS()
        End If

    End Sub

#End Region

#Region " Status, Setup, Config, Test "

    Public Delegate Sub SetStatusDelegate(ByVal T1 As String, ByVal T3 As String)

    Private Sub SetStatus(ByVal T1 As String, ByVal T3 As String)
        Const cMaxMsgLength = 1024 * 10
        Dim txtMessage As String

        Dim logMsg As String = "{0} - {1}"

        If T1 <> "" Then
            TextBox1.Text = T1
            MxLogger.LogInformationToTrace(logMsg, T1, T3)
        Else
            MxLogger.LogInformationToTrace(T3)
        End If
        If T3 <> "" Then
            txtMessage = Format(Now, "dd-MMM-yy HH:mm:ss") & " - " & T3 & vbCrLf & TextBox3.Text
            If txtMessage.Length > cMaxMsgLength Then
                txtMessage = txtMessage.Remove(cMaxMsgLength, txtMessage.Length - cMaxMsgLength)
            End If
            TextBox3.Text = txtMessage
            'If Len(TextBox3.Text) > 1024 Then
            'TextBox3.Text = Mid(TextBox3.Text, 1, 512)
            'End If
            TextBox3.Refresh()
        End If
    End Sub

    Private Sub Button4_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        'GET MENU ITEMS FROM THE SERVER
        Dim sClientID As String = LiveLinkConfiguration.ClientID
        Dim sSecurityCode As String = LiveLinkConfiguration.SecurityCode
        Dim sStoreID As String = If(LiveLinkConfiguration.StoreID_KeyExists(), LiveLinkConfiguration.StoreID.ToString(), "")
        Dim sDataFormat As String = "Cratos"
        Dim MyPosData As New LiveLinkWebService ' TODO: Enable GZip support: eRetailerCompressed
        'Set the webservice reference
        MyPosData.Url = LiveLinkConfiguration.WebService_URL
        LoadProxy(MyPosData)
        TextBox4.Text = "Wait..."
        TextBox4.Refresh()
        Me.Refresh()
        MySalesData = MyPosData.Get_MenuItems(sClientID, sSecurityCode, sDataFormat, sStoreID)
        DataGrid1.DataSource = MySalesData
        TextBox4.Text = "Complete"
    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        Dim AwakeInterval As Integer
        If Not Processing Then
            Timer1.Enabled = False
            Processing = True
            CurrentCount += 1
            If (CurrentCount >= myFlowInterval) Or (CheckBox1.Checked) Then
                ProcessPosi()
                ' send any processed transaction level data
                SendDataToServer()
                ' send any processed summary data (if it is enabled)
                SendData()
                ' reset current count
                CurrentCount = 0
            End If
            AwakeCount += 1
            If HasConditionalLease() Then
                AwakeInterval = 2 * myAwakeInterval ' If a conditional lease has been granted, then slow down the awake messages
            Else
                AwakeInterval = myAwakeInterval
            End If
            If (AwakeCount >= AwakeInterval) Then
                If ProcessAwake() Then
                    AwakeCount = 0 ' Only reset the awake counter after a successful heartbeat
                End If
            End If
            If (MyDCAEnabled = True) Then
                DCACount += 1
                If (CurrentCount >= Val(TextBoxDCAPriceSetCheckInterval.Text)) Then
                    ProcessDCA()
                    DCACount = 0
                End If
            End If

            If MyParEnabled Then
                ProcessPar()
                UpdateParStatus()
            End If

            If MyAlohaEnabled Then
                ProcessAloha()
                UpdateAlohaStatus() ' Used to update the clock on the Aloha page
            End If

            If MyMicrosEnabled Then
                ProcessMicros()
                UpdateMicrosStatus()
            End If

            If MyComprisEnabled Then
                ProcessCompris()
                UpdateComprisStatus()
            End If

            If MyPanasonicEnabled Then
                ProcessPanasonic()
                UpdatePanasonicStatus()
            End If

            If MyIntouchEnabled Then
                ProcessIntouch()
                UpdateIntouchStatus()
            End If

            If LiveLinkConfiguration.ComprisPDPEnabled Then
                ProcessComprisPDP()
            End If

            If LiveLinkConfiguration.ComprisPDPUpdatesEnabled Then
                ProcessComprisPDPUpdates()
            End If

            If LiveLinkConfiguration.ComprisPollFilesEnabled Then
                ProcessComprisPollFiles()
            End If

            If LiveLinkConfiguration.OctaneEnabled Then
                ProcessOctane()
                UpdateOctaneStatus()
            End If

            If LiveLinkConfiguration.ICGEnabled Then
                ProcessICG()
                UpdateICGStatus()
            End If

            If LiveLinkConfiguration.RMSEnabled Then
                ProcessRMS()
                UpdateRMSStatus()
            End If

            If LiveLinkConfiguration.NewPOSEnabled Then
                ProcessNewPOS()
                UpdateNewPOSStatus()
            End If

            If LiveLinkConfiguration.iPOSEnabled Then
                ProcessiPOS()
                UpdateiPOSStatus()
            End If

            If LiveLinkConfiguration.UniwellEnabled Then
                ProcessUniwell()
                UpdateUniwellStatus()
            End If

            If LiveLinkConfiguration.RadiantEnabled Then
                ProcessRadiant()
                UpdateRadiantStatus()
            End If

            If LiveLinkConfiguration.NAXMLEnabled Then
                ProcessNAXML()
                UpdateNAXMLStatus()
            End If

            If LiveLinkConfiguration.XpientEnabled Then
                ProcessXpient()
                UpdateXpientStatus()
            End If

            If LiveLinkConfiguration.RetalixEnabled Then
                ProcessRetalix()
                UpdateRetalixStatus()
            End If

            If LiveLinkConfiguration.BreezeEnabled Then
                ProcessBreeze()
                UpdateBreezeStatus()
            End If

            If LiveLinkConfiguration.ZKFingerprintEnabled Then
                ProcessZKFingerprintsTA()
                UpdateZKFingerprintStatus()
            End If

            If LiveLinkConfiguration.TimePunchImportEnabled Then
                ProcessTimePunchImport()
            End If

            If LiveLinkConfiguration.SUSImportEnabled Then
                ProcessSUSPOSImport()
            End If

            If LiveLinkConfiguration.ServiceReportEnabled Then
                ProcessServiceReport()
            End If

            If LiveLinkConfiguration.ArtsEnabled Then
                ProcessArts()
                UpdateArtsStatus()
            End If

            If LiveLinkConfiguration.POSixmlEnabled Then
                ProcessPOSixml()
                UpdatePOSixmlStatus()
            End If

            If LiveLinkConfiguration.RPOSEnabled Then
                ProcessRPOS()
                UpdateRPOSStatus()
            End If

            ProcessPosCommon()

            If LiveLinkConfiguration.IntouchMenuPackageEnabled Then
                ProcessIntouchMenuPackage()
            End If

            If LiveLinkConfiguration.AlertSystemsTrafficDataFileTransferEnabled Then
                ProcessTrafficData()
            End If

            ProcessOfflineCashup()
            ProcessLiveSync()

            ' check if any servers or tasks need to be kept alive
            AutoKeepAlive()

            If MessageWaiting Then
                RetrieveMessage()
            End If

            If MyTimeClockAvailable Then
                MyTimeClockIntervalCount += 1
                ' synchronizing of the time clock users occur over a configurable period (in hours)
                If MyTimeClockForceUserSync OrElse ((MyTimeClockIntervalCount / 60) >= MyTimeClockSyncInterval) Then
                    MyTimeClockIntervalCount = 0
                    MyTimeClockForceUserSync = False

                    SyncLocalUsers()

                    Try
                        ' trigger the reload of the drop down list
                        If MyTimeClockInstance IsNot Nothing Then
                            MyTimeClockInstance.ReloadUserList()
                        End If
                    Catch ex As Exception
                        LogEvent("Exception: Failed to reload user list - " + ex.Message, EventLogEntryType.Error)
                    End Try
                End If
            End If

            ' Check if livelink needs to be shutdown or restarted
            CheckAction()

            Processing = False
            Timer1.Enabled = True
        End If
    End Sub

    Private Sub CheckAction()
        If BounceLiveLink() Then
            LogEvent(EventLogEntryType.Information, "LiveLink re-start triggered. Application was started at [{0}]. LiveLink is set to re-start every {1} days", LiveLinkStartTime.ToString(DateFormatNames.DatabaseDateTimeFormatWithSecond), LiveLinkRestartFrequency)
        ElseIf ForceShutDown() Then
            LogEvent(EventLogEntryType.Information, "User forced a shutdown with file [{0}]", LiveLinkShutDownFile)
        End If
    End Sub

    Private Function BounceLiveLink() As Boolean
        BounceLiveLink = False
        Dim TimeNow As DateTime = DateTime.Now

        If LiveLinkRestartFrequency > 0 AndAlso _
         TimeNow > LiveLinkStartTime.AddDays(LiveLinkRestartFrequency) AndAlso _
         TimeNow.Hour >= 3 AndAlso _
         TimeNow.Hour < 5 Then

            RestartLiveLink()
            BounceLiveLink = True
        End If
    End Function

    Private Function ForceShutDown() As Boolean
        ForceShutDown = False
        Try
            If File.Exists(LiveLinkShutDownFile) Then
                File.Delete(LiveLinkShutDownFile)
                Application.Exit()
                ForceShutDown = True
            End If
        Catch deleteEx As IOException
            LogEvent(EventLogEntryType.Error, "IO Exception during a user forced shutdown while deleting [{0}]: {1}", LiveLinkShutDownFile, deleteEx.ToString)
        Catch UnAuthEx As UnauthorizedAccessException
            LogEvent(EventLogEntryType.Error, "Unauthorized Access Exception occured while attempting to delete [{0}] during a user forced shutdown: {1}", LiveLinkShutDownFile, UnAuthEx.ToString)
        Catch ex As Exception
            LogEvent(EventLogEntryType.Error, "Got an exception during a user forced shutdown: {0}", ex.ToString)
        End Try
    End Function

    Private Function ProcessAwake(Optional ByVal Force As Boolean = False) As Boolean
        ProcessAwake = False
        UserAccess(False)
        If Force OrElse HasConditionalLease() OrElse Is_OK_To_Send() Then
            GetLease()
            If Not StartupSent Then
                ' send startup information
                SendStartupInfo()
            End If

            If myConditionalSessionKey <> String.Empty Then ' Applies to both lease types
                Try
                    Dim LogDataSet As New DataSet
                    Dim AwakeMessage As String = String.Empty
                    CreateLogTable(LogDataSet)
                    If Force Or (AwakeCounter = 0) Then ' Only send verbose info every "AwakeCounterVerbose" times
                        If Force Then
                            AwakeMessage = "** Awake Info - Forced (UTC) **"
                        Else
                            AwakeMessage = "** Awake Info (UTC) **"
                        End If
                        AddLogInfo(LogDataSet, SessionMessage.msg_Information, AwakeMessage, Format(Now.ToUniversalTime, "dd-MMM-yyyy HH:mm:ss"))
                        AddLogInfo(LogDataSet, SessionMessage.msg_Information, "TotalRecords", GetAwakeData("TotalRecords"))
                        AddLogInfo(LogDataSet, SessionMessage.msg_Information, "UnSyncedRecords", GetAwakeData("UnSyncedRecords"))
                        AddLogInfo(LogDataSet, SessionMessage.msg_Information, "ErrorRecords", GetAwakeData("ErrorRecords"))
                        AddLogInfo(LogDataSet, SessionMessage.msg_Information, "WrongEntityRecords", GetAwakeData("WrongEntity"))
                        AddLogInfo(LogDataSet, SessionMessage.msg_Information, "DateLastRecord", GetAwakeData("DateLastRecord"))
                        AddLogInfo(LogDataSet, SessionMessage.msg_Information, "DateLastSyncedRecord", GetAwakeData("DateLastSyncedRecord"))
                        Try
                            Dim objSysInfo As New clsSysInfo
                            With objSysInfo
                                If .FixedDisk1 <> "" Then
                                    AddLogInfo(LogDataSet, SessionMessage.msg_Information, "Fixed Disk 1", .FixedDisk1)
                                End If
                                If .FixedDisk2 <> "" Then
                                    AddLogInfo(LogDataSet, SessionMessage.msg_Information, "Fixed Disk 2", .FixedDisk2)
                                End If
                                If .FixedDisk3 <> "" Then
                                    AddLogInfo(LogDataSet, SessionMessage.msg_Information, "Fixed Disk 3", .FixedDisk3)
                                End If
                            End With
                        Catch ex As Exception
                            AddLogInfo(LogDataSet, SessionMessage.msg_Warning, "System Info", "Unable to retrieve disk space info")
                        End Try
                    End If
                    If Force OrElse HasConditionalLease() Then
                        If Not Force And (AwakeMessage = String.Empty) Then
                            AddLogInfo(LogDataSet, SessionMessage.msg_Information, "** Awake Heartbeat - Conditional (UTC) **", Format(Now.ToUniversalTime, "dd-MMM-yyyy HH:mm:ss"))
                        End If
                        ' Now attach the entityid and registerid for information purposes
                        ' In the case of Quicken, the Serial_Number needs to be sent before the site can be activated
                        AddLogInfo(LogDataSet, SessionMessage.msg_Information, "Store Info", "EntityId: " & GetEntityId() & ", RegisterId: " & GetRegisterId())
                    Else
                        If AwakeMessage = String.Empty Then
                            AddLogInfo(LogDataSet, SessionMessage.msg_Information, "** Awake Heartbeat(UTC) **", Format(Now.ToUniversalTime, "dd-MMM-yyyy HH:mm:ss"))
                            ' Send a POS specific message for the system monitor
                            If MyIntouchEnabled And MyIntouchLatestExportChanged Then
                                ' If using the InTouch POS, send the latest export file time
                                AddLogInfo(LogDataSet, SessionMessage.msg_Information, "InTouch;Latest Export", Format(MyIntouchLatestExport, "dd-MMM-yyyy HH:mm:ss"))
                                MyIntouchLatestExportChanged = False
                            End If
                        End If
                    End If
                    SendLogInfo(LogDataSet)
                    'Ignore any result
                    AwakeCounter = (AwakeCounter + 1) Mod myAwakeCounterVerbose
                    SetStatus("", "Heartbeat sent")
                    ProcessAwake = True

                Catch ex As WebException
                    SetStatus("Error", ex.Message)
                    BackOffCommunications(False)
                Catch ex As Exception
                    SetStatus("Error", "Unable to send Awake Info")
                End Try
            End If
        End If
    End Function

    Private Sub SendOfflineCashupInfo()
        If Is_OK_To_Send() Then
            ToggleSendButtons(False)
            GetLease()
            If mySessionKey <> String.Empty Then
                Try
                    For i As Integer = 1 To LiveLinkConfiguration.OfflineCashupBatchIterations
                        Dim batch As OfflineCashManagementClientToServerList = OfflineCashup.GetBatchToSend()

                        If (batch.Count = 0) Then Exit For

                        Dim mxWebService As New LiveLinkWebService
                        Dim myLiveLinkResponse As Mx.POS.Common.eRetailer.LiveLinkResponse
                        SetStatus("Synchronising offline cashup data...", String.Format("Sending batch {0} of {1} cashup transactions to server", i, batch.Count))

                        mxWebService.Url = myWebService
                        LoadProxy(mxWebService)
                        Dim serializer As XmlSerializer = New XmlSerializer(GetType(Mx.BusinessObjects.OfflineCashManagementClientToServerList))
                        Using ms As New MemoryStream
                            serializer.Serialize(ms, batch)
                            myLiveLinkResponse = mxWebService.SendOfflineCashupInfo(MyIdentity, mySessionKey, GetEntityId(), CompressByteArray(ms.ToArray))
                        End Using

                        Select Case myLiveLinkResponse.ResponseCode
                            Case eRetailer.LiveLinkResponseCode.Success
                                SetStatus("Synchronising offline cashup data...", "Cashup information sent")
                                OfflineCashup.MarkBatchAsSent(batch)
                            Case Else
                                ProcessResponse(myLiveLinkResponse, False)
                        End Select
                    Next
                Catch ex As Exception
                    SetStatus("Error", "Unable to send cashup data")
                    LogEvent("Unable to send cashup data" & vbCrLf & "Error: " & ex.ToString, EventLogEntryType.Error)
                End Try
            End If
        End If
        ToggleSendButtons(True)
    End Sub


#Region "LiveSync data send"


    Private Sub ProcessLiveSync(Optional ByVal force As Boolean = False)
        ' check live sync is enabled
        If (Not LiveLinkConfiguration.LiveSyncEnabled) Then
            Return
        End If

        ' check if a sync is due
        If (liveSyncLastPoll.AddMinutes(LiveLinkConfiguration.LiveSyncInterval) < DateTime.Now OrElse force) Then
            ' get a lease before syncing data
            GetLease()
            ' check lease is acquired
            If mySessionKey <> String.Empty Then
                ' send data to the server
                If Not LiveSyncDataSend.SendLiveSync(MyIdentity, mySessionKey, GetEntityId(), AddressOf SetStatus) Then
                    SetStatus("Error", "Error occurring syncing live data with server. See event logs for details")
                End If
            End If
            ' set the last poll time to now
            liveSyncLastPoll = DateTime.Now
        End If

    End Sub


#End Region

    ' this is a delegate function that is called from the TimeClock thread
    Delegate Function TriggerUserSync() As Boolean

    ' sets a boolean parameter to force 
    Private Function TriggerTimeClockUserSync() As Boolean
        'If Not Processing Then
        '    Try
        '        MyTimeClockForceUserSync = True
        '        Processing = True
        '        SyncLocalUsers()  
        '    Catch ex As Exception
        '        LogEvent("Exception: Failed to reload user list - " & ex.Message, EventLogEntryType.Error)
        '    Finally
        '        Processing = False
        '    End Try
        'Else
        MyTimeClockForceUserSync = True

        'End If
    End Function

    Private Sub TimeClock_TriggerUserSync() Handles MyTimeClockInstance.TimeClockSyncLocalUsers
        Me.Invoke(New TriggerUserSync(AddressOf TriggerTimeClockUserSync))
        Application.DoEvents()
    End Sub

    Private Function SyncLocalUsers() As Boolean
        Dim MyRemoteSync As New LiveLinkWebService
        Dim MyLiveLinkResponse As eRetailer.LiveLinkResponse

        MyRemoteSync.Url = myWebService
        LoadProxy(MyRemoteSync)

        GetLease()
        If mySessionKey <> String.Empty Then
            Try
                SetStatus("", "Retrieving user list")

                If (LiveLinkConfiguration.TimeClockJobRoleEnabled Or LiveLinkConfiguration.TimeClockJobsOnlyEnabled) Then
                    MyLiveLinkResponse = MyRemoteSync.GetUserWithJobsAndRoles(MyIdentity, mySessionKey, GetEntityId())
                Else
                    MyLiveLinkResponse = MyRemoteSync.GetUsers(MyIdentity, mySessionKey, GetEntityId())
                End If

                If MyLiveLinkResponse.ResponseCode = eRetailer.LiveLinkResponseCode.Success Then

                    _livelinkCore.ReplaceLocalUsers(MyLiveLinkResponse.ResponseData)

                    If (LiveLinkConfiguration.TimeClockJobRoleEnabled Or LiveLinkConfiguration.TimeClockJobsOnlyEnabled) Then
                        ReplaceLocalEmployeeJobs(MyLiveLinkResponse.ResponseData)
                        ReplaceLocalEmployeeRoles(MyLiveLinkResponse.ResponseData)
                        ReplaceLocalJobRoles(MyLiveLinkResponse.ResponseData)
                        SyncLocalJobsAndRoles()
                    End If

                Else
                    SetStatus("", "Failed to retrieve user list")
                End If
            Catch ex As Exception
                SetStatus("Error", ex.Message)
            End Try
        End If
    End Function

    Private Function SyncLocalJobsAndRoles() As Boolean
        Dim myRemoteSync As New LiveLinkWebService
        Dim myLiveLinkResponse As Mx.POS.Common.eRetailer.LiveLinkResponse

        myRemoteSync.Url = myWebService
        LoadProxy(myRemoteSync)

        GetLease()
        If mySessionKey <> String.Empty Then
            Try
                SetStatus("", "Retrieving jobs and roles list")
                myLiveLinkResponse = myRemoteSync.GetJobsAndRoles(MyIdentity, mySessionKey)
                If myLiveLinkResponse.ResponseCode = eRetailer.LiveLinkResponseCode.Success Then

                    ReplaceLocalJobs(myLiveLinkResponse.ResponseData)
                    ReplaceLocalRoles(myLiveLinkResponse.ResponseData)

                Else
                    SetStatus("", "Failed to retrieve jobs and roles list")
                End If
            Catch ex As Exception
                SetStatus("Error", ex.Message)
            End Try
        End If
    End Function

    Private Function ReplaceLocalEmployeeJobs(ByVal usersData As DataSet) As Boolean
        Dim posType As String = LiveLinkConfiguration.POSType
        Dim useODBC As Boolean = LiveLinkConfiguration.Use_ODBC_DB_Connect
        Dim myRow As System.Data.DataRow
        Dim beginTransactionSQL As String = "BEGIN TRANSACTION" ' sql to begin a transaction
        Const deleteSQL As String = "DELETE FROM tbLabourEmployeeJob" + vbCrLf ' sql to delete local users
        Const sybaseDeleteSQL As String = "DELETE tbLabourEmployeeJob" + vbCrLf ' sql to delete local users for sybase
        Const endTransSQL As String = "COMMIT TRANSACTION"
        Dim insertSQL As String = String.Empty
        Dim sql As String
        Dim errorMsg As String = String.Empty

        Dim employeeID As String
        Dim jobId As String
        Dim isPrimary As String

        ' add a ; to the end of the begin transaction and to the commit transaction statements for Sybase databases
        Select Case posType
            Case "MICROS", "PIXELPOINT"
                beginTransactionSQL += vbCrLf + sybaseDeleteSQL
            Case Else
                beginTransactionSQL += vbCrLf + deleteSQL
        End Select

        If usersData.Tables(1).Rows.Count > 0 Then
            For Each myRow In usersData.Tables(1).Rows
                employeeID = EvalNull(myRow.Item("EmployeeId"), "")
                jobId = EvalNull(myRow.Item("JobId"), "")
                isPrimary = EvalNull(myRow.Item("IsPrimary"), "0")

                If employeeID > 0 AndAlso jobId > 0 Then
                    ' create insert query
                    insertSQL += "INSERT INTO tbLabourEmployeeJob (EmployeeId, JobId, IsPrimary) VALUES ("
                    insertSQL += employeeID.ToString + ", "
                    insertSQL += jobId.ToString + ", "
                    insertSQL += "'" + isPrimary.ToString() + "') " + vbCrLf
                End If
            Next

            ' if there are records to insert, then create transaction sql
            If insertSQL <> String.Empty Then
                sql = beginTransactionSQL + insertSQL + endTransSQL
                If Not MMSGeneric_Execute(useODBC, sql, errorMsg) Then
                    LogEvent(EventLogEntryType.Error, "Failed to update local users jobs.{0}Query=[{1}]{2}Error=[{3}]", vbCrLf, sql, vbCrLf, errorMsg)
                    SetStatus("", "Failed to update users jobs")
                    Return False
                End If
                SetStatus("", "Updated local users jobs")
                Return True
            End If
        End If

        SetStatus("", "No users jobs retrieved")
        Return False
    End Function

    Private Function ReplaceLocalEmployeeRoles(ByVal usersData As DataSet) As Boolean
        Dim posType As String = LiveLinkConfiguration.POSType
        Dim useODBC As Boolean = LiveLinkConfiguration.Use_ODBC_DB_Connect
        Dim myRow As System.Data.DataRow
        Dim beginTransactionSQL As String = "BEGIN TRANSACTION" ' sql to begin a transaction
        Const deleteSQL As String = "DELETE FROM tbLabourEmployeeRole" + vbCrLf ' sql to delete local users
        Const sybaseDeleteSQL As String = "DELETE tbLabourEmployeeRole" + vbCrLf ' sql to delete local users for sybase
        Const endTransSQL As String = "COMMIT TRANSACTION"
        Dim insertSQL As String = String.Empty
        Dim sql As String
        Dim errorMsg As String = String.Empty

        Dim employeeID As String
        Dim roleId As String

        ' add a ; to the end of the begin transaction and to the commit transaction statements for Sybase databases
        Select Case posType
            Case "MICROS", "PIXELPOINT"
                beginTransactionSQL += vbCrLf + sybaseDeleteSQL
            Case Else
                beginTransactionSQL += vbCrLf + deleteSQL
        End Select

        If usersData.Tables(2).Rows.Count > 0 Then
            For Each myRow In usersData.Tables(2).Rows
                employeeID = EvalNull(myRow.Item("EmployeeId"), "")
                roleId = EvalNull(myRow.Item("RoleId"), "")

                If employeeID > 0 AndAlso roleId > 0 Then
                    ' create insert query
                    insertSQL += "INSERT INTO tbLabourEmployeeRole (EmployeeId, RoleId) VALUES ("
                    insertSQL += employeeID.ToString + ", "
                    insertSQL += roleId.ToString() + ") " + vbCrLf
                End If
            Next

            ' if there are records to insert, then create transaction sql
            If insertSQL <> String.Empty Then
                sql = beginTransactionSQL + insertSQL + endTransSQL
                If Not MMSGeneric_Execute(useODBC, sql, errorMsg) Then
                    LogEvent(EventLogEntryType.Error, "Failed to update local users roles.{0}Query=[{1}]{2}Error=[{3}]", vbCrLf, sql, vbCrLf, errorMsg)
                    SetStatus("", "Failed to update users roles")
                    Return False
                End If
                SetStatus("", "Updated local users roles")
                Return True
            End If
        End If

        SetStatus("", "No users roles retrieved")
        Return False
    End Function

    Private Function ReplaceLocalJobRoles(ByVal usersData As DataSet) As Boolean
        Dim posType As String = LiveLinkConfiguration.POSType
        Dim useODBC As Boolean = LiveLinkConfiguration.Use_ODBC_DB_Connect
        Dim myRow As System.Data.DataRow
        Dim beginTransactionSQL As String = "BEGIN TRANSACTION" ' sql to begin a transaction
        Const deleteSQL As String = "DELETE FROM tbLabourJobTypeRole" + vbCrLf ' sql to delete local users
        Const sybaseDeleteSQL As String = "DELETE tbLabourJobTypeRole" + vbCrLf ' sql to delete local users for sybase
        Const endTransSQL As String = "COMMIT TRANSACTION"
        Dim insertSQL As String = String.Empty
        Dim sql As String
        Dim errorMsg As String = String.Empty

        Dim jobId As String
        Dim roleId As String

        ' add a ; to the end of the begin transaction and to the commit transaction statements for Sybase databases
        Select Case posType
            Case "MICROS", "PIXELPOINT"
                beginTransactionSQL += vbCrLf + sybaseDeleteSQL
            Case Else
                beginTransactionSQL += vbCrLf + deleteSQL
        End Select

        If usersData.Tables(3).Rows.Count > 0 Then
            For Each myRow In usersData.Tables(3).Rows
                jobId = EvalNull(myRow.Item("JobId"), "")
                roleId = EvalNull(myRow.Item("RoleId"), "")

                If jobId > 0 AndAlso roleId > 0 Then
                    ' create insert query
                    insertSQL += "INSERT INTO tbLabourJobTypeRole (JobId, RoleId) VALUES ("
                    insertSQL += jobId.ToString + ", "
                    insertSQL += roleId.ToString() + ") " + vbCrLf
                End If
            Next

            ' if there are records to insert, then create transaction sql
            If insertSQL <> String.Empty Then
                sql = beginTransactionSQL + insertSQL + endTransSQL
                If Not MMSGeneric_Execute(useODBC, sql, errorMsg) Then
                    LogEvent(EventLogEntryType.Error, "Failed to update local job roles.{0}Query=[{1}]{2}Error=[{3}]", vbCrLf, sql, vbCrLf, errorMsg)
                    SetStatus("", "Failed to update users roles")
                    Return False
                End If
                SetStatus("", "Updated local job roles")
                Return True
            End If
        End If

        SetStatus("", "No job roles retrieved")
        Return False
    End Function

    Private Function ReplaceLocalJobs(ByVal jobRoleData As DataSet) As Boolean
        Dim posType As String = LiveLinkConfiguration.POSType
        Dim useODBC As Boolean = LiveLinkConfiguration.Use_ODBC_DB_Connect
        Dim myRow As System.Data.DataRow
        Dim beginTransactionSQL As String = "BEGIN TRANSACTION" ' sql to begin a transaction
        Const deleteSQL As String = "DELETE FROM tbLabourJob" + vbCrLf ' sql to delete local users
        Const sybaseDeleteSQL As String = "DELETE tbLabourJob" + vbCrLf ' sql to delete local users for sybase
        Const endTransSQL As String = "COMMIT TRANSACTION"
        Dim insertSQL As String = String.Empty
        Dim sql As String
        Dim errorMsg As String = String.Empty

        Dim jobId As String
        Dim jobName As String

        ' add a ; to the end of the begin transaction and to the commit transaction statements for Sybase databases
        Select Case posType
            Case "MICROS", "PIXELPOINT"
                beginTransactionSQL += vbCrLf + sybaseDeleteSQL
            Case Else
                beginTransactionSQL += vbCrLf + deleteSQL
        End Select

        If jobRoleData.Tables(0).Rows.Count > 0 Then
            For Each myRow In jobRoleData.Tables(0).Rows
                jobId = EvalNull(myRow.Item("JobId"), "")
                jobName = EvalNull(myRow.Item("Name"), "")

                If Name <> "" AndAlso jobId > 0 Then
                    ' create insert query
                    insertSQL += "INSERT INTO tbLabourJob (JobId, Name) VALUES ("
                    insertSQL += jobId.ToString + ", "
                    insertSQL += "'" + EncodeSQL(jobName) + "') " + vbCrLf
                End If
            Next

            ' if there are records to insert, then create transaction sql
            If insertSQL <> String.Empty Then
                sql = beginTransactionSQL + insertSQL + endTransSQL
                If Not MMSGeneric_Execute(useODBC, sql, errorMsg) Then
                    LogEvent(EventLogEntryType.Error, "Failed to update local jobs.{0}Query=[{1}]{2}Error=[{3}]", vbCrLf, sql, vbCrLf, errorMsg)
                    SetStatus("", "Failed to update jobs")
                    Return False
                End If
                SetStatus("", "Updated local jobs")
                Return True
            End If
        End If

        SetStatus("", "No jobs retrieved")
        Return False
    End Function

    Private Function ReplaceLocalRoles(ByVal jobRoleData As DataSet) As Boolean
        Dim posType As String = LiveLinkConfiguration.POSType
        Dim useODBC As Boolean = LiveLinkConfiguration.Use_ODBC_DB_Connect
        Dim myRow As System.Data.DataRow
        Dim beginTransactionSQL As String = "BEGIN TRANSACTION" ' sql to begin a transaction
        Const deleteSQL As String = "DELETE FROM tbLabourRole" + vbCrLf ' sql to delete local users
        Const sybaseDeleteSQL As String = "DELETE tbLabourRole" + vbCrLf ' sql to delete local users for sybase
        Const endTransSQL As String = "COMMIT TRANSACTION"
        Dim insertSQL As String = String.Empty
        Dim sql As String
        Dim errorMsg As String = String.Empty

        Dim roleId As String
        Dim roleName As String

        ' add a ; to the end of the begin transaction and to the commit transaction statements for Sybase databases
        Select Case posType
            Case "MICROS", "PIXELPOINT"
                beginTransactionSQL += vbCrLf + sybaseDeleteSQL
            Case Else
                beginTransactionSQL += vbCrLf + deleteSQL
        End Select

        If jobRoleData.Tables(1).Rows.Count > 0 Then
            For Each myRow In jobRoleData.Tables(1).Rows
                roleId = EvalNull(myRow.Item("RoleId"), "")
                roleName = EvalNull(myRow.Item("Name"), "")

                If Name <> "" AndAlso roleId > 0 Then
                    ' create insert query
                    insertSQL += "INSERT INTO tbLabourRole (RoleId, Name) VALUES ("
                    insertSQL += roleId.ToString + ", "
                    insertSQL += "'" + EncodeSQL(roleName) + "') " + vbCrLf
                End If
            Next
        Else
            insertSQL = vbCrLf
        End If

        ' if there are records to insert, then create transaction sql
        If insertSQL <> String.Empty Then
            sql = beginTransactionSQL + insertSQL + endTransSQL
            If Not MMSGeneric_Execute(useODBC, sql, errorMsg) Then
                LogEvent(EventLogEntryType.Error, "Failed to update local roles.{0}Query=[{1}]{2}Error=[{3}]", vbCrLf, sql, vbCrLf, errorMsg)
                SetStatus("", "Failed to update roles")
                Return False
            End If
            SetStatus("", "Updated local roles")
            Return True
        End If

        SetStatus("", "No roles retrieved")
        Return False
    End Function

    Private Function GetAwakeData(ByVal sDataType As String) As String
        GetAwakeData = "Unknown"
        Try
            Dim sStoreID As String = GetEntityId()
            Dim Use_ODBC As Boolean = LiveLinkConfiguration.Use_ODBC_DB_Connect
            Dim sPOSType As String = LiveLinkConfiguration.POSType.Trim.ToUpper
            Dim sQry As String
            Dim bNoMSA_Flag As Boolean

            Select Case sPOSType
                Case Is = "MICROS"
                    bNoMSA_Flag = True
                Case Else
                    bNoMSA_Flag = False
            End Select

            Select Case sDataType
                Case "TotalRecords"
                    sQry = "Select Count(*) from tbSalesMain "
                Case "UnSyncedRecords"
                    sQry = "Select Count(*) from tbSalesMain Where Synced=0"
                Case "ErrorRecords"
                    sQry = "Select Count(*) from tbSalesMain Where Synced=2"
                Case "WrongEntity"
                    sQry = "Select Count(*) from tbSalesMain Where Synced=3"
                Case "DateLastRecord"
                    Select Case sPOSType
                        Case "QUICKEN" ' Uses a different field name for Polled Date
                            sQry = "Select Max(Date_Time) from tbSalesMain"
                        Case "MICROS", "PROTOUCH" ' Stores Polled Date as a DateTime
                            sQry = "Select Max(PollDate) from tbSalesMain"
                        Case Else ' Stores PolledDate as a VARCHAR
                            sQry = "Select Max(CONVERT(DATETIME, PollDate)) from tbSalesMain"
                    End Select
                Case "DateLastSyncedRecord"
                    Select Case sPOSType
                        Case "QUICKEN" ' Uses a different field name for Polled Date
                            sQry = "Select Max(Date_Time) from tbSalesMain Where Synced = 1"
                        Case "MICROS", "PROTOUCH" ' Stores Polled Date as a DateTime
                            sQry = "Select Max(PollDate) from tbSalesMain Where Synced = 1"
                        Case Else ' Stores Polled Date as a VARCHAR
                            sQry = "Select Max(CONVERT(DATETIME, PollDate)) from tbSalesMain Where Synced = 1"
                    End Select
            End Select

            Dim sError As String = String.Empty
            Dim awkDataSet As DataSet
            If MMSGeneric_ListAll(Use_ODBC, sQry, "tbSalesMain", awkDataSet, sError, bNoMSA_Flag) Then
                If awkDataSet.Tables("tbSalesMain").Rows.Count = 1 Then
                    Select Case sDataType
                        Case "DateLastRecord", "DateLastSyncedRecord"
                            GetAwakeData = Format(awkDataSet.Tables("tbSalesMain").Rows(0).Item(0), "dd-MMM-yyyy HH:mm:ss")
                        Case Else
                            GetAwakeData = awkDataSet.Tables("tbSalesMain").Rows(0).Item(0).ToString
                    End Select
                End If
            End If
        Catch ex As Exception
        End Try
    End Function

    Private Sub btnAccessCode_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAccessCode.Click
        Dim sAccessCode As String = LiveLinkConfiguration.AccessCode
        ' check if access code is case sensitive or insensitive and apply the correct check
        If (Not LiveLinkConfiguration.AccessCodeCaseSensitive AndAlso (UCase(TextBoxAccessCode.Text) = UCase(sAccessCode))) _
            OrElse (LiveLinkConfiguration.AccessCodeCaseSensitive AndAlso (TextBoxAccessCode.Text = sAccessCode)) Then
            UserAccess(True)
            TextBoxAccessCode.Text = ""
            Me.AcceptButton = Me.btnProcessNow
            Me.btnProcessNow.Focus()
        Else
            UserAccess(False)
            TextBoxAccessCode.Focus()
            TextBoxAccessCode.SelectAll()
        End If
        mnuTrayExit.Enabled = UserAccessStatus And (LabelStatus.Text = "Stopped")
    End Sub

    Private Sub TextBoxAccessCode_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBoxAccessCode.TextChanged
        Me.AcceptButton = Me.btnAccessCode
    End Sub

    Private Sub TextBoxAccessCode_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBoxAccessCode.LostFocus
        Me.AcceptButton = Me.btnProcessNow
    End Sub

    Private Sub UserAccess(ByVal bStatus As Boolean)
        UserAccessStatus = bStatus
        TabControl1.Enabled = bStatus
        Button1.Enabled = bStatus
        MenuItem5.Enabled = bStatus And (LabelStatus.Text = "Stopped")
        mnuTrayExit.Enabled = bStatus And (LabelStatus.Text = "Stopped")
    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        Me.WindowState = FormWindowState.Normal
        Me.WindowState = FormWindowState.Minimized
    End Sub

    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        'GET DATA FROM LOCAL TABLE to Test
        Try
            Dim MyTestData As DataSet
            Dim sConnect_ODBC As String = LiveLinkConfiguration.ODBC_DB_Connect
            Dim sPOSType As String = LiveLinkConfiguration.POSType
            Select Case UCase(Trim(sPOSType))
                Case Is = "MICROS"
                    bNoMSA_Flag = True
                Case Else
                    bNoMSA_Flag = False
            End Select
            Dim sQryTX As String = ""
            Dim sError As String = ""
            TextBox4.Text = "Wait..."
            TextBox4.Refresh()
            Me.Refresh()
            sQryTX = "Select Top 100 * from tbSalesMain Where (TransactionID>0) and (Synced=0) Order By TransactionID, SalesMainID "
            If MMSLocalGeneric_ListAll_MSA(sConnect_ODBC, sQryTX, "tbSalesMain", MyTestData, sError, bNoMSA_Flag) Then
                DataGrid1.DataSource = MyTestData
            Else
                MsgBox(sError)
                TextBox4.Text = "Error 1"
                Exit Sub
            End If
            DataGrid1.DataSource = MyTestData
            TextBox4.Text = "Complete"
        Catch
            TextBox4.Text = "Error 2"
        End Try
    End Sub
#End Region

#Region " Message Retrieval "

    Private Sub RetrieveMessage()
        Dim MessageStatus As RetrieveMessageStatus = RetrieveMessageStatus.Success ' The status of the downloaded message, to be sent back to the server (1 = Success; > 1 means error)
        Dim multiCommandMessage As Boolean = False

        UserAccess(False)
        If Is_OK_To_Send() Then
            GetLease()
            If myConditionalSessionKey <> "" Then ' If the machine is online or awaiting activation, then continue
                Try
                    Dim MyMessage As New LiveLinkWebService
                    Dim sPOSType As String = LiveLinkConfiguration.POSType.Trim
                    Dim MyLiveLinkResponse As Mx.POS.Common.eRetailer.LiveLinkResponse
                    SetStatus("Retrieving message ...", "Retrieving message from server")

                    MyMessage.Url = myWebService
                    LoadProxy(MyMessage)
                    MyLiveLinkResponse = MyMessage.RetrieveMessage(MyIdentity, myConditionalSessionKey, 0, 0)
                    MessageWaiting = MyLiveLinkResponse.ResponseMessageWaiting
                    ' determine if the current message is a multicommand message (e.g. there is more than one command in a sequence that need to be executed)
                    multiCommandMessage = (MyLiveLinkResponse.RecordCount > 0)
                    Select Case MyLiveLinkResponse.ResponseCode
                        Case eRetailer.LiveLinkResponseCode.UnknownError
                            If MyLiveLinkResponse.ResponseString.Trim.ToUpper = "NO MESSAGE FOUND" Then
                                SetStatus("Information", "No messages waiting")
                                MessageWaiting = False
                            Else
                                SetStatus("Error", "Invalid message - ignoring")
                            End If
                        Case eRetailer.LiveLinkResponseCode.NewVersion
                            SetStatus("Message", "A new version is available for download")
                            Dim NewVersion As String() = MyLiveLinkResponse.ResponseString.Split(";")
                            If NewVersion.Length = 2 Then
                                ' Flag that the response has been received so that the server can flag as done successfully (1)
                                ' Pass the message id back to the server
                                MyLiveLinkResponse = MyMessage.RetrieveMessage(MyIdentity, myConditionalSessionKey, MyLiveLinkResponse.ResponseNumber, RetrieveMessageStatus.Success)
                                If MyLiveLinkResponse.ResponseCode = eRetailer.LiveLinkResponseCode.Success Then
                                    UpgradeLiveLink(NewVersion(0), NewVersion(1))
                                Else
                                    SetStatus("Error", "Upgrade has been cancelled")
                                End If
                            Else
                                SetStatus("Error", "Invalid url and checksum retrieved")
                            End If
                        Case eRetailer.LiveLinkResponseCode.ChangeConfigsPermanent
                            SetStatus("Message", "Configuration settings are being updated")
                            If WriteNewConfigSettings(MyLiveLinkResponse.ResponseString) Then
                                MessageStatus = RetrieveMessageStatus.Success
                            Else
                                MessageStatus = RetrieveMessageStatus.NotSuccessful
                                SetStatus("Error", "Configuration settings not updated")
                            End If
                            MyLiveLinkResponse = MyMessage.RetrieveMessage(MyIdentity, myConditionalSessionKey, MyLiveLinkResponse.ResponseNumber, MessageStatus)

                        Case eRetailer.LiveLinkResponseCode.POSCommand
                            SetStatus("Message", "POS Command Received")
                            Dim POSCommand As String = MyLiveLinkResponse.ResponseString
                            If POSCommand = String.Empty Then
                                MessageStatus = RetrieveMessageStatus.InvalidMessageFormat
                            Else
                                If SendPOSCommand(POSCommand, MyLiveLinkResponse.ResponseNumber) Then
                                    MessageStatus = RetrieveMessageStatus.Success
                                Else
                                    MessageStatus = RetrieveMessageStatus.NotSuccessful
                                End If
                            End If
                            ' Flag the command with the response status, Pass the message id back to the server 
                            MyLiveLinkResponse = MyMessage.RetrieveMessage(MyIdentity, myConditionalSessionKey, MyLiveLinkResponse.ResponseNumber, MessageStatus)


                        Case eRetailer.LiveLinkResponseCode.POSCommandData
                            SetStatus("Message", "POS Command Data Received")
                            Dim posCommandData As String = MyLiveLinkResponse.ResponseString
                            If String.IsNullOrEmpty(posCommandData) Then
                                MessageStatus = RetrieveMessageStatus.InvalidMessageFormat
                            Else
                                If SavePOSCommandData(posCommandData, MyLiveLinkResponse.ResponseNumber) Then
                                    MessageStatus = RetrieveMessageStatus.Success
                                Else
                                    MessageStatus = RetrieveMessageStatus.NotSuccessful
                                End If
                            End If
                            ' Flag the POS command with the response status, Pass the message id back to the server 
                            MyLiveLinkResponse = MyMessage.RetrieveMessage(MyIdentity, myConditionalSessionKey, MyLiveLinkResponse.ResponseNumber, MessageStatus)

                        Case eRetailer.LiveLinkResponseCode.Execute
                            SetStatus("Message", "Program to execute")
                            Try
                                Dim Command As String() = MyLiveLinkResponse.ResponseString.Split(";")
                                Dim Result As Integer
                                '
                                ' Command is a ; seperated string
                                ' 1st parameter = execution text
                                ' 2nd parameter = wait, 0=false, 1=true
                                ' 3rd parameter = timeout(optional), -1=infinite(default)
                                '
                                Select Case Command.Length
                                    Case 2
                                        Result = Shell(Command(0), AppWinStyle.MinimizedNoFocus, Wait:=IIf(Command(1) = 0, False, True))
                                        ' If wait=true, 0=success, <> 0 = timeout
                                        ' If wait=false, Result=ProcessID, 0 = failed
                                        If Command(1) <> "0" AndAlso Result <> 0 Then
                                            SetStatus("Message", "Execution timed out")
                                            MessageStatus = RetrieveMessageStatus.NotSuccessful
                                        ElseIf Command(1) = "0" AndAlso Result = 0 Then
                                            SetStatus("Message", "Execution failed")
                                            MessageStatus = RetrieveMessageStatus.NotSuccessful
                                        Else
                                            SetStatus("Message", "Program executed succesfully")
                                            MessageStatus = RetrieveMessageStatus.Success
                                        End If
                                    Case 3
                                        Result = Shell(Command(0), AppWinStyle.MinimizedNoFocus, Wait:=IIf(Command(1) = 0, False, True), Timeout:=Command(2))
                                        ' If wait=true, 0=success, <> 0 = timeout
                                        ' If wait=false, Result=ProcessID, 0 = failed
                                        If Command(1) <> "0" AndAlso Result <> 0 Then
                                            SetStatus("Message", "Execution timed out")
                                            MessageStatus = RetrieveMessageStatus.NotSuccessful
                                        ElseIf Command(1) = "0" AndAlso Result = 0 Then
                                            SetStatus("Message", "Execution failed")
                                            MessageStatus = RetrieveMessageStatus.NotSuccessful
                                        Else
                                            SetStatus("Message", "Program executed succesfully")
                                            MessageStatus = RetrieveMessageStatus.Success
                                        End If
                                    Case Else
                                        SetStatus("Message", "Execution failed - invalid format")
                                        MessageStatus = RetrieveMessageStatus.InvalidMessageFormat
                                End Select

                            Catch fileEx As FileNotFoundException
                                SetStatus("Message", "Execution failed - file not found")
                                MessageStatus = RetrieveMessageStatus.NotSuccessful
                            Catch ex As Exception
                                SetStatus("Message", "Execution failed - " & ex.ToString)
                                MessageStatus = RetrieveMessageStatus.NotSuccessful
                            End Try

                            ' Flag the command with the response status, Pass the message id back to the server 
                            MyLiveLinkResponse = MyMessage.RetrieveMessage(MyIdentity, myConditionalSessionKey, MyLiveLinkResponse.ResponseNumber, MessageStatus)
                            If MyLiveLinkResponse.ResponseCode = LiveLinkResponseCode.LeaseExpired Then
                                ' This is a long running activity and the lease may have expired in the interim.  So get a new lease and retry.
                                ExpireLease()
                                GetLease()
                                MyLiveLinkResponse = MyMessage.RetrieveMessage(MyIdentity, myConditionalSessionKey, MyLiveLinkResponse.ResponseNumber, MessageStatus)
                            End If

                        Case eRetailer.LiveLinkResponseCode.DownloadFile
                            SetStatus("Message", "Download command received.  Downloading ...")
                            Dim DownloadInfo As String() = MyLiveLinkResponse.ResponseString.Split(";")
                            If DownloadInfo.Length = 2 Then
                                ' Download url is first element
                                Dim url As String = DownloadInfo(0)
                                Dim LocalFile As String = DownloadInfo(1)
                                Dim myError As String = String.Empty
                                Try
                                    If Not Directory.Exists(Path.GetDirectoryName(LocalFile)) Then
                                        Directory.CreateDirectory(Path.GetDirectoryName(LocalFile))
                                    End If
                                    If HttpDownload.GetFile(url, LocalFile, myError, myProxy) Then
                                        SetStatus("Message", "[" & LocalFile & "] has been downloaded.")
                                        MessageStatus = RetrieveMessageStatus.Success
                                        SendLogInfo(SessionMessage.msg_Information, "Download Succeeded", LocalFile)
                                    Else
                                        MessageStatus = RetrieveMessageStatus.NotSuccessful
                                        SetStatus("Error", "Download failed: " & myError)
                                        SendLogInfo(SessionMessage.msg_Error, "Download Failed", myError)
                                    End If
                                Catch ex As Exception
                                    MessageStatus = RetrieveMessageStatus.NotSuccessful
                                    SendLogInfo(SessionMessage.msg_Error, "Download Failed", ex.Message)
                                End Try
                            Else
                                MessageStatus = RetrieveMessageStatus.InvalidMessageFormat
                                SetStatus("Error", "Invalid url and local file name retrieved")
                                SendLogInfo(SessionMessage.msg_Error, "Download Failed", RetrieveMessageStatus.InvalidMessageFormat.ToString)
                            End If
                            ' Send back message response
                            MyLiveLinkResponse = MyMessage.RetrieveMessage(MyIdentity, myConditionalSessionKey, MyLiveLinkResponse.ResponseNumber, MessageStatus)
                            If MyLiveLinkResponse.ResponseCode = LiveLinkResponseCode.LeaseExpired Then
                                ' This is a long running activity and the lease may have expired in the interim.  So get a new lease and retry.
                                ExpireLease()
                                GetLease()
                                MyMessage.RetrieveMessage(MyIdentity, myConditionalSessionKey, MyLiveLinkResponse.ResponseNumber, MessageStatus)
                            End If
                        Case eRetailer.LiveLinkResponseCode.Restart
                            MyLiveLinkResponse = MyMessage.RetrieveMessage(MyIdentity, myConditionalSessionKey, MyLiveLinkResponse.ResponseNumber, RetrieveMessageStatus.Success)
                            If MyLiveLinkResponse.ResponseCode = eRetailer.LiveLinkResponseCode.Success Then
                                LogEvent(EventLogEntryType.Information, "Restart command received from server")
                                SetStatus("Message", "Restart command received.  Restarting in 5 seconds ...")
                                Threading.Thread.Sleep(5000)
                                RestartLiveLink()
                            End If

                        Case eRetailer.LiveLinkResponseCode.DatabaseUpdate
                            SetStatus("Message", "Database update received. Executing...")
                            ' Split the query string on ";"
                            Dim SqlList As String() = MyLiveLinkResponse.ResponseString.Split(";")
                            Dim sSQL, sError As String
                            Dim Result As Boolean = True
                            Dim UseODBC As Boolean = LiveLinkConfiguration.Use_ODBC_DB_Connect

                            ' Execute each query string in the list. Queries are seperated by a ";"
                            For Each sSQL In SqlList
                                If sSQL <> String.Empty Then
                                    If Not MMSGeneric_Execute(UseODBC, sSQL, sError) Then
                                        SetStatus("Message", "Query Failed: [" & sSQL & "]" & vbCrLf & sError)
                                        LogEvent("DatabaseUpdate : Query Failed [" & sSQL & "]" & vbCrLf & sError, EventLogEntryType.Error)
                                        Result = False
                                    End If
                                End If
                            Next

                            ' Check if any of the queries failed
                            If Result Then
                                MessageStatus = RetrieveMessageStatus.Success
                            Else
                                MessageStatus = RetrieveMessageStatus.NotSuccessful
                            End If
                            SetStatus("Message", "Done - " & SqlList.Length & " command(s) executed.")

                            ' Send back message response
                            MyLiveLinkResponse = MyMessage.RetrieveMessage(MyIdentity, myConditionalSessionKey, MyLiveLinkResponse.ResponseNumber, MessageStatus)
                            If MyLiveLinkResponse.ResponseCode = LiveLinkResponseCode.LeaseExpired Then
                                ' This is a long running activity and the lease may have expired in the interim.  So get a new lease and retry.
                                ExpireLease()
                                GetLease()
                                MyMessage.RetrieveMessage(MyIdentity, myConditionalSessionKey, MyLiveLinkResponse.ResponseNumber, MessageStatus)
                            End If

                        Case eRetailer.LiveLinkResponseCode.DatabaseQuery
                            SetStatus("Message", "Database query received. Executing...")
                            ' Split the query string on ";"
                            Dim SqlList As String() = MyLiveLinkResponse.ResponseString.Split(";")
                            Dim sSQL, sError As String
                            Dim Result As Boolean = True
                            Dim UseODBC As Boolean = LiveLinkConfiguration.Use_ODBC_DB_Connect
                            Dim ds As DataSet
                            Dim FullDs As New DataSet
                            Dim Table As DataTable
                            Dim TableCounter As Integer = 0

                            FullDs.DataSetName = "CommandId_" & MyLiveLinkResponse.ResponseNumber.ToString & "-LiveLink_EntityId_" & GetEntityId.ToString

                            ' Execute each query string in the list. Queries are seperated by a ";"
                            For Each sSQL In SqlList
                                If sSQL <> String.Empty Then
                                    If Not MMSGeneric_ListAll(UseODBC, sSQL, "Data", ds, sError, False) Then
                                        SetStatus("Message", "Query Failed: [" & sSQL & "]" & vbCrLf & sError)
                                        LogEvent("DatabaseUpdate : Query Failed [" & sSQL & "]" & vbCrLf & sError, EventLogEntryType.Error)
                                        Result = False
                                    Else
                                        For Each Table In ds.Tables
                                            TableCounter += 1
                                            Table.TableName = "Query-" & TableCounter.ToString
                                            FullDs.Tables.Add(Table.Clone)
                                            FullDs.Merge(Table)
                                        Next
                                    End If
                                End If
                            Next

                            ' Check if any of the queries failed
                            If Result Then
                                MessageStatus = RetrieveMessageStatus.Success
                            Else
                                MessageStatus = RetrieveMessageStatus.NotSuccessful
                            End If
                            SetStatus("Message", "Done - " & TableCounter.ToString & " table(s) retrieved.")

                            ' Send back message response
                            If FullDs.Tables.Count > 0 Then
                                MyMessage.RetrieveMessage(MyIdentity, myConditionalSessionKey, MyLiveLinkResponse.ResponseNumber, MessageStatus, CompressDataSet(FullDs))
                            Else
                                MyMessage.RetrieveMessage(MyIdentity, myConditionalSessionKey, MyLiveLinkResponse.ResponseNumber, MessageStatus)
                            End If

                        Case eRetailer.LiveLinkResponseCode.RetrieveProjectionInfo
                            SetStatus("Message", "Projection Command Received")
                            Dim projectionDate As DateTime
                            If DateTime.TryParse(MyLiveLinkResponse.ResponseString, projectionDate) AndAlso RetrieveProjections(projectionDate) Then
                                MessageStatus = RetrieveMessageStatus.Success
                            Else
                                MessageStatus = RetrieveMessageStatus.NotSuccessful
                            End If
                            ' Flag the command with the response status, Pass the message id back to the server 
                            MyLiveLinkResponse = MyMessage.RetrieveMessage(MyIdentity, myConditionalSessionKey, MyLiveLinkResponse.ResponseNumber, MessageStatus)
                        Case eRetailer.LiveLinkResponseCode.RetrieveFile
                            Dim localFile As String = String.Empty
                            SetStatus("Message", "Retrieve File command received")
                            If RetrieveFile(MyLiveLinkResponse.ResponseString, MyLiveLinkResponse.ResponseNumber, localFile) Then
                                MessageStatus = RetrieveMessageStatus.Success
                                SetStatus("Message", "Done - " & localFile & " retrieved.")
                            Else
                                MessageStatus = RetrieveMessageStatus.NotSuccessful
                            End If
                            MyLiveLinkResponse = MyMessage.RetrieveMessage(MyIdentity, myConditionalSessionKey, MyLiveLinkResponse.ResponseNumber, MessageStatus)
                        Case eRetailer.LiveLinkResponseCode.RetrieveOfflineCashupInfo
                            SetStatus("Message", "Offline Cashup Update Command Received")
                            Dim offlineSyncID As Integer = 0
                            Integer.TryParse(MyLiveLinkResponse.ResponseString, offlineSyncID)
                            If RetrieveOfflineCashupInfo(offlineSyncID) Then
                                MessageStatus = RetrieveMessageStatus.Success
                            Else
                                MessageStatus = RetrieveMessageStatus.NotSuccessful
                            End If
                            ' Flag the command with the response status, Pass the message id back to the server 
                            MyLiveLinkResponse = MyMessage.RetrieveMessage(MyIdentity, myConditionalSessionKey, MyLiveLinkResponse.ResponseNumber, MessageStatus)
                        Case Else
                            SetStatus("Error", "Message type not handled")
                            ' Flag the message as having been received but not handled (3) 
                            MyMessage.RetrieveMessage(MyIdentity, myConditionalSessionKey, MyLiveLinkResponse.ResponseNumber, 3)
                    End Select

                    ' check if command response update was successful and there is another linked message waiting (RecordCount>0 indicates a linked message)
                    If (MessageStatus = RetrieveMessageStatus.Success AndAlso MessageWaiting = True AndAlso multiCommandMessage) Then
                        ' recursively call the RetrieveMessage() function
                        ' this will get all linked messages in sequence
                        RetrieveMessage()
                    End If
                Catch ex As WebException
                    SetStatus("Error", ex.Message)
                    BackOffCommunications(False)
                Catch ex As Exception
                    MessageWaiting = False
                    SetStatus("Error", ex.Message)
                    BackOffCommunications(True)
                End Try
            End If
        End If
    End Sub

    Private Function RetrieveProjections(ByVal projectionDate As DateTime) As Boolean
        UserAccess(False)
        If Is_OK_To_Send() Then
            GetLease()
            If mySessionKey <> "" Then ' If the machine is online, then continue
                Try
                    Dim myMessage As New LiveLinkWebService
                    Dim myLiveLinkResponse As Mx.POS.Common.eRetailer.LiveLinkResponse
                    SetStatus("Updating projections ...", "Retrieving projection info from server")

                    myMessage.Url = myWebService
                    LoadProxy(myMessage)
                    myLiveLinkResponse = myMessage.GetProductionInfo(MyIdentity, mySessionKey, GetEntityId(), projectionDate)
                    Select Case myLiveLinkResponse.ResponseCode
                        Case eRetailer.LiveLinkResponseCode.Success
                            SetStatus("Updating projections ...", "Updating projections")
                            If Projection.UpdateProjections(DecompressDataSet(myLiveLinkResponse.ResponseCompressedData), projectionDate) Then
                                SetStatus("Updating projections ...", "Projection update completed")
                            Else
                                SetStatus("Error updating projections ...", "Projection update did NOT complete")
                            End If
                            Return True
                        Case Else
                            ProcessResponse(myLiveLinkResponse, True)
                    End Select
                Catch ex As WebException
                    SetStatus("Error", ex.Message)
                    BackOffCommunications(False)
                    Return False
                Catch ex As Exception
                    MessageWaiting = False
                    SetStatus("Error", ex.Message)
                    BackOffCommunications(True)
                    Return False
                End Try
            End If
        End If
        Return False
    End Function

    Function RetrieveFile(ByVal command As String, ByVal messageId As Integer, ByRef fullFileName As String) As Boolean
        Dim commandInfo As String() = command.Split(";"c)

        If commandInfo.Length < 2 Then
            SetStatus("Error", "Invalid file information")
            SendLogInfo(SessionMessage.msg_Error, "Retrieve File Failed", RetrieveMessageStatus.InvalidMessageFormat.ToString)
            Return False
        End If

        Dim targetFolder As String = commandInfo(1).Trim
        Dim targetFilename As String
        If commandInfo.Length >= 4 AndAlso commandInfo(3).Trim <> String.Empty Then
            ' The file needs to be renamed
            targetFilename = commandInfo(3).Trim
        Else
            ' Use the source file name
            targetFilename = Path.GetFileName(commandInfo(0).Trim)
        End If
        fullFileName = Path.Combine(targetFolder, targetFilename)
        Dim tempFileName As String = Path.ChangeExtension(Path.Combine(targetFolder, String.Format("~mx_{0}_{1}", Path.GetFileNameWithoutExtension(fullFileName), Path.GetExtension(fullFileName).Replace(".", ""))), ".mx~")
        Dim overwrite As Boolean = False

        If commandInfo.Length >= 3 AndAlso commandInfo(2).Trim = "1" Then
            overwrite = True
        End If

        'Flag 1 - Data is compressed 0- not compressed
        Dim unCompress As Boolean = False
        If commandInfo.Length >= 6 AndAlso commandInfo(5).Trim = "1" Then
            unCompress = True
        End If

        If Not overwrite AndAlso File.Exists(fullFileName) Then
            SetStatus("Error", "Target file already exists")
            SendLogInfo(SessionMessage.msg_Error, "Retrieve File Failed", "File already exists")
            Return False
        End If

        GetLease()
        If mySessionKey <> "" Then ' If the machine is online, then continue
            Try
                Dim myRetrievedFile As New LiveLinkWebService
                Dim myLiveLinkResponse As Mx.POS.Common.eRetailer.LiveLinkResponse
                SetStatus("Retrieving file ...", "Retrieving file from server")

                myRetrievedFile.Url = myWebService
                LoadProxy(myRetrievedFile)
                myLiveLinkResponse = myRetrievedFile.RetrieveFile(MyIdentity, mySessionKey, GetEntityId(), messageId)
                Select Case myLiveLinkResponse.ResponseCode
                    Case eRetailer.LiveLinkResponseCode.Success
                        '------------------------
                        If Not Directory.Exists(targetFolder) Then
                            Directory.CreateDirectory(targetFolder)
                        End If
                        '------------------------
                        ' Download to a temp file and then rename so that the file is not accessed before the download is complete
                        Using ms As MemoryStream = DecompressByteArray(myLiveLinkResponse.ResponseCompressedData)
                            Using fs As New FileStream(tempFileName, FileMode.Create) ' Overwrite if the file exists
                                Using bw As New BinaryWriter(fs)
                                    bw.Write(ms.ToArray)
                                End Using
                            End Using
                        End Using
                        '------------------------
                        If File.Exists(fullFileName) Then
                            ' The file needs to be overwritten
                            File.Delete(fullFileName)
                        End If
                        ' Rename the file
                        File.Move(tempFileName, fullFileName)

                        'If UNCOMPRESS
                        Dim extractError As String = String.Empty
                        If unCompress Then
                            ExtractZip(fullFileName, targetFolder, extractError)
                            If (Not String.IsNullOrEmpty(extractError)) Then
                                Throw New Exception(extractError)
                            End If
                        End If

                        LogEvent(EventLogEntryType.Information, "File retrieval successful ... [{0}]", fullFileName)
                        Return True
                    Case Else
                        ProcessResponse(myLiveLinkResponse, True)
                End Select
            Catch ex As WebException
                SetStatus("Error", ex.Message)
                SendLogInfo(SessionMessage.msg_Error, "Retrieve Failed", ex.Message)
                LogEvent(EventLogEntryType.Error, "File retrieval failed ... {0}", ex)
                BackOffCommunications(False)
                Return False
            Catch ex As Exception
                MessageWaiting = False
                SetStatus("Error", ex.Message)
                SendLogInfo(SessionMessage.msg_Error, "Retrieve Failed", ex.Message)
                LogEvent(EventLogEntryType.Error, "File retrieve failed ... {0}", ex)
                BackOffCommunications(True)
                Return False
            End Try
            Return False
        End If

    End Function

    Private Function RetrieveOfflineCashupSettings() As Boolean

        Dim ok As Boolean = True
        ' create schema
        Try
            DatabaseIntallerService.InstallMxClientDatabaseIfNotAlreadyInstalled(Mx.BusinessObjects.MxClientApplication.OfflineCashManager)

            If (Not StoreInfoService.IsStoreSet) Then
                SetStatus("Cannot synchronise offline cashup data.", "Store must be set before offline cashup settings can be retrieved.")
                Return False
            End If
        Catch ex As Exception
            ok = False
            modLiveLink.LogEvent(ex.Message, EventLogEntryType.Error)
        End Try

        If (Not ok) Then Return False

        UserAccess(False)
        If Is_OK_To_Send() Then
            GetLease()
            If mySessionKey <> "" Then ' If the machine is online, then continue
                Try
                    Dim myMessage As New LiveLinkWebService
                    Dim myLiveLinkResponse As Mx.POS.Common.eRetailer.LiveLinkResponse
                    SetStatus("Synchronising offline cashup data...", "Retrieving offline cashup settings from server")

                    myMessage.Url = myWebService
                    LoadProxy(myMessage)
                    myLiveLinkResponse = myMessage.GetOfflineCashupSettings(MyIdentity, mySessionKey, GetEntityId(), DateTime.Now)

                    Select Case myLiveLinkResponse.ResponseCode
                        Case eRetailer.LiveLinkResponseCode.Success
                            '---------------------------------
                            Dim offlineCashupInfo As Mx.BusinessObjects.OfflineCashManagementServerToClient
                            ' Decompress and Deserialise the offline cashup business object
                            Using ms As MemoryStream = DecompressByteArray(myLiveLinkResponse.ResponseCompressedData)
                                Dim serializer As XmlSerializer = New XmlSerializer(GetType(Mx.BusinessObjects.OfflineCashManagementServerToClient))
                                ms.Position = 0
                                offlineCashupInfo = CType(serializer.Deserialize(ms), Mx.BusinessObjects.OfflineCashManagementServerToClient)
                            End Using
                            SetStatus("Synchronising offline cashup data...", "Saving offline cashup settings")
                            If offlineCashupInfo Is Nothing Then
                                SetStatus("Synchronising offline cashup data...", "No data found")
                            ElseIf OfflineCashup.ProcessOfflineSettingsFromServer(offlineCashupInfo) Then
                                ' need to track if settings are downloaded - can't do anything else until they are complete.
                                mxOfflineCashupSetupSettingsRetrieved = True
                                SetStatus("Synchronising offline cashup data...", "Offline cashup update completed")
                            Else
                                SetStatus("Error synchronising offline cashup settings...", "Offline cashup synchronisation did NOT complete. This may be because the Offline Cash Manager has not been run yet.")
                            End If
                            Return True
                        Case Else
                            ProcessResponse(myLiveLinkResponse, True)
                    End Select
                Catch ex As WebException
                    SetStatus("Error", ex.Message)
                    BackOffCommunications(False)
                    Return False
                Catch ex As Exception
                    MessageWaiting = False
                    SetStatus("Error", ex.Message)
                    BackOffCommunications(True)
                    Return False
                End Try
            End If
        End If
        Return False
    End Function

    Private Function RetrieveOfflineCashupInfo(ByVal offlineSyncID As Integer) As Boolean

        If (Not mxOfflineCashupSetupSettingsRetrieved) Then
            If Not (RetrieveOfflineCashupSettings()) Then
                SetStatus("Cannot synchronise offline cashup data.", "Could not retrieve offline cashup settings.")
                Return False
            End If
        End If

        UserAccess(False)
        If Is_OK_To_Send() Then
            GetLease()
            If mySessionKey <> "" Then ' If the machine is online, then continue
                Try
                    Dim myMessage As New LiveLinkWebService
                    Dim myLiveLinkResponse As Mx.POS.Common.eRetailer.LiveLinkResponse
                    SetStatus("Synchronising offline cashup data...", "Retrieving offline cashup info from server")

                    myMessage.Url = myWebService
                    LoadProxy(myMessage)
                    myLiveLinkResponse = myMessage.GetOfflineCashupInfoByID(MyIdentity, mySessionKey, GetEntityId(), DateTime.Now, offlineSyncID)

                    Select Case myLiveLinkResponse.ResponseCode
                        Case eRetailer.LiveLinkResponseCode.Success
                            Dim offlineCashupInfo As Mx.BusinessObjects.OfflineCashManagementServerToClient
                            ' Decompress and Deserialise the offline cashup business object
                            Using ms As MemoryStream = DecompressByteArray(myLiveLinkResponse.ResponseCompressedData)
                                Dim serializer As XmlSerializer = New XmlSerializer(GetType(Mx.BusinessObjects.OfflineCashManagementServerToClient))
                                ms.Position = 0
                                offlineCashupInfo = CType(serializer.Deserialize(ms), Mx.BusinessObjects.OfflineCashManagementServerToClient)
                            End Using
                            SetStatus("Synchronising offline cashup data...", "Saving offline cashup data")
                            If offlineCashupInfo Is Nothing Then
                                SetStatus("Synchronising offline cashup data...", "No data found")
                                Return False
                            ElseIf OfflineCashup.ProcessOfflineCashupFromServer(offlineCashupInfo) Then
                                SetStatus("Synchronising offline cashup data...", "Offline cashup update completed")
                            Else
                                SetStatus("Error synchronising offline cashup data...", "Offline cashup synchronisation did NOT complete")
                                Return False
                            End If
                            Return True
                        Case Else
                            ProcessResponse(myLiveLinkResponse, True)
                    End Select
                Catch ex As WebException
                    SetStatus("Error", ex.Message)
                    BackOffCommunications(False)
                    Return False
                Catch ex As Exception
                    MessageWaiting = False
                    SetStatus("Error", ex.Message)
                    BackOffCommunications(True)
                    Return False
                End Try
            End If
        End If
        Return False
    End Function

#End Region

#Region " PAR Gateway Interface "

    Private Sub btnParGetTld_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnParGetTld.Click

        ParGetTld(True)
        UpdateParStatus()

    End Sub

    Private Sub btnParImport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnParImport.Click

        If CheckBoxParTLDServer.Checked Then
            ParImport(True, MyParTLDListen)
        Else
            ParImportFile(True)
        End If
        UpdateParStatus()

    End Sub

    Private Sub CheckBoxParTLDServer_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBoxParTLDServer.CheckedChanged
        If CheckBoxParTLDServer.Checked Then
            MyParUseTLDServer = True
            ButtonParTLDListener.Enabled = True
        Else
            MyParUseTLDServer = False
            ButtonParTLDListener.Enabled = False
            ' if the listener is active, then stop it
            If MyParTLDListen Then
                MyParTLDListen = False
                ToggleTLDListener(MyParTLDListen)
            End If
        End If
    End Sub

    Private Sub ButtonParTLDListener_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonParTLDListener.Click
        If MyParTLDListen AndAlso Not MyParTLDServer Is Nothing Then
            MyParTLDListen = False
        Else
            MyParTLDListen = True
        End If
        ToggleTLDListener(MyParTLDListen)
    End Sub

    Private Sub ToggleTLDListener(ByVal Listen As Boolean)
        If Listen Then
            If GetEntityId() > 0 Then
                ButtonParTLDListener.Text = "Stop Listener"
                LabelParTLDStatus.Image = ImageListStatus.Images(2)
                ' start the listener
                MyParImportedCount = 0
                MyParTLDServer = New ParTLDServer(LiveLinkConfiguration.POSType, GetEntityId(), MyConnectionString, MyDontProcessBefore)
                ' start the data capture, pass in all the possible configurations
                ' default values are automatically applied to these parameters if they are not set
                MyParTLDServer.StartCapture(LiveLinkConfiguration.PARTimeToLive, _
                                            LiveLinkConfiguration.PARAutoRecoverAtClose, _
                                            LiveLinkConfiguration.PARAutoRecoverDelay, _
                                            LiveLinkConfiguration.PARRecoverOnEventError, _
                                            LiveLinkConfiguration.PARRecoverOnStatusError, _
                                            LiveLinkConfiguration.PARRecoverOnDataError, _
                                            LiveLinkConfiguration.PARRecoveryMinimumInterval, _
                                            LiveLinkConfiguration.PARDaysToRecoverOnStartUp, _
                                            LiveLinkConfiguration.PARDaysToRecoverOnError, _
                                            LiveLinkConfiguration.PARRecoveryMaxRunTime, _
                                            LiveLinkConfiguration.PARCacheTransactions, _
                                            LiveLinkConfiguration.PARClearTransactionCache, _
                                            LiveLinkConfiguration.PARClearCacheWhileRecovering, _
                                            LiveLinkConfiguration.PARCacheClearFrequency, _
                                            LiveLinkConfiguration.PARCacheClearDelayOnClose, _
                                            LiveLinkConfiguration.PARLockTimeout, _
                                            LiveLinkConfiguration.PARTLDServerLogLevel, _
                                            LiveLinkConfiguration.PARLogLevel)
            End If
        Else
            ButtonParTLDListener.Text = "Start Listener"
            LabelParTLDStatus.Image = ImageListStatus.Images(1)
            ' stop the listener
            If Not MyParTLDServer Is Nothing Then
                MyParTLDServer.EndCapture()
                MyParTLDServer = Nothing
            End If
        End If
    End Sub

    Private Sub ProcessPar()
        If GetEntityId() > 0 Then ' Skip the process if the system has not yet been setup (ie EntityId <= 0)
            ' if the par tld listener is to be used, start it (starting it also does a full import)
            ' if it has already been started, then continue on to the import.
            If Not StartPARListener() Then
                ' if the TLD Server listener is not being used or has already been started... do an import
                If MyParEnabled AndAlso CheckBoxParEnabled.Checked AndAlso (MyParLastPoll.AddMinutes(MyParPollInterval) < DateTime.Now) Then
                    If MyParUseTLDServer Then
                        If MyParTLDServer IsNot Nothing Then
                            ' check the health of the TLD server
                            ' will initialise a recovery if necessary
                            MyParTLDServer.CheckStatus()
                            MyParLastPoll = DateTime.Now
                        End If
                    Else
                        If Format(CurrentDayPAR, "dd-MMM-yyyy") <> Format(Now, "dd-MMM-yyyy") Then
                            MyParPerformFullImport = True
                            CurrentDayPAR = Format(Now, "dd-MMM-yyyy")
                        End If

                        ' do an import using the standard GetTLD
                        If MyParPerformFullImport Then
                            ParGetTld(True)
                            ParImportFile(True)
                            MyParPerformFullImport = False
                        Else
                            ParGetTld(False)
                            ParImportFile(False)
                        End If
                    End If
                    UpdateParStatus()
                End If
            End If
        End If
    End Sub

    Private Sub ParGetTld(ByVal FullImport As Boolean)
        Dim Result As Integer
        Dim GetTLD As String
        Try
            If (btnParGetTld.Enabled) Or (TabControl1.Enabled = False) Then
                btnParGetTld.Enabled = False
                btnParImport.Enabled = False
                SetEndofDayStatus("Get PAR Data...")

                If FullImport Then
                    GetTLD = MyParGetAllTLDsExe
                    Result = Shell(GetTLD, Style:=AppWinStyle.MinimizedNoFocus, Wait:=True)
                    txtParStatus.Text = "Get all TLDs executed."
                Else
                    GetTLD = MyParGetTLDExe
                    Result = Shell(GetTLD, Style:=AppWinStyle.MinimizedNoFocus, Wait:=True)
                    txtParStatus.Text = "GetTLD executed."
                End If
                If Result <> 0 Then
                    txtParStatus.Text = "GetTLD failed. Result code: " & Result
                    LogEvent("Error running GetTLD." & vbCrLf & "Executable: " & GetTLD & vbCrLf & "Result Code: " & Result, EventLogEntryType.Error)
                End If
            End If
            ' Now pause for 5 seconds.  It appears as if shell returns even with Wait=True before the file has been fully released
            Threading.Thread.Sleep(5000)
        Catch ex As Exception
            SetStatus("Error...", "Unable to execute GetTLD")
            SetStatus("", "Error: " & ex.Message)
            txtParStatus.Text = "GetTLD failed. Error: " & ex.Message
            LogEvent("GetTLD execution failed." & vbCrLf & "Executable: " & GetTLD & vbCrLf & "Error: " & ex.Message, EventLogEntryType.Error)
        Finally
            SetEndofDayStatus("")
            btnParGetTld.Enabled = True
            btnParImport.Enabled = True
        End Try
    End Sub

    Private Sub ParImportFile(ByVal FullImport As Boolean)
        If GetEntityId() > 0 Then ' Skip process if store has not yet been setup (ie EntityId <= 0)
            SetEndofDayStatus("Process PAR...")
            MyParDateStart = DateTime.Now
            MyParImportedTotalCount = 0
            MyParSkippedTotalCount = 0

            If FullImport Or MyParLastPoll.Date <> DateTime.Now.Date Then
                Dim CurrentDayOfWeek As Integer = DateTime.Now.DayOfWeek
                For i As Integer = 0 To 6
                    ParImportFileSpecific((i + CurrentDayOfWeek + 1) Mod 7)
                Next
            Else
                ParImportFileSpecific(DateTime.Now.DayOfWeek)
            End If

            MyParLastPoll = DateTime.Now
            MyParFirstPollDone = True
            SetEndofDayStatus("")
        End If
    End Sub

    Private Sub ParImportFileSpecific(ByVal FileNumber As Integer)
        Try
            If (btnParImport.Enabled) Or (TabControl1.Enabled = False) Then
                btnParGetTld.Enabled = False
                btnParImport.Enabled = False

                Dim strFileName As String = MyParImportFilePath & "TLD." & FileNumber

                If Not File.Exists(strFileName) Then
                    txtParStatus.Text = "Import file " & Path.GetFileName(strFileName) & " does not exist."
                    SetStatus("Error...", txtParStatus.Text)
                Else
                    txtParStatus.Text = "Opening file..."

                    MyParImportedCount = 0
                    MyParSkippedCount = 0

                    MyParFile = New ParFile(MyConnectionString)
                    If MyParFile.Import(strFileName) = False Then
                        SetStatus("Error...", "Error importing file TLD." & FileNumber.ToString)
                    End If
                    MyParFile = Nothing

                    ' Archive file.

                    ArchiveFile(strFileName, ArchiveFolderStyle.Archive_yyyy_MM_dd, ArchiveFileNameStyle.orig_A_yyyyMMdd_HHmmss)

                    'Dim importFile As New FileInfo(strFileName)
                    'Dim archiveFilePath As String = Path.Combine(MyParArchiveFolderPath, DateTime.Now.ToString("yyyyMMddhhmmss") & "_" & importFile.Name)
                    'importFile.MoveTo(archiveFilePath)

                    txtParStatus.Text = "Finished. " & MyParImportedTotalCount & " records in " & DateTime.Now.Subtract(MyParDateStart).TotalSeconds & " seconds. " & vbCrLf & MyParSkippedCount & " records skipped."
                    SetStatus("", "TLD." & FileNumber.ToString & " imported.  Records: " & MyParImportedCount.ToString & ", skipped: " & MyParSkippedCount)
                End If

                btnParGetTld.Enabled = True
                btnParImport.Enabled = True
            End If

        Catch ex As Exception
            LogEvent("Error during ParImportFileSpecific (TLD." & FileNumber.ToString & ")" & vbCrLf & "Error: " & ex.Message, EventLogEntryType.Error)
        End Try

    End Sub

    Private Sub MyParFile_FileRecordImported() Handles MyParFile.RecordImported

        MyParImportedCount += 1
        MyParImportedTotalCount += 1

        txtParStatus.Text = "Importing file " & Path.GetFileName(MyParFile.FileName) & " - record " & MyParImportedCount

        Application.DoEvents()

    End Sub

    Private Sub CheckBoxParEnabled_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBoxParEnabled.CheckedChanged

        MyParPerformFullImport = True
        MyParLastPoll = DateTime.MinValue 'DateTime.Now
        UpdateParStatus()

    End Sub

    Private Sub UpdateParStatus()

        txtParCurrentTime.Text = DateTime.Now.ToString("h:mm tt")

        txtParLastPoll.Text = IIf(MyParFirstPollDone, MyParLastPoll.ToString("h:mm tt"), "")
        txtParNextPoll.Text = IIf(CheckBoxParEnabled.Checked, IIf(MyParLastPoll = DateTime.MinValue, "0 min", CInt(MyParLastPoll.AddMinutes(MyParPollInterval).Subtract(DateTime.Now).TotalMinutes) & " min"), "")

        Application.DoEvents()
    End Sub

#Region " PAR TLD Server "

    Private Function StartPARListener() As Boolean
        If MyParEnabled And MyParUseTLDServer And MyParTLDListen Then
            If MyParTLDServer Is Nothing Then
                ToggleTLDListener(MyParTLDListen)
                'if the tld server is being used and has just been started then return true
                Return True
            End If
        End If
        ' return false if the tld server listener has already been started
        Return False
    End Function

    Private Sub StopPARListener()
        If MyParEnabled Then
            If MyParTLDListen AndAlso Not MyParTLDServer Is Nothing Then
                MyParTLDListen = False
                ToggleTLDListener(MyParTLDListen)
            End If
        End If
    End Sub

    Private Sub ParImport(ByVal FullImport As Boolean, Optional ByVal InitiateRecovery As Boolean = False)
        If GetEntityId() > 0 Then ' Skip process if store has not yet been setup (ie EntityId <= 0)
            SetEndofDayStatus("Process PAR...")
            MyParDateStart = DateTime.Now
            MyParImportedTotalCount = 0
            MyParSkippedTotalCount = 0

            If FullImport Or MyParLastPoll.Date <> DateTime.Now.Date Then
                ' reset the import count for the new day, or for full import
                MyParImportedCount = 0
                MyParTLDDayServer = New ParTLDDayServer(LiveLinkConfiguration.POSType, GetEntityId(), MyConnectionString, MyParTLDNumberOfDaysToRecover, MyDontProcessBefore)
            Else
                ' recover the last 3 days by default
                MyParTLDDayServer = New ParTLDDayServer(LiveLinkConfiguration.POSType, GetEntityId(), MyConnectionString, 3, MyDontProcessBefore)
            End If
            MyParTLDDayServer.Import(InitiateRecovery)

            MyParLastPoll = DateTime.Now
            MyParFirstPollDone = True
            SetEndofDayStatus("")
        End If
    End Sub

    Delegate Sub UpdateStatusText(ByVal Text As String)

    Private Sub UpdateParStatusText(ByVal Text As String)
        txtParStatus.Text = Text
    End Sub

    Private Sub MyParFile_RecordImported() Handles MyParTLDDayServer.RecordImported, MyParTLDServer.RecordImported

        MyParImportedCount += 1
        MyParImportedTotalCount += 1

        Me.Invoke(New UpdateStatusText(AddressOf UpdateParStatusText), New Object() {"Importing record from TLD Server - record " & MyParImportedCount})

        Application.DoEvents()

    End Sub


    Private Sub MyParFile_RecordSkipped() Handles MyParFile.RecordSkipped, MyParTLDDayServer.RecordSkipped, MyParTLDServer.RecordSkipped

        MyParSkippedCount += 1
        MyParSkippedTotalCount += 1

    End Sub

#End Region

#End Region

#Region " Aloha Gateway Interface "

    ' External function to call
    Public Sub UpdateAlohaProcessDate(ByVal processDate As String)
        Me.Invoke(New UpdateProcessDate(AddressOf AlohaProcessDate), New Object() {processDate})
    End Sub

    ' This is a delegate function that is called from any POS integration to update the process date
    Delegate Sub UpdateProcessDate(ByVal processDate As String)

    ' Updates the Aloha process date
    Private Sub AlohaProcessDate(ByVal processDate As String)
        txtAlohaProcessDate.Text = processDate
        DateTime.TryParse(processDate, dtpAlohaProcessDate.Value)
    End Sub

    Private Sub ButtonAlohaProcessDateApply_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonAlohaProcessDateApply.Click
        If dtpAlohaProcessDate.Value <> DateTime.MinValue Then
            Dim sql As String = String.Format("UPDATE tbStoreInfo SET LastDate='{0:dd-MMM-yyyy}'", dtpAlohaProcessDate.Value)
            Dim errMsg As String = String.Empty
            ' update the LastDate field in tbStoreInfo
            If Not MMSGeneric_Execute(sql, errMsg) Then
                LogEvent(EventLogEntryType.Error, "Failed to update Aloha date to process from. SQL Error:{0}{0}{1}{0}", vbCrLf, errMsg)
            Else
                txtAlohaProcessDate.Text = dtpAlohaProcessDate.Value.ToString("dd-MMM-yyyy")
            End If
        End If
    End Sub

    Private Sub StripIndexBit(ByVal FileName As String)
        Try
            Dim i As Integer
            Dim fs As FileStream
            Dim Retry As Boolean
            For i = 1 To 3 ' Attempt to open the file exclusively 3 times, waiting 5 seconds after each failed attempt
                Try
                    Retry = False
                    fs = File.Open(FileName, FileMode.Open, FileAccess.Write, FileShare.None)
                    fs.Seek(28, SeekOrigin.Begin) ' The 28th byte contains a 0 if no corresponding index file exists or 1 if one does
                    fs.WriteByte(0) ' Disable index requirement
                Catch ex As Exception
                    Retry = True
                Finally
                    Try
                        fs.Close()
                    Catch ex As Exception
                        Retry = True
                    End Try
                End Try
                If Not Retry Then
                    Exit Sub ' Break here if the update was successful
                End If
                Threading.Thread.Sleep(5000)
            Next i
        Catch ex As Exception
        End Try
    End Sub

    Private Sub RemoveIndexBitFromDBFs(ByVal Folder As String)
        ' Certain errors are occurring on site that cannot be reproduced in the dev environment.
        ' The "Index Not Found" error on the TDR.DBF file is one such error and this routine is a workaround.
        ' It Changes Byte 28 of the DBF file (part of the DBF header) to indicate that an index is not required.
        Try
            StripIndexBit(Path.Combine(Folder, "TDR.DBF"))
            StripIndexBit(Path.Combine(Folder, "RSN.DBF"))
            StripIndexBit(Path.Combine(Folder, "GNDBREAK.DBF"))
            StripIndexBit(Path.Combine(Folder, "EMP.DBF"))
            StripIndexBit(Path.Combine(Folder, "ITM.DBF")) ' Not sure about this one
            StripIndexBit(Path.Combine(Folder, "CMP.DBF"))
            StripIndexBit(Path.Combine(Folder, "PRO.DBF"))
            StripIndexBit(Path.Combine(Folder, "ADJTIME.DBF"))
            StripIndexBit(Path.Combine(Folder, "GNDITEM.DBF"))
            StripIndexBit(Path.Combine(Folder, "CAT.DBF"))
            StripIndexBit(Path.Combine(Folder, "GNDDEPST.DBF"))
        Catch ex As Exception
        End Try
    End Sub

    Private Sub ProcessAloha(Optional ByVal Force As Boolean = False)
        ToggleSendButtons(False)
        If GetEntityId() > 0 Then ' Skip the process if the system has not yet been setup (ie EntityId <= 0)
            If MyAlohaEnabled And CheckBoxAlohaEnabled.Checked And _
                ((MyAlohaLastPoll.AddMinutes(MyAlohaPollInterval) < DateTime.Now) Or Force) Then
                Try
                    Dim MyAlohaPreviousDaysToCheck As Integer = LiveLinkConfiguration.AlohaPreviousDaysToCheck
                    Dim sConnect_OleDB As String = LiveLinkConfiguration.OleDB_DB_Connect
                    Dim sConnect_OleDB_Template As String = sConnect_OleDB
                    Dim AlohaImportFilePath As String = LiveLinkConfiguration.AlohaImportFilePath
                    Dim AlohaImportFilePath_Template As String = AlohaImportFilePath
                    Dim sCurrentDOB As String = String.Empty
                    Dim sCurrentCheck As String = String.Empty
                    Dim myEntityId As Integer = GetEntityId()
                    Dim sPOSType As String = LiveLinkConfiguration.POSType
                    Dim sFieldListInsert As String = GettbSalesMainFieldsForInsert(sPOSType)

                    MyAlohaFirstPollDone = True
                    Mx.POS.Aloha.AlohaV1.GetDoneFromtbStoreInfo(sCurrentDOB, sCurrentCheck)

                    If sCurrentDOB <> String.Empty Then
                        Dim theDate As DateTime = Now
                        ' Determine how many days back to process with a maximum
                        Dim DaysToProcess As Integer = Max(Min(DateDiff(DateInterval.Day, CDate(sCurrentDOB).Date, theDate.Date), MyAlohaPreviousDaysToCheck), LiveLinkConfiguration.AlohaPreviousDaysToIncludeInProcess)
                        Dim i As Integer
                        Dim GrindFolder As String

                        'First perform the Grind on the current day's data
                        If Not chkSkipGrind.Checked Then
                            SetEndofDayStatus("Grinding ...")
                            AlohaGrind()
                        End If

                        SetEndofDayStatus("Importing ...")
                        ShowDebug("Processing " & DaysToProcess & " days.", DebugModes.Verbose)
                        For i = 0 To DaysToProcess
                            'Start with the oldest day first
                            If i = DaysToProcess Then
                                GrindFolder = "DATA"
                            Else
                                GrindFolder = Format(DateAdd(DateInterval.Day, i - DaysToProcess, theDate), "yyyyMMdd")
                            End If
                            If Not ((GrindFolder = "DATA") And chkSkipData.Checked) Then
                                sConnect_OleDB = sConnect_OleDB_Template.Replace("[DataDir]", GrindFolder)
                                AlohaImportFilePath = AlohaImportFilePath_Template.Replace("[DataDir]", GrindFolder)
                                If Directory.Exists(AlohaImportFilePath) Then
                                    SetStatus("", "Processing Grind data [" & GrindFolder & "]")
                                    txtAlohaStatus.Text = "Importing Grind information (" & GrindFolder & ")"
                                    ShowDebug(txtAlohaStatus.Text, DebugModes.Verbose)
                                    RemoveIndexBitFromDBFs(AlohaImportFilePath)
                                    Application.DoEvents()
                                    If MyAlohaVersion = "VER2" Then
                                        ' dispose of the previous class instance if it is not nothing
                                        If MyAloha IsNot Nothing Then
                                            MyAloha.Dispose()
                                            MyAloha = Nothing
                                        End If
                                        ' create new instance of the aloha interface
                                        MyAloha = New AlohaV2(sConnect_OleDB, myEntityId, sFieldListInsert, MyAlohaServiceType, MyAlohaQuickServiceSOS, MyAlohaTimeAttendanceEnabled, MyAlohaServiceTypeMapping, AlohaImportFilePath)
                                        MyAloha.Process()
                                        MyAloha.Dispose()
                                        MyAloha = Nothing

                                        If CancelFlag Then
                                            SetStatus("", String.Format("User cancelled processing ({0})", GrindFolder))
                                            Exit For
                                        End If
                                    Else
                                        Dim alohaV1 As AlohaV1 = New AlohaV1()
                                        If Not alohaV1.AlohaImportFile(sConnect_OleDB, myEntityId, sFieldListInsert, MyAlohaServiceType, MyAlohaQuickServiceSOS) Then
                                            ' If an error occurred while processing the file, do not 
                                            ' process any more files as this will cause any outstanding transactions 
                                            ' in the error file to be skipped.
                                            If Not CancelFlag Then
                                                SetStatus("Error", "An error occurred while processing (" & GrindFolder & ")")
                                            Else
                                                SetStatus("", "User cancelled processing (" & GrindFolder & ")")
                                            End If
                                            Exit For
                                        End If
                                    End If
                                End If
                            End If
                        Next
                    Else
                        SetStatus("Error", "Unable to retrieve latest processed date")
                    End If
                Catch ex As Exception
                    SetStatus("Error", ex.Message)
                Finally
                    MyAlohaLastPoll = Now
                    UpdateAlohaStatus()
                    txtAlohaStatus.Text = ""
                    SetEndofDayStatus("")
                End Try
            End If
        Else
            SetStatus("", "Site not configured - skipping Aloha process")
        End If
        ToggleSendButtons(True)
    End Sub

    Private Sub AlohaGrind(Optional ByVal GrindFolder As String = "DATA")
        Dim GrindProcessId As Integer
        Dim GrindProcesses() As Process
        Dim GrindProcess As Process
        Dim GrindStartTime As DateTime
        Try
            If (btnAlohaImport.Enabled) Or (TabControl1.Enabled = False) Then
                btnAlohaImport.Enabled = False
                SetEndofDayStatus("Get Aloha Data ...")
                ' Check to see whether the Grind process is already running and wait a specified amount of time 
                ' for it to complete.  If it is still running after this time.
                txtAlohaStatus.Text = "Waiting for Grind process to complete"
                GrindStartTime = Now
                GrindProcesses = Process.GetProcessesByName(Path.GetFileNameWithoutExtension(MyAlohaGrindExe))
                While (GrindProcesses.Length > 0) And (DateDiff(DateInterval.Second, GrindStartTime, Now) < GrindProcessTimeout)
                    Threading.Thread.Sleep(100) ' Prevents LiveLink from consuming 100% CPU while waiting
                    GrindProcesses = Process.GetProcessesByName(Path.GetFileNameWithoutExtension(MyAlohaGrindExe))
                End While
                If GrindProcesses.Length <> 0 Then ' there must be blocked grind Processes (eg Error Dialog box waiting to be cleared)
                    SetStatus("", "Terminating locked Grind processes")
                    txtAlohaStatus.Text = "Terminating locked Grind processes"
                    For Each GrindProcess In GrindProcesses
                        Try
                            If GrindProcess.Id > 0 Then ' The DOt Net process bug may cause process arrays with dummy processes to be returned.
                                GrindProcess.Kill() ' ignore any errors in case these processes have terminated in the meantime
                            End If
                        Catch ex As Exception
                        End Try
                    Next
                End If
                Try
                    txtAlohaStatus.Text = "Grinding " & GrindFolder
                    GrindStartTime = Now
                    GrindProcessId = Shell("""" & MyAlohaGrindExe & """ /DATE " & GrindFolder & " /NoWindow", AppWinStyle.MinimizedFocus, True, GrindProcessTimeout * 1000)
                    If GrindProcessId <> 0 Then
                        SetStatus("Error", "The Grind process did not automatically terminate")
                        Try
                            GrindProcess.Kill()
                        Catch ex As Exception
                        End Try
                    Else
                        SetStatus("", "Grind process completed")
                        txtAlohaStatus.Text = "Grind process completed"
                    End If
                Catch ex As Exception
                    SetStatus("Error", "Unable to Grind data - " & ex.Message)
                End Try
                '                txtParStatus.Text = "Grind executed."
            End If

        Catch ex As Exception
            SetStatus("Error", ex.Message)
        Finally
            SetEndofDayStatus("")
            btnAlohaImport.Enabled = True
        End Try
    End Sub

    Private Sub UpdateAlohaStatus()
        txtAlohaCurrentTime.Text = DateTime.Now.ToString("h:mm tt")
        txtAlohaLastPoll.Text = IIf(MyAlohaFirstPollDone, MyAlohaLastPoll.ToString("h:mm tt"), "")
        txtAlohaNextPoll.Text = IIf(CheckBoxAlohaEnabled.Checked, IIf(MyAlohaLastPoll = DateTime.MinValue, "0 min", CInt(MyAlohaLastPoll.AddMinutes(MyAlohaPollInterval).Subtract(DateTime.Now).TotalMinutes) & " min"), "")
        Application.DoEvents()
    End Sub

    Private Sub btnAlohaImport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAlohaImport.Click
        If Not Processing Then
            Processing = True
            SetEndofDayStatus("Grinding ...")
            ProcessAloha(True)
            SetEndofDayStatus("")
            Processing = False
        End If
    End Sub

    Private Sub CheckBoxAlohaEnabled_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBoxAlohaEnabled.CheckedChanged
        MyAlohaEnabled = CheckBoxAlohaEnabled.Checked
        UpdateAlohaStatus()
    End Sub

#End Region

#Region " Micros Gateway Interface "

    Private Sub ProcessMicros(Optional ByVal Force As Boolean = False)
        ToggleSendButtons(False)
        If GetEntityId() > 0 Then ' Skip the process if the system has not yet been setup (ie EntityId <= 0)
            If MyMicrosEnabled And CheckBoxMicrosEnabled.Checked And _
                ((MyMicrosLastPoll.AddMinutes(MyMicrosPollInterval) < DateTime.Now) Or Force) Then
                Try
                    Dim MyMicrosPreviousDaysToCheck As Integer = Max(Min(LiveLinkConfiguration.MicrosPreviousDaysToCheck, 14), 1)
                    Dim MicrosDontProcessBefore As String = LiveLinkConfiguration.MicrosDontProcessBefore
                    Dim StartDate As DateTime
                    Dim CurrentProcessingDate As DateTime
                    Dim EndDate As DateTime
                    Dim sConnect_OleDB As String = LiveLinkConfiguration.OleDB_DB_Connect
                    Dim sConnect_OleDB_Template As String = sConnect_OleDB
                    Dim MicrosImportFilePath As String = LiveLinkConfiguration.MicrosImportFilePath
                    Dim MicrosImportFilePath_Template As String = MicrosImportFilePath
                    Dim sCurrentDOB As String
                    Dim sCurrentCheck As String
                    Dim myEntityId As Integer = GetEntityId()
                    Dim sPOSType As String = LiveLinkConfiguration.POSType
                    Dim MicrosUseCheckDetailTax As Boolean = LiveLinkConfiguration.MicrosUseCheckDetailTax
                    Dim sFieldListInsert As String = GettbSalesMainFieldsForInsert(sPOSType)
                    Dim ChecksProcessed As Integer
                    Dim TimeRecordsProcessed As Integer = 0
                    Dim TotalChecksProcessed As Integer = 0
                    Dim TotalTimeRecordsProcessed As Integer = 0
                    Dim NoSaleRecordsProcessed As Integer = 0
                    Dim TotalNoSaleRecordsProcessed As Integer = 0
                    Dim timeNow As DateTime = DateTime.Now

                    MyMicrosFirstPollDone = True
                    If Not IsDate(MicrosDontProcessBefore) Then
                        MicrosDontProcessBefore = Format(DateTime.MinValue.Date, "dd-MMM-yyyy")
                    End If
                    EndDate = GetLatestMicrosClosedCheck.Date
                    If (MyMicrosLastPoll = DateTime.MinValue) Or (timeNow.Day <> MyMicrosLastPoll.Day) Or Force Then
                        ' If LiveLink has just started, or the day has rolled over, go back and check the past x days
                        ' for any old transactions that have just been closed
                        SetStatus("", "Checking previous days for missing checks")
                        StartDate = DateAdd(DateInterval.Day, -MyMicrosPreviousDaysToCheck, EndDate)
                    Else
                        StartDate = EndDate
                        ' if time is within X hours of midnight, then continue to poll the previous business day to make sure no sales are left behind.
                        If (timeNow.Hour < LiveLinkConfiguration.MicrosBusinessDayOffsetHours) Then
                            SetStatus("", "Checking previous business day for missing checks")
                            StartDate = StartDate.AddDays(-1)
                        End If
                    End If
                    ' Now make sure that we don't process any data before the specified date in the config file
                    If StartDate < CDate(MicrosDontProcessBefore) Then
                        StartDate = CDate(MicrosDontProcessBefore)
                    End If
                    CurrentProcessingDate = StartDate
                    While CurrentProcessingDate <= EndDate
                        SetStatus("", "Importing Micros data [" & Format(CurrentProcessingDate, "dd-MMM-yyyy") & "]")
                        If Not MicrosImportData(CurrentProcessingDate, myEntityId, MyMicrosGatewayVersion, ChecksProcessed, TimeRecordsProcessed, NoSaleRecordsProcessed, MicrosUseCheckDetailTax) Then
                            If Not CancelFlag Then
                                SetStatus("Error", "An error occurred while importing")
                            Else
                                SetStatus("", "User cancelled processing")
                            End If
                            SetStatus("", "Number of checks processed: " & ChecksProcessed)
                            If TimeRecordsProcessed > 0 Then
                                SetStatus("", "Time/attendance processed: " & TimeRecordsProcessed)
                                TotalTimeRecordsProcessed += TimeRecordsProcessed
                            ElseIf TimeRecordsProcessed < 0 Then
                                SetStatus("Error", "Error processing time/attendance data")
                            End If
                            TotalChecksProcessed += ChecksProcessed
                            Exit While
                        End If
                        SetStatus("", "Number of checks processed: " & ChecksProcessed)
                        If TimeRecordsProcessed > 0 Then
                            SetStatus("", "Time/attendance processed: " & TimeRecordsProcessed)
                            TotalTimeRecordsProcessed += TimeRecordsProcessed
                        ElseIf TimeRecordsProcessed < 0 Then
                            SetStatus("Error", "Error processing time/attendance data")
                        End If
                        If NoSaleRecordsProcessed > 0 Then
                            SetStatus("", "NoSales processed: " & NoSaleRecordsProcessed)
                            TotalNoSaleRecordsProcessed += NoSaleRecordsProcessed
                        ElseIf NoSaleRecordsProcessed < 0 Then
                            SetStatus("Error", "Error processing NoSale data")
                        End If
                        CurrentProcessingDate = DateAdd(DateInterval.Day, 1, CurrentProcessingDate)
                        TotalChecksProcessed += ChecksProcessed
                    End While
                    SetStatus("", "Importing Micros data complete [" & TotalChecksProcessed & " check(s)]")
                    If TotalTimeRecordsProcessed > 0 Then
                        SetStatus("", "Importing T/A data complete [" & TotalTimeRecordsProcessed & " record(s)]")
                    End If
                    If TotalNoSaleRecordsProcessed > 0 Then
                        SetStatus("", "Importing NoSale data complete [" & TotalNoSaleRecordsProcessed & " record(s)]")
                    End If
                Catch ex As Exception
                    SetStatus("Error", ex.Message)
                Finally
                    MyMicrosLastPoll = Now
                    UpdateMicrosStatus()
                    'txtMicrosStatus.Text = ""
                    SetEndofDayStatus("")
                End Try
            End If
        Else
            SetStatus("", "Site not configured - skipping Micros process")
        End If
        ToggleSendButtons(True)
    End Sub

    ' select which version of micros to use for importing data
    Private Function MicrosImportData(ByVal TradingDate As Date, ByVal EntityId As Integer, ByVal MicrosGatewayVersion As String, ByRef ChecksProcessed As Integer, ByRef TimeRecordsProcessed As Integer, ByRef NoSaleRecordsProcessed As Integer, ByVal MicrosUseCheckDetailTax As Boolean) As Boolean
        Dim sPOSType As String = LiveLinkConfiguration.POSType

        Try
            Select Case sPOSType.ToUpper
                Case "MICROS"
                    Return Micros3700.Micros3700_ImportData(TradingDate, EntityId, MicrosGatewayVersion, ChecksProcessed, TimeRecordsProcessed, NoSaleRecordsProcessed, MicrosUseCheckDetailTax, MyMicrosRevenueCentreFilter)
                Case "MICROS9700"
                    Return Micros9700_ImportData(TradingDate, EntityId, MicrosGatewayVersion, ChecksProcessed, TimeRecordsProcessed, MicrosUseCheckDetailTax)
                Case Else
                    Return False
            End Select
        Catch ex As Exception
        End Try
    End Function

    ' select which version of micros to use
    Private Function GetLatestMicrosClosedCheck() As Date
        Dim sPOSType As String = LiveLinkConfiguration.POSType

        Try
            Select Case sPOSType.ToUpper
                Case "MICROS"
                    Return Micros3700.Micros3700_GetLatestClosedCheck(MyMicrosRevenueCentreFilter)
                Case "MICROS9700"
                    Return Micros9700_GetLatestClosedCheck()
            End Select
        Catch ex As Exception
        End Try
        Return DateTime.MinValue
    End Function

    Private Sub UpdateMicrosStatus()
        txtMicrosCurrentTime.Text = DateTime.Now.ToString("h:mm tt")
        txtMicrosLastPoll.Text = IIf(MyMicrosFirstPollDone, MyMicrosLastPoll.ToString("h:mm tt"), "")
        txtMicrosNextPoll.Text = IIf(CheckBoxMicrosEnabled.Checked, IIf(MyMicrosLastPoll = DateTime.MinValue, "0 min", CInt(MyMicrosLastPoll.AddMinutes(MyMicrosPollInterval).Subtract(DateTime.Now).TotalMinutes) & " min"), "")
        Application.DoEvents()
    End Sub

    Private Sub btnMicrosImport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMicrosImport.Click
        If Not Processing Then
            Processing = True
            SetEndofDayStatus("Importing ...")
            ProcessMicros(True)
            SetEndofDayStatus("")
            Processing = False
        End If
    End Sub

    Private Sub CheckBoxMicrosEnabled_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBoxMicrosEnabled.CheckedChanged
        MyMicrosEnabled = CheckBoxMicrosEnabled.Checked
        UpdateMicrosStatus()
    End Sub

#End Region

#Region " POSI Gateway Interface "

    Private Sub btnPosiImport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPosiImport.Click

        PosiImportDatabase()
        UpdatePosiStatus()

    End Sub

    Private Sub ProcessPosi()

        If MyPosiEnabled And CheckBoxPosiEnabled.Checked And MyPosiLastPoll.AddMinutes(MyPosiPollInterval) < DateTime.Now Then
            PosiImportDatabase()
            UpdatePosiStatus()
        End If

    End Sub

    Private Sub PosiImportDatabase()

        If btnPosiImport.Enabled Then
            btnPosiImport.Enabled = False

            Dim dateStart As DateTime = DateTime.Now
            MyPosiImportedCount = 0
            MyPosiSkippedCount = 0

            MyPosiDatabase = New PosiDatabase(MyConnectionString)
            MyPosiDatabase.Import(MyPosiConnectionString)
            MyPosiDatabase = Nothing

            MyPosiLastPoll = DateTime.Now
            MyPosiFirstPollDone = True

            txtPosiStatus.Text = "Finished. " & MyPosiImportedCount & " records imported from " & Path.GetFileName(MyPosiConnectionString) & " in " & DateTime.Now.Subtract(dateStart).TotalSeconds & " seconds. " & vbCrLf & MyPosiSkippedCount & " records skipped."

            btnPosiImport.Enabled = True
        End If

    End Sub

    Private Sub MyPosiFile_RecordImported() Handles MyPosiDatabase.RecordImported

        MyPosiImportedCount = MyPosiImportedCount + 1

        txtPosiStatus.Text = "Importing record " & MyPosiImportedCount

        Application.DoEvents()

    End Sub

    Private Sub MyPosiFile_RecordSkipped() Handles MyPosiDatabase.RecordSkipped

        MyPosiSkippedCount = MyPosiSkippedCount + 1

    End Sub

    Private Sub CheckBoxPosiEnabled_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBoxPosiEnabled.CheckedChanged

        MyPosiLastPoll = DateTime.Now
        UpdatePosiStatus()

    End Sub

    Private Sub UpdatePosiStatus()

        txtPosiCurrentTime.Text = DateTime.Now.ToString("h:mm tt")

        txtPosiLastPoll.Text = IIf(MyPosiFirstPollDone, MyPosiLastPoll.ToString("h:mm tt"), "")
        txtPosiNextPoll.Text = IIf(CheckBoxPosiEnabled.Checked, CInt(MyPosiLastPoll.AddMinutes(MyPosiPollInterval).Subtract(DateTime.Now).TotalMinutes) & " min", "")

    End Sub

#End Region

#Region " Compris Gateway Interface "

    Private Function ProcessCompris(Optional ByVal Force As Boolean = False) As Boolean
        Dim dataProcessed As Boolean = False

        If GetEntityId() > 0 Then
            If MyComprisEnabled And _
                CheckBoxComprisEnabled.Checked = True And _
                (MyComprisLastPoll.AddMinutes(MyComprisPollInterval) < DateTime.Now Or Force) Then
                Try
                    ButtonComprisImport.Enabled = False
                    MyCompris = (New ComprisLoader(LiveLinkConfiguration.ComprisVariantType)).LoadPos(MyConnectionString, MyComprisXMLFilePath, LiveLinkConfiguration.DaysBeforeObsolete)
                    MyCompris.ImportXmlData()

                    ' check if any files were processed
                    If (MyCompris.ProcessedFileCount > 0) Then
                        ' only set data as processed when files have been processed
                        dataProcessed = True
                    End If

                    MyCompris.Dispose()
                    MyComprisLastPoll = Now
                    MyComprisFirstPollDone = True


                Catch ex As Exception
                    LogEvent(EventLogEntryType.Error, "Exception: [ProcessCompris]  {0}{1}{1}{2}", ex.Message, vbCrLf, ex.StackTrace)
                    SetStatus("Error", ex.Message)
                Finally
                    ButtonComprisImport.Enabled = True
                    UpdateComprisStatus()
                End Try
            End If
        End If
        ' return boolean indicating if data was processed
        Return dataProcessed
    End Function

    Private Sub UpdateComprisStatus()
        txtComprisCurrentTime.Text = DateTime.Now.ToString("h:mm tt")
        txtComprisLastPoll.Text = IIf(MyComprisFirstPollDone, MyComprisLastPoll.ToString("h:mm tt"), "")
        txtComprisNextPoll.Text = IIf(CheckBoxComprisEnabled.Checked, IIf(MyComprisLastPoll = DateTime.MinValue, "0 min", CInt(MyComprisLastPoll.AddMinutes(MyComprisPollInterval).Subtract(DateTime.Now).TotalMinutes) & " min"), "")
        Application.DoEvents()
    End Sub

    Private Sub ButtonComprisProcess_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonComprisImport.Click
        If Not Processing Then
            Processing = True
            SetEndofDayStatus("Processing...")
            ProcessCompris(True)
            SetEndofDayStatus("")
            Processing = False
        End If
    End Sub


    Private Sub MyComprisListener_FileCreated(ByVal sender As Object, ByVal args As System.IO.FileSystemEventArgs) Handles MyComprisListener.FileCreated
        tabCompris.Invoke(New ProcessComprisAndSendDelegate(AddressOf ProcessComprisAndSend))
    End Sub

    Private Delegate Sub ProcessComprisAndSendDelegate()

    Private Sub ProcessComprisAndSend()
        ' check if there are already tasks running
        If Not Processing Then
            ' set processing flag to true, this prevents other processing from occurring while this is running
            Processing = True

            ' process compris data and check if this was successfull
            If ProcessCompris(True) Then
                ' send any processed data to the server
                SendDataToServer()
                ' Check for any pending messages
                RetrieveMessage()
            End If

            ' reset processing flag so other tasks can be run
            Processing = False
        End If
    End Sub

    Private Sub StartComprisListener()
        Try
            ' check if compris is enabled
            If (LiveLinkConfiguration.ComprisEnabled AndAlso LiveLinkConfiguration.ComprisListenerEnabled) Then
                ' check if object has been created
                If (MyComprisListener Is Nothing) Then
                    ' create new listener object
                    MyComprisListener = New MxCompris.ComprisListener()
                End If
                ' start the listener
                MyComprisListener.Start()
            End If
        Catch ex As Exception
            LogEvent(EventLogEntryType.Error, "Exception occurred starting the compris file listener. Exception: {0}", ex.ToString)
        End Try
    End Sub

    Private Sub StopComprisListener()
        Try
            ' check if compris is enabled
            If (LiveLinkConfiguration.ComprisEnabled AndAlso LiveLinkConfiguration.ComprisListenerEnabled) Then
                ' check if compris listener has been created (only stop if it has been created)
                If (MyComprisListener IsNot Nothing) Then
                    MyComprisListener.Stop()
                End If
            End If
        Catch ex As Exception
            LogEvent(EventLogEntryType.Error, "Exception occurred stopping the compris file listener. Exception: {0}", ex.ToString)
        End Try
    End Sub

    Private Sub ProcessComprisPDP(Optional ByVal forceProcess As Boolean = False)
        If GetEntityId() > 0 Then
            If MyComprisEnabled AndAlso _
                CheckBoxComprisEnabled.Checked = True AndAlso _
                LiveLinkConfiguration.ComprisPDPEnabled AndAlso _
                (MyComprisPDPLastPoll.AddMinutes(LiveLinkConfiguration.ComprisPDPPollInterval) < DateTime.Now OrElse forceProcess) Then
                Try
                    ButtonComprisPDPProcess.Enabled = False
                    SetEndofDayStatus("Processing PDP...")
                    Application.DoEvents()
                    SetStatus("", "Retrieving PDP totals")

                    ' create a new DCR report and get the totals
                    Dim comprisPDP As New MxCompris.Reports.DCR(GetEntityId())
                    comprisPDP.GetTotals()
                    ' set last poll time as now
                    MyComprisPDPLastPoll = Now

                Catch ex As Exception
                    LogEvent(EventLogEntryType.Error, "Exception: [ProcessComprisPDP]  {0}{1}{1}{2}", ex.Message, vbCrLf, ex.StackTrace)
                    SetStatus("Error", ex.Message)
                Finally
                    ButtonComprisPDPProcess.Enabled = True
                    SetEndofDayStatus("")
                End Try
            End If
        End If
    End Sub

    ' callback function to display status message
    Public Sub LogStatus(ByVal status As String)
        SetStatus("", status)
    End Sub

    Private Sub ProcessComprisPDPUpdates(Optional ByVal forceUpdate As Boolean = False)
        If GetEntityId() > 0 Then
            If MyComprisEnabled AndAlso _
                CheckBoxComprisEnabled.Checked = True AndAlso _
                LiveLinkConfiguration.ComprisPDPUpdatesEnabled AndAlso _
                (MyComprisPDPUpdatesLastPoll.AddMinutes(LiveLinkConfiguration.ComprisPDPUpdatesPollInterval) < DateTime.Now OrElse forceUpdate) Then
                Try
                    ButtonComprisPDPUpdatesProcess.Enabled = False
                    SetEndofDayStatus("PDP Updates...")
                    Application.DoEvents()
                    ' get a lease prior to sending updates (the session key and identity are required for communications)
                    GetLease()
                    ' create a new DCR report and get the totals
                    MxCompris.Updates.Helper.Update(MyIdentity, mySessionKey, GetEntityId(), AddressOf LogStatus)
                    ' set last poll time as now
                    MyComprisPDPUpdatesLastPoll = Now

                Catch ex As Exception
                    LogEvent(EventLogEntryType.Error, "Exception: [ProcessComprisPDPUpdates]  {0}{1}{1}{2}", ex.Message, vbCrLf, ex.StackTrace)
                    SetStatus("Error", ex.Message)
                Finally
                    ButtonComprisPDPUpdatesProcess.Enabled = True
                    SetEndofDayStatus("")
                End Try
            End If
        End If
    End Sub

    Private Sub ProcessComprisPollFiles(Optional ByVal forceUpdate As Boolean = False)
        If GetEntityId() > 0 Then
            If MyComprisEnabled AndAlso _
                CheckBoxComprisEnabled.Checked = True AndAlso _
                LiveLinkConfiguration.ComprisPollFilesEnabled AndAlso _
                (MyComprisPollFilesLastPoll.AddMinutes(LiveLinkConfiguration.ComprisPollFilesPollInterval) < DateTime.Now OrElse forceUpdate) Then
                Try
                    ButtonComprisPollFilesProcess.Enabled = False
                    SetEndofDayStatus("Poll Files...")
                    Application.DoEvents()
                    ' get a lease prior to sending updates (the session key and identity are required for communications)
                    GetLease()
                    ' process any poll files, COGS or Transfers.
                    MxCompris.Reports.PollFiles.Process(MyIdentity, mySessionKey, GetEntityId(), MxCompris.Reports.PollFileType.COGS)
                    MxCompris.Reports.PollFiles.Process(MyIdentity, mySessionKey, GetEntityId(), MxCompris.Reports.PollFileType.Transfers)
                    ' set last poll time as now
                    MyComprisPollFilesLastPoll = Now

                Catch ex As Exception
                    LogEvent(EventLogEntryType.Error, "Exception: [ProcessComprisPollFiles]  {0}{1}{1}{2}", ex.Message, vbCrLf, ex.StackTrace)
                    SetStatus("Error", ex.Message)
                Finally
                    ButtonComprisPollFilesProcess.Enabled = True
                    SetEndofDayStatus("")
                End Try
            End If
        End If
    End Sub

    Private Sub MenuItemTrayRunComprisActive_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemRunComprisActive.Click, MenuItemTrayRunComprisActive.Click
        ' Create an object of ComprisPOSActiveX, which is a Singleton class.
        ' This will only allow one instance of the ComprisPOSActiveX class to be created at a time. It will return 'Nothing' if there is a class already instantiated
        ' ComprisPOSActiveX is a singleton class that encapsulates the running of the ComprisPOSActiveX form
        ' It enables only one instance of the ComprisPOSActiveX form to be running at any time
        Dim ComprisPOSActiveXInstance As ComprisPOSActiveXSingleton

        ' Attempts to get an instance of the time clock.
        ' If instance is obtained, no new instance can be retrieved till ComprisPOSActiveXSingleton.ReleaseInstance is called.
        ' This is called within ComprisPOSActiveXSingleton on exit of ComprisPOSActiveX
        ComprisPOSActiveXInstance = ComprisPOSActiveXSingleton.GetInstance(MyComprisActiveMeKeySOD, _
                                                                            MyComprisActiveMeKeyEOD, _
                                                                            MyComprisActiveAccessCode, _
                                                                            MyComprisActiveScriptPreSOD, _
                                                                            MyComprisActiveScriptPostSOD, _
                                                                            MyComprisActiveScriptPreEOD, _
                                                                            MyComprisActiveScriptPostEOD, _
                                                                            MyComprisActiveRegisterCount, _
                                                                            MyLogoImage, _
                                                                            MyLogoColor)

        ' Check if instance was created
        If Not (ComprisPOSActiveXInstance Is Nothing) Then
            Try
                ' Create a new thread to run the TimeClock form from. This will make it still available when LiveLink is blocking at any point
                Dim ComprisPOSActiveXThread As New System.Threading.Thread(New System.Threading.ThreadStart(AddressOf ComprisPOSActiveXInstance.RunComprisPOSActiveXForm))
                ComprisPOSActiveXThread.Name = "ComprisPOSActiveX"
                ComprisPOSActiveXThread.Start()
                Me.WindowState = FormWindowState.Normal
                Me.WindowState = FormWindowState.Minimized
            Catch ex As Exception
                LogEvent("Error creating new thread for ComprisPOSActiveX." & vbCrLf & vbCrLf & "Error: " & ex.Message, EventLogEntryType.Warning)
            End Try
        Else
            'MessageBox.Show("ComprisPOSActiveX is already running")
        End If
    End Sub


    Private Sub ButtonComprisPDPProcess_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonComprisPDPProcess.Click
        ProcessComprisPDP(True)
    End Sub

    Private Sub ButtonComprisPDPUpdatesProcess_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonComprisPDPUpdatesProcess.Click
        ProcessComprisPDPUpdates(True)
    End Sub

    Private Sub ButtonComprisPollFilesProcess_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonComprisPollFilesProcess.Click
        ProcessComprisPollFiles(True)
    End Sub

#End Region

#Region " Panasonic Gateway Interface"

    Private Sub ProcessPanasonic(Optional ByVal Force As Boolean = False)
        If GetEntityId() > 0 Then
            If MyPanasonicEnabled And _
                CheckBoxPanasonicEnabled.Checked = True And _
                (MyPanasonicLastPoll.AddMinutes(MyPanasonicPollInterval) < DateTime.Now Or Force) Then
                Try
                    Mx.POS.Panasonic.PAN7700.PAN7700_ImportData()
                    MyPanasonicLastPoll = Now
                    MyPanasonicFirstPollDone = True
                Catch ex As Exception
                    SetStatus("Error", ex.Message)
                Finally
                    UpdatePanasonicStatus()
                End Try
            End If
        End If
    End Sub

    Private Sub UpdatePanasonicStatus()
        txtPanasonicCurrentTime.Text = DateTime.Now.ToString("h:mm tt")
        txtPanasonicLastPoll.Text = IIf(MyPanasonicFirstPollDone, MyPanasonicLastPoll.ToString("h:mm tt"), "")
        txtPanasonicNextPoll.Text = IIf(CheckBoxPanasonicEnabled.Checked, IIf(MyPanasonicLastPoll = DateTime.MinValue, "0 min", CInt(MyPanasonicLastPoll.AddMinutes(MyPanasonicPollInterval).Subtract(DateTime.Now).TotalMinutes) & " min"), "")
        Application.DoEvents()
    End Sub

    Private Sub ButtonPanasonicProcess_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonPanasonicImport.Click
        If Not Processing Then
            Processing = True
            SetEndofDayStatus("Processing...")
            ProcessPanasonic(True)
            SetEndofDayStatus("")
            Processing = False
        End If
    End Sub

#End Region

#Region " Intouch Gateway Interface"

    Private Sub ProcessIntouch(Optional ByVal Force As Boolean = False)
        Dim EntityId As Integer = GetEntityId()
        If EntityId > 0 Then
            If MyIntouchEnabled And _
                CheckBoxIntouchEnabled.Checked = True And _
                (MyIntouchLastPoll.AddMinutes(MyIntouchPollInterval) < DateTime.Now Or Force) Then
                Try
                    ButtonIntouchImport.Enabled = False
                    ' show a message in livelink, showing it is currently processing
                    SetEndofDayStatus("Processing...")

                    ' Instantiate the Intouch interface class
                    MyIntouch = New Intouch(MyConnectionString, _
                                            MyIntouchExportExe, _
                                            MyIntouchExportFilePath, _
                                            EntityId, _
                                            MyIntouchServiceTypeMappings, _
                                            MyIntouchAutomatedExport, _
                                            MyIntouchDrawerPullsEnabled, _
                                            MyIntouchItemCodeField, _
                                            MyIntouchItemDescriptionField, _
                                            MyIntouchModifierCodeField, _
                                            MyIntouchModifierDescriptionField)
                    ' Process intouch
                    MyIntouch.Process()
                    If MyIntouch.LatestInTouchExport > MyIntouchLatestExport Then
                        MyIntouchLatestExport = MyIntouch.LatestInTouchExport
                        MyIntouchLatestExportChanged = True
                    End If
                    MyIntouch = Nothing

                    MyIntouchLastPoll = Now
                    MyIntouchFirstPollDone = True
                Catch ex As Exception
                    SetStatus("Error", ex.Message)
                Finally
                    ButtonIntouchImport.Enabled = True
                    SetEndofDayStatus("")
                    UpdateIntouchStatus()
                End Try
            End If
        End If
    End Sub

    Private Sub UpdateIntouchStatus()
        txtIntouchCurrentTime.Text = DateTime.Now.ToString("h:mm tt")
        txtIntouchLastPoll.Text = IIf(MyIntouchFirstPollDone, MyIntouchLastPoll.ToString("h:mm tt"), "")
        txtIntouchNextPoll.Text = IIf(CheckBoxIntouchEnabled.Checked, IIf(MyIntouchLastPoll = DateTime.MinValue, "0 min", CInt(MyIntouchLastPoll.AddMinutes(MyIntouchPollInterval).Subtract(DateTime.Now).TotalMinutes) & " min"), "")
        Application.DoEvents()
    End Sub

    Private Sub ButtonIntouchProcess_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonIntouchImport.Click
        If Not Processing Then
            Processing = True
            ProcessIntouch(True)
            Processing = False
        End If
    End Sub

    Private Sub ProcessIntouchMenuPackage()
        Dim entityId As Integer = GetEntityId()
        If entityId > 0 Then
            ' check if process is set to poll
            If LiveLinkConfiguration.IntouchMenuPackageEnabled And (MyMenuPackageLastPoll.AddMinutes(LiveLinkConfiguration.IntouchMenuPackagePollInterval) < DateTime.Now) Then
                Try
                    Dim myIntouchMenuPkg As New IntouchMenuPackage()
                    myIntouchMenuPkg.ProcessOutputFile()
                    MyMenuPackageLastPoll = Now
                Catch ex As Exception
                    SetStatus("Error checking Intouch menu package.", ex.ToString)
                End Try
            End If
        End If
    End Sub
#End Region

#Region " Octane POS"


    Private Sub ProcessOctane(Optional ByVal Force As Boolean = False)
        Dim EntityId As Integer = GetEntityId()
        If EntityId > 0 Then
            If LiveLinkConfiguration.OctaneEnabled And _
                CheckBoxOctaneEnabled.Checked = True And _
                (MyOctaneLastPoll.AddMinutes(LiveLinkConfiguration.OctanePollInterval) < DateTime.Now Or Force) Then
                Try
                    ButtonOctaneImport.Enabled = False
                    ' show a message in livelink, showing it is currently processing
                    SetEndofDayStatus("Processing...")

                    ' Instantiate the Octane interface class if it hasn't been done already
                    If MyOctane Is Nothing Then
                        MyOctane = New Octane(MyConnectionString, _
                                            LiveLinkConfiguration.OctaneExportFilePath, _
                                            LiveLinkConfiguration.OctaneWorkingFilePath, _
                                            EntityId, _
                                            LiveLinkConfiguration.DaysBeforeObsolete)
                    End If

                    ' Process Octane
                    MyOctane.Process()

                    MyOctaneLastPoll = Now
                    MyOctaneFirstPollDone = True
                Catch ex As Exception
                    SetStatus("Error", ex.ToString)
                Finally
                    ButtonOctaneImport.Enabled = True
                    SetEndofDayStatus("")
                    UpdateOctaneStatus()
                End Try
            End If
        End If
    End Sub

    Private Sub ProcessSUS(Optional ByVal Force As Boolean = False)
        Dim EntityId As Integer = GetEntityId()
        If EntityId > 0 Then
            If LiveLinkConfiguration.SUSImportEnabled And _
                CheckBox5.Checked = True And _
                (MySUSPOSImportLastPoll.AddMinutes(LiveLinkConfiguration.SUSImportPollInterval) < DateTime.Now Or Force) Then
                Try
                    Button11.Enabled = False
                    ' show a message in livelink, showing it is currently processing
                    SetEndofDayStatus("Processing...")

                    ' Instantiate the Octane interface class if it hasn't been done already
                    If MySUSPOSImportPOS Is Nothing Then
                        MySUSPOSImportPOS = New SUSPOS()
                    End If

                    ' Process Octane
                    MySUSPOSImportPOS.DataProcess(GetEntityId())

                    MySUSPOSImportLastPoll = Now
                    MySUSPOSFirstPollDone = True
                Catch ex As Exception
                    SetStatus("Error", ex.ToString)
                Finally
                    Button11.Enabled = True
                    SetEndofDayStatus("")
                    UpdateSUSStatus()
                End Try
            End If
        End If
    End Sub


    Private Sub UpdateOctaneStatus()
        txtOctaneCurrentTime.Text = DateTime.Now.ToString("h:mm tt")
        txtOctaneLastPoll.Text = IIf(MyOctaneFirstPollDone, MyOctaneLastPoll.ToString("h:mm tt"), "")
        txtOctaneNextPoll.Text = IIf(CheckBoxOctaneEnabled.Checked, IIf(MyOctaneLastPoll = DateTime.MinValue, "0 min", CInt(MyOctaneLastPoll.AddMinutes(LiveLinkConfiguration.OctanePollInterval).Subtract(DateTime.Now).TotalMinutes) & " min"), "")
        Application.DoEvents()
    End Sub

    Private Sub UpdateSUSStatus()
        Label102.Text = DateTime.Now.ToString("h:mm tt")
        Label101.Text = MySUSPosPollInterval
        Label98.Text = IIf(MySUSPOSFirstPollDone, MySUSPOSImportLastPoll.ToString("h:mm tt"), "")
        Label94.Text = IIf(CheckBox5.Checked, IIf(MySUSPOSImportLastPoll = DateTime.MinValue, "0 min", CInt(MySUSPOSImportLastPoll.AddMinutes(MySUSPosPollInterval).Subtract(DateTime.Now).TotalMinutes) & " min"), "")
        Application.DoEvents()
    End Sub


    Private Sub ButtonOctaneImport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonOctaneImport.Click
        If Not Processing Then
            Processing = True
            ProcessOctane(True)
            Processing = False
        End If
    End Sub

    Private Sub ButtonSUSImport_Click_1(sender As System.Object, e As System.EventArgs) Handles ButtonRPOSImport.Click, Button11.Click
        If Not Processing Then
            Processing = True
            ProcessSUS(True)
            Processing = False
        End If
    End Sub

#End Region

#Region " ARTS POS"

    Private Sub ProcessArts(Optional ByVal force As Boolean = False)
        Dim EntityId As Integer = GetEntityId()
        If EntityId > 0 Then
            If LiveLinkConfiguration.ArtsEnabled And _
                CheckBoxArtsEnabled.Checked = True And _
                (MyArtsLastPoll.AddMinutes(LiveLinkConfiguration.ArtsPollInterval) < DateTime.Now Or force) Then
                Try
                    ButtonArtsImport.Enabled = False
                    ' show a message in livelink, showing it is currently processing
                    SetEndofDayStatus("Processing...")

                    ' process ARTS data
                    Dim artsPOS As ArtsPosImport = New ArtsPosImport()
                    artsPOS.ProcessExportXmlFiles(EntityId)

                    MyArtsLastPoll = Now
                    MyArtsFirstPollDone = True
                Catch ex As Exception
                    SetStatus("Error", ex.ToString)
                Finally
                    ButtonArtsImport.Enabled = True
                    SetEndofDayStatus("")
                    UpdateArtsStatus()
                End Try
            End If
        End If
    End Sub


    Private Sub UpdateArtsStatus()
        txtArtsCurrentTime.Text = DateTime.Now.ToString("h:mm tt")
        txtArtsLastPoll.Text = IIf(MyArtsFirstPollDone, MyArtsLastPoll.ToString("h:mm tt"), "")
        txtArtsNextPoll.Text = IIf(CheckBoxArtsEnabled.Checked, IIf(MyArtsLastPoll = DateTime.MinValue, "0 min", CInt(MyArtsLastPoll.AddMinutes(LiveLinkConfiguration.ArtsPollInterval).Subtract(DateTime.Now).TotalMinutes) & " min"), "")
        Application.DoEvents()
    End Sub

    Private Sub ButtonArtsImport_Click(sender As System.Object, e As System.EventArgs) Handles ButtonArtsImport.Click
        If Not Processing Then
            Processing = True
            ProcessArts(True)
            Processing = False
        End If
    End Sub

#End Region

#Region " ICG Gateway"


    Private Sub ProcessICG(Optional ByVal Force As Boolean = False)
        Dim EntityId As Integer = GetEntityId()
        If EntityId > 0 Then
            If LiveLinkConfiguration.ICGEnabled And _
                CheckBoxICGEnabled.Checked = True And _
                (MyICGLastPoll.AddMinutes(LiveLinkConfiguration.ICGPollInterval) < DateTime.Now Or Force) Then
                Try
                    ButtonICGImport.Enabled = False
                    ' show a message in livelink, showing it is currently processing
                    SetEndofDayStatus("Processing...")

                    ' Instantiate the ICG interface class if it hasn't been done already
                    If MyICG Is Nothing Then
                        MyICG = New ICG.ICGProcess(EntityId)
                    End If

                    ' Process ICG
                    MyICG.Process()

                    MyICGLastPoll = Now
                    MyICGFirstPollDone = True
                Catch ex As Exception
                    SetStatus("Error", ex.ToString)
                Finally
                    ButtonICGImport.Enabled = True
                    SetEndofDayStatus("")
                    UpdateICGStatus()
                End Try
            End If
        End If
    End Sub



    Private Sub UpdateICGStatus()
        txtICGCurrentTime.Text = DateTime.Now.ToString("h:mm tt")
        txtICGLastPoll.Text = IIf(MyICGFirstPollDone, MyICGLastPoll.ToString("h:mm tt"), "")
        txtICGNextPoll.Text = IIf(CheckBoxICGEnabled.Checked, IIf(MyICGLastPoll = DateTime.MinValue, "0 min", CInt(MyICGLastPoll.AddMinutes(LiveLinkConfiguration.ICGPollInterval).Subtract(DateTime.Now).TotalMinutes) & " min"), "")
        Application.DoEvents()
    End Sub


    Private Sub ButtonICGImport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonICGImport.Click
        If Not Processing Then
            Processing = True
            ProcessICG(True)
            Processing = False
        End If
    End Sub

#End Region

#Region " Microsoft RMS Gateway"


    Private Sub ProcessRMS(Optional ByVal Force As Boolean = False)
        Dim EntityId As Integer = GetEntityId()
        If EntityId > 0 Then
            If LiveLinkConfiguration.RMSEnabled And _
                CheckBoxRMSEnabled.Checked = True And _
                (MyRMSLastPoll.AddMinutes(LiveLinkConfiguration.RMSPollInterval) < DateTime.Now Or Force) Then
                Try
                    ButtonRMSImport.Enabled = False
                    ' show a message in livelink, showing it is currently processing
                    SetEndofDayStatus("Processing...")

                    ' Instantiate the RMS interface class if it hasn't been done already
                    If MyRMS Is Nothing Then
                        MyRMS = New MicrosoftRMS.RMS(CLng(EntityId))
                    End If

                    ' Process RMS
                    MyRMS.Process()

                    MyRMSLastPoll = Now
                    MyRMSFirstPollDone = True
                Catch ex As Exception
                    SetStatus("Error", ex.ToString)
                Finally
                    ButtonRMSImport.Enabled = True
                    SetEndofDayStatus("")
                    UpdateRMSStatus()
                End Try
            End If
        End If
    End Sub


    Private Sub UpdateRMSStatus()
        txtRMSCurrentTime.Text = DateTime.Now.ToString("h:mm tt")
        txtRMSLastPoll.Text = IIf(MyRMSFirstPollDone, MyRMSLastPoll.ToString("h:mm tt"), "")
        txtRMSNextPoll.Text = IIf(CheckBoxRMSEnabled.Checked, IIf(MyRMSLastPoll = DateTime.MinValue, "0 min", CInt(MyRMSLastPoll.AddMinutes(LiveLinkConfiguration.RMSPollInterval).Subtract(DateTime.Now).TotalMinutes) & " min"), "")
        Application.DoEvents()
    End Sub


    Private Sub ButtonRMSImport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonRMSImport.Click
        If Not Processing Then
            Processing = True
            ProcessRMS(True)
            Processing = False
        End If
    End Sub

#End Region

#Region " NewPOS Gateway"


    Private Sub ProcessNewPOS(Optional ByVal Force As Boolean = False)
        Dim EntityId As Integer = GetEntityId()
        If EntityId > 0 Then
            If LiveLinkConfiguration.NewPOSEnabled And _
                CheckBoxNewPOSEnabled.Checked = True And _
                (MyNewPOSLastPoll.AddMinutes(LiveLinkConfiguration.NewPOSPollInterval) < DateTime.Now Or Force) Then
                Try
                    ButtonNewPOSImport.Enabled = False
                    ' show a message in livelink, showing it is currently processing
                    SetEndofDayStatus("Processing...")

                    ' Instantiate the NewPOS interface class if it hasn't been done already
                    If MyNewPOS Is Nothing Then
                        MyNewPOS = New NewPOS(CInt(EntityId), LiveLinkConfiguration.ConnectionString, _
                                              LiveLinkConfiguration.NewPOSExportFilePath, LiveLinkConfiguration.NewPOSRegisterListFile, _
                                              LiveLinkConfiguration.NewPOSPipeName, LiveLinkConfiguration.NewPOSTableVersion)
                    End If

                    Dim queryDate As DateTime
                    If (Force OrElse MyNewPOSFirstPollDone = False) Then
                        queryDate = DateTime.Now.AddDays(-LiveLinkConfiguration.NewPOSDaysToRecover).Date
                    Else
                        queryDate = MyNewPOS.GetLastQueryDate()
                        ' if failed to get a query date, then recover previous days
                        If queryDate = DateTime.MinValue Then
                            queryDate = DateTime.Now.AddDays(-LiveLinkConfiguration.NewPOSDaysToRecover).Date
                        End If
                    End If

                    ' process up until current day
                    While (queryDate <= DateTime.Now.Date)
                        Application.DoEvents()
                        ' Process NewPOS
                        If MyNewPOS.Process(queryDate) Then
                            ' if process was successfull, update the last query date value
                            MyNewPOS.SetLastQueryDate(queryDate)
                        End If
                        ' attemp to query the next business day 
                        queryDate = queryDate.AddDays(1)
                    End While

                    MyNewPOSLastPoll = Now
                    MyNewPOSFirstPollDone = True
                Catch ex As Exception
                    SetStatus("Error", ex.ToString)
                Finally
                    ButtonNewPOSImport.Enabled = True
                    SetEndofDayStatus("")
                    UpdateNewPOSStatus()
                End Try
            End If
        End If
    End Sub


    Private Sub UpdateNewPOSStatus()
        txtNewPOSCurrentTime.Text = DateTime.Now.ToString("h:mm tt")
        txtNewPOSLastPoll.Text = IIf(MyNewPOSFirstPollDone, MyNewPOSLastPoll.ToString("h:mm tt"), "")
        txtNewPOSNextPoll.Text = IIf(CheckBoxNewPOSEnabled.Checked, IIf(MyNewPOSLastPoll = DateTime.MinValue, "0 min", CInt(MyNewPOSLastPoll.AddMinutes(LiveLinkConfiguration.NewPOSPollInterval).Subtract(DateTime.Now).TotalMinutes) & " min"), "")
        Application.DoEvents()
    End Sub


    Private Sub ButtonNewPOSImport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonNewPOSImport.Click
        If Not Processing Then
            Processing = True
            ProcessNewPOS(True)
            Processing = False
        End If
    End Sub

#End Region

#Region "iPOS Gateway"


    Private Sub ProcessiPOS(Optional ByVal Force As Boolean = False)
        Dim EntityId As Integer = GetEntityId()

        If (MyiPOSUpdatingiPOSDatabase) Then
            SetStatus("", "iPOS database update in progress, skipping data import")
            Exit Sub
        End If

        If EntityId > 0 Then
            If LiveLinkConfiguration.iPOSEnabled And _
                CheckBoxiPOSEnabled.Checked = True And _
                (MyiPOSLastPoll.AddMinutes(LiveLinkConfiguration.iPOSPollInterval) < DateTime.Now Or Force) Then
                Try
                    grpiPOSUpdates.Enabled = False
                    grpiPOSProcess.Enabled = False

                    ' show a message in livelink, showing it is currently processing
                    SetEndofDayStatus("Processing...")

                    ' Instantiate the iPOS interface class if it hasn't been done already
                    If MyiPOS Is Nothing Then
                        MyiPOS = New iPOS(EntityId, LiveLinkConfiguration.ConnectionString)
                    Else
                        ' refresh the entityID cached value (in case it has been changed)
                        MyiPOS.EntityID = EntityId
                    End If

                    ' store the current poll time
                    Dim currentPoll As DateTime = Now
                    Dim recoveryTime As DateTime = currentPoll.Date.AddHours(LiveLinkConfiguration.iPOSAutoRecoverHour)

                    ' get the last query date
                    Dim queryDate As DateTime = MyiPOS.GetLastQueryDate()

                    If (queryDate = DateTime.MinValue) Then
                        queryDate = currentPoll.AddDays(-LiveLinkConfiguration.iPOSDaysToRecover).Date
                    ElseIf (Force OrElse (Not MyiPOSFirstPollDone) OrElse (MyiPOSLastPoll < recoveryTime AndAlso currentPoll >= recoveryTime)) Then
                        ' if first poll or forced poll, check to see the amount of days to process in the past is at least the value of DaysToRecover on startup
                        ' if last poll date was prior to the recovery time (e.g. 5am) but the currentPoll is greater than recovery time, reprocess historical data.
                        If (queryDate > currentPoll.AddDays(-LiveLinkConfiguration.iPOSDaysToRecoverOnStartup).Date) Then
                            queryDate = currentPoll.AddDays(-LiveLinkConfiguration.iPOSDaysToRecoverOnStartup).Date
                        End If
                    End If
                    ' display the last query date on the form
                    UpdateiPOSProcessDate(queryDate)

                    ' make sure query date isn't too far in the past
                    If ((LiveLinkConfiguration.DaysBeforeObsolete <> 0) AndAlso (queryDate < currentPoll.AddDays(-LiveLinkConfiguration.DaysBeforeObsolete))) Then
                        queryDate = currentPoll.AddDays(-LiveLinkConfiguration.DaysBeforeObsolete).Date
                    End If

                    'If (Force OrElse MyiPOSFirstPollDone = False) Then
                    '    queryDate = DateTime.Now.AddDays(-LiveLinkConfiguration.iPOSDaysToRecover).Date
                    'Else
                    '    queryDate = MyiPOS.GetLastQueryDate()
                    '    ' if failed to get a query date, then recover previous days
                    '    If queryDate = DateTime.MinValue Then
                    '        queryDate = DateTime.Now.AddDays(-LiveLinkConfiguration.iPOSDaysToRecover).Date
                    '    End If
                    'End If

                    ' process up until current day (use currentPoll value so it doesn't change during processing).
                    While (queryDate <= currentPoll.Date)
                        Application.DoEvents()

                        If (MyiPOSUpdatingiPOSDatabase) Then
                            SetStatus("", "iPOS database update in progress, ending the data import")
                            Exit While
                        End If

                        ' Process iPOS
                        If MyiPOS.Process(queryDate) Then
                            ' if process was successfull, update the last query date value
                            MyiPOS.SetLastQueryDate(queryDate)
                            UpdateiPOSProcessDate(queryDate)
                        End If
                        ' attemp to query the next business day 
                        queryDate = queryDate.AddDays(1)
                    End While

                    ' clean up rolling cache
                    If (LiveLinkConfiguration.DaysBeforeObsolete <> 0) Then
                        ' delete old cache
                        MyiPOS.DeleteFromCache(currentPoll.AddDays(-LiveLinkConfiguration.DaysBeforeObsolete).Date)
                    End If

                    ' set the last poll to the current poll time, this will ensure when the iPOSAutoRecoverHour is hit we re-process the last X days.
                    MyiPOSLastPoll = currentPoll
                    MyiPOSFirstPollDone = True
                Catch ex As Exception
                    LogEvent(EventLogEntryType.Error, "Exception occurred processing iPOS data. Exception: {0}", ex.ToString)
                    SetStatus("Error", ex.ToString)
                Finally
                    grpiPOSUpdates.Enabled = True
                    grpiPOSProcess.Enabled = True
                    SetEndofDayStatus("")
                    UpdateiPOSStatus()
                End Try
            End If
        End If
    End Sub


    Private Sub UpdateiPOSStatus()
        txtiPOSCurrentTime.Text = DateTime.Now.ToString("h:mm tt")
        txtiPOSLastPoll.Text = IIf(MyiPOSFirstPollDone, MyiPOSLastPoll.ToString("h:mm tt"), "")
        txtiPOSNextPoll.Text = IIf(CheckBoxiPOSEnabled.Checked, IIf(MyiPOSLastPoll = DateTime.MinValue, "0 min", CInt(MyiPOSLastPoll.AddMinutes(LiveLinkConfiguration.iPOSPollInterval).Subtract(DateTime.Now).TotalMinutes) & " min"), "")
        Application.DoEvents()
    End Sub


    Private Sub ButtoniPOSImport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtoniPOSImport.Click
        If Not Processing Then
            Processing = True
            ProcessiPOS(True)
            Processing = False
        End If
    End Sub

    Private Sub UpdateiPOSProcessDate(ByVal processDate As DateTime, Optional ByVal updateInformationLabel As Boolean = True)
        dtiPOSProcessDate.Value = processDate
        dtiPOSProcessDate.Refresh()
        ' check if label should be updated (only update if actual value is known)
        If (updateInformationLabel) Then
            txtiPOSProcessDate.Text = processDate.ToString("dd-MMM-yyyy")
        End If
    End Sub

    Private Sub ButtoniPOSUpdateProcessDate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtoniPOSUpdateProcessDate.Click
        Try
            If (dtiPOSProcessDate.Value = DateTime.MinValue) Then
                SetStatus("", "Failed to update iPOS process date. No valid time set")
                Exit Sub
            End If
            If (dtiPOSProcessDate.Value > DateTime.Now) Then
                SetStatus("", "Failed to update iPOS process date. Date must be current day or prior")
                Exit Sub
            End If

            Dim entityId As Long = GetEntityId()
            If (entityId <= 0) Then
                SetStatus("", "Failed to update iPOS process date. No store configured")
                Exit Sub
            End If

            ' Instantiate the iPOS interface class if it hasn't been done already
            If MyiPOS Is Nothing Then
                MyiPOS = New iPOS(entityId, LiveLinkConfiguration.ConnectionString)
            Else
                ' refresh the entityID cached value (in case it has been changed)
                MyiPOS.EntityID = entityId
            End If

            ' set the last query date to specified date
            MyiPOS.SetLastQueryDate(dtiPOSProcessDate.Value.Date)
            UpdateiPOSProcessDate(dtiPOSProcessDate.Value)

        Catch ex As Exception
            LogEvent(EventLogEntryType.Error, "Exception occurred updating iPOS process date. Exception: {0}", ex.ToString)
        End Try
    End Sub


    Private Sub ButtoniPOSUpdateProducts_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtoniPOSUpdateProducts.Click
        If (MyiPOSUpdatingiPOSDatabase) Then
            SetStatus("", "iPOS database update in progress, cannot action product update")
            Exit Sub
        End If

        Try
            MyiPOSUpdatingiPOSDatabase = True
            SetEndofDayStatus("Updating iPOS...")

            LogEvent(EventLogEntryType.Information, "Attempt to update iPOS products triggered from UI")
            ButtoniPOSUpdateProducts.Enabled = False
            ButtoniPOSUpdateStaff.Enabled = False
            ' make sure you have a lease prior to update
            GetLease()
            ' apply the encrypted password prior to applying update
            If (MyiPOS IsNot Nothing) Then
                MyiPOS.ApplyEncryptedPassword()
            End If
            ' process product update
            iPOSUpdateHelper.ProcessUpdate(iPOSUpdateHelper.UpdateType.iPOSProductUpdate, MyIdentity, mySessionKey, GetEntityId())

        Catch ex As Exception
            LogEvent(EventLogEntryType.Error, "Exception occurred doing manual update of all iPOS products. Exception: {0}", ex.ToString)
        Finally
            MyiPOSUpdatingiPOSDatabase = False
            ButtoniPOSUpdateProducts.Enabled = True
            ButtoniPOSUpdateStaff.Enabled = True
            SetEndofDayStatus("")
        End Try
    End Sub

    Private Sub ButtoniPOSUpdateStaff_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtoniPOSUpdateStaff.Click
        If (MyiPOSUpdatingiPOSDatabase) Then
            SetStatus("", "iPOS database update in progress, cannot action staff update")
            Exit Sub
        End If

        Try
            MyiPOSUpdatingiPOSDatabase = True
            SetEndofDayStatus("Updating iPOS...")

            LogEvent(EventLogEntryType.Information, "Attempt to update iPOS staff triggered from UI")
            ButtoniPOSUpdateProducts.Enabled = False
            ButtoniPOSUpdateStaff.Enabled = False
            ' make sure you have a lease prior to update
            GetLease()
            ' apply the encrypted password prior to applying update
            If (MyiPOS IsNot Nothing) Then
                MyiPOS.ApplyEncryptedPassword()
            End If
            ' process staff update
            iPOSUpdateHelper.ProcessUpdate(iPOSUpdateHelper.UpdateType.iPOSStaffUpdate, MyIdentity, mySessionKey, GetEntityId())

        Catch ex As Exception
            LogEvent(EventLogEntryType.Error, "Exception occurred doing manual update of all iPOS staff. Exception: {0}", ex.ToString)
        Finally
            MyiPOSUpdatingiPOSDatabase = False
            ButtoniPOSUpdateProducts.Enabled = True
            ButtoniPOSUpdateStaff.Enabled = True
            SetEndofDayStatus("")
        End Try
    End Sub

#End Region

#Region "Uniwell Gateway"


    Private Sub ProcessUniwell(Optional ByVal Force As Boolean = False)
        Dim EntityId As Integer = GetEntityId()
        If EntityId > 0 Then
            If LiveLinkConfiguration.UniwellEnabled And _
                CheckBoxUniwellEnabled.Checked = True And _
                (MyUniwellLastPoll.AddMinutes(LiveLinkConfiguration.UniwellPollInterval) < DateTime.Now Or Force) Then
                Try
                    ButtonUniwellImport.Enabled = False
                    ' show a message in livelink, showing it is currently processing
                    SetEndofDayStatus("Processing...")

                    ' process Uniwell reports
                    Uniwell.DataHelper.ProcessReports(EntityId)

                    MyUniwellLastPoll = Now
                    MyUniwellFirstPollDone = True
                Catch ex As Exception
                    SetStatus("Error", ex.ToString)
                Finally
                    ButtonUniwellImport.Enabled = True
                    SetEndofDayStatus("")
                    UpdateUniwellStatus()
                End Try
            End If
        End If
    End Sub


    Private Sub UpdateUniwellStatus()
        txtUniwellCurrentTime.Text = DateTime.Now.ToString("h:mm tt")
        txtUniwellLastpoll.Text = IIf(MyUniwellFirstPollDone, MyUniwellLastPoll.ToString("h:mm tt"), "")
        txtUniwellNextPoll.Text = IIf(CheckBoxUniwellEnabled.Checked, IIf(MyUniwellLastPoll = DateTime.MinValue, "0 min", CInt(MyUniwellLastPoll.AddMinutes(LiveLinkConfiguration.UniwellPollInterval).Subtract(DateTime.Now).TotalMinutes) & " min"), "")
        Application.DoEvents()
    End Sub


    Private Sub ButtonUniwellImport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonUniwellImport.Click
        If Not Processing Then
            Processing = True
            ProcessUniwell(True)
            Processing = False
        End If
    End Sub


#End Region

#Region "Radiant Gateway"


    Private Sub ProcessRadiant(Optional ByVal Force As Boolean = False)
        If LiveLinkConfiguration.RadiantEnabled And _
            CheckBoxRadiantEnabled.Checked = True And _
            (MyRadiantLastPoll.AddMinutes(LiveLinkConfiguration.RadiantPollInterval) < DateTime.Now Or Force) Then
            Try
                ButtonRadiantImport.Enabled = False
                ' show a message in livelink, showing it is currently processing
                SetEndofDayStatus("Processing...")

                ' process Radiant data (add reference to Radiant class here)
                Dim Radiant As Mx.POS.Radiant.RadiantPOS
                Radiant = New Mx.POS.Radiant.RadiantPOS()
                Radiant.ProcessExportXMLFiles()

                ' process Radiant data (add reference to Radiant class here)
                Dim RadiantPunch As Mx.POS.Radiant.RadiantPunch
                RadiantPunch = New Mx.POS.Radiant.RadiantPunch()
                RadiantPunch.ProcessExportXMLFiles()

                MyRadiantLastPoll = Now
                MyRadiantFirstPollDone = True
            Catch ex As Exception
                SetStatus("Error", ex.ToString)
            Finally
                ButtonRadiantImport.Enabled = True
                SetEndofDayStatus("")
                UpdateRadiantStatus()
            End Try
        End If
    End Sub


    Private Sub UpdateRadiantStatus()
        txtRadiantCurrentTime.Text = DateTime.Now.ToString("h:mm tt")
        txtRadiantLastPoll.Text = IIf(MyRadiantFirstPollDone, MyRadiantLastPoll.ToString("h:mm tt"), "")
        txtRadiantNextPoll.Text = IIf(CheckBoxRadiantEnabled.Checked, IIf(MyRadiantLastPoll = DateTime.MinValue, "0 min", CInt(MyRadiantLastPoll.AddMinutes(LiveLinkConfiguration.RadiantPollInterval).Subtract(DateTime.Now).TotalMinutes) & " min"), "")
        Application.DoEvents()
    End Sub


    Private Sub ButtonRadiantImport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonRadiantImport.Click
        If Not Processing Then
            Processing = True
            ProcessRadiant(True)
            Processing = False
        End If
    End Sub


#End Region

#Region "NAXML Gateway"

    Private Sub ProcessNAXML(Optional ByVal Force As Boolean = False)
        If LiveLinkConfiguration.NAXMLEnabled And _
            CheckBoxNAXMLEnabled.Checked = True And _
            (MyNAXMLLastPoll.AddMinutes(LiveLinkConfiguration.NAXMLPollInterval) < DateTime.Now Or Force) Then
            Try
                ButtonNAXMLImport.Enabled = False
                ' show a message in livelink, showing it is currently processing
                SetEndofDayStatus("Processing...")

                'Mx.POS.NAXML.NAXMLConfiguration.DaysBeforeObsolete = LiveLinkConfiguration.DaysBeforeObsolete
                'Mx.POS.NAXML.NAXMLConfiguration.NAXMLFilePath = LiveLinkConfiguration.NAXMLFilePath
                'Mx.POS.NAXML.NAXMLConfiguration.NAXMLMovementFilePath = LiveLinkConfiguration.NAXMLMovementFilePath
                'Mx.POS.NAXML.NAXMLConfiguration.NAXMLPumpTestTenderCode = LiveLinkConfiguration.NAXMLPumpTestTenderCode
                'Mx.POS.NAXML.NAXMLConfiguration.NAXMLRadiantCheckTaxItemizerMask = LiveLinkConfiguration.NAXMLRadiantCheckTaxItemizerMask
                'Mx.POS.NAXML.NAXMLConfiguration.NAXMLRadiantNonTaxaxbleItemFlag = LiveLinkConfiguration.NAXMLRadiantNonTaxaxbleItemFlag
                'Mx.POS.NAXML.NAXMLConfiguration.NAXMLRadiantTaxaxbleItemFlag = LiveLinkConfiguration.NAXMLRadiantTaxaxbleItemFlag

                ' process NAXML data (add reference to NAXML class here)
                Dim NAXMLPos As Mx.POS.NAXML.NAXMLPOS
                NAXMLPos = New Mx.POS.NAXML.NAXMLPOS()
                NAXMLPos.Process()

                Dim NAXMLMovement As Mx.POS.NAXML.NAXMLMovement
                NAXMLMovement = New Mx.POS.NAXML.NAXMLMovement()
                NAXMLMovement.Process()

                MyNAXMLLastPoll = Now
                MyNAXMLFirstPollDone = True
            Catch ex As Exception
                SetStatus("Error", ex.ToString)
            Finally
                ButtonNAXMLImport.Enabled = True
                SetEndofDayStatus("")
                UpdateNAXMLStatus()
            End Try
        End If
    End Sub

    Private Sub UpdateNAXMLStatus()
        txtNAXMLCurrentTime.Text = DateTime.Now.ToString("h:mm tt")
        txtNAXMLLastPoll.Text = IIf(MyNAXMLFirstPollDone, MyNAXMLLastPoll.ToString("h:mm tt"), "")
        txtNAXMLNextPoll.Text = IIf(CheckBoxNAXMLEnabled.Checked, IIf(MyNAXMLLastPoll = DateTime.MinValue, "0 min", CInt(MyNAXMLLastPoll.AddMinutes(LiveLinkConfiguration.NAXMLPollInterval).Subtract(DateTime.Now).TotalMinutes) & " min"), "")
        Application.DoEvents()
    End Sub

    Private Sub ButtonNAXMLImport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonNAXMLImport.Click
        If Not Processing Then
            Processing = True
            ProcessNAXML(True)
            Processing = False
        End If
    End Sub


#End Region

#Region "Xpient Gateway"

    Private Sub ProcessXpient(Optional ByVal Force As Boolean = False)
        Dim EntityId As Integer = GetEntityId()

        If LiveLinkConfiguration.XpientEnabled AndAlso _
            CheckBoxXpientEnabled.Checked = True AndAlso _
            EntityId > 0 AndAlso _
            (MyXpientLastPoll.AddMinutes(LiveLinkConfiguration.XpientPollInterval) < DateTime.Now Or Force) Then
            Try
                ButtonXpientImport.Enabled = False
                ' show a message in livelink, showing it is currently processing
                SetEndofDayStatus("Processing...")
                If (MyXpientPOS Is Nothing) Then
                    MyXpientPOS = New XpientPOS()
                End If

                ' process the xpient data.
                MyXpientPOS.DataProcess(EntityId)

                MyXpientLastPoll = Now
                MyXpientFirstPollDone = True
            Catch ex As Exception
                SetStatus("Error", ex.ToString)
            Finally
                ButtonXpientImport.Enabled = True
                SetEndofDayStatus("")
                UpdateXpientStatus()
            End Try
        End If
    End Sub

    Private Sub UpdateXpientStatus()
        txtXpientCurrentTime.Text = DateTime.Now.ToString("h:mm tt")
        txtXpientLastPoll.Text = IIf(MyXpientFirstPollDone, MyXpientLastPoll.ToString("h:mm tt"), "")
        txtXpientNextPoll.Text = IIf(CheckBoxXpientEnabled.Checked, IIf(MyXpientLastPoll = DateTime.MinValue, "0 min", CInt(MyXpientLastPoll.AddMinutes(LiveLinkConfiguration.XpientPollInterval).Subtract(DateTime.Now).TotalMinutes) & " min"), "")
        Application.DoEvents()
    End Sub

    Private Sub ButtonXpientImport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonXpientImport.Click
        If Not Processing Then
            Processing = True
            ProcessXpient(True)
            Processing = False
        End If
    End Sub


#End Region

#Region "Retalix Gateway"

    Private Sub ProcessRetalix(Optional ByVal Force As Boolean = False)
        Dim EntityId As Integer = GetEntityId()

        If LiveLinkConfiguration.RetalixEnabled And _
            CheckBoxRetalixEnabled.Checked = True And _
            EntityId > 0 AndAlso _
            (MyRetalixLastPoll.AddMinutes(LiveLinkConfiguration.RetalixPollInterval) < DateTime.Now Or Force) Then
            Try
                ButtonRetalixImport.Enabled = False
                ' show a message in livelink, showing it is currently processing
                SetEndofDayStatus("Processing...")

                If (MyRetalixPOS Is Nothing) Then
                    MyRetalixPOS = New RetalixPOS()
                End If
                MyRetalixPOS.ProcessData(EntityId)

                MyRetalixLastPoll = Now
                MyRetalixFirstPollDone = True
            Catch ex As Exception
                SetStatus("Error", ex.ToString)
            Finally
                ButtonRetalixImport.Enabled = True
                SetEndofDayStatus("")
                UpdateRetalixStatus()
            End Try
        End If
    End Sub

    Private Sub UpdateRetalixStatus()
        txtRetalixCurrentTime.Text = DateTime.Now.ToString("h:mm tt")
        txtRetalixLastPoll.Text = IIf(MyRetalixFirstPollDone, MyRetalixLastPoll.ToString("h:mm tt"), "")
        txtRetalixNextPoll.Text = IIf(CheckBoxRetalixEnabled.Checked, IIf(MyRetalixLastPoll = DateTime.MinValue, "0 min", CInt(MyRetalixLastPoll.AddMinutes(LiveLinkConfiguration.RetalixPollInterval).Subtract(DateTime.Now).TotalMinutes) & " min"), "")
        Application.DoEvents()
    End Sub

    Private Sub ButtonRetalixImport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonRetalixImport.Click
        If Not Processing Then
            Processing = True
            ProcessRetalix(True)
            Processing = False
        End If
    End Sub


#End Region

#Region "Breeze Gateway"

    Private Sub ProcessBreeze(Optional ByVal Force As Boolean = False)
        Dim entityId As Long = GetEntityId()

        If LiveLinkConfiguration.BreezeEnabled AndAlso _
            CheckBoxBreezeEnabled.Checked = True AndAlso _
            entityId > 0 AndAlso _
            (MyBreezeLastPoll.AddMinutes(LiveLinkConfiguration.BreezePollInterval) < DateTime.Now OrElse Force) Then
            Try
                BreezeProcessGroup.Enabled = False
                ' show a message in livelink, showing it is currently processing
                SetEndofDayStatus("Processing...")
                If (breezeImport Is Nothing) Then
                    breezeImport = New BreezePOS()
                End If
                ' process Breeze data (add reference to Breeze class here)
                Dim queryDate As DateTime? = Mx.Services.LiveLink.StoreInfoService.GetLastDate()
                Dim lastDate As DateTime? = queryDate
                If (Not queryDate.HasValue) Then
                    ' default to the maximum amount of days to store data for
                    queryDate = DateTime.Now.Date.AddDays(-LiveLinkConfiguration.DaysBeforeObsolete)
                ElseIf (LiveLinkConfiguration.DaysBeforeObsolete > 0 AndAlso _
                        queryDate.Value < DateTime.Now.Date.AddDays(-LiveLinkConfiguration.DaysBeforeObsolete)) Then
                    ' check if last date is older than "DaysBeforeObsolete"
                    queryDate = DateTime.Now.Date.AddDays(-LiveLinkConfiguration.DaysBeforeObsolete)
                ElseIf (Not MyBreezeFirstPollDone OrElse MyBreezeLastPoll.Date < DateTime.Now.Date) Then
                    ' if first poll has not been completed or the day has rolled over
                    If (queryDate > DateTime.Now.Date.AddDays(-LiveLinkConfiguration.BreezeDaysToRecoverOnStartUp)) Then
                        ' set query date to process the last X days on start up
                        queryDate = DateTime.Now.Date.AddDays(-LiveLinkConfiguration.BreezeDaysToRecoverOnStartUp)
                    End If
                Else
                    If (queryDate > DateTime.Now.Date.AddDays(-LiveLinkConfiguration.BreezeDaysToReprocess)) Then
                        ' set query date to process the last X days
                        queryDate = DateTime.Now.Date.AddDays(-LiveLinkConfiguration.BreezeDaysToReprocess)
                    End If
                End If
                breezeImport.LoadData()

                While (queryDate.Value <= DateTime.Now.Date)
                    Application.DoEvents()
                    UpdateBreezeProcessDate(queryDate)
                    Dim salesProcessed As Integer = breezeImport.Process(entityId, queryDate.Value)
                    Application.DoEvents()

                    ' update LastDate in tbStoreInfo with QueryDate
                    If (salesProcessed > 0 AndAlso lastDate.HasValue AndAlso queryDate > lastDate) Then
                        Mx.Services.LiveLink.StoreInfoService.UpdateLastDate(queryDate.Value)
                    End If
                    queryDate = queryDate.Value.AddDays(1)
                End While

                MyBreezeLastPoll = Now
                MyBreezeFirstPollDone = True
            Catch timeoutEx As TimeoutException
                SetStatus("Error", timeoutEx.Message)
            Catch ex As Exception
                SetStatus("Error", ex.ToString)
            Finally
                BreezeProcessGroup.Enabled = True
                SetEndofDayStatus("")
                UpdateBreezeStatus()
                ButtonBreezeImport.Text = "Process"
            End Try
        End If
    End Sub

    Private Sub UpdateBreezeStatus()
        txtBreezeCurrentTime.Text = DateTime.Now.ToString("h:mm tt")
        txtBreezeLastPoll.Text = IIf(MyBreezeFirstPollDone, MyBreezeLastPoll.ToString("h:mm tt"), "")
        txtBreezeNextPoll.Text = IIf(CheckBoxBreezeEnabled.Checked, IIf(MyBreezeLastPoll = DateTime.MinValue, "0 min", CInt(MyBreezeLastPoll.AddMinutes(LiveLinkConfiguration.BreezePollInterval).Subtract(DateTime.Now).TotalMinutes) & " min"), "")
        Application.DoEvents()
    End Sub

    Private Sub ButtonBreezeImport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonBreezeImport.Click
        If Not Processing Then
            Processing = True
            ProcessBreeze(True)
            Processing = False
        End If
    End Sub

    Private Sub ButtonBreezeUpdateProcessDate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonBreezeUpdateProcessDate.Click
        Try
            If (dtBreezeProcessDate.Value = DateTime.MinValue) Then
                SetStatus("", "Failed to update Breeze process date. No valid time set")
                Exit Sub
            End If
            If (dtBreezeProcessDate.Value > DateTime.Now) Then
                SetStatus("", "Failed to update Breeze process date. Date must be current day or prior")
                Exit Sub
            End If

            Dim entityId As Long = GetEntityId()
            If (entityId <= 0) Then
                SetStatus("", "Failed to update Breeze process date. No store configured")
                Exit Sub
            End If

            ' Instantiate the Breeze interface class if it hasn't been done already
            If breezeImport Is Nothing Then
                breezeImport = New BreezePOS()
            Else
                ' refresh the entityID cached value (in case it has been changed)
                breezeImport.EntityID = entityId
            End If

            ' set the last query date to specified date
            Mx.Services.LiveLink.StoreInfoService.UpdateLastDate(dtBreezeProcessDate.Value.Date)
            UpdateBreezeProcessDate(dtBreezeProcessDate.Value)

        Catch ex As Exception
            LogEvent(EventLogEntryType.Error, "Exception occurred updating Breeze process date. Exception: {0}", ex.ToString)
        End Try
    End Sub

    Private Sub UpdateBreezeProcessDate(ByVal processDate As DateTime, Optional ByVal updateInformationLabel As Boolean = True)
        dtBreezeProcessDate.Value = processDate
        dtBreezeProcessDate.Refresh()
        ' check if label should be updated (only update if actual value is known)
        If (updateInformationLabel) Then
            txtBreezeProcessDate.Text = processDate.ToString("dd-MMM-yyyy")
        End If
    End Sub

#End Region

#Region " POSixml POS"

    Private Sub ProcessPOSixml(Optional ByVal force As Boolean = False)
        If LiveLinkConfiguration.POSixmlEnabled And _
                CheckBoxPOSixmlEnabled.Checked = True And _
                (MyPOSixmlLastPoll.AddMinutes(LiveLinkConfiguration.POSixmlPollInterval) < DateTime.Now Or force) Then
            Try
                ButtonPOSixmlImport.Enabled = False
                ' show a message in livelink, showing it is currently processing
                SetEndofDayStatus("Processing...")

                ' process POSixml data
                Dim posixmlPOS As POSixmlImport = New POSixmlImport()
                posixmlPOS.ProcessExportXmlFiles()

                MyPOSixmlLastPoll = Now
                MyPOSixmlFirstPollDone = True
            Catch ex As Exception
                SetStatus("Error", ex.ToString)
            Finally
                ButtonPOSixmlImport.Enabled = True
                SetEndofDayStatus("")
                UpdatePOSixmlStatus()
            End Try
        End If
    End Sub


    Private Sub UpdatePOSixmlStatus()
        txtPOSixmlCurrentTime.Text = DateTime.Now.ToString("h:mm tt")
        txtPOSixmlLastPoll.Text = IIf(MyPOSixmlFirstPollDone, MyPOSixmlLastPoll.ToString("h:mm tt"), "")
        txtPOSixmlNextPoll.Text = IIf(CheckBoxPOSixmlEnabled.Checked, IIf(MyPOSixmlLastPoll = DateTime.MinValue, "0 min", CInt(MyPOSixmlLastPoll.AddMinutes(LiveLinkConfiguration.POSixmlPollInterval).Subtract(DateTime.Now).TotalMinutes) & " min"), "")
        Application.DoEvents()
    End Sub

    Private Sub ButtonPOSixmlImport_Click(sender As System.Object, e As System.EventArgs) Handles ButtonPOSixmlImport.Click
        If Not Processing Then
            Processing = True
            ProcessPOSixml(True)
            Processing = False
        End If
    End Sub

#End Region

#Region " Radiant RPOS"

    Private Sub ProcessRPOS(Optional ByVal force As Boolean = False)
        If LiveLinkConfiguration.RPOSEnabled And _
                CheckBoxRPOSEnabled.Checked = True And _
                (MyRPOSLastPoll.AddMinutes(LiveLinkConfiguration.RPOSPollInterval) < DateTime.Now Or force) Then
            Try
                ButtonRPOSImport.Enabled = False
                ' show a message in livelink, showing it is currently processing
                SetEndofDayStatus("Processing...")

                ' process RPOS data
                Dim rposPOS As RadiantRPOS = New RadiantRPOS()
                rposPOS.ProcessData()

                MyRPOSLastPoll = Now
                MyRPOSFirstPollDone = True
            Catch ex As Exception
                SetStatus("Error", ex.ToString)
            Finally
                ButtonRPOSImport.Enabled = True
                SetEndofDayStatus("")
                UpdateRPOSStatus()
            End Try
        End If
    End Sub

    Private Sub UpdateRPOSStatus()
        txtRPOSCurrentTime.Text = DateTime.Now.ToString("h:mm tt")
        txtRPOSLastPoll.Text = IIf(MyRPOSFirstPollDone, MyRPOSLastPoll.ToString("h:mm tt"), "")
        txtRPOSNextPoll.Text = IIf(CheckBoxRPOSEnabled.Checked, IIf(MyRPOSLastPoll = DateTime.MinValue, "0 min", CInt(MyRPOSLastPoll.AddMinutes(LiveLinkConfiguration.RPOSPollInterval).Subtract(DateTime.Now).TotalMinutes) & " min"), "")
        Application.DoEvents()
    End Sub

    Private Sub ButtonRPOSImport_Click(sender As System.Object, e As System.EventArgs) Handles ButtonRPOSImport.Click
        If Not Processing Then
            Processing = True
            ProcessRPOS(True)
            Processing = False
        End If
    End Sub

#End Region

#Region " ZK Fingerprint Scanner"


    Private Function ProcessZKFingerprintsTA(Optional ByVal Force As Boolean = False) As Boolean
        Dim entityId As Integer = GetEntityId()
        If entityId > 0 Then
            If LiveLinkConfiguration.ZKFingerprintEnabled And _
                    CheckBoxZKEnabled.Checked = True And _
                    (MyZKFingerprintLastPoll.AddMinutes(LiveLinkConfiguration.ZKFingerprintPollInterval) < DateTime.Now Or Force) Then
                Try
                    ButtonZKFingerprintImport.Enabled = False

                    If MyZKFingerprint Is Nothing Then
                        ' initialise the fingerprint device interdface
                        MyZKFingerprint = New zkFingerPrint(entityId, LiveLinkConfiguration.ZKFingerprintIP, LiveLinkConfiguration.ZKFingerprintPort)
                    End If

                    ' get all the TA records
                    MyZKFingerprint.ExecuteRequest(zkFingerPrint.RequestType.RetrieveTA)

                    MyZKFingerprintLastPoll = Now
                    MyZKFingerprintFirstPollDone = True
                Catch ex As Exception
                    SetStatus("Error", ex.ToString)
                Finally
                    ButtonZKFingerprintImport.Enabled = True
                    SetEndofDayStatus("")
                    UpdateZKFingerprintStatus()
                End Try
            End If
        End If
    End Function


    Private Sub UpdateZKFingerprintStatus()
        txtZKCurrentTime.Text = DateTime.Now.ToString("h:mm tt")
        txtZKLastPoll.Text = IIf(MyZKFingerprintFirstPollDone, MyZKFingerprintLastPoll.ToString("h:mm tt"), "")
        txtZKNextPoll.Text = IIf(CheckBoxZKEnabled.Checked, IIf(MyZKFingerprintLastPoll = DateTime.MinValue, "0 min", CInt(MyZKFingerprintLastPoll.AddMinutes(LiveLinkConfiguration.ZKFingerprintPollInterval).Subtract(DateTime.Now).TotalMinutes) & " min"), "")
        Application.DoEvents()
    End Sub


    Private Sub ButtonZKFingerprintImport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonZKFingerprintImport.Click
        If Not Processing Then
            Processing = True
            ProcessZKFingerprintsTA(True)
            Processing = False
        End If
    End Sub

#End Region

#Region "Time Punch Import"

    Private Sub ProcessTimePunchImport(Optional ByVal force As Boolean = False)
        Dim entityId As Integer = GetEntityId()

        If (LiveLinkConfiguration.TimePunchImportEnabled AndAlso entityId > 0 AndAlso _
            MyTimePunchImportLastPoll.AddMinutes(LiveLinkConfiguration.TimePunchImportPollInterval) < DateTime.Now Or force) Then
            Try
                ' show a message in livelink, showing it is currently processing
                SetEndofDayStatus("Processing...")
                If (MyTimePunchImportPOS Is Nothing) Then
                    MyTimePunchImportPOS = New TimePunchImportPOS()
                End If

                ' process the Time Punch data.
                MyTimePunchImportPOS.DataProcess(entityId)

                MyTimePunchImportLastPoll = Now
            Catch ex As Exception
                SetStatus("Error", ex.ToString)
            Finally
                SetEndofDayStatus("")
                Application.DoEvents()
            End Try
        End If
    End Sub

#End Region

#Region "SUS POS Import"

    Private Sub ProcessSUSPOSImport(Optional ByVal force As Boolean = False)
        Dim entityId As Integer = GetEntityId()

        If (LiveLinkConfiguration.SUSImportEnabled AndAlso entityId > 0 AndAlso _
            MySUSPOSImportLastPoll.AddMinutes(LiveLinkConfiguration.SUSImportPollInterval) < DateTime.Now Or force) Then
            Try
                ' show a message in livelink, showing it is currently processing
                SetEndofDayStatus("Processing...")
                If (MySUSPOSImportPOS Is Nothing) Then
                    MySUSPOSImportPOS = New SUSPOS()
                End If

                ' process the Time Punch data.
                MySUSPOSImportPOS.DataProcess(entityId)

                MySUSPOSImportLastPoll = Now
            Catch ex As Exception
                SetStatus("Error", ex.ToString)
            Finally
                SetEndofDayStatus("")
                Application.DoEvents()
            End Try
        End If
    End Sub

#End Region

#Region "Common POS"

    Private Sub btImportCommon_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btImportCommon.Click
        If Not Processing Then
            Processing = True
            SetEndofDayStatus("Importing ...")
            ProcessPosCommon(True)
            SetEndofDayStatus("")
            Processing = False
        End If
    End Sub

    Private Sub ProcessPosCommon(Optional ByVal force As Boolean = False)
        If chkEnableGatewayCommon.Checked = True AndAlso _
                (_posCommon.LastPollDate.AddMinutes(_posCommon.PollInterval) < DateTime.Now OrElse force) Then
            Try
                btImportCommon.Enabled = False
                Dim entityId As Integer = GetEntityId()
                If entityId > 0 Then
                    _posCommon.ProcessData(entityId)
                End If
            Catch ex As Exception
                SetStatus("Error", ex.ToString)
            Finally
                btImportCommon.Enabled = True
            End Try

            _posCommon.LastPollDate = Now

            UpdatePosStatusCommon()
        End If
    End Sub

    Private Sub UpdatePosStatusCommon()
        txtCurrentTime.Text = DateTime.Now.ToString("h:mm tt")
        txtLastPoll.Text = IIf(_posCommon.LastPollDate = DateTime.MinValue, "", _posCommon.LastPollDate.ToString("h:mm tt"))
        txtNextPoll.Text = IIf(chkEnableGatewayCommon.Checked, IIf(_posCommon.LastPollDate = DateTime.MinValue, "0 min", CInt(_posCommon.LastPollDate.AddMinutes(_posCommon.PollInterval).Subtract(DateTime.Now).TotalMinutes) & " min"), "")
        Application.DoEvents()
    End Sub

    'Action trigger when pos status changed
    Private Sub ShowStatus(ByVal sender As Object, ByVal e As Progress)
        'Show status for common pos
        Dim msg As KeyValuePair(Of String, String) = e.StatusMsg
        SetStatus(msg.Key, msg.Value)
    End Sub

#End Region

#Region "Speed of service reporting"

    Private Function ProcessServiceReport() As Boolean
        Dim entityId As Integer = GetEntityId()
        If entityId > 0 Then
            ' check if process is set to poll
            If LiveLinkConfiguration.ServiceReportEnabled And (MyServiceReportLastPoll.AddMinutes(LiveLinkConfiguration.ServiceReportInterval) < DateTime.Now) Then
                Try
                    ' get the speed of service reporting data and add it to the database
                    If ServiceReport.GetServiceTimes(entityId) Then
                        SetStatus("Info", "Speed of service report processed")
                    Else
                        SetStatus("Error", "Speed of service report failed")
                    End If
                    ' set last poll time to now
                    MyServiceReportLastPoll = Now
                Catch ex As Exception
                    SetStatus("Error", ex.ToString)
                End Try
            End If
        End If
    End Function

#End Region

#Region " DCA POS   "

    Private Sub Button9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button9.Click
        If OpenFileDialog1.ShowDialog() = 1 Then
            TextBoxDCAPriceSetImportFileName.Text = OpenFileDialog1.FileName
        End If
    End Sub

    Private Sub Button10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button10.Click
        Try
            If CheckBoxDCAActive.Checked Then
                LiveLinkConfiguration.DCAPriceSetEnabled = True
            Else
                LiveLinkConfiguration.DCAPriceSetEnabled = False
            End If
            LiveLinkConfiguration.DCAPriceSetCheckInterval = CInt(Val(TextBoxDCAPriceSetCheckInterval.Text))
            LiveLinkConfiguration.DCAPriceSetImportFileName = TextBoxDCAPriceSetImportFileName.Text
            LiveLinkConfiguration.DCAPriceSetImportFileConnectionString = TextBoxDCAPriceSetImportFileConnectionString.Text
            LiveLinkConfiguration.DCAPriceSetPOSFileConnectionString = TextBoxDCAPriceSetPOSFileConnectionString.Text

            LiveLinkConfiguration.Save()
        Catch

        End Try
    End Sub

    Private Sub ProcessDCA()
        'Import text file with Price changes

        'Set Prices on MSDE database


        'delete  import file

        'Write to log table????

    End Sub

#End Region

#Region "Alert Systems Traffic Data"

    Private Sub UpdateAlertSystemsTrafficDataStatus()
        Label110.Text = DateTime.Now.ToString("dd MMM yyyy HH:mm") ' current time
        Label109.Text = MyAlertSystemsTrafficDataPollInterval & " min" ' poll interval
        Label107.Text = IIf(MyAlertSystemsTrafficDataFirstPollDone, MyAlertSystemsTrafficDataLastPoll.ToString("h:mm tt"), "") ' last poll
        Label105.Text = LiveLinkConfiguration.AlertSystemsTrafficDataFilePath
        Application.DoEvents()
    End Sub

    Private Sub btnTrafficDataProcess_Click(sender As System.Object, e As System.EventArgs) Handles btnTrafficDataProcess.Click
        If Not Processing Then
            Processing = True
            ProcessTrafficData(True)
            Processing = False
        End If
    End Sub

    Private Sub ProcessTrafficData(Optional ByVal Force As Boolean = False)
        Try
            If LiveLinkConfiguration.AlertSystemsTrafficDataFileTransferEnabled Then
                LogStatus("Beginning transmitting traffic data...")
                ProcessAlertSystemsTrafficDataFiles(Force)
                LogStatus("Finished transmitting traffic data.")
            End If
        Catch ex As Exception
            SetStatus("Error", ex.Message)
        End Try
    End Sub

    Public Sub ProcessAlertSystemsTrafficDataFiles(Optional ByVal Force As Boolean = False)
        Dim entityId As Integer = GetEntityId()

        If (LiveLinkConfiguration.AlertSystemsTrafficDataFileTransferEnabled AndAlso entityId > 0 AndAlso MyAlertSystemsTrafficDataLastPoll.AddMinutes(LiveLinkConfiguration.AlertSystemsTrafficDataPollInterval) < DateTime.Now Or Force) Then
            Try
                SetEndofDayStatus("Processing...")
                GetLease()

                Dim liveLinkWebService As LiveLinkWebService = New LiveLinkWebService()
                liveLinkWebService.Url = LiveLinkConfiguration.WebService_URL
                LoadProxy(liveLinkWebService)

                Dim trafficDataProcessor As AlertSystemsFileProcessor = New AlertSystemsFileProcessor(MyIdentity, mySessionKey, entityId, liveLinkWebService)
                trafficDataProcessor.ProcessTrafficDataFiles()

                MyAlertSystemsTrafficDataLastPoll = DateTime.Now
                MyAlertSystemsTrafficDataFirstPollDone = True
            Catch ex As Exception
                SetStatus("Error", ex.ToString)
            Finally
                SetEndofDayStatus(String.Empty)
                UpdateAlertSystemsTrafficDataStatus()
                Application.DoEvents()
            End Try
        End If
    End Sub

#End Region

#Region " Setup Tab   "

    Private Structure EntityListItem
        Dim EntityId As Integer
        Dim Entity As String

        Public Sub New(ByVal EntityId As Integer, ByVal Entity As String)
            Me.EntityId = EntityId
            Me.Entity = Entity
        End Sub

        Public Overrides Function ToString() As String
            Return Me.Entity
        End Function
    End Structure

    Private Sub btnLoadStore_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLoadStores.Click
        Dim MyData As New LiveLinkWebService ' TODO: Enable GZip support: eRetailerCompressed
        Dim MyLiveLinkResponse As Mx.POS.Common.eRetailer.LiveLinkResponse
        Dim Row As DataRow
        Dim Store As EntityListItem

        Try
            MyData.Url = myWebService
            LoadProxy(MyData)
            cboStores.Items.Clear()
            cboStores.Items.Add("Loading ...")
            cboStores.SelectedIndex = 0
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            GetLease()
            If myConditionalSessionKey <> String.Empty Then
                MyLiveLinkResponse = MyData.ListStores(MyIdentity, myConditionalSessionKey)
                If MyLiveLinkResponse.ResponseCode = eRetailer.LiveLinkResponseCode.Success Then
                    cboStores.Items.Clear()
                    If MyLiveLinkResponse.RecordCount = 0 Then
                        cboStores.Items.Add("No stores found")
                    Else
                        cboStores.Items.Clear()
                        For Each Row In MyLiveLinkResponse.ResponseData.Tables("StoreList").Rows
                            cboStores.Items.Add(New EntityListItem(Row.Item("EntityId"), Row.Item("Entity")))
                        Next
                        btnUpdateStore.Enabled = True
                        SetStatus("", "Stores loaded")
                        SuccessfulCommunications()
                        If IsNumeric(txtConfiguredStore.Text) And ((txtConfiguredStore.Text) <> "-1") Then
                            For Each Store In cboStores.Items
                                If Store.EntityId = txtConfiguredStore.Text Then
                                    cboStores.SelectedIndex = cboStores.Items.IndexOf(Store)
                                    Exit For
                                End If
                            Next
                        End If
                        If cboStores.SelectedIndex = -1 Then
                            cboStores.SelectedIndex = 0
                        End If
                        btnLoadStores.Enabled = False
                    End If
                Else
                    cboStores.Items.Clear()
                    cboStores.Items.Add("Unable to load stores")
                    cboStores.SelectedIndex = 0
                    ProcessResponse(MyLiveLinkResponse)
                End If
            Else
                cboStores.Items.Clear()
                cboStores.Items.Add("Unable to load stores")
                cboStores.SelectedIndex = 0
            End If
        Catch ex As WebException
            SetStatus("Error", ex.Message)
            cboStores.Items.Clear()
            cboStores.Items.Add("Unable to load stores")
            cboStores.SelectedIndex = 0
            BackOffCommunications(False)
        Catch ex As Exception
            cboStores.Items.Clear()
            cboStores.Items.Add("Unable to load stores")
            cboStores.SelectedIndex = 0
        End Try
        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub

    Private Sub cboStores_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboStores.SelectedIndexChanged
        txtEntityId.Text = ""
        Try
            Dim MyEntityItem As EntityListItem = CType(cboStores.SelectedItem, EntityListItem)
            txtEntityId.Text = MyEntityItem.EntityId
        Catch ex As Exception
        End Try
    End Sub

    Private Sub TabControl1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TabControl1.SelectedIndexChanged
        Select Case TabControl1.SelectedTab.Text
            Case tabSetup.Text
                cboStores.Items.Clear()
                cboStores.Items.Add("No stores loaded")
                cboStores.SelectedIndex = 0
                btnUpdateStore.Enabled = False
                txtConfiguredStore.Text = GetEntityId()
                If txtConfiguredStore.Text = "-1" Then
                    txtConfiguredStore.Text = "unknown"
                End If
                btnLoadStores.Enabled = True
            Case tabProxy.Text
                SetupProxyTab()
        End Select
    End Sub

    Private Function GetRegisterId() As Int64
        ' This function returns the EntityId that is used to populate tbSalesMain
        ' It differs for each POS type
        Dim Use_ODBC As Boolean = LiveLinkConfiguration.Use_ODBC_DB_Connect
        Dim sConnect_ODBC As String = LiveLinkConfiguration.ODBC_DB_Connect
        Dim sPOSType As String = LiveLinkConfiguration.POSType
        Dim sSql As String = String.Empty
        Dim WSDataSet As DataSet
        Dim sError As String = String.Empty

        GetRegisterId = -1
        Try
            Select Case sPOSType.ToUpper.Trim
                Case "PARJOURNAL", "PAN7700", "CRATOS", "ALOHA", "SIGNATURE", "PARINFUSION", "PCAMERICA", "PIXELPOINT", "COMPRIS", "PAREXALT4", "SILVERWARE", "ICG", "MICROSOFTRMS", "NEWPOS", "IPOS", "UNIWELL"
                Case "MICROS"
                Case "QUICKEN"
                    ' EThe unique register ID is passed through to the web service.  
                    ' Retrive the register Id from tbSalesMain - Serial_Number.
                    sSql = "SELECT Top 1 Serial_Number As RegisterId From tbSalesMain Order by SalesMainId DESC"
                Case "PROTOUCH"
                Case Else
            End Select

            If sSql <> String.Empty Then
                If Use_ODBC Then
                    If MMSLocalGeneric_ListAll(sConnect_ODBC, sSql, "tbStoreInfo", WSDataSet, sError) Then
                        If WSDataSet.Tables("tbStoreinfo").Rows.Count = 1 Then
                            ' Quicken uses 8 byte serial numbers that overflow int64.  COnvert to unsigned int64 and then back with -ve sign if necessary
                            GetRegisterId = Int64.Parse(Convert.ToUInt64(WSDataSet.Tables("tbStoreInfo").Rows(0).Item("RegisterId")).ToString("X"), Globalization.NumberStyles.HexNumber)
                        End If
                    End If
                Else
                    If MMSGeneric_ListAll(sSql, "tbStoreInfo", WSDataSet, sError) Then
                        If WSDataSet.Tables("tbStoreinfo").Rows.Count = 1 Then
                            ' Quicken uses 8 byte serial numbers that overflow int64.  COnvert to unsigned int64 and then back with -ve sign if necessary
                            GetRegisterId = Int64.Parse(Convert.ToUInt64(WSDataSet.Tables("tbStoreInfo").Rows(0).Item("RegisterId")).ToString("X"), Globalization.NumberStyles.HexNumber)
                        End If
                    End If
                End If
            End If

        Catch ex As Exception
            GetRegisterId = -1
        End Try
    End Function

    Private Function SaveEntityId(ByVal EntityId As Integer) As Boolean
        ' This function saves EntityId that is used to populate tbSalesMain
        ' The location differs for each POS type
        Dim Use_ODBC As Boolean = LiveLinkConfiguration.Use_ODBC_DB_Connect
        Dim sConnect_ODBC As String = LiveLinkConfiguration.ODBC_DB_Connect
        Dim sPOSType As String = LiveLinkConfiguration.POSType
        Dim sSql As String = String.Empty
        Dim sError As String = String.Empty

        SaveEntityId = False

        Try
            Select Case sPOSType.ToUpper
                Case "PARJOURNAL", "PAN7700", "CRATOS", "ALOHA", "SIGNATURE", "PARINFUSION", "MICROS9700", "PCAMERICA", "PIXELPOINT", "COMPRIS", "INTOUCH", "PAREXALT4", "SILVERWARE", "OCTANE", "ICG", "MICROSOFTRMS", "NEWPOS", "IPOS", "UNIWELL"
                    ' Entity Id is stored in the tbStoreInfo table in the MacromatiX database
                    sSql = "IF NOT EXISTS (SELECT * FROM tbStoreInfo) "
                    sSql = sSql + "INSERT INTO tbStoreInfo (StoreId, RegisterId) VALUES (" & EntityId & ", 0) "
                    sSql = sSql + "ELSE "
                    sSql = sSql + "UPDATE tbStoreinfo SET StoreId = " & EntityId & " "
                Case "MICROS"
                    ' Entity Id is saved in the custom.tbStoreInfo table in the Micros Sybase database
                    sSql = "IF NOT EXISTS (SELECT * FROM custom.tbStoreInfo) "
                    sSql = sSql + "INSERT INTO custom.tbStoreInfo (StoreId) VALUES (" & EntityId & ") "
                    sSql = sSql + "ELSE "
                    sSql = sSql + "UPDATE custom.tbStoreinfo SET StoreId = " & EntityId & " "
                Case "QUICKEN"
                    ' Entity Id is not set anywhere on the POS.  The unique register ID is passed through to
                    ' the web service.  Store the Entity Id in mxlivelink.config as a double check only.
                    LiveLinkConfiguration.StoreID = EntityId
                    LiveLinkConfiguration.Save()
                    SaveEntityId = True
                Case "PROTOUCH"
                    ' Save the EntityId from the Protouch ini file
                    Dim ProTouchIniFile As String = LiveLinkConfiguration.ProTouchIniFile
                    If ProTouchIniFile = "" Then
                        ProTouchIniFile = cProTouchIniFile
                    End If
                    Dim objIniFile As New IniFile(ProTouchIniFile)
                    objIniFile.WriteInteger("MacroMatix", "EntityId", EntityId)
                    If objIniFile.GetInteger("MacroMatix", "EntityId", "-1") = EntityId Then
                        SaveEntityId = True
                    End If
                Case Else
                    If Use_ODBC Then
                        ' default to saving the Entity Id in mxlivelink.config
                        LiveLinkConfiguration.StoreID = EntityId
                        LiveLinkConfiguration.Save()
                        SaveEntityId = True
                    Else
                        ' default to saving the Entity Id in tbStoreInfo
                        sSql = "IF NOT EXISTS (SELECT * FROM tbStoreInfo) "
                        sSql = sSql + "INSERT INTO tbStoreInfo (StoreId, RegisterId) VALUES (" & EntityId & ", 0) "
                        sSql = sSql + "ELSE "
                        sSql = sSql + "UPDATE tbStoreinfo SET StoreId = " & EntityId & " "
                    End If
            End Select
            If sSql <> String.Empty Then
                If Use_ODBC Then
                    If MMSLocalGeneric_Execute(sConnect_ODBC, sSql, sError) Then
                        SaveEntityId = True
                    Else
                        LogEvent("Unable to save store number" & vbCrLf & "Error: " & sError & vbCrLf & "SQL: " & sSql, EventLogEntryType.Error)
                    End If
                Else
                    If MMSGeneric_Execute(sSql, sError) Then
                        SaveEntityId = True
                    Else
                        LogEvent("Unable to save store number" & vbCrLf & "Error: " & sError & vbCrLf & "SQL: " & sSql, EventLogEntryType.Error)
                    End If
                End If
            End If
        Catch ex As Exception
            SaveEntityId = False
        End Try
    End Function

    Private Sub btnUpdateStore_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdateStore.Click
        If txtEntityId.Text <> txtConfiguredStore.Text Then
            If MsgBox("Are you sure that you wish to update the store number?", MsgBoxStyle.Question + MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton2, "Save store number?") = MsgBoxResult.Yes Then
                If SaveEntityId(txtEntityId.Text) Then
                    txtConfiguredStore.Text = txtEntityId.Text
                    If (LiveLinkConfiguration.UseOfflineCashup_KeyExists AndAlso LiveLinkConfiguration.UseOfflineCashup) Then
                        RetrieveOfflineCashupSettings()
                    End If
                Else
                    MsgBox("Unable to save the new store number", MsgBoxStyle.OkOnly + MsgBoxStyle.Exclamation, "Error")
                End If
            End If
        End If
    End Sub

    Private Sub ButtonNewMachineID_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonNewMachineID.Click
        If MessageBox.Show("Are you sure you want to clear the existing Machine ID?", "LiveLink", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
            LogEvent("Re-Creating Unique MachineId", EventLogEntryType.Information)

            ' create a new machine id, and re-create the unique machineId
            _identity = String.Empty
            LeaseExpired = True ' expire lease, a new machine ID needs a new lease

            If RecreateUniqueMachineId(_identity) Then
                MessageBox.Show("A new Machine ID has been created." + vbCrLf + vbCrLf + "You will need to re-active LiveLink!", "LiveLink", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Else
                MessageBox.Show("Failed to create a new Machine ID", "LiveLink", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        End If
    End Sub

    'Private Sub ButtonResetSchema_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonResetSchema.Click
    '    If MessageBox.Show("Are you sure you want reset the database Schema?" + vbCrLf + "All historical data will be deleted.", "LiveLink", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
    '        DropTables()
    '        SetupDatabase()
    '    End If
    'End Sub

#End Region

#Region "  Session/Lease Handling   "

    Function CreateChallenge() As String

        Dim sClientCode As String = LiveLinkConfiguration.ClientCode
        Dim sSecurityCode As String = LiveLinkConfiguration.SecurityCode
        Dim ChallengeTime As String = Format(Date.UtcNow, "yyyyMMddHHmmss")
        CreateChallenge = Polling.ChallengeResponse(Guid.NewGuid.ToString(), sClientCode, sSecurityCode) & ChallengeTime & Format(CInt(SessionServiceType.LiveLink), "000")
    End Function

    Private Sub ExpireLease()
        LeaseExpired = True
        mySessionKey = String.Empty
        myConditionalSessionKey = String.Empty
    End Sub

    Private Sub ShowDebug(ByVal Msg As String, ByVal DebugLevel As DebugModes)
        Try
            Select Case DebugMode
                Case DebugModes.Simple
                    If (DebugLevel = DebugModes.Simple) Then
                        SetStatus("Debug", Msg)
                    End If
                Case DebugModes.Verbose
                    If (DebugLevel = DebugModes.Simple) Or (DebugLevel = DebugModes.Verbose) Then
                        SetStatus("Debug", Msg)
                    End If
            End Select
        Catch ex As Exception
        End Try
    End Sub

    Private Function HasConditionalLease() As Boolean
        ' Returns true if a conditional lease has been obtained
        If Not LeaseExpired And mySessionKey = String.Empty And myConditionalSessionKey <> String.Empty Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub GetLease()
        Dim MySession As New LiveLinkWebService ' TODO: Enable GZip support: eRetailerCompressed
        Dim MyLiveLinkResponse As Mx.POS.Common.eRetailer.LiveLinkResponse
        Dim ResponseServiceType As SessionServiceType
        Dim myKey As String
        Dim myChallenge As String
        Dim LeaseObtained As Boolean = False

        If MyIdentity <> String.Empty Then
            MySession.Url = myWebService
            LoadProxy(MySession)

            If LeaseExpired Then
                Try
                    SetDebugLevel()
                    myChallenge = CreateChallenge()
                    SetStatus("", "Acquiring lease")
                    ShowDebug("Live Link Challenge: [" & myChallenge & "]", DebugModes.Verbose)
                    MyLiveLinkResponse = MySession.RequestLease(MyIdentity, myChallenge, Mx.BusinessObjects.VersionUtil.GetBuild())
                    If MyLiveLinkResponse.ResponseCode = eRetailer.LiveLinkResponseCode.Success Or _
                    MyLiveLinkResponse.ResponseCode = eRetailer.LiveLinkResponseCode.AwaitingActivation Then
                        Dim sClientCode As String = LiveLinkConfiguration.ClientCode
                        Dim sSecurityCode As String = LiveLinkConfiguration.SecurityCode
                        Dim ParseResult As Integer
                        ShowDebug("Server Response: [" & MyLiveLinkResponse.ResponseString & "]", DebugModes.Verbose)
                        ParseResult = Polling.ParseKey(MyLiveLinkResponse.ResponseString, True, myKey, ResponseServiceType)
                        ShowDebug("Server Response Key: [" & myKey & "]", DebugModes.Verbose)
                        If ParseResult = 0 Then
                            ' At this stage a valid response was returned, within the date range
                            Dim myServerChallenge As String = Polling.ChallengeResponse(myChallenge, sClientCode, sSecurityCode)
                            ShowDebug("Live Link Validation: [" & myServerChallenge & "]", DebugModes.Verbose)
                            If (ResponseServiceType = SessionServiceType.LiveLink) And (myKey = myServerChallenge) Then
                                ' At this stage, the service code recived is correct and the initial challange was met 
                                ' with a valid response.  ie The server has properly authenticated itself to the client
                                ' Now calculate the Session key that will be used to authticate the client to the server.
                                ' (the server has already saved its own calculated version into tbSessionStatus)
                                myConditionalSessionKey = Polling.ChallengeResponse(MyLiveLinkResponse.ResponseString, sClientCode, sSecurityCode) & Format(CInt(SessionServiceType.LiveLink), "000")
                                ' If the client has not yet been activated by the server (manual step), then the session key
                                ' can only be used for limited functionality.
                                If MyLiveLinkResponse.ResponseCode = eRetailer.LiveLinkResponseCode.Success Then
                                    ' If the client is deemed to be Online by the server (ie activated), then the
                                    ' session key may be used for all functions.
                                    mySessionKey = myConditionalSessionKey
                                    PingEnabled = True
                                    ' Alter the transaction flow if required
                                    With MyLiveLinkResponse
                                        Dim FlowChanged As Boolean = False
                                        Dim AwakeFlowChanged As Boolean = False
                                        If (.ResponseFlow(0) > 0) And (.ResponseFlow(0) <> myFlowTxInBatch) Then
                                            myFlowTxInBatch = .ResponseFlow(0)
                                            FlowChanged = True
                                        End If
                                        If (.ResponseFlow(1) > 0) And (.ResponseFlow(1) <> myFlowIterations) Then
                                            myFlowIterations = .ResponseFlow(1)
                                            FlowChanged = True
                                        End If
                                        If (.ResponseFlow(2) > 0) And (.ResponseFlow(2) <> myFlowMaxRecords) Then
                                            myFlowMaxRecords = .ResponseFlow(2)
                                            FlowChanged = True
                                        End If
                                        If (.ResponseFlow(3) > 0) And (.ResponseFlow(3) <> myFlowInterval) Then
                                            myFlowInterval = .ResponseFlow(3)
                                            txtFlowInterval.Text = myFlowInterval
                                            FlowChanged = True
                                        End If
                                        If FlowChanged Then
                                            SetStatus("", "Flow adjusted: " & myFlowIterations.ToString & " batches of " & myFlowTxInBatch.ToString & " (Max records: " & myFlowMaxRecords & ")")
                                            SetStatus("", "Flow adjusted: Send interval - " & myFlowInterval.ToString & " minute(s)")
                                        End If
                                        If (.ResponseFlow(4) > 0) And (.ResponseFlow(4) <> myAwakeInterval) Then
                                            myAwakeInterval = .ResponseFlow(4)
                                            TextBoxAwake.Text = myAwakeInterval
                                            AwakeFlowChanged = True
                                        End If
                                        If (.ResponseFlow(5) > 0) And (.ResponseFlow(5) <> myAwakeCounterVerbose) Then
                                            myAwakeCounterVerbose = .ResponseFlow(5)
                                            AwakeFlowChanged = True
                                        End If
                                        If AwakeFlowChanged Then
                                            SetStatus("", "Awake Flow adjusted: Send interval - " & myAwakeInterval.ToString & " minute(s)")
                                            SetStatus("", "Awake Flow adjusted: Verbose iterations - " & myAwakeCounterVerbose.ToString)
                                        End If
                                    End With
                                Else ' If site not activated then ...
                                    BackOffCommunications(True) ' Make sure that comms slowly backoff if site is not activated
                                End If
                                tmrSessionLease.Stop()
                                ' Set the lease time as 30 seconds less than the granted lease time to account for
                                ' communication delays
                                ' Lease must be between 1 and 60 minutes
                                tmrSessionLease.Interval = Max(1, Min(60, CInt(MyLiveLinkResponse.ResponseNumber))) * 60000 - 30000
                                LeaseExpired = False
                                LeaseObtained = True
                                MessageWaiting = MyLiveLinkResponse.ResponseMessageWaiting
                                tmrSessionLease.Start()
                                SetStatus("", "Lease acquired")
                            Else
                                ExpireLease()
                                PingEnabled = False
                                SetStatus("", "Lease acquisition failed - negotiation failed")
                            End If
                        Else
                            ExpireLease()
                            PingEnabled = False
                            SetStatus("", "Lease acquisition failed - invalid response (code " & ParseResult & ")")
                        End If
                    Else
                        ExpireLease()
                        PingEnabled = False
                        ProcessResponse(MyLiveLinkResponse)
                    End If
                    If Not LeaseObtained Then
                        SetStatus("", "Lease not obtained")
                        BackOffCommunications(False)
                    End If

                Catch ex As WebException
                    SetStatus("Error", ex.Message)
                    BackOffCommunications(False)
                Catch ex As Exception
                    LogEvent(String.Format("Error occurred while obtaining lease{0}Error: {1}", vbCrLf, ex), EventLogEntryType.Error)
                    SetStatus("", "Error occurred while obtaining lease.")
                End Try
            End If
        Else
            ExpireLease()
            SetStatus("Error", "Unable to determine LiveLink's unique id.")
        End If
    End Sub

    Private Sub tmrSessionLease_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmrSessionLease.Tick
        ExpireLease()
    End Sub

#End Region

#Region "  SQL Functions "

    Private Function sqlSelectTopNonSyncedHeaders(ByVal sPosType As String, ByVal TxInBatch As Integer, Optional ByVal DataType As DataType = DataType.SalesData) As String
        Dim sFieldList As String = GettbSalesMainFields(sPosType)
        Select Case DataType
            Case DataType.SalesData
                'First record must be header record "H" or a "1" for Quicken
                Select Case sPosType.ToUpper
                    Case "QUICKEN"
                        sqlSelectTopNonSyncedHeaders = "Select Top " & TxInBatch.ToString & " " & sFieldList & " from tbSalesMain Where (Sequence_Number=1) and (Synced=0) Order By Date_Time, SalesMainID "
                    Case "PARJOURNAL", "PAREXALT4", "ICG", "MICROSOFTRMS", "IPOS"
                        sqlSelectTopNonSyncedHeaders = "Select Top " & TxInBatch.ToString & " " & sFieldList & " from tbSalesMain Where (RecordType ='H') and (Synced=0) Order By CONVERT(DATETIME,PollDate), TransactionVersion, SalesMainID "
                    Case "PIXELPOINT" ' PixelPoint uses Sybase 5 which does not support the TOP command
                        If TxInBatch = 1 Then
                            sqlSelectTopNonSyncedHeaders = "Select First " & sFieldList & " from tbSalesMain Where (RecordType ='H') and (Synced=0) Order By SalesMainID "
                        Else
                            sqlSelectTopNonSyncedHeaders = "Select " & sFieldList & " from tbSalesMain Where (RecordType ='H') and (Synced=0) Order By SalesMainID "
                        End If
                    Case Else
                        sqlSelectTopNonSyncedHeaders = "Select Top " & TxInBatch.ToString & " " & sFieldList & " from tbSalesMain Where (RecordType ='H') and (Synced=0) Order By SalesMainID "
                End Select
            Case DataType.AttendanceData
                Select Case sPosType.ToUpper
                    Case "QUICKEN" ' Not supported on Quicken so return empty recordset
                        sqlSelectTopNonSyncedHeaders = "Select Top " & TxInBatch.ToString & " " & sFieldList & " from tbSalesMain Where (Sequence_Number=1) and (Sequence_Number=0) " ' Return empty recordset
                    Case "PIXELPOINT" ' PixelPoint uses Sybase 5 which does not support the TOP command
                        If TxInBatch = 1 Then
                            sqlSelectTopNonSyncedHeaders = "Select First " & sFieldList & " from tbSalesMain Where (RecordType ='T') and (Synced=0) Order By SalesMainID "
                        Else
                            sqlSelectTopNonSyncedHeaders = "Select " & sFieldList & " from tbSalesMain Where (RecordType ='T') and (Synced=0) Order By SalesMainID "
                        End If
                    Case Else
                        sqlSelectTopNonSyncedHeaders = "Select Top " & TxInBatch.ToString & " " & sFieldList & " from tbSalesMain Where (RecordType ='T') and (Synced=0) Order By SalesMainID "
                End Select
        End Select
    End Function

    Private Function sqlUpdateSyncToken(ByVal sPosType As String, ByVal HeaderRow As DataRow, Optional ByVal DataType As DataType = DataType.SalesData) As String
        Dim sQry As String = String.Empty
        Try
            Select Case DataType
                Case DataType.SalesData
                    sQry = "Update tbSalesMain set SyncToken ='" & Guid.NewGuid.ToString & "' where (TransactionId =" & HeaderRow.Item("TransactionID").ToString & ") And (SyncToken is Null) "
                    Select Case sPosType.Trim.ToUpper
                        Case "QUICKEN" ' Quicken resets transaction ID's every day
                            sQry &= " AND  (Date_Time=#" & Format(HeaderRow.Item("Date_Time"), "dd-MMM-yyyy HH:mm:ss") & "#) "

                        Case "ALOHA" ' Aloha resets transaction ID's every day
                            sQry &= " AND (CONVERT(DATETIME, PollDate)='" & Format(CDate(HeaderRow.Item("PollDate")), "dd-MMM-yyyy HH:mm:ss") & "') "
                            sQry &= " AND (TransactionVersion = " & HeaderRow.Item("TransactionVersion").ToString & ")"

                        Case "MICROS", "PIXELPOINT", "PCAMERICA", "SIGNATURE", "COMPRIS", "INTOUCH", "PAREXALT4", "SILVERWARE", "OCTANE" ' Micros checks that are reopened get assigned the same transaction number plus a new version
                            sQry &= " AND (TransactionVersion = " & HeaderRow.Item("TransactionVersion").ToString & ")"

                        Case "NEWPOS", "IPOS"
                            sQry &= String.Format(" AND (RegisterID = {0})", HeaderRow.Item("RegisterID"))
                            sQry &= String.Format(" AND (PollDate = '{0}')", HeaderRow.Item("PollDate"))
                            sQry &= String.Format(" AND (TransactionVersion = {0})", HeaderRow.Item("TransactionVersion"))

                        Case "MICROSOFTRMS"
                            sQry &= String.Format(" AND (PollDate = '{0}')", HeaderRow.Item("PollDate"))
                            sQry &= String.Format(" AND (TransactionVersion = {0})", HeaderRow.Item("TransactionVersion"))

                        Case "ICG"
                            sQry &= String.Format(" AND (TransactionVersion = {0}) ", IntegerValue(HeaderRow("TransactionVersion"), 0))
                            sQry &= String.Format(" AND (PollDate = '{0}') ", HeaderRow("PollDate").ToString)
                            sQry &= String.Format(" AND (SubTypeDescription = '{0}') ", HeaderRow("SubTypeDescription").ToString)

                        Case "PARJOURNAL", "PAN7700"
                            ' do not need to add extra fields

                        Case Else ' else handles any generic POS types
                            sQry &= String.Format(" AND (PollDate = '{0}')", HeaderRow.Item("PollDate"))
                            sQry &= String.Format(" AND (TransactionVersion = {0})", HeaderRow.Item("TransactionVersion"))
                    End Select
                    sQry &= " AND (RecordType IN (" & cSalesRecordTypes & "))"

                Case DataType.AttendanceData
                    sQry = "Update tbSalesMain set SyncToken ='" & Guid.NewGuid.ToString & "' where (SalesMainId =" & HeaderRow.Item("SalesMainID").ToString & ") And (SyncToken is Null) "
                    'Select Case sPosType.Trim.ToUpper
                    '    Case "QUICKEN" ' Quicken resets transaction ID's every day
                    '        sQry &= " AND  (Date_Time=#" & Format(HeaderRow.Item("Date_Time"), "dd-MMM-yyyy HH:mm:ss") & "#) "
                    '    Case "ALOHA" ' Aloha resets transaction ID's every day
                    '        sQry &= " AND  (CONVERT(DATETIME, PollDate)='" & Format(CDate(HeaderRow.Item("PollDate")), "dd-MMM-yyyy HH:mm:ss") & "') "
                    '    Case "MICROS" ' Micros checks that are reopened get assigned the same transaction number plus a new version
                    '        sQry &= " AND (TransactionVersion = " & HeaderRow.Item("TransactionVersion").ToString & ")"
                    'End Select
                    'sQry &= " AND (RecordType IN (" & cSalesRecordTypes & "))"
            End Select
        Catch ex As Exception
            sQry = ""
        End Try
        sqlUpdateSyncToken = sQry
    End Function

    Private Function sqlGetSyncToken(ByVal sPosType As String, ByVal HeaderRow As DataRow, Optional ByVal DataType As DataType = DataType.SalesData) As String
        Dim sQry As String = String.Empty
        Try
            Select Case DataType
                Case DataType.SalesData
                    sQry = "SELECT SyncToken FROM tbSalesMain WHERE (TransactionId =" & HeaderRow.Item("TransactionID").ToString & ") "
                    Select Case sPosType.Trim.ToUpper
                        Case "QUICKEN" ' Quicken resets transaction ID's every day
                            sQry &= " AND  (Date_Time=#" & Format(HeaderRow.Item("Date_Time"), "dd-MMM-yyyy HH:mm:ss") & "#) "

                        Case "ALOHA" ' Aloha resets transaction ID's every day
                            sQry &= " AND (CONVERT(DATETIME, PollDate)='" & Format(CDate(HeaderRow.Item("PollDate")), "dd-MMM-yyyy HH:mm:ss") & "') "
                            sQry &= " AND (TransactionVersion = " & HeaderRow.Item("TransactionVersion").ToString & ")"

                        Case "MICROS", "PIXELPOINT", "PCAMERICA", "SIGNATURE", "COMPRIS", "PAREXALT4", "SILVERWARE" ' Micros checks that are reopened get assigned the same transaction number plus a new version
                            sQry &= " AND (TransactionVersion = " & HeaderRow.Item("TransactionVersion").ToString & ")"

                        Case "NEWPOS", "IPOS"
                            sQry &= String.Format(" AND (RegisterID = {0})", HeaderRow.Item("RegisterID"))
                            sQry &= String.Format(" AND (PollDate = '{0}')", HeaderRow.Item("PollDate"))
                            sQry &= String.Format(" AND (TransactionVersion = {0})", HeaderRow.Item("TransactionVersion"))

                        Case "MICROSOFTRMS"
                            sQry &= String.Format(" AND (PollDate = '{0}')", HeaderRow.Item("PollDate"))
                            sQry &= String.Format(" AND (TransactionVersion = {0})", HeaderRow.Item("TransactionVersion"))

                        Case "ICG"
                            sQry &= String.Format(" AND (TransactionVersion = {0}) ", IntegerValue(HeaderRow("TransactionVersion"), 0))
                            sQry &= String.Format(" AND (PollDate = '{0}') ", HeaderRow("PollDate").ToString)
                            sQry &= String.Format(" AND (SubTypeDescription = '{0}') ", HeaderRow("SubTypeDescription").ToString)

                        Case "PARJOURNAL", "PAN7700"
                            ' do not need to add extra fields

                        Case Else ' else handles any generic POS types
                            sQry &= String.Format(" AND (PollDate = '{0}')", HeaderRow.Item("PollDate"))
                            sQry &= String.Format(" AND (TransactionVersion = {0})", HeaderRow.Item("TransactionVersion"))
                    End Select

                    sQry &= " AND (RecordType IN (" & cSalesRecordTypes & "))"
                Case DataType.AttendanceData
                    sQry = "SELECT SyncToken FROM tbSalesMain WHERE (SalesMainId =" & HeaderRow.Item("SalesMainID").ToString & ") "
            End Select
        Catch ex As Exception
            sQry = ""
        End Try
        sqlGetSyncToken = sQry
    End Function

    Private Function sqlSelectAllRecordsForTransaction(ByVal sPosType As String, ByVal UseODBC As Boolean, ByVal HeaderRow As DataRow, ByVal sFieldList As String) As String
        ' Sales Data only
        Dim sQry As String
        Dim sTransactionId As String

        Try
            sTransactionId = HeaderRow.Item("TransactionID").ToString
            Select Case sPosType.Trim.ToUpper
                Case "QUICKEN"
                    sQry = "Select * from tbSalesMain Where (TransactionID=" & sTransactionId & ") AND (Synced = 0) "
                    sQry &= " AND  (Date_Time=#" & Format(HeaderRow.Item("Date_Time"), "dd-MMM-yyyy HH:mm:ss") & "#) "
                    ' sQry &= " AND (RecordType IN (" & cSalesRecordTypes & ")) " ??? NOT SURE FOR QUICKEN
                    sQry &= "Order by Sequence_Number"

                Case "ALOHA" ' Aloha resets transaction ID's each day
                    sQry = "Select 1 as myOrder, " & sFieldList & " from tbSalesMain Where (TransactionID=" & sTransactionId & ") AND (TransactionVersion=" & HeaderRow.Item("TransactionVersion").ToString & ") and (RecordType = 'H') AND (Synced = 0) "
                    sQry &= "AND (CONVERT(DATETIME, PollDate)='" & Format(CDate(HeaderRow.Item("PollDate")), "dd-MMM-yyyy HH:mm:ss") & "') "
                    sQry &= "UNION Select 2 as myOrder, " & sFieldList & " from tbSalesMain Where (TransactionID=" & sTransactionId & ") AND (TransactionVersion=" & HeaderRow.Item("TransactionVersion").ToString & ") and (RecordType <> 'H') AND (Synced = 0) "
                    sQry &= "AND (CONVERT(DATETIME, PollDate)='" & Format(CDate(HeaderRow.Item("PollDate")), "dd-MMM-yyyy HH:mm:ss") & "') "
                    sQry &= " AND (RecordType IN (" & cSalesRecordTypes & ")) "
                    If UseODBC Then
                        sQry &= "Order by 1, ParentID, SalesMainID"
                    Else
                        sQry &= "Order by myOrder, ParentID, SalesMainID"
                    End If

                Case "MICROS", "PIXELPOINT", "PCAMERICA", "SIGNATURE", "COMPRIS", "INTOUCH", "PAREXALT4", "SILVERWARE", "OCTANE" ' Micros checks that are reopened get assigned the same transaction number plus a new version
                    sQry = "Select 1 as myOrder, " & sFieldList & " from tbSalesMain Where (TransactionId =" & sTransactionId & ") AND (TransactionVersion = " & HeaderRow.Item("TransactionVersion").ToString & ") AND (RecordType = 'H') AND (Synced = 0) "
                    sQry &= "UNION Select 2 as myOrder, " & sFieldList & " from tbSalesMain Where (TransactionId =" & sTransactionId & ") AND (TransactionVersion = " & HeaderRow.Item("TransactionVersion").ToString & ") AND (RecordType <> 'H') AND (Synced = 0) "
                    sQry &= " AND (RecordType IN (" & cSalesRecordTypes & ")) "
                    If UseODBC Then
                        ' For Micros RES 3.2, the version of Sybase does not support ORDER BY columnName when a UNION is used.  This is OK in RES 4.1.
                        sQry &= "Order by 1, 22, 2" ' ORDER BY myOrder, ParentId, SalesMainID (Note: Add 1 to the column number in sFieldList as myOrder is inserted as column 1)
                    Else
                        sQry &= "Order by myOrder, ParentId, SalesMainID"
                    End If

                Case "NEWPOS", "IPOS"
                    Dim sql As New StringBuilder(1024)
                    Dim pollDate As String = HeaderRow.Item("PollDate").ToString
                    Dim registerId As Integer = IntegerValue(HeaderRow.Item("RegisterID"), 0)
                    Dim transactionVersion As Integer = IntegerValue(HeaderRow.Item("TransactionVersion"), 0)

                    ' create the query string
                    sql.AppendFormat("select (case recordtype when 'H' then 1 when 'I' then 2 when 'IM' then 3 when 'F' then 4 else 5 end) as myOrder, {0} {1}", sFieldList, vbCrLf)
                    sql.AppendFormat("FROM tbSalesMain WHERE (TransactionID = {0} AND PollDate = '{1}' AND RegisterID = {2} AND TransactionVersion = {3}) {4}", sTransactionId, pollDate, registerId, transactionVersion, vbCrLf)
                    sql.AppendFormat("AND (Synced = 0) {0}", vbCrLf)
                    sql.AppendFormat("ORDER BY myOrder, ParentId, SalesMainID {0}", vbCrLf)
                    sQry = sql.ToString

                Case "MICROSOFTRMS"
                    Dim sql As New StringBuilder(1024)
                    Dim pollDate As String = HeaderRow.Item("PollDate").ToString
                    Dim transactionVersion As Integer = IntegerValue(HeaderRow.Item("TransactionVersion"), 0)

                    ' create the query string
                    sql.AppendFormat("SELECT 1 AS myOrder, {0} {1}", sFieldList, vbCrLf)
                    sql.AppendFormat("FROM tbSalesMain WHERE (TransactionID = {0} AND PollDate = '{1}' AND TransactionVersion = '{2}') {3}", sTransactionId, pollDate, transactionVersion, vbCrLf)
                    sql.AppendFormat("AND (RecordType = 'H') AND (Synced = 0) {0}", vbCrLf)
                    sql.AppendFormat("UNION Select 2 as myOrder, {0} {1}", sFieldList, vbCrLf)
                    sql.AppendFormat("FROM tbSalesMain WHERE (TransactionID = {0} AND PollDate = '{1}' AND TransactionVersion = '{2}') {3}", sTransactionId, pollDate, transactionVersion, vbCrLf)
                    sql.AppendFormat("AND (RecordType <> 'H') AND (Synced = 0) {0}", vbCrLf)
                    sql.AppendFormat("ORDER BY myOrder, ParentId, SalesMainID {0}", vbCrLf)
                    sQry = sql.ToString

                Case "ICG"
                    Dim sql As New StringBuilder(1024)
                    Dim pollDate As String = HeaderRow.Item("PollDate").ToString
                    Dim posRegisterID As String = HeaderRow.Item("SubTypeDescription").ToString
                    Dim transactionVersion As Integer = IntegerValue(HeaderRow.Item("TransactionVersion"), 0)

                    ' create the query string, since ICG has data for multiple entities the 
                    sql.AppendFormat("SELECT 1 AS myOrder, {0} {1}", sFieldList, vbCrLf)
                    sql.AppendFormat("FROM tbSalesMain WHERE (TransactionID = {0} AND TransactionVersion = {1} AND PollDate = '{2}' AND SubTypeDescription = '{3}') {4}", sTransactionId, transactionVersion, pollDate, posRegisterID, vbCrLf)
                    sql.AppendFormat("AND (RecordType = 'H') AND (Synced = 0) {0}", vbCrLf)
                    sql.AppendFormat("UNION Select 2 as myOrder, {0} {1}", sFieldList, vbCrLf)
                    sql.AppendFormat("FROM tbSalesMain WHERE (TransactionID = {0} AND TransactionVersion = {1} AND PollDate = '{2}' AND SubTypeDescription = '{3}') {4}", sTransactionId, transactionVersion, pollDate, posRegisterID, vbCrLf)
                    sql.AppendFormat("AND (RecordType <> 'H') AND (Synced = 0) {0}", vbCrLf)
                    sql.AppendFormat("ORDER BY myOrder {0}", vbCrLf)
                    sQry = sql.ToString

                Case "PARJOURNAL", "PAN7700"
                    Dim sql As New StringBuilder(1024)
                    ' create the query string (specifically doesn't use TransactionVersion column)
                    sql.AppendFormat("SELECT 1 AS myOrder, {0} {1}", sFieldList, vbCrLf)
                    sql.AppendFormat("FROM tbSalesMain WHERE (TransactionID = {0}) {1}", sTransactionId, vbCrLf)
                    sql.AppendFormat("AND (RecordType = 'H') AND (Synced = 0) {0}", vbCrLf)
                    sql.AppendFormat("UNION Select 2 as myOrder, {0} {1}", sFieldList, vbCrLf)
                    sql.AppendFormat("FROM tbSalesMain WHERE (TransactionID = {0}) {1}", sTransactionId, vbCrLf)
                    sql.AppendFormat("AND (RecordType <> 'H') AND (Synced = 0) {0}", vbCrLf)

                    ' define order method depending on use of ODBC or not
                    If UseODBC Then
                        sql.AppendFormat("ORDER BY 1, ParentId, SalesMainID {0}", vbCrLf)
                    Else
                        sql.AppendFormat("ORDER BY myOrder, ParentId, SalesMainID {0}", vbCrLf)
                    End If
                    sQry = sql.ToString

                Case Else
                    Dim sql As New StringBuilder(1024)
                    Dim pollDate As String = HeaderRow.Item("PollDate").ToString
                    Dim transactionVersion As Integer = IntegerValue(HeaderRow.Item("TransactionVersion"), 0)

                    ' create the query string
                    sql.AppendFormat("SELECT 1 AS myOrder, {0} {1}", sFieldList, vbCrLf)
                    sql.AppendFormat("FROM tbSalesMain WHERE (TransactionID = {0} AND PollDate = '{1}' AND TransactionVersion = '{2}') {3}", sTransactionId, pollDate, transactionVersion, vbCrLf)
                    sql.AppendFormat("AND (RecordType = 'H') AND (Synced = 0) {0}", vbCrLf)
                    sql.AppendFormat("UNION Select 2 as myOrder, {0} {1}", sFieldList, vbCrLf)
                    sql.AppendFormat("FROM tbSalesMain WHERE (TransactionID = {0} AND PollDate = '{1}' AND TransactionVersion = '{2}') {3}", sTransactionId, pollDate, transactionVersion, vbCrLf)
                    sql.AppendFormat("AND (RecordType <> 'H') AND (Synced = 0) {0}", vbCrLf)

                    ' define order method depending on use of ODBC or not
                    If UseODBC Then
                        sql.AppendFormat("ORDER BY 1, ParentId, SalesMainID {0}", vbCrLf)
                    Else
                        sql.AppendFormat("ORDER BY myOrder, ParentId, SalesMainID {0}", vbCrLf)
                    End If
                    sQry = sql.ToString
            End Select
        Catch ex As Exception
            sQry = ""
        End Try
        sqlSelectAllRecordsForTransaction = sQry
    End Function

    Private Function MarkDuplicateTransactionIdsAsErrors(ByVal sPosType As String, ByVal UseODBC As Boolean)
        Dim sSql As String = String.Empty
        Dim sError As String = String.Empty

        Try
            Select Case sPosType.ToUpper.Trim
                Case "CRATOS"
                    ' Cratos only adds the PollDate to Header records
                    ' First update the PollDate field to match the previous header record
                    ' This will also assist with archiving old data
                    sSql = "SET NOCOUNT ON "
                    sSql &= "WHILE EXISTS("
                    sSql &= "  SELECT A.SalesMainId "
                    sSql &= "  FROM  tbSalesMain A JOIN tbSalesMain B "
                    sSql &= "  ON A.SalesMainId = B.SalesMainId + 1 "
                    sSql &= "  WHERE A.TransactionId = B.TransactionId "
                    sSql &= "  AND A.PollDate IS NULL "
                    sSql &= "  AND B.PollDate IS NOT NULL) "
                    sSql &= "    UPDATE A SET PollDate = B.PollDate  "
                    sSql &= "    FROM tbSalesMain A JOIN tbSalesMain B "
                    sSql &= "    ON A.SalesMainId = B.SalesMainId + 1 "
                    sSql &= "    WHERE A.TransactionId = B.TransactionId "
                    sSql &= "    AND A.PollDate IS NULL "
                    sSql &= "    AND B.PollDate IS NOT NULL "

                    sSql &= "Update tbSalesMain "
                    sSql &= "SET Synced=11 "  ' Flag as Error - TransactionId appears twice in one day
                    sSql &= "WHERE SalesMainId IN ( "
                    sSql &= "  Select SalesMainId "
                    sSql &= "  FROM tbSalesMain A "
                    sSql &= "  WHERE SalesMainId < ( "
                    sSql &= "    SELECT MAX(SalesMainId) As SalesMainId "
                    sSql &= "    FROM tbsalesmain B "
                    sSql &= "    Where Synced=0 AND RecordType = 'H' "
                    sSql &= "    AND "
                    sSql &= "    (Convert(VarChar(15), DatePart(year, Convert(datetime, A.PollDate))) + "
                    sSql &= "     DateName(month, Convert(datetime, A.PollDate)) + "
                    sSql &= "     Convert(VarChar(15), DatePart(Day, Convert(datetime, A.PollDate)))) = "
                    sSql &= "    (Convert(VarChar(15), DatePart(year, Convert(datetime, B.PollDate))) + "
                    sSql &= "     DateName(month, Convert(datetime, B.PollDate)) + "
                    sSql &= "     Convert(VarChar(15), DatePart(Day, Convert(datetime, B.PollDate)))) "
                    sSql &= "    AND A.TransactionId = B.TransactionId "
                    sSql &= "    GROUP BY TransactionId, "
                    sSql &= "    Convert(VarChar(15), DatePart(year, Convert(datetime, PollDate))) + "
                    sSql &= "    DateName(month, Convert(datetime, PollDate)) + "
                    sSql &= "    Convert(VarChar(15), DatePart(Day, Convert(datetime, PollDate))) "
                    sSql &= "    HAVING Count(TransactionId) > 1 "
                    sSql &= "  ) "
                    sSql &= ") "
            End Select
            If sSql <> String.Empty Then
                If Not MMSGeneric_Execute(UseODBC, sSql, sError) Then
                    LogEvent("Error clearing duplicate TransactionIds" & vbCrLf & "Error: " & sError & vbCrLf & "SQL: " & sSql, EventLogEntryType.Warning)
                End If
            End If
        Catch ex As Exception
            LogEvent("Error clearing duplicate TransactionIds" & vbCrLf & "Exception: " & ex.Message & vbCrLf & "SQL: " & sSql, EventLogEntryType.Warning)
        End Try
    End Function

    Private Function sqlUpdateSyncedFlag(ByVal sPosType As String, ByVal ResultRow As DataRow, Optional ByVal DataType As DataType = DataType.SalesData)
        Dim sQry As String = String.Empty
        Try
            Select Case DataType
                Case DataType.SalesData
                    sQry = "Update tbSalesMain set Synced = " & ResultRow.Item("Result") & " where TransactionId =" & ResultRow.Item("TransactionID").ToString & " AND Synced=0"
                    Select Case sPosType.Trim.ToUpper
                        Case "QUICKEN"
                            sQry &= " AND  (Date_Time=#" & Format(ResultRow.Item("Date_Time"), "dd-MMM-yyyy HH:mm:ss") & "#) "

                        Case "ALOHA" ' Aloha resets transaction ID's every day
                            sQry &= " AND (CONVERT(DATETIME, PollDate)='" & Format(CDate(ResultRow.Item("PollDate")), "dd-MMM-yyyy HH:mm:ss") & "') "
                            sQry &= " AND (TransactionVersion = " & ResultRow.Item("TransactionVersion").ToString & ")"

                        Case "NEWPOS", "IPOS"
                            sQry &= String.Format(" AND (PollDate = '{0}')", ResultRow.Item("PollDate"))
                            sQry &= String.Format(" AND (RegisterID = {0})", ResultRow.Item("RegisterID"))
                            sQry &= String.Format(" AND (TransactionVersion = {0})", ResultRow.Item("TransactionVersion"))

                        Case "MICROSOFTRMS"
                            sQry &= String.Format(" AND (PollDate = '{0}')", ResultRow.Item("PollDate"))
                            sQry &= String.Format(" AND (TransactionVersion = {0})", ResultRow.Item("TransactionVersion"))

                        Case "MICROS", "PIXELPOINT", "PCAMERICA", "SIGNATURE", "COMPRIS", "INTOUCH", "PAREXALT4", "SILVERWARE", "OCTANE" ' Micros checks that are reopened get assigned the same transaction number plus a new version
                            sQry &= " AND (TransactionVersion = " & ResultRow.Item("TransactionVersion").ToString & ")"

                        Case "ICG"
                            ' match the date, and the subtypedescription (this holds an alpha-numeric register id value)
                            sQry &= String.Format(" AND TransactionVersion = {0} AND PollDate = '{1}' AND SubTypeDescription = '{2}'", ResultRow("TransactionVersion"), ResultRow("PollDate"), ResultRow("PosRegisterID"))

                        Case "PARJOURNAL", "PAN7700"
                            ' do not need to add extra fields

                        Case Else
                            ' handle all other POS types using PollDate and TransactionVersion as the required columns
                            sQry &= String.Format(" AND (PollDate = '{0}')", ResultRow.Item("PollDate"))
                            sQry &= String.Format(" AND (TransactionVersion = {0})", ResultRow.Item("TransactionVersion"))
                    End Select
                    sQry &= " AND (RecordType IN (" & cSalesRecordTypes & ")) "

                Case DataType.AttendanceData
                    sQry = "Update tbSalesMain set Synced = " & ResultRow.Item("Result") & " where SalesMainId =" & ResultRow.Item("SalesMainID").ToString & " AND Synced=0"
                    'Select Case sPosType.Trim.ToUpper
                    '    Case "QUICKEN"
                    '        sQry &= " AND  (Date_Time=#" & Format(ResultRow.Item("Date_Time"), "dd-MMM-yyyy HH:mm:ss") & "#) "
                    '    Case "ALOHA" ' Aloha resets transaction ID's every day
                    '        sQry &= " AND  (CONVERT(DATETIME, PollDate)='" & Format(CDate(ResultRow.Item("PollDate")), "dd-MMM-yyyy HH:mm:ss") & "') "
                    '    Case "MICROS" ' Micros checks that are reopened get assigned the same transaction number plus a new version
                    '        sQry &= " AND (TransactionVersion = " & ResultRow.Item("TransactionVersion").ToString & ")"
                    'End Select
            End Select
        Catch ex As Exception
            LogEvent(EventLogEntryType.Error, "sqlUpdateSyncedFlag error.  Error: {1}", sPosType, ex)
            sQry = ""
        End Try
        sqlUpdateSyncedFlag = sQry
    End Function

    Private Function isDataToSend(ByRef SalesData As Boolean, ByRef AttendanceData As Boolean) As Boolean
        Dim Use_ODBC As Boolean = LiveLinkConfiguration.Use_ODBC_DB_Connect
        Dim sPOSType As String = LiveLinkConfiguration.POSType.Trim
        Dim HeaderDataSet As DataSet
        Dim sError As String = String.Empty
        Dim sSql As String

        SalesData = False
        AttendanceData = False

        isDataToSend = False

        Select Case sPOSType.ToUpper
            Case Is = "MICROS", "PIXELPOINT"
                bNoMSA_Flag = True
            Case Else
                bNoMSA_Flag = False
        End Select
        Try
            ' First check for Sales Data
            sSql = sqlSelectTopNonSyncedHeaders(sPOSType, 1, DataType.SalesData)
            If MMSGeneric_ListAll(Use_ODBC, sSql, "SalesData", HeaderDataSet, sError, bNoMSA_Flag) Then
                If HeaderDataSet.Tables("SalesData").Rows.Count > 0 Then
                    SalesData = True
                End If

            Else
                LogEvent("Error checking for unsynced sales data " & vbCrLf & "Error: " & sError & vbCrLf & "SQL: " & sSql, EventLogEntryType.Error)
            End If

            ' Next check for Attendance Data
            sSql = sqlSelectTopNonSyncedHeaders(sPOSType, 1, DataType.AttendanceData)
            If MMSGeneric_ListAll(Use_ODBC, sSql, "AttendanceData", HeaderDataSet, sError, bNoMSA_Flag) Then
                If HeaderDataSet.Tables("AttendanceData").Rows.Count > 0 Then
                    AttendanceData = True
                End If
            Else
                LogEvent("Error checking for unsynced attendance data " & vbCrLf & "Error: " & sError & vbCrLf & "SQL: " & sSql, EventLogEntryType.Error)
            End If
            isDataToSend = SalesData Or AttendanceData
        Catch ex As Exception
            LogEvent("Error checking for unsynced data (isDataToSend)" & vbCrLf & "Error: " & ex.Message, EventLogEntryType.Error)
        End Try
    End Function

    Private Function CreateTableName(ByVal sPosType As String, ByVal HeaderRow As DataRow) As String
        Select Case sPosType.Trim.ToUpper
            Case "QUICKEN"
                Return "SalesData-" & Format(CDate(HeaderRow.Item("Date_Time")), "yyyyMMddHHmmss") & "-" & HeaderRow.Item("TransactionID").ToString
            Case "MICROS", "PIXELPOINT", "PCAMERICA", "SIGNATURE", "COMPRIS", "INTOUCH", "PAREXALT4", "SILVERWARE", "OCTANE", "ALOHA"
                Return "SalesData-" & Format(CDate(HeaderRow.Item("PollDate")), "yyyyMMdd") & "-" & HeaderRow.Item("TransactionID").ToString & "." & HeaderRow.Item("TransactionVersion").ToString
            Case "NEWPOS", "IPOS"
                ' return the table name "SalesData-PollDate-RegisterID-TransactionID-TransactionVersion". e.g. "SalesData-20090115123500-113-1234-0"
                Return String.Format("SalesData-{0:yyyyMMddHHmmss}-{1}-{2}-{3}", CDate(HeaderRow("PollDate")), HeaderRow("RegisterID"), HeaderRow("TransactionID"), HeaderRow("TransactionVersion"))
            Case "MICROSOFTRMS"
                ' return the table name "SalesData-PollDate-TransactionID-TransactionVersion". e.g. "SalesData-20090115123500-1234-0"
                Return String.Format("SalesData-{0:yyyyMMddHHmmss}-{1}-{2}", CDate(HeaderRow("PollDate")), HeaderRow("TransactionID"), HeaderRow("TransactionVersion"))
            Case "ICG"
                ' return the table name "SalesData-PollDate-PosRegisterID-TransactionID-TransactionVersion". e.g. "SalesData-20090115123500-01Z-1234-0"
                Return String.Format("SalesData-{0:yyyyMMddHHmmss}-{1}-{2}-{3}", CDate(HeaderRow("PollDate")), HeaderRow("SubTypeDescription"), HeaderRow("TransactionID"), HeaderRow("TransactionVersion"))
            Case "PARJOURNAL", "PAN7700"
                Return "SalesData-" & Format(CDate(HeaderRow.Item("PollDate")), "yyyyMMdd") & "-" & HeaderRow.Item("TransactionID").ToString

            Case Else ' default POS type handling
                ' return the table name "SalesData-PollDate-TransactionID-TransactionVersion". e.g. "SalesData-20090115123500-1234-0"
                Return String.Format("SalesData-{0:yyyyMMddHHmmss}-{1}-{2}", CDate(HeaderRow("PollDate")), HeaderRow("TransactionID"), HeaderRow("TransactionVersion"))
        End Select
    End Function

#End Region

#Region " Pre-Send Processing"

    ' check to see if any pre-processing should be done on data before sending it
    Private Sub PreSendProcess(ByRef ds As DataSet)
        ' Check what type of processing to do on data, prior to sending it
        Select Case MyPreSendProcess.ToUpper
            Case "PLUMAP"
                ' at the moment, only PLUMAP is a presend process
                PLUMap(ds)
        End Select
    End Sub

#Region " PLU Mapping functions"

    Private Sub PLUMap(ByRef ds As DataSet)
        Try
            Dim TableIndex As Integer
            Dim RowIndex As Integer

            Dim ModifierPLU As Integer
            Dim ServiceType As Integer
            Dim ReplacementPLU As Integer

            Dim PluNode As XmlNode
            Dim PluMapNode As XmlNode

            ' If xml document hasn't been read, load it
            If MyPLUMapXmlDoc Is Nothing Then
                Try
                    MyPLUMapXmlDoc = New XmlDocument
                    MyPLUMapXmlDoc.Load(Path.Combine(Application.StartupPath, MyPLUMapFile))
                Catch ex As Exception
                    LogEvent("Error: Failed to read PLU Mappings from file:[" & Path.Combine(Application.StartupPath, MyPLUMapFile) & "] - Exception: " & ex.Message, EventLogEntryType.Error)
                    Return
                End Try
            End If

            ' loop through each transaction
            For TableIndex = 0 To (ds.Tables.Count - 1)
                With ds.Tables(TableIndex)
                    ' loop through each item in the transaction
                    For RowIndex = 0 To (.Rows.Count - 1)
                        ' only check Items
                        If EvalNull(.Rows(RowIndex).Item("RecordType")) = "I" Then
                            Try
                                ' check if there is a node with a matching plu value
                                PluNode = MyPLUMapXmlDoc.SelectSingleNode("plumap/plu[@value=""" & EvalNull(.Rows(RowIndex).Item("PLUCodeId")) & """]")
                                If Not PluNode Is Nothing Then
                                    ' if plu node is not nothing, then there are mappings available for this plu
                                    For Each PluMapNode In PluNode.ChildNodes
                                        ' allocate the childnodes modifier plu, servicetype and replacement plu...
                                        ' if they are not set, then they will default to -1
                                        ModifierPLU = GetNodeAttribute(PluMapNode, "modifier")
                                        ServiceType = GetNodeAttribute(PluMapNode, "servicetype")
                                        ReplacementPLU = GetNodeAttribute(PluMapNode, "replacement")

                                        ' replacement plu must be set, otherwise there is no point comparing
                                        If ReplacementPLU = -1 Then
                                            ' exit this for loop, continue onto next item
                                            LogEvent("Warning [PLUMap]: replacement PLU is not set for PLU=[" & EvalNull(.Rows(RowIndex).Item("PLUCodeId")) & "]", EventLogEntryType.Warning)
                                            Exit For
                                        End If

                                        ' check to see if there is a modifier item associated with it
                                        If (RowIndex + 1) < .Rows.Count Then
                                            ' check if the plu matches, (plu < 0, is classed as not required)
                                            If ModifierPLU < 0 Or _
                                                (EvalNull(.Rows(RowIndex + 1).Item("RecordType")) = "I" AndAlso _
                                                EvalNull(.Rows(RowIndex + 1).Item("PLUCodeId")) = ModifierPLU) Then

                                                ' check if the service type matches, (servicetype < 0, is classed as not set)
                                                If ServiceType < 0 Or _
                                                    ((Not ds.Tables(TableIndex).Rows(0).Item("Flag") Is DBNull.Value) AndAlso _
                                                    EvalNull(ds.Tables(TableIndex).Rows(0).Item("RecordType")) = "H" AndAlso _
                                                    ds.Tables(TableIndex).Rows(0).Item("Flag") = ServiceType) Then

                                                    ' MATCH FOUND -- Replace the PLU, then exit the for loop
                                                    .Rows(RowIndex).Item("PLUCodeId") = ReplacementPLU
                                                    Exit For
                                                End If
                                            End If
                                        Else
                                            ' check if the ModPLU is not required (plu < 0, is classed as not required)
                                            ' and that the service type is not required or matches. Service type is stored in the Flag field
                                            If ModifierPLU < 0 AndAlso _
                                                (ServiceType < 0 Or _
                                                (Not ds.Tables(TableIndex).Rows(0).Item("Flag") Is DBNull.Value) AndAlso _
                                                EvalNull(ds.Tables(TableIndex).Rows(0).Item("RecordType")) = "H" AndAlso _
                                                ds.Tables(TableIndex).Rows(0).Item("Flag") = ServiceType) Then

                                                ' MATCH FOUND -- Replace the PLU, then exit the for loop
                                                .Rows(RowIndex).Item("PLUCodeId") = ReplacementPLU
                                                Exit For
                                            End If
                                        End If
                                    Next
                                End If

                            Catch ex As Exception
                                LogEvent("Error: Failed to check for PLU mappings, Item PLU=[" & EvalNull(.Rows(RowIndex).Item("PLUCodeId")) & "] - Exception: " & ex.Message, EventLogEntryType.Error)
                            End Try
                        End If
                    Next
                End With
            Next
        Catch ex As Exception
            LogEvent("Error: Failed to check for PLU mappings - Exception: " & ex.Message, EventLogEntryType.Error)
        End Try
    End Sub

    Private Function GetNodeAttribute(ByVal Node As XmlNode, ByVal NodeName As String) As Integer
        GetNodeAttribute = -1
        Try
            Dim AttrNode As XmlNode
            AttrNode = Node.SelectSingleNode(NodeName)
            If Not AttrNode Is Nothing Then
                If AttrNode.Attributes.Count > 0 Then
                    GetNodeAttribute = AttrNode.Attributes(0).Value
                End If
            End If
        Catch ex As Exception
        End Try
    End Function

#End Region

#End Region

#Region "  Send & Proxy Functions "

    Private Sub LoadProxy(ByRef WebService As LiveLinkWebService)
        WebService.Proxy = myProxy()
    End Sub

    Private Function myProxy() As IWebProxy
        ' Checks the config file for proxy settings and configures the web service appropriately
        Try
            Dim myWebServiceProxy As String = LiveLinkConfiguration.WebService_Proxy
            Dim myWebServiceProxyAuth As String = LiveLinkConfiguration.WebService_Proxy_Auth
            Dim myWebServiceProxyUser As String = LiveLinkConfiguration.WebService_Proxy_User
            Dim myWebServiceProxyPassword As String = DecryptString(LiveLinkConfiguration.WebService_Proxy_Password, LiveLinkConfiguration.ClientCode, LiveLinkConfiguration.SecurityCode)
            Dim myWebServiceProxyDomain As String = LiveLinkConfiguration.WebService_Proxy_Domain
            Dim myWebServiceDestinationUrl As String = LiveLinkConfiguration.WebService_URL

            Return HttpDownload.Proxy(myWebServiceProxy, myWebServiceProxyAuth, myWebServiceProxyUser, myWebServiceProxyPassword, myWebServiceProxyDomain, myWebServiceDestinationUrl)
        Catch ex As Exception
            LogEvent("Proxy error" & vbCrLf & "Exception: " & ex.Message, EventLogEntryType.Warning)
            Return Nothing
        End Try
    End Function

    Private Sub SendData()
        Dim dataType As SendDataHelper.LiveLinkDataType
        ' check if summary data is enabled
        If (String.IsNullOrEmpty(LiveLinkConfiguration.SendDataType)) Then
            Exit Sub
        End If

        Try
            ' try to parse the SendDataType value into the LiveLinkDataType enum
            dataType = DirectCast([Enum].Parse(GetType(SendDataHelper.LiveLinkDataType), LiveLinkConfiguration.SendDataType, True), SendDataHelper.LiveLinkDataType)
        Catch ex As Exception
            LogEvent(EventLogEntryType.Warning, "Warning: Failed to parse send data type from value ({0}). Exception: {1}", LiveLinkConfiguration.SendDataType, ex.ToString)
            Exit Sub
        End Try

        Try
            UserAccess(False)
            ' check ok to send
            If Not Is_OK_To_Send() Then
                Exit Sub
            End If

            ' check there is data to send
            Dim headerRecords As DataTable = SendDataHelper.GetHeaders(dataType, myFlowTxInBatch)
            If headerRecords Is Nothing OrElse headerRecords.Rows.Count <= 0 Then
                SetStatus("Waiting...", String.Format("No data ({0}) to send.", dataType))
                Exit Sub
            End If
            ' toggle visible buttons during send
            ToggleSendButtons(False)
            CancelFlag = False

            ' get a new lease
            GetLease()
            ' check lease status
            If (String.IsNullOrEmpty(mySessionKey)) Then
                If (myConditionalSessionKey <> "") And (mySessionKey = String.Empty) Then
                    SetStatus("", "Site not yet activated.")
                End If
                ' exit send sub
                Exit Sub
            End If

            ' log information on send process
            If Not UseCompression Then
                SetStatus("", "Warning: Compression is disabled")
            End If

            Dim posType As String = LiveLinkConfiguration.POSType
            Dim entityID As Integer = GetEntityId()
            Dim response As eRetailer.LiveLinkResponse
            Dim posData As New LiveLinkWebService
            Dim transferredCount As Integer = 0

            'Set the webservice reference
            posData.Url = myWebService
            LoadProxy(posData)

            'Loop through all transactions and synchronise 1 at a time. Can send more than transaction in a batch
            SetStatus("Started", String.Format("Preparing ({0}) batches - {1} batches of {2} transactions.", dataType, myFlowIterations, myFlowTxInBatch))

            For batchNo As Integer = 1 To myFlowIterations
                Dim transferData As New DataSet
                Dim transferRecords As Integer = 0
                Dim dataList As New List(Of ILiveLinkData)

                ' Get first batch of header records
                headerRecords = SendDataHelper.GetHeaders(dataType, myFlowTxInBatch)
                If headerRecords Is Nothing OrElse headerRecords.Rows.Count <= 0 Then
                    SetStatus("Waiting...", String.Format("No data ({0}) to send for batch number {1}", dataType, batchNo))
                    Exit For
                End If

                ' process each data header retrieved
                For Each header As DataRow In headerRecords.Rows
                    Dim dataRecord As ILiveLinkData
                    dataRecord = SendDataHelper.Create(dataType, header)

                    ' do any required application events
                    Application.DoEvents()

                    ' check if table already exists in dataset, this prevents duplicate table error which blocks sending
                    If transferData.Tables.Contains(dataRecord.TableName) Then
                        LogEvent(EventLogEntryType.Error, "Error [SendData]: Duplicate ({0}) table created ({1}). Marking as error and continueing", dataType, dataRecord.TableName)
                        SetStatus("Error", String.Format("Duplicate ({0}) table ({1}). Skipping transaction.", dataType, dataRecord.TableName))
                        dataRecord.UpdateSynced(SendDataHelper.Synced.DuplicateTransaction)
                        Continue For
                    End If

                    ' clone the data table into the dataset to transfer
                    transferData.Tables.Add(dataRecord.Data.Clone())
                    transferData.Merge(dataRecord.Data)
                    transferRecords += dataRecord.Data.Rows.Count

                    ' store the parsed data record in a list
                    dataList.Add(dataRecord)
                Next

                ' check if there is data to send
                If transferData.Tables.Count > 0 Then
                    ' log current progress
                    SetStatus("Sending data...", String.Format("Batch {0}. Sending {1} transactions ({2} records).", batchNo, transferData.Tables.Count, transferRecords))

                    ' Check for a valid lease. Get if not valid
                    GetLease()
                    Application.DoEvents()


                    ' get the data format & result data format
                    Dim dataFormat As String = dataList(0).DataFormat
                    Dim resultDataFormat As String = dataList(0).ResultDataFormat

                    ' send data to server
                    If UseCompression Then
                        response = posData.Send_POSData2(MyIdentity, mySessionKey, dataFormat, CompressDataSet(transferData), entityID, posType)
                    Else
                        response = posData.Send_POSData2(MyIdentity, mySessionKey, dataFormat, transferData, entityID, posType)
                    End If


                    ' process response
                    With response
                        ' check response code
                        If .ResponseCode <> eRetailer.LiveLinkResponseCode.Success Then
                            ' Response code returned was not Success
                            ProcessResponse(response, True)
                            Exit For ' Do not process any more
                        End If

                        ' set the message waiting flag
                        MessageWaiting = response.ResponseMessageWaiting

                        ' check valid response data
                        If .ResponseData Is Nothing Then
                            ' This should never occur - empty dataset of results
                            SetStatus("Error ...", "No results were returned")
                            BackOffCommunications(False)
                            Exit For ' Do not process any more
                        End If

                        ' check valid transaction response count was returned
                        If .ResponseData.Tables(resultDataFormat).Rows.Count <> transferData.Tables.Count Then
                            ' This should never occur
                            SetStatus("Warning...", "Response count differs from transactions sent")
                            BackOffCommunications(False)
                        End If

                        For Each result As DataRow In .ResponseData.Tables(resultDataFormat).Rows
                            ' match the returned result to the processed data record
                            Dim dataRecord As ILiveLinkData = SendDataHelper.CompareResultList(dataList, result)

                            If dataRecord Is Nothing Then
                                ' this should never happen
                                SetStatus("Warning...", String.Format("Failed to match result to transaction ({0}). TransactionID: {1}", dataType, result("TransactionID")))
                                Continue For
                            End If

                            ' update the transactions synced value with the returned result
                            dataRecord.UpdateSynced()

                            ' All is good at this point
                            '
                            ' It may still be possible to receive a successful response and have certain
                            ' transaction flagged as error or wrong entity
                            ' These transactions then get flagged as errors in tbSalesMain but processing continues.
                            ' Note these errors are not communications errors or web service failures,
                            ' but rather explicit errors returned by the web service.
                            '
                            Select Case result("Result")
                                Case LiveLinkTransactionResponseCode.Unsynced
                                    ' This should never happen
                                    SetStatus("", String.Format("Warning: Transaction [{0}] remains unsynced.", dataRecord.TransactionID))
                                Case LiveLinkTransactionResponseCode.DataError
                                    ' This should never happen
                                    SetStatus("", String.Format("Warning: Transaction [{0}] is invalid.", dataRecord.TransactionID))
                                Case LiveLinkTransactionResponseCode.WrongEntity
                                    ' This should never happen
                                    SetStatus("", String.Format("Warning: Transaction [{0}] is for the wrong store.", dataRecord.TransactionID))
                                Case LiveLinkTransactionResponseCode.Sync
                                    transferredCount += 1
                            End Select
                            ' mark successful communications
                            SuccessfulCommunications()
                        Next
                    End With
                End If
            Next

            ' log total transferred count
            If transferredCount > 0 Then
                SetStatus("Waiting...", String.Format("Transmission complete ({0}) - {1} successful transactions.", dataType, transferredCount))
            Else
                SetStatus("Waiting...", String.Format("Transmission complete ({0})", dataType))
            End If

        Catch ex As Exception
            LogEvent(EventLogEntryType.Error, "Error [SendData]: Occurred sending data ({0}) to server. Exception: {1}", dataType, ex.ToString)
            SetStatus("Error", ex.Message)
            ' on exception, back off communications
            BackOffCommunications(True)
        Finally
            ' reset buttons to "Send"
            ToggleSendButtons(True)
        End Try
    End Sub


    Private Sub SendDataToServer()
        ' This function replaces SendDataToServer and DoQuickenPOS.
        ' It handles all POS types and all connection types
        Dim MyLiveLinkResponse As Mx.POS.Common.eRetailer.LiveLinkResponse
        Dim isSalesData As Boolean = False
        Dim isAttendancedata As Boolean = False
        UserAccess(False)
        If Is_OK_To_Send() Then
            If isDataToSend(isSalesData, isAttendancedata) Then
                ToggleSendButtons(False)
                CancelFlag = False

                If Not StartupSent Then
                    ' send startup information
                    SendStartupInfo()
                End If

                GetLease()
                If mySessionKey <> String.Empty Then
                    SetEndofDayStatus("Sending...")
                    'SEND POS DATA TO SERVER
                    Dim WSDataSet As DataSet
                    Dim HeaderDataSet As DataSet
                    Dim TXDataSet As DataSet
                    Dim sFieldListtbSalesMain As String = "*"
                    Dim dbResultOK As Boolean

                    'Get settings from config file
                    Dim sConnect As String = LiveLinkConfiguration.ConnectionString
                    Dim sConnect_ODBC As String = LiveLinkConfiguration.ODBC_DB_Connect
                    Dim Use_ODBC As Boolean = LiveLinkConfiguration.Use_ODBC_DB_Connect
                    Dim iDaysBeforeObsolete As Integer = LiveLinkConfiguration.DaysBeforeObsolete
                    If iDaysBeforeObsolete < 0 Then iDaysBeforeObsolete = 0 '0 means do not clear data
                    If (iDaysBeforeObsolete > 0) And (iDaysBeforeObsolete < 7) Then iDaysBeforeObsolete = 7 'Keep Minimum 7 days
                    Dim sStoreID As String = GetEntityId()

                    Dim sPOSType As String = LiveLinkConfiguration.POSType.Trim
                    Select Case sPOSType.ToUpper
                        Case Is = "MICROS", "PIXELPOINT"
                            bNoMSA_Flag = True
                        Case Else
                            bNoMSA_Flag = False
                    End Select

                    sFieldListtbSalesMain = GettbSalesMainFields(sPOSType.ToUpper)

                    Dim sDataFormat As String = ""
                    If TokenMode Then
                        sDataFormat = "SalesDataWithToken"
                    Else
                        sDataFormat = "SalesData"
                    End If

                    Dim MyPosData As New LiveLinkWebService
                    'Set the webservice reference
                    MyPosData.Url = myWebService
                    LoadProxy(MyPosData)
                    Dim MyCount As Integer

                    'Select records which have not been synchronised. 
                    'First record must be header record "H" or a "1" for Quicken
                    'Followed by Item "I" and then Financial "F" records
                    Dim sQry As String
                    Dim sQryTX As String = ""
                    Dim sQrySynced As String = ""
                    Dim sError As String = ""

                    If Not UseCompression Then
                        SetStatus("", "Warning: Compression is disabled")
                    End If
                    If Not TokenMode Then
                        SetStatus("", "Warning: Token mode is disabled")
                    End If
                    Dim i As Integer
                    Dim j As Integer
                    Dim RecordCount As Integer
                    Dim TxCount As Integer = 0
                    Dim AttendanceCount As Integer = 0
                    Dim HeaderRow As DataRow
                    Dim ResultRow As DataRow
                    Dim myTableName As String
                    Try
                        ' First process Sales Data
                        If isSalesData Then
                            'Loop through all transactions and synchronise 1 at a time. Can send more than transaction in a batch
                            SetStatus("Started", "Preparing batches - " & myFlowIterations.ToString & " batches of " & myFlowTxInBatch.ToString & " transactions.")

                            For i = 1 To myFlowIterations
                                WSDataSet = New DataSet
                                RecordCount = 0
                                MarkDuplicateTransactionIdsAsErrors(sPOSType, Use_ODBC)
                                sQry = sqlSelectTopNonSyncedHeaders(sPOSType, myFlowTxInBatch)
                                'Get all Transactions in this batch
                                If MMSGeneric_ListAll(Use_ODBC, sQry, "SalesData", HeaderDataSet, sError, bNoMSA_Flag) Then
                                    ' For each header row, receive all associated records and add to a unique recordset
                                    For Each HeaderRow In HeaderDataSet.Tables("SalesData").Rows
                                        If TokenMode Then
                                            'need to set a guid for this record if it does not exist
                                            'If it exists, data has been sent previously but needs to be resent
                                            sQryTX = sqlUpdateSyncToken(sPOSType, HeaderRow)
                                            If sQryTX <> "" Then
                                                If Not MMSGeneric_Execute(Use_ODBC, sQryTX, sError) Then
                                                    SetStatus("Error...", "Unable to set local token.")
                                                    SetStatus("", sError)
                                                    LogEvent("Unable to set local token" & vbCrLf & "Error: " & sError & vbCrLf & "SQL: " & sQryTX, EventLogEntryType.Error)
                                                    BackOffCommunications(True)
                                                    Exit For
                                                End If
                                            Else
                                                SetStatus("Error...", "Unable to set local token from header record.")
                                                BackOffCommunications(True)
                                                Exit For
                                            End If
                                        End If
                                        Application.DoEvents()
                                        'Get all records for this Transaction associated with header record
                                        sQryTX = sqlSelectAllRecordsForTransaction(sPOSType, Use_ODBC, HeaderRow, sFieldListtbSalesMain)
                                        If sQryTX <> "" Then
                                            myTableName = CreateTableName(sPOSType, HeaderRow)
                                            If MMSGeneric_ListAll(Use_ODBC, sQryTX, myTableName, TXDataSet, sError, bNoMSA_Flag) Then
                                                Application.DoEvents()
                                                'Add this recordset to the WSDataset (there will be one recordset per transaction)
                                                With TXDataSet.Tables(myTableName)
                                                    WSDataSet.Tables.Add(.Clone)
                                                    WSDataSet.Merge(TXDataSet.Tables(myTableName))
                                                    RecordCount += .Rows.Count
                                                End With
                                            Else
                                                Try
                                                    SetStatus("Sending data to server...", "Unable to retrieve detail for transaction " & HeaderRow.Item("TransactionID").ToString & ")")
                                                Catch ex As Exception
                                                End Try
                                                SetStatus("", sError)
                                                BackOffCommunications(True)
                                            End If
                                        Else
                                            SetStatus("Error...", "Unable to retrieve transaction detail - (sql error)")
                                            BackOffCommunications(True)
                                        End If
                                        TXDataSet = Nothing
                                        If RecordCount > myFlowMaxRecords Then
                                            ' If the maximum number of records tp be sent in a single batch has been exceed, then do not add
                                            ' any more transaction recordsets to the dataset to be transmitted
                                            Exit For
                                        End If
                                        Application.DoEvents()
                                        If WSDataSet.Tables.Count >= myFlowTxInBatch Then
                                            Exit For ' Sybase 5 (PixelPoint) cannot limit the number of header rows, so check here
                                        End If
                                    Next
                                    ' ------------ END CREATION OF DATASET TO SEND -----------------
                                    If WSDataSet.Tables.Count > 0 Then ' If there was at least one transaction retrieved for sending

                                        '
                                        ' NEW PRE-SEND PROCESSING - MG 19/09/2006
                                        '
                                        PreSendProcess(WSDataSet)

                                        'Now process all records for this transaction.
                                        'Send Transaction and wait for result
                                        SetStatus("Sending data to server...", "Batch " & i.ToString & ". Sending " & WSDataSet.Tables.Count.ToString & " transactions (" & RecordCount.ToString & " records)")
                                        GetLease()
                                        Application.DoEvents()
                                        If UseCompression Then
                                            MyLiveLinkResponse = MyPosData.Send_POSData2(MyIdentity, mySessionKey, sDataFormat, CompressDataSet(WSDataSet), sStoreID, sPOSType)
                                        Else
                                            MyLiveLinkResponse = MyPosData.Send_POSData2(MyIdentity, mySessionKey, sDataFormat, WSDataSet, sStoreID, sPOSType)
                                        End If
                                        Application.DoEvents()
                                        'MyPosData.Send_POSData2(myIdentity, mySessionKey, sDataFormat, TxDataSet, sStoreID, sPOSType)
                                        '***MyCount = MyPosData.Send_POSDataMain(sClientCode, sSecurityCode, sDataFormat, TXDataSet, sStoreID, sPOSType)
                                        'If Success then set that record to synced
                                        With MyLiveLinkResponse
                                            If .ResponseCode = eRetailer.LiveLinkResponseCode.Success Then
                                                MessageWaiting = MyLiveLinkResponse.ResponseMessageWaiting
                                                If Not (.ResponseData Is Nothing) Then
                                                    ' There should be a reecord called "SalesDataResults" set containing the results of each transaction
                                                    If .ResponseData.Tables("SalesDataResults").Rows.Count <> WSDataSet.Tables.Count Then
                                                        ' This should never occur
                                                        SetStatus("Warning ...", "Response count differs from transactions sent")
                                                        BackOffCommunications(False)
                                                    End If
                                                    ' Continue to process any transactions returned, even if the number of results differs from the number sent
                                                    For Each ResultRow In .ResponseData.Tables("SalesDataResults").Rows
                                                        sQrySynced = sqlUpdateSyncedFlag(sPOSType, ResultRow)
                                                        If sQrySynced <> "" Then
                                                            If MMSGeneric_Execute(Use_ODBC, sQrySynced, sError) Then
                                                                ' All is good
                                                                ' It may still be possible to receive a successful response and have certain
                                                                ' transaction flagged as error or wrong entity
                                                                ' These transactions then get flagged as errors in tbSalesMain but processing continues.
                                                                ' Note these errors are not communications errors or web service failures,
                                                                ' but rather explicit errors returned by the web service.
                                                                Select Case ResultRow.Item("Result")
                                                                    Case LiveLinkTransactionResponseCode.Unsynced
                                                                        ' This should never happen
                                                                        SetStatus("", "Warning: Transaction [" & ResultRow.Item("TransactionID").ToString & "] remains unsynced.")
                                                                    Case LiveLinkTransactionResponseCode.DataError
                                                                        ' This should never happen
                                                                        SetStatus("", "Warning: Transaction [" & ResultRow.Item("TransactionID").ToString & "] is invalid.")
                                                                    Case LiveLinkTransactionResponseCode.WrongEntity
                                                                        ' This should never happen
                                                                        SetStatus("", "Warning: Transaction [" & ResultRow.Item("TransactionID").ToString & "] is for the wrong store.")
                                                                    Case LiveLinkTransactionResponseCode.Sync
                                                                        TxCount += 1
                                                                End Select
                                                                SuccessfulCommunications()
                                                            Else
                                                                Try
                                                                    SetStatus("Error ...", "Unable to save results for transaction " & ResultRow.Item("TransactionID").ToString)
                                                                Catch ex As Exception
                                                                End Try
                                                                SetStatus("", sError)
                                                                LogEvent("Unable to save results for transaction " & ResultRow.Item("TransactionID").ToString & vbCrLf & "Error: " & sError & vbCrLf & "SQL: " & sQrySynced, EventLogEntryType.Error)
                                                                BackOffCommunications(True)
                                                                ' Don't exit FOR loop here because other resuts might be OK
                                                            End If
                                                        Else
                                                            ' Error generating SQL code
                                                            SetStatus("Error ...", "Unable to save results - (sql error)")
                                                            BackOffCommunications(True)
                                                            ' Don't exit FOR loop here because other resuts might be OK
                                                        End If
                                                        Application.DoEvents()
                                                    Next
                                                Else
                                                    ' This should never occur - empty dataset of results
                                                    SetStatus("Error ...", "No results were returned")
                                                    BackOffCommunications(False)
                                                    Exit For ' Do not process any more
                                                End If
                                            Else
                                                ' Response code returned was not Success
                                                ' TODO: Handle other responses
                                                ProcessResponse(MyLiveLinkResponse, True)
                                                Exit For ' Do not process any more
                                            End If
                                        End With
                                    Else
                                        SetStatus("Waiting...", "No data to send.")
                                        Exit For
                                    End If
                                Else
                                    SetStatus("Error", "Unable to retrieve unsynced headers")
                                    SetStatus("", sError)
                                    BackOffCommunications(True)
                                    Exit For
                                End If
                                Application.DoEvents()
                                If CancelFlag Then
                                    SetStatus("", "Sending has been cancelled")
                                    Exit For
                                End If
                                WSDataSet = Nothing
                            Next
                        End If
                        '++++++++++++++++++ NOW SEND ATTENDANCE DATA ++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                        If (Not CancelFlag) And isAttendancedata Then
                            Dim AttendanceDataSet As DataSet

                            'Loop through all transactions and synchronise 1 at a time. Can send more than transaction in a batch
                            SetStatus("Started", "Preparing Attendance batches - " & myFlowIterations.ToString & " batches of " & myFlowMaxRecords.ToString & " records.")

                            sDataFormat = "AttendanceData"
                            For i = 1 To myFlowIterations
                                RecordCount = 0
                                sQry = sqlSelectTopNonSyncedHeaders(sPOSType, myFlowMaxRecords, DataType.AttendanceData)
                                'Get all records in this batch
                                If MMSGeneric_ListAll(Use_ODBC, sQry, "AttendanceData", AttendanceDataSet, sError, bNoMSA_Flag) Then
                                    ' For each header row, receive all associated records and add to a unique recordset
                                    j = 0
                                    For Each HeaderRow In AttendanceDataSet.Tables("AttendanceData").Rows
                                        'If TokenMode Then ' TokenMode is now mandatory
                                        'need to set a guid for this record if it does not exist
                                        'If it exists, data has been sent previously but needs to be resent
                                        sQryTX = sqlUpdateSyncToken(sPOSType, HeaderRow, DataType.AttendanceData)
                                        If sQryTX <> "" Then
                                            If MMSGeneric_Execute(Use_ODBC, sQryTX, sError) Then
                                                ' Now return the SyncToken and update the dataset
                                                sQryTX = sqlGetSyncToken(sPOSType, HeaderRow, DataType.AttendanceData)
                                                If sQryTX <> "" Then
                                                    If MMSGeneric_ListAll(Use_ODBC, sQryTX, "SyncToken", TXDataSet, sError, bNoMSA_Flag) _
                                                    AndAlso TXDataSet.Tables("SyncToken").Rows.Count = 1 Then
                                                        HeaderRow.Item("SyncToken") = TXDataSet.Tables("SyncToken").Rows(0).Item("SyncToken")
                                                    Else
                                                        SetStatus("Error...", "Unable to set retrieve local token (attendance).")
                                                        SetStatus("", sError)
                                                        BackOffCommunications(True)
                                                        Exit For
                                                    End If
                                                Else
                                                    SetStatus("Error...", "Unable to retrieve local token (attendance) from header record.")
                                                    BackOffCommunications(True)
                                                    Exit For
                                                End If
                                            Else
                                                SetStatus("Error...", "Unable to set local token (attendance).")
                                                SetStatus("", sError)
                                                LogEvent("Unable to set local token (attendance)" & vbCrLf & "Error: " & sError & vbCrLf & "SQL: " & sQryTX, EventLogEntryType.Error)
                                                BackOffCommunications(True)
                                                Exit For
                                            End If
                                        Else
                                            SetStatus("Error...", "Unable to set local token (attendance) from header record.")
                                            BackOffCommunications(True)
                                            Exit For
                                        End If
                                        'End If
                                        Application.DoEvents()
                                        j += 1
                                        If j >= myFlowMaxRecords Then
                                            Exit For ' Sybase 5 (PixelPoint) cannot limit the number of header rows, so check here
                                        End If
                                    Next
                                    ' ------------ END CREATION OF ATTENDANCE DATASET TO SEND -----------------
                                    If AttendanceDataSet.Tables("AttendanceData").Rows.Count > 0 Then ' If there was at least one attendance record retrieved for sending
                                        'Now process all attendance records
                                        'Send Transaction and wait for result
                                        SetStatus("Sending attendance data to server...", "Attendance Batch " & i.ToString & ". Sending " & AttendanceDataSet.Tables("AttendanceData").Rows.Count & " records")
                                        GetLease()
                                        Application.DoEvents()
                                        If UseCompression Then
                                            MyLiveLinkResponse = MyPosData.Send_POSData2(MyIdentity, mySessionKey, sDataFormat, CompressDataSet(AttendanceDataSet), sStoreID, sPOSType)
                                        Else
                                            MyLiveLinkResponse = MyPosData.Send_POSData2(MyIdentity, mySessionKey, sDataFormat, AttendanceDataSet, sStoreID, sPOSType)
                                        End If
                                        Application.DoEvents()
                                        'MyPosData.Send_POSData2(myIdentity, mySessionKey, sDataFormat, TxDataSet, sStoreID, sPOSType)
                                        '***MyCount = MyPosData.Send_POSDataMain(sClientCode, sSecurityCode, sDataFormat, TXDataSet, sStoreID, sPOSType)
                                        'If Success then set that record to synced
                                        With MyLiveLinkResponse
                                            If .ResponseCode = eRetailer.LiveLinkResponseCode.Success Then
                                                If Not (.ResponseData Is Nothing) Then
                                                    ' There should be a datatable called "SalesDataResults" containing the results of each transaction
                                                    If .ResponseData.Tables("AttendanceDataResults").Rows.Count <> AttendanceDataSet.Tables("AttendanceData").Rows.Count Then
                                                        ' This should never occur
                                                        SetStatus("Warning ...", "Response count differs from attendance records sent")
                                                        BackOffCommunications(False)
                                                    End If
                                                    ' Continue to process any transactions returned, even if the number of results differs from the number sent
                                                    For Each ResultRow In .ResponseData.Tables("AttendanceDataResults").Rows
                                                        sQrySynced = sqlUpdateSyncedFlag(sPOSType, ResultRow, DataType.AttendanceData)
                                                        If sQrySynced <> "" Then
                                                            If MMSGeneric_Execute(Use_ODBC, sQrySynced, sError) Then
                                                                ' All is good
                                                                ' It may still be possible to receive a successful response and have certain
                                                                ' records flagged as error or wrong entity
                                                                ' These transactions then get flagged as errors in tbSalesMain but processing continues.
                                                                ' Note these errors are not communications errors or web service failures,
                                                                ' but rather explicit errors returned by the web service.
                                                                Select Case ResultRow.Item("Result")
                                                                    Case LiveLinkTransactionResponseCode.Unsynced
                                                                        ' This should never happen
                                                                        SetStatus("", "Warning: Record [" & ResultRow.Item("SalesMainID").ToString & "] remains unsynced.")
                                                                    Case LiveLinkTransactionResponseCode.DataError
                                                                        ' This should never happen
                                                                        SetStatus("", "Warning: Record [" & ResultRow.Item("SalesMainID").ToString & "] is invalid.")
                                                                    Case LiveLinkTransactionResponseCode.WrongEntity
                                                                        ' This should never happen
                                                                        SetStatus("", "Warning: Record [" & ResultRow.Item("SalesMainID").ToString & "] is for the wrong store.")
                                                                    Case LiveLinkTransactionResponseCode.Sync
                                                                        AttendanceCount += 1
                                                                End Select
                                                                SuccessfulCommunications()
                                                            Else
                                                                Try
                                                                    SetStatus("Error ...", "Unable to save results for record " & ResultRow.Item("SalesMainID").ToString)
                                                                Catch ex As Exception
                                                                End Try
                                                                SetStatus("", sError)
                                                                LogEvent("Unable to save results for record " & ResultRow.Item("SalesMainID").ToString & vbCrLf & "Error: " & sError & vbCrLf & "SQL: " & sQrySynced, EventLogEntryType.Error)
                                                                BackOffCommunications(True)
                                                                ' Don't exit FOR loop here because other resuts might be OK
                                                            End If
                                                        Else
                                                            ' Error generating SQL code
                                                            SetStatus("Error ...", "Unable to save attendance results - (sql error)")
                                                            BackOffCommunications(True)
                                                            ' Don't exit FOR loop here because other resuts might be OK
                                                        End If
                                                        Application.DoEvents()
                                                    Next
                                                Else
                                                    ' This should never occur - empty dataset of results
                                                    SetStatus("Error ...", "No attendance results were returned")
                                                    BackOffCommunications(False)
                                                    Exit For ' Do not process any more
                                                End If
                                            Else
                                                ' Response code returned was not Success
                                                ' TODO: Handle other responses
                                                ProcessResponse(MyLiveLinkResponse, True)
                                                Exit For ' Do not process any more
                                            End If
                                        End With
                                    Else
                                        SetStatus("Waiting...", "No data to send.")
                                        Exit For
                                    End If
                                Else
                                    SetStatus("Error", "Unable to retrieve attendance data")
                                    SetStatus("", sError)
                                    BackOffCommunications(True)
                                    Exit For
                                End If
                                Application.DoEvents()
                                If CancelFlag Then
                                    SetStatus("", "Sending has been cancelled")
                                    Exit For
                                End If
                                AttendanceDataSet = Nothing
                            Next
                        End If
                    Catch ex As WebException
                        SetStatus("Error", ex.Message)
                        BackOffCommunications(False)
                    Catch ex As Exception
                        SetStatus("Error", ex.ToString)
                        BackOffCommunications(True)
                    End Try
                    If isSalesData Then
                        If TxCount > 0 Then
                            SetStatus("Waiting...", "Sales transmission complete - " & TxCount.ToString & " successful transactions.")
                        Else
                            SetStatus("Waiting...", "Sales transmission complete")
                        End If
                    End If
                    If isAttendancedata Then
                        If AttendanceCount > 0 Then
                            SetStatus("Waiting...", "Attendance transmission complete - " & AttendanceCount.ToString & " successful records.")
                        Else
                            SetStatus("Waiting...", "Attendance transmission complete")
                        End If
                    End If

                    Dim alwaysUseDefaultMethod As Boolean = Not LiveLinkConfiguration.DataPurgeMethod_KeyExists()
                    'If data purge method setting exists in customer mx.config, we will use that setting, it not, we always use default purge method

                    _livelinkCore.PurgeAllOldData(Use_ODBC, iDaysBeforeObsolete, sPOSType, alwaysUseDefaultMethod)

                    SetEndofDayStatus("")
                Else
                    If (myConditionalSessionKey <> "") And (mySessionKey = String.Empty) Then
                        SetStatus("", "Site not yet activated.")
                    End If
                End If
            Else
                SetStatus("Waiting...", "No data to send.")
            End If
        End If
        ToggleSendButtons(True)
    End Sub

#End Region

#Region "  Main Form functionality "

    Private Sub ToggleSendButtons(ByVal AllowToSend As Boolean)
        If AllowToSend Then
            btnProcessNow.Enabled = True
            btnAccessCode.Enabled = True
            btnEndOfDay.Enabled = True
            btnProcessNow.Text = "Process Now"
            TextBoxAccessCode.Enabled = True
            CancelFlag = False
            SetEndofDayStatus(String.Empty)
        Else
            UserAccess(False)
            btnProcessNow.Enabled = False
            btnAccessCode.Enabled = False
            btnEndOfDay.Enabled = False
            TextBoxAccessCode.Enabled = False
            btnProcessNow.Text = "Cancel"
            btnProcessNow.Enabled = True
        End If
    End Sub

    Private Sub btnEndOfDay_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEndOfDay.Click
        If Not Processing Then
            Processing = True
            OK_To_Send = True ' Override the backoff timer if End of Day button is clicked
            StartStopMainTimer(False)
            TabControl1.Enabled = False
            btnProcessNow.Enabled = False
            Button1.Enabled = False
            btnEndOfDay.Enabled = False
            SetEndofDayStatus("Start")
            If MyParEnabled Then
                If MyParUseTLDServer Then
                    UpdateParStatus()
                    ParImport(MyParPerformFullImport, MyParTLDListen)
                    UpdateParStatus()
                    SendDataToServer()
                    UpdateParStatus()
                Else
                    ParGetTld(MyParPerformFullImport)
                    UpdateParStatus()
                    ParImportFile(MyParPerformFullImport)
                    UpdateParStatus()
                    SendDataToServer()
                    UpdateParStatus()
                End If
                'Need to add code to set flag on server to process data 
            End If
            If MyAlohaEnabled Then
                SetEndofDayStatus("Grinding ...")
                ProcessAloha(True)
                SetEndofDayStatus("Sending ...")
                SendDataToServer()
            End If
            If MyMicrosEnabled Then
                SetEndofDayStatus("Importing ...")
                ProcessMicros(True)
                SetEndofDayStatus("Sending ...")
                SendDataToServer()
            End If

            If _posCommon.HasPosData() Then
                SetEndofDayStatus("Importing ...")
                ProcessPosCommon(True)
                SetEndofDayStatus("Sending ...")
                SendDataToServer()
            End If

            If MySusPosEnabled Then
                SetEndofDayStatus("Importing ...")

                SetEndofDayStatus("Sending ...")
                SendDataToServer()
            End If

            SetEndofDayStatus("")
            If LabelStatus.Text = "Running" Then
                CurrentCount = 0
                StartStopMainTimer(True)
            End If
            btnProcessNow.Enabled = True
            Button1.Enabled = True
            btnEndOfDay.Enabled = True
            Processing = False
        End If
    End Sub
    Private Sub SetEndofDayStatus(ByVal sStatus As String)
        LabelEndOfDayStatus.Text = sStatus
        LabelEndOfDayStatus.Update()
        Application.DoEvents()
    End Sub

    Private Sub txtFlowInterval_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtFlowInterval.TextChanged
        Try
            myFlowInterval = Val(txtFlowInterval.Text.Trim)
        Catch ex As Exception
            myFlowInterval = 15
        End Try
        txtFlowInterval.Text = myFlowInterval.ToString
    End Sub

    Private Sub LiveLinkMain_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Resize
        If LaunchHidden Then
            If Me.WindowState = FormWindowState.Minimized Then
                Me.ShowInTaskbar = False
            Else
                Me.ShowInTaskbar = True
            End If
        End If
    End Sub

    Private Sub mnuShowLiveLink_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuShowLiveLink.Click
        Me.WindowState = FormWindowState.Normal
        Me.Show()
    End Sub

    Private Sub mnuTrayExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuTrayExit.Click
        LogEvent("Use attempting to quit LiveLink from the System Tray", EventLogEntryType.Information)
        EndApplication()
    End Sub

    'Private Sub LiveLinkMain_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
    '    Try
    '        Dim myStackTrace As System.Diagnostics.StackTrace = New System.Diagnostics.StackTrace(True)
    '        Dim PotentialCancel As Boolean = False
    '        Dim ReasonCode As Integer = 0

    '        ' Stackframe 7 is a call from code
    '        If myStackTrace.GetFrame(7).GetMethod.Name.ToString = "SendMessage" Then
    '            PotentialCancel = True
    '            ReasonCode = 1
    '        ElseIf myStackTrace.FrameCount >= 14 Then
    '            ' Stackframe 14 is the X, ALT-F4 and Menu Exits
    '            If myStackTrace.GetFrame(14).GetMethod.Name.ToString = "WmSysCommand" Then
    '                PotentialCancel = True
    '                ReasonCode = 2
    '            End If
    '        End If

    '        If PotentialCancel Then
    '            ' This will only be true if the program is exiting becuase of a call in code, the X button, ALT-F4 etc
    '            ' This will not be true if Windows is shutting down or logging off
    '            ' If the user has initiated a close, and the AutoiProcess is Stopped and the AccessCode has been entered,
    '            ' then shut down the app, otherwise minimise it.
    '            If Not ((LabelStatus.Text = "Stopped") And (UserAccessStatus)) Then
    '                Me.WindowState = FormWindowState.Minimized
    '                e.Cancel = True
    '            End If
    '        End If
    '        If Not e.Cancel Then
    '            LogEvent("Live Link shutting down.  Reason code: " & ReasonCode, EventLogEntryType.Information)
    '            Application.Exit()
    '        End If
    '    Catch ex As Exception
    '    End Try
    'End Sub

#End Region

#Region "  Logging functionality "

    Private Function AddLogInfo(ByRef LogDataSet As DataSet, ByVal MessageType As SessionMessage, ByVal MessageName As String, ByVal MessageDescription As String)
        Dim InfoRow As DataRow
        Try
            InfoRow = LogDataSet.Tables(cLogDataFormat).NewRow
            InfoRow.Item("MessageType") = MessageType
            InfoRow.Item("MessageName") = MessageName
            InfoRow.Item("MessageDescription") = MessageDescription
            LogDataSet.Tables(cLogDataFormat).Rows.Add(InfoRow)
        Catch ex As Exception
        End Try
    End Function

    Private Sub CreateLogTable(ByRef LogDataSet As DataSet)
        Try
            LogDataSet.Tables.Add(New DataTable(cLogDataFormat))
            LogDataSet.Tables(cLogDataFormat).Columns.Add(New DataColumn("MessageType", System.Type.GetType("System.String")))
            LogDataSet.Tables(cLogDataFormat).Columns.Add(New DataColumn("MessageName", System.Type.GetType("System.String")))
            LogDataSet.Tables(cLogDataFormat).Columns.Add(New DataColumn("MessageDescription", System.Type.GetType("System.String")))
        Catch ex As Exception
        End Try
    End Sub

    Private Function SendLogInfo(ByVal MessageType As SessionMessage, ByVal MessageName As String, ByVal MessageDescription As String) As Boolean
        GetLease()
        SendLogInfo = False
        If HasConditionalLease() OrElse Is_OK_To_Send() Then
            Try
                If myConditionalSessionKey <> "" Then
                    Dim WSDataSet As New DataSet
                    CreateLogTable(WSDataSet)
                    AddLogInfo(WSDataSet, MessageType, MessageName, MessageDescription)
                    SendLogInfo = SendLogInfo(WSDataSet) ' Calls overloaded function with same name
                End If
            Catch ex As Exception
            End Try
        End If
    End Function

    Private Function SendLogInfo(ByVal LogDataSet As DataSet) As Boolean
        GetLease()
        SendLogInfo = False
        If HasConditionalLease() OrElse Is_OK_To_Send() Then
            If myConditionalSessionKey <> String.Empty Then
                Dim MyLogInfo As New LiveLinkWebService
                Dim sStoreID As String = GetEntityId()
                Dim sPOSType As String = LiveLinkConfiguration.POSType.Trim
                Dim MyLiveLinkResponse As Mx.POS.Common.eRetailer.LiveLinkResponse

                MyLogInfo.Url = myWebService
                LoadProxy(MyLogInfo)
                Dim Retries As Integer
                For Retries = 1 To 2 ' The lease may have expired due to long running activities such as large file downloads
                    If UseCompression Then
                        MyLiveLinkResponse = MyLogInfo.Send_POSData2(MyIdentity, myConditionalSessionKey, "Information", CompressDataSet(LogDataSet), sStoreID, sPOSType)
                    Else
                        MyLiveLinkResponse = MyLogInfo.Send_POSData2(MyIdentity, myConditionalSessionKey, "Information", LogDataSet, sStoreID, sPOSType)
                    End If
                    Select Case MyLiveLinkResponse.ResponseCode
                        Case eRetailer.LiveLinkResponseCode.Success
                            SendLogInfo = True
                            MessageWaiting = MyLiveLinkResponse.ResponseMessageWaiting
                            UpdateLastSuccessfulCommunication()
                            Exit For
                        Case eRetailer.LiveLinkResponseCode.LeaseExpired
                            If Retries = 1 Then
                                ExpireLease()
                                GetLease()
                            End If
                        Case Else
                            SendLogInfo = False
                            Exit For
                    End Select
                Next
            End If
        End If
    End Function

#End Region

#Region "Setup Information"

    Function SendStartupInfo() As Boolean
        Dim Msg As String
        Dim LogDataSet As New DataSet
        CreateLogTable(LogDataSet)
        Msg = "Live Link was started at " & Format(LiveLinkStartTime, "dd-MMM-yyyy HH:mm:ss") & " (" & Format(LiveLinkStartTime.ToUniversalTime, "dd-MMM-yyyy HH:mm:ss") & " UTC)"
        AddLogInfo(LogDataSet, SessionMessage.msg_Information, "Live Link started", Msg)
        AddLogInfo(LogDataSet, SessionMessage.msg_Information, "Live Link Version", Application.ProductVersion)
        AddLogInfo(LogDataSet, SessionMessage.msg_Information, "Live Link Directory", Application.StartupPath)
        Try
            ' if the store number argument is supplied, and auto-activation is enabled. Log the information
            If (Not String.IsNullOrEmpty(_argumentStoreNumber)) Then
                AddLogInfo(LogDataSet, SessionMessage.msg_Information, "Auto-Activation Store Number", _argumentStoreNumber)
            End If

            Dim objSysInfo As New clsSysInfo
            With objSysInfo
                AddLogInfo(LogDataSet, SessionMessage.msg_Information, "Computer Name", .ComputerName)
                AddLogInfo(LogDataSet, SessionMessage.msg_Information, "Computer Manufacturer", .Manufacturer)
                AddLogInfo(LogDataSet, SessionMessage.msg_Information, "Computer Model", .Model)
                AddLogInfo(LogDataSet, SessionMessage.msg_Information, "OS Name", .OsName)
                AddLogInfo(LogDataSet, SessionMessage.msg_Information, "OS Version", .OSVersion)
                AddLogInfo(LogDataSet, SessionMessage.msg_Information, "System Type", .SystemType)
                AddLogInfo(LogDataSet, SessionMessage.msg_Information, "Total Physical Memory", .TotalPhysicalMemory)
                AddLogInfo(LogDataSet, SessionMessage.msg_Information, "Windows Directory", .WindowsDirectory)
                AddLogInfo(LogDataSet, SessionMessage.msg_Information, "DotNet 1.1 Installed", IsDotNetInstalled("1.1.4322"))
                AddLogInfo(LogDataSet, SessionMessage.msg_Information, "DotNet 2.0 Installed", IsDotNetInstalled("2.0.50727"))
                If .FixedDisk1 <> "" Then
                    AddLogInfo(LogDataSet, SessionMessage.msg_Information, "Fixed Disk 1", .FixedDisk1)
                End If
                If .FixedDisk2 <> "" Then
                    AddLogInfo(LogDataSet, SessionMessage.msg_Information, "Fixed Disk 2", .FixedDisk2)
                End If
                If .FixedDisk3 <> "" Then
                    AddLogInfo(LogDataSet, SessionMessage.msg_Information, "Fixed Disk 3", .FixedDisk3)
                End If
            End With
        Catch ex As Exception
            SetStatus("", ex.Message)
            AddLogInfo(LogDataSet, SessionMessage.msg_Warning, "System Info", "Unable to retrieve system information")
        End Try

        Try
            If SendLogInfo(LogDataSet) Then
                StartupSent = True
            End If
        Catch ex As WebException
            SetStatus("Error", ex.Message)
            BackOffCommunications(False)
        Catch ex As Exception
            SetStatus("Error", "Unable to send Startup Info")
        End Try
    End Function

    Private Function IsDotNetInstalled(ByVal version As String) As String
        Dim result As String = RegRead(Registry.LocalMachine, "Software\Microsoft\NET Framework Setup\NDP\v" + version, "Install", "Unknown")
        Return result
    End Function

#End Region


    Private Sub TextBoxAwake_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBoxAwake.TextChanged
        Try
            myAwakeInterval = Val(TextBoxAwake.Text.Trim)
        Catch ex As Exception
            myAwakeInterval = 30
        End Try
        TextBoxAwake.Text = myAwakeInterval.ToString
    End Sub

    Private Enum CompareByOptions
        FileName
        LastWriteTime
        Length
    End Enum

    Private Class CompareFileInfoEntries
        Implements IComparer

        Private compareBy As CompareByOptions = CompareByOptions.FileName

        Public Sub New(ByVal cBy As CompareByOptions)
            compareBy = cBy
        End Sub

        Public Overridable Overloads Function Compare(ByVal file1 As Object, _
               ByVal file2 As Object) As Integer Implements IComparer.Compare
            'Convert file1 and file2 to FileInfo entries
            Dim f1 As FileInfo = CType(file1, FileInfo)
            Dim f2 As FileInfo = CType(file2, FileInfo)

            'Compare the file names
            Select Case compareBy
                Case CompareByOptions.FileName
                    Return String.Compare(f1.Name, f2.Name)
                Case CompareByOptions.LastWriteTime
                    Return DateTime.Compare(f1.LastWriteTime, f2.LastWriteTime)
                Case CompareByOptions.Length
                    Return f1.Length - f2.Length
            End Select
        End Function
    End Class

    Private Sub DeleteAllFilesInDirectory(ByVal DirectoryName)
        Try
            Dim Files As String() = Directory.GetFiles(DirectoryName)
            Dim dirFile As String

            ' Delete all of the files in the directory
            For Each dirFile In Files
                ' Remove any ReadOnly attributes first
                ClearFileAttribute(Path.GetFullPath(dirFile), FileAttributes.ReadOnly Or FileAttributes.Hidden Or FileAttributes.System)
                File.Delete(Path.GetFullPath(dirFile))
            Next

            ' Now do the same for all subdirectories within this folder
            Dim Directories As String() = Directory.GetDirectories(DirectoryName)
            Dim dirDirectories As String
            For Each dirDirectories In Directories
                ' Calls itself recursively
                DeleteAllFilesInDirectory(Path.GetFullPath(dirDirectories))
                Directory.Delete(Path.GetFullPath(dirDirectories))
            Next
        Catch ex As Exception
        End Try
    End Sub

    Private Sub CopyAllFiles(ByVal SourceDir As String, ByVal TargetDir As String)
        Try
            Dim Files As String() = Directory.GetFiles(SourceDir)
            Dim dirFile As String

            If Strings.Right(SourceDir, 1) <> "\" Then
                SourceDir &= "\"
            End If
            If Strings.Right(TargetDir, 1) <> "\" Then
                TargetDir &= "\"
            End If

            ' Copy all of the files in the directory
            For Each dirFile In Files
                File.Copy(Path.GetFullPath(dirFile), TargetDir & Path.GetFileName(dirFile), True)
            Next
        Catch ex As Exception
        End Try
    End Sub

#Region "LiveLink Upgrade"

    Private Sub UpgradeLiveLink(ByVal url As String, ByVal Hash As String)
        Try
            Dim ErrorMessage As String = String.Empty

            Const cUpgradeFile As String = "LiveLinkUpgrade.zip"
            Const cLiveLinkUpdater As String = "LiveLinkUpdate.exe"

            SetStatus("Updating ...", "Updating LiveLink")
            Dim LiveLinkPath2 As String = Path.GetDirectoryName(Environment.GetCommandLineArgs(0))
            Dim LiveLinkPath As String = Application.StartupPath
            If Strings.Right(LiveLinkPath, 1) <> "\" Then
                LiveLinkPath &= "\"
            End If
            Dim NewVersionDirectory As String = LiveLinkPath & "Upgrade\"
            Dim DownloadDirectory As String = LiveLinkPath & "Download\"
            Dim BackupDirectory As String = LiveLinkPath & "Backup\"
            If Not Directory.Exists(DownloadDirectory) Then
                Directory.CreateDirectory(DownloadDirectory)
            End If
            If File.Exists(DownloadDirectory & cUpgradeFile) Then
                ' Make sure the file is not readonly
                ClearFileAttribute(DownloadDirectory & cUpgradeFile, FileAttributes.ReadOnly)
                File.Delete(DownloadDirectory & cUpgradeFile)
            End If
            ' Download the update file
            SetStatus("", "Downloading new version")
            If HttpDownload.GetFile(url, DownloadDirectory & cUpgradeFile, ErrorMessage, myProxy) Then
                ' TODO: Backup the existing version
                If Not Directory.Exists(BackupDirectory) Then
                    Directory.CreateDirectory(BackupDirectory)
                End If
                DeleteAllFilesInDirectory(BackupDirectory)
                CopyAllFiles(LiveLinkPath, BackupDirectory)
                ' Extract the downloaded files (first delete all files from the extraction directory)
                DeleteAllFilesInDirectory(NewVersionDirectory)
                If ExtractZip(DownloadDirectory & cUpgradeFile, NewVersionDirectory, ErrorMessage) Then
                    ' Validate the extracted files
                    ' Create part 1 of the checksum by adding the file lengths together, each multiplied by their position in the sorted list
                    ' Create part 2 of the checksum by creating a concatenated list of the sorted file names
                    Dim di As New DirectoryInfo(NewVersionDirectory)
                    Dim fiArr As FileInfo() = di.GetFiles()
                    Dim fi As FileInfo
                    Dim i As Integer
                    Dim AllFileNames As String = String.Empty
                    Dim myCheckSum As Long = 0

                    fiArr.Sort(fiArr, New CompareFileInfoEntries(CompareByOptions.FileName))
                    For i = 0 To fiArr.Length - 1
                        myCheckSum += fiArr(i).Length * (i + 1)
                        AllFileNames &= Path.GetFileName(fiArr(i).FullName).Trim.ToUpper
                    Next

                    ' Now add the two parts together and calculate the SHA1 hash - this should match the checksum
                    If Polling.SHA1(myCheckSum.ToString & AllFileNames) = Hash Then
                        SetStatus("", "File validation completed")
                        ' Check for a new version of the updater and, if it exists copy to the LiveLink directory
                        Try
                            If File.Exists(NewVersionDirectory & cLiveLinkUpdater) Then
                                File.Copy(NewVersionDirectory & cLiveLinkUpdater, LiveLinkPath & cLiveLinkUpdater, True)
                            End If
                        Catch ex As Exception
                        End Try
                        ' Launch the LiveLink updater and shut down if it launches successfully
                        SetStatus("", "Launching updater")
                        Dim ProcessId = Shell(LiveLinkPath & cLiveLinkUpdater & " " & Process.GetCurrentProcess.Id.ToString & " " & Hash, AppWinStyle.MinimizedNoFocus, False)
                        ' Pause for 2 seconds
                        Threading.Thread.Sleep(2000)
                        Try
                            Dim UpdateProcess As Process = Process.GetProcessById(ProcessId)
                            ' Do a couple of checks to make sure that the LiveLink Update process has in fact started,
                            ' then shut down LiveLink.  The Updater will restart LiveLink.
                            If Not UpdateProcess.HasExited Then
                                If UpdateProcess.ProcessName.Trim.ToUpper = "LIVELINKUPDATE" Then
                                    SetStatus("Shut down", "LiveLink will shut down in 5 seconds")
                                    SetStatus("", "It will automatically restart after the update")
                                    Threading.Thread.Sleep(5000)
                                    LogEvent("Live Link shutting down for update", EventLogEntryType.Information)
                                    EndApplication(True)
                                End If
                            End If
                        Catch ex As Exception
                            ' This error should only occur if the known DOT NET process error causes it
                            ' Under these circumstances, assume that the Updater has started and close LiveLink
                            SetStatus("Shut down", "LiveLink will shut down in 5 seconds")
                            SetStatus("", "It will automatically restart after the update")
                            Threading.Thread.Sleep(5000)
                            LogEvent("Live Link shutting down for update", EventLogEntryType.Information)
                            EndApplication(True)
                        End Try
                    Else
                        SetStatus("Error", "Downloaded files could not be validated")
                    End If
                Else
                    SetStatus("Error", ErrorMessage)
                End If
            Else
                SetStatus("Error", ErrorMessage)
            End If
        Catch ex As Exception
            SetStatus("", ex.Message)
        End Try
    End Sub

#End Region

#Region "POS Commands"

    ' Load a balloon tip to display the POS error message
    Private Sub LoadPOSErrorBalloonTip(ByVal Message As String)
        ' check if the new enhanced message is enabled
        If LiveLinkConfiguration.ComprisActiveErrorBalloonTipEnabled Then
            LogEvent("Displaying POS Message Form. Message=[" & Message & "]", EventLogEntryType.Warning)
            NotifyIcon1.ShowBalloonTip(120000, "MacromatiX", Message, ToolTipIcon.Error)
        End If
    End Sub

    ' Load a new non-blocking message form for a Message to do with a POS
    Private Sub LoadPOSErrorDialog(ByVal messageType As ComprisActiveMessageType, ByVal errorType As ComprisActiveErrorType, Optional ByVal terminalList As List(Of Short) = Nothing)
        ' check if compris pos error dialog is enabled
        If LiveLinkConfiguration.ComprisActiveErrorDialogEnabled Then
            ' load the error message translations
            If MyComprisActiveMessages Is Nothing Then
                MyComprisActiveMessages = ComprisActiveMessages.LoadMessages(LiveLinkConfiguration.ComprisActiveErrorMessagesFile)
            End If

            ' get specific message details
            Dim messageDetail As ComprisActiveErrorMessage = MyComprisActiveMessages.GetMessage(messageType, errorType)
            If messageDetail Is Nothing Then
                Exit Sub
            End If

            Dim posMsg As New StringBuilder(1024)
            ' set the initial message to display as the supplied message
            posMsg.AppendFormat("{0}{1}{1}", messageDetail.messageText, vbCrLf)

            If Not String.IsNullOrEmpty(messageDetail.messageSuffix) Then
                posMsg.Append(messageDetail.messageSuffix)
            End If

            If MyComprisErrorDialog IsNot Nothing Then
                MyComprisErrorDialog.Close()
                MyComprisErrorDialog = Nothing
            End If

            MyComprisErrorDialog = New ComprisPOSMessage(messageDetail.dialogTitle, posMsg.ToString, messageDetail.documentLink, messageDetail.documentLinkText, terminalList)
            MyComprisErrorDialog.Show()
            MyComprisErrorDialog.Focus()

        End If
    End Sub

    Private Function SavePOSCommandData(ByVal commandData As String, ByVal messageID As Integer) As Boolean
        If (LiveLinkConfiguration.YisEnabled) Then
            Return HandleYISCommandData(commandData, messageID)
        End If

        If (LiveLinkConfiguration.LiveLinkPOSCommandDataEnabled) Then
            Return HandlePOSCommandData(commandData, messageID)
        End If

        Return True
    End Function

    Private Function HandlePOSCommandData(ByVal request As String, ByVal messageID As Integer) As Boolean
        Dim status As Boolean = True
        Dim type As String = "POSCommand"
        Try

            If (String.IsNullOrEmpty(request)) Then
                LogEvent(EventLogEntryType.Error, "Failed to parse POS command data:[{0}]", request)
                Return False
            End If


            ' add the POS Command data to tbPOSCommandData
            Mx.Services.LiveLink.POSCommandService.InsertData(New Mx.BusinessObjects.LiveLink.POSCommandData(Nothing, type, messageID, request, String.Empty, DateTime.Now, DateTime.Now))
            ' delete any old built up data
            If (LiveLinkConfiguration.DaysBeforeObsolete <> 0) Then
                Mx.Services.LiveLink.POSCommandService.DeleteObsoleteData(LiveLinkConfiguration.DaysBeforeObsolete, type)
            End If

        Catch ex As Exception
            LogEvent(EventLogEntryType.Error, "Exception occurred handling POS command data:[{0}]. Exception: {1}", request, ex.ToString)
            status = False
        End Try
        Return status
    End Function

    ' send commands to the POS... SOD, EOD etc.
    Private Function SendPOSCommand(ByVal command As String, ByVal messageID As Integer) As Boolean
        Dim sPOSType As String = LiveLinkConfiguration.POSType.Trim.ToUpper
        SendPOSCommand = True

        Try
            ' check if YIS is enabled
            If (LiveLinkConfiguration.YisEnabled) Then
                ' only do yis commands if YIS enabled
                Return HandleYISCommand(command, messageID)
            End If

            Select Case sPOSType.ToUpper
                Case "COMPRIS"
                    ' COMPRIS is the only POS that supports commands so far
                    Dim ComprisPOS As New ComprisActiveX(MyComprisActiveMeKeySOD, _
                                                            MyComprisActiveMeKeyEOD, _
                                                            MyComprisActiveScriptPreSOD, _
                                                            MyComprisActiveScriptPostSOD, _
                                                            MyComprisActiveScriptPreEOD, _
                                                            MyComprisActiveScriptPostEOD, _
                                                            MyComprisActiveRegisterCount)


                    Dim CmdParameters() As String = command.Split(";")
                    Select Case CmdParameters(0).ToUpper
                        Case "SOD"
                            SetStatus("Message", "Sending SOD to POS")
                            LogEvent("Received SOD command from server. Sending to POS", EventLogEntryType.Information)
                            ' send a start of day to the compris POS
                            '
                            Dim ResultList As Object() = Nothing
                            Dim Result As Integer
                            Dim TerminalNo As Integer = 1
                            Dim Message As String = String.Empty
                            Dim FailedCount As Integer = 0
                            Dim FailedTerminalNo As Integer = 0

                            Try
                                Select Case ComprisPOS.SendOpen(ResultList)
                                    Case ComprisStatusSOD.Success
                                        If ResultList Is Nothing Then
                                            LoadPOSErrorBalloonTip("Failed to send START OF DAY")
                                            LoadPOSErrorDialog(ComprisActiveMessageType.sod, ComprisActiveErrorType.NoResult)
                                            SendPOSCommand = False
                                        Else
                                            Dim errorTerminals As New List(Of Short)
                                            For Each Result In ResultList
                                                If Result = 0 Then
                                                    SetStatus("Message", "Terminal #" & TerminalNo & " - SENT")
                                                Else
                                                    SetStatus("Message", "Terminal #" & TerminalNo & " - FAILED")
                                                    SendPOSCommand = False

                                                    FailedCount += 1
                                                    FailedTerminalNo = TerminalNo
                                                    errorTerminals.Add(CShort(TerminalNo))
                                                End If
                                                TerminalNo += 1
                                            Next

                                            If errorTerminals.Count > 0 Then
                                                LoadPOSErrorDialog(ComprisActiveMessageType.sod, ComprisActiveErrorType.TerminalFailure, errorTerminals)
                                            Else
                                                LogEvent("Successfullly Sent SOD server command to POS", EventLogEntryType.Information)
                                            End If

                                            If FailedCount = 1 Then
                                                LoadPOSErrorBalloonTip("Failed to send START OF DAY to terminal #" & FailedTerminalNo)
                                            ElseIf FailedCount > 1 Then
                                                LoadPOSErrorBalloonTip("Failed to send START OF DAY to multiple terminals")
                                            End If
                                        End If

                                    Case ComprisStatusSOD.DayAlreadyOpen
                                        LoadPOSErrorBalloonTip("Failed to send START OF DAY. Day is already open!")
                                        LoadPOSErrorDialog(ComprisActiveMessageType.sod, ComprisActiveErrorType.DayAlreadyOpen)
                                        SendPOSCommand = True ' No need to throw error.

                                    Case ComprisStatusSOD.NoResponse
                                        LoadPOSErrorBalloonTip("Failed to send START OF DAY. Got failure response.")
                                        LoadPOSErrorDialog(ComprisActiveMessageType.sod, ComprisActiveErrorType.NoResponse)
                                        SendPOSCommand = False

                                    Case Else
                                        LoadPOSErrorBalloonTip("Failed to send START OF DAY")
                                        LoadPOSErrorDialog(ComprisActiveMessageType.sod, ComprisActiveErrorType.Generic)
                                        SendPOSCommand = False
                                End Select

                            Catch ex As Exception
                                LogEvent("Exception: [SendPOSCommand] - (" & sPOSType & ") - " & ex.Message, EventLogEntryType.Error)
                                SetStatus("Message", "SOD Exception - " & ex.Message)
                                LoadPOSErrorBalloonTip("Failed to send START OF DAY.")
                                LoadPOSErrorDialog(ComprisActiveMessageType.sod, ComprisActiveErrorType.Exception)
                                SendPOSCommand = False
                            End Try

                        Case "EOD"
                            SetStatus("Message", "Sending EOD to POS")
                            LogEvent("Received EOD command from server. Sending to POS", EventLogEntryType.Information)
                            '
                            ' send a end of day to the compris POS
                            '
                            ' If the length of the parameter list is <= 1 or parameter value <> "NOCHECK", then check online status by default
                            '
                            Try
                                ' check online status of terminals
                                Dim offlineTerminals As List(Of Short) = ComprisPOS.CheckOnlineStatus()
                                Dim openOrdersExist As Boolean = ComprisPOS.CheckOpenOrders()

                                If (Not LiveLinkConfiguration.ComprisProcessEODIgnoreOpenOrdersOrOfflineTerminals) Then

                                    If CmdParameters.Length <= 1 OrElse CmdParameters(1).ToUpper <> "NOCHECK" Then
                                        ' raise error if terminals are offline
                                        If offlineTerminals IsNot Nothing Then
                                            LogEvent("Info: There are terminals offline. Cannot send EOD", EventLogEntryType.Warning)
                                            LoadPOSErrorBalloonTip("There are terminals offline. Cannot send END OF DAY")
                                            LoadPOSErrorDialog(ComprisActiveMessageType.eod, ComprisActiveErrorType.TerminalsOffline, offlineTerminals)
                                            SendPOSCommand = False
                                            Exit Select
                                        End If
                                    End If

                                    If openOrdersExist Then
                                        LogEvent("Info: There are currently open orders. Cannot send EOD", EventLogEntryType.Warning)
                                        LoadPOSErrorBalloonTip("There are currently open orders. Cannot send END OF DAY")
                                        LoadPOSErrorDialog(ComprisActiveMessageType.eod, ComprisActiveErrorType.OrdersOpen)
                                        SendPOSCommand = False
                                        Exit Select
                                    End If
                                Else
                                    If (offlineTerminals IsNot Nothing OrElse openOrdersExist) Then
                                        Dim terminalList As String = If(offlineTerminals IsNot Nothing, SplitIds(offlineTerminals), String.Empty)
                                        LogEvent(String.Format("EOD completed. But there are currently open orders or terminals offline : {0}", terminalList), EventLogEntryType.Warning)
                                        'Send an ALERT message
                                        Dim entityId As Integer = GetEntityId()
                                        Mx.Services.LiveLink.SalesMainService.InsertAlertMessage(entityId, AlertRecordSubType.Warning, "COMPRIS EOD Process Warning", String.Format("There are {0}Open order(s) and ({1}) offline terminal(s)", If(openOrdersExist, "", "NO "), If(String.IsNullOrEmpty(terminalList), "NO", terminalList)))
                                    End If
                                End If

                                Dim ResultList As Object() = Nothing
                                Dim Result As Integer
                                Dim TerminalNo As Integer = 1
                                Dim FailedCount As Integer = 0
                                Dim FailedTerminalNo As Integer = 0

                                Select Case ComprisPOS.SendClose(ResultList)
                                    Case ComprisStatusEOD.Success
                                        If ResultList Is Nothing Then
                                            LoadPOSErrorBalloonTip("Failed to send END OF DAY")
                                            LoadPOSErrorDialog(ComprisActiveMessageType.eod, ComprisActiveErrorType.NoResult)
                                            SendPOSCommand = False
                                        Else
                                            Dim errorTerminals As New List(Of Short)
                                            For Each Result In ResultList
                                                If Result = 0 Then
                                                    SetStatus("Message", "Terminal #" & TerminalNo & " - SENT")
                                                Else
                                                    SetStatus("Message", "Terminal #" & TerminalNo & " - FAILED")
                                                    SendPOSCommand = False

                                                    FailedCount += 1
                                                    FailedTerminalNo = TerminalNo
                                                    errorTerminals.Add(CShort(TerminalNo))
                                                End If
                                                TerminalNo += 1
                                            Next

                                            If errorTerminals.Count > 0 Then
                                                LoadPOSErrorDialog(ComprisActiveMessageType.eod, ComprisActiveErrorType.TerminalFailure, errorTerminals)
                                            Else
                                                LogEvent("Successfullly Sent EOD server command to POS", EventLogEntryType.Information)
                                            End If

                                            If FailedCount = 1 Then
                                                LoadPOSErrorBalloonTip("Failed to send END OF DAY to terminal #" & FailedTerminalNo)
                                            ElseIf FailedCount > 1 Then
                                                LoadPOSErrorBalloonTip("Failed to send END OF DAY to multiple terminals")
                                            End If
                                        End If

                                    Case ComprisStatusEOD.NoResponse
                                        LoadPOSErrorBalloonTip("Failed to send END OF DAY. Got failure response.")
                                        LoadPOSErrorDialog(ComprisActiveMessageType.eod, ComprisActiveErrorType.NoResponse)
                                        SendPOSCommand = False

                                    Case Else
                                        LoadPOSErrorBalloonTip("Failed to send END OF DAY.")
                                        LoadPOSErrorDialog(ComprisActiveMessageType.eod, ComprisActiveErrorType.Generic)
                                        SendPOSCommand = False
                                End Select

                            Catch ex As Exception
                                LogEvent("Exception: [SendPOSCommand] - (" & sPOSType & ") - " & ex.Message, EventLogEntryType.Error)
                                SetStatus("Message", "EOD Exception - " & ex.Message)
                                LoadPOSErrorBalloonTip("Failed to send END OF DAY.")
                                LoadPOSErrorDialog(ComprisActiveMessageType.eod, ComprisActiveErrorType.Exception)
                                SendPOSCommand = False
                            End Try

                        Case "PDDFILES"
                            ' if PddFiles command received, then force a register of the DLL
                            ComprisInterop.RegisterPddFilesDLL(True)
                    End Select

                Case "IPOS"
                    ' if update flag is set to true, then there is already an update in progress. Do not action another!
                    If (MyiPOSUpdatingiPOSDatabase) Then
                        LogEvent("Error: iPOS database update in progress, cannot action remote update.")
                        Return False
                    End If
                    Try
                        MyiPOSUpdatingiPOSDatabase = True
                        SetEndofDayStatus("Updating iPOS...")

                        ' iPOS updates (e.g. Products/Staff)
                        If (Not iPOSUpdateHelper.ProcessUpdate(command, MyIdentity, mySessionKey, GetEntityId())) Then
                            SendPOSCommand = False
                        End If
                    Catch ex As Exception
                        LogEvent(EventLogEntryType.Error, "Exception: [iPOS Update Command] - ({0}) - {1}", sPOSType, ex.ToString)
                        SendPOSCommand = False
                    Finally
                        MyiPOSUpdatingiPOSDatabase = False
                        SetEndofDayStatus("")
                    End Try
            End Select

            ' check if a ZK fingerprint command is requested
            If LiveLinkConfiguration.ZKFingerprintEnabled Then
                Dim entityId As Integer = GetEntityId()
                If entityId > 0 Then
                    If MyZKFingerprint Is Nothing Then
                        ' initialise the fingerprint device interdface
                        MyZKFingerprint = New zkFingerPrint(entityId, LiveLinkConfiguration.ZKFingerprintIP, LiveLinkConfiguration.ZKFingerprintPort)
                    End If
                    ' attempt to process the command
                    If Not MyZKFingerprint.ProcessCommand(command) Then
                        SendPOSCommand = False
                    End If
                End If

            End If

        Catch ex As Exception
            LogEvent("Exception: [SendPOSCommand] - (" & sPOSType & ") - " & ex.Message, EventLogEntryType.Error)
            SetStatus("Message", "POS Command Error - " & ex.Message)
            SendPOSCommand = False
        End Try
    End Function


#End Region

    Private Function WriteNewConfigSettings(ByVal newConfigs As String) As Boolean
        Try
            ' New configuration settings are returned as name value pairs separated by semicolons
            ' eg.  UseMicros=Yes;Version=;"ServiceTypes=PickUp=1;TakeOut=2" ...
            '      yields (1) Key: UseMicros, Value: Yes
            '             (2) Key: ServiceTypes, Value: Pickup=1;TakeOut=2
            '      Note that ; are ignored when surrounded by quotes
            '      Note also, only the first equals sign is used to split
            Dim newSettings As New List(Of String())

            Using sr As New StringReader(newConfigs)
                Using textParser As New Microsoft.VisualBasic.FileIO.TextFieldParser(sr)
                    textParser.TextFieldType = Microsoft.VisualBasic.FileIO.FieldType.Delimited
                    textParser.Delimiters = New String() {";"}
                    textParser.HasFieldsEnclosedInQuotes = True
                    'Loop through all of the rows in the stream
                    While Not textParser.EndOfData
                        Dim rowFields As String() = textParser.ReadFields()
                        For Each field As String In rowFields
                            Dim nvPair As String() = field.Split(New Char() {"="c}, 2) ' Split on first equals sign only
                            If nvPair.Length = 2 Then
                                ' Store the settings in a list.  This ensures that we only update the settings if the entire string can be properly parsed.
                                newSettings.Add(nvPair)
                            Else
                                Throw New Microsoft.VisualBasic.FileIO.MalformedLineException(String.Format("Unable to split field [{0}]", field))
                            End If
                        Next
                    End While
                End Using
            End Using
            For Each nvPair As String() In newSettings
                LiveLinkConfiguration.SetSetting(nvPair(0), nvPair(1))
            Next
            LiveLinkConfiguration.Save()
            Return True
        Catch ex As Exception
            LogEvent("Error writing new config settings." & vbCrLf & vbCrLf & "Error: " & ex.Message, EventLogEntryType.Warning)
            Return False
        End Try

    End Function

    'Private Sub ResetDatabase()
    '    Dim MyConfig As MXConfigReader.ConfigFile = eRetailerWebServiceConfig()
    '    Dim Use_ODBC As Boolean = Val(MyConfig.GetAppSetting("Use_ODBC_DB_Connect"))
    '    Dim POSType As String = MyConfig.GetAppSetting("POSType").Trim.ToUpper

    '    DropTables(Use_ODBC, POSType)
    '    Select Case POSType
    '        Case "PARJOURNAL", "ALOHA", "SIGNATURE", "PARINFUSION", "MICROS9700", "PCAMERICA", "COMPRIS", "PAN7700", "INTOUCH", "PAREXALT4"
    '            DropTables(Use_ODBC, POSType)

    '        Case "MICROS"
    '            DropTables(Use_ODBC, POSType)
    '    End Select

    '    SetupDatabase()
    'End Sub



    Private Sub btnSysInfo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSysInfo.Click
        TabControl1.SelectedTab = tabStatus
        ExpireLease() ' Force a new lease acquisition
        ProcessAwake(True)
    End Sub

    Private Sub chkSkipData_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSkipData.CheckedChanged
        If chkSkipData.Checked Then
            chkSkipGrind.Checked = True
            chkSkipGrind.Enabled = False
        Else
            chkSkipGrind.Checked = False
            chkSkipGrind.Enabled = True
        End If
    End Sub

    Private Sub chkSkipGrind_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSkipGrind.CheckedChanged

    End Sub


#Region "Easy Clock Load Functions"


    Private Enum EasyClockType
        TimeClock
        UserClock
    End Enum

    Public Function CreateEasyClock() As IEasyClock
        Dim entityID As Integer = GetEntityId()
        Dim type As EasyClockType
        Try
            type = [Enum].Parse(GetType(EasyClockType), LiveLinkConfiguration.TimeClockType)
        Catch ex As Exception
            type = EasyClockType.TimeClock
        End Try

        Dim clock As IEasyClock = Nothing
        Dim UseODBC As Boolean = LiveLinkConfiguration.Use_ODBC_DB_Connect
        Dim POSType As String = LiveLinkConfiguration.POSType

        Select Case type
            Case EasyClockType.UserClock
                ' load the UserClock application
                clock = EasyClock.UserClockView.GetInstance(entityID, POSType, UseODBC, MyIdentity, mySessionKey)

            Case Else
                ' Create an object of TimeClock, which is a Singleton class.
                ' This will only allow one instance of the TimeClock class to be created at a time. It will return 'Nothing' if there is a class already instantiated
                ' TimeClockSingleton is a singleton class that encapsulates the running of the TimeClock form
                ' It enables only one instance of the TimeClock form to be running at any time
                Dim TimeClockInstance As TimeClockSingleton
                Dim EntityName As String = GetEntityName(entityID)

                ' Attempts to get an instance of the time clock.
                ' If instance is obtained, no new instance can be retrieved till TimeClockSingleton.ReleaseInstance is called.
                ' This is called within TimeClockSingleton on exit of TimeClock
                clock = TimeClockSingleton.GetInstance(MyIdentity, mySessionKey, entityID.ToString, EntityName, POSType, UseODBC, MyTimeClockLoginWindow, MyTimeClockAllowBreaks, MyTimeClockAllowNewEmployees, MyLogoImage, MyLogoColor)
        End Select
        ' return the new IEasyClock object
        Return clock
    End Function


    Private Sub LoadTimeClock()
        ' sync local users � moved from within the try/catch below.
        SyncLocalUsers()
        ' create the IEasyClock object
        Dim easyClock As IEasyClock = CreateEasyClock()
        ' Check if instance was created
        If easyClock IsNot Nothing Then
            Try
                ' update the local reference to the time clock
                MyTimeClockInstance = easyClock
                MyTimeClockInstance.AllowClose = MyTimeClockAllowClose
                ' Syncronize the local users before spawning time clock thread
                ' Create a new thread to run the TimeClock form from. This will make it still available when LiveLink is blocking at any point
                Dim TimeClockThread As New Thread(New ThreadStart(AddressOf MyTimeClockInstance.Show))
                TimeClockThread.Name = "EasyClock"
                TimeClockThread.Start()
                Me.WindowState = FormWindowState.Normal
                Me.WindowState = FormWindowState.Minimized
            Catch ex As Exception
                LogEvent("Error creating new thread for time clock." & vbCrLf & vbCrLf & "Error: " & ex.Message, EventLogEntryType.Warning)
            End Try
        Else
            'MessageBox.Show("Time Clock is already running")
        End If
    End Sub


    Private Sub MenuItemRunTimeClock_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemRunTimeClock.Click, MenuItemTrayRunTimeClock.Click
        LoadTimeClock()
    End Sub


#End Region

#Region "YIS Interface"

    Private Function HandleYISCommandData(ByVal request As String, ByVal messageID As Integer) As Boolean
        Dim status As Boolean = True
        Try
            ' parse the command
            Dim commandData As LiveLinkPOSCommandData = LiveLinkPOSCommandData.Parse(request)
            If (commandData Is Nothing) Then
                LogEvent(EventLogEntryType.Error, "Failed to parse YIS command data:[{0}]", request)
                Return False
            End If

            ' TODO : possible check if it exists first (by Type & ApplyDate)

            ' add the POS Command data to tbPOSCommandData
            Mx.Services.LiveLink.POSCommandService.InsertData(New Mx.BusinessObjects.LiveLink.POSCommandData(Nothing, commandData.Type, commandData.Options, commandData.CData, commandData.FilePath, commandData.ApplyDate, DateTime.Now))
            ' delete any old built up data
            If (LiveLinkConfiguration.DaysBeforeObsolete <> 0) Then
                Mx.Services.LiveLink.POSCommandService.DeleteObsoleteData(LiveLinkConfiguration.DaysBeforeObsolete, commandData.Type)
            End If

        Catch ex As Exception
            LogEvent(EventLogEntryType.Error, "Exception occurred handling YIS command data:[{0}]. Exception: {1}", request, ex.ToString)
            status = False
        End Try
        Return status
    End Function

    Private Function HandleYISCommand(ByVal request As String, ByVal messageID As Integer) As Boolean
        Dim status As Boolean = True
        Try
            ' parse the command
            Dim yisCommand As LiveLinkPOSCommand = LiveLinkPOSCommand.Parse(request)
            If (yisCommand Is Nothing) Then
                LogEvent(EventLogEntryType.Error, "Failed to parse YIS command:[{0}]", request)
                Return False
            End If

            ' create the YIS command message and pass in any parameters
            Dim message As Producer.POSControlMessage = New Producer.POSControlMessage(LiveLinkConfiguration.YisDMCServerPort, yisCommand.CommandType, MxCommandSource.Online, messageID, yisCommand.MxParameterCollection)
            ' check message was created successfully
            If (message Is Nothing) Then
                LogEvent(EventLogEntryType.Error, "YIS Message not initialised")
                Return False
            End If

            ' send the message to YIS
            message.SendMessage()
        Catch ex As Exception
            LogEvent(EventLogEntryType.Error, "Exception occurred handling YIS command:[{0}]. Exception: {1}", request, ex.ToString)
            status = False
        End Try
        Return status
    End Function

    Private Function StartYISConsumers() As Boolean
        ' check YIS is enabled
        If (Not LiveLinkConfiguration.YisEnabled) Then
            Return True
        End If

        Dim status As Boolean = True

        ' POS Control consumer
        Try
            ' instantiate the new POSControl consumer
            If (yisConsumerPOSControl Is Nothing) Then
                yisConsumerPOSControl = New Mx.YIS.Consumer.POSControlResponse(LiveLinkConfiguration.YisDMCServerPort, LiveLinkConfiguration.YisConsumerPOSControlPort, Consumer.DMC.YisConsumerMessageClass.POSControlResponseMessage)
            End If

            ' start the POS Control consumer
            yisConsumerPOSControl.Start()
        Catch ex As Exception
            LogEvent(EventLogEntryType.Error, "Exception occurred starting the POS Control YIS consumer. Exception: {0}", ex.ToString)
            status = False
        End Try
        ' RDS display consumer
        Try
            ' instantiate the new RDSDisplay consumer
            If (yisConsumerRDSDisplay Is Nothing) Then
                yisConsumerRDSDisplay = New Mx.YIS.Consumer.POSControlResponse(LiveLinkConfiguration.YisDMCServerPort, LiveLinkConfiguration.YisConsumerRDSDisplayPort, Consumer.DMC.YisConsumerMessageClass.RDSDisplayResponseMessage)
            End If

            ' start the RDS Display consumer
            yisConsumerRDSDisplay.Start()
        Catch ex As Exception
            LogEvent(EventLogEntryType.Error, "Exception occurred starting the RDS Display YIS consumer. Exception: {0}", ex.ToString)
            status = False
        End Try
        ' HME Timer consumer
        Try
            ' instantiate the new HMETimer consumer
            If (yisConsumerHMETimer Is Nothing) Then
                yisConsumerHMETimer = New Mx.YIS.Consumer.SOSTimer(LiveLinkConfiguration.YisDMCServerPort, LiveLinkConfiguration.YisConsumerHMETimerPort, Consumer.DMC.YisConsumerMessageClass.HMESOS)
            End If

            ' start the HME Timer consumer
            yisConsumerHMETimer.Start()
        Catch ex As Exception
            LogEvent(EventLogEntryType.Error, "Exception occurred starting the HME Timer YIS consumer. Exception: {0}", ex.ToString)
            'status = False
        End Try
        ' QSR Timer consumer
        Try
            ' instantiate the new QSRTimer consumer
            If (yisConsumerQSRTimer Is Nothing) Then
                yisConsumerQSRTimer = New Mx.YIS.Consumer.SOSTimer(LiveLinkConfiguration.YisDMCServerPort, LiveLinkConfiguration.YisConsumerQSRTimerPort, Consumer.DMC.YisConsumerMessageClass.QSRSOS)
            End If

            ' start the QSR Timer consumer
            yisConsumerQSRTimer.Start()
        Catch ex As Exception
            LogEvent(EventLogEntryType.Error, "Exception occurred starting the QSR Timer YIS consumer. Exception: {0}", ex.ToString)
            'status = False
        End Try

        ' set the buttons to load the offline form to enabled
        If (status) Then
            MenuItemYISController.Enabled = True
            MenuItemTrayYISController.Enabled = True
        End If
        ' return result
        Return status
    End Function

    Private Sub StopYISConsumers()
        If (Not LiveLinkConfiguration.YisEnabled) Then
            MenuItemYISController.Enabled = False
            MenuItemTrayYISController.Enabled = False
            Return
        End If
        ' stop the POS Control YIS consumer
        Try
            If (yisConsumerPOSControl IsNot Nothing) Then
                yisConsumerPOSControl.Stop()
            End If
        Catch ex As Exception
            LogEvent(EventLogEntryType.Error, "Exception occurred stopping POS Control YIS consumer. Exception: {0}", ex.ToString)
        End Try
        ' stop the RDS Display YIS consumer
        Try
            If (yisConsumerRDSDisplay IsNot Nothing) Then
                yisConsumerRDSDisplay.Stop()
            End If
        Catch ex As Exception
            LogEvent(EventLogEntryType.Error, "Exception occurred stopping RDS Display YIS consumer. Exception: {0}", ex.ToString)
        End Try
        ' stop the HME Timer YIS consumer
        Try
            If (yisConsumerHMETimer IsNot Nothing) Then
                yisConsumerHMETimer.Stop()
            End If
        Catch ex As Exception
            LogEvent(EventLogEntryType.Error, "Exception occurred stopping HME Timer YIS consumer. Exception: {0}", ex.ToString)
        End Try
        ' stop the QSR Timer YIS consumer
        Try
            If (yisConsumerQSRTimer IsNot Nothing) Then
                yisConsumerQSRTimer.Stop()
            End If
        Catch ex As Exception
            LogEvent(EventLogEntryType.Error, "Exception occurred stopping QSR Timer YIS consumer. Exception: {0}", ex.ToString)
        End Try

        ' set the buttons to disabled
        MenuItemYISController.Enabled = False
        MenuItemTrayYISController.Enabled = False
    End Sub

    Private Sub KeepAliveYIS()
        If (Not LiveLinkConfiguration.YisEnabled) Then
            Return
        End If
        ' ensure the POS Control YIS consumer is kept alive
        Try
            If (yisConsumerPOSControl IsNot Nothing) Then
                yisConsumerPOSControl.KeepAlive()
            End If
        Catch ex As Exception
            LogEvent(EventLogEntryType.Error, "Exception occurred keeping alive the POS Control YIS consumer. Exception: {0}", ex.ToString)
        End Try
        ' ensure the RDS Display YIS consumer is kept alive
        Try
            If (yisConsumerRDSDisplay IsNot Nothing) Then
                yisConsumerRDSDisplay.KeepAlive()
            End If
        Catch ex As Exception
            LogEvent(EventLogEntryType.Error, "Exception occurred keeping alive the RDS Display YIS consumer. Exception: {0}", ex.ToString)
        End Try
    End Sub


    ' YIS offline application

    Private Sub MenuItemTrayYISController_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemYISController.Click, MenuItemTrayYISController.Click
        If (Not LiveLinkConfiguration.YisEnabled) Then
            Return
        End If

        Try
            LogEvent(EventLogEntryType.Information, "Loading YIS Controller")
            ' instantiate YIS controller object
            Dim localYISControl As YISController = New YISController(yisConsumerPOSControl, yisConsumerRDSDisplay, LiveLinkConfiguration.YisDMCServerPort, _
                                                                     LiveLinkConfiguration.AccessCode, LiveLinkConfiguration.AccessCodeCaseSensitive, _
                                                                     LiveLinkConfiguration.YisDayOpenTimeout, LiveLinkConfiguration.YisDayCloseTimeout, _
                                                                     LiveLinkConfiguration.YisEmployeeUpdateTimeout)

            ' if instantiated successfully, then store global reference to this class
            yisControl = localYISControl
            ' create thread for yis application
            yisThread = New Thread(AddressOf RunYISController)
            yisThread.SetApartmentState(ApartmentState.STA)
            yisThread.Start()
            ' set local windowstate to minimised
            Me.WindowState = FormWindowState.Normal
            Me.WindowState = FormWindowState.Minimized
        Catch ex As Exception
            LogEvent(EventLogEntryType.Error, "Exception occurred creating YIS controller application. Exception: {0}", ex.ToString)
        End Try

    End Sub

    ' YIS Controller run function (called in new thread)
    Private Sub RunYISController()
        Dim mutexOwner As Boolean = False
        Dim yisMutex As Mutex

        Try
            yisMutex = YISController.GetMutexLock(mutexOwner)
            If (mutexOwner AndAlso yisMutex IsNot Nothing) Then
                Application.Run(yisControl)
                LogEvent(EventLogEntryType.Information, "YIS Controller closed")
            Else
                LogEvent(EventLogEntryType.Warning, "YIS Controller is already open")
            End If
        Catch ex As Exception
            LogEvent(EventLogEntryType.Error, "Exception occurred in YIS Controller applciation. Exception: {0}", ex.ToString)
        Finally
            yisControl.Dispose()
            yisControl = Nothing
            YISController.ReleaseMutexLock(mutexOwner, yisMutex)
        End Try

    End Sub



#End Region

    Private Function GetEntityName(ByVal EntityID As String) As String
        Dim MyMessage As New LiveLinkWebService
        Dim MyLiveLinkResponse As Mx.POS.Common.eRetailer.LiveLinkResponse
        Dim EntityName As String = String.Empty

        MyMessage.Url = myWebService
        LoadProxy(MyMessage)

        GetLease()
        If mySessionKey <> String.Empty Then
            Try
                MyLiveLinkResponse = MyMessage.GetStore(MyIdentity, mySessionKey, GetEntityId())
                If MyLiveLinkResponse.ResponseCode = Mx.POS.Common.eRetailer.LiveLinkResponseCode.Success Then
                    If MyLiveLinkResponse.ResponseData.Tables("EntityStore").Rows.Count > 0 Then
                        EntityName = EvalNull(MyLiveLinkResponse.ResponseData.Tables("EntityStore").Rows(0).Item("Entity"), "")
                    End If
                End If
            Catch ex As Exception
            End Try
        End If
        Return EntityName
    End Function

    Private Function EncodeSQL(ByVal StringToFix As String) As String
        Return StringToFix.Replace("'", "''")
    End Function

    Private Sub RestartLiveLink()
        LogEvent(EventLogEntryType.Information, "LiveLink is restarting ...")
        Application.Restart()
    End Sub

    Private Sub btnReceiveMessage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReceiveMessage.Click
        TabControl1.SelectedTab = tabStatus
        RetrieveMessage()
    End Sub

    Private Sub chkAutoStartOnLogin_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkAutoStartOnLogin.CheckedChanged
        EnsureLiveLinkAutoStarts(chkAutoStartOnLogin.Checked)
        LiveLinkConfiguration.AutoStartOnLogin = chkAutoStartOnLogin.Checked
        LiveLinkConfiguration.Save()
    End Sub

    Private Sub cmdRefreshSettings_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdRefreshSettings.Click
        TabControl1.SelectedTab = tabStatus
        RetrieveOfflineCashupSettings()
    End Sub

    Private Sub tmrPing_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmrPing.Tick
        Try
            tmrPing.Enabled = False
            Dim pingService As New LiveLinkWebService

            pingService.Url = myWebService
            LoadProxy(pingService)
            pingService.Timeout = 10000 ' 10 Seconds
            If (LiveLinkConfiguration.PingEnhancedEnabled) Then
                ' if enabled, get the enhanced ping response
                Dim response As Mx.POS.Common.eRetailer.EnhancedPingResponse = pingService.PingEnhanced()
                ' check for valid response
                If (response IsNot Nothing) Then
                    ' update the "Status" flag, with the back
                    Mx.Services.LiveLink.StoreInfoService.UpdateStatus(response.QueueSizeExceeded)
                End If
            Else
                pingService.Ping()
            End If
            If PictureBoxPingEnabled.Tag = "Ping1" Then
                PictureBoxPingEnabled.Image = My.Resources.Ping2
                PictureBoxPingEnabled.Tag = "Ping2"
            Else
                PictureBoxPingEnabled.Image = My.Resources.Ping1
                PictureBoxPingEnabled.Tag = "Ping1"
            End If

            If (LiveLinkConfiguration.UseOfflineCashup AndAlso Not _isConnected) Then
                Dim storeInfo As StoreInfo = StoreInfoService.GetStoreInfo()
                If (storeInfo IsNot Nothing) Then
                    Dim details As String = String.Format("LiveLink established connectivity to webserver. Previous valid connection at: {0}", storeInfo.LastAwake)
                    GenerateAuditAndSync(AuditItem.AuditEvent.LiveLinkConnectionAvailable, "LiveLink Connection Established", details)
                End If
                _isConnected = True
            End If

            SuccessfulCommunications()
        Catch ex As Exception
            If PictureBoxPingEnabled.Tag = "ErrorPing1" Then
                PictureBoxPingEnabled.Image = My.Resources.PingFailed2
                PictureBoxPingEnabled.Tag = "ErrorPing2"
            Else
                PictureBoxPingEnabled.Image = My.Resources.PingFailed1
                PictureBoxPingEnabled.Tag = "ErrorPing1"
            End If

            If (LiveLinkConfiguration.UseOfflineCashup AndAlso _isConnected) Then
                Dim details As String = "LiveLink lost connectivity to webserver."
                GenerateAuditAndSync(AuditItem.AuditEvent.LiveLinkConnectionUnavailable, "LiveLink Connection Lost", details)
                _isConnected = False
            End If

            LogEvent(EventLogEntryType.Warning, "Error checking connectivity to webserver. Exception: {0}", ex.ToString)
        Finally
            tmrPing.Enabled = True
        End Try
    End Sub

    Private Enum ProxyAuth
        <EnumDescription("None")> None
        <EnumDescription("Windows Current User")> Windows_CurrentUser
        <EnumDescription("NTLM")> NTLM
        <EnumDescription("Digest")> Digest
        <EnumDescription("Basic")> Basic
    End Enum

    Private Sub cboProxyAuth_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboProxyAuth.SelectedIndexChanged
        Select Case DirectCast(cboProxyAuth.SelectedItem, ComboItem).Value
            Case ProxyAuth.None, ProxyAuth.Windows_CurrentUser
                grpProxyAuth.Enabled = False
            Case Else
                grpProxyAuth.Enabled = True
        End Select
    End Sub

    Private Sub chkUseProxy_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkUseProxy.CheckedChanged
        grpProxy.Enabled = chkUseProxy.Checked
    End Sub

#Region "ComboItem Class"

    Private Class ComboItem

        Private _value As ProxyAuth
        Private _text As String

        Public Property Value() As ProxyAuth
            Get
                Return _value
            End Get
            Set(ByVal value As ProxyAuth)
                _value = value
            End Set
        End Property


        Public Property Text() As String
            Get
                Return _text
            End Get
            Set(ByVal value As String)
                _text = value
            End Set
        End Property

        Public Sub New(ByVal value As ProxyAuth, ByVal text As String)
            _value = value
            _text = text
        End Sub

        Public Overrides Function ToString() As String
            Return _text
        End Function
    End Class

#End Region

#Region "LiveLink Auto-Activation Process"


    Private Sub AutoActivateLiveLink()
        ' check if auto-activation is enabled
        If (Not LiveLinkConfiguration.AutoActivationEnabled) Then
            Return
        End If
        Try
            ' check if store details were supplied
            If (String.IsNullOrEmpty(_argumentStoreNumber)) Then
                ' no store number supplied from the command line, so do not activate
                Return
            End If

            If (String.IsNullOrEmpty(MyIdentity)) Then
                ' ensure that the LiveLinkID is retrieved before proceeding
                RefreshLiveLinkID()
            End If

            ' get a lease from the server before attempting activation
            GetLease()

            Dim sessionKey As String = String.Empty
            ' check if a conditional or full lease has been granted
            ' (this ensures the LiveLink setup is valid and communicating with the server correctly)
            If (HasConditionalLease()) Then
                sessionKey = myConditionalSessionKey
            ElseIf (Is_OK_To_Send()) Then
                sessionKey = mySessionKey
            Else
                ' not allowed to send with a conditional lease or valid lease
                LogEvent(EventLogEntryType.Warning, "Auto-Activation: Failed to get lease for Auto-Activation of LiveLink")
                SetStatus("Error", "Activation failed. Lease error")
                Return
            End If

            ' create webservice
            Dim webService As New LiveLinkWebService
            Dim response As Mx.POS.Common.eRetailer.LiveLinkResponse = Nothing
            Dim entityList As SimpleEntityList = Nothing

            ' setup webservice URL and proxy
            webService.Url = LiveLinkConfiguration.WebService_URL
            webService.Proxy = ConfigProxy()

            ' get the list of entities for the supplied store number
            response = webService.ValidateStoreNumber(MyIdentity, sessionKey, _argumentStoreNumber)
            ' check response code from request
            Select Case (response.ResponseCode)
                Case eRetailer.LiveLinkResponseCode.Success
                    ' valid result returned to get entity information (de-serialise the result list)
                    entityList = entityList.Deserialise(response.ResponseCompressedData)
                    Exit Select

                Case eRetailer.LiveLinkResponseCode.InvalidConfiguration
                    If (String.IsNullOrEmpty(response.ResponseString)) Then
                        LogEvent(EventLogEntryType.Error, "Auto-Activation: Invalid configuration response when requesting Entity for StoreNumber:[{0}]", _argumentStoreNumber)
                        SetStatus("Error", "Activation failed. Invalid Configuration")
                    Else
                        LogEvent(EventLogEntryType.Error, "Auto-Activation: Invalid configuration response when requesting Entity for StoreNumber:[{0}]. Error: {1}", _argumentStoreNumber, response.ResponseString)
                        SetStatus("Error", String.Format("Activation failed. {0}", response.ResponseString))
                    End If
                    Return
                Case eRetailer.LiveLinkResponseCode.Backoff
                    LogEvent(EventLogEntryType.Error, "Auto-Activation: BackOff response returned when requesting Entity for StoreNumber:[{0}]", _argumentStoreNumber)
                    Return
                Case eRetailer.LiveLinkResponseCode.Offline
                    LogEvent(EventLogEntryType.Error, "Auto-Activation: Offline response returned when requesting Entity for StoreNumber:[{0}]", _argumentStoreNumber)
                    Return
                Case Else
                    If (String.IsNullOrEmpty(response.ResponseString)) Then
                        LogEvent(EventLogEntryType.Error, "Auto-Activation: Unhandled response:[{0}] for requesting Entity for StoreNumber:[{1}]", response.ResponseCode, _argumentStoreNumber)
                        SetStatus("Error", String.Format("Activation failed. Unhandled response: {0}", response.ResponseCode))
                    Else
                        LogEvent(EventLogEntryType.Error, "Auto-Activation: Unhandled response:[{0}] for requesting Entity for StoreNumber:[{1}]. Error: {2}", response.ResponseCode, _argumentStoreNumber, response.ResponseString)
                        SetStatus("Error", String.Format("Activation failed. {0}", response.ResponseString))
                    End If
                    Return
            End Select

            ' check valid entityList is available
            If (entityList Is Nothing OrElse entityList.Count <= 0) Then
                LogEvent(EventLogEntryType.Error, "Auto-Activation: No entities returned for StoreNumber:[{0}]. Activation failed!", _argumentStoreNumber)
                SetStatus("Error", String.Format("Activation failed. No entities for Store #{0}", _argumentStoreNumber))
                Return
            End If

            ' check that only 1 entity was returned (anymore than 1 results in ambiguous StoreNumber)
            If (entityList.Count > 1) Then
                LogEvent(EventLogEntryType.Error, "Auto-Activation: Ambiguous StoreNumber:[{0}]. More than one entity returned (total of {0}). Activation failed!", _argumentStoreNumber, entityList.Count)
                SetStatus("Error", String.Format("Activation failed. Ambiguous Store #{0}", _argumentStoreNumber))
                Return
            End If

            ' use the supplied EntityID
            Dim currentEntityID As Long = GetEntityId()
            Dim entityID As Long = entityList(0).EntityID

            ' check if there is a currently saved entityID and if it differs from the new one being setup
            If (currentEntityID <= 0) Then
                ' EntityID is not currently set, so this is a new install
            ElseIf (currentEntityID <> entityID) Then
                ' There is already a saved EntityID, switching store to new EntityID (log information)
                LogEvent(EventLogEntryType.Warning, "Auto-Activation: Saved EntityID ({0}) is being changed to new EntityID ({1}) for Store #{2}", currentEntityID, entityID, _argumentStoreNumber)
                SetStatus("Warning", "EntityID for LiveLink activation has changed")
            End If
            ' save the EntityID in tbStoreInfo
            SaveEntityId(entityID)

            ' call webservice to Auto-Activate the LiveLink store
            response = webService.AutoActivateStore(MyIdentity, sessionKey, entityID)

            ' check response code from request
            Select Case (response.ResponseCode)
                Case eRetailer.LiveLinkResponseCode.Success
                    ' Auto-Activation succeeded! log success event
                    LogEvent(EventLogEntryType.Information, "Auto-Activation: Successfully activated StoreNumber:[{0}] for EntityID:[{1}]", _argumentStoreNumber, entityID)
                    SetStatus("", String.Format("Activation succeeded for Store #{0}", _argumentStoreNumber))
                    Exit Select

                Case eRetailer.LiveLinkResponseCode.InvalidConfiguration
                    If (String.IsNullOrEmpty(response.ResponseString)) Then
                        LogEvent(EventLogEntryType.Error, "Auto-Activation: Invalid configuration response when requesting activation of StoreNumber:[{0}], EntityID:[{1}]", _argumentStoreNumber, entityID)
                        SetStatus("Error", "Activation failed. Invalid Configuration")
                    Else
                        LogEvent(EventLogEntryType.Error, "Auto-Activation: Invalid configuration response when requesting activation of StoreNumber:[{0}], EntityID:[{1}]. Error: {2}", _argumentStoreNumber, entityID, response.ResponseString)
                        SetStatus("Error", String.Format("Activation failed. {0}", response.ResponseString))
                    End If
                    Return
                Case eRetailer.LiveLinkResponseCode.Backoff
                    LogEvent(EventLogEntryType.Error, "Auto-Activation: Backoff response when requesting activation of StoreNumber:[{0}], EntityID:[{1}]", _argumentStoreNumber, entityID)
                    Return
                Case eRetailer.LiveLinkResponseCode.Offline
                    LogEvent(EventLogEntryType.Error, "Auto-Activation: Offline response when requesting activation of StoreNumber:[{0}], EntityID:[{1}]", _argumentStoreNumber, entityID)
                    Return
                Case Else
                    If (String.IsNullOrEmpty(response.ResponseString)) Then
                        LogEvent(EventLogEntryType.Error, "Auto-Activation: Unhandled response:[{0}] when requesting activation of StoreNumber:[{1}], EntityID:[{2}]", response.ResponseCode, _argumentStoreNumber, entityID)
                        SetStatus("Error", String.Format("Activation failed. Unhandled response: {0}", response.ResponseCode))
                    Else
                        LogEvent(EventLogEntryType.Error, "Auto-Activation: Unhandled response:[{0}] when requesting activation of StoreNumber:[{1}], EntityID:[{2}]. Error: {3}", response.ResponseCode, _argumentStoreNumber, entityID, response.ResponseString)
                        SetStatus("Error", String.Format("Activation failed. {0}", response.ResponseString))
                    End If
                    Return
            End Select

        Catch ex As Exception
            ' log an event on exception
            LogEvent(EventLogEntryType.Error, "Exception occurred auto-activating LiveLink. Exception: {0}", ex.ToString)
            SetStatus("Error", String.Format("Activation failed. Exception: {0}", ex.Message))
        End Try
    End Sub


#End Region

    Private Sub SetupProxyTab()
        If cboProxyAuth.Items.Count = 0 Then
            Dim authTypes As New Dictionary(Of ProxyAuth, ComboItem)
            For Each authType As ProxyAuth In [Enum].GetValues(GetType(ProxyAuth))
                Dim authComboItem As New ComboItem(authType, authType.ToString)
                authTypes.Add(authType, authComboItem)
                'cboProxyAuth.Items.Add(New ComboItem(authType, EnumUtils(Of ProxyAuth).GetDescription(authType))) 'TODO: Fix localisation so that this works in LiveLink 
                cboProxyAuth.Items.Add(authComboItem)
            Next
            cboProxyAuth.SelectedItem = authTypes(ProxyAuth.Windows_CurrentUser)
            chkUseProxy.Checked = LiveLinkConfiguration.WebService_Proxy.ToUpper <> "NONE"
            If (LiveLinkConfiguration.WebService_Proxy.ToUpper = "USE_IE_PROXY") Or (LiveLinkConfiguration.WebService_Proxy = "") Then
                txtCustomProxy.Text = ""
                radUseIEProxy.Checked = True
            Else
                If LiveLinkConfiguration.WebService_Proxy = "NONE" Then
                    txtCustomProxy.Text = ""
                Else
                    txtCustomProxy.Text = LiveLinkConfiguration.WebService_Proxy
                End If
                radCustomProxy.Checked = True
            End If
            If LiveLinkConfiguration.WebService_Proxy_Auth <> String.Empty Then
                cboProxyAuth.SelectedItem = authTypes([Enum].Parse(GetType(ProxyAuth), LiveLinkConfiguration.WebService_Proxy_Auth.Replace("-", "_"), True))
            End If
            txtProxyUserName.Text = LiveLinkConfiguration.WebService_Proxy_User
            txtProxyDomain.Text = LiveLinkConfiguration.WebService_Proxy_Domain
            If LiveLinkConfiguration.WebService_Proxy_Password <> String.Empty Then
                txtProxyPassword.Text = DateTime.UtcNow.ToString("dd-MMM-yyyy HH:mm:ss") ' Random string
            Else
                txtProxyPassword.Text = String.Empty
            End If
        End If
        Dim webProxy As New WebProxy
        txtSystemProxy.Text = If(webProxy.GetDefaultProxy.Address Is Nothing, String.Empty, webProxy.GetDefaultProxy.Address.OriginalString)
    End Sub

    Private Sub radCustomProxy_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radCustomProxy.CheckedChanged
        txtCustomProxy.Enabled = radCustomProxy.Checked
    End Sub

    Private Sub btnSaveProxySettings_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveProxySettings.Click
        If MsgBox("Are you sure that you wish to save these proxy settings?", MsgBoxStyle.YesNo + MsgBoxStyle.Question, "Save proxy settings") = MsgBoxResult.Yes Then
            If Not chkUseProxy.Checked Then
                LiveLinkConfiguration.WebService_Proxy = "NONE"
            Else
                If radUseIEProxy.Checked Then
                    LiveLinkConfiguration.WebService_Proxy = "USE_IE_PROXY"
                Else
                    LiveLinkConfiguration.WebService_Proxy = txtCustomProxy.Text.Trim
                End If
            End If
            LiveLinkConfiguration.WebService_Proxy_Auth = DirectCast(cboProxyAuth.SelectedItem, ComboItem).Value.ToString.Replace("_", "-")
            LiveLinkConfiguration.WebService_Proxy_User = txtProxyUserName.Text.Trim
            LiveLinkConfiguration.WebService_Proxy_Domain = txtProxyDomain.Text.Trim
            If txtProxyPassword.Text <> String.Empty Then
                LiveLinkConfiguration.WebService_Proxy_Password = EncryptString(txtProxyPassword.Text, LiveLinkConfiguration.ClientCode, LiveLinkConfiguration.SecurityCode)
            Else
                LiveLinkConfiguration.WebService_Proxy_Password = ""
            End If
            LiveLinkConfiguration.Save()
        End If
    End Sub

    Private Sub txtProxyPassword_Enter(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtProxyPassword.Enter
        txtProxyPassword.Text = String.Empty
    End Sub

    Private Sub MenuItemZKViewUsers_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItemZKViewUsers.Click, MenuItemTrayZKViewUsers.Click
        Try
            ' Create an object of the zkUsersView singleton class
            Dim usersView As zkUsersView = zkUsersView.GetInstance()
            ' Check if instance was created
            If (usersView IsNot Nothing) Then
                ' update the local reference to the ZK Users form
                MyZKUsersView = usersView
                Dim usersViewThread As New Thread(New System.Threading.ThreadStart(AddressOf MyZKUsersView.Show))
                usersViewThread.Name = "ZKUsersView"
                usersViewThread.Start()
                Me.WindowState = FormWindowState.Normal
                Me.WindowState = FormWindowState.Minimized
            End If
        Catch ex As Exception
            LogEvent(EventLogEntryType.Warning, "Error creating new thread for ZK users form. Exception: {0}", ex.ToString)
        End Try
    End Sub

    Private Sub StartZKServer()
        ' check zk server is enabled
        If (Not LiveLinkConfiguration.ZKFingerprintAuthServerEnabled) Then
            Exit Sub
        End If

        Try
            ' get a lease prior to initialise, so the session key is valid
            GetLease()

            ' only start if server is nothing, or current state is stopped
            If (MyZKServer Is Nothing) Then
                MyZKServer = New zkCommunications.zkServer(MyIdentity, mySessionKey, GetEntityId(), LiveLinkConfiguration.ZKFingerprintAuthServerPort)
            End If

            If (MyZKServer.GetState = zkCommunications.zkServer.ServerState.Stopped) Then
                ' re-initialise the session key (in case original setup didn't have a valid lease)
                MyZKServer.SessionKey = mySessionKey
                ' start thread
                MyZKThread = New Thread(New ThreadStart(AddressOf MyZKServer.StartServer))
                MyZKThread.Name = "ZKServer"
                MyZKThread.Start()
            End If

        Catch ex As Exception
            LogEvent(EventLogEntryType.Warning, "Error creating new thread for ZK validation server. Exception: {0}", ex.ToString)
        End Try
    End Sub

    Private Sub StopZKServer()
        ' check zk server is enabled
        If (Not LiveLinkConfiguration.ZKFingerprintAuthServerEnabled) Then
            Exit Sub
        End If

        Try
            ' if server is not null, then stop it
            If (MyZKServer IsNot Nothing) Then
                MyZKServer.StopServer()
            End If
        Catch ex As Exception
            LogEvent(EventLogEntryType.Warning, "Error stopping ZK validation server. Exception: {0}", ex.ToString)
        End Try
    End Sub

    Private Sub ButtonZKUpdateFirmware_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonZKUpdateFirmware.Click
        UpdateZKFirmware()
    End Sub

    Private Sub UpdateZKFirmware(Optional ByVal fileName As String = "main.gz", Optional ByVal rebootDevice As Boolean = True)
        ' check zk server is enabled
        If (Not LiveLinkConfiguration.ZKFingerprintAuthServerEnabled) Then
            Exit Sub
        End If

        Try
            ButtonZKUpdateFirmware.Enabled = False

            If (MyZKServer Is Nothing) Then
                textZKAuthServerStatus.Text = "Not initialised"
                Exit Sub
            End If


            Dim zkState As zkCommunications.zkServer.ServerState
            zkState = MyZKServer.GetState()

            Select Case (zkState)
                Case zkCommunications.zkServer.ServerState.Stopped, zkCommunications.zkServer.ServerState.Stopping
                    textZKAuthServerStatus.Text = zkState.ToString()
                    Exit Sub
                Case zkCommunications.zkServer.ServerState.WaitingForConnection
                    textZKAuthServerStatus.Text = "Waiting for device..."
                    Exit Sub
            End Select

            textZKAuthServerStatus.Text = "Connected..."
            Application.DoEvents()

            If (MyZKServer IsNot Nothing) Then
                ' update the zk devices firmware
                MyZKServer.UpdateFile(fileName)
                ' check the fingerprint processing object has been created
                If (MyZKFingerprint Is Nothing) Then
                    MyZKFingerprint = New zkFingerPrint(GetEntityId(), LiveLinkConfiguration.ZKFingerprintIP, LiveLinkConfiguration.ZKFingerprintPort)
                End If

                Const timeoutMilliseconds As Integer = 200
                Const maxRetries As Integer = 300   ' 300 retries of 200 millisecond interval. Will cause a maximum wait of 1 minute..
                Dim completed As Boolean = False
                Dim retries As Integer = 0

                While (Not completed And (retries < maxRetries))
                    If (MyZKServer.UpdateCompleted(timeoutMilliseconds)) Then
                        completed = True
                        textZKAuthServerStatus.Text = "Refreshing device data..."
                        Application.DoEvents()
                        ' call a data refresh on the device after the update (this will force a sync of the filesystem)
                        MyZKFingerprint.ExecuteRequest(zkFingerPrint.RequestType.RefreshData)
                        '' restart the device so that all the changes will take effect
                        If (rebootDevice) Then
                            ' wait for 10 Seconds (100 * 100 millisecond sleeps) before initialising device restart
                            ' will give the device time to sync file system changes
                            For syncWaitCount As Integer = 0 To 100
                                Thread.CurrentThread.Sleep(100)
                                Application.DoEvents()
                            Next
                            textZKAuthServerStatus.Text = "Rebooting device..."
                            Application.DoEvents()
                            ' execute restart request
                            MyZKFingerprint.ExecuteRequest(zkFingerPrint.RequestType.RestartDevice)
                        End If
                    End If
                    ' do any events in between wait
                    Application.DoEvents()
                    retries += 1
                End While

                If (Not completed) Then
                    LogEvent(EventLogEntryType.Error, "Error getting ZK update completion response. Forcing filesystem sync, but no reboot")
                    textZKAuthServerStatus.Text = "Refreshing..."
                    Application.DoEvents()
                    MyZKFingerprint.ExecuteRequest(zkFingerPrint.RequestType.RefreshData)
                End If

                ' after update or failed update, set status back to waiting for device
                textZKAuthServerStatus.Text = "Waiting for device..."
                Application.DoEvents()
            End If
        Catch ex As Exception
            LogEvent(EventLogEntryType.Warning, "Error updating file through ZK auth server (File:{0}). Exception: {1}", fileName, ex.ToString)
        Finally
            ButtonZKUpdateFirmware.Enabled = True
        End Try
    End Sub

    Private Sub minimiseTimer_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles minimiseTimer.Tick
        Me.WindowState = FormWindowState.Minimized
        Me.Show()
        minimiseTimer.Enabled = False
    End Sub

    Private Sub GenerateAuditAndSync(ByVal auditEvent As AuditItem.AuditEvent, ByVal title As String, ByVal details As String)
        Dim audit As AuditItem = AuditService.GetStandardAuditItem( _
                AuditItem.AuditCategory.OfflineClient, _
                auditEvent, _
                title, _
                details, _
                StoreInfoService.GetStoreInfo().StoreID _
            )
        AuditService.Audit(New AuditableItem(audit))

        OfflineServiceClient.InsertSyncRecordNoSession(OfflineSyncType.AuditMessage, audit.AuditID, audit.EventEntityID)
    End Sub

End Class
