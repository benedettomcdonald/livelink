﻿using System;
using System.Collections.Generic;
using Mx.POS.ACTAtek;
using Mx.POS.Common.Base;
using Mx.POS.Common.BusinessObjects;
using Mx.POS.Common.Interfaces;
using Mx.POS.Transight;

namespace Mx.POS.Common.Composite
{
    public class CompositePOS : ICompositePOS
    {
        private readonly List<IPosCommon> _posList = new List<IPosCommon>();

        public CompositePOS()
        {
            // This order has implications see the code below "GetMasterPOS"
            if (LiveLinkConfiguration.TransightEnabled)
            {
                _posList.Add(new TransightPos());
            }
            if (LiveLinkConfiguration.NewPOSTLDEnabled)
            {
                _posList.Add(new NewPOSTLD.NewPOSTLD());
            }
            if (LiveLinkConfiguration.NewPOSSummaryEnabled)
            {
                _posList.Add(new NewPOSSummary.NewPOSSummary());
            }
            if (LiveLinkConfiguration.HomeServiceEnabled)
            {
                _posList.Add(new HomeService.HomeService());
            }
            if (LiveLinkConfiguration.STMEnabled)
            {
                _posList.Add(new STM.STM());
            }
            if (LiveLinkConfiguration.ACTAtekFingerScannerEnabled)
            {
                _posList.Add(new FingerScanner(
                                new SalesMainRepository(LiveLinkConfiguration.ConnectionString), 
                                new Logger(),
                                new ActAtekDataSource(FingerScannerConfiguration.ACTAtekFingerScannerIPAddress, 
                                                 FingerScannerConfiguration.ACTAtekFingerScannerUserName, 
                                                 FingerScannerConfiguration.ACTAtekFingerScannerPassword),
                                new CommonConfig
                                                {
                                                        DaysToRecover = FingerScannerConfiguration.ACTAtekFingerScannerDaysToRecover,
                                                        DaysToRecoverOnStartup = FingerScannerConfiguration.ACTAtekFingerScannerDaysToRecoverOnStartup,
                                                        DoNotProcessBefore = FingerScannerConfiguration.ACTAtekFingerScannerDoNotProcessBefore,
                                                        GatewayEnabled = FingerScannerConfiguration.ACTAtekFingerScannerGatewayEnabled,
                                                        PollInterval = FingerScannerConfiguration.ACTAtekFingerScannerPollInterval
                                                }
                                           )
                             );
            }            
            RegisterChildHandlers();
        }

        private void RegisterChildHandlers()
        {
            _posList.ForEach(e => e.StatusChangedEvent += MyHandler);
        }

        private void MyHandler(object sender, Progress e)
        {
            StatusChangedEvent(this, e);
        }

        public string PosName
        {
            get { return GetMasterPOS().PosName; }
        }

        private IPosCommon GetMasterPOS()
        {
            return _posList[0];
        }

        public bool GatewayEnabled
        {
            get { return GetMasterPOS().GatewayEnabled; ; }
        }

        public int PollInterval
        {
            get { return GetMasterPOS().PollInterval; }
        }

        public DateTime LastPollDate
        {
            get { return GetMasterPOS().LastPollDate; }
            set {
                _posList.ForEach(p => p.LastPollDate = value);
            }
        }

        public void SetupDatabase()
        {
            _posList.ForEach(p => p.SetupDatabase());
        }

        public void ProcessData(int entityId)
        {
            _posList.ForEach(p => p.ProcessData(entityId));
        }

        public void PurgeOldData(int daysBeforeObsolete)
        {
            _posList.ForEach(p => p.PurgeOldData(daysBeforeObsolete));
        }

        public event EventHandler<Progress> StatusChangedEvent;

        public bool HasPosData()
        {
            return _posList.Count > 0;
        }
    }

    public interface ICompositePOS : IPosCommon
    {
        bool HasPosData();
    }
}
