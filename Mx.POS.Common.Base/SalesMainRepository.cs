using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using MMS.Utilities;
using Mx.POS.Common.Base.BusinessObjects;

namespace Mx.POS.Common.Base
{
    public class SalesMainRepository : ISalesMainRepository
    {
        private readonly string _connectionString;

        public SalesMainRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public bool PersistTransaction(ISalesTransaction salesTransaction)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                var transaction = connection.BeginTransaction();
                try
                {
                    foreach (var salesItem in salesTransaction.Records)
                    {
                        Insert(salesItem, connection, transaction);
                    }
                    transaction.Commit();
                }
                catch (SqlException)
                {
                    transaction.Rollback();
                }
                catch (Exception)
                {
                    transaction.Rollback();
                }
            }

            return true;
        }

        public SalesTransaction LoadTransaction(SalesItemRecord record)
        {
            var records = new List<SalesItemRecord>();
            using (var connection = new SqlConnection(_connectionString))
            {
                var cmd = new SqlCommand { Connection = connection };
                cmd.Connection.Open();

                cmd.CommandText =
                    string.Format("with LatestTransaction(TransactionVersion) " +
                                    "AS " +
                                    "( " +
                                    "    select MAX(TransactionVersion)from [tbSalesMain]  " +
                                    "    where POSTransactionID = {0}" +
                                    ") " +
                                    "select [SalesMainID] " +
                                        ",[RecordType] " +
                                        ",[TransactionID] " +
                                        ",[RecordSubType] " +
                                        ",[EntityID] " +
                                        ",[RegisterID] " +
                                        ",[PollDate] " +
                                        ",[PollCount] " +
                                        ",[PollAmount] " +
                                        ",[ClerkID] " +
                                        ",[ClerkName] " +
                                        ",[CustomerID] " +
                                        ",[Synced] " +
                                        ",[PLUCodeID] " +
                                        ",[PLUCode] " +
                                        ",[SequenceNo] " +
                                        ",[Flag] " +
                                        ",[ApplyTax] " +
                                        ",[ItemTax] " +
                                        ",[PriceLevel] " +
                                        ",[DateAdded] " +
                                        ",[SubTypeDescription] " +
                                        ",[POSTransactionID] " +
                                        ",[TransactionVersion] " +
                                        ",[SyncToken] " +
                                        ",[ParentId] " +
                                        ",[BusinessDay] " +
                                        ",[ItemDiscount] from [tbSalesMain] " + 
                                        " where POSTransactionID = {0} and transactionVersion = (select TransactionVersion from LatestTransaction)" +
                                        " order by SalesMainId asc" ,
                                  record.POSTransactionId);

                var reader = cmd.ExecuteReader();

                ISalesItemFactory factory = new SalesItemFactory();
                while (reader.Read())
                {
                    records.Add(factory.CreateSalesRow(reader));
                }
                reader.Close();
                connection.Close();
            }

            return new SalesTransaction(records);
        }        

        private void Insert(SalesItemRecord salesTransaction, SqlConnection connection, SqlTransaction transaction)
        {
            var cmd = GetSqlInsertCommand(salesTransaction, connection);

            cmd.Transaction = transaction;
            cmd.ExecuteNonQuery();
        }

        private SqlCommand GetSqlInsertCommand(SalesItemRecord salesTransaction, SqlConnection connection)
        {
            var cmd = connection.CreateCommand();

            var columns = new StringBuilder("[RecordType] " +
                                            ",[TransactionID] " +
                                            ",[EntityID] " +
                                            ",[PollDate] " +
                                            ",[PollCount] " +
                                            ",[PollAmount] " +
                                            ",[ClerkID] " +
                                            ",[ClerkName] " +
                                            ",[CustomerID] " +
                                            ",[Synced] " +
                                            ",[ApplyTax] " +
                                            ",[ItemTax] " +
                                            ",[DateAdded] " +
                                            ",[SubTypeDescription] " +
                                            ",[POSTransactionID] " +
                                            ",[TransactionVersion]");
            var values = new StringBuilder("@RecordType " +
                                           ",@TransactionID " +
                                           ",@EntityID " +
                                           ",@PollDate " +
                                           ",@PollCount " +
                                           ",@PollAmount " +
                                           ",@ClerkID " +
                                           ",@ClerkName " +
                                           ",@CustomerID " +
                                           ",@Synced " +
                                           ",@ApplyTax " +
                                           ",@ItemTax " +
                                           ",GetDate() " +
                                           ",@SubTypeDescription " +
                                           ",@POSTransactionID " +
                                           ",@TransactionVersion");

            cmd.Parameters.AddWithValue("@RecordType", EnumUtils<Mx.BusinessObjects.LiveLink.SalesRecordType>.GetClassName(salesTransaction.RecordType));
            cmd.Parameters.AddWithValue("@TransactionId", salesTransaction.TransactionId);
            cmd.Parameters.AddWithValue("@EntityID", salesTransaction.EntityId);
            cmd.Parameters.AddWithValue("@PollDate", salesTransaction.PollDate);
            cmd.Parameters.AddWithValue("@PollCount", salesTransaction.PollCount);
            cmd.Parameters.AddWithValue("@PollAmount", salesTransaction.PollAmount);
            cmd.Parameters.AddWithValue("@ClerkID", salesTransaction.ClerkId);
            cmd.Parameters.AddWithValue("@ClerkName", salesTransaction.ClerkName);
            cmd.Parameters.AddWithValue("@CustomerID", salesTransaction.CustomerId);
            cmd.Parameters.AddWithValue("@Synced", salesTransaction.Synced);
            cmd.Parameters.AddWithValue("@ApplyTax", salesTransaction.ApplyTax);
            cmd.Parameters.AddWithValue("@ItemTax", salesTransaction.ItemTax);
            cmd.Parameters.AddWithValue("@SubTypeDescription", salesTransaction.SubTypeDescription);
            cmd.Parameters.AddWithValue("@POSTransactionID", salesTransaction.POSTransactionId);
            cmd.Parameters.AddWithValue("@TransactionVersion", salesTransaction.TransactionVerion);

            if (salesTransaction.BusinessDay != null)
            {
                cmd.Parameters.AddWithValue("@BusinessDay", salesTransaction.BusinessDay);
                values.Append(", @BusinessDay");
                columns.Append(", BusinessDay");
            }

            if (salesTransaction.RecordSubType != null)
            {
                cmd.Parameters.AddWithValue("@RecordSubType", salesTransaction.RecordSubType);
                values.Append(", @RecordSubType");
                columns.Append(", RecordSubType");
            }

            if (salesTransaction.RegisterId != null)
            {
                cmd.Parameters.AddWithValue("@RegisterId", salesTransaction.RegisterId);
                values.Append(", @RegisterId");
                columns.Append(", RegisterId");
            }

            if (salesTransaction.ItemDiscount != null)
            {
                cmd.Parameters.AddWithValue("@ItemDiscount", salesTransaction.ItemDiscount);
                values.Append(", @ItemDiscount");
                columns.Append(", ItemDiscount");
            }

            if (salesTransaction.PLUCode != null)
            {
                cmd.Parameters.AddWithValue("@PLUCode", salesTransaction.PLUCode);
                values.Append(", @PLUCode");
                columns.Append(", PLUCode");
            }

            if (salesTransaction.PLUCodeID != null)
            {
                cmd.Parameters.AddWithValue("@PLUCodeID", salesTransaction.PLUCodeID);
                values.Append(", @PLUCodeID");
                columns.Append(", PLUCodeID");
            }

            if (salesTransaction.SequenceNo != null)
            {
                cmd.Parameters.AddWithValue("@SequenceNo", salesTransaction.SequenceNo);
                values.Append(", @SequenceNo");
                columns.Append(", SequenceNo");
            }

            if (salesTransaction.Flag != null)
            {
                cmd.Parameters.AddWithValue("@Flag", salesTransaction.Flag);
                values.Append(", @Flag");
                columns.Append(", Flag");
            }
            if (salesTransaction.PriceLevel != null)
            {
                cmd.Parameters.AddWithValue("@PriceLevel", salesTransaction.PriceLevel);
                values.Append(", @PriceLevel");
                columns.Append(", PriceLevel");
            }
            if (salesTransaction.MicrosId != null)
            {
                cmd.Parameters.AddWithValue("@MicrosId", salesTransaction.MicrosId);
                values.Append(", @MicrosId");
                columns.Append(", MicrosId");
            }
            if (salesTransaction.ParentId != null)
            {
                cmd.Parameters.AddWithValue("@ParentId", salesTransaction.ParentId);
                values.Append(", @ParentId");
                columns.Append(", ParentId");
            }

            cmd.CommandText = string.Format("INSERT INTO [tbSalesMain] ({0}) VALUES ({1})",
                                            columns,
                                            values);
            return cmd;
        }
    }

    public interface ISalesMainRepository
    {
        bool PersistTransaction(ISalesTransaction salesTransaction);
        SalesTransaction LoadTransaction(SalesItemRecord record);
    }

    public interface ISalesItemFactory
    {
        SalesItemRecord CreateSalesRow(SqlDataReader reader);
    }
}