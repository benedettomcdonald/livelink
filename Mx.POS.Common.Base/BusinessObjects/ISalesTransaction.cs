using System.Collections.Generic;

namespace Mx.POS.Common.Base.BusinessObjects
{
    public interface ISalesTransaction
    {
        bool IsValid();
        IEnumerable<SalesItemRecord> Records { get; }
        void SetTransactionVersion(int transactionVersion);
        bool HasRecords();
    }
}