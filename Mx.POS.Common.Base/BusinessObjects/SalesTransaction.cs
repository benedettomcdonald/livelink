using System.Collections.Generic;
using System.Linq;

namespace Mx.POS.Common.Base.BusinessObjects
{
    public class SalesTransaction : ISalesTransaction
    {
        private readonly List<SalesItemRecord> _records = null;

        public SalesTransaction(List<SalesItemRecord> records)
        {
            _records = records;
        }

        public IEnumerable<SalesItemRecord> Records
        {
            get
            {
                 return _records;
            }
        }

        public bool IsValid()
        {
            return true;
        }

        public void SetTransactionVersion(int transactionVersion)
        {
            foreach (var item in _records)
            {
                item.TransactionVerion = transactionVersion;
            }
        }

        public bool HasRecords()
        {
            return _records.Any();
        }

        public int GetVersionNumber()
        {
            return (_records[0].TransactionVerion.HasValue)? _records[0].TransactionVerion.Value : 0;
        }
    }
}