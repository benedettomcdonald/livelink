using System;
using Mx.BusinessObjects.LiveLink;

namespace Mx.POS.Common.Base.BusinessObjects
{
    public class SalesItemRecord
    {
        public SalesItemRecord()
        {

        }

        public SalesItemRecord(SalesItemRecord item)
        {
            RecordType = item.RecordType;
            TransactionId = item.TransactionId;
            POSTransactionId = item.POSTransactionId;
            EntityId = item.EntityId;
            RegisterId = item.RegisterId;
            ClerkId = item.ClerkId;
            ClerkName = item.ClerkName;
            PollDate = item.PollDate;
            Synced = item.Synced;
            PLUCode = item.PLUCode;
            PLUCodeID = item.PLUCodeID;
            PollCount = item.PollCount;
            PollAmount = item.PollAmount;
            ItemTax = item.ItemTax;
            ItemDiscount = item.ItemDiscount;
            ApplyTax = item.ApplyTax;
            BusinessDay = item.BusinessDay;
            ParentId = item.ParentId;
            RecordSubType = item.RecordSubType;
            CustomerId = item.CustomerId;
            SequenceNo = item.SequenceNo;
            Flag = item.Flag;
            PriceLevel = item.PriceLevel;
            TransactionVerion = item.TransactionVerion;
            MicrosId = item.MicrosId;
            SubTypeDescription = item.SubTypeDescription;
            LastUpdateTime = item.LastUpdateTime;
        }

        public Mx.BusinessObjects.LiveLink.SalesRecordType RecordType { get; set; }
        public long? TransactionId { get; set; }
        public long? POSTransactionId { get; set; }
        public int? EntityId { get; set; }
        public int? RegisterId { get; set; }
        public int? ClerkId { get; set; }
        public string ClerkName { get; set; }
        public string PollDate { get; set; }
        public int? Synced { get; set; }
        public string PLUCode { get; set; }
        public long? PLUCodeID { get; set; }
        public decimal? PollCount { get; set; }
        public decimal? PollAmount { get; set; }
        public decimal? ItemTax { get; set; }
        public decimal? ItemDiscount { get; set; }
        public int? ApplyTax { get; set; }
        public string BusinessDay { get; set; }
        public int? ParentId { get; set; }
        public string RecordSubType { get; set; }
        public string CustomerId { get; set; }
        public int? SequenceNo { get; set; }
        public int? Flag { get; set; }
        public string PriceLevel { get; set; }
        public int? TransactionVerion { get; set; }
        public int? MicrosId { get; set; }
        public string SubTypeDescription { get; set; }
        public DateTime? RefundDate { get; set; }
        public string RefundType { get; set; }
        public DateTime LastUpdateTime { get; set; }
    }
}

