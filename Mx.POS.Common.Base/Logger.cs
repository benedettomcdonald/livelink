using System.Collections.Generic;
using MMS.Utilities;

namespace Mx.POS.Common.Base
{
    public class Logger : ILogger
    {
        public void LogInformationToTrace(string messageFormat, string param)
        {
            MxLogger.LogInformationToTrace(messageFormat, param);
        }

        public void LogError(MxLogger.LogCategory category, string messageFormat, params object[] parameters)
        {
            MxLogger.LogError(category, messageFormat, parameters);
        }

        public void LogError(MxLogger.LogCategory category, MxLogger.EventId eventId, int priority, string title, Dictionary<string, object> extraProperties,
            string messageFormat, params object[] parameters)
        {
            MxLogger.LogError(category, eventId, priority, title, extraProperties, messageFormat, parameters);
        }
    }
}