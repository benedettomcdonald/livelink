using System;
using System.Data.SqlClient;
using System.Reflection;
using MMS.Utilities;
using Mx.POS.Common.Base.BusinessObjects;

namespace Mx.POS.Common.Base
{
    public class SalesItemFactory : ISalesItemFactory
    {
        public SalesItemRecord CreateSalesRow(SqlDataReader reader)
        {
            var salesItem = new SalesItemRecord
            {
                RecordType = GetRecordTypeFromDescription(ReadType<string>(reader, "RecordType")),
                TransactionId = ReadType<long?>(reader, "TransactionID"),
                RecordSubType = ReadType<string>(reader, "RecordSubType"),
                EntityId = ReadType<int?>(reader, "EntityID"),
                RegisterId = ReadType<int?>(reader, "RegisterID"),
                PollDate = ReadType<string>(reader, "PollDate"),
                PollCount = (decimal?) ReadNullableFloat(reader, "PollCount"),
                PollAmount = ReadType<decimal?>(reader, "PollAmount"),
                ClerkId = ReadType<int?>(reader, "ClerkID"),
                ClerkName = ReadType<string>(reader, "ClerkName"),
                CustomerId = ReadType<string>(reader, "CustomerID"),
                Synced = ReadType<int?>(reader, "Synced"),
                PLUCodeID = ReadType<long?>(reader, "PLUCodeID"),
                PLUCode = ReadType<string>(reader, "PLUCode"),
                SequenceNo = ReadType<int?>(reader, "SequenceNo"),
                Flag = ReadType<int?>(reader, "Flag"),
                ApplyTax = ReadType<int?>(reader, "ApplyTax"),
                ItemTax = ReadType<decimal?>(reader, "ItemTax"),
                PriceLevel = ReadType<string>(reader, "PriceLevel"),
                SubTypeDescription = ReadType<string>(reader, "SubTypeDescription"),
                POSTransactionId = ReadType<long?>(reader, "POSTransactionID"),
                TransactionVerion = ReadType<int?>(reader, "TransactionVersion"),
                ParentId = ReadType<int?>(reader, "ParentId"),
                BusinessDay = NullableDateToString(reader),
                ItemDiscount = ReadType<decimal?>(reader, "ItemDiscount"),
                MicrosId = SafeReadMicrosId(reader, null)
            };

            return salesItem;
        }

        private int? SafeReadMicrosId(SqlDataReader reader, int? defaultValue)
        {
            try
            {
                return ReadType<int?>(reader, "MicrosId");
            }
            catch (Exception)
            {
                return defaultValue;
            }
        }

        private Mx.BusinessObjects.LiveLink.SalesRecordType GetRecordTypeFromDescription(string value)
        {
            foreach (Mx.BusinessObjects.LiveLink.SalesRecordType type in Enum.GetValues(typeof(Mx.BusinessObjects.LiveLink.SalesRecordType)))
            {
                if (EnumUtils<Mx.BusinessObjects.LiveLink.SalesRecordType>.GetClassName(type) == value)
                {
                    return type;
                }
            }
            throw new AmbiguousMatchException(String.Format("[{0}] is not a valid SalesRecordType", value));
        }

        private float? ReadNullableFloat(SqlDataReader reader, string fieldName)
        {
            if (reader[fieldName] == DBNull.Value)
            {
                return null;
            }
            return float.Parse(reader[fieldName].ToString());
        }

        private T ReadType<T>(SqlDataReader reader, string fieldName)
        {
           return (reader[fieldName] == DBNull.Value) ? (T)(object)null : (T)reader[fieldName];
        }

        private string NullableDateToString(SqlDataReader reader)
        {
            var nullableDate = ReadType<DateTime?>(reader, "BusinessDay");
            return nullableDate.HasValue ? nullableDate.Value.ToString(DateFormatNames.DatabaseNeutralDateFormatWithMilliSecond) : null;
        }
    }
}