﻿namespace Mx.POS.Common.Base
{
    public class CommonConfig
    {
        public bool GatewayEnabled { get; set; }
        public int PollInterval { get; set; }
        public int DaysToRecover { get; set; }
        public int DaysToRecoverOnStartup { get; set; }
        public string DoNotProcessBefore { get; set; }
    }
}
