using System.Collections.Generic;
using MMS.Utilities;

namespace Mx.POS.Common.Base
{
    public interface ILogger
    {
        void LogInformationToTrace(string messageFormat, string param);

        void LogError(MxLogger.LogCategory category, string messageFormat, params object[] parameters);

        void LogError(MxLogger.LogCategory category, MxLogger.EventId eventId, int priority, string title, Dictionary<string, object> extraProperties, string messageFormat, params object[] parameters);
    }
}